package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.accounts.dto.GratuityPaymentDTO;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.dto.PettyCashExpenseDTO;
import in.gov.cooptex.finance.service.GratuityPaymentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("${finance.api.url}/gratuitypayment")
@Api(tags = "Gratuity Payment", value = "Gratuity Payment")
@Log4j2
public class GratuityPaymentController {

	@Autowired
	GratuityPaymentService gratuityPaymentService;
	
	@RequestMapping(value = "/getAllGratuityEmployeesByEntityId", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getAllGratuityEmployeesByEntityId() {
		log.info("GratuityPaymentController:getAllGratuityEmployeesByEntityId()");
		BaseDTO baseDTO = gratuityPaymentService.getAllGratuityEmployeesByEntityId();
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getGratuityDetailsByEmployeeId/{employeeId}", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getGratuityDetailsByEmployeeId(@PathVariable Long employeeId) {
		log.info("GratuityPaymentController:getGratuityDetailsByEmployeeId()");
		BaseDTO baseDTO = gratuityPaymentService.getGratuityDetailsByEmployeeId(employeeId);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getById/{id}", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getById(@PathVariable Long id) {
		log.info("GratuityPaymentController:getById()");
		BaseDTO baseDTO = gratuityPaymentService.getById(id);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> createGratuityPayment(@RequestBody GratuityPaymentDTO gratuityPaymentDTO) {
		log.info("PettyCashExpenseController:createGratuityPayment()");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = gratuityPaymentService.createGratuityPayment(gratuityPaymentDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/lazyloadlist", method = RequestMethod.POST)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> lazyloadlist(@RequestBody PaginationDTO paginationDTO) {
		log.info("GratuityPaymentController:lazyloadlist()");
		BaseDTO baseDTO = gratuityPaymentService.lazyloadlist(paginationDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/approveGratuityPayment", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> approveGratuityPayment(
			@RequestBody GratuityPaymentDTO gratuityPaymentDTO) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = gratuityPaymentService.approveGratuityPayment(gratuityPaymentDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/rejectGratuityPayment", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> rejectGratuityPayment(
			@RequestBody GratuityPaymentDTO gratuityPaymentDTO) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = gratuityPaymentService.rejectGratuityPayment(gratuityPaymentDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
}
