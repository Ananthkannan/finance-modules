package in.gov.cooptex.finance.controller;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.SocietyStopInvoiceDTO;
import in.gov.cooptex.core.dto.StopSocietyInvoiceDTO;
import in.gov.cooptex.dto.pos.ReleaseSocietyInvoicesListDTO;
import in.gov.cooptex.dto.pos.SocietyReleaseInvoiceDTO;
import in.gov.cooptex.finance.InvoiceDetailsRequestDTO;
import in.gov.cooptex.finance.dto.SearchSocietyInvoiceAdjustmentDTO;
import in.gov.cooptex.finance.service.ReleaseSocietyInvoiceService;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/societyreleaseinvoice")
public class ReleaseInvoiceController {

	@Autowired
	ReleaseSocietyInvoiceService releaseSocietyInvoiceService;

	/**
	 * Return the Released purchase invoices
	 * 
	 * @param paginationDTO
	 * @return BaseDTO
	 */
	@RequestMapping(value = "/getReleaseSocietyInvoice", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> getReleaseSocietyInvoice(@RequestBody PaginationDTO paginationDTO) {
		log.info("<---------Starts releaseSocietyInvoiceService.getReleaseSocietyInvoiceService--->");
		BaseDTO baseDTO = releaseSocietyInvoiceService.getReleaseSocietyInvoiceService(paginationDTO);
		log.info("<---------Ended releaseSocietyInvoiceService.getReleaseSocietyInvoiceService---->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);

	}

	/**
	 * Create Release Purchase Invoices based on the given inputs
	 * 
	 * @param societyStopInvoiceDTO
	 * @return
	 */
	@RequestMapping(value = "/addReleaseSocietyInvoice", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> addReleaseSocietyInvoice(@RequestBody SocietyStopInvoiceDTO societyStopInvoiceDTO) {
		log.info("<---------Starts releaseSocietyInvoiceService.createReleaseStopInvoice----->");
		BaseDTO baseDTO = releaseSocietyInvoiceService.createReleaseStopInvoice(societyStopInvoiceDTO);
		log.info("<---------Ended releaseSocietyInvoiceService.createReleaseStopInvoice----->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);

	}

	/**
	 * Search the Stopped Purchase invoices based on the given search inputs
	 * 
	 * @param societyId
	 * @return
	 */
	@RequestMapping(value = "/searchReleaseSocietyInvoice/{societyId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> searchReleaseSocietyInvoice(@PathVariable Long societyId) {
		log.info("<---------Starts releaseSocietyInvoiceService.searchPurchaseInvoiceDetails----->");
		BaseDTO baseDTO = releaseSocietyInvoiceService.searchPurchaseInvoiceDetails(societyId);
		log.info("<---------Ended releaseSocietyInvoiceService.searchPurchaseInvoiceDetails----->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);

	}

	/**
	 * View the Released purchase invoice details
	 * 
	 * @param societyReleaseInvoiceDTO
	 * @return
	 */
	@RequestMapping(value = "/viewreleasepurchaseinvoicedetails", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> viewReleasePurchaseInvoiceDetails(
			@RequestBody SocietyReleaseInvoiceDTO societyReleaseInvoiceDTO) {
		log.info("<---------Starts releaseSocietyInvoiceService.viewReleasePurchaseInvoiceDetails----->");
		BaseDTO baseDTO = releaseSocietyInvoiceService.viewReleasePurchaseInvoiceDetails(societyReleaseInvoiceDTO);
		log.info("<---------Ended releaseSocietyInvoiceService.viewReleasePurchaseInvoiceDetails----->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);

	}

	@RequestMapping(value = "/getcirclemasterdetailsbydate", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<BaseDTO> getCircleMasterDetailsbyDate(
			@RequestBody SearchSocietyInvoiceAdjustmentDTO sivid) {
		log.info(
				" ----------------Controller - SocietyPaymentVoucherController : method : getCircleMasterByDistrictIds :: starts----------- ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = releaseSocietyInvoiceService.getCircleDetailsbyDate(sivid);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/searchsocietypurchaseinvoice", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> searchSocietyPurchaseInvoice(
			@RequestBody InvoiceDetailsRequestDTO invoiceDetailsRequestDTO) {
		log.info(
				"<-----------Starts societystopinvoice.searchPurchaseInvoiceDetails------>" + invoiceDetailsRequestDTO);
		BaseDTO baseDTO = releaseSocietyInvoiceService.searchPurchaseInvoiceDetails(invoiceDetailsRequestDTO);
		log.info("<-----------Ended societystopinvoice.searchPurchaseInvoiceDetails------>" + invoiceDetailsRequestDTO);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);

	}
	
//	get society 
	@RequestMapping(value = "/getsocietybystopinvoice", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> getSocietyByStopInvoice(@RequestBody SearchSocietyInvoiceAdjustmentDTO searchSocietyInvoiceAdjustmentDTO) {
		log.info(" =======5555===Controller - SocietyPaymentVoucherController : method : getSocietyByLoomTypes :: starts==============");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = releaseSocietyInvoiceService.getSocietyByStopInvoice(searchSocietyInvoiceAdjustmentDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/updatesocietystoplog", method = RequestMethod.PUT)
	public ResponseEntity<BaseDTO> updateSocietyStopLog(@RequestBody StopSocietyInvoiceDTO stopSocietyInvoiceDTO, HttpServletRequest request) {
		log.info("<<=========== IncrementController-----societystopinvoice ========STARTS ");
		BaseDTO	baseDTO = releaseSocietyInvoiceService.updateSocietyStopLog(stopSocietyInvoiceDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	} 

}
