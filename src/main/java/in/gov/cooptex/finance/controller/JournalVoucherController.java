package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.accounts.model.CashCreditLoanLimit;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.JournalVoucherDTO;
import in.gov.cooptex.finance.dto.WeaversBenefitPaymentDTO;
import in.gov.cooptex.finance.service.JournalVoucherService;
import in.gov.cooptex.finance.service.WeaverBenefitPaymentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("${finance.api.url}/journalvoucher")
@Api(tags = "Journal Voucher", value = "Journal Voucher")
@Log4j2
public class JournalVoucherController {
	
	@Autowired
	JournalVoucherService journalVoucherService;
	
	
	@RequestMapping(value = "/getallglaccounts", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getAllGlAccounts() {
		log.info("JournalVoucherController:getAllGlAccounts()");
		BaseDTO baseDTO = journalVoucherService.getAllGlAccounts();
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> createjournalVoucher(@RequestBody JournalVoucherDTO journalVoucherDTO) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = journalVoucherService.createjournalVoucher(journalVoucherDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	
	
    @ApiOperation(value = "journalVoucher Get ALL Service",response = Iterable.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "All journalVoucher Get Successfully Retrived")})
    @RequestMapping(value = "/getjournalvoucherlist", method = RequestMethod.POST)
    public ResponseEntity<BaseDTO> getweaverBenefitPaymentList(@RequestBody PaginationDTO paginationDTO) {
        log.info("<--Starts getjournalvoucherController .getjournalvoucherlist-->");
        BaseDTO baseDTO = journalVoucherService.getjournalvoucherlist(paginationDTO);
        log.info("<--Ends .getjournalvoucherlist-->");
        return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
    }
    
    
    @RequestMapping(value = "/getdetailsbyid/{id}", method = RequestMethod.GET)
    public @ResponseBody
    BaseDTO getJournalVouchersById(@PathVariable("id") Long id) {
            return journalVoucherService.getDetailsById(id);
    }
    
	@RequestMapping(value = "/approvejv", method = RequestMethod.POST)
	public @ResponseBody BaseDTO approvePayment(@RequestBody JournalVoucherDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			baseDTO = journalVoucherService.approvejv(requestDTO);
		} catch (Exception ex) {
			log.info("JournalVoucher Controller ex:" + ex);
		}
		return baseDTO;
	}
	
	@RequestMapping(value = "/rejectjv", method = RequestMethod.POST)
	public @ResponseBody BaseDTO rejectPayment(@RequestBody JournalVoucherDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			baseDTO = journalVoucherService.rejectjv(requestDTO);
		} catch (Exception ex) {
			log.info("Journal Voucher Controller ex:" + ex);
		}
		return baseDTO;
	}
	
	@RequestMapping(value = "/getEntity", method = RequestMethod.GET)
	public BaseDTO getEntity() {
		log.info("<-------------getjournalvoucherController getEntity method----------------->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			baseDTO = journalVoucherService.getEntity();
		} catch (Exception e) {
			log.info("<<==  Exception occured in getEntity ==>>",e);
			
		}
		return baseDTO;
	}

}
