package in.gov.cooptex.finance.controller;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.BankToBankResponseDTO;
import in.gov.cooptex.finance.BankToBankViewResponseDTO;
import in.gov.cooptex.finance.BanktoBankAddDTO;
import in.gov.cooptex.finance.ChequeToBankResponseDTO;
import in.gov.cooptex.finance.service.AmountTransferDetailsService;
import in.gov.cooptex.finance.service.BankToBankService;
import in.gov.cooptex.finance.service.CashToBankServices;
import io.swagger.annotations.Api;
import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/banktobank")
@Api(tags = "bank bank", value = "bank bank")
@Log4j2
public class BankToBankController {
    @Autowired
    CashToBankServices cashToBankServices;
    @Autowired
    AmountTransferDetailsService amountTransferDetailsService;
    

    @Autowired
    BankToBankService bankToBankService;

 /*   @RequestMapping(value = "/addbanktobank", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<BaseDTO> addBankToBank(@RequestBody BanktoBankAddDTO data) {

            log.info("<--Starts BankToBankController Creating-->"+data);
        BaseDTO wrapperDto=amountTransferDetailsService.addBankToBank(data);

            log.info("<--Ends BankToBankController Creating Ended -->");
            return new ResponseEntity<BaseDTO>(wrapperDto, HttpStatus.OK );
    }
    */
    
    @RequestMapping(method = RequestMethod.POST, value = "/addbanktobank")
	public BaseDTO addBankToBank(@RequestBody BanktoBankAddDTO data) {
        log.info("<--Starts BankToBankController Creating-->"+data);
		return amountTransferDetailsService.addBankToBank(data);
	}
	
    
	@RequestMapping(value = "/getallemployee", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getAllEmployee() {
		BaseDTO	baseDTO=amountTransferDetailsService.getAllEmployee();
		log.info("<---Ends EmployeeController.getAllEmployee()--->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
    
    
    
    
    
    
    @RequestMapping(value = "/getall", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<BaseDTO> getBankToBankDetails(@RequestBody PaginationDTO paginationDTO) {
        BaseDTO wrapperDto=bankToBankService.getBankToBankDetails(paginationDTO);
        return new ResponseEntity<BaseDTO>(wrapperDto, HttpStatus.OK );
    }
    
    
    @RequestMapping(value = "/getdetailsbyid/{id}", method = RequestMethod.GET)
    public @ResponseBody
    BaseDTO getBankToBankDetailsById(@PathVariable("id") Long id) {
          // BaseDTO wrapperDto=bankToBankService.getDetailsById(id);
            return bankToBankService.getDetailsById(id);
    }
    
    @RequestMapping(value = "/geteditbyid/{id}", method = RequestMethod.GET)
    public @ResponseBody
    BaseDTO getBankToBankEditById(@PathVariable("id") Long id) {
          // BaseDTO wrapperDto=bankToBankService.getDetailsById(id);
            return bankToBankService.getBankToBankEditById(id);

    }
    
    

	
	@RequestMapping(value = "/approveBankToBank", method = RequestMethod.POST)
	public @ResponseBody BaseDTO approveBankToBank(@RequestBody BankToBankResponseDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			baseDTO = bankToBankService.approveBankToBank(requestDTO);
		} catch (Exception ex) {
			log.info("Cheque to Bank Transfer Controller ex:" + ex);
		}
		return baseDTO;
	}
	
	
	
	@RequestMapping(value = "/rejectBankToBank", method = RequestMethod.POST)
	public @ResponseBody BaseDTO rejectChequeToBank(@RequestBody BankToBankResponseDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			baseDTO = bankToBankService.rejectBankToBank(requestDTO);
		} catch (Exception ex) {
			log.info("Cheque to Bank Transfer Controller ex:" + ex);
		}
		return baseDTO;
	}
    
	@RequestMapping(method = RequestMethod.POST, value = "/updateAmountTransferDetails")
	public BaseDTO updateBanktoBank(@RequestBody BankToBankViewResponseDTO data) {
        log.info("<--Starts BankToBankController Creating-->"+data);
		return amountTransferDetailsService.updateAmountTransferDetails(data);
	}
	
    
    
}
