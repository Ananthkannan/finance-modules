package in.gov.cooptex.finance.controller;

import in.gov.cooptex.common.service.EntityMasterService;
import in.gov.cooptex.core.accounts.dto.TdsPaymentRequestDTO;
import in.gov.cooptex.core.accounts.dto.TdsPaymentViewDTO;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.TourProgramResponseDTO;
import in.gov.cooptex.finance.TourProgramSaveDto;
import in.gov.cooptex.finance.service.TdsPaymentService;
import in.gov.cooptex.finance.service.TourProgramJournalService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/tdsPayment")
@Log4j2
public class TdsPaymentController {

    
    @Autowired
    TourProgramJournalService tourProgramJournalService;
    
    @Autowired
    TdsPaymentService tdsPaymentService;
    
    @Autowired
    EntityMasterService entityMasterService;

    @RequestMapping(value = "/getall", method = RequestMethod.POST)
    public @ResponseBody
    BaseDTO getTourProgramDetails(@RequestBody PaginationDTO paginationDTO) {
    	log.info("<===== Start TdsPaymentController.getAll TdsPaymentList ======>");
        BaseDTO wrapperDto = null;
        try {
            return wrapperDto= tdsPaymentService.getTdsPaymentDetails(paginationDTO);
        } catch (Exception ex) {
            log.info("TdsPayment Controller exception while getAll ex:" + ex);
        }
        return wrapperDto;
    }
    
     
    @RequestMapping(value = "/getSupplierDetails/{id}", method = RequestMethod.GET)
    public ResponseEntity<BaseDTO> getSupplerDetailsByEntityId(@PathVariable("id") Long id) {
		BaseDTO	baseDTO=tdsPaymentService.getSupplierDetails(id);
		log.info("<---Ends TdsPaymentController.getAllEmployee()--->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
    
    @RequestMapping(value = "/getAllEntityMaster", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getAllEntity() {
		BaseDTO	baseDTO=tdsPaymentService.getAllActiveEntityMaster();
		log.info("<---Ends TdsPaymentController.getAllEmployee()--->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
    
    @RequestMapping(value = "/getPurchaseDetails", method = RequestMethod.POST)
    public @ResponseBody
    BaseDTO getPurchaseDetials(@RequestBody TdsPaymentRequestDTO tdsPaymentDTO) {
    	log.info("<===== Start TDsPaymentController.getPurchaseDetails ======>");
        BaseDTO wrapperDto = null;
        try {
            return wrapperDto= tdsPaymentService.getPurchaseDetails(tdsPaymentDTO);
        } catch (Exception ex) {
            log.info("TdsPayment Controller exception while getPurchaseDetails ex:" + ex);
        }
        return wrapperDto;
    }
    
    @RequestMapping(method = RequestMethod.POST, value = "/savetdspayment")
	public BaseDTO savetourprogram(@RequestBody TdsPaymentRequestDTO data) {
        log.info("<--Starts TdsPaymentController Saveing -->"+data);
		return tdsPaymentService.saveTdsPaymentDetails(data);
	}
    
    @RequestMapping(value = "/getEdit/{id}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<BaseDTO> getEditTourById(@PathVariable("id") Long id) {
    	log.info("<===== Start TDSPaymentController.getEdit TdsPaymentList ======>");
        BaseDTO wrapperDto=tdsPaymentService.getTdsPaymentEditById(id);
        return new ResponseEntity<BaseDTO>(wrapperDto, HttpStatus.OK );
    }
    
    
	@RequestMapping(value = "/approveTdsPayment", method = RequestMethod.POST)
	public @ResponseBody BaseDTO approveBankToCash(@RequestBody TdsPaymentViewDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			baseDTO = tdsPaymentService.approveTdsPayment(requestDTO);
		} catch (Exception ex) {
			log.info("TdsPaymentController while approved ex:" + ex);
		}
		return baseDTO;
	}
	
	@RequestMapping(value = "/rejectTdsPayment", method = RequestMethod.POST)
	public @ResponseBody BaseDTO rejectChequeToCash(@RequestBody TdsPaymentViewDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			baseDTO = tdsPaymentService.rejectTdsPayment(requestDTO);
		} catch (Exception ex) {
			log.info("TdsPaymentController while reject ex:" + ex);
		}
		return baseDTO;
	}
}


