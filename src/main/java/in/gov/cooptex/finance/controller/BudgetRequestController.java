package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.accounts.model.BudgetRequest;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.dto.BudgetRequestDTO;
import in.gov.cooptex.finance.service.BudgetRequestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("${finance.api.url}/budgetrequest")
@Api(tags = "Budget Request", value = "Budget Request")
@Log4j2
public class BudgetRequestController {

	@Autowired
	BudgetRequestService budgetRequestService;

	@RequestMapping(value = "/get", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> getById(@RequestBody BudgetRequestDTO selectedBudgetRequestDto) {
		log.info("BudgetRequestController:get() [" + selectedBudgetRequestDto + "]");
		BaseDTO baseDTO = budgetRequestService.getById(selectedBudgetRequestDto);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/getActiveBudgetRequest", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getActiveBudgetRequest() {
		log.info("BudgetRequestController:getActiveBudgetRequest()");
		BaseDTO baseDTO = budgetRequestService.getActiveBudgetRequest();
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> createBudgetRequest(@RequestBody BudgetRequestDTO budgetRequest) {
		log.info("BudgetRequestController:createBudgetRequest"+budgetRequest);
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = budgetRequestService.createBudgetRequest(budgetRequest);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	
	@GetMapping(value = "/generate/{budgetConfigId}/{entityId}")
	public ResponseEntity<BaseDTO> generateBudgetExpense(@PathVariable("budgetConfigId") Long budgetConfigId,@PathVariable("entityId") Long entityId) {
		BaseDTO baseDTO = new BaseDTO();
		log.info("BudgetRequestController generateBudgetExpense starts ......");
		log.info("budgetConfidId::"+budgetConfigId+"\t entityId ::"+entityId);
		baseDTO = budgetRequestService.generateExpenseDetails(budgetConfigId,entityId,null);
		log.info("BudgetRequestController generateBudgetExpense completed ......");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping(value = "/generate/{budgetConfigId}/{entityId}/{sectionId}")
	public ResponseEntity<BaseDTO> generateBudgetExpense(@PathVariable("budgetConfigId") Long budgetConfigId,@PathVariable("entityId") Long entityId,@PathVariable("sectionId") Long sectionId) {
		BaseDTO baseDTO = new BaseDTO();
		log.info("BudgetRequestController generateBudgetExpense starts ......");
		log.info("budgetConfidId::"+budgetConfigId+"\t entityId ::"+entityId+"\t sectionId::"+sectionId);
		baseDTO = budgetRequestService.generateExpenseDetails(budgetConfigId,entityId,sectionId);
		log.info("BudgetRequestController generateBudgetExpense completed ......");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/approve", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO>  approveBudgetRequest(@RequestBody BudgetRequestDTO budgetRequestDTO) {
		log.info("<--Starts BudgetRequestController .approveBudgetRequest-->");	
		BaseDTO baseDTO = budgetRequestService.approveBudgetRequest(budgetRequestDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping(value="/reject")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO>  rejectBudgetRequest(@RequestBody BudgetRequestDTO budgetRequestDTO) {
		log.info("<--Starts BudgetRequestController .rejectBudgetRequest-->");	
		BaseDTO baseDTO = budgetRequestService.rejectBudgetRequest(budgetRequestDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping(value="/finalapprove")
	public ResponseEntity<BaseDTO>  finalApproveBudgetRequest(@RequestBody BudgetRequestDTO budgetRequestDTO) {
		log.info("<--Starts BudgetRequestController .finalApproveBudgetRequest-->");	
		BaseDTO baseDTO = budgetRequestService.finalApproveBudgetRequest(budgetRequestDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping(value="/getallbudgetrequestlistlazy")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO>  getAllBudgetRequestListLazy(@RequestBody PaginationDTO paginationDto) {
		log.info("<--Starts BudgetRequestController .getAllBudgetRequestListLazy-->");	
		BaseDTO baseDTO = budgetRequestService.getAllBudgetRequestListLazy(paginationDto);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@DeleteMapping(value="/deletebyid/{budgetRequestId}")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO>  deleteById(@PathVariable Long budgetRequestId) {
		log.info("<--Starts BudgetRequestController .deleteById-->");	
		BaseDTO baseDTO = budgetRequestService.deleteById(budgetRequestId);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/findempdetailsbyloggedinuser/{userId}", method = RequestMethod.GET)
	public BaseDTO findEmployeeDetailsByLoggedInUser(@PathVariable Long userId) {
		log.info("<--- Starts find Employee Details By LoggedIn User Id :- "+userId);
		BaseDTO	baseDTO = budgetRequestService.findEmployeeDetailsByLoggedInUser(userId);
		return baseDTO;
	}
	
	
}