package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.accounts.dto.InvestmentInterestCollectionDTO;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.service.InvestmentInterestCollectionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("${finance.api.url}/investmentInterestCollection")
@Api(tags = "Investment InterestCollection", value = "Investment InterestCollection")
@Log4j2
public class InvestmentInterestCollectionController {

	@Autowired
	InvestmentInterestCollectionService investmentInterestCollectionService;

	@RequestMapping(value = "/get/{id}/{notificationId}", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getById(@PathVariable Long id,@PathVariable Long notificationId) {
		log.info("InvestmentClosingController:get() [" + id + "]");
		BaseDTO baseDTO = investmentInterestCollectionService.getById(id,notificationId);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> createInvestmentClosing(
			@RequestBody InvestmentInterestCollectionDTO investmentInterestCollectionDTO) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = investmentInterestCollectionService
				.createInvestmentInterestCollection(investmentInterestCollectionDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/getLazylist", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<BaseDTO> getInvestmentInterestCollectionList(@RequestBody PaginationDTO paginationDTO) {
		log.info("<===== Start StaffMonthlyConveyanceController.getStaffMonthlyConveyanceList ======>");
		BaseDTO baseDTO = investmentInterestCollectionService.searchLazyList(paginationDTO);
		log.info("<===== Start StaffMonthlyConveyanceController.getStaffMonthlyConveyanceList ======>");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/approve", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> approve(
			@RequestBody InvestmentInterestCollectionDTO investmentInterestCollectionDTO) {
		log.info("<===== Start StaffMonthlyConveyanceController.staffConveyanceApprove ======>");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = investmentInterestCollectionService.approve(investmentInterestCollectionDTO);
		log.info("<===== End StaffMonthlyConveyanceController.staffConveyanceApprove ======>");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	
	@RequestMapping(value = "/getpaymentmethod", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> loadPaymentMethod() {
		log.info("<--- Starts PaymentController .loadPaymentMethod() ---> ");
		BaseDTO baseDTO = investmentInterestCollectionService.loadPaymentMethod();
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	} 
	
	@RequestMapping(value = "/getsomepaymentmethod", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getsomepaymentmethod() {
		log.info("<--- Starts PaymentController .loadPaymentMethod() ---> ");
		BaseDTO baseDTO = investmentInterestCollectionService.getsomepaymentmethod();
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
}