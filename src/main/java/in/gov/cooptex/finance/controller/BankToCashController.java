package in.gov.cooptex.finance.controller;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.BankToBankResponseDTO;
import in.gov.cooptex.finance.BankToCashResponseDTO;
import in.gov.cooptex.finance.BankToCashSaveRequestDTO;
import in.gov.cooptex.finance.BanktoBankAddDTO;
import in.gov.cooptex.finance.BanktoCashAddDTO;
import in.gov.cooptex.finance.service.BankToCashService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/banktocash")
@Log4j2
public class BankToCashController {

    @Autowired
    BankToCashService bankToCashService;

    @RequestMapping(value = "/getall", method = RequestMethod.POST)
    public @ResponseBody
    BaseDTO getBankToCashDetails(@RequestBody PaginationDTO paginationDTO) {
    	log.info("<===== Start BankToCashController.getAll BankToCashList ======>");
        BaseDTO wrapperDto = null;
        try {
            return wrapperDto= bankToCashService.getBankToCashDetails(paginationDTO);
        } catch (Exception ex) {
            log.info("Bank to Cash Transfer Controller ex:" + ex);
        }
        return wrapperDto;
    }
    
    
    @RequestMapping(value = "/getdetailsbyid/{id}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<BaseDTO> getBankToCashDetailsById(@PathVariable("id") Long id) {
        BaseDTO wrapperDto=bankToCashService.getDetailsById(id);
        return new ResponseEntity<BaseDTO>(wrapperDto, HttpStatus.OK );

    }
    
    @RequestMapping(value = "/getEdit/{id}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<BaseDTO> getEditBankToCashById(@PathVariable("id") Long id) {
        BaseDTO wrapperDto=bankToCashService.getBankToCashEditById(id);
        return new ResponseEntity<BaseDTO>(wrapperDto, HttpStatus.OK );
    }
    
   
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public @ResponseBody
    BaseDTO save(@RequestBody BankToCashSaveRequestDTO requestDTO) {
    	log.info("Enter to BankToCash Controller");
        BaseDTO wrapperDto = null;
        try {
            return wrapperDto= bankToCashService.save(requestDTO);
        } catch (Exception ex) {
            log.info("Bank to Cash Transfer Controller ex:" + ex);
        }
        return wrapperDto;
    }
    
    @RequestMapping(method = RequestMethod.POST, value = "/savebanktoCash")
	public BaseDTO addBankToCash(@RequestBody BanktoCashAddDTO data) {
        log.info("<--Starts BankToCashController Saveing -->"+data);
		return bankToCashService.addBankToCash(data);
	}
    
    
    @RequestMapping(method = RequestMethod.POST, value = "/banktoCashEditedStore")
   	public BaseDTO BankToCashEditedStore(@RequestBody BanktoCashAddDTO data) {
           log.info("<--Starts BankToCashController EditedStore -->"+data);
   		return bankToCashService.addBankToCash(data);
   	}
	
    
	@RequestMapping(value = "/getallemployee", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getAllEmployee() {
		BaseDTO	baseDTO=bankToCashService.getAllEmployee();
		log.info("<---Ends EmployeeController.getAllEmployee()--->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/approveBankToCash", method = RequestMethod.POST)
	public @ResponseBody BaseDTO approveBankToCash(@RequestBody BankToCashResponseDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			baseDTO = bankToCashService.approveBankToCash(requestDTO);
		} catch (Exception ex) {
			log.info("Cheque to Bank Transfer Controller ex:" + ex);
		}
		return baseDTO;
	}
	
	@RequestMapping(value = "/rejectBankToCash", method = RequestMethod.POST)
	public @ResponseBody BaseDTO rejectChequeToCash(@RequestBody BankToCashResponseDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			baseDTO = bankToCashService.rejectBankToCash(requestDTO);
		} catch (Exception ex) {
			log.info("Cheque to Bank Transfer Controller ex:" + ex);
		}
		return baseDTO;
	}
}


