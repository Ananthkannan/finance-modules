package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.accounts.model.RebateClaim;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.dto.ClaimStatementDto;
import in.gov.cooptex.finance.service.RebateClaimService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/rebateclaim")
@Api(tags = "Rebate Claim", value = "Rebate Claim")
@Log4j2
public class RebateClaimController {

	@Autowired
	RebateClaimService rebateClaimService;

	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getById(@PathVariable Long id) {
		log.info("RebateClaimController:get() [" + id + "]");
		BaseDTO baseDTO = rebateClaimService.getById(id);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/getActiveRebateClaim", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getActiveRebateClaim() {
		log.info("RebateClaimController:getActiveRebateClaim()");
		BaseDTO baseDTO = rebateClaimService.getActiveRebateClaim();
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> createRebateClaim(@RequestBody RebateClaim rebateClaim) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = rebateClaimService.createRebateClaim(rebateClaim);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<BaseDTO> saveRebateClaimData(@RequestBody ClaimStatementDto claimStatementDto) {
		log.info(":: RebateClaimController - save Rebate Claim Data ::");
		BaseDTO baseDTO = rebateClaimService.saveRebateClaimData(claimStatementDto);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	/*This Controller method is used to get data for list and search*/
	@RequestMapping(value = "/getrebatedetails", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<BaseDTO> getRebateDetails(@RequestBody PaginationDTO paginationDTO) {
		log.info("::Start RebateClaimController - Search And Load LazyList ::");
		BaseDTO baseDTO = rebateClaimService.getRebateDetails(paginationDTO);
		log.info(":: End RebateClaimController - Search And Load LazyList ::");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	/*This Controller method is used to get data for list and search*/
	@RequestMapping(value = "/viewstatus", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<BaseDTO> showViewPage(@RequestBody ClaimStatementDto claimStatementDto) {
		log.info("::Start RebateClaimController - Search And Load LazyList ::");
		BaseDTO baseDTO = rebateClaimService.showViewPage(claimStatementDto);
		log.info(":: End RebateClaimController - Search And Load LazyList ::");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/savenotelog", method = RequestMethod.POST)
	@ResponseBody
	public void saveNotAndLog(@RequestBody ClaimStatementDto claimStatementDto) {
		log.info(":: RebateClaimController - save Rebate Claim Data ::");
		rebateClaimService.saveRebateClaimNoteAndLog(claimStatementDto.getRebateClaim(),claimStatementDto);
		log.info(":: RebateClaimController - save Rebate Claim Data ::");
	}
	
	@RequestMapping(value = "/approveOrReject", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<BaseDTO> approveOrRejectClaimStatement(@RequestBody ClaimStatementDto claimStatementDto) {
		log.info(":: RebateClaimController - save Rebate Claim Data ::");
		BaseDTO baseDTO =rebateClaimService.approvedRejectClaimStatement(claimStatementDto.getRebateClaim(),claimStatementDto);
		log.info(":: RebateClaimController - save Rebate Claim Data ::");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	
	
	
}