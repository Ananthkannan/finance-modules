package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.IntensiveInspectionTestStockDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.dto.IntensiveInspectionAuditRequestDto;
import in.gov.cooptex.finance.dto.IntensiveInspectionBudgetDTO;
import in.gov.cooptex.finance.dto.IntensiveInspectionCreditSalesDTO;
import in.gov.cooptex.finance.dto.StockDetailsDto;
import in.gov.cooptex.finance.dto.StockDetailsRequestDetails;
import in.gov.cooptex.finance.service.IntensiveInspectionReportService;
import in.gov.cooptex.operation.model.IntensiveInspectionAudit;
import in.gov.cooptex.operation.model.IntensiveInspectionCreditDetails;
import in.gov.cooptex.operation.model.IntensiveInspectionGeneralDetails;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/intensiveInspection")
@Log4j2
public class IntensiveInspectionReportController {

	@Autowired
	IntensiveInspectionReportService intensiveInspectionReportService;

	@RequestMapping(method = RequestMethod.PUT, path = "/saveorupdateintensiveinspectionAudit")
	public ResponseEntity<BaseDTO> saveOrUpdateIntensiveInspection(
			@RequestBody IntensiveInspectionAudit intensiveInspectionAudit) {
		log.info("IntensiveInspectionReportController.saveOrUpdateIntensiveInspection() - START");
		BaseDTO baseDto = new BaseDTO();
		baseDto = intensiveInspectionReportService.saveOrUpdateIntensiveInspectionAudit(intensiveInspectionAudit);
		log.info("IntensiveInspectionReportController.saveOrUpdateIntensiveInspection() - END");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}

	@RequestMapping(value = "/getFinanceYear", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getFinanceYear() {
		log.info("IntensiveInspectionReportController.getFinanceYear() - START");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = intensiveInspectionReportService.getFinanceYear();
		log.info("IntensiveInspectionReportController.getFinanceYear() - END");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getemployeebyentityId/{entityId}/{auditId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getEmployeeListByEntityId(@PathVariable("entityId") Long entityId, @PathVariable("auditId") Long auditId) {
		log.info("IntensiveInspectionReportController.getEmployeeListByEntityId() - START");
		BaseDTO baseDto = intensiveInspectionReportService.getEmployeeListByEntityId(entityId, auditId);
		log.info("IntensiveInspectionReportController.getEmployeeListByEntityId() - END");
		if (baseDto != null) {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/deleteintensiveemployee/{auditEmployeeId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> deleteIntensiveEmployeeId(@PathVariable("auditEmployeeId") Long auditEmployeeId) {
		log.info("IntensiveInspectionReportController.deleteIntensiveEmployeeId() - START");
		BaseDTO baseDto = intensiveInspectionReportService.deleteIntensiveEmployeeId(auditEmployeeId);
		log.info("IntensiveInspectionReportController.deleteIntensiveEmployeeId() - END");
		if (baseDto != null) {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/saveorupdateintensiveinspectionEmployee")
	public ResponseEntity<BaseDTO> saveOrUpdateAuditSales(@RequestBody IntensiveInspectionAuditRequestDto intensiveInspectionAuditRequestDto) {
		log.info("IntensiveInspectionReportController.saveOrUpdateAuditSales() - START");
		BaseDTO baseDto = new BaseDTO();
		baseDto = intensiveInspectionReportService.saveOrUpdateIntensiveInspectionEmployee(intensiveInspectionAuditRequestDto);
		log.info("IntensiveInspectionReportController.saveOrUpdateAuditSales() - END");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, path = "/saveorupdateintensiveinspectionclosing")
	public ResponseEntity<BaseDTO> saveOrUpdateIntensiveInspectionClosing(
			@RequestBody IntensiveInspectionAuditRequestDto intensiveInspectionAuditRequestDto) {
		log.info("=======START IntensiveInspectionReportController.saveOrUpdateIntensiveInspection=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = intensiveInspectionReportService.saveOrUpdateIntensiveInspectionClosings(intensiveInspectionAuditRequestDto);
		log.info("=======END IntensiveInspectionReportController.saveOrUpdate=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/saveorupdateintensivegeneraldetails")
	public ResponseEntity<BaseDTO> saveOrUpdateIntensiveGeneralDetails(
			@RequestBody IntensiveInspectionGeneralDetails intensiveInspectionGeneralDetails) {
		log.info("IntensiveInspectionReportController.saveOrUpdateIntensiveGeneralDetails() - START");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = intensiveInspectionReportService.saveOrUpdateIntensiveGeneralDetails(intensiveInspectionGeneralDetails);
		log.info("IntensiveInspectionReportController.saveOrUpdateIntensiveGeneralDetails() - END");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/saveorupdatecreditdetails")
	public ResponseEntity<BaseDTO> saveOrUpdateIntensiveCreditDetails(
			@RequestBody IntensiveInspectionCreditDetails intensiveInspectionCreditDetails) {
		log.info("IntensiveInspectionReportController.saveOrUpdateIntensiveCreditDetails() - START");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = intensiveInspectionReportService.saveOrUpdateIntensiveCreditDetails(intensiveInspectionCreditDetails);
		log.info("IntensiveInspectionReportController.saveOrUpdateIntensiveCreditDetails() - END");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getlazyload", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<BaseDTO> getLazyLoadData(@RequestBody PaginationDTO paginationDTO) {
		log.info("IntensiveInspectionReportController.getLazyLoadData() - START");
		BaseDTO baseDTO = intensiveInspectionReportService.getLazyLoadData(paginationDTO);
		log.info("IntensiveInspectionReportController.getLazyLoadData() - END");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/savebudgetdetails")
	public ResponseEntity<BaseDTO> saveOrUpdateBudgetDetails(
			@RequestBody IntensiveInspectionBudgetDTO intensiveInspectionBudgetDTO) {
		log.info("IntensiveInspectionReportController.saveOrUpdateBudgetDetails() - START");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = intensiveInspectionReportService.saveOrUpdateBudgetDetails(intensiveInspectionBudgetDTO);
		log.info("IntensiveInspectionReportController.saveOrUpdateBudgetDetails() - END");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/saveteststockdetails")
	public ResponseEntity<BaseDTO> saveOrUpdateTestStockDetails(
			@RequestBody IntensiveInspectionTestStockDTO intensiveInspectionTestStockDTO) {
		log.info("IntensiveInspectionReportController.saveOrUpdateTestStockDetails() - START");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = intensiveInspectionReportService.saveOrUpdateTestStockDetails(intensiveInspectionTestStockDTO);
		log.info("IntensiveInspectionReportController.saveOrUpdateTestStockDetails() - END");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getclosingData/{showroomId}/{startYear}/{endYear}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getClosingData(@PathVariable("showroomId") Long showroomId,
			@PathVariable("startYear") Long startYear, @PathVariable("endYear") Long endYear) {
		log.info("IntensiveInspectionReportController.getClosingData() - START");
		BaseDTO baseDto = intensiveInspectionReportService.getIntensiveInspectionClosingData(showroomId, startYear,
				endYear);
		log.info("IntensiveInspectionReportController.getClosingData() - END");
		if (baseDto != null) {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getById/{auditId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getById(@PathVariable("auditId") Long auditId) {
		log.info("IntensiveInspectionReportController.getById() - START");
		BaseDTO baseDto = intensiveInspectionReportService.getById(auditId);
		log.info("IntensiveInspectionReportController.getById() - END");
		if (baseDto != null) {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(method = RequestMethod.PUT, path = "/saveStockValues")
	public ResponseEntity<BaseDTO> saveOrUpdateIntensiveInspectionStock( @RequestBody StockDetailsRequestDetails stockDetailsRequestDetails) {
		log.info("=======START IntensiveInspectionReportController.saveOrUpdateIntensiveInspectionStock=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = intensiveInspectionReportService.saveOrUpdateIntensiveInspectionStock(stockDetailsRequestDetails);
		log.info("=======END IntensiveInspectionReportController.saveOrUpdateIntensiveInspectionStock=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}

	@RequestMapping(value = "/getallactiveshowrooms/{query}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getAllActiveShowrooms(@PathVariable("query") String query) {
		log.info("IntensiveInspectionReportController. getAllActiveShowrooms() - START");
		BaseDTO baseDto = intensiveInspectionReportService.getAllActiveShowrooms(query);
		log.info("IntensiveInspectionReportController. getAllActiveShowrooms() - END");
		if (baseDto != null) {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getLastInspectionDateByShowroomId/{showroomId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getLastInspectionDateByShowroomId(@PathVariable("showroomId") Long showroomId) {
		log.info("IntensiveInspectionReportController. getLastInspectionDateByShowroomId() - START");
		BaseDTO baseDto = intensiveInspectionReportService.getLastInspectionDateByShowroomId(showroomId);
		log.info("IntensiveInspectionReportController. getLastInspectionDateByShowroomId() - END");
		if (baseDto != null) {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getbudgetreviewData/{showroomId}/{startYear}/{endYear}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getBudgetReviewData(@PathVariable("showroomId") Long showroomId,
			@PathVariable("startYear") Long startYear, @PathVariable("endYear") Long endYear) {
		log.info("IntensiveInspectionReportController. getBudgetReviewData() - START");
		BaseDTO baseDto = intensiveInspectionReportService.getBudgetReviewData(showroomId, startYear, endYear);
		log.info("IntensiveInspectionReportController. getBudgetReviewData() - END");
		if (baseDto != null) {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getcreditSalesData", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> getCreditSalesData(
			@RequestBody IntensiveInspectionCreditSalesDTO intensiveInspectionCreditSalesDTO) {
		log.info("IntensiveInspectionReportController. getCreditSalesData() - START");
		BaseDTO baseDto = intensiveInspectionReportService.getCreditSalesData(intensiveInspectionCreditSalesDTO);
		log.info("IntensiveInspectionReportController. getCreditSalesData() - END");
		if (baseDto != null) {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getActiveProductVarietyForAutoComplete/{productCodeOrName}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getActiveProductVarietyForAutoComplete(@PathVariable("productCodeOrName") String productCodeOrName) {
		log.info("IntensiveInspectionReportController. getActiveProductVarietyForAutoComplete() - START");
		BaseDTO baseDto = intensiveInspectionReportService.getActiveProductVarietyForAutoComplete(productCodeOrName);
		log.info("IntensiveInspectionReportController. getActiveProductVarietyForAutoComplete() - END");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getBookedQtyValue/{auditId}/{productId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getBookedQtyValue(@PathVariable("auditId") Long auditId, @PathVariable("productId") Long productId) {
		log.info("IntensiveInspectionReportController. getBookedQtyValue() - START");
		BaseDTO baseDto = intensiveInspectionReportService.getBookedQtyValue(auditId, productId);
		log.info("IntensiveInspectionReportController. getBookedQtyValue() - END");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getStockVerificationDateValue", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> getStockVerificationDateValue(
			@RequestBody StockDetailsDto stockDetailsDto) {
		log.info("IntensiveInspectionReportController. getStockVerificationDateValue() - START");
		BaseDTO baseDto = intensiveInspectionReportService.getStockVerificationDateValue(stockDetailsDto);
		log.info("IntensiveInspectionReportController. getStockVerificationDateValue() - END");
		if (baseDto != null) {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getIntensiveStockData/{showroomId}/{inspectionDateStr}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getAgedStockData(@PathVariable("showroomId") Long showroomId,
			@PathVariable("inspectionDateStr") String inspectionDateStr) {
		log.info("IntensiveInspectionReportController. getIntensiveStockData() - START");
		BaseDTO baseDto = intensiveInspectionReportService.getIntensiveStockData(showroomId, inspectionDateStr);
		log.info("IntensiveInspectionReportController. getIntensiveStockData() - END");
		if (baseDto != null) {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.NO_CONTENT);
		}
	}
	
}
