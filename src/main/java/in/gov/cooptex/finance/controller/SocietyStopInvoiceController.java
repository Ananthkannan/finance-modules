package in.gov.cooptex.finance.controller;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.IncrementForwardDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.SocietyStopInvoiceDTO;
import in.gov.cooptex.core.dto.StopSocietyInvoiceDTO;
import in.gov.cooptex.dto.pos.SocietyReleaseInvoiceDTO;
import in.gov.cooptex.finance.InvoiceDetailsRequestDTO;
import in.gov.cooptex.finance.service.SocietyStopInvoiceService;
import lombok.extern.log4j.Log4j2;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
@RequestMapping("/societystopinvoice")
public class SocietyStopInvoiceController {

    @Autowired
    private SocietyStopInvoiceService societystopinvoice;

    /**
     * Return the Stopped purchase Invoices
     * @param paginationRequest
     * @return BaseDTO
     */
    @RequestMapping(value = "/getallSocietyAdjustmentlistlazy", method = RequestMethod.POST)
    public ResponseEntity<BaseDTO> loadLazySocietyStopInvoice(@RequestBody PaginationDTO paginationRequest) {
        log.info("<------ Starts societystopinvoice.loadLazySocietyStopInvoice------->" + paginationRequest);
        BaseDTO baseDto = societystopinvoice.loadLazySocietyStopInvoice(paginationRequest);
        log.info("<------ Ended societystopinvoice.loadLazySocietyStopInvoice------->" + paginationRequest);
        return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
    }

    /**
     * Create the Stop purchase Invoice
     * @param societyStopInvoiceDTO
     * @return BaseDTO
     */
    @RequestMapping(value = "/createstopinvoice", method = RequestMethod.POST)
    public ResponseEntity<BaseDTO> createPurchaseStopInvoice(@RequestBody SocietyStopInvoiceDTO societyStopInvoiceDTO) {
        log.info("<---------Starts societystopinvoice.createPurchaseStopInvoice ---------->" + societyStopInvoiceDTO);
        BaseDTO baseDto = societystopinvoice.createPurchaseStopInvoice(societyStopInvoiceDTO);
        log.info("<---------Ended societystopinvoice.createPurchaseStopInvoice ---------->" + societyStopInvoiceDTO);
        return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
    }

    /**
     * Search the purchase invoice details based on given search inputs
     * @param invoiceDetailsRequestDTO
     * @return BaseDTO
     */
    @RequestMapping(value = "/searchsocietypurchaseinvoice", method = RequestMethod.POST)
    public ResponseEntity<BaseDTO> searchSocietyPurchaseInvoice(@RequestBody InvoiceDetailsRequestDTO invoiceDetailsRequestDTO)
    {
        log.info( "<-----------Starts societystopinvoice.searchPurchaseInvoiceDetails------>" + invoiceDetailsRequestDTO );
        BaseDTO baseDTO = societystopinvoice.searchPurchaseInvoiceDetails( invoiceDetailsRequestDTO );
        log.info( "<-----------Ended societystopinvoice.searchPurchaseInvoiceDetails------>" + invoiceDetailsRequestDTO );
        return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);

    }
    
    @RequestMapping(value = "/delete/id/{id}", method = RequestMethod.DELETE)
	public BaseDTO deleteStopSociety(@PathVariable Long id) {
		log.info("<--- Starts deleteStopSociety--->");
		BaseDTO	baseDTO = societystopinvoice.deleteStopSociety(id);
		return baseDTO;
	}
    
    @RequestMapping(value = "/updatesocietystoplog", method = RequestMethod.PUT)
	public ResponseEntity<BaseDTO> updateSocietyStopLog(@RequestBody StopSocietyInvoiceDTO stopSocietyInvoiceDTO, HttpServletRequest request) {
		log.info("<<=========== IncrementController-----societystopinvoice ========STARTS ");
		BaseDTO	baseDTO = societystopinvoice.updateSocietyStopLog(stopSocietyInvoiceDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	} 
    
    @RequestMapping(value = "/viewreleasepurchaseinvoicedetails", method = RequestMethod.POST)
    public ResponseEntity<BaseDTO> viewReleasePurchaseInvoiceDetails(@RequestBody SocietyReleaseInvoiceDTO societyReleaseInvoiceDTO)
    {
        log.info( "<---------Starts societystopinvoice.viewReleasePurchaseInvoiceDetails----->" );
        BaseDTO baseDTO = societystopinvoice.viewReleasePurchaseInvoiceDetails( societyReleaseInvoiceDTO );
        log.info( "<---------Ended societystopinvoice.viewReleasePurchaseInvoiceDetails----->" );
        return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);

    }
    
}

