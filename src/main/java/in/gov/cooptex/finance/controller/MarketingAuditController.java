package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.accounts.model.MarketingAuditEntity;
import in.gov.cooptex.core.accounts.model.MarketingAuditOthers;
import in.gov.cooptex.core.accounts.model.MarketingAuditStockArrangement;
import in.gov.cooptex.core.accounts.model.MarketingInspectionEntity;
import in.gov.cooptex.core.accounts.model.MarketingInspectionInventory;
import in.gov.cooptex.core.accounts.model.MarketingInspectionModernization;
import in.gov.cooptex.core.accounts.model.MarketingInspectionProfitability;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.dto.MarketingAuditRequestDto;
import in.gov.cooptex.finance.service.MarketingAuditService;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/marketingAudit")
@Log4j2
public class MarketingAuditController {
	
	@Autowired
	MarketingAuditService marketingAuditService;
	
	/** This method search the employee with the given input */
	@RequestMapping(value = "/getAutoCompleteEmployee/{employeeSearchName}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getEmployeeByEmpName(@PathVariable String employeeSearchName) {
		log.info("<--- Starts MarketingAuditController.getEmployeeByEmpName() --->" + employeeSearchName);
		BaseDTO baseDTO = marketingAuditService.getEmployeeByAutoComplete(employeeSearchName);
		log.info("<---Ends MarketingAuditController.getEmployeeByEmpName()--->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.PUT, path = "/saveorupdateinspectiondetails")
	public ResponseEntity<BaseDTO> saveOrUpdateInspectionDetails(@RequestBody MarketingInspectionEntity marketingInspectionEntity) {
		log.info("=======START MarketingAuditController.saveOrUpdate=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = marketingAuditService.saveOrUpdateMarkettingAudit(marketingInspectionEntity);
		log.info("=======END MarketingAuditController.saveOrUpdate=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getallactiveregions", method = RequestMethod.GET)
	public BaseDTO getAllActiveRegions() {
		log.info("=======START MarketingAuditController.getAllActiveRegions=====");
		return marketingAuditService.getAllActiveRegions();
		
	}
	
	@RequestMapping(value = "/getallshowroomforregion/{regionId}", method = RequestMethod.GET)
	public BaseDTO getAllShowroomForRegion(@PathVariable("regionId") Long regionId) {
		log.info("=======START MarketingAuditController.getAllShowroomForRegion=====");
		return marketingAuditService.getAllShowroomForRegion(regionId);
		
	}
	
	
	@RequestMapping(method = RequestMethod.PUT, path = "/saveorupdateProfitability")
	public ResponseEntity<BaseDTO> saveOrUpdateProfitability(@RequestBody MarketingInspectionProfitability marketingInspectionProfitability) {
		log.info("=======START MarketingAuditController.saveOrUpdateProfitability=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = marketingAuditService.saveOrUpdateProfitability(marketingInspectionProfitability);
		log.info("=======END MarketingAuditController.saveOrUpdateProfitability=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}

	/** This method search the employee with the given input */
	@RequestMapping(value = "/getAutoCompleteProductVariety/{productName}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getProductVarietyName(@PathVariable String productName) {
		log.info("<--- Starts MarketingAuditController.getProductVarietyName() --->" + productName);
		BaseDTO baseDTO = marketingAuditService.getProductVarietyNameAutoComplete(productName);
		log.info("<---Ends MarketingAuditController.getProductVarietyName()--->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.PUT, path = "/saveorupdateInventory")
	public ResponseEntity<BaseDTO> saveorupdateInventory(@RequestBody MarketingInspectionInventory marketingInspectionInventory) {
		log.info("=======START MarketingAuditController.saveorupdateInventory=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = marketingAuditService.saveOrUpdateMarketingInspectionInventory(marketingInspectionInventory);
		log.info("=======END MarketingAuditController.saveorupdateInventory=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.PUT, path = "/saveorupdateModernization")
	public ResponseEntity<BaseDTO> saveOrUpdateModernization(@RequestBody MarketingInspectionModernization marketingInspectionModernization) {
		log.info("=======START MarketingAuditController.saveOrUpdateModernization=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = marketingAuditService.saveOrUpdateModernization(marketingInspectionModernization);
		log.info("=======END MarketingAuditController.saveOrUpdateModernization=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getAllBuildType", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getAllBuildTypeData() {
		log.info("=======START MarketingAuditController.getAllBuildTypeData=====");
		BaseDTO	baseDTO = marketingAuditService.getAllBuildType();
		log.info("=======END MarketingAuditController.getAllBuildTypeData=====");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	/*This Controller method is used to get data for list and search*/
	@RequestMapping(value = "/getlazyload", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<BaseDTO> getLazyLoadDatas(@RequestBody PaginationDTO paginationDTO) {
		log.info("=======START MarketingAuditController.getLazyLoadDatas=====");
		BaseDTO baseDTO = marketingAuditService.getLazyLoadData(paginationDTO);
		log.info("=======END MarketingAuditController.getLazyLoadDatas=====");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getById/{id}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getById(@PathVariable Long id) {
		log.info("=======START MarketingAuditController.getAllBuildTypeData=====");
		BaseDTO	baseDTO = marketingAuditService.getMarketingAuditById(id);
		log.info("=======END MarketingAuditController.getAllBuildTypeData=====");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/getAllEmplistbyShowroomId/{showroomId}", method = RequestMethod.GET)
	public BaseDTO getAllEmplistbyShowroomId(@PathVariable("showroomId") Long showroomId) {
		log.info("=======START MarketingAuditController.getAllEmplistbyShowroomId=====");
		return marketingAuditService.getAllEmplistbyShowroomId(showroomId);
		
	}

	@RequestMapping(value = "/loadAllSalesDtlsTable/{showroomId}", method = RequestMethod.GET)
	public BaseDTO loadAllSalesDtlsTable(@PathVariable("showroomId") Long showroomId) {
		log.info("=======START MarketingAuditController.loadAllSalesDtlsTable=====");
		return marketingAuditService.loadAllSalesDetailsDatatable(showroomId);
	}
	
	@RequestMapping(value = "/loadAllInventoryDtlsTable/{showroomId}", method = RequestMethod.GET)
	public BaseDTO loadAllInventoryDtlsTable(@PathVariable("showroomId") Long showroomId) {
		log.info("=======START MarketingAuditController.loadAllInventoryDtlsTable=====");
		return marketingAuditService.loadAllInventoryDtlsTable(showroomId);
	}
	
	@RequestMapping(value = "/loadProfitabilityDtlsTable/{showroomId}", method = RequestMethod.GET)
	public BaseDTO loadProfitabilityDtlsTable(@PathVariable("showroomId") Long showroomId) {
		log.info("=======START MarketingAuditController.loadProfitabilityDtlsTable=====");
		return marketingAuditService.loadProfitabilityDtlsTable(showroomId);
	}
}
