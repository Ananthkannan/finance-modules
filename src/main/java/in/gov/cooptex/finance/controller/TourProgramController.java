package in.gov.cooptex.finance.controller;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.BankToCashResponseDTO;
import in.gov.cooptex.finance.BankToCashSaveRequestDTO;
import in.gov.cooptex.finance.BanktoCashAddDTO;
import in.gov.cooptex.finance.TourProgramResponseDTO;
import in.gov.cooptex.finance.TourProgramSaveDto;
import in.gov.cooptex.finance.service.BankToCashService;
import in.gov.cooptex.finance.service.TourProgramService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/tourprogram")
@Log4j2
public class TourProgramController {

	@Autowired
	BankToCashService bankToCashService;

	@Autowired
	TourProgramService tourProgramService;

	/**
	 * @param paginationDTO
	 * @return
	 */
	@RequestMapping(value = "/getall", method = RequestMethod.POST)
	public @ResponseBody BaseDTO getTourProgramDetails(@RequestBody PaginationDTO paginationDTO) {
		log.info("<===== Start TourProgramController.getAll TourProgramList ======>");
		BaseDTO wrapperDto = null;
		try {
			return wrapperDto = tourProgramService.getTourProgramDetails(paginationDTO);
		} catch (Exception ex) {
			log.info("TourProgram Controller exception while getAll ex:" + ex);
		}
		return wrapperDto;
	}

	/**
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/getEdit/{id}/{notificationId}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> getEditTourById(@PathVariable("id") Long id,
			@PathVariable("notificationId") Long notificationId) {
		log.info("<===== Start TourProgramController.getEdit TourProgramList ======>");
		BaseDTO wrapperDto = tourProgramService.getTourEditById(id, notificationId);
		return new ResponseEntity<BaseDTO>(wrapperDto, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/savetourprogram")
	public BaseDTO savetourprogram(@RequestBody TourProgramSaveDto data) {
		log.info("<--Starts TourProgramController Saveing -->" + data);
		return tourProgramService.saveTourProgramDetails(data);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/savetourdeeviation")
	public BaseDTO saveDeeviationTourProgram(@RequestBody TourProgramSaveDto data) {
		log.info("<--Starts TourProgramController Saveing -->" + data);
		return tourProgramService.saveDeeviationTourProgramDetails(data);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/banktoCashEditedStore")
	public BaseDTO BankToCashEditedStore(@RequestBody BanktoCashAddDTO data) {
		log.info("<--Starts BankToCashController EditedStore -->" + data);
		return bankToCashService.addBankToCash(data);
	}

	@RequestMapping(value = "/getallemployee", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getAllEmployee() {
		BaseDTO baseDTO = tourProgramService.getAllEmployee();
		log.info("<---Ends EmployeeController.getAllEmployee()--->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/approveTourProgram", method = RequestMethod.POST)
	public @ResponseBody BaseDTO approveBankToCash(@RequestBody TourProgramResponseDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			baseDTO = tourProgramService.approveTourProgram(requestDTO);
		} catch (Exception ex) {
			log.info("Cheque to Bank Transfer Controller ex:" + ex);
		}
		return baseDTO;
	}

	@RequestMapping(value = "/rejectTourProgram", method = RequestMethod.POST)
	public @ResponseBody BaseDTO rejectChequeToCash(@RequestBody TourProgramResponseDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			baseDTO = tourProgramService.rejectTourProgram(requestDTO);
		} catch (Exception ex) {
			log.info("Cheque to Bank Transfer Controller ex:" + ex);
		}
		return baseDTO;
	}
}
