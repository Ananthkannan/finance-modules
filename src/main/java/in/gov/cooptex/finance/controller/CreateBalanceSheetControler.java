package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.accounts.dto.CreateBalanceSheetDto;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.service.CreateBalanceSheetService;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/createBalanceSheet")
@Log4j2
public class CreateBalanceSheetControler {
	@Autowired
	CreateBalanceSheetService createBalanceSheetService;
	
	@RequestMapping(value = "/getDetailsForCreateBalanceSheet", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<BaseDTO> getDetailsForCreateBalanceSheet(@RequestBody CreateBalanceSheetDto createBalanceSheetDto) {
		log.info(":: Controller - Search CreateBalanceSheetDto ::");
		BaseDTO baseDTO = createBalanceSheetService.getDetailsForCreateBalanceSheetService(createBalanceSheetDto);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}

}
