package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.dto.ClaimStatementRequestDto;
import in.gov.cooptex.finance.service.ClaimStatementService;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/claimstatement")
@Log4j2
public class ClaimStatementController {
	
	@Autowired
	ClaimStatementService claimStatementService;
	
	    @RequestMapping(value = "/getclaimdata", method = RequestMethod.POST)
	    public @ResponseBody
	    BaseDTO getClaimData(@RequestBody ClaimStatementRequestDto claimStatementRequestDto) {
		 
		    log.info("Start ClaimStatementController :" + claimStatementRequestDto);
	        BaseDTO baseDTO = claimStatementService.getClaimData(claimStatementRequestDto);
	        log.info("End ClaimStatementController baseDTO:" + baseDTO);
	        return baseDTO;
	    }

}
