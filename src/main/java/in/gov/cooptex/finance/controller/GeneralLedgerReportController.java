package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.finance.service.GeneralLedgerReportService;
import in.gov.cooptex.mis.dto.EmpRetirmentDto;
import in.gov.cooptex.mis.dto.GeneralLedgerRequestDto;
import in.gov.cooptex.mis.dto.ProductCategoryWiseReportDTO;

import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping("${finance.api.url}/generallederreport")
public class GeneralLedgerReportController {

	@Autowired
	GeneralLedgerReportService generalLedgerReportService;

	@RequestMapping(value = "/getAllCategoryList", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getAllCategoryList() {
		log.info("getAllCategoryList() ");
		BaseDTO baseDto = generalLedgerReportService.getAllCategoryList();
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}

	@RequestMapping(value = "/searchPCWPP", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<BaseDTO> getProductCategoryWiseReport(@RequestBody ProductCategoryWiseReportDTO request) {
		log.info("======================= goProductCategoryWiseReport() ===============================");
		BaseDTO baseDTO = generalLedgerReportService.getProductCategoryWiseReport(request);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/searchMWPP", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<BaseDTO> getMonthWiseReport(@RequestBody ProductCategoryWiseReportDTO request) {
		log.info("======================= getMonthWiseReport() ===============================");
		BaseDTO baseDTO = generalLedgerReportService.getMonthWiseReport(request);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}

	/* Employee Retirment List */
	@RequestMapping(value = "/empretirmentreport", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> getEmployeeRetairmentReport(@RequestBody EmpRetirmentDto paginationDto) {
		BaseDTO baseDTO = generalLedgerReportService.getEmployeeRetairmentReport("ERR", paginationDto);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}

	/* Employee Retirment Download Report */
	@RequestMapping(value = "/downloadempretirment", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> getEmployeeRetairmentDownloadReport(@RequestBody EmpRetirmentDto paginationDto) {
		log.info("<-- getEmployeeRetairmentDownloadReport started -->");
		BaseDTO baseDTO = generalLedgerReportService.getEmployeeRetairmentDownloadReport(paginationDto);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}

	/* General Ledger report */
	@RequestMapping(value = "/generalledgerreport", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> getGeneralLedgerReport(@RequestBody GeneralLedgerRequestDto paginationDto) {
		log.info("<-- getGeneralLedgerReport started -->");
		BaseDTO baseDTO = generalLedgerReportService.getGeneralLedgerReport("GLR", paginationDto);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}

	/* Employee Retirment Download Report */
	@RequestMapping(value = "/downloadgeneralledgerreport", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> getGeneralLedgerDownloadReport(@RequestBody GeneralLedgerRequestDto paginationDto) {
		log.info("<-- getGeneralLedgerDownloadReport started -->");
		BaseDTO baseDTO = generalLedgerReportService.getGeneralLedgerDownloadReport(paginationDto);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
}
