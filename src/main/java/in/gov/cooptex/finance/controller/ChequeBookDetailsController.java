package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.ChequeBookDetailsDTO;
import in.gov.cooptex.finance.service.ChequeBookDetailsService;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/chequebookdetails")
@Log4j2
public class ChequeBookDetailsController {
	@Autowired
	ChequeBookDetailsService chequeBookDetailsService;

	@RequestMapping(value = "/getEntity", method = RequestMethod.GET)
	public BaseDTO getEntity() {
		log.info("inside controller getEntity method----------------->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			baseDTO = chequeBookDetailsService.getEntity();
		} catch (Exception e) {
			log.info("<<==  Exception occured in getEntity ==>>",e);
			
		}
		return baseDTO;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> createChequeBookDetails(@RequestBody ChequeBookDetailsDTO chequeBookDetailsDTO) {
		log.info("inside controller createChequeBookDetails method----------------->");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = chequeBookDetailsService.createChequeBookDetails(chequeBookDetailsDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getChequeBookDetailsById(@PathVariable("id") Long id) {
		log.info("inside controller getChequeBookDetailsById method----------------->");
		BaseDTO baseDTO = chequeBookDetailsService.getChequeBookDetailsById(id);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/getChequeBookDetailsByChequeBookNumber/{chequeBookNumber}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getChequeBookDetailsByChequeBookNumber(@PathVariable("chequeBookNumber") String chequeBookNumber) {
		log.info("inside controller getChequeBookDetailsByChequeBookNumber method----------------->");
		BaseDTO baseDTO = chequeBookDetailsService.getChequeBookDetailsByChequeBookNumber(chequeBookNumber);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> editChequeBook(@RequestBody ChequeBookDetailsDTO chequeBookDetailsDTO) {
		log.info("inside controller editChequeBook method----------------->");
		BaseDTO baseDTO = chequeBookDetailsService.updateChequeBookDetails(chequeBookDetailsDTO);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/searchDataLazy", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> lazySearch(@RequestBody PaginationDTO paginationDto) {
		log.info("inside controller lazySearch method----------------->");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = chequeBookDetailsService.getChequeBookLazy(paginationDto);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

}
