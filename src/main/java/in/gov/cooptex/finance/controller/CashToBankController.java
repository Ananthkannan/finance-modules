package in.gov.cooptex.finance.controller;

import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.TenderDTO;
import in.gov.cooptex.finance.CashToBankAddRequestDTO;
import in.gov.cooptex.finance.CashtoBankRequestDTO;
import in.gov.cooptex.finance.CashtoBankResponseDTO;
import in.gov.cooptex.finance.CashtoBankSearchRequestDTO;
import in.gov.cooptex.finance.service.CashToBankServices;
import in.gov.cooptex.operation.tender.model.Tender;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import in.gov.cooptex.core.accounts.model.AmountTransferDetails;
import in.gov.cooptex.finance.service.AmountTransferDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import in.gov.cooptex.core.dto.BaseDTO;
import lombok.extern.log4j.Log4j2;

import java.util.List;

@RestController
@RequestMapping("/cashtobank")
@Log4j2
public class CashToBankController {

    @Autowired
    CashToBankServices cashToBankServices;
    @Autowired
     AmountTransferDetailsService amountTransferDetailsService;

    @RequestMapping(value = "/getall/{userID}", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<BaseDTO> getAll(@RequestBody PaginationDTO paginationDTO,@PathVariable Long userID)
    {
            log.info("<--Starts attendanceTypeController .getAll-->");
            BaseDTO baseDTO= cashToBankServices.getAllDetails(paginationDTO,userID);
            log.info("<--Ends attendanceTypeController .getAll with no. of element is -->");
            return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);

    }
    
    @RequestMapping(value = "/searchcashtobankdata", method = RequestMethod.POST)
     @ResponseBody
     public ResponseEntity<BaseDTO> searchData(@RequestBody CashtoBankRequestDTO data) {

            log.info("<--Starts attendanceTypeController .getAll--> date: "+data);
            BaseDTO baseDTO = cashToBankServices.searchData(data);
            log.info("<--Ends attendanceTypeController .getAll with no. of element is -->");
            return new ResponseEntity<BaseDTO>(baseDTO,HttpStatus.OK) ;

    }
    
    @RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> createCashTOBank(@RequestBody CashToBankAddRequestDTO data) {
		log.info("CashToBank Controller--:createCashTOBank() Start ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = cashToBankServices.createCashToBank(data);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
		
	}
    
    @RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> updateCashToBank(@RequestBody CashToBankAddRequestDTO data) {
		log.info("CashToBank Controller--:updateCashToBank() Start ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = cashToBankServices.updateCashToBank(data);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
		
	}
    
    @RequestMapping(value = "/getallemployee", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getAllEmployee() {
		BaseDTO	baseDTO=cashToBankServices.getAllEmployee();
		log.info("<---Ends EmployeeController.getAllEmployee()--->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
    
    @RequestMapping(value = "/view", method = RequestMethod.POST)
   	public ResponseEntity<BaseDTO> viewCashTOBank(@RequestBody CashtoBankResponseDTO data) {
   		log.info("CashToBank Controller--:viewCashTOBank() Start ");
   		BaseDTO baseDTO = new BaseDTO();
   		baseDTO = cashToBankServices.getViewedCashToBankDetails(data);
   		if (baseDTO != null) {
   			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
   		} else {
   			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
   		}
   		
   	}
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> deleteCashToBank(@RequestBody CashtoBankResponseDTO selectedCtoB) {
		log.info("CashToBank===>:deleteCashToBank() Method Start");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = cashToBankServices.deleteCashToBank(selectedCtoB);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
    
    @RequestMapping(value = "/approveCashToBank", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> approveCashToBank(@RequestBody CashToBankAddRequestDTO selectedCtoB) {
		log.info("CashToBank===>:approveCashToBank() Method Start");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = cashToBankServices.approveCashToBank(selectedCtoB);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
    
    @RequestMapping(value = "/rejectCashToBank", method = RequestMethod.POST)
   	public ResponseEntity<BaseDTO> rejectCashToBank(@RequestBody CashToBankAddRequestDTO selectedCtoB) {
   		log.info("CashToBank===>:rejectCashToBank() Method Start");
   		BaseDTO baseDTO = new BaseDTO();
   		baseDTO = cashToBankServices.rejectCashToBank(selectedCtoB);
   		if (baseDTO != null) {
   			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
   		} else {
   			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
   		}
   	}

    @GetMapping(value="/getEmployeeListByEntityId/{id}")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public  ResponseEntity<BaseDTO> getAllSocietyRegistrationRequest(@PathVariable Long id) {
		log.info("<--Starts societyFieldVerificationController .getAllSocietyRegistrationRequest-->");
		BaseDTO baseDTO = cashToBankServices.getEmployeeListByEnityId(id);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
    
    @RequestMapping(value = "/findempdetailsbyloggedinuser/{userId}", method = RequestMethod.GET)
	public BaseDTO findEmployeeDetailsByLoggedInUser(@PathVariable Long userId) {
		log.info("<--- Starts find Employee Details By LoggedIn User Id :- "+userId);
		BaseDTO	baseDTO = cashToBankServices.findEmployeeDetailsByLoggedInUser(userId);
		return baseDTO;
	}
    
}
