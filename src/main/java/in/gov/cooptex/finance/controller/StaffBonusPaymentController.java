package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.common.service.EntityMasterService;
import in.gov.cooptex.core.accounts.model.StaffBonusPayment;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.dto.StaffBonusPaymentDTO;
import in.gov.cooptex.finance.service.StaffBonusPaymentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("${finance.api.url}/staffbonuspayment")
@Api(tags = "Staff Bonus Payment", value = "Staff Bonus Payment")
@Log4j2
public class StaffBonusPaymentController{

	@Autowired
	StaffBonusPaymentService staffBonusPaymentService;

	@Autowired
	EntityMasterService entityMasterService;


	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS")})
	public ResponseEntity<BaseDTO> getById(@PathVariable Long id) {
		log.info("StaffBonusPaymentController:get() [" + id + "]");
		BaseDTO baseDTO = staffBonusPaymentService.getById(id);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> createStaffBonusPayment(@RequestBody StaffBonusPaymentDTO staffBonusPayment) {
		log.info("inside staffbonuspayment createStaffBonusPayment ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = staffBonusPaymentService.createStaffBonusPayment(staffBonusPayment);

		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public BaseDTO getAll() {
		log.info("<--- Starts EntityMasterController .getAll --->");
		BaseDTO baseDTO = entityMasterService.getAll();
		log.info("<---Ends EntityMasterController .getAll--->");
		return baseDTO;
	}
	
    @RequestMapping(value = "/getfinyear", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getFinYear() {
		log.info("Start StaffBonusPaymentController:getFinYear");
		BaseDTO baseDTO = staffBonusPaymentService.getFinYear();
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	@RequestMapping(value = "/getStaffBonuspaymentDetails", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> getStaffBonuspaymentDetails(@RequestBody StaffBonusPaymentDTO request) {
		log.info("StaffBonusPaymentController:getStaffBonuspaymentDetails()");
		BaseDTO baseDTO = staffBonusPaymentService.getStaffBonuspaymentDetails(request);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/searchDataLazy", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> lazySearch(@RequestBody PaginationDTO paginationDto) {
		log.info("StaffBonusPaymentController:lazySearch()");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = staffBonusPaymentService.getStaffBonusPaymentDetailsLazy(paginationDto);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getByStaffBonusPaymentdId/{id}", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS")})
	public ResponseEntity<BaseDTO> getByStaffBonusPaymentdId(@PathVariable Long id) {
		log.info("StaffBonusPaymentController:get() [" + id + "]");
		BaseDTO baseDTO = staffBonusPaymentService.getByStaffBonusPaymentId(id);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/viewStaffBonusPaymentDetailsById/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> viewStaffBonusPaymentDetailsById(@PathVariable("id") Long id) {
		log.info("inside controller viewStaffBonusPaymentDetailsById method----------------->");
		BaseDTO baseDTO = staffBonusPaymentService.viewStaffBonusPaymentDetailsById(id);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);

	}
	
	@RequestMapping(value = "/approveStaffBonusPayment", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> approveStaffBonusPayment(@RequestBody StaffBonusPaymentDTO staffBonusPaymentDTO) {
		log.info("StaffBonusPaymentController:approveStaffBonusPayment()");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = staffBonusPaymentService.approveStaffBonusPayment(staffBonusPaymentDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/rejectStaffBonusPayment", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> rejectStaffBonusPayment(@RequestBody StaffBonusPaymentDTO staffBonusPaymentDTO) {
		log.info("StaffBonusPaymentController:rejectStaffBonusPayment()");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = staffBonusPaymentService.rejectStaffBonusPayment(staffBonusPaymentDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
}