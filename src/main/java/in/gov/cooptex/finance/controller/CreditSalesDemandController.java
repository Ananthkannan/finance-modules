package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.accounts.model.CreditSalesDemand;
import in.gov.cooptex.core.accounts.model.CreditSalesRequest;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.CreditSalesDemandScheduleDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.dto.CreditSalesDemandDto;
import in.gov.cooptex.finance.dto.CreditSalesDemandRequestDto;
import in.gov.cooptex.finance.service.CreditSalesDemandService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/creditsalesdemand")
@Api(tags = "Credit Sales Demand", value = "Credit Sales Demand")
@Log4j2
public class CreditSalesDemandController {

	@Autowired
	CreditSalesDemandService creditSalesDemandService;

	@PostMapping(value = "/getallcreditsalesdemandlistlazy")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getAllCreditSalesDemandListLazy(@RequestBody PaginationDTO paginationDto) {
		log.info("<--Starts CreditSalesDemandController .getAllCreditsalesDemandListLazy-->");
		BaseDTO baseDTO = creditSalesDemandService.getAllCreditsalesDemandListLazy(paginationDto);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping(value = "/getallcreditsalesrequest/{regionId}")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getAllCreditSalesRequest(@PathVariable Long regionId) {
		log.info("<--Starts CreditSalesDemandController .getAllCreditSalesRequest-->");
		BaseDTO baseDTO = creditSalesDemandService.getAllCreditSalesRequestByRegion(regionId);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@DeleteMapping(value = "/deletebyid/{demandId}")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> deleteById(@PathVariable Long demandId) {
		log.info("<--Starts CreditSalesDemandController .deleteById-->");
		BaseDTO baseDTO = creditSalesDemandService.deleteById(demandId);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@GetMapping(value = "/getbyid/{demandId}")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getById(@PathVariable Long demandId) {
		log.info("<--Starts CreditSalesDemandController .getById-->");
		BaseDTO baseDTO = creditSalesDemandService.getByDemandId(demandId);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping(value = "/getallorganizations")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getAllOraganizationMastersByCreditSalesRequestDate(
			@RequestBody CreditSalesRequest request) {
		log.info("<--Starts CreditSalesDemandController .getAllOraganizationMastersByCreditSalesRequestDate-->");
		BaseDTO baseDTO = creditSalesDemandService.getAllOraganizationMastersByCreditSalesRequestDate(request);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping(value = "/create")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> createCreditSalesDemand(@RequestBody CreditSalesDemandRequestDto request) {
		log.info("<--Starts CreditSalesDemandController .createCreditSalesDemand-->");
		BaseDTO baseDTO = creditSalesDemandService.createCreditSalesDemand(request);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping(value = "/getdemanddetails")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getDemandDetailsList(@RequestBody CreditSalesDemandDto request) {
		log.info("<--Starts CreditSalesDemandController .getDemandDetailsList-->");
		BaseDTO baseDTO = creditSalesDemandService.getDemandDetailsList(request);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping(value = "/approve")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> approveCreditSalesDemand(@RequestBody CreditSalesDemand request) {
		log.info("<--Starts CreditSalesDemandController .approveCreditSalesDemand-->");
		BaseDTO baseDTO = creditSalesDemandService.approveCreditSalesDemand(request);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping(value = "/reject")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> rejectCreditSalesDemand(@RequestBody CreditSalesDemand request) {
		log.info("<--Starts CreditSalesDemandController .rejectCreditSalesDemand-->");
		BaseDTO baseDTO = creditSalesDemandService.rejectCreditSalesDemand(request);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping(value = "/finalapprove")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> finalApproveCreditSalesDemand(@RequestBody CreditSalesDemand request) {
		log.info("<--Starts CreditSalesDemandController .finalApproveCreditSalesDemand-->");
		BaseDTO baseDTO = creditSalesDemandService.finalApproveCreditSalesDemand(request);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	/**
	 * @param creditSalesDemandScheduleDTO
	 * @return
	 */
	@PostMapping(value = "/initiatecreditdemandschedule")
	@ApiResponses(value = { @ApiResponse(code = 0, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> initiateCreditDemandSchedule(
			@RequestBody CreditSalesDemandScheduleDTO creditSalesDemandScheduleDTO) {
		log.info("<--Starts CreditSalesDemandController.initiateCreditDemandSchedule--> "
				+ creditSalesDemandScheduleDTO);
		BaseDTO baseDTO = creditSalesDemandService.initiateCreditDemandSchedule(creditSalesDemandScheduleDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping(value = "/getdemandlist")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getDemandList(@RequestBody CreditSalesDemandDto request) {
		log.info("<--Starts CreditSalesDemandController .getDemandList-->");
		BaseDTO baseDTO = creditSalesDemandService.getDemandList(request);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping(value = "/getdemanddetailslist")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getDemandDetailsListNew(@RequestBody CreditSalesDemandDto request) {
		log.info("<--Starts CreditSalesDemandController .getDemandDetailsList-->");
		BaseDTO baseDTO = creditSalesDemandService.getDemandDetailsListNew(request);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

}