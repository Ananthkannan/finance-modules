package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.finance.dto.BankReconcilleDTO;
import in.gov.cooptex.finance.service.BankReconcilleService;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping("${finance.api.url}/accounts/bankReconcille")
public class BankReconcilleController {

	@Autowired
	BankReconcilleService bankReconcilleService;
	
	@RequestMapping(value = "/getListofVoucherTypeList", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> getListofVoucherTypeList() {
		log.info("<---BankReconcilleController() Received getListofChequeDetails  ---> ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = bankReconcilleService.getListofVoucherTypeList();
		if (baseDTO != null && baseDTO.getStatusCode()==0 ) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getListofChequeDetails/{id}/{fromDate}/{toDate}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> getListofChequeDetails(@PathVariable("id") String id, 
			@PathVariable("fromDate") String fromDate, @PathVariable("toDate") String toDate) {
		log.info("<---BankReconcilleController() Received getListofChequeDetails  ---> ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = bankReconcilleService.getListofChequeDetails(id,fromDate,toDate);
		if (baseDTO != null && baseDTO.getStatusCode()==0 ) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	@RequestMapping(value = "/updatePaymentChequeDetails", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> updatePaymentChequeDetails(@RequestBody BankReconcilleDTO paymentDetails){
		BaseDTO baseDTO = new BaseDTO();
		log.info("<---BankReconcilleController() updatePaymentChequeDetails ---> ");
		baseDTO = bankReconcilleService.updatePaymentChequeDetails(paymentDetails);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
//	@RequestMapping(value = "/searchDataLazy", method = RequestMethod.POST)
//	public ResponseEntity<BaseDTO> lazySearch(@RequestBody BankReconcilleDTO bankReconcilleDTO){
//		BaseDTO baseDTO = new BaseDTO();
//		log.info("<--- loadLazy BankReconcilleController Service ---> ");
//		baseDTO = bankReconcilleService.lazySearch(bankReconcilleDTO);
//		if (baseDTO != null) {
//			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
//		} else {
//			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
//		}
//	}
	@RequestMapping(value = "/searchDataLazy", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> lazySearch(@RequestBody BankReconcilleDTO bankReconcilleDTO){
		BaseDTO baseDTO = new BaseDTO();
		log.info("<---BankReconcilleController() loadLazy ---> ");
		baseDTO = bankReconcilleService.getAdvancePaymentLazy(bankReconcilleDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
}
