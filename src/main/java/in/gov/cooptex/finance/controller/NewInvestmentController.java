package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.accounts.model.InvestmentClosing;
import in.gov.cooptex.core.accounts.model.NewInvestment;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.SocietyRegistrationReqDTO;
import in.gov.cooptex.finance.dto.NewInvestmentDTO;
import in.gov.cooptex.finance.service.InvestmentClosingService;
import in.gov.cooptex.finance.service.NewInvestmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("${finance.api.url}/newinvestment")
@Api(tags = "New Investment", value = "New Investment")
@Log4j2
public class NewInvestmentController {

	@Autowired
	NewInvestmentService newInvestmentService;

	@RequestMapping(value = "/getById/{id}/{notificationId}", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getById(@PathVariable Long id,@PathVariable Long notificationId) { 
		log.info("InvestmentClosingController:get() [" + id + notificationId+ "]");
		BaseDTO baseDTO = newInvestmentService.getById(id,notificationId);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/saveorupdate", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> saveorupdate(@RequestBody NewInvestment newInvestment) {
		log.info("<--Starts newInvestmentController .createInvestmentClosing-->"+newInvestment.getPaymentMethod().getId());
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = newInvestmentService.saveorupdate(newInvestment);
		log.info("<--Stop newInvestmentController .createInvestmentClosing-->");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getallnewInvestmentlistlazy", method = RequestMethod.POST)
	public BaseDTO getAllNewInvestmentlistlazy(@RequestBody PaginationDTO paginationDto) {
		log.info("<--Starts newInvestmentController .getAllNewInvestmentlistlazy-->");	
		return newInvestmentService.getAllNewInvestmentlistlazy(paginationDto);
	}
	
	
	@RequestMapping(value = "/newInvestmentForwardStage", method = RequestMethod.POST)
	public  BaseDTO newInvestmentForwardStage(@RequestBody NewInvestmentDTO newInvestmentDTO) {
		log.info("<--Starts newInvestmentController .newInvestmentForwardStage-->");
		return newInvestmentService.newInvestmentForwardStage(newInvestmentDTO);
	}
	
	
	@RequestMapping(value = "/getpaymentmethod", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> loadPaymentMethod() {
		log.info("<--- Starts newInvestmentController .loadPaymentMethod() ---> ");
		BaseDTO baseDTO = newInvestmentService.loadPaymentMethod();
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	} 
	
	@RequestMapping(value = "/getSuppilerByType/{supplierTypeCode}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getSuppilerByType(@PathVariable String supplierTypeCode) {
		log.info("<--- Starts newInvestmentController .getSuppilerByType() ---> ");
		BaseDTO baseDTO = newInvestmentService.getSuppilerByType(supplierTypeCode);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	} 
	
	
	@RequestMapping(value = "/getInvestmentType", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getInvestmentType() {
		log.info("newInvestmentController:getInvestmentType()");
		BaseDTO baseDTO = newInvestmentService.getInvestmentType();
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
}