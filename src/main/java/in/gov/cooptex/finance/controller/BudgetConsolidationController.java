package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.dto.BudgetConsolidationDTO;
import in.gov.cooptex.finance.service.BudgetConsolidationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("${finance.api.url}/budgetconsolidation")
@Api(tags = "Budget Consolidation", value = "Budget Consolidation")
@Log4j2
public class BudgetConsolidationController {

	@Autowired
	BudgetConsolidationService budgetConsolidationService;

	@RequestMapping(value = "/get", method = RequestMethod.POST)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getById(@RequestBody BudgetConsolidationDTO budgetConsolidationDTO) {
		log.info("BudgetConsolidationController:get() [" + budgetConsolidationDTO.getId() + "]");
		BaseDTO baseDTO = budgetConsolidationService.getById(budgetConsolidationDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/getActiveBudgetConsolidation", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getActiveBudgetConsolidation() {
		log.info("BudgetConsolidationController:getActiveBudgetConsolidation()");
		BaseDTO baseDTO = budgetConsolidationService.getActiveBudgetConsolidation();
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> createBudgetConsolidation(@RequestBody BudgetConsolidationDTO budgetConsolidationDTO) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = budgetConsolidationService.createBudgetConsolidation(budgetConsolidationDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getBudgetNameList", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getBudgetNameList() {
		log.info("BudgetConsolidationController:getBudgetNameList-========");
		BaseDTO baseDTO = budgetConsolidationService.getBudgetNameList();
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getBudgetDetails/{budgetConfigId}", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getBudgetDetails(@PathVariable Long budgetConfigId) {
		log.info("BudgetConsolidationController:getBudgetNameList-========");
		BaseDTO baseDTO = budgetConsolidationService.getBudgetDetails(budgetConfigId);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/lazyLoadBudgetConsolidation", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> lazyLoadBudgetConsolidation(@RequestBody PaginationDTO paginationDTO) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = budgetConsolidationService.lazyLoadBudgetConsolidation(paginationDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/approveBudgetConsolidate", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> approveBudgetConsolidate(@RequestBody BudgetConsolidationDTO budgetConsolidationDTO) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = budgetConsolidationService.approveBudgetConsolidate(budgetConsolidationDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/rejectBudgetConsolidate", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> rejectBudgetConsolidate(@RequestBody BudgetConsolidationDTO budgetConsolidationDTO) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = budgetConsolidationService.rejectBudgetConsolidate(budgetConsolidationDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
}