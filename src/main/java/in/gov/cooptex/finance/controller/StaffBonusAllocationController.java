package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.accounts.model.StaffBonusAllocation;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.finance.service.StaffBonusAllocationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("${finance.api.url}/staffbonusallocation")
@Api(tags = "Staff Bonus Allocation", value = "Staff Bonus Allocation")
@Log4j2
public class StaffBonusAllocationController {

	@Autowired
	StaffBonusAllocationService staffBonusAllocationService;

	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getById(@PathVariable Long id) {
		log.info("StaffBonusAllocationController:get() [" + id + "]");
		BaseDTO baseDTO = staffBonusAllocationService.getById(id);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> createStaffBonusAllocation(@RequestBody StaffBonusAllocation staffBonusAllocation) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = staffBonusAllocationService.createStaffBonusAllocation(staffBonusAllocation);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
}