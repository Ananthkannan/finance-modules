package in.gov.cooptex.finance.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.accounts.service.GlAccountHeadService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.finance.dto.PettyCashReceiptDTO;
import in.gov.cooptex.finance.service.PaymentReceiptService;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/paymentreceipt")
@Log4j2
public class PaymentReceiptController {

	@Autowired
	GlAccountHeadService glAccountHeadService;
	
	@Autowired
	PaymentReceiptService paymentreceiptservice;

	@Autowired
	EmployeeMasterRepository employeeMasterRepository;

	@RequestMapping(value = "/getAutoCompleteSupplierCode/{supplierTypeId}/{supplierCode}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getAutoCompleteSupplierCode(@PathVariable Long supplierTypeId,
			@PathVariable String supplierCode) {
		log.info("<--- Starts paymentreceiptservice.getCode --->" + supplierCode);
		BaseDTO baseDTO = paymentreceiptservice.getSupplierCodeAutoComplete(supplierTypeId, supplierCode);
		log.info("<---Ends SupplierCode--->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/getAutoCompleteCustomerCode/{customerTypeId}/{customerCode}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getAutoCompleteCustomerCode(@PathVariable Long customerTypeId,
			@PathVariable String customerCode) {
		log.info("<--- Starts paymentreceiptservice.getCode --->" + customerCode + " customerId : " + customerTypeId);
		BaseDTO baseDTO = paymentreceiptservice.getCustomerCodeAutoComplete(customerTypeId, customerCode);
		log.info("<---Ends CustomerCode--->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/getActiveEmployeeNameOrderList", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getActiveEmployeeNameOrderList() {
		BaseDTO baseDTO = paymentreceiptservice.getActiveEmployeeNameOrderList();
		log.info("<---Ends EmployeeController.getAllEmployee()--->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/getEmployeeMasterDetails/{empCodeorName}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getEmployeeMasterDetails(@PathVariable String empCodeorName) {
		List<EmployeeMaster> empMasterList = employeeMasterRepository.getEmployeeByAutoComplete(empCodeorName);
		BaseDTO baseDTO = new BaseDTO();
		baseDTO.setResponseContents(empMasterList);
		log.info("<---Ends EmployeeController.getAllEmployee()--->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> createPettyCashReceipt(@RequestBody PettyCashReceiptDTO pettyCashReceiptDTO) {
		log.info("PettyCashReceiptController:createPettyCashReceipt()");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = paymentreceiptservice.createCashReceipt(pettyCashReceiptDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	/*
	 * @RequestMapping(value = "/getCustomerTypeLoading/{id}", method =
	 * RequestMethod.GET) public @ResponseBody ResponseEntity<BaseDTO>
	 * getCustomerTypeLoading(@PathVariable("id") Long id) {
	 * log.info("<--- Received getAllCustomerTypeMaster ---> ");
	 * 
	 * BaseDTO baseDTO = paymentreceiptservice.getCustomerTypeLoading(id); if
	 * (baseDTO != null) { log.info("<--- Starts getAllCustomerTypeMaster() ---> ");
	 * return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK); } else {
	 * log.error("<--- Starts IntendRequestController .created Failed() ---> ");
	 * return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT); }
	 * 
	 * }
	 */

	@RequestMapping(value = "/getallreceipttransactiondetails", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getAllReceiptTransactionDetails() {
		BaseDTO baseDTO = new BaseDTO();
		try {
//			log.info("############### PettyCashReceiptController:getAllReceiptTransactionDetails Starts ############");
			baseDTO = paymentreceiptservice.loadAllReceiptTransactionDetails();
		} catch (Exception e) {
			// TODO: handle exception
			log.error(
					" Exception in PettyCashReceiptController:loadAllReceiptTransactionDetails() : " + e.getMessage());
		}
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);

	}

	@RequestMapping(value = "/loadallreceipttrransactionlazylist", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> loadAllReceiptTransactionDetails(@RequestBody PaginationDTO paginationDTO) {
		log.info("PettyCashReceiptController:loadAllReceiptTransactionDetails()");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = paymentreceiptservice.loadAllReceiptTransactionDetails(paginationDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/checkbankdetails/{referenceNo}/{paymentModeId}/{bankMasterCode}", method = RequestMethod.GET)
	public BaseDTO checkBankDetailsAlreadyExistorNot(@PathVariable String referenceNo,
			@PathVariable String paymentModeId, @PathVariable String bankMasterCode) {
		log.info(" Bank Details Count  : ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			long count = paymentreceiptservice.checkBankDetailsAlreadyExistorNot(referenceNo, bankMasterCode,
					paymentModeId);
			log.info(" Bank Details Count  : " + count);
			baseDTO.setStatusCode((int) count);
		} catch (Exception e) {

			log.error(
					" Exception in PettyCashReceiptController:checkBankDetailsAlreadyExistorNot() : " + e.getMessage());
		}
		return baseDTO;
	}

	// viewDetails
	@RequestMapping(value = "/getdetailsbyvoucherid/{voucherid}/{notificationId}", method = RequestMethod.GET)
	public BaseDTO getDetailsByVoucherId(@PathVariable long voucherid,@PathVariable Long notificationId) {
		log.info(" PettyCashReceiptController -> getDetailsByVoucherId Method starts : ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			baseDTO = paymentreceiptservice.getDetailsByVoucherId(voucherid,notificationId);
		} catch (Exception e) {
			log.error(" Exception in PettyCashReceiptController:getDetailsByVoucherId() : " + e.getMessage());
		}
		return baseDTO;
	}
	
	@RequestMapping(value = "/loadallentity/{searchParam}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> loadforEntityMasterList(@PathVariable String searchParam) {
		BaseDTO baseDTO = null;
		baseDTO = paymentreceiptservice.loadforentityMasterList(searchParam);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.FOUND);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NOT_FOUND);
		}
	}	
	
	
	@RequestMapping(value="/submitstatus",method=RequestMethod.POST)
	public ResponseEntity<BaseDTO> submitStatus(@RequestBody PettyCashReceiptDTO pcDTO)
	{
		BaseDTO baseDTO = null;
		baseDTO=paymentreceiptservice.submitStatus(pcDTO);
		if(baseDTO!=null)
		{
			return new ResponseEntity<BaseDTO>(baseDTO,HttpStatus.ACCEPTED);
		}else
		{
			return new ResponseEntity<BaseDTO>(baseDTO,HttpStatus.INTERNAL_SERVER_ERROR);	
		}
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> updatePettyCashReceipt(@RequestBody PettyCashReceiptDTO pettyCashReceiptDTO) {
		log.info("PettyCashReceiptController:createPettyCashReceipt()");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = paymentreceiptservice.updatePaymentDetails(pettyCashReceiptDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/cancelInvoiceByVoucherId/{voucherId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> cancelInvoiceByVoucherId(@PathVariable Long voucherId) {
		BaseDTO baseDTO = null;
		baseDTO = paymentreceiptservice.cancelInvoiceByVoucherId(voucherId);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	@RequestMapping(value = "/checkPaymentDetailsRecordByVoucherId/{voucherId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> checkPaymentDetailsRecordByVoucherId(@PathVariable Long voucherId) {
		BaseDTO baseDTO = null;
		baseDTO = paymentreceiptservice.getPaymentAmount(voucherId);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/makepayment", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> makepayment(@RequestBody PettyCashReceiptDTO pettyCashReceiptDTO) {
		log.info("PettyCashReceiptController:makepayment()");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO =  paymentreceiptservice.makePayment(pettyCashReceiptDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/getGlaccountByCode/{headCode}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getGlaccountByCode(@PathVariable String headCode) {
		log.info("PettyCashReceiptController. getGlaccountByCode()  STARTS ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = paymentreceiptservice.getGlaccountByCode(headCode);
		log.info("PettyCashReceiptController. getGlaccountByCode()  ENDS ");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getSundryAdvanceOBAmount/{loginEntityId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getSundryAdvanceOBAmount(@PathVariable Long loginEntityId) {
		log.info("PettyCashReceiptController. getSundryAdvanceOBAmount()  STARTS ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = paymentreceiptservice.getSundryAdvanceOBAmount(loginEntityId);
		log.info("PettyCashReceiptController. getSundryAdvanceOBAmount()  ENDS ");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
}
