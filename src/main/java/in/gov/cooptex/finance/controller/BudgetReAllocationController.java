package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.dto.BudgetReAllocationDTO;
import in.gov.cooptex.finance.service.BudgetReAllocationService;
import io.swagger.annotations.Api;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("${finance.api.url}/budgetreallocation")
@Api(tags = "Budget Reallocation", value = "Budget Reallocation")
@Log4j2
public class BudgetReAllocationController {

	@Autowired
	BudgetReAllocationService budgetReAllocationService;
	
	@PostMapping("/lazyloadbudgetreallocation")
	public ResponseEntity<BaseDTO> getAllBudgetReAllocationLazyList(@RequestBody PaginationDTO paginationDTO) {
		log.info("<=====Starts BudgetReAllocationController.getAllBudgetReAllocationLazyList =======>"+paginationDTO);
		BaseDTO baseDTO = budgetReAllocationService.getAllBudgetReAllocationLazyList(paginationDTO);
		log.info("<=====Ends BudgetReAllocationController.getAllBudgetReAllocationLazyList =======>");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/getbudgetconfig")
	public ResponseEntity<BaseDTO> getBudgetCongig(){
		log.info("<----------BudgetReAllocationController getBudgetCongig() starts----------->");
		BaseDTO baseDTO = budgetReAllocationService.getBudgetConfig();
		log.info("<----------BudgetReAllocationController getBudgetCongig() ends----------->");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/generate")
	public ResponseEntity<BaseDTO> generateBudgetReAllocation(@RequestBody BudgetReAllocationDTO budgetReAllocationDTO){
		log.info("<=====Starts BudgetReAllocationController.generateBudgetReAllocation =======>");
		BaseDTO baseDTO = budgetReAllocationService.generateBudgetReAllocation(budgetReAllocationDTO);
		log.info("<=====Ends BudgetReAllocationController.generateBudgetReAllocation =======>");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/saveorupdate")
	public ResponseEntity<BaseDTO> saveOrUpdate(@RequestBody BudgetReAllocationDTO budgetReAllocationDTO){
		log.info("<=====Starts BudgetReAllocationController.saveOrUpdate =======>");
		BaseDTO baseDTO = budgetReAllocationService.saveOrUpdate(budgetReAllocationDTO);
		log.info("<=====Ends BudgetReAllocationController.saveOrUpdate =======>");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/viewbudgettransfer")
	public ResponseEntity<BaseDTO> getBudgetTransfer(@RequestBody BudgetReAllocationDTO budgetReAllocationDTO){
		log.info("<----------BudgetReAllocationController getBudgetTransfer() starts----------->");
		BaseDTO baseDTO = budgetReAllocationService.getBudgetTransfer(budgetReAllocationDTO);
		log.info("<----------BudgetReAllocationController getBudgetTransfer() ends----------->");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/approvebudgetreallocation")
	public ResponseEntity<BaseDTO> approveBudgetReAllocation(@RequestBody BudgetReAllocationDTO budgetReAllocationDTO){
		log.info("<=====Starts BudgetReAllocationController.approveBudgetReAllocation =======>");
		BaseDTO baseDTO = budgetReAllocationService.approveBudgetReAllocation(budgetReAllocationDTO);
		log.info("<=====Ends BudgetReAllocationController.approveBudgetReAllocation =======>");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/rejectbudgetreallocation")
	public ResponseEntity<BaseDTO> rejectBudgetReAllocation(@RequestBody BudgetReAllocationDTO budgetReAllocationDTO){
		log.info("<=====Starts BudgetReAllocationController.rejectBudgetReAllocation =======>");
		BaseDTO baseDTO = budgetReAllocationService.rejectBudgetReAllocation(budgetReAllocationDTO);
		log.info("<=====Ends BudgetReAllocationController.rejectBudgetReAllocation =======>");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
}
