package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.accounts.model.FinalAuditDetails;
import in.gov.cooptex.core.accounts.model.MarketingInspectionInventory;
import in.gov.cooptex.core.accounts.model.MarketingInspectionModernization;
import in.gov.cooptex.core.accounts.model.MarketingInspectionProfitability;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.dto.FinalAuditResponseDto;
import in.gov.cooptex.finance.service.FinalAuditService;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/finalAudit")
@Log4j2
public class FinalAuditController {

	@Autowired
	FinalAuditService finalAuditService;
	// ---------------------

	@RequestMapping(value = "/getallactiveshowrooms/{query}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getAllActiveShowrooms(@PathVariable("query") String query) {
		log.info("IntensiveInspectionReportController. getAllActiveShowrooms() - START");
		BaseDTO baseDto = finalAuditService.getAllActiveShowrooms(query);
		log.info("IntensiveInspectionReportController. getAllActiveShowrooms() - END");
		if (baseDto != null) {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/saveorupdatefinalauditdetails")
	public ResponseEntity<BaseDTO> saveOrUpdateFinalAuditDetails(@RequestBody FinalAuditDetails finalAuditDetails) {
		log.info("=======START FinalAuditController.saveOrUpdateFinalAuditDetails=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = finalAuditService.saveOrUpdateFinalAudit(finalAuditDetails);
		log.info("=======END FinalAuditController.saveOrUpdateFinalAuditDetails=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/saveorupdatesalesdetails")
	public ResponseEntity<BaseDTO> saveOrUpdateSalesDetails(@RequestBody FinalAuditDetails finalAuditDetails) {
		log.info("=======START FinalAuditController.saveOrUpdateSalesDetails=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = finalAuditService.saveOrUpdateSalesDetails(finalAuditDetails);
		log.info("=======END FinalAuditController.saveOrUpdateSalesDetails=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/saveorupdatestockdetails")
	public ResponseEntity<BaseDTO> saveOrUpdateStockDetails(@RequestBody FinalAuditResponseDto finalAuditResponseDto) {
		log.info("=======START FinalAuditController.saveOrUpdateStockDetails=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = finalAuditService.saveOrUpdateStockDetails(finalAuditResponseDto);
		log.info("=======END FinalAuditController.saveOrUpdateStockDetails=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/saveorupdateOtherdetails")
	public ResponseEntity<BaseDTO> saveorupdateOtherdetails(@RequestBody FinalAuditDetails finalAuditDetails) {
		log.info("=======START FinalAuditController.saveorupdateOtherdetails=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = finalAuditService.saveorupdateOtherdetails(finalAuditDetails);
		log.info("=======END FinalAuditController.saveorupdateOtherdetails=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}

	@RequestMapping(value = "/loadAllSalesDtlsTable", method = RequestMethod.POST)
	public BaseDTO loadAllSalesDtlsTable(@RequestBody FinalAuditDetails finalAuditDetails) {
		log.info("=======START FinalAuditController.loadAllSalesDtlsTable=====");
		return finalAuditService.loadAllSalesDetailsDatatable(finalAuditDetails);
	}

	@RequestMapping(value = "/loadAllStockDtlsTable", method = RequestMethod.POST)
	public BaseDTO loadAllStockDtlsTable(@RequestBody FinalAuditDetails finalAuditDetails) {
		log.info("=======START FinalAuditController.loadAllStockDtlsTable=====");
		return finalAuditService.loadAllStockDtlsTable(finalAuditDetails);
	}

	@RequestMapping(value = "/loadAllGoodsDtlsTable", method = RequestMethod.POST)
	public BaseDTO loadAllGoodDtlsTable(@RequestBody FinalAuditDetails finalAuditDetails) {
		log.info("=======START FinalAuditController.loadAllGoodDtlsTable=====");
		return finalAuditService.loadAllGoodDtlsTable(finalAuditDetails);
	}

	@RequestMapping(value = "/loadAllOtherDtlsTable", method = RequestMethod.POST)
	public BaseDTO loadAllOtherDtlsTable(@RequestBody FinalAuditDetails finalAuditDetails) {
		log.info("=======START FinalAuditController.loadAllOtherDtlsTable=====");
		return finalAuditService.loadAllOtherDtlsTable(finalAuditDetails);
	}
	// ---------------------

	@RequestMapping(value = "/getallactiveregions", method = RequestMethod.GET)
	public BaseDTO getAllActiveRegions() {
		log.info("=======START FinalAuditController.getAllActiveRegions=====");
		return finalAuditService.getAllActiveRegions();

	}

	@RequestMapping(value = "/getallshowroomforregion/{regionId}", method = RequestMethod.GET)
	public BaseDTO getAllShowroomForRegion(@PathVariable("regionId") Long regionId) {
		log.info("=======START FinalAuditController.getAllShowroomForRegion=====");
		return finalAuditService.getAllShowroomForRegion(regionId);

	}

	@RequestMapping(method = RequestMethod.PUT, path = "/saveorupdateProfitability")
	public ResponseEntity<BaseDTO> saveOrUpdateProfitability(
			@RequestBody MarketingInspectionProfitability marketingInspectionProfitability) {
		log.info("=======START FinalAuditController.saveOrUpdateProfitability=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = finalAuditService.saveOrUpdateProfitability(marketingInspectionProfitability);
		log.info("=======END FinalAuditController.saveOrUpdateProfitability=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}

	/** This method search the employee with the given input */
	@RequestMapping(value = "/getAutoCompleteProductVariety/{productName}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getProductVarietyName(@PathVariable String productName) {
		log.info("<--- Starts FinalAuditController.getProductVarietyName() --->" + productName);
		BaseDTO baseDTO = finalAuditService.getProductVarietyNameAutoComplete(productName);
		log.info("<---Ends FinalAuditController.getProductVarietyName()--->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/saveorupdateInventory")
	public ResponseEntity<BaseDTO> saveorupdateInventory(
			@RequestBody MarketingInspectionInventory marketingInspectionInventory) {
		log.info("=======START FinalAuditController.saveorupdateInventory=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = finalAuditService.saveOrUpdateMarketingInspectionInventory(marketingInspectionInventory);
		log.info("=======END FinalAuditController.saveorupdateInventory=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/saveorupdateModernization")
	public ResponseEntity<BaseDTO> saveOrUpdateModernization(
			@RequestBody MarketingInspectionModernization marketingInspectionModernization) {
		log.info("=======START FinalAuditController.saveOrUpdateModernization=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = finalAuditService.saveOrUpdateModernization(marketingInspectionModernization);
		log.info("=======END FinalAuditController.saveOrUpdateModernization=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}

	@RequestMapping(value = "/getAllBuildType", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getAllBuildTypeData() {
		log.info("=======START FinalAuditController.getAllBuildTypeData=====");
		BaseDTO baseDTO = finalAuditService.getAllBuildType();
		log.info("=======END FinalAuditController.getAllBuildTypeData=====");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}

	/* This Controller method is used to get data for list and search */
	@RequestMapping(value = "/getlazyload", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<BaseDTO> getLazyLoadDatas(@RequestBody PaginationDTO paginationDTO) {
		log.info("=======START FinalAuditController.getLazyLoadDatas=====");
		BaseDTO baseDTO = finalAuditService.getLazyLoadData(paginationDTO);
		log.info("=======END FinalAuditController.getLazyLoadDatas=====");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/getById/{id}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getById(@PathVariable Long id) {
		log.info("=======START FinalAuditController.getAllBuildTypeData=====");
		BaseDTO baseDTO = finalAuditService.getFinalAuditById(id);
		log.info("=======END FinalAuditController.getAllBuildTypeData=====");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/loadAllInventoryDtlsTable/{showroomId}", method = RequestMethod.GET)
	public BaseDTO loadAllInventoryDtlsTable(@PathVariable("showroomId") Long showroomId) {
		log.info("=======START FinalAuditController.loadAllInventoryDtlsTable=====");
		return finalAuditService.loadAllInventoryDtlsTable(showroomId);
	}

	@RequestMapping(value = "/loadProfitabilityDtlsTable/{showroomId}", method = RequestMethod.GET)
	public BaseDTO loadProfitabilityDtlsTable(@PathVariable("showroomId") Long showroomId) {
		log.info("=======START FinalAuditController.loadProfitabilityDtlsTable=====");
		return finalAuditService.loadProfitabilityDtlsTable(showroomId);
	}
}
