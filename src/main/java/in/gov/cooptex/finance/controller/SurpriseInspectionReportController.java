package in.gov.cooptex.finance.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.accounts.model.SurpriseInspection;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.IntensiveInspectionTestStockDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.dto.IntensiveInspectionAuditRequestDto;
import in.gov.cooptex.finance.dto.IntensiveInspectionBudgetDTO;
import in.gov.cooptex.finance.dto.IntensiveInspectionCreditSalesDTO;
import in.gov.cooptex.finance.dto.StockDetailsDto;
import in.gov.cooptex.finance.dto.StockDetailsRequestDetails;
import in.gov.cooptex.finance.dto.SurpriseInspectionCashDTO;
import in.gov.cooptex.finance.dto.SurpriseInspectionDTO;
import in.gov.cooptex.finance.service.SurpriseInspectionReportService;
import in.gov.cooptex.operation.model.IntensiveInspectionAudit;
import in.gov.cooptex.operation.model.IntensiveInspectionCreditDetails;
import in.gov.cooptex.operation.model.IntensiveInspectionGeneralDetails;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/surpriseInspection")
@Log4j2
public class SurpriseInspectionReportController {

	@Autowired
	SurpriseInspectionReportService surpriseInspectionReportService;
	
	
	@RequestMapping(value = "/getlazyload", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<BaseDTO> getLazyLoadData(@RequestBody PaginationDTO paginationDTO) {
		log.info("SurpriseInspectionReportController.getLazyLoadData() - START");
		BaseDTO baseDTO = surpriseInspectionReportService.getLazyLoadData(paginationDTO);
		log.info("SurpriseInspectionReportController.getLazyLoadData() - END");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
//	===================================================================================================

	@RequestMapping(method = RequestMethod.PUT, path = "/saveorupdateintensiveinspectionAudit")
	public ResponseEntity<BaseDTO> saveOrUpdateIntensiveInspection(
			@RequestBody IntensiveInspectionAudit intensiveInspectionAudit) {
		log.info("IntensiveInspectionReportController.saveOrUpdateIntensiveInspection() - START");
		BaseDTO baseDto = new BaseDTO();
		baseDto = surpriseInspectionReportService.saveOrUpdateIntensiveInspectionAudit(intensiveInspectionAudit);
		log.info("IntensiveInspectionReportController.saveOrUpdateIntensiveInspection() - END");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}

	@RequestMapping(value = "/getFinanceYear", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getFinanceYear() {
		log.info("IntensiveInspectionReportController.getFinanceYear() - START");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = surpriseInspectionReportService.getFinanceYear();
		log.info("IntensiveInspectionReportController.getFinanceYear() - END");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getemployeebyentityId/{entityId}/{auditId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getEmployeeListByEntityId(@PathVariable("entityId") Long entityId, @PathVariable("auditId") Long auditId) {
		log.info("IntensiveInspectionReportController.getEmployeeListByEntityId() - START");
		BaseDTO baseDto = surpriseInspectionReportService.getEmployeeListByEntityId(entityId, auditId);
		log.info("IntensiveInspectionReportController.getEmployeeListByEntityId() - END");
		if (baseDto != null) {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/deleteintensiveemployee/{auditEmployeeId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> deleteIntensiveEmployeeId(@PathVariable("auditEmployeeId") Long auditEmployeeId) {
		log.info("IntensiveInspectionReportController.deleteIntensiveEmployeeId() - START");
		BaseDTO baseDto = surpriseInspectionReportService.deleteIntensiveEmployeeId(auditEmployeeId);
		log.info("IntensiveInspectionReportController.deleteIntensiveEmployeeId() - END");
		if (baseDto != null) {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/saveorupdateintensiveinspectionEmployee")
	public ResponseEntity<BaseDTO> saveOrUpdateAuditSales(@RequestBody IntensiveInspectionAuditRequestDto intensiveInspectionAuditRequestDto) {
		log.info("IntensiveInspectionReportController.saveOrUpdateAuditSales() - START");
		BaseDTO baseDto = new BaseDTO();
		baseDto = surpriseInspectionReportService.saveOrUpdateIntensiveInspectionEmployee(intensiveInspectionAuditRequestDto);
		log.info("IntensiveInspectionReportController.saveOrUpdateAuditSales() - END");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, path = "/saveorupdateintensiveinspectionclosing")
	public ResponseEntity<BaseDTO> saveOrUpdateIntensiveInspectionClosing(
			@RequestBody IntensiveInspectionAuditRequestDto intensiveInspectionAuditRequestDto) {
		log.info("=======START IntensiveInspectionReportController.saveOrUpdateIntensiveInspection=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = surpriseInspectionReportService.saveOrUpdateIntensiveInspectionClosings(intensiveInspectionAuditRequestDto);
		log.info("=======END IntensiveInspectionReportController.saveOrUpdate=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/saveorupdateintensivegeneraldetails")
	public ResponseEntity<BaseDTO> saveOrUpdateIntensiveGeneralDetails(
			@RequestBody IntensiveInspectionGeneralDetails intensiveInspectionGeneralDetails) {
		log.info("IntensiveInspectionReportController.saveOrUpdateIntensiveGeneralDetails() - START");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = surpriseInspectionReportService.saveOrUpdateIntensiveGeneralDetails(intensiveInspectionGeneralDetails);
		log.info("IntensiveInspectionReportController.saveOrUpdateIntensiveGeneralDetails() - END");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/saveorupdatecreditdetails")
	public ResponseEntity<BaseDTO> saveOrUpdateIntensiveCreditDetails(
			@RequestBody IntensiveInspectionCreditDetails intensiveInspectionCreditDetails) {
		log.info("IntensiveInspectionReportController.saveOrUpdateIntensiveCreditDetails() - START");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = surpriseInspectionReportService.saveOrUpdateIntensiveCreditDetails(intensiveInspectionCreditDetails);
		log.info("IntensiveInspectionReportController.saveOrUpdateIntensiveCreditDetails() - END");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/savebudgetdetails")
	public ResponseEntity<BaseDTO> saveOrUpdateBudgetDetails(
			@RequestBody IntensiveInspectionBudgetDTO intensiveInspectionBudgetDTO) {
		log.info("IntensiveInspectionReportController.saveOrUpdateBudgetDetails() - START");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = surpriseInspectionReportService.saveOrUpdateBudgetDetails(intensiveInspectionBudgetDTO);
		log.info("IntensiveInspectionReportController.saveOrUpdateBudgetDetails() - END");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/saveteststockdetails")
	public ResponseEntity<BaseDTO> saveOrUpdateTestStockDetails(
			@RequestBody IntensiveInspectionTestStockDTO intensiveInspectionTestStockDTO) {
		log.info("IntensiveInspectionReportController.saveOrUpdateTestStockDetails() - START");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = surpriseInspectionReportService.saveOrUpdateTestStockDetails(intensiveInspectionTestStockDTO);
		log.info("IntensiveInspectionReportController.saveOrUpdateTestStockDetails() - END");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}	
	@RequestMapping(value = "/getById/{auditId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getById(@PathVariable("auditId") Long auditId) {
		log.info("IntensiveInspectionReportController.getById() - START");
		BaseDTO baseDto = surpriseInspectionReportService.getById(auditId);
		log.info("IntensiveInspectionReportController.getById() - END");
		if (baseDto != null) {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getByInspId/{auditId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getByInspId(@PathVariable("auditId") Long auditId) {
		log.info("IntensiveInspectionReportController.getById() - START");
		BaseDTO baseDto = surpriseInspectionReportService.getByInspId(auditId);
		log.info("IntensiveInspectionReportController.getById() - END");
		if (baseDto != null) {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.NO_CONTENT);
		}
	}
	
	
	@RequestMapping(method = RequestMethod.PUT, path = "/saveStockValues")
	public ResponseEntity<BaseDTO> saveOrUpdateIntensiveInspectionStock( @RequestBody StockDetailsRequestDetails stockDetailsRequestDetails) {
		log.info("=======START IntensiveInspectionReportController.saveOrUpdateIntensiveInspectionStock=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = surpriseInspectionReportService.saveOrUpdateIntensiveInspectionStock(stockDetailsRequestDetails);
		log.info("=======END IntensiveInspectionReportController.saveOrUpdateIntensiveInspectionStock=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}

	@RequestMapping(value = "/getallactiveshowrooms/{query}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getAllActiveShowrooms(@PathVariable("query") String query) {
		log.info("IntensiveInspectionReportController. getAllActiveShowrooms() - START");
		BaseDTO baseDto = surpriseInspectionReportService.getAllActiveShowrooms(query);
		log.info("IntensiveInspectionReportController. getAllActiveShowrooms() - END");
		if (baseDto != null) {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getLastInspectionDateByShowroomId/{showroomId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getLastInspectionDateByShowroomId(@PathVariable("showroomId") Long showroomId) {
		log.info("IntensiveInspectionReportController. getLastInspectionDateByShowroomId() - START");
		BaseDTO baseDto = surpriseInspectionReportService.getLastInspectionDateByShowroomId(showroomId);
		log.info("IntensiveInspectionReportController. getLastInspectionDateByShowroomId() - END");
		if (baseDto != null) {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getbudgetreviewData/{showroomId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getBudgetReviewData(@PathVariable("showroomId") Long showroomId) {
		log.info("IntensiveInspectionReportController. getBudgetReviewData() - START");
		BaseDTO baseDto = surpriseInspectionReportService.getBudgetReviewData(showroomId);
		log.info("IntensiveInspectionReportController. getBudgetReviewData() - END");
		if (baseDto != null) {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getcreditSalesData", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> getCreditSalesData(
			@RequestBody IntensiveInspectionCreditSalesDTO intensiveInspectionCreditSalesDTO) {
		log.info("IntensiveInspectionReportController. getCreditSalesData() - START");
		BaseDTO baseDto = surpriseInspectionReportService.getCreditSalesData(intensiveInspectionCreditSalesDTO);
		log.info("IntensiveInspectionReportController. getCreditSalesData() - END");
		if (baseDto != null) {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getActiveProductVarietyForAutoComplete/{productCodeOrName}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getActiveProductVarietyForAutoComplete(@PathVariable("productCodeOrName") String productCodeOrName) {
		log.info("IntensiveInspectionReportController. getActiveProductVarietyForAutoComplete() - START");
		BaseDTO baseDto = surpriseInspectionReportService.getActiveProductVarietyForAutoComplete(productCodeOrName);
		log.info("IntensiveInspectionReportController. getActiveProductVarietyForAutoComplete() - END");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getBookedQtyValue/{auditId}/{productId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getBookedQtyValue(@PathVariable("auditId") Long auditId, @PathVariable("productId") Long productId) {
		log.info("IntensiveInspectionReportController. getBookedQtyValue() - START");
		BaseDTO baseDto = surpriseInspectionReportService.getBookedQtyValue(auditId, productId);
		log.info("IntensiveInspectionReportController. getBookedQtyValue() - END");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getStockVerificationDateValue", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> getStockVerificationDateValue(
			@RequestBody StockDetailsDto stockDetailsDto) {
		log.info("IntensiveInspectionReportController. getStockVerificationDateValue() - START");
		BaseDTO baseDto = surpriseInspectionReportService.getStockVerificationDateValue(stockDetailsDto);
		log.info("IntensiveInspectionReportController. getStockVerificationDateValue() - END");
		if (baseDto != null) {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/loadCashBalanceData")
	public ResponseEntity<BaseDTO> loadCashBalanceData(@RequestBody SurpriseInspectionDTO surpriseInspectionDTO) {
		log.info("SurpriseInspectionReportController.loadCashBalanceData() - START");
		BaseDTO baseDto = surpriseInspectionReportService.loadCashBalanceData(surpriseInspectionDTO);
		log.info("SurpriseInspectionReportController.loadCashBalanceData() - END");
		if (baseDto != null) {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value="/savesurpriseinspectiondetails" , method=RequestMethod.POST)
	public ResponseEntity<BaseDTO> saveSurpriseVisit(@RequestBody SurpriseInspection surpriseInspection){
		BaseDTO baseDto = surpriseInspectionReportService.saveSurpriseVisit(surpriseInspection);
		log.info("SurpriseInspectionReportController.loadCashBalanceData() - END");
		if (baseDto != null) {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.NO_CONTENT);
		}	
	}
	
	@RequestMapping(value="/savedenominationdetails" , method=RequestMethod.POST)
	public ResponseEntity<BaseDTO> saveDenoDetails(@RequestBody SurpriseInspectionCashDTO surpriseInspectionCashDTO){
		BaseDTO baseDto = surpriseInspectionReportService.saveDenoDetails(surpriseInspectionCashDTO);
		log.info("SurpriseInspectionReportController.loadCashBalanceData() - END");
		if (baseDto != null) {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.NO_CONTENT);
		}	
	}
	
	@RequestMapping(value="/updatedenominationdetails" , method=RequestMethod.POST)
	public ResponseEntity<BaseDTO> updateDenoDetails(@RequestBody List<SurpriseInspectionCashDTO> surpriseInspectionCashDTOList){
		BaseDTO baseDto = surpriseInspectionReportService.updateDenoDetails(surpriseInspectionCashDTOList);
		log.info("SurpriseInspectionReportController.loadCashBalanceData() - END");
		if (baseDto != null) {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.NO_CONTENT);
		}	
	}
	
//	------------------------------------------
	
	@RequestMapping(value = "/loadallstockdtlstable/{showroomId}", method = RequestMethod.GET)
	public BaseDTO loadAllStockDtlsTable(@PathVariable("showroomId") Long showroomId) {
 		log.info("=======START SurpriseInspectionReportController.loadAllStockDtlsTable=====");
		return surpriseInspectionReportService.loadAllStockDetailsDatatable(showroomId);
	}
	@RequestMapping(value = "/loadallsalesdtlstable/{showroomId}", method = RequestMethod.GET)
	public BaseDTO loadAllSalesDtlsTable(@PathVariable("showroomId") Long showroomId) {
		log.info("=======START SurpriseInspectionReportController.loadAllSalesDtlsTable=====");
		return surpriseInspectionReportService.loadAllSalesDtlsTable(showroomId);
	}
	@RequestMapping(value = "/loadallothrdtlstable/{showroomId}", method = RequestMethod.GET)
	public BaseDTO loadAllOtherDtlsTable(@PathVariable("showroomId") Long showroomId) {
		log.info("=======START SurpriseInspectionReportController.loadAllOtherDtlsTable=====");
		return surpriseInspectionReportService.loadAllOtherDtlsTable(showroomId);
	}
	
	
}
