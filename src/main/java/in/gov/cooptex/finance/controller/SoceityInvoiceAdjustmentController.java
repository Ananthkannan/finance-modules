package in.gov.cooptex.finance.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.SoceityInvoiceAdjustmentDTO;
import in.gov.cooptex.finance.service.SoceityInvoiceAdjustmentService;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping("/societyinvoiceadjustment")
public class SoceityInvoiceAdjustmentController {

	@Autowired
	SoceityInvoiceAdjustmentService soceityInvoiceAdjustmentService;

	@RequestMapping(value = "/saveSocityAdjustmentMster", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> saveSoceityAdjustmentMaster(
			@RequestBody List<SoceityInvoiceAdjustmentDTO> soceityInvoiceAdjustmentDTOList) {
		log.info("<---------Starts societyadjustmentmaster.saveSoceityAdjustmentMaster ---------->"+soceityInvoiceAdjustmentDTOList!=null?soceityInvoiceAdjustmentDTOList.size():0);
		BaseDTO baseDto = soceityInvoiceAdjustmentService.saveSoceityInvoiceAdjustmentMaster(soceityInvoiceAdjustmentDTOList);
		log.info("<---------Ended societyadjustmentmaster.saveSoceityAdjustmentMaster ---------->");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}

	@RequestMapping(value = "/getSocietyAdjustmentMasterByCodeOrName/{codeOrName}", method = RequestMethod.GET)
	public BaseDTO getSocietyAdjustmentMasterByCodeOrName(@PathVariable String codeOrName) {
		log.info("<--Starts getEntityMasterUsingNameAndCode -->");
		BaseDTO baseDto = soceityInvoiceAdjustmentService.getSocietyAdjustmentMasterByCodeOrName(codeOrName);
		log.info("<--Ends getEntityMasterUsingNameAndCode .is -->");
		return baseDto;
	}

	@RequestMapping(value = "/getSectionMasterCodeOrName/{codeOrName}", method = RequestMethod.GET)
	public BaseDTO getSectionMasterCodeOrName(@PathVariable String codeOrName) {
		log.info("<--Starts getEntityMasterUsingNameAndCode -->");
		BaseDTO baseDto = soceityInvoiceAdjustmentService.getSectionMasterCodeOrName(codeOrName);
		log.info("<--Ends getEntityMasterUsingNameAndCode .is -->");
		return baseDto;
	}

	@RequestMapping(value = "/getlazyload", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<BaseDTO> getLazyLoadDatas(@RequestBody PaginationDTO paginationDTO) {
		log.info("=======START SoceityInvoiceAdjustmentController.getLazyLoadDatas=====");
		BaseDTO baseDTO = soceityInvoiceAdjustmentService.getLazyLoadData(paginationDTO);
		log.info("=======END SoceityInvoiceAdjustmentController.getLazyLoadDatas=====");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
}
