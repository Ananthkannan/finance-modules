package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.finance.dto.ReceiptPaymentRequestDTO;
import in.gov.cooptex.finance.service.InsuranceReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("${finance.api.url}/insurancereport")
@Api(tags = "Insurance Report", value = "Insurance Report")
@Log4j2
public class InsuranceReportController {
	
	@Autowired
	InsuranceReportService insuranceReportService;

	@RequestMapping(value = "/gendrateinsurancereport/{regId}", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> gendrateInsuranceReport(@PathVariable Long regId) {
		log.info("InsuranceReportController:gendrateInsuranceReport------");
		BaseDTO baseDTO = insuranceReportService.gendrateInsuranceReport(regId);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT); 
		}
	}
}
