package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.GeneratePdfDTO;
import in.gov.cooptex.core.dto.TaxInvoiceDTO;
import in.gov.cooptex.finance.GstPaymentDTO;
import in.gov.cooptex.finance.JournalVoucherDTO;
import in.gov.cooptex.finance.service.GstPaymentService;
import io.swagger.annotations.Api;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("${finance.api.url}/gstPaymentController")
@Api(tags = "GST Payment", value = "GST Payment")
@Log4j2
public class GstPaymentController {

	@Autowired
	GstPaymentService gstPaymentService;
	
	
	
	@RequestMapping(value = "/getstatment1", method = RequestMethod.POST)
	public BaseDTO getstatment1(@RequestBody GstPaymentDTO gstPaymentDTO) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = gstPaymentService.getstatment1(gstPaymentDTO);
		if (baseDTO != null) {
			return baseDTO;
		} else {
			return null;
		}
	}
	
	@RequestMapping(value = "/getstatment2", method = RequestMethod.POST)
	public BaseDTO getstatment2(@RequestBody GstPaymentDTO gstPaymentDTO) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = gstPaymentService.getstatment2(gstPaymentDTO);
		if (baseDTO != null) {
			return baseDTO;
		} else {
			return null;
		}
	}
	
	@RequestMapping(value = "/getstatment5", method = RequestMethod.POST)
	public BaseDTO getstatment5(@RequestBody GstPaymentDTO gstPaymentDTO) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = gstPaymentService.getstatment5(gstPaymentDTO);
		if (baseDTO != null) {
			return baseDTO;
		} else {
			return null;
		}
	}
	
	@RequestMapping(value = "/gettaxinvoicelistforstatements", method = RequestMethod.POST)
	public BaseDTO getTaxInvoiceListForStatements(@RequestBody TaxInvoiceDTO taxInvoiceDTO) {
		log.info("GstPaymentController. getTaxInvoiceListForStatements() Starts");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = gstPaymentService.getTaxInvoiceListForStatements(taxInvoiceDTO);
		if (baseDTO != null) {
			return baseDTO;
		} else {
			return null;
		}
	}
	
	@RequestMapping(value = "/gstnumberlist", method = RequestMethod.POST)
	public BaseDTO getGstNumberList(@RequestBody TaxInvoiceDTO taxInvoiceDTO) {
		log.info("GstPaymentController. getGstNumberList() Starts");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = gstPaymentService.getGstNumberList(taxInvoiceDTO);
		if (baseDTO != null) {
			return baseDTO;
		} else {
			return null;
		}
	}
	
	@RequestMapping(value = "/checkTaxInvoiceRecordInNextMonth", method = RequestMethod.POST)
	public BaseDTO checkTaxInvoiceRecordInNextMonth(@RequestBody TaxInvoiceDTO taxInvoiceDTO) {
		log.info("GstPaymentController. checkTaxInvoiceRecordInNextMonth() START");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = gstPaymentService.checkTaxInvoiceRecordInNextMonth(taxInvoiceDTO);
		if (baseDTO != null) {
			return baseDTO;
		} else {
			return null;
		}
	}
	
	@RequestMapping(value = "/generatePdf", method = RequestMethod.POST)
	public BaseDTO generatePdf(@RequestBody GeneratePdfDTO generatePdfDTO) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = gstPaymentService.downloadPDF(generatePdfDTO);
		if (baseDTO != null) {
			return baseDTO;
		} else {
			return null;
		}
	}
	
}
