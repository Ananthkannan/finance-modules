package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.finance.dto.CreditSalesRecoveryDTO;
import in.gov.cooptex.finance.dto.CreditSalesRecoveryRequestDTO;
import in.gov.cooptex.finance.dto.RecoveryCollectionSearchRequestDto;
import in.gov.cooptex.finance.service.CreditSalesRecoveryCollectionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
@RestController
@RequestMapping("${finance.api.url}/creditsalesrecovery")
@Api(tags = "Recovery Collection Details", value = "Recovery Collection Details")
@Log4j2
public class CreditSalesRecoveryCollectionController {

	@Autowired
	CreditSalesRecoveryCollectionService recoveryCollectionDetailService;
	
	//to get organization list fromDemand table based on creditSalesRequestId
	@GetMapping(value="/getallorganizations/{creditSalesRequestId}")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO>  getAllOraganizationMasters(@PathVariable("creditSalesRequestId") Long creditSalesRequestId) {
		log.info("<--Starts CreditSalesRecoveryCollectionController .getAllOraganizationMasters-->");	
		BaseDTO baseDTO = recoveryCollectionDetailService.getAllOraganizationMasters(creditSalesRequestId);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	//to get all creditSalesRequest list fromDemand table
	@GetMapping(value="/getallcreditsalesrequest")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO>  getAllCreditSalesRequests() {
		log.info("<--Starts CreditSalesRecoveryCollectionController .getAllCreditSalesRequests-->");	
		BaseDTO baseDTO = recoveryCollectionDetailService.getAllCreditSalesRequests();
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	//to get Customer list fromDemandDetails table based on creditSalesRequestId
	@GetMapping(value="/getallcustomers/{creditSalesRequestId}")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO>  getAllCustomerMastersByCreditSalesId(@PathVariable("creditSalesRequestId") Long creditSalesRequestId) {
		log.info("<--Starts CreditSalesRecoveryCollectionController .getAllCustomerMastersByCreditSalesId-->");	
		BaseDTO baseDTO = recoveryCollectionDetailService.getAllCustomerMasters(creditSalesRequestId);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	//fetch recoveryCollectionDetails from demandDetail and demand table
	//based on creditSalesRequst and organization or customer in recovery create page
	@PostMapping(value="/search/recoverydetails")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO>  searchRecoveryDetails(@RequestBody RecoveryCollectionSearchRequestDto request) {
		log.info("<--Starts CreditSalesRecoveryCollectionController .searchRecoveryDetails-->"+request);	
		BaseDTO baseDTO = recoveryCollectionDetailService.searchRecoveryCollectionDetails(request);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	//to save creditSalesRecovery
	@PostMapping(value="/save")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO>  saveRecoveryDetails(@RequestBody CreditSalesRecoveryRequestDTO request) {
		log.info("<--Starts CreditSalesRecoveryCollectionController .getAllCustomerMastersByCreditSalesId-->"+request);	
		BaseDTO baseDTO = recoveryCollectionDetailService.saveCreditRecovery(request);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	
	//list page lazy load
	@PostMapping(value="/getallcreditsalesrecoverylistlazy")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO>  creditSalesDemandListLazy(@RequestBody CreditSalesRecoveryDTO request) {
		log.info("<--Starts CreditSalesRecoveryCollectionController .creditSalesDemandListLazy-->");	
		BaseDTO baseDTO = recoveryCollectionDetailService.searchCollectionRecovery(request);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	
	//for view  to get recoveryDetails from recovery table based on voucherId
	@GetMapping(value="/getrecoverydetailsby/{voucherId}")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO>  getRecoveryCollectionByVoucherId(@PathVariable("voucherId") Long voucherId) {
		log.info("<--Starts CreditSalesRecoveryCollectionController .getRecoveryCollectionByVoucherId-->");	
		BaseDTO baseDTO = recoveryCollectionDetailService.getRecoveryCollectionByVoucherId(voucherId);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	
//Load Month Year and Installment
	@PostMapping(value="/loadMonthYearInstallment")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO>  loadMonthYearInstallment(@RequestBody RecoveryCollectionSearchRequestDto request) {
		log.info("<--Starts CreditSalesRecoveryCollectionController .loadMonthYearInstallment-->"+request);	
		BaseDTO baseDTO = recoveryCollectionDetailService.loadMonthYearInstallment(request);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	
}