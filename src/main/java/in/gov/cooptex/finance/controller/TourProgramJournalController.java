package in.gov.cooptex.finance.controller;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.BankToCashResponseDTO;
import in.gov.cooptex.finance.BankToCashSaveRequestDTO;
import in.gov.cooptex.finance.BanktoCashAddDTO;
import in.gov.cooptex.finance.TourProgramResponseDTO;
import in.gov.cooptex.finance.TourProgramSaveDto;
import in.gov.cooptex.finance.service.BankToCashService;
import in.gov.cooptex.finance.service.TourProgramJournalService;
import in.gov.cooptex.finance.service.TourProgramService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/tourprogramjournal")
@Log4j2
public class TourProgramJournalController {

	@Autowired
	TourProgramJournalService tourProgramJournalService;

	@RequestMapping(value = "/getall", method = RequestMethod.POST)
	public @ResponseBody BaseDTO getTourProgramDetails(@RequestBody PaginationDTO paginationDTO) {
		log.info("<===== Start TourProgramJouranlController.getAll TourProgramList ======>");
		BaseDTO wrapperDto = null;
		try {
			return wrapperDto = tourProgramJournalService.getTourProgramDetails(paginationDTO);
		} catch (Exception ex) {
			log.info("TourProgramJouranl Controller exception while getAll ex:" + ex);
		}
		return wrapperDto;
	}

	@RequestMapping(value = "/getEdit/{id}/{notificationId}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> getEditTourById(@PathVariable("id") Long id,
			@PathVariable("notificationId") Long notificationId) {
		log.info("<===== Start TourProgramJouranlController.getEdit TourProgramList ======>");
		BaseDTO wrapperDto = tourProgramJournalService.getTourEditById(id, notificationId);
		return new ResponseEntity<BaseDTO>(wrapperDto, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/savetourprogram")
	public BaseDTO savetourprogram(@RequestBody TourProgramSaveDto data) {
		log.info("<--Starts TourProgramJouranlController Saveing -->" + data);
		return tourProgramJournalService.saveTourProgramDetails(data);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/getTourProgramDetailList")
	public BaseDTO getTourProgramDetailList(@RequestBody TourProgramSaveDto data) {
		log.info("<--Starts TourProgramJouranlController Saveing -->" + data);
		return tourProgramJournalService.getTourProgramDetailList(data);
	}

	@RequestMapping(value = "/getallemployee", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getAllEmployee() {
		BaseDTO baseDTO = tourProgramJournalService.getAllEmployee();
		log.info("<---Ends TourProgramJouranlController.getAllEmployee()--->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/getallMonthByEmpId/{empId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getAllMonthByEmpId(@PathVariable("empId") Long empId) {
		BaseDTO baseDTO = tourProgramJournalService.getAllMonths(empId);
		log.info("<---Ends TourJournalController.getAllMonths()--->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/approveTourProgram", method = RequestMethod.POST)
	public @ResponseBody BaseDTO approveBankToCash(@RequestBody TourProgramResponseDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			baseDTO = tourProgramJournalService.approveTourProgram(requestDTO);
		} catch (Exception ex) {
			log.info("TourJournalController while approved ex:" + ex);
		}
		return baseDTO;
	}

	@RequestMapping(value = "/rejectTourProgram", method = RequestMethod.POST)
	public @ResponseBody BaseDTO rejectChequeToCash(@RequestBody TourProgramResponseDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			baseDTO = tourProgramJournalService.rejectTourProgram(requestDTO);
		} catch (Exception ex) {
			log.info("TourJournalController while reject ex:" + ex);
		}
		return baseDTO;
	}
}
