package in.gov.cooptex.finance.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.dto.ConcurrentAuditCbDto;
import in.gov.cooptex.finance.dto.ConcurrentAuditCollectionDTO;
import in.gov.cooptex.finance.dto.ConcurrentAuditSalesDueDTO;
import in.gov.cooptex.finance.service.ConcurrentAuditService;
import in.gov.cooptex.operation.model.ConcurrentAudit;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/concurrentAudit")
@Log4j2
public class ConcurrentAuditController {

	@Autowired
	ConcurrentAuditService concurrentAuditService;

	@RequestMapping(value = "/getById/{id}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getById(@PathVariable Long id) {
		log.info("=======START ConcurrentAuditController.getById=====");
		BaseDTO baseDTO = concurrentAuditService.getConcurrentAuditById(id);
		log.info("=======END ConcurrentAuditController.getById=====");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}

	/* This Controller method is used to get data for list and search */
	@RequestMapping(value = "/getlazyload", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<BaseDTO> getLazyLoadDatas(@RequestBody PaginationDTO paginationDTO) {
		log.info("=======START ConcurrentAuditController.getLazyLoadDatas=====");
		BaseDTO baseDTO = concurrentAuditService.getLazyLoadData(paginationDTO);
		log.info("=======END ConcurrentAuditController.getLazyLoadDatas=====");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/saveorupdateConcurrentAudit")
	public ResponseEntity<BaseDTO> saveOrUpdateConcurrentAudit(@RequestBody ConcurrentAudit concurrentAudit) {
		log.info("=======START ConcurrentAuditController.saveorupdateConcurrentAudit=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = concurrentAuditService.saveOrUpdateConcurrentAudit(concurrentAudit);
		log.info("=======END saveorupdateConcurrentAudit.saveOrUpdate=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/saveClosingBalance")
	public ResponseEntity<BaseDTO> saveClosingBalance(@RequestBody ConcurrentAuditCbDto concurrentAuditCbDto) {
		log.info("=======START ConcurrentAuditController.saveClosingBalance=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = concurrentAuditService.saveClosingBalance(concurrentAuditCbDto);
		log.info("=======END saveorupdateConcurrentAudit.saveClosingBalance=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/getKntAccount")
	public ResponseEntity<BaseDTO> getKntAccount(@RequestBody Date dueDate) {
		log.info("=======START ConcurrentAuditController.getKntAccount=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = concurrentAuditService.getKntAccount(dueDate);
		log.info("=======END saveorupdateConcurrentAudit.getKntAccount=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/getFurnitureAndFitting")
	public ResponseEntity<BaseDTO> getFurnitureAndFitting(@RequestBody Date dueDate) {
		log.info("=======START ConcurrentAuditController.getFurnitureAndFitting=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = concurrentAuditService.getFurnitureAndFitting(dueDate);
		log.info("=======END saveorupdateConcurrentAudit.getFurnitureAndFitting=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/getPlantAndMachinery")
	public ResponseEntity<BaseDTO> getPlantAndMachinery(@RequestBody Date dueDate) {
		log.info("=======START ConcurrentAuditController.getPlantAndMachinery=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = concurrentAuditService.getPlantAndMachinery(dueDate);
		log.info("=======END saveorupdateConcurrentAudit.getPlantAndMachinery=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/getModernizationAccount")
	public ResponseEntity<BaseDTO> getModernizationAccount(@RequestBody Date dueDate) {
		log.info("=======START ConcurrentAuditController.getModernizationAccount=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = concurrentAuditService.getModernizationAccount(dueDate);
		log.info("=======END saveorupdateConcurrentAudit.getModernizationAccount=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/getCollectionDetails")
	public ResponseEntity<BaseDTO> getCollectionDetails(@RequestBody Date dueDate) {
		log.info("=======START ConcurrentAuditController.getCollectionDetails=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = concurrentAuditService.getCollectionDetails(dueDate);
		log.info("=======END ConcurrentAuditController.getCollectionDetails=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/saveConcurrentAuditCollection")
	public ResponseEntity<BaseDTO> saveConcurrentAuditCollection(
			@RequestBody ConcurrentAuditCollectionDTO concurrentAuditCollectionDTO) {
		log.info("=======START ConcurrentAuditController.saveConcurrentAuditCollection=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = concurrentAuditService.saveConcurrentAuditCollection(concurrentAuditCollectionDTO);
		log.info("=======END ConcurrentAuditController.saveConcurrentAuditCollection=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/getShowroomWiseYearWiseDue")
	public ResponseEntity<BaseDTO> getShowroomWiseYearWiseDue(
			@RequestBody ConcurrentAuditSalesDueDTO concurrentAuditSalesDueDTO) {
		log.info("=======START ConcurrentAuditController.getShowroomWiseYearWiseDue=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = concurrentAuditService.getShowroomWiseYearWiseDue(concurrentAuditSalesDueDTO);
		log.info("=======END ConcurrentAuditController.getShowroomWiseYearWiseDue=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/saveConcurrentAuditSalesDue")
	public ResponseEntity<BaseDTO> saveConcurrentAuditSalesDue(
			@RequestBody ConcurrentAuditSalesDueDTO concurrentAuditSalesDueDTO) {
		log.info("=======START ConcurrentAuditController.saveConcurrentAuditCollection=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = concurrentAuditService.saveConcurrentAuditSalesDue(concurrentAuditSalesDueDTO);
		log.info("=======END ConcurrentAuditController.saveConcurrentAuditCollection=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, path = "/getCreditSalesDueAsperBankStmt")
	public ResponseEntity<BaseDTO> getCreditSalesDueAsperBankStmt(@RequestBody Date dueDate) {
		log.info("=======START ConcurrentAuditController.getCreditSalesDueAsperBankStmt=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = concurrentAuditService.getCreditSalesDueAsperBankStmt(dueDate);
		log.info("=======END ConcurrentAuditController.getCreditSalesDueAsperBankStmt=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}

}
