package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.finance.service.InvestmentCategoryService;
import in.gov.cooptex.finance.service.InvestmentInstitutionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("${finance.api.url}/investmentinstitution")
@Api(tags = "Investment Institution", value = "Investment Institution")
@Log4j2
public class InvestmentInstitutionController {

	@Autowired
	InvestmentInstitutionService investmentInstitutionService;

	@RequestMapping(value = "/getById/{id}", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getById(@PathVariable Long id) {
		log.info("InvestmentInstitutionController.getById()---- "+id);
		BaseDTO baseDTO = investmentInstitutionService.getById(id);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getInverstmentInstitutionList", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getInverstmentInstitutionList() {
		log.info("InvestmentInstitutionController:getInverstmentInstitutionList--------");
		BaseDTO baseDTO = investmentInstitutionService.getInvestmentInstitutionList();
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
}