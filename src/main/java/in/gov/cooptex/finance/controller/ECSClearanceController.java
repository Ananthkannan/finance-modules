package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.finance.dto.ECSClearanceDTO;
import in.gov.cooptex.finance.service.ECSClearanceService;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping("/ecsClearanceController")
public class ECSClearanceController {

	@Autowired
	ECSClearanceService eCSClearanceService;

	@RequestMapping(value = "/getAllECSClearance", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> getAllECSClearance(@RequestBody ECSClearanceDTO eCSClearanceDTO) {
		BaseDTO baseDTO = new BaseDTO();
		log.info("<---ECSClearanceController() loadLazy ---> ");
		baseDTO = eCSClearanceService.getAllECSClearance(eCSClearanceDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/updateECSClearanceDetails", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> updatePaymentChequeDetails(@RequestBody ECSClearanceDTO paymentDetails) {
		BaseDTO baseDTO = new BaseDTO();
		log.info("<---ECSClearanceController() updateECSClearanceDetails ---> ");
		baseDTO = eCSClearanceService.updateECSClearanceDetails(paymentDetails);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/viewECSClearanceDetails", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> viewECSClearanceDetails(@RequestBody ECSClearanceDTO eCSClearanceDTO) {
		BaseDTO baseDTO = new BaseDTO();
		log.info("<---ECSClearanceController() updateECSClearanceDetails ---> ");
		baseDTO = eCSClearanceService.viewECSClearanceDetails(eCSClearanceDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/splitEcsReceipt", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> splitEcsReceipt(@RequestBody ECSClearanceDTO eCSClearanceDTO) {
		BaseDTO baseDTO = new BaseDTO();
		log.info("<---ECSClearanceController() splitEcsReceipt ---> ");
		baseDTO = eCSClearanceService.splitEcsReceipt(eCSClearanceDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

}
