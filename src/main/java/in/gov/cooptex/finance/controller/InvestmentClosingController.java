package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.accounts.dto.InvestmentDTO;
import in.gov.cooptex.core.accounts.model.InvestmentClosing;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.service.InvestmentClosingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("${finance.api.url}/investmentclosing")
@Api(tags = "Investment Closing", value = "Investment Closing")
@Log4j2
public class InvestmentClosingController {

	@Autowired
	InvestmentClosingService investmentClosingService;

	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getById(@PathVariable Long id) {
		log.info("InvestmentClosingController:get() [" + id + "]");
		BaseDTO baseDTO = investmentClosingService.getById(id);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> createInvestmentClosing(@RequestBody InvestmentDTO investmentDTO) {
		log.info("InvestmentClosingController create method start-----");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = investmentClosingService.createInvestmentClosing(investmentDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/lazyInvestmentClosing", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> lazyInvestmentClosing(@RequestBody PaginationDTO pagination) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = investmentClosingService.lazyInvestmentClosing(pagination);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/loadReferenceNoList/{categoryId}/{typeId}/{branchId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> loadReferenceNoList(@PathVariable("categoryId") Long categoryId,
			@PathVariable("typeId") Long typeId, @PathVariable("branchId") Long branchId) {
		log.info("InvestmentClosingController loadReferenceNoList----");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = investmentClosingService.loadReferenceNoList(categoryId,typeId,branchId);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/referenceNoByInstitution/{categoryId}/{typeId}/{institutionId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> referenceNoByInstitution(@PathVariable("categoryId") Long categoryId,
			@PathVariable("typeId") Long typeId, @PathVariable("institutionId") Long institutionId) {
		log.info("InvestmentClosingController loadReferenceNoList----");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = investmentClosingService.referenceNoByInstitution(categoryId,typeId,institutionId);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/checkInvestment/{investmentId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> checkInvestment(@PathVariable("investmentId") Long investmentId) {
		log.info("InvestmentClosingController loadReferenceNoList----");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = investmentClosingService.checkInvestment(investmentId);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
}