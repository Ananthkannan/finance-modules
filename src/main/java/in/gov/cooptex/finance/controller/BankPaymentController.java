package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.finance.dto.BankPaymentDTO;
import in.gov.cooptex.finance.service.BankPaymentService;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping("${finance.api.url}/accounts/bankPaymentController")
public class BankPaymentController {

	@Autowired
	BankPaymentService bankPaymentService;
	
	@RequestMapping(value = "/loadListofVoucherList", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> getListofVoucherList() {
		log.info("<---BankPaymentController() Received getListofVoucherTypeList  ---> ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = bankPaymentService.loadListofVoucherList();
		if (baseDTO != null && baseDTO.getStatusCode()==0 ) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/searchDataLazy", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> lazySearch(@RequestBody BankPaymentDTO bankPaymentDTO){
		BaseDTO baseDTO = new BaseDTO();
		log.info("<---BankPaymentController() loadLazy ---> ");
		baseDTO = bankPaymentService.getAdvancePaymentLazy(bankPaymentDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/searchDataLazySecondTable", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> lazySearchSecond(@RequestBody BankPaymentDTO bankPaymentDTO){
		BaseDTO baseDTO = new BaseDTO();
		log.info("<---BankPaymentController() getBankPaymentLazy SecondList Called---> ");
		baseDTO = bankPaymentService.getBankPaymentLazyList(bankPaymentDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getBankMaterByUserEntityID/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> getBankMaterByUserEntityID(@PathVariable("id") Long id) {
		log.info("<---BankPaymentController() Received getBankMaterByUserEntityID  ---> ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = bankPaymentService.loadBankMaterByUserEntityID(id);
		if (baseDTO != null && baseDTO.getStatusCode()==0 ) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	@RequestMapping(value = "/loadEntityBankBranchAccountDetails/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> loadEntityBankBranchAccountDetails(@PathVariable("id") Long id) {
		log.info("<---BankPaymentController() Received loadEntityBankBranchAccountDetails  ---> ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = bankPaymentService.loadEntityBankBranchAccountDetails(id);
		if (baseDTO != null && baseDTO.getStatusCode()==0 ) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/createBankPayment", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> createBankPayment(@RequestBody BankPaymentDTO bankPaymentDTO){
		BaseDTO baseDTO = new BaseDTO();
		log.info("<---BankPaymentController() create ---> ");
		baseDTO = bankPaymentService.create(bankPaymentDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getChequeBookListByBranchID/{entityID}/{branchID}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> getChequeBookListByBranchID(@PathVariable("entityID") Long entityID,
			@PathVariable("branchID") Long branchID) {
		log.info("<---BankPaymentController() Received getChequeBookListByBranchID  ---> ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = bankPaymentService.getChequeBookListByBranchID(entityID, branchID);
		if (baseDTO != null && baseDTO.getStatusCode()==0 ) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	@RequestMapping(value = "/viewBankPayment/{voucherID}/{entityID}/{branchID}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> viewBankPayment(@PathVariable("voucherID") Long voucherID,@PathVariable("entityID") Long entityID,
			@PathVariable("branchID") Long branchID) {
		log.info("<---BankPaymentController() Received viewBankPayment  ---> ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = bankPaymentService.getBankPaymentView(voucherID,entityID,branchID);
		if (baseDTO != null && baseDTO.getStatusCode()==0 ) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/editBankPayment/{voucherID}/{entityID}/{branchID}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> editBankPayment(@PathVariable("voucherID") Long voucherID,@PathVariable("entityID") Long entityID,
			@PathVariable("branchID") Long branchID) {
		log.info("<---BankPaymentController() Received viewBankPayment  ---> ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = bankPaymentService.getBankPaymenteEDIT(voucherID,entityID,branchID);
		if (baseDTO != null && baseDTO.getStatusCode()==0 ) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/udpateBankPayment", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> udpateBankPayment(@RequestBody BankPaymentDTO bankPaymentDTO){
		BaseDTO baseDTO = new BaseDTO();
		log.info("<---BankPaymentController() update---> ");
		baseDTO = bankPaymentService.updateBankPayment(bankPaymentDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

}
