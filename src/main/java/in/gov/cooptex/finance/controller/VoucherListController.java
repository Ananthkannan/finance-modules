/**
 * 
 */
package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.service.VoucherListService;
import lombok.extern.log4j.Log4j2;

/**
 * @author user
 *
 */

@Log4j2
@RestController
@RequestMapping("/voucherList")
public class VoucherListController {
	
	@Autowired
	VoucherListService voucherListService;
	
	
	@PostMapping(value="/getAllVoucherList")
	public BaseDTO getAllVoucherListLazy(@RequestBody PaginationDTO paginationDto) {
		log.info("<-Inside VoucherListController starts getAllVoucherListLazy method->");	
		return voucherListService.fetchAllVoucherListLazy(paginationDto);
	}
	
	
	@RequestMapping(value="/getById/{id}", method=RequestMethod.GET)
	public ResponseEntity<BaseDTO> getVoucherDeatailsById(@PathVariable Long id){
		log.info("<--- VoucherListController getVoucherDeatailsById Started ---> ");
		BaseDTO baseDTO = voucherListService.getVoucherDeatailsById(id);
		log.info(baseDTO.getResponseContent());
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	

}
