package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.accounts.service.GlAccountHeadService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.dto.PettyCashExpenseDTO;
import in.gov.cooptex.finance.service.PettyCashExpenseService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("${finance.api.url}/pettycashexpense")
@Log4j2
public class PettyCashExpenseController {

	@Autowired
	PettyCashExpenseService pettyCashExpenseService;
	
	@Autowired
	GlAccountHeadService glAccountHeadService;
	
	

	@RequestMapping(value = "/getAllExpenseTypes", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getAllExpenseTypes() {
		log.info("PettyCashExpenseController:getAllExpenseTypes()");
		BaseDTO baseDTO = pettyCashExpenseService.getAllExpenseTypes();
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/getSectionDetails/{empCode}", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getSectionDetails(@PathVariable("empCode") String empCode) {
		log.info("PettyCashExpenseController:getSectionDetails()");
		BaseDTO baseDTO = pettyCashExpenseService.getSectionDetails(empCode);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> createPettyCashExpense(@RequestBody PettyCashExpenseDTO pettyCashExpenseDTO) {
		log.info("PettyCashExpenseController:createPettyCashExpense()");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = pettyCashExpenseService.createPettyCashExpense(pettyCashExpenseDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/loadlazylist", method = RequestMethod.POST)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> search(@RequestBody PaginationDTO paginationDTO) {
		log.info("PettyCashExpenseController:search()");
		BaseDTO baseDTO = pettyCashExpenseService.search(paginationDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/getDetailsById/{id}/{notificationid}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> getPettyCashExpenseDetailsById(@PathVariable("id") Long id,@PathVariable("notificationid") Long notificationId) {
		log.info("inside controller getDetailsById method----------------->");
		BaseDTO baseDTO = pettyCashExpenseService.getPettyCashExpenseDetailsById(id,notificationId);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);

	}

	@RequestMapping(value = "/approvePettyCashExpensePayment", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> approvePettyCashExpensePayment(
			@RequestBody PettyCashExpenseDTO pettyCashExpenseDTO) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = pettyCashExpenseService.approvePettyCashExpensePayment(pettyCashExpenseDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/rejectPettyCashExpensePayment", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> rejectPettyCashExpensePayment(@RequestBody PettyCashExpenseDTO pettyCashExpenseDTO) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = pettyCashExpenseService.rejectPettyCashExpensePayment(pettyCashExpenseDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/getAllExpenseReason", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getAllExpenseReason() {
		log.info("PettyCashExpenseController:getAllExpenseReason()");
		BaseDTO baseDTO = pettyCashExpenseService.getAllExpenseReason();
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/getAllPurchaseSalesAdvancebyEmpId/{empId}", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getAllPurchaseSalesAdvancebyEmpId(@PathVariable("empId") Long employeeId) {
		log.info("<<::::PettyCashExpenseController:getAllPurchaseSalesAdvancebyEmpId()::::>>" + employeeId);
		BaseDTO baseDTO = pettyCashExpenseService.getAllPurchaseSalesAdvancebyEmpId(employeeId);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/getglaccountlistbycategorycode/{categorycode}")
	public ResponseEntity<BaseDTO> getGlAccountListByCategoryCode(@PathVariable String categorycode) {
		log.info("<< GlAccountHeadController.getGlAccountListByCategoryName >>   ######" + categorycode);
		BaseDTO baseDto = glAccountHeadService.getGlAccountListByCategoryCode(categorycode);
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);

	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/getglaccountlistbyautocomplete/{codeName}")
	public ResponseEntity<BaseDTO> getGlaccountListByAutoComplete(@PathVariable String codeName) {
		log.info("<< GlAccountHeadController.getGlaccountListByAutoComplete >>   ######" + codeName);
		BaseDTO baseDto = glAccountHeadService.getGlaccountListByAutoComplete(codeName);
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);

	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/getglaccountlistbycategorycode")
	public ResponseEntity<BaseDTO> getGlAccountListMappedwithGlGroup() {
		log.info("<< GlAccountHeadController.getGlAccountListMappedwithGlGroup >>");
		BaseDTO baseDto = glAccountHeadService.getGlAccountListMappedwithGlGroup();
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);

	}

	@RequestMapping(method = RequestMethod.GET, path = "/getglaccountlistbycodetransname/{codeName}/{transactionName}")
	public ResponseEntity<BaseDTO> getGlaccountByCodeAndTransName(@PathVariable String codeName, @PathVariable String transactionName) {
		log.info("<< GlAccountHeadController.getGlaccountByCodeAndTransName >>   ######" + codeName + "Transaction Name: "+ transactionName);
		BaseDTO baseDto = glAccountHeadService.getGlaccountByCodeAndTransName(codeName, transactionName);
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);

	}
}