package in.gov.cooptex.finance.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.finance.dto.DamageGoodsDTO;
import in.gov.cooptex.finance.service.DamageGoodsService;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("${finance.api.url}/damagegoods")
@Log4j2
public class DamagedGoodsController {

	@Autowired
	DamageGoodsService damageGoodsService;
	
	@RequestMapping(value = "/generate", method = RequestMethod.POST)
	public BaseDTO generateRegularDamageGoods(@RequestBody DamageGoodsDTO damageGoodsDTO) {
		log.info("DamagedGoodsController generateRegularDamageGoods starts---");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = damageGoodsService.generateRegularToDamageGoods(damageGoodsDTO);
		log.info("DamagedGoodsController generateRegularDamageGoods ends---");
		return baseDTO;
	}
	
	@RequestMapping(value = "/movetodamage", method = RequestMethod.POST)
	public BaseDTO damageGoodsSave(@RequestBody List<Long> inventoryIdList) {
		log.info("DamagedGoodsController damageGoodsSave starts---");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = damageGoodsService.moveToDamageGoods(inventoryIdList);
		log.info("DamagedGoodsController damageGoodsSave ends---");
		return baseDTO;
	}
}
