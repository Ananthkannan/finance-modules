package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.finance.FlatCardToBankDTO;
import in.gov.cooptex.finance.service.CardToBankService;
import io.swagger.annotations.Api;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/cardtobank")
@Api(tags = "card bank", value = "card bank")
@Log4j2
public class CardToBankController {

	@Autowired
	CardToBankService cardToBankService;
	

	@RequestMapping(value = "/getCardDetails/{id}/{fromdate}/{todate}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> getCardDetails(@PathVariable("id") Long entityCode,
			@PathVariable("fromdate") String fromDate, @PathVariable("todate") String toDate) {
		log.info("<-- Starts CardToBankController getCardDetails Method -->" + entityCode + "Parameter 2 : " + fromDate
				+ " parameter 3 : " + toDate);
		BaseDTO wrapperDto = cardToBankService.getCardDetails(entityCode, fromDate, toDate);
		return new ResponseEntity<BaseDTO>(wrapperDto, HttpStatus.OK);
	}

	// @RequestMapping(method = RequestMethod.GET,value =
	// "/getUpdateStatus/{selectedId}/{status}")
	// public @ResponseBody BaseDTO getUpdateStatus(@PathVariable("selectedId")
	// String selectedId,@PathVariable("status") String status)
	// {
	// log.info("<-- Starts CardToBankController getUpdateStatus Method -->"
	// +"Parameter 1 : "+selectedId+" Parameter 2 : "+status);
	// return cardToBankService.updateDepositedStatus(selectedId,status);
	// }

	@RequestMapping(value = "/getUpdateStatus", method = RequestMethod.POST)
	public @ResponseBody BaseDTO getUpdateStatus(@RequestBody FlatCardToBankDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			baseDTO = cardToBankService.updateDepositedStatus(requestDTO);
		} catch (Exception ex) {
			log.info("Cheque to Bank Transfer Controller ex:" + ex);
		}
		return baseDTO;
	}

	@RequestMapping(value = "/getCardDetailsByRegionCode/{regionId}/{fromdate}/{todate}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> getCardDetailsByRegionCode(@PathVariable("regionId") Long regionId,
			@PathVariable("fromdate") String fromDate, @PathVariable("todate") String toDate) {
		log.info("<-- Starts CardToBankController getCardDetailsByRegionCode Method -->" + regionId + "Parameter 2 : "
				+ fromDate + " parameter 3 : " + toDate);
		BaseDTO wrapperDto = cardToBankService.getCardDetailsByRegionCode(regionId, fromDate, toDate);
		return new ResponseEntity<BaseDTO>(wrapperDto, HttpStatus.OK);
	}

	@RequestMapping(value = "/getcardtobankdetails/{cardToBankId}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> getCardToBankDetails(@PathVariable("cardToBankId") Long cardToBankId) {
		log.info("CardToBankController. getCardToBankDetails()" + cardToBankId);
		BaseDTO response = cardToBankService.getCardToBankDetails(cardToBankId);
		return new ResponseEntity<BaseDTO>(response, HttpStatus.OK);
	}

}
