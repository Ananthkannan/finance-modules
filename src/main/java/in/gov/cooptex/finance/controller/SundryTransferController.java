package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.finance.dto.SundryTransferDTO;
import in.gov.cooptex.finance.service.SundryTransferService;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping("/sundryTransfer")
public class SundryTransferController {

	@Autowired
	private SundryTransferService sundryTransferService;
	

	@RequestMapping(value = "/loadsoundrydata", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> loadSundryData(@RequestBody SundryTransferDTO request) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = sundryTransferService.loadAllSundryData(request);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/loadsoundryadjustmentdetails", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> loadSundryAdjustmentDetails(@RequestBody SundryTransferDTO request) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = sundryTransferService.adjustment(request);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/deleteadjustmentdetails", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> deleteAdjustmentDetails(@RequestBody SundryTransferDTO request) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = sundryTransferService.deleteAdjustmentDetails(request);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/loadjournalentriesdata", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> loadJournalEntriesData(@RequestBody SundryTransferDTO request) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = sundryTransferService.loadJournalEntriesData(request);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> save(@RequestBody SundryTransferDTO sundryTransferDTO) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO=sundryTransferService.saveSundryAdjustment(sundryTransferDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/getheadcodename/{headcodename}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getHeadCodeName(@PathVariable String headcodename) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO=sundryTransferService.getGlaccountListByAutoComplete(headcodename);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getheadcodebyid/{id}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getHeadCodeName(@PathVariable Long id) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO=sundryTransferService.getHeadCodeandNameById(id);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

}
