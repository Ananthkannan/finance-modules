package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.finance.dto.NorStatementDTO;
import in.gov.cooptex.finance.service.NorStatementService;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/norstatement")
@Log4j2
public class NorStatementController {

	@Autowired
	NorStatementService norStatementService;
	
	@RequestMapping(value= "/generatenorstatement", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> generateNorStatement(@RequestBody NorStatementDTO norStatementDTO) {
		log.info("NorStatementController. generateNorStatement()");
		BaseDTO baseDTO = null;
		baseDTO = norStatementService.generateNorStatement(norStatementDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value= "/getnorstatementstatus", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> getNorStatementStatus(@RequestBody NorStatementDTO norStatementDTO) {
		log.info("NorStatementController. getNorStatementStatus()");
		BaseDTO baseDTO = null;
		baseDTO = norStatementService.getNorStatementStatus(norStatementDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	
	@RequestMapping(value = "/savenorstatement" , method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> saveNorStatement(@RequestBody NorStatementDTO norStatementDTO)  
	{
		BaseDTO baseDTO = null;
		 
		baseDTO=norStatementService.saveNorStatement(norStatementDTO);
		if(baseDTO!=null)
		{
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);

		}else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/checkwetheralreadysubmitornot" , method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> checkWetherAlreadySubmitorNot(@RequestBody NorStatementDTO norStatementDTO)  
	{
		BaseDTO baseDTO = null;
		 
		baseDTO=norStatementService.checkWetherAlreadySubmitorNot(norStatementDTO);
		if(baseDTO!=null)
		{
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);

		}else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
