package in.gov.cooptex.finance.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.finance.BanktoBankAddDTO;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/amountTransfer")
@Log4j2
public class AmountTransferController {

	// @Autowired
	// AmountTransferService amountTransferService;

	/**
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/BanktoBank/add", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> add(@RequestBody BanktoBankAddDTO banktoBankAddDTO) {
		BaseDTO responseDTO = new BaseDTO();
		try {
			log.info("============== BankBranchMasterController inside addBankBranch() ==============> Start");
			// responseDTO = amountTransferService.addBanktoBankDetails(banktoBankAddDTO);
		} catch (Exception e) {
			log.info("<<==  Exception occured in addBankBranch ==>>");
		}
		log.info("============== BankBranchMasterController inside addBankBranch() ==============> End");
		return new ResponseEntity<BaseDTO>(responseDTO, HttpStatus.CREATED);
	}
}

// @RequestMapping(value = "/CashtoBank/search", method = RequestMethod.POST)
// public ResponseEntity<BaseDTO> searchForCashtoBankAmount() {
// SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
// String parsedDate =
// format.parse(searchDate);log.info("BankBranchMasterController :
// searchForBankBranchMaster()");
// //BaseDTO baseDTO = amountTransferService.searchForCashtoBankAmount();
//
// if (baseDTO != null && baseDTO.getStatusCode() == 0) {
// return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
// } else {
// return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
// }
// }

//
// @RequestMapping(value = "/updateBankBranch", method = RequestMethod.PUT)
// public BaseDTO updateBankBranch(@RequestBody BankBranchMaster
// bankBranchMaster, HttpServletRequest request) {
// BaseDTO responseDTO = new BaseDTO();
// try {
// log.info("============== BankBranchMasterController inside updateBankBranch()
// ==============> Start");
// responseDTO = bankBranchMasterService.updateBankBranch(bankBranchMaster);
// } catch (Exception e) {
// log.info("<<== Exception occured in updateBankBranch ==>>");
// }
// log.info("============== BankBranchMasterController inside updateBankBranch()
// ==============> End");
// return responseDTO;
// }
//
// @RequestMapping(value = "/deleteBankBranch", method = RequestMethod.POST)
// public ResponseEntity<BaseDTO> deleteBankBranch(@RequestBody BankBranchMaster
// bankBranchMaster) {
// log.info("============== BankBranchMasterController inside deleteBankBranch()
// ==============> Start");
// BaseDTO baseDto = null;
// try {
// log.info("Branch code" + bankBranchMaster.getBranchCode());
// baseDto = bankBranchMasterService.deleteBankBranch(bankBranchMaster);
// log.info("controller " + baseDto);
// } catch (ConstraintViolationException e) {
// log.fatal("ConstraintViolationException >>>> " + e.toString());
// }
// log.info("============== BankBranchMasterController inside deleteBankBranch()
// ==============> end");
// return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
// }
//
// /**
// * @return
// */
// @RequestMapping(value = "/getAllBankBranchs", method = RequestMethod.GET)
// public ResponseEntity<BaseDTO> getAllBankBranchs() {
// BaseDTO responseDTO = new BaseDTO();
// try {
// log.info("============== BankBranchMasterController inside
// getAllBankBranchs() ==============> Start");
// responseDTO = bankBranchMasterService.getAllBankBranchs();
// } catch (Exception e) {
// log.info("<<== Exception occured in getAllBankBranchs ==>>");
// }
// log.info("============== BankBranchMasterController inside
// getAllBankBranchs() ==============> End");
// if (responseDTO != null) {
// return new ResponseEntity<BaseDTO>(responseDTO, HttpStatus.FOUND);
// } else {
// return new ResponseEntity<BaseDTO>(responseDTO, HttpStatus.NO_CONTENT);
// }
// }
//
// @RequestMapping(value = "/getbybank/{bankId}", method = RequestMethod.GET)
// public BaseDTO getBranchByBank(@PathVariable("bankId") Long bankId) {
// BaseDTO responseDTO = new BaseDTO();
// try {
// log.info("============== BankBranchMasterController inside getBranchByBank()
// ==============> Start");
// responseDTO = bankBranchMasterService.getAllBankBranchsByBank(bankId);
// } catch (Exception e) {
// log.info("<<== Exception occured in getBranchByBank ==>>");
// }
// log.info("============== BankBranchMasterController inside getBranchByBank()
// ==============> End");
// return responseDTO;
// }
//
// @RequestMapping(value = "/getbybranchid/{id}", method = RequestMethod.GET)
// public BaseDTO getById(@PathVariable("id") Long getbybranchid) {
// BaseDTO responseDTO = new BaseDTO();
// try {
// log.info("============== BankBranchMasterController inside getById()
// ==============> Start");
// responseDTO = bankBranchMasterService.getById(getbybranchid);
// } catch (Exception e) {
// log.info("<<== Exception occured in getBranchByBank ==>>");
// }
// log.info("============== BankBranchMasterController inside getById()
// ==============> End");
// return responseDTO;
// }
//
// @RequestMapping(value = "/search", method = RequestMethod.POST)
// public ResponseEntity<BaseDTO> searchForBankBranchMaster(@RequestBody
// BankBranchMasterDTO request) {
// log.info("BankBranchMasterController : searchForBankBranchMaster() [" +
// request + "]");
// BaseDTO baseDTO = bankBranchMasterService.searchForBankBranch(request);
//
// if (baseDTO != null && baseDTO.getStatusCode() == 0) {
// return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
// } else {
// return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
// }
// }
// }
