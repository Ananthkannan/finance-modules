package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.accounts.model.UnitInspectionEntity;
import in.gov.cooptex.core.accounts.model.UnitInspectionOthers;
import in.gov.cooptex.core.accounts.model.UnitInspectionStockArrangement;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.dto.UnitInspectionRequestDto;
import in.gov.cooptex.finance.service.UnitInspectionService;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/unitInspection")
@Log4j2
public class UnitInspectionController {
	
	@Autowired
	UnitInspectionService unitInspectionService;
	
	/** This method search the employee with the given input */
	@RequestMapping(value = "/getAutoCompleteEmployee/{employeeSearchName}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getEmployeeByEmpName(@PathVariable String employeeSearchName) {
		log.info("<--- Starts UnitInspectionController.getEmployeeByEmpName() --->" + employeeSearchName);
		BaseDTO baseDTO = unitInspectionService.getEmployeeByAutoComplete(employeeSearchName);
		log.info("<---Ends UnitInspectionController.getEmployeeByEmpName()--->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.PUT, path = "/saveorupdateinspectiondetails")
	public ResponseEntity<BaseDTO> saveOrUpdateInspectionDetails(@RequestBody UnitInspectionEntity unitInspectionEntity) {
		log.info("=======START UnitInspectionController.saveOrUpdateInspectionDetails=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = unitInspectionService.saveOrUpdateUnitInspectionEntity(unitInspectionEntity);
		log.info("=======END UnitInspectionController.saveOrUpdateInspectionDetails=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getallactiveregions", method = RequestMethod.GET)
	public BaseDTO getAllActiveRegions() {
		log.info("=======START UnitInspectionController.getAllActiveRegions=====");
		return unitInspectionService.getAllActiveRegions();
		
	}
	
	@RequestMapping(value = "/getallshowroomforregion/{regionId}", method = RequestMethod.GET)
	public BaseDTO getAllShowroomForRegion(@PathVariable("regionId") Long regionId) {
		log.info("=======START UnitInspectionController.getAllShowroomForRegion=====");
		return unitInspectionService.getAllShowroomForRegion(regionId);
		
	}
	
	@RequestMapping(value = "/getmanagerandaddrbyshowroomid/{showroomId}", method = RequestMethod.GET)
	public BaseDTO getManagerAndAddrByShowroomId(@PathVariable("showroomId") Long showroomId) {
		log.info("=======START UnitInspectionController.getAllShowroomForRegion=====");
		return unitInspectionService.getManagerAndAddrByShowroomId(showroomId);
		
	}
	
	
	/** This method search the employee with the given input */
	@RequestMapping(value = "/getAutoCompleteProductVariety/{productName}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getProductVarietyName(@PathVariable String productName) {
		log.info("<--- Starts UnitInspectionController.getProductVarietyName() --->" + productName);
		BaseDTO baseDTO = unitInspectionService.getProductVarietyNameAutoComplete(productName);
		log.info("<---Ends UnitInspectionController.getProductVarietyName()--->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.PUT, path = "/saveorupdateStockArrangeMents")
	public ResponseEntity<BaseDTO> saveOrUpdateUnitInspectionStockArrangements(@RequestBody UnitInspectionStockArrangement unitInspectionStockArrangement) {
		log.info("=======START UnitInspectionController.saveOrUpdateUnitInspectionStockArrangements=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = unitInspectionService.saveOrUpdateUnitInspectionStockArrangement(unitInspectionStockArrangement);
		log.info("=======END UnitInspectionController.saveOrUpdateUnitInspectionStockArrangements=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}
	
	
	@RequestMapping(method = RequestMethod.POST, path = "/saveorupdateStockSales")
	public ResponseEntity<BaseDTO> saveOrUpdateStockSales(@RequestBody UnitInspectionRequestDto unitInspectionRequestDto) {
		log.info("=======START UnitInspectionController.saveOrUpdateAuditSales=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = unitInspectionService.saveOrUpdateUnitInspectionSales(unitInspectionRequestDto);
		log.info("=======END UnitInspectionController.saveOrUpdateAuditSales=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.PUT, path = "/saveorupdateotherunitinspectiondtls")
	public ResponseEntity<BaseDTO> saveOrUpdateOtherUnitInspectiondtls(@RequestBody UnitInspectionOthers unitInspectionOthers) {
		log.info("=======START UnitInspectionController.saveOrUpdateOtherUnitInspectiondtls=====");
		BaseDTO baseDto = new BaseDTO();
		baseDto = unitInspectionService.saveOrUpdateOtherUnitInspection(unitInspectionOthers);
		log.info("=======END UnitInspectionController.saveOrUpdateOtherUnitInspectiondtls=====");
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getAllBuildType", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getAllBuildTypeData() {
		log.info("=======START UnitInspectionController.getAllBuildTypeData=====");
		BaseDTO	baseDTO = unitInspectionService.getAllBuildType();
		log.info("=======END UnitInspectionController.getAllBuildTypeData=====");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	/*This Controller method is used to get data for list and search*/
	@RequestMapping(value = "/getlazyload", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<BaseDTO> getLazyLoadDatas(@RequestBody PaginationDTO paginationDTO) {
		log.info("=======START UnitInspectionController.getLazyLoadDatas=====");
		BaseDTO baseDTO = unitInspectionService.getLazyLoadData(paginationDTO);
		log.info("=======END UnitInspectionController.getLazyLoadDatas=====");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getById/{id}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getById(@PathVariable Long id) {
		log.info("=======START UnitInspectionController.getById=====");
		BaseDTO	baseDTO = unitInspectionService.getUnitInspectionById(id);
		log.info("=======END UnitInspectionController.getById=====");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/getfastslowmovingproductsbyshowroomid/{showroomId}", method = RequestMethod.GET)
	public BaseDTO getFastAndSlowMovingProducts(@PathVariable("showroomId") Long showroomId) {
		log.info("=======START UnitInspectionController.getFastAndSlowMovingProducts=====");
		return unitInspectionService.getFastAndSlowMovingProducts(showroomId);
		
	}
	

}
