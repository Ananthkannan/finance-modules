/**
 * 
 */
package in.gov.cooptex.finance.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.accounts.model.ChequeBook;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.QuotationDTO;
import in.gov.cooptex.finance.service.ChequeCancellationService;
import lombok.extern.log4j.Log4j2;

/**
 * @author Preethi
 *
 */

@Log4j2
@RestController
@RequestMapping("/chequeCancellation")
public class ChequeCancellationController {
	
	@Autowired
	ChequeCancellationService chequeCancellationService;

	@RequestMapping(value = "/getAllBranch", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> bankBranchNameList() {
		log.info("<-Inside CONTROLLER starts bankBranchNameList method->");
		BaseDTO	baseDTO=chequeCancellationService.bankBranchNameList();
		log.info("<-Ends ChequeCancellationController.bankBranchNameList()->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getAllAccountNumber/{bankBranchMasterId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> accountNumberList(@PathVariable Long bankBranchMasterId) {
		log.info("<-Inside CONTROLLER starts accountNumberList method->");
		BaseDTO	baseDTO=chequeCancellationService.accountNumberList(bankBranchMasterId);
		log.info("<-Ends ChequeCancellationController.accountNumberList()->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getAllChequeBookNumber/{entityBankBranchId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> chequeBookNumberList(@PathVariable Long entityBankBranchId) {
		log.info("<-Inside CONTROLLER starts chequeBookNumberList method->");
		BaseDTO	baseDTO=chequeCancellationService.chequeBookNumberList(entityBankBranchId);
		log.info("<-Ends ChequeCancellationController.chequeBookNumberList()->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/getAllChequeDetails/{chequeBookNumber}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> chequeBookDetails(@PathVariable String chequeBookNumber) {
		log.info("<-Inside CONTROLLER starts chequeBookDetails method->");
		BaseDTO	baseDTO=chequeCancellationService.chequeBookDetails(chequeBookNumber);
		log.info("<-Ends ChequeCancellationController.chequeBookDetails()->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/cancelUnpaidCheque", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> updateChequeBook(@RequestBody List<ChequeBook> selectedChequeBook){
		log.info("<-Inside CONTROLLER starts updateChequeBook method to update unpaid cheque as cancelled cheque->");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = chequeCancellationService.updateChequeBook(selectedChequeBook);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	

}
