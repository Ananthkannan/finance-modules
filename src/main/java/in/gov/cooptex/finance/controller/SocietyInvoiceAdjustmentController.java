package in.gov.cooptex.finance.controller;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.PurchaseInvoiceAdjustment;
import in.gov.cooptex.finance.SocietyDropDownRequestDTO;
import in.gov.cooptex.finance.dto.SearchSocietyInvoiceAdjustmentDTO;
import in.gov.cooptex.finance.service.SocietyInvoiceAdjustmentService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/societytranscation/adjustment")
@Log4j2
public class SocietyInvoiceAdjustmentController { 

    @Autowired
    SocietyInvoiceAdjustmentService societyInvoiceAdjustmentService;

    @ApiOperation(value = "PurchaseInvoiceList Get ALL Service",response = Iterable.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "All PurchaseInvoiceList Get Successfully Retrived"),
            @ApiResponse(code = 401, message = "You are not authorized to Get All PurchaseInvoiceList"),
            @ApiResponse(code = 403, message = "Getting of All PurchaseInvoiceList is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "Internal Error Occured while Getting All PurchaseInvoiceList") })
    @RequestMapping(value = "/getpurchaseinvoicelist", method = RequestMethod.POST)
    public ResponseEntity<BaseDTO> getPurchaseInvoiceList(@RequestBody PaginationDTO paginationDTO) {
        log.info("<--Starts SocietyInvoiceAdjustmentController .getSocietyAdjustmentList-->");
        BaseDTO baseDTO = societyInvoiceAdjustmentService.getPurchaseInvoiceList(paginationDTO);
        log.info("<--Ends SocietyInvoiceAdjustmentController .getPurchaseInvoiceList-->");
        return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
    }

    @ApiOperation(value = "SocietyInvoiceAdjustment Get ALL Service",response = Iterable.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "All SocietyInvoiceAdjustment Get Successfully Retrived"),
            @ApiResponse(code = 401, message = "You are not authorized to Get All SocietyInvoiceAdjustment"),
            @ApiResponse(code = 403, message = "Getting of All SocietyInvoiceAdjustment is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "Internal Error Occured while Getting All SocietyInvoiceAdjustment") })
    @RequestMapping(value = "/getsocietyinvoiceadjustmentlist", method = RequestMethod.GET)
    public ResponseEntity<BaseDTO> getSocietyAdjustmentList() {
        log.info("<--Starts SocietyInvoiceAdjustmentController .getAll-->");
        BaseDTO baseDTO = societyInvoiceAdjustmentService.getSocietyAdjustmentList();
        log.info("<--Ends SocietyInvoiceAdjustmentController .getSocietyAdjustmentList-->");
        return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
    }

    @ApiOperation(value = "SocietyInvoiceAdjustment Get ALL CircleMasterCodeDropDown",response = Iterable.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "All CircleMasterCodeDropDown Get Successfully Retrived"),
            @ApiResponse(code = 401, message = "You are not authorized to Get All CircleMasterCodeDropDown"),
            @ApiResponse(code = 403, message = "Getting of All CircleMasterCodeDropDown is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "Internal Error Occured while Getting All CircleMasterCodeDropDown") })
    @RequestMapping(value = "/getcirclemastercodedropdown", method = RequestMethod.GET)
    public ResponseEntity<BaseDTO> getCircleMasterCodeDropDown() {
        log.info("<--Starts SocietyInvoiceAdjustmentController .getAll-->");
        BaseDTO baseDTO = societyInvoiceAdjustmentService.getRegionDropdown();
        log.info("<--Ends SocietyInvoiceAdjustmentController .getCircleMasterCodeDropDown-->");
        return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK); 
    }
    
    @RequestMapping(value = "/getbycode/{code}",method=RequestMethod.GET)
	public ResponseEntity<BaseDTO> getByCode(@PathVariable String code){
		log.info("<<=========  get() getByCode==> "+code);
		BaseDTO baseDTO = societyInvoiceAdjustmentService.getByCode(code);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
    
    @RequestMapping(value = "/getsocietybycode/{code}",method=RequestMethod.GET)
   	public ResponseEntity<BaseDTO> getSocietybycode(@PathVariable String code){
   		log.info("<<=========  get() getSocietybycode==> "+code);
   		BaseDTO baseDTO = societyInvoiceAdjustmentService.getSocietybycode(code);
   		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
   	}

    @ApiOperation(value = "SocietyInvoiceAdjustment Get ALL getSocietyCodeDropDown",response = Iterable.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "All getSocietyCodeDropDown Get Successfully Retrived"),
            @ApiResponse(code = 401, message = "You are not authorized to Get All getSocietyCodeDropDown"),
            @ApiResponse(code = 403, message = "Getting of All getSocietyCodeDropDown is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "Internal Error Occured while Getting All getSocietyCodeDropDown") })
    @RequestMapping(value = "/getsocietycodedropdown", method = RequestMethod.POST)
    public ResponseEntity<BaseDTO> getSocietyCodeDropDown(@RequestBody SocietyDropDownRequestDTO circleMasters) {
        log.info("<--Starts SocietyInvoiceAdjustmentController .getSocietyCodeDropDown-->");
        BaseDTO baseDTO = societyInvoiceAdjustmentService.getSocietyDropdown(circleMasters);
        log.info("<--Ends SocietyInvoiceAdjustmentController .getSocietyCodeDropDown-->");
        return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
    }

    @RequestMapping(value = "/getinvoicedetailsbysocietyid", method = RequestMethod.POST)
    public ResponseEntity<BaseDTO> getSocietyInvoiceAdjustmentDataBySocietyId(@RequestBody SearchSocietyInvoiceAdjustmentDTO request) {
        log.info("<--Starts SocietyInvoiceAdjustmentController .getSocietyInvoiceAdjustmentDataBySocietyId-->");
        BaseDTO baseDTO = societyInvoiceAdjustmentService.getSocietyInvoiceAdjustmentDataBySocietyId(request);
        log.info("<--Ends SocietyInvoiceAdjustmentController .getSocietyInvoiceAdjustmentDataBySocietyId-->");
        return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
    }

    @RequestMapping(value = "/getinvoicedetailsbyallsocietyids", method = RequestMethod.POST)
    public ResponseEntity<BaseDTO> getSocietyInvoiceAdjustmentDataByAllSocietyId(@RequestBody SearchSocietyInvoiceAdjustmentDTO request) {
        log.info("<--Starts SocietyInvoiceAdjustmentController .getSocietyInvoiceAdjustmentDataByAllSocietyId-->");
        BaseDTO baseDTO = societyInvoiceAdjustmentService.getSocietyInvoiceAdjustmentDataByAllSocietyId(request);
        log.info("<--Ends SocietyInvoiceAdjustmentController .getSocietyInvoiceAdjustmentDataByAllSocietyId-->");
        return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
    }
    
    @ApiResponses(value = { @ApiResponse(code = 200, message = "SocietyInvoiceAdjustment Get Successfully Retrived"),
            @ApiResponse(code = 401, message = "You are not authorized to Get SocietyInvoiceAdjustment"),
            @ApiResponse(code = 403, message = "Get SocietyInvoiceAdjustment is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "Internal Error Occured while Get SocietyInvoiceAdjustment") })
    @PostMapping(value="/searchsocietyinvoiceadjustment")
    public ResponseEntity<BaseDTO> searchSocietyInvoiceAdjustment(@RequestBody SearchSocietyInvoiceAdjustmentDTO searchDto) {
        log.info("<--Starts SocietyInvoiceAdjustmentController.searchSocietyInvoiceAdjustment-->");
        BaseDTO baseDTO = societyInvoiceAdjustmentService.searchSocietyInvoiceAdjustment(searchDto);
        log.info("<--Ends SocietyInvoiceAdjustmentController .searchSocietyInvoiceAdjustment-->");
        return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
    }

//    @ApiOperation(value = "SocietyInvoiceAdjustment GSocietyInvoiceAdjustmeet ALL Service",response = Iterable.class)
//    @ApiResponses(value = { @ApiResponse(code = 200, message = "All SocietyInvoiceAdjustment Get Successfully Retrived"),
//            @ApiResponse(code = 401, message = "You are not authorized to Get All SocietyInvoiceAdjustment"),
//            @ApiResponse(code = 403, message = "Getting of All SocietyInvoiceAdjustment is forbidden"),
//            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
//            @ApiResponse(code = 500, message = "Internal Error Occured while Getting All SocietyInvoiceAdjustment") })
//    @RequestMapping(value = "/getsocietyadjustmentlistbysocietycode", method = RequestMethod.POST)
//    public ResponseEntity<BaseDTO> getSocietyAdjustmentListBySocietyCode(@RequestBody SearchSocietyInvoiceAdjustmentDTO searchDto) {
//        log.info("<--Starts SocietyInvoiceAdjustmentController .getAll--> ");
//        BaseDTO baseDTO = societyInvoiceAdjustmentService.getSocietyAdjustmentListBySocietyCode(searchDto);
//        log.info("<--Ends SocietyInvoiceAdjustmentController .getSocietyAdjustmentListBySocietyCode-->");
//        return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
//    }

    @ApiResponses(value = { @ApiResponse(code = 200, message = "SocietyInvoiceAdjustment Saved Successfully"),
            @ApiResponse(code = 401, message = "You are not authorized to Save SocietyInvoiceAdjustment"),
            @ApiResponse(code = 403, message = "Save SocietyInvoiceAdjustment is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "Internal Error Occured while Save SocietyInvoiceAdjustment") })
    @PostMapping(value="/saveorupdate")
    public  BaseDTO saveOrUpdate(@RequestBody List<PurchaseInvoiceAdjustment> purchaseInvoiceAdjustmentList) {
        log.info("<--Starts SocietyInvoiceAdjustmentController .saveOrUpdate-->");
        return societyInvoiceAdjustmentService.saveOrUpdate(purchaseInvoiceAdjustmentList);
    }

    @ApiResponses(value = { @ApiResponse(code = 200, message = "SocietyInvoiceAdjustment Get Successfully Retrived"),
            @ApiResponse(code = 401, message = "You are not authorized to Get SocietyInvoiceAdjustment"),
            @ApiResponse(code = 403, message = "Get SocietyInvoiceAdjustment is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "Internal Error Occured while Get SocietyInvoiceAdjustment") })
    @GetMapping(value="/getSocietyWiseAdjustment/{invoiceId}")
    public  BaseDTO getSocietyInvoiceAdjustmentByInvoiceId(@PathVariable Long invoiceId) {
        log.info("<--Starts SocietyInvoiceAdjustmentController .getSocietyInvoiceAdjustmentByInvoiceId-->");
        return societyInvoiceAdjustmentService.getSocietyInvoiceAdjustmentByInvoiceId(invoiceId);
    }
    
    @RequestMapping(value = "/delete/id/{id}", method = RequestMethod.DELETE)
	public BaseDTO deleteSocietyAdjustment(@PathVariable Long id) {
		log.info("<--- Starts CommunalRosterController:search --->");
		BaseDTO	baseDTO = societyInvoiceAdjustmentService.deleteSocietyAdjustment(id);
		return baseDTO;
	}
}

