package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.accounts.dto.PfPaymentDTO;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.service.PfPaymentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("${finance.api.url}/pfpayment")
@Api(tags = "PF Payment", value = "PF Payment")
@Log4j2
public class PfPaymentController {

	@Autowired
	PfPaymentService pfPaymentService;

	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getById(@PathVariable Long id) {
		log.info("PfPaymentController:get() [" + id + "]");
		BaseDTO baseDTO = pfPaymentService.getById(id);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getViewStaffPfDetails",  method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> getStaffPfDetailsById(@RequestBody PfPaymentDTO pfPaymentDTO) {
		log.info("PfPaymentController:getStaffPfDetailsById() ");
		BaseDTO baseDTO = pfPaymentService.getStaffPfDetailsById(pfPaymentDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> createPfPayment(@RequestBody PfPaymentDTO pfPaymentDTO) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = pfPaymentService.createPfPayment(pfPaymentDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getEntityWisePfPaymentDetails", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> getEntityWisePfPaymentDetails(@RequestBody PfPaymentDTO pfPaymentDTO) {
		log.info("PfPaymentController getEntityWisePfPaymentDetails method started ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = pfPaymentService.getEntityWisePfPaymentDetails(pfPaymentDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getRegionWisePfPaymentDetails", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> getRegionWisePfPaymentDetails(@RequestBody PfPaymentDTO pfPaymentDTO) {
		log.info("PfPaymentController getRegionWisePfPaymentDetails method started ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = pfPaymentService.getRegionWisePfPaymentDetails(pfPaymentDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/lazyloadlist", method = RequestMethod.POST)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> lazyloadlist(@RequestBody PaginationDTO paginationDTO) {
		log.info("PfPaymentController:getServiceInvoiceList()");
		BaseDTO baseDTO = pfPaymentService.lazyloadlist(paginationDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/approveStaffPfPayment", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> approveStaffPfPayment(
			@RequestBody PfPaymentDTO pfPaymentDTO) {
		log.info("PfPaymentController:approveStaffPfPayment()");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = pfPaymentService.approveStaffPfPayment(pfPaymentDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/rejectStaffPfPayment", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> rejectStaffPfPayment(
			@RequestBody PfPaymentDTO pfPaymentDTO) {
		log.info("PfPaymentController:rejectStaffPfPayment()");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = pfPaymentService.rejectStaffPfPayment(pfPaymentDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
}