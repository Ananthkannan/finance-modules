package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.accounts.dto.SalesPaymentCollectionDTO;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.service.SalesPaymentCollectionService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/salespaymentcollection")
@Log4j2
public class SalesPaymentCollectionController {

	@Autowired
	SalesPaymentCollectionService salesPaymentCollectionService;
	
	@RequestMapping(value = "/getsalesinvoicedetails", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> getSalesInvoiceDetails(@RequestBody SalesPaymentCollectionDTO salesPaymentCollectionDTO) {
		log.info("<--- Starts SalesPaymentCollectionController .getSalesInvoiceDetails() ---> " + salesPaymentCollectionDTO);
		BaseDTO baseDTO = salesPaymentCollectionService.getSalesInvoiceDetails(salesPaymentCollectionDTO);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	} 
	
	@RequestMapping(value = "/getadvancecollectiondetails", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> getCustomerAdvanceCollectionDetails(@RequestBody SalesPaymentCollectionDTO salesPaymentCollectionDTO) {
		log.info("<--- Starts SalesPaymentCollectionController .getCustomerAdvanceCollectionDetails() ---> " + salesPaymentCollectionDTO);
		BaseDTO baseDTO = salesPaymentCollectionService.getCustomerAdvanceCollectionDetails(salesPaymentCollectionDTO);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	} 
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> create(@RequestBody SalesPaymentCollectionDTO salesPaymentCollectionDTO) {
		log.info("<--- Starts SalesPaymentCollectionController .create() ---> " + salesPaymentCollectionDTO);
		BaseDTO baseDTO = salesPaymentCollectionService.create(salesPaymentCollectionDTO);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> update(@RequestBody SalesPaymentCollectionDTO salesPaymentCollectionDTO) {
		log.info("<--- Starts SalesPaymentCollectionController .update() ---> " + salesPaymentCollectionDTO);
		BaseDTO baseDTO = salesPaymentCollectionService.update(salesPaymentCollectionDTO);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getvoucherdetails", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> getVoucherDetails(@RequestBody SalesPaymentCollectionDTO salesPaymentCollectionDTO) {
		log.info("<--- Starts SalesPaymentCollectionController .getvoucherdetails() ---> " + salesPaymentCollectionDTO);
		BaseDTO baseDTO = salesPaymentCollectionService.getVoucherDetails(salesPaymentCollectionDTO);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	} 
	
	
	@RequestMapping(value = "/loadlazylist", method = RequestMethod.POST)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> search(@RequestBody PaginationDTO paginationDTO) {
		log.info("SalesPaymentCollectionController:search()");
		BaseDTO baseDTO = salesPaymentCollectionService.search(paginationDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getpaymentmethod", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> loadPaymentMethod() {
		log.info("<--- Starts SalesPaymentCollectionController .loadPaymentMethod() ---> ");
		BaseDTO baseDTO = salesPaymentCollectionService.loadPaymentMethod();
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	/**
	 * This method is used for getAllCustomersByType
	 */

	@RequestMapping(value = "/customerlistbynametype/{name}/{custtypeid}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getAllCustomersByNameType(@PathVariable String name ,@PathVariable Long custtypeid) {
		log.info("<--- Starts salesPaymentCollectionService.getAllCustomersByName() --->" + name);
		BaseDTO baseDTO = salesPaymentCollectionService.getAllCustomersByNameType(name,custtypeid);
		log.info("<---Ends salesPaymentCollectionService.getAllCustomersByName()--->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/customerlistbyname/{name}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getAllCustomersByName(@PathVariable String name) {
		log.info("<--- Starts salesPaymentCollectionService.getAllCustomersByName() --->" + name);
		BaseDTO baseDTO = salesPaymentCollectionService.getAllCustomersByName(name);
		log.info("<---Ends salesPaymentCollectionService.getAllCustomersByName()--->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
}
