package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.dto.AdditionalBudgetDTO;
import in.gov.cooptex.finance.service.AdditionalBudgetService;
import io.swagger.annotations.Api;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("${finance.api.url}/additionalbudget")
@Api(tags = "Additional Budget", value = "Additional Budget")
@Log4j2
public class AdditionalBudgetController {

	@Autowired
	AdditionalBudgetService additionalBudgetService;
	
	@GetMapping("/getbudgetconfig/{entityId}")
	public ResponseEntity<BaseDTO> getBudgetConfig(@PathVariable("entityId") Long entityId){
		log.info("AdditionalBudgetController getBudgetConfig starts------"+entityId);
		BaseDTO baseDTO = additionalBudgetService.getBudgetConfig(entityId);
		log.info("AdditionalBudgetController getBudgetConfig ends------");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/generatebudget/{requestId}/{entityId}/{sectionId}/{code}")
	public ResponseEntity<BaseDTO> generateBudget(@PathVariable("requestId") Long requestId,
			@PathVariable("entityId") Long entityId,@PathVariable("sectionId") Long sectionId,
			@PathVariable("code") String budgetCode){
		log.info("AdditionalBudgetController generateBudget starts------"+entityId);
		BaseDTO baseDTO = additionalBudgetService.generateBudget(requestId,entityId,sectionId,budgetCode);
		log.info("AdditionalBudgetController generateBudget ends------");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/saveadditionalbudget")
	public ResponseEntity<BaseDTO> saveAdditionalBudget(@RequestBody AdditionalBudgetDTO additionalBudgetDTO){
		log.info("<=====Starts AdditionalBudgetController.saveAdditionalBudget =======>");
		BaseDTO baseDTO = additionalBudgetService.saveAdditionalBudget(additionalBudgetDTO);	
		log.info("<=====Ends AdditionalBudgetController.saveAdditionalBudget =======>");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/lazyLoadAdditionalBudget", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> lazyLoadAdditionalBudget(@RequestBody PaginationDTO paginationDTO) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = additionalBudgetService.lazyLoadAdditionalBudget(paginationDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/viewAdditionalBudget")
	public ResponseEntity<BaseDTO> viewAdditionalBudget(@RequestBody AdditionalBudgetDTO additionalBudgetDTO){
		log.info("AdditionalBudgetController viewAdditionalBudget starts------"+additionalBudgetDTO);
		BaseDTO baseDTO = additionalBudgetService.viewAdditionalBudget(additionalBudgetDTO);
		log.info("AdditionalBudgetController viewAdditionalBudget ends------");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/approveorreject", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> approveOrReject(@RequestBody AdditionalBudgetDTO additionalBudgetDTO) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = additionalBudgetService.approveOrReject(additionalBudgetDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
}
