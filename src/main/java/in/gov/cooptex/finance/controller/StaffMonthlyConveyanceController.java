package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.accounts.dto.StaffMonthlyConveyanceDTO;
import in.gov.cooptex.core.accounts.dto.StaffMonthlyConveyanceResponseDTO;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.service.StaffMonthlyConveyanceService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/staffMonthlyConveyance")
@Log4j2
public class StaffMonthlyConveyanceController {

	@Autowired
	StaffMonthlyConveyanceService staffMonthlyConveyanceService;

	@GetMapping("/getEmployeeByLocationId/{locationId}")
	@ApiOperation(value = "Payment Mode Find All", response = Iterable.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Payment Mode Find All Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to Fetch Payment Mode"),
			@ApiResponse(code = 403, message = "Fetch Payment Mode is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 500, message = "Internal Error Occured while Fetching Payment Mode") })
	public BaseDTO getEmployeeByLocationId(@PathVariable("locationId") Long locationId) {
		log.info("<===== Start EmployeePersonalInfoEmploymentController.getEmployeeByLocationId ======>");
		BaseDTO baseDto = staffMonthlyConveyanceService.getEmployeeByLocationId(locationId);
		log.info("<===== End EmployeePersonalInfoEmploymentController.getEmployeeByLocationId ======>");
		return baseDto;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> createStaffConveyancePaymen(
			@RequestBody StaffMonthlyConveyanceDTO staffMonthlyConveyanceDTO) {
		log.info("<===== Start StaffMonthlyConveyanceController.createStaffConveyancePaymen ======>");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = staffMonthlyConveyanceService.createStaffConveyancePayment(staffMonthlyConveyanceDTO);
		log.info("<===== End StaffMonthlyConveyanceController.createStaffConveyancePaymen ======>");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/show", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> showStaffConveyancePaymen(
			@RequestBody StaffMonthlyConveyanceDTO staffMonthlyConveyanceDTO) {
		log.info("<===== Start StaffMonthlyConveyanceController.showStaffConveyancePaymen ======>");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = staffMonthlyConveyanceService.showStaffConveyancePayment(staffMonthlyConveyanceDTO);
		log.info("<===== End StaffMonthlyConveyanceController.showStaffConveyancePaymen ======>");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/getLazylist", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<BaseDTO> getStaffMonthlyConveyanceList(@RequestBody PaginationDTO paginationDTO) {
		log.info("<===== Start StaffMonthlyConveyanceController.getStaffMonthlyConveyanceList ======>");
		BaseDTO baseDTO = staffMonthlyConveyanceService.searchLazyList(paginationDTO);
		log.info("<===== Start StaffMonthlyConveyanceController.getStaffMonthlyConveyanceList ======>");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}

	@GetMapping("/getFindStafeConveyance/{voucherId}")
	@ApiOperation(value = "getFindStaffConveyance", response = Iterable.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "v Find All Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to Fetch StaffConveyance"),
			@ApiResponse(code = 403, message = "StaffConveyance is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 500, message = "Internal Error Occured while Fetching StaffConveyance") })
	public BaseDTO getFindStafeConveyance(@PathVariable("voucherId") Long voucherId) {
		log.info("<===== Start StaffMonthlyConveyanceController.getFindStafeConveyance ======>");
		BaseDTO baseDto = staffMonthlyConveyanceService.getStaffConveyanceId(voucherId);
		log.info("<===== End StaffMonthlyConveyanceController.getFindStafeConveyance ======>");
		return baseDto;
	}
	@RequestMapping(value = "/staffConveyanceApprove", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> staffConveyanceApprove(
			@RequestBody StaffMonthlyConveyanceResponseDTO staffMonthlyConveyanceResponseDTO) {
		log.info("<===== Start StaffMonthlyConveyanceController.staffConveyanceApprove ======>");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = staffMonthlyConveyanceService.staffConveyanceApprove(staffMonthlyConveyanceResponseDTO);
		log.info("<===== End StaffMonthlyConveyanceController.staffConveyanceApprove ======>");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
}
