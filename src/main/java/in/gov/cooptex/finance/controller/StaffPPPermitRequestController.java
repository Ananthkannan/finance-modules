package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.finance.dto.StaffPPPermitInfoDTO;
import in.gov.cooptex.finance.dto.StaffPPPermitSearchDTO;
import in.gov.cooptex.finance.dto.StaffPPSalesRequestDTO;
import in.gov.cooptex.finance.service.StaffPPPermitRequestService;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(value="/ppPermitRequest")
@Log4j2	
public class StaffPPPermitRequestController {
	
	@Autowired
	StaffPPPermitRequestService staffPPPermitRequestService;

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> create(@RequestBody StaffPPSalesRequestDTO request) {
		log.info("<<========= StaffPPPermitRequestController ----  create  =====##STARTS");
		BaseDTO baseDTO = staffPPPermitRequestService.create(request);
		log.info("<<========= StaffPPPermitRequestController ----  create  =====##ENDS");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> update(@RequestBody StaffPPSalesRequestDTO request) {
		log.info("<<========= StaffPPPermitRequestController ----  create  =====##STARTS");
		BaseDTO baseDTO = staffPPPermitRequestService.update(request);
		log.info("<<========= StaffPPPermitRequestController ----  create  =====##ENDS");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getByType/{typeId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getByPPType(@PathVariable Long typeId) {
		log.info("<<========= StaffPPPermitRequestController ----  getByPPType  =====##STARTS");
		BaseDTO baseDTO = staffPPPermitRequestService.getByPPType(typeId);
		log.info("<<========= StaffPPPermitRequestController ----  getByPPType  =====##ENDS");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	@RequestMapping(value = "/getAmountInfo/{empId}/{ppTypeName}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> calculateAmountDetails(@PathVariable Long empId,@PathVariable String ppTypeName) {
		log.info("<<========= StaffPPPermitRequestController ----  getByPPType  =====##STARTS");
		BaseDTO baseDTO = staffPPPermitRequestService.calculateAmountDetails(empId,ppTypeName);
		log.info("<<========= StaffPPPermitRequestController ----  getByPPType  =====##ENDS");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> search(@RequestBody StaffPPPermitSearchDTO request) {
		log.info("<<========= StaffPPPermitRequestController ----  search  =====##STARTS");
		BaseDTO baseDTO = staffPPPermitRequestService.search(request);
		log.info("<<========= StaffPPPermitRequestController ----  search  =====##ENDS");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	
	
	
	@RequestMapping(value = "/getsalesRequestDetails", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> calculateAmountDetails(@RequestBody StaffPPPermitSearchDTO requestDTO) {
		log.info("<<========= StaffPPPermitRequestController ----  getByPPType  =====##STARTS");
		BaseDTO baseDTO = staffPPPermitRequestService.calculateAmountDetails(requestDTO);
		log.info("<<========= StaffPPPermitRequestController ----  getByPPType  =====##ENDS");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/ppPermitForwardStage", method = RequestMethod.POST)
	public BaseDTO rentCollectionForwardStage(@RequestBody StaffPPPermitInfoDTO staffPPPermitInfoDTO) {
		log.info("<--Starts StaffPPPermitRequestController .ppPermitForwardStage-->");
		return staffPPPermitRequestService.ppPermitForwardStage(staffPPPermitInfoDTO);
	}
	
	
	@RequestMapping(value = "/getActiveEmployeeNameOrderList", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getActiveEmployeeNameOrderList() {
		BaseDTO baseDTO = staffPPPermitRequestService.getActiveEmployeeNameOrderList();
		log.info("<---Ends StaffPPPermitRequestController.getAllEmployee()--->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/getRetirementEmployeeNameOrderList", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getRetirementEmployeeNameOrderList() {
		BaseDTO baseDTO = staffPPPermitRequestService.getRetirementEmployeeNameOrderList();
		log.info("<---Ends StaffPPPermitRequestController.getAllEmployee()--->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/findempdetailsbyloggedinuserdetails/{userId}", method = RequestMethod.GET)
	public BaseDTO findEmployeeDetailsByLoggedInUserDetails(@PathVariable Long userId) {
		log.info("<--- Starts find Employee Details By LoggedIn User Id :- " + userId);
		BaseDTO baseDTO = staffPPPermitRequestService.findEmployeeDetailsByLoggedInUserDetails(userId);
		return baseDTO;
	}
	
	@RequestMapping(value = "/getvaluemultipleppallowed", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getValueMultiplePPRequestAllowed() {
		BaseDTO baseDTO = staffPPPermitRequestService.getValueMultiplePPRequestAllowed();
		log.info("<---Ends StaffPPPermitRequestController.getValueMultiplePPRequestAllowed()--->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
}
