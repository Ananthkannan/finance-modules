package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.accounts.model.StaffTelephonePayment;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.dto.StaffTelephonePaymentDTO;
import in.gov.cooptex.finance.service.StaffTelephonePaymentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("${finance.api.url}/stafftelephonepayment")
@Api(tags = "Staff Telephone Payment", value = "Staff Telephone Payment")
@Log4j2
public class StaffTelephonePaymentController {

	@Autowired
	StaffTelephonePaymentService staffTelephonePaymentService;

	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getById(@PathVariable Long id) {
		log.info("StaffTelephonePaymentController:get() [" + id + "]");
		BaseDTO baseDTO = staffTelephonePaymentService.getById(id);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/getActiveStaffTelephonePayment", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getActiveStaffTelephonePayment() {
		log.info("StaffTelephonePaymentController:getActiveStaffTelephonePayment()");
		BaseDTO baseDTO = staffTelephonePaymentService.getActiveStaffTelephonePayment();
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> createStaffTelephonePayment(
			@RequestBody StaffTelephonePaymentDTO staffTelephonePaymentDTO) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = staffTelephonePaymentService.createStaffTelephonePayment(staffTelephonePaymentDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/searchStaffPayment", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> searchStaffPayment(
			@RequestBody StaffTelephonePaymentDTO staffTelephonePaymentDTO) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = staffTelephonePaymentService.searchStaffPayment(staffTelephonePaymentDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/lazyLoadStaffTelephone", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> lazyLoadStaffTelephone(
			@RequestBody PaginationDTO paginationDTO) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = staffTelephonePaymentService.lazyLoadStaffTelephone(paginationDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/approveStaffPayment", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> approveStaffPayment(
			@RequestBody StaffTelephonePaymentDTO staffTelephonePaymentDTO) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = staffTelephonePaymentService.approveStaffPayment(staffTelephonePaymentDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/rejectStaffPayment", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> rejectStaffPayment(
			@RequestBody StaffTelephonePaymentDTO staffTelephonePaymentDTO) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = staffTelephonePaymentService.rejectStaffPayment(staffTelephonePaymentDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
}