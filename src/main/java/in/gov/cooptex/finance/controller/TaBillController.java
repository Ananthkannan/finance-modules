package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.TaBillDTO;
import in.gov.cooptex.finance.service.TaBillService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("${finance.api.url}/tabill")
@Api(tags = "TaBill Claim", value = "TaBill Claim")
@Log4j2
public class TaBillController {
	
	@Autowired
	TaBillService taBillService;
	
	
	@RequestMapping(value = "/getallplancode", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getAllplancode() {
		log.info("taBillController:getallplancode()");
		BaseDTO baseDTO = taBillService.getAllplancode();
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getallplancodebystage", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getAllplancodeByStage() {
		log.info("taBillController:getAllplancodeByStage()");
		BaseDTO baseDTO = taBillService.getAllplancodeByStage();
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/getemp/{id}", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public BaseDTO getempById(@PathVariable Long id) {
		log.info("taBillController:getEmpByID() [" + id + "]");
		BaseDTO baseDTO = taBillService.getempById(id);
			return baseDTO;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> createtabill(@RequestBody TaBillDTO taBillDTO) {
		log.info("taBillController:createtabill() ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = taBillService.createtabill(taBillDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	
	@RequestMapping(value = "/getall", method = RequestMethod.POST)
	public @ResponseBody BaseDTO getAll(@RequestBody PaginationDTO paginationDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			baseDTO = taBillService.getAll(paginationDTO);
		} catch (Exception ex) {
			log.error("inside getAll Method exception", ex);
		}
		return baseDTO;
	}
	
	
	 @RequestMapping(value = "/getdetailsbyid/{id}", method = RequestMethod.GET)
	    public @ResponseBody
	    BaseDTO gettaBillById(@PathVariable("id") Long id) {
	            return taBillService.getDetailsById(id);
	    }
	    
		@RequestMapping(value = "/approvetabillvoucher", method = RequestMethod.POST)
		public @ResponseBody BaseDTO approvetabill(@RequestBody TaBillDTO requestDTO) {
			BaseDTO baseDTO = new BaseDTO();
			try {
				baseDTO = taBillService.approvetabill(requestDTO);
			} catch (Exception ex) {
				log.info("Tabill Controller ex:" + ex);
			}
			return baseDTO;
		}
		
		@RequestMapping(value = "/rejecttabillvoucher", method = RequestMethod.POST)
		public @ResponseBody BaseDTO rejecttabill(@RequestBody TaBillDTO requestDTO) {
			BaseDTO baseDTO = new BaseDTO();
			try {
				baseDTO = taBillService.rejecttabill(requestDTO);
			} catch (Exception ex) {
				log.info("Cheque to Bank Transfer Controller ex:" + ex);
			}
			return baseDTO;
		}

}
