package in.gov.cooptex.finance.controller;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.ChequeToBankResponseDTO;
import in.gov.cooptex.finance.service.ChequeToBankService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/chequetobank")
@Log4j2
public class ChequeToBankController {
	
	@Autowired
	ChequeToBankService chequeToBankService;

	@RequestMapping(value = "/getall", method = RequestMethod.POST)
	public @ResponseBody BaseDTO getAll(@RequestBody PaginationDTO paginationDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			baseDTO = chequeToBankService.getAll(paginationDTO);
		} catch (Exception ex) {
			log.error("inside getAll Method exception", ex);
		}
		return baseDTO;
	}

	@RequestMapping(value = "/getDetailsById/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> getBankToCashDetailsById(@PathVariable("id") Long id) {
		log.info("inside controller getDetailsById method----------------->");
		BaseDTO wrapperDto = chequeToBankService.getChequeToBankDetailsById(id);
		return new ResponseEntity<BaseDTO>(wrapperDto, HttpStatus.OK);

	}

	@RequestMapping(value = "/saveChequeToBank", method = RequestMethod.POST)
	public @ResponseBody BaseDTO saveChequeToBank(@RequestBody ChequeToBankResponseDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			baseDTO = chequeToBankService.saveChequeToBank(requestDTO);
		} catch (Exception ex) {
			log.info("Cheque to Bank Transfer Controller ex:" + ex);
		}
		return baseDTO;
	}
	
	@RequestMapping(value = "/editChequeToBank", method = RequestMethod.POST)
	public @ResponseBody BaseDTO editChequeToBank(@RequestBody ChequeToBankResponseDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			baseDTO = chequeToBankService.editChequeToBank(requestDTO);
		} catch (Exception ex) {
			log.info("Cheque to Bank Transfer Controller ex:" + ex);
		}
		return baseDTO;
	}

	@RequestMapping(value = "/getPaymentDetails", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<BaseDTO> getPaymentDetails(
			@RequestBody ChequeToBankResponseDTO chequeToBankResponseDTO) {
		BaseDTO baseDTO = chequeToBankService.getPaymentDetails(chequeToBankResponseDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/getBranchByBankId/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> getBranchByBankId(@PathVariable("id") Long id) {
		BaseDTO wrapperDto = chequeToBankService.getBranchByBankId(id);
		return new ResponseEntity<BaseDTO>(wrapperDto, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/approveChequeToBank", method = RequestMethod.POST)
	public @ResponseBody BaseDTO approveChequeToBank(@RequestBody ChequeToBankResponseDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			baseDTO = chequeToBankService.approveChequeToBank(requestDTO);
		} catch (Exception ex) {
			log.info("Cheque to Bank Transfer Controller ex:" + ex);
		}
		return baseDTO;
	}
	
	@RequestMapping(value = "/rejectChequeToBank", method = RequestMethod.POST)
	public @ResponseBody BaseDTO rejectChequeToBank(@RequestBody ChequeToBankResponseDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			baseDTO = chequeToBankService.rejectChequeToBank(requestDTO);
		} catch (Exception ex) {
			log.info("Cheque to Bank Transfer Controller ex:" + ex);
		}
		return baseDTO;
	}
	
	@RequestMapping(value = "/checkBalanceAmount", method = RequestMethod.POST)
	public @ResponseBody BaseDTO checkBalanceAmount(@RequestBody ChequeToBankResponseDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			baseDTO = chequeToBankService.checkBalanceAmount(requestDTO);
		} catch (Exception ex) {
			log.info("Cheque to Bank Transfer Controller checkBalanceAmount ex:" + ex);
		}
		return baseDTO;
	}
	
}
