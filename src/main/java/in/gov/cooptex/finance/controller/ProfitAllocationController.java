package in.gov.cooptex.finance.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.accounts.model.ProfitAllocation;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.JobApplication;
import in.gov.cooptex.finance.dto.ProfitAllocationViewDTO;
import in.gov.cooptex.finance.service.ProfitAllocationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("${finance.api.url}/profitallocation")
@Api(tags = "Profit Allocation", value = "Profit Allocation")
@Log4j2
public class ProfitAllocationController {
	
	@Autowired
	ProfitAllocationService profitAllocationService;

	@RequestMapping(value = "/getFinancialYear", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getFinancialYear() {
		log.info("ProfitAllocationController:getFinancialYear------");
		BaseDTO baseDTO = profitAllocationService.getFinancialYear();
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getNetAmountByFinancialId/{financialYearId}", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getNetAmountByFinancialId(@PathVariable Long financialYearId) {
		log.info("ProfitAllocationController:getNetAmountByFinancialId------"+financialYearId);
		BaseDTO baseDTO = profitAllocationService.getNetAmountByFinancialId(financialYearId);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	
	@RequestMapping(value = "/saveprofitallocation", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> saveProfitAllocation(@RequestBody ProfitAllocation profitAllocation, HttpServletRequest request) {
		log.info("<--- Inside Save Job Application --->");
		BaseDTO	baseDTO = profitAllocationService.saveProfitAllocation(profitAllocation);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/updateprofitallocation", method = RequestMethod.PUT)
	public ResponseEntity<BaseDTO> updateprofitallocation(@RequestBody ProfitAllocation profitAllocation, HttpServletRequest request) {
		log.info("<--- Inside Update Job Application --->");
		BaseDTO	baseDTO = profitAllocationService.updateProfitallocation(profitAllocation);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@PostMapping("/lazyload/search")
	public ResponseEntity<BaseDTO> search(@RequestBody ProfitAllocation profitAllocation) {
		log.info("BiometricDeviceRegisterController:search()");
		BaseDTO baseDTO = profitAllocationService.search(profitAllocation);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/delete/id/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<BaseDTO> delete(@PathVariable Long id) {
		log.info("<--- Starts Profile Allocation Delete --->");
		BaseDTO baseDTO = profitAllocationService.deleteById(id);
		log.info("<---Ends Profile Allocation Delete --->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getbyid/{id}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getProfitAllocation(@PathVariable Long id) {
		log.info("<--- Starts getProfitAllocation--->");
		BaseDTO baseDTO = profitAllocationService.getProfitAllocation(id,0L);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getbyidnotification/{id}/{notificationId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getProfitAllocationByIdandnotification(@PathVariable Long id,@PathVariable Long notificationId) {
		log.info("<--- Starts getProfitAllocation--->");
		BaseDTO baseDTO = profitAllocationService.getProfitAllocation(id,notificationId);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/deleteAllocationDetails/{profitAllocationDetailsId}", method = RequestMethod.DELETE)
	public ResponseEntity<BaseDTO> deleteAllocationDetails(@PathVariable Long profitAllocationDetailsId) {
		log.info("<--- Starts Profile Allocation Details Delete --->");
		BaseDTO baseDTO = profitAllocationService.deleteAllocationDetails(profitAllocationDetailsId);
		log.info("<---Ends Profile Allocation Details Delete --->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/approveprofitallocation", method = RequestMethod.POST)
	public BaseDTO approveProfitAllocation(@RequestBody ProfitAllocationViewDTO profitAllocationViewDTO) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = profitAllocationService.approveProfitAllocation(profitAllocationViewDTO);
		return baseDTO;
	}
	
	@RequestMapping(value = "/rejectprofitallocation", method = RequestMethod.POST)
	public BaseDTO rejectProfitAllocation(@RequestBody ProfitAllocationViewDTO profitAllocationViewDTO) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = profitAllocationService.rejectProfitAllocation(profitAllocationViewDTO);
		return baseDTO;
	}
}
