package in.gov.cooptex.finance.controller;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;


import in.gov.cooptex.finance.dto.SearchSocietyInvoiceAdjustmentDTO;
import in.gov.cooptex.finance.dto.SocietyInvoicePaymentDto;
import in.gov.cooptex.finance.dto.SocietyPaymentVoucherAddDTO;
import in.gov.cooptex.finance.dto.SocietyPaymentVoucherSearchResponseDTO;
import in.gov.cooptex.finance.dto.VoucherDto;
import in.gov.cooptex.finance.service.SocietyPaymentVoucherService;
import in.gov.cooptex.finance.service.SocietyStopInvoiceService;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/societyPaymentVoucherController")
@Log4j2
public class SocietyPaymentVoucherController {

	@Autowired
	SocietyPaymentVoucherService societyPaymentVoucherService;

	@Autowired
	SocietyStopInvoiceService societyStopInvoiceService;


	/**
	 * @apiNote List Society Payment Voucher - List Api
	 * @param paginationDto
	 * @return
	 */
	@RequestMapping(value = "/getAllPaymentVouchers", method = RequestMethod.POST)
	public BaseDTO getAllPaymentVouchers(@RequestBody PaginationDTO paginationDto) {
		log.info("<-- SocietyPaymentVoucherController.getAllPaymentVouchers() Invoked-->");
		return societyPaymentVoucherService.getAllPaymentVouchersLazy(paginationDto);
	}


//	/**
//	 * @apiNote Create Society Payment Voucher - Action Api
//	 * @param invoiceDetailsRequestDTO
//	 * @return
//	 */
//	@RequestMapping(value = "/getallpendinginvoicesbasedonsociety", method = RequestMethod.POST)
//	public ResponseEntity<BaseDTO> getAllPendingInvoicesBasedOnSociety(@RequestBody InvoiceDetailsRequestDTO invoiceDetailsRequestDTO) {
//		log.info(" Controller - societyPaymentVoucherController : method : getAllPendingInvoicesBasedOnSociety :: starts here");
//		BaseDTO baseDTO = societyStopInvoiceService.searchPurchaseInvoiceDetails(invoiceDetailsRequestDTO);
//		log.info("Controller - societyPaymentVoucherController : method : getAllPendingInvoicesBasedOnSociety :: ends here");
////		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
//		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
//	}



	/**
	 * @apiNote Create Society Payment Voucher - Search Api
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/getallpendinginvoicesbasedgroupedbysociety", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> getAllPendingInvoicesBasedOnGrouppedBySociety(@RequestBody SearchSocietyInvoiceAdjustmentDTO searchSocietyInvoiceAdjustmentDTO) {
		log.info(" Controller - SocietyPaymentVoucherController : method : getAllPendingInvoicesBasedOnGrouppedBySociety :: starts here");
		BaseDTO baseDTO = societyStopInvoiceService.searchPendingPurchaseInvoiceDetailsGroupedBySociety(searchSocietyInvoiceAdjustmentDTO);
		log.info("Controller - SocietyPaymentVoucherController : method : getAllPendingInvoicesBasedOnGrouppedBySociety :: ends here");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}


		/**
	 * @apiNote Create Society Payment Voucher - Add Api
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/addsocietypaymentvoucher", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> addSocietyPaymentVoucher(@RequestBody SocietyPaymentVoucherAddDTO societyPaymentVoucherAddDTO) {
		log.info(" Controller - SocietyPaymentVoucherController : method : addSocietyPaymentVoucher :: invoked");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = societyPaymentVoucherService.saveSocietyPaymentVoucher(societyPaymentVoucherAddDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/viewvoucherdetails/{voucherId}", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> viewVoucherDetails(@PathVariable Long voucherId) {
		log.info(" Controller - SocietyPaymentVoucherController : method : viewVoucherDetails :: starts here");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = societyPaymentVoucherService.searchPurchaseInvoiceDetailsBasedOnVoucherId(voucherId);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	
	

	@RequestMapping(value = "/runAdvaice/{societyPaymentId}", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> runAdvaiceDetails(@PathVariable Long societyPaymentId) {
		log.info(" Controller - SocietyPaymentVoucherController : method : viewVoucherDetails :: starts here");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = societyPaymentVoucherService.generatePDFBasedOnSocietyPaymentId(societyPaymentId);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<BaseDTO> delete(@PathVariable Long id) {
		log.info("<---  Controller - SocietyPaymentVoucherController : method : delete :: starts here --->");
		BaseDTO baseDTO = societyStopInvoiceService.deleteVoucherById(id);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getproductionplanforlist", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> loadProductionPlanForList() {
		log.info(" Controller - SocietyPaymentVoucherController : method : loadProductionPlanForList :: starts here");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = societyPaymentVoucherService.loadProductionPlanForList();
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getgovtschemeplanlist", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> loadGovtSchemePlan() {
		log.info(" Controller - SocietyPaymentVoucherController : method : loadGovtSchemePlan :: starts ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = societyPaymentVoucherService.loadGovtSchemePlan();
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getproductionplanbyplantype/{code}", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getProductionPlanByPlanType(@PathVariable("code") String planCode) {
		log.info(" Controller - SocietyPaymentVoucherController : method : getProductionPlanByPlanType :: starts ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = societyPaymentVoucherService.getProductionPlanByPlanType(planCode);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getproductionplanbyschemetype/{code}", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getProductionPlanBySchemeType(@PathVariable("code") String schemeCode) {
		log.info(" Controller - SocietyPaymentVoucherController : method : getProductionPlanBySchemeType :: starts ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = societyPaymentVoucherService.getProductionPlanBySchemeType(schemeCode);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	/**
	 * @apiNote Create Society Payment Voucher - Search Api
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/getallpendingpurchaseinvoices", method = RequestMethod.POST) 
	public ResponseEntity<BaseDTO> getAllPendingPurchaseInvoices(@RequestBody SocietyPaymentVoucherSearchResponseDTO responseDTO) {
		log.info(" Controller - SocietyPaymentVoucherController : method : getAllPendingPurchaseInvoices :: starts here");
		BaseDTO baseDTO = societyPaymentVoucherService.getAllPendingPurchaseInvoices(responseDTO);
		log.info("Controller - SocietyPaymentVoucherController : method : getAllPendingPurchaseInvoices :: ends here");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/loadgovtschemetypes", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> govtSchemeTypeList() {
		log.info("<--Starts FDSController .govtSchemeTypeList-->");
		BaseDTO baseDTO = societyPaymentVoucherService.govtSchemeTypeList();
		log.info("<--Ends FDSController .segovtSchemeTypeListarch-->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}

	
	@RequestMapping(value = "/getloomtypedropdown/{circleId}", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getAllLoomType(@PathVariable("circleId") Long circleId) {
		log.info(" Controller - SocietyPaymentVoucherController : method : getAllLoomType :: starts ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = societyPaymentVoucherService.getAllLoomType(circleId);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getsocietycodedropdown/{loomtype}", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getAllSocietyDetails(@PathVariable("loomtype") String loomtype) {
		log.info(" Controller - SocietyPaymentVoucherController : method : getAllSocietyDetails :: starts ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = societyPaymentVoucherService.getAllSocietyDetails(loomtype);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getcirclemaster/{districId}", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getCircleMasterByDistrictId(@PathVariable("districId") Long districId) {
		log.info(" Controller - SocietyPaymentVoucherController : method : getCircleMasterByDistrictId :: starts ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = societyPaymentVoucherService.getCircleMasterByDistrictId(districId);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getdistric", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> loadDistrictBySupplier() {
		log.info(" Controller - SocietyPaymentVoucherController : method : loadDistrictBySupplier :: starts here");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = societyPaymentVoucherService.getDistrictBySupplier();
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	
	@RequestMapping(value = "/rejectSocietyPayment", method = RequestMethod.POST)
	public @ResponseBody BaseDTO rejectSocietyPayment(@RequestBody VoucherDto voucherDto) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			baseDTO = societyPaymentVoucherService.rejectSocietyPayment(voucherDto);
		} catch (Exception ex) {
			log.info("SocietyPayment Voucher Controller ex:" + ex);
		}
		return baseDTO;
	}
	
	@RequestMapping(value = "/approveSocietyPayment", method = RequestMethod.POST)
	public @ResponseBody BaseDTO approveSocietyPayment(@RequestBody SocietyInvoicePaymentDto societyInvoicePaymentDto) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			baseDTO = societyPaymentVoucherService.approveSocietyPayment(societyInvoicePaymentDto);
		} catch (Exception ex) {
			log.info("SocietyPayment Voucher Controller ex:" + ex);
		}
		return baseDTO;
	}
	
	@RequestMapping(value = "/getcirclemasterbyDistrictIds", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<BaseDTO> getCircleMasterByDistrictIds(@RequestBody List<Long> DistrictIds) {
		log.info(" Controller - SocietyPaymentVoucherController : method : getCircleMasterByDistrictIds :: starts ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = societyPaymentVoucherService.getCircleMasterbyDistrictIds(DistrictIds);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
//	Wrote By Vinayak - 2019-10-29
	@RequestMapping(value = "/getcirclemasterdetailsbydate",method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<BaseDTO> getCircleMasterDetailsbyDate(@RequestBody SearchSocietyInvoiceAdjustmentDTO sivid)
	{
		log.info(" ----------------Controller - SocietyPaymentVoucherController : method : getCircleMasterByDistrictIds :: starts----------- ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = societyPaymentVoucherService.getCircleDetailsbyDate(sivid);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getloomtypebycircleids", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<BaseDTO> getLoomTypeByCircleIds(@RequestBody List<Long> CircleIds) {
		log.info(" Controller - SocietyPaymentVoucherController : method : getLoomTypeByCircleIds:: starts ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = societyPaymentVoucherService.getLoomTypeByCircleIds(CircleIds);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
//	wrote by vinayak
	@RequestMapping(value = "getloomtypemaster",method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<BaseDTO> getLoopTypeMaster(@RequestBody SearchSocietyInvoiceAdjustmentDTO ssivdto){
		log.info(" -----------------Controller - SocietyPaymentVoucherController : method :  getLoopTypeMaster-------------");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = societyPaymentVoucherService.getLoomTypeMasterDetails(ssivdto);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	 
	}
	
	@RequestMapping(value = "/getsocietybyloomtypes", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> getSocietyByLoomTypes(@RequestBody List<String> loomtypes) {
		log.info(" Controller - SocietyPaymentVoucherController : method : getSocietyByLoomTypes :: starts ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = societyPaymentVoucherService.getSocietyByLoomTypes(loomtypes);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
//	get society 
	@RequestMapping(value = "/getsocietybyloomtypesvone", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> getSocietyByLoomTypesvone(@RequestBody SearchSocietyInvoiceAdjustmentDTO lptype) {
		log.info(" =======5555===Controller - SocietyPaymentVoucherController : method : getSocietyByLoomTypes :: starts==============");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = societyPaymentVoucherService.getSocietyByLoomTypesv1(lptype);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
//	get district code
	@RequestMapping(value = "/getdistrictcodebyinvoicedate", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<BaseDTO> getDistrictCodebyInvoicedate(@RequestBody SearchSocietyInvoiceAdjustmentDTO ssiaDTO) {
		log.info(" ===============Controller - SocietyPaymentVoucherController : method : getLoomTypeByCircleIds:: starts ===================== ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = societyPaymentVoucherService.getDistrictCodebyInvoicedate(ssiaDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	@RequestMapping(value = "/getalldropdownlistbyinvoicedate", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<BaseDTO> getAllDropdownListbyInvoicedate(@RequestBody SearchSocietyInvoiceAdjustmentDTO ssiaDTO) {
		log.info(" SocietyPaymentVoucherController -> getAllDropdownListbyInvoicedate starts ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = societyPaymentVoucherService.getAllDropdownListbyInvoicedate(ssiaDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
}
