package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.EntityMonthScheduleDTO;
import in.gov.cooptex.finance.service.MonthlyStatementProcessService;
import io.swagger.annotations.Api;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("${finance.api.url}/monthlystatement")
@Api(tags = "Monthly Statement Process", value = "Monthly Statement Process")
@Log4j2
public class MonthlyStatementProcessController {

	@Autowired
	MonthlyStatementProcessService monthlyStatementProcessService;
	
	
	/**
	 * @param entityMonthScheduleDTO
	 * @return
	 */
	@PostMapping("/runmonthlystatement")
	public ResponseEntity<BaseDTO> runMonthlyStatement(@RequestBody EntityMonthScheduleDTO entityMonthScheduleDTO) {
		log.info("<===== Starts BankAnalysisController.runMonthlyStatement =======>");
		BaseDTO baseDTO = monthlyStatementProcessService.runMonthlyStatement(entityMonthScheduleDTO);
		log.info("<=====Ends BankAnalysisController.runMonthlyStatement =======>");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	/**
	 * 
	 * @param entityMonthScheduleDTO
	 * @return
	 */
	@PostMapping("/locknor")
	public ResponseEntity<BaseDTO> lockNOR(@RequestBody EntityMonthScheduleDTO entityMonthScheduleDTO) {
		log.info("<===== Starts MonthlyStatementProcessController.lockNOR =======>");
		BaseDTO baseDTO = monthlyStatementProcessService.lockNOR(entityMonthScheduleDTO);
		log.info("<=====Ends MonthlyStatementProcessController.lockNOR =======>");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	/**
	 * 
	 * @param entityMonthScheduleDTO
	 * @return
	 */
	@PostMapping("/checklocknor")
	public ResponseEntity<BaseDTO> checkLockNOR(@RequestBody EntityMonthScheduleDTO entityMonthScheduleDTO) {
		log.info("<===== Starts MonthlyStatementProcessController.checkLockNOR =======>");
		BaseDTO baseDTO = monthlyStatementProcessService.checkLockNOR(entityMonthScheduleDTO);
		log.info("<=====Ends MonthlyStatementProcessController.checkLockNOR =======>");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/checknorprogress")
	public ResponseEntity<BaseDTO> checkNORProgressStatus(@RequestBody EntityMonthScheduleDTO entityMonthScheduleDTO) {
		log.info("<===== Starts MonthlyStatementProcessController.checkNORProgressStatus =======>");
		BaseDTO baseDTO = monthlyStatementProcessService.checkNORProgressStatus(entityMonthScheduleDTO);
		log.info("<=====Ends MonthlyStatementProcessController.checkNORProgressStatus =======>");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value= "/getstatementstatus", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> getStatementStatus(@RequestBody EntityMonthScheduleDTO entityMonthScheduleDTO) {
		log.info("MonthlyStatementProcessController. getStatementStatus()");
		BaseDTO baseDTO = null;
		baseDTO = monthlyStatementProcessService.getStatementStatus(entityMonthScheduleDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
}
