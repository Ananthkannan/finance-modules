package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.dto.ClaimStatementDto;
import in.gov.cooptex.finance.dto.RebateCollectionAmountDto;
import in.gov.cooptex.finance.service.RebateClaimCollectionService;
import io.swagger.annotations.Api;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/rebateclaimcontroller")
@Api(tags = "Rebate Claim", value = "Rebate Claim")
@Log4j2
public class RebateClaimCollectionController {
	
	@Autowired
	RebateClaimCollectionService rebateClaimCollectionService;
	
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<BaseDTO> saveRebateClaimData(@RequestBody RebateCollectionAmountDto rebateCollectionAmountDto) {
		log.info(":: RebateClaimController - save Rebate Claim Data ::");
		BaseDTO baseDTO = rebateClaimCollectionService.saveRebateClaimCollectionData(rebateCollectionAmountDto);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value = "/getrebatedetails", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<BaseDTO> getRebateDetails(@RequestBody RebateCollectionAmountDto rebateCollectionAmountDto) {
		log.info("::Start RebateClaimCollectionController - getRebateDetails ::");
		BaseDTO baseDTO = rebateClaimCollectionService.getRebateDetails(rebateCollectionAmountDto);
		log.info(":: End RebateClaimCollectionController - getRebateDetails ::");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getall", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<BaseDTO> getAllRebateDetails() {
		log.info("::Start RebateClaimCollectionController - getAllRebateDetails ::");
		BaseDTO baseDTO = rebateClaimCollectionService.getAllRebateDetails();
		log.info(":: End RebateClaimCollectionController - getAllRebateDetails ::");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	/*This Controller method is used to get data for list and search*/
	@RequestMapping(value = "/getcollectiondata", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<BaseDTO> getCollectionData(@RequestBody PaginationDTO paginationDTO) {
		log.info("::Start RebateClaimCollectionController - Search And Load LazyList ::");
		BaseDTO baseDTO = rebateClaimCollectionService.getCollectionData(paginationDTO);
		log.info(":: End RebateClaimCollectionController - Search And Load LazyList ::");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	
	/*This Controller method is used to get data for list and search*/
	@RequestMapping(value = "/viewstatus", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<BaseDTO> showViewPage(@RequestBody RebateCollectionAmountDto rebateCollectionAmountDto) {
		log.info("::Start RebateClaimController - Search And Load LazyList ::");
		BaseDTO baseDTO = rebateClaimCollectionService.showViewPage(rebateCollectionAmountDto);
		log.info(":: End RebateClaimController - Search And Load LazyList ::");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	

}
