package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.TaBillDTO;
import in.gov.cooptex.finance.TaBillPaymentDTO;
import in.gov.cooptex.finance.service.TaBillPaymentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("${finance.api.url}/tabillpayment")
@Api(tags = "TaBill Payment", value = "TaBill Payment")
@Log4j2
public class TaBillPaymentController {

	@Autowired
	TaBillPaymentService taBillPaymentService;
	
	@RequestMapping(value = "/getemployeelist", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getemployeelist() {
		log.info("taBillPaymentController:getemployeelist()");
		BaseDTO baseDTO = taBillPaymentService.getemployeelist();
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getpaymentmodelist", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getpaymentmodelist() {
		log.info("taBillPaymentController:getemployeelist()");
		BaseDTO baseDTO = taBillPaymentService.getpaymentmodelist();
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> createtabill(@RequestBody TaBillPaymentDTO taBillpaymentDTO) {
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = taBillPaymentService.createtabill(taBillpaymentDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	
	@RequestMapping(value = "/getall", method = RequestMethod.POST)
	public @ResponseBody BaseDTO getAll(@RequestBody PaginationDTO paginationDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			baseDTO = taBillPaymentService.getAll(paginationDTO);
		} catch (Exception ex) {
			log.error("inside getAll Method exception", ex);
		}
		return baseDTO;
	}
	
	
	 @RequestMapping(value = "/getdetailsbyid/{id}", method = RequestMethod.GET)
	    public @ResponseBody
	    BaseDTO gettaBillById(@PathVariable("id") Long id) {
	            return taBillPaymentService.getDetailsById(id);
	    }
	    
		@RequestMapping(value = "/approvetabillvoucher", method = RequestMethod.POST)
		public @ResponseBody BaseDTO approvetabill(@RequestBody TaBillPaymentDTO requestDTO) {
			BaseDTO baseDTO = new BaseDTO();
			try {
				baseDTO = taBillPaymentService.approvetabill(requestDTO);
			} catch (Exception ex) {
				log.info("Tabill Controller ex:" + ex);
			}
			return baseDTO;
		}
		
		@RequestMapping(value = "/rejecttabillvoucher", method = RequestMethod.POST)
		public @ResponseBody BaseDTO rejecttabill(@RequestBody TaBillPaymentDTO requestDTO) {
			BaseDTO baseDTO = new BaseDTO();
			try {
				baseDTO = taBillPaymentService.rejecttabill(requestDTO);
			} catch (Exception ex) {
				log.info("Cheque to Bank Transfer Controller ex:" + ex);
			}
			return baseDTO;
		}
	
		 @RequestMapping(value = "/getadvance/{id}", method = RequestMethod.GET)
		    public @ResponseBody BaseDTO getadvance(@PathVariable("id") Long id) {
		            return taBillPaymentService.getadvance(id);
		    }
	
}
