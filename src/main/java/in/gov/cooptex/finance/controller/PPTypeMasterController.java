package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.finance.service.PPTypeMasterService;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping(value="/ppTypeMaster")
@Log4j2	
public class PPTypeMasterController {
	@Autowired
	PPTypeMasterService pPTypeMasterService;
	
	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getAll() {
		log.info("<<========= PPTypeMasterController ----  getAll  =====##STARTS");
		BaseDTO baseDTO = pPTypeMasterService.getAllTypes();
		log.info("<<========= PPTypeMasterController ----  getAll  =====##ENDS");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

}
