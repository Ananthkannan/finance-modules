package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.dto.ImprestRequestDTO;
import in.gov.cooptex.finance.service.ImprestRequestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("${finance.api.url}/imprestrequest")
@Api(tags = "Imprest Request", value = "Imprest Request")
@Log4j2
public class ImprestRequestController {

	@Autowired
	ImprestRequestService imprestRequestService;
	
	@RequestMapping(value = "/getByBudgetConfig/{id}", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getByBudgetConfig(@PathVariable Long id) {
		log.info("ImprestRequestController:getByBudgetConfig() [" + id + "]");
		BaseDTO baseDTO = imprestRequestService.getByBudgetConfig(id);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/generateImprestRequest", method = RequestMethod.POST)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public BaseDTO generateImprestRequest(@RequestBody ImprestRequestDTO imprestRequestDTO) {
		log.info("ImprestRequestController:generateImprestRequest------");
		BaseDTO baseDTO = imprestRequestService.generateImprestRequest(imprestRequestDTO);
		return baseDTO;
	}
	
	@RequestMapping(value = "/saveImprestRequest", method = RequestMethod.POST)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public BaseDTO saveImprestRequest(@RequestBody ImprestRequestDTO imprestRequestDTO) {
		log.info("ImprestRequestController:saveImprestRequest------");
		BaseDTO baseDTO = imprestRequestService.saveImprestRequest(imprestRequestDTO);
		return baseDTO;
	}
	
	@RequestMapping(value = "/lazyLoadImprestRequest", method = RequestMethod.POST)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public BaseDTO lazyLoadImprestRequest(@RequestBody PaginationDTO paginationDTO) {
		log.info("ImprestRequestController:lazyLoadImprestRequest------");
		BaseDTO baseDTO = imprestRequestService.lazyLoadImprestRequest(paginationDTO);
		return baseDTO;
	}
	
	@RequestMapping(value = "/viewImprestRequest", method = RequestMethod.POST)
	public BaseDTO viewImprestRequest(@RequestBody ImprestRequestDTO imprestRequestDTO) {
		log.info("ImprestRequestController:viewImprestRequest------");
		BaseDTO baseDTO = imprestRequestService.viewImprestRequest(imprestRequestDTO);
		return baseDTO;
	}
	
	@RequestMapping(value = "/approveImprestRequest", method = RequestMethod.POST)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public BaseDTO approveImprestRequest(@RequestBody ImprestRequestDTO imprestRequestDTO) {
		log.info("ImprestRequestController:approveImprestRequest------");
		BaseDTO baseDTO = imprestRequestService.approveImprestRequest(imprestRequestDTO);
		return baseDTO;
	}
	
	@RequestMapping(value = "/rejectImprestRequest", method = RequestMethod.POST)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public BaseDTO rejectImprestRequest(@RequestBody ImprestRequestDTO imprestRequestDTO) {
		log.info("ImprestRequestController:approveImprestRequest------");
		BaseDTO baseDTO = imprestRequestService.rejectImprestRequest(imprestRequestDTO);
		return baseDTO;
	}
}
