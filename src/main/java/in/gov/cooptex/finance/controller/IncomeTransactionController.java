package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import in.gov.cooptex.common.service.CustomerMasterService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.dto.IncomeTransactionDTO;
import in.gov.cooptex.finance.service.IncomeTransactionService;
import in.gov.cooptex.operation.model.SalesInvoice;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("${finance.api.url}/incometransaction")
@Log4j2
public class IncomeTransactionController {

	@Autowired
	IncomeTransactionService incomeTransactionService;
	
	@Autowired
	CustomerMasterService customerMasterService;
	
	@RequestMapping(value = "/getGovtSchemeIncomeDetails", method = RequestMethod.POST)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getGovtSchemeIncomeDetails(@RequestBody IncomeTransactionDTO incomeTransactionDTO) {
		log.info("IncomeTransactionController:getGovtSchemeIncomeDetails()");
		BaseDTO baseDTO = incomeTransactionService.getGovtSchemeIncomeDetails(incomeTransactionDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> create(@RequestBody SalesInvoice salesInvoice) {
		log.info("IncomeTransactionController:create()");
		BaseDTO baseDTO = incomeTransactionService.create(salesInvoice);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/lazyloadlist", method = RequestMethod.POST)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getServiceInvoiceList(@RequestBody PaginationDTO paginationDTO) {
		log.info("IncomeTransactionController:getServiceInvoiceList()");
		BaseDTO baseDTO = incomeTransactionService.getServiceInvoiceList(paginationDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getById(@PathVariable Long id) {
		log.info("IncomeTransactionController:get() [" + id + "]");
		BaseDTO baseDTO = incomeTransactionService.getById(id);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/customerlistbytypeId/{name}/{custtypeid}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getAllCustomersByTypeID(@PathVariable String name ,@PathVariable Long custtypeid) {
		log.info("<--- Starts IncomeTransactionController:get.getAllCustomersByTypeID() --->" + name+":: CustomerType ID ::"+custtypeid);
		BaseDTO baseDTO = incomeTransactionService.getAllCustomersByTypeId(name,custtypeid);
		log.info("<---Ends EmployeePromotionDetails.getAllCustomersByName()--->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
}
