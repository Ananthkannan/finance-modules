package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.dto.BudgetAllocationDTO;
import in.gov.cooptex.finance.service.BudgetAllocationService;
import io.swagger.annotations.Api;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("${finance.api.url}/budgetallocation")
@Api(tags = "Budget Allocation", value = "Budget Allocation")
@Log4j2
public class BudgetAllocationController {

	@Autowired
	BudgetAllocationService budgetAllocationService;
	
	@PostMapping("/getallbudgetallocationlazy")
	public ResponseEntity<BaseDTO> getAllBudgetAllocationListLazy(@RequestBody PaginationDTO paginationDTO) {
		log.info("<=====Starts BudgetAllocationController.getAllBudgetAllocationListLazy =======>"+paginationDTO);
		BaseDTO baseDTO = budgetAllocationService.getAllAllocationListLazy(paginationDTO);
		log.info("<=====Ends BudgetAllocationController.getAllBudgetAllocationListLazy =======>");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/getby/{allocationId}")
	public ResponseEntity<BaseDTO>  getAllocationById(@PathVariable("allocationId") Long allocationId) {
		log.info("<=====Starts BudgetAllocationController.getAllocationById =======>"+allocationId);
		BaseDTO baseDTO = budgetAllocationService.getById(allocationId);	
		log.info("<=====Ends BudgetAllocationController.getAllocationById =======>");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/saveorupdate")
	public ResponseEntity<BaseDTO>  createBudgetAllocation(@RequestBody BudgetAllocationDTO budgetAllocationDto) {
		log.info("<=====Starts BudgetAllocationController.createBudgetAllocation =======>");
		BaseDTO baseDTO = budgetAllocationService.createOrEditBudgetAllocation(budgetAllocationDto);	
		log.info("<=====Ends BudgetAllocationController.createBudgetAllocation =======>");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/generate")
	public ResponseEntity<BaseDTO>  generateBudgetAllocation(@RequestBody BudgetAllocationDTO budgetAllocationDTO) {
		log.info("<=====Starts BudgetAllocationController.generateBudgetAllocation =======>");
		BaseDTO baseDTO = budgetAllocationService.generateBudgetAllocation(budgetAllocationDTO);		
		log.info("<=====Ends BudgetAllocationController.generateBudgetAllocation =======>");
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	
}
