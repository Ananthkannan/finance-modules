package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.accounts.model.AccountTransaction;
import in.gov.cooptex.core.accounts.model.PurchaseInvoice;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.dto.pos.SupplierPaymentDTO;
import in.gov.cooptex.finance.dto.VoucherDto;
import in.gov.cooptex.finance.service.SupplierPaymentService;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping("/supplierpayment")
public class SupplierPaymentController {

	@Autowired
	SupplierPaymentService supplierPaymentService;
	
	
	@RequestMapping(value = "/create",method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BaseDTO> createSupplierPayment(
			@RequestBody PurchaseInvoice purchaseInvoice) {
		log.info("SupplierPaymentController:createSupplierPayment start==>");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = supplierPaymentService.createSupplierPayment(purchaseInvoice);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/createAccount",method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BaseDTO> createAccount(
			@RequestBody AccountTransaction accountTransaction) {
		log.info("SupplierPaymentController:createAccount start==>");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = supplierPaymentService.createAccount(accountTransaction);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getinvoicebillamount/{supplierId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getInvoiceBillAmount(@PathVariable Long supplierId) {
		log.info("<--- Starts SupplierPaymentController.supplierId() --->"+supplierId);
		BaseDTO	baseDTO=supplierPaymentService.getInvoiceBillAmount(supplierId);
		log.info("<---Ends SupplierPaymentController.supplierId()--->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/savesupplierpayment",method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BaseDTO> saveSupplierPayment(
			@RequestBody VoucherDto voucherDto) {
		log.info("SupplierPaymentController:saveSupplierPayment start==>");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = supplierPaymentService.saveSupplierPayment(voucherDto);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("/lazyload/search")
	public ResponseEntity<BaseDTO> search(@RequestBody Voucher voucher) {
		log.info("BiometricDeviceRegisterController:search()");
		BaseDTO baseDTO = supplierPaymentService.search(voucher);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getbyid/{voucherId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getVoucherById(@PathVariable Long voucherId) {
		log.info("<--- Starts SupplierPaymentController.getVoucherById() --->"+voucherId);
		BaseDTO	baseDTO=supplierPaymentService.getVoucherById(voucherId);
		log.info("<---Ends SupplierPaymentController.supplierId()--->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getinvoicebillamountasview/{voucherId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getInvoiceBillAmountAsView(@PathVariable Long voucherId) {
		log.info("<--- Starts SupplierPaymentController.getInvoiceBillAmountAsView() --->"+voucherId);
		BaseDTO	baseDTO=supplierPaymentService.getInvoiceBillAmountAsView(voucherId);
		log.info("<---Ends SupplierPaymentController.getInvoiceBillAmountAsView()--->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/delete/id/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<BaseDTO> delete(@PathVariable Long id) {
		log.info("SupplierPaymentController:delete()");
		BaseDTO baseDTO = supplierPaymentService.deleteById(id);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/supplierPaymentApproval", method = RequestMethod.POST)
	public @ResponseBody BaseDTO supplierPaymentApproval(@RequestBody SupplierPaymentDTO supplierPaymentDTO) {
		BaseDTO baseDTO = new BaseDTO();
		log.info("<--- Starts SupplierPaymentController.supplierPaymentApproval() --->");
		try {
			baseDTO = supplierPaymentService.supplierPaymentApproval(supplierPaymentDTO);
		} catch (Exception ex) {
			log.info("supplierPaymentApproval Controller Exception:" + ex);
		}
		return baseDTO;
	}
	
	
	@RequestMapping(value = "/loadSupplierBySupplierTypeId/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> loadSupplierBySupplierTypeId(@PathVariable("id") Long id) {
		log.info("<--- Received getAllSupplierMaster .create() ---> ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = supplierPaymentService.loadSupplierBySupplierTypeId(id);
		if (baseDTO != null) {
			log.info("<--- Starts getAllSupplierMaster .fetch Success() ---> ");
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			log.error("<--- Starts IntendRequestController .created Failed() ---> ");
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}

	
	@RequestMapping(value="/loadpurchaseorderlistbysupplierid/{id}",method=RequestMethod.GET)
	public BaseDTO loadPurchaseOrderListBySupplierId(@PathVariable Long id) {
		log.info("<< == Start Of getting product variety : controller ==>>");
		BaseDTO baseDTO=supplierPaymentService.loadPurchaseOrderListBySupplierId(id);
		log.info("<< == End Of Getting product variety : Service == >> ");
		return baseDTO;
	}
	
	@RequestMapping(value = "/getpurchaseorderitembyorderid/{purchaseOrderId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getPurchaseOrderItemByOrderId(@PathVariable Long purchaseOrderId) {
		BaseDTO	baseDTO=supplierPaymentService.getSelectedPurchaseOrderItem(purchaseOrderId);
		log.info("<---Ends PurchaseOrderItemController.getPurchaseOrderItemByOrderId()--->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
}
