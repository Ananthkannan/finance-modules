package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.finance.dto.ReceiptPaymentRequestDTO;
import in.gov.cooptex.finance.service.ReceiptAndPaymentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("${finance.api.url}/receiptpaymentreport")
@Api(tags = "Receipt And Payment", value = "Receipt And Payment")
@Log4j2
public class ReceiptAndPaymentController {

	@Autowired
	ReceiptAndPaymentService receiptAndPaymentService;

	@RequestMapping(value = "/getreceiptpaymentreport", method = RequestMethod.POST)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getReceiptPaymentReport(@RequestBody ReceiptPaymentRequestDTO receiptPaymentRequestDTO) {
		log.info("ReceiptAndPaymentController:getReceiptPaymentReport------");
		BaseDTO baseDTO = receiptAndPaymentService.getReceiptPaymentReport(receiptPaymentRequestDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
}
