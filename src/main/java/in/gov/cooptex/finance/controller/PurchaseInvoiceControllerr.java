package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.finance.service.PurchaseInvoiceService;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping("/purchaseinvoice")
public class PurchaseInvoiceControllerr {

	@Autowired
	PurchaseInvoiceService purchaseInvoiceService;
	
	@RequestMapping(value="/loadpurchaseinvoicebysupplierid/{supplierId}",method=RequestMethod.GET)
	public ResponseEntity<BaseDTO> loadPurchaseInvoiceBySupplierId(@PathVariable Long supplierId)
	{
		log.info("<--- PurchaseInvoiceControllerr loadPurchaseInvoiceBySupplierId Started --->");
		BaseDTO responce=purchaseInvoiceService.loadPurchaseInvoiceBySupplierId(supplierId);
		return new ResponseEntity<BaseDTO>(responce, HttpStatus.OK);
	}
	
}
