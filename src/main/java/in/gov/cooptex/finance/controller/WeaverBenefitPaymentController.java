package in.gov.cooptex.finance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.BanktoBankAddDTO;
import in.gov.cooptex.finance.ChequeToBankResponseDTO;
import in.gov.cooptex.finance.dto.WeaversBenefitPaymentDTO;
import in.gov.cooptex.finance.service.WeaverBenefitPaymentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("${finance.api.url}/weaverbenefitpayment")
@Api(tags = "Weavers Benefit Payment", value = "Weavers Benefit Payment")
@Log4j2
public class WeaverBenefitPaymentController {

	@Autowired
	WeaverBenefitPaymentService weaverBenefitPaymentService;
	
	
	@RequestMapping(value = "/getActiveWeaversBenefitScheme", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getActiveWeaversBenefitScheme() {
		log.info("WeaversBenefitSchemeController:getActiveWeaversBenefitScheme()");
		BaseDTO baseDTO = weaverBenefitPaymentService.getActiveWeaversBenefitScheme();
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	
	@RequestMapping(value = "/loadallcircles", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> loadAllCircles(){
		BaseDTO baseDTO=weaverBenefitPaymentService.loadAllCircleMasters();
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value = "/getallsupplymaster/{circleId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> getAllByCircleId(@PathVariable Long circleId) {
		log.info("<--Starts SupplierMasterController .getAllByCircleId--> " + circleId);
		BaseDTO baseDTO = weaverBenefitPaymentService.getAllByCircleId(circleId);
		log.info("<--Ends SupplierMasterController .getAllByCircleId-->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK); 
	}
	
	

	@RequestMapping(value = "/getallrelationship", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> loadAllrelationship(){
		BaseDTO baseDTO=weaverBenefitPaymentService.loadAllrelationship();
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@GetMapping("/loadpayment")
	@ApiOperation(value = "Payment Mode Find All", response = Iterable.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Payment Mode Find All Successfully"),
			@ApiResponse(code = 401, message = "You are not authorized to Fetch Payment Mode"),
			@ApiResponse(code = 403, message = "Fetch Payment Mode is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 500, message = "Internal Error Occured while Fetching Payment Mode") })
	public ResponseEntity<BaseDTO> loadPayments() {
		log.info(":: Controller - Fetch All Payment Mode ::");
		BaseDTO baseDTO = weaverBenefitPaymentService.loadPayments();
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
    @RequestMapping(method = RequestMethod.POST, value = "/create")
	public BaseDTO addWeaversbenefitpayment(@RequestBody WeaversBenefitPaymentDTO data) {
        log.info("<--Starts weaverBenefitPaymentController Creating-->"+data);
		return weaverBenefitPaymentService.addWeaversbenefitpayment(data);
	}
    
    
    @ApiOperation(value = "weaverBenefitPayment Get ALL Service",response = Iterable.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "All weaverBenefitPayment Get Successfully Retrived")})
    @RequestMapping(value = "/getweaverBenefitPaymentList", method = RequestMethod.POST)
    public ResponseEntity<BaseDTO> getweaverBenefitPaymentList(@RequestBody PaginationDTO paginationDTO) {
        log.info("<--Starts weaverBenefitPaymentController .getweaverBenefitPaymentList-->");
        BaseDTO baseDTO = weaverBenefitPaymentService.getweaverBenefitPaymentList(paginationDTO);
        log.info("<--Ends .getweaverBenefitPaymentList-->");
        return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/getdetailsbyid/{id}", method = RequestMethod.GET)
    public @ResponseBody
    BaseDTO getweversBenefitPaymentById(@PathVariable("id") Long id) {
          // BaseDTO wrapperDto=bankToBankService.getDetailsById(id);
            return weaverBenefitPaymentService.getDetailsById(id);
    }
    
	@RequestMapping(value = "/approvePayment", method = RequestMethod.POST)
	public @ResponseBody BaseDTO approvePayment(@RequestBody WeaversBenefitPaymentDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			baseDTO = weaverBenefitPaymentService.approvePayment(requestDTO);
		} catch (Exception ex) {
			log.info("weaver Benefit Payment Controller ex:" + ex);
		}
		return baseDTO;
	}
	
	@RequestMapping(value = "/rejectPayment", method = RequestMethod.POST)
	public @ResponseBody BaseDTO rejectPayment(@RequestBody WeaversBenefitPaymentDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			baseDTO = weaverBenefitPaymentService.rejectPayment(requestDTO);
		} catch (Exception ex) {
			log.info("weaver Benefit Payment Controller ex:" + ex);
		}
		return baseDTO;
	}
	

	
	
	
	
}
