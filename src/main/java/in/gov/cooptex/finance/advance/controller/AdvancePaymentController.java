package in.gov.cooptex.finance.advance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.advance.service.AdvancePaymentService;
import in.gov.cooptex.finance.dto.AdvancePaymentDTO;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping("/accounts/advancePayment")
public class AdvancePaymentController {
	
	
	@Autowired
	AdvancePaymentService advancePaymentService;
	
	@RequestMapping(value = "/getPurhaseOrder/{purchaseOrderTypeid}/{supplierId}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> getAdvanceAmount(@PathVariable("purchaseOrderTypeid") Long purchaseOrderTypeid,@PathVariable("supplierId") Long supplierId) {
		log.info("<---AdvancePaymentController() Received purhaseOrderList  ---> ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = advancePaymentService.getPurchaseOrderListByID(purchaseOrderTypeid,supplierId);
		if (baseDTO != null && baseDTO.getStatusCode()==0 ) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getSocietyMasterList", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> getSocietyMasterList() {
		log.info("<---AdvancePaymentController() Received getSocietyMasterList  ---> ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = advancePaymentService.getSocietyMasterListByStatus();
		if (baseDTO != null && baseDTO.getStatusCode()==0 ) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	@RequestMapping(value = "/getActiveEmployeeMaster/{workingLocationById}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> getActiveEmployeeMaster(@PathVariable("workingLocationById") Long workingLocById) {
		log.info("<---AdvancePaymentController() Received getActiveEmployeeMaster  ---> ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = advancePaymentService.getActiveEmployeeMasterList(workingLocById);
		if (baseDTO != null && baseDTO.getStatusCode()==0 ) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	
	@RequestMapping(value = "/createAdvancePayment", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> createAdvancePayment(@RequestBody AdvancePaymentDTO advancePaymentDTO){
		BaseDTO baseDTO = new BaseDTO();
		log.info("<---AdvancePaymentController() createAdvancePayment ---> ");
		baseDTO = advancePaymentService.saveAdvancePayment(advancePaymentDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	@RequestMapping(value = "/editAdvancePayment", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> editAdvancePayment(@RequestBody AdvancePaymentDTO advancePaymentDTO){
		BaseDTO baseDTO = new BaseDTO();
		log.info("<---AdvancePaymentController() udpateAdvancePayment ---> ");
		baseDTO = advancePaymentService.udpateAdvancePayment(advancePaymentDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/searchDataLazy", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> lazySearch(@RequestBody PaginationDTO paginationDto){
		BaseDTO baseDTO = new BaseDTO();
		log.info("<---AdvancePaymentController() loadLazy ---> ");
		baseDTO = advancePaymentService.getAdvancePaymentLazy(paginationDto);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/viewAdvanceAmount/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> viewAdvanceAmount(@PathVariable("id") Long id) {
		log.info("<---AdvancePaymentController() Received view  ---> ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = advancePaymentService.viewAdvancePaymentByID(id);
		if (baseDTO != null && baseDTO.getStatusCode()==0 ) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getSupplierTypeMasterDetailsByID/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> getSupplierTypeMasterDetailsByID(@PathVariable("id") Long id) {
		log.info("<--- Received getAllSupplierMaster .create() ---> ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = advancePaymentService.getAllSupplierTypeMasterAllByID(id);
		if (baseDTO != null) {
			log.info("<--- Starts getAllSupplierMaster .fetch Success() ---> ");
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			log.error("<--- Starts IntendRequestController .created Failed() ---> ");
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}

	}

	@RequestMapping(value = "/updateAdvanceAmount", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> udpateAdvancePayment(@RequestBody AdvancePaymentDTO advancePaymentDTO){
		BaseDTO baseDTO = new BaseDTO();
		log.info("<---AdvancePaymentController() updateAdvancePayment ---> ");
		baseDTO = advancePaymentService.updateAdvancePayment(advancePaymentDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/deleteTargetDesignStageVerification/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> deleteTargetDesignStageVerification(@PathVariable("id") Long id) {
		log.info("<---AdvancePaymentController() Received statusVerification  ---> ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = advancePaymentService.deleteByIdAdvancePaymentCheckStatus(id);
		if (baseDTO != null && ( baseDTO.getStatusCode()==0 || baseDTO.getStatusCode()== 66677 ) ) {
			log.info("Stataus Code Success for delete second ");
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			log.info("Stataus Code Faile for delete second ");
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/deleteAdvancePayment/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> deleteTargetDesign(@PathVariable("id") Long id) {
		log.info("<---AdvancePaymentController() Received deleteAdvancePayment ---> ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = advancePaymentService.deleteByIdAdvancePayment(id);
		if (baseDTO != null && baseDTO.getStatusCode()==0) {
			log.info("<--- AdvancePaymentController() Delete Items Success() ---> ");
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			log.error("<--- AdvancePaymentController() Delete Failed() ---> ");
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
							  
	@RequestMapping(value = "/getSupplierMasterListByCircleID/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> getSupplierMasterListByCircleID(@PathVariable("id") Long id) {
		log.info("<--- Received AdvancePaymentController.getSupplierMasterListByCircleID .create() ---> ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = advancePaymentService.getSupplierMasterListByCircleID(id);
		if (baseDTO != null) {
			log.info("<--- Starts AdvancePaymentController.getSupplierMasterListByCircleID .fetch Success() ---> ");
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			log.error("<--- Starts AdvancePaymentController.getSupplierMasterListByCircleID Failed() ---> ");
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}

	}
	@RequestMapping(value = "/getAllSupplierType", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getAllSupplierType() {
		log.info("getAllSupplierType Controller:get()");
		BaseDTO baseDTO = advancePaymentService.getAllSupplierType();
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	 
	@RequestMapping(value = "/getallPurchaseOrderType", method = RequestMethod.GET)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "SUCCESS") })
	public ResponseEntity<BaseDTO> getallPurchaseOrderType() {
		log.info("PurchaseOrderTypeController:getAll()");
		BaseDTO baseDTO = advancePaymentService.getallPurchaseOrderType();
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/loadAllSocietyAdjustment", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> loadAllSocietyAdjustment(){
		BaseDTO baseDTO=advancePaymentService.loadAllSocietyAdjustment();
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
}
