package in.gov.cooptex.finance.advance.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.advance.service.AdvanceAmountCollectionSpService;
import in.gov.cooptex.finance.dto.AdvanceAmountCollectionSpDTO;
import in.gov.cooptex.operation.model.GovtSchemeType;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping("/advanceAmountCollectionSp")
public class AdvanceAmountCollectionSpController {

	@Autowired
	AdvanceAmountCollectionSpService advanceAmountCollectionSpService;
	
	@RequestMapping(value = "/createAdvance", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> govtSchemeTypeList(@RequestBody AdvanceAmountCollectionSpDTO dto) {
		log.info("<--Starts advanceAmountCollectionSp .save-->");
		BaseDTO baseDTO = advanceAmountCollectionSpService.saveAdvanceAmountCollectionSp(dto);
		log.info("<--Ends advanceAmountCollectionSp .save-->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/createAdvanceCollection", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<BaseDTO> createAdvanceCollection(@RequestBody AdvanceAmountCollectionSpDTO appConfig) {
		log.info("<--- Received advanceAmountCollectionSp  ---> ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = advanceAmountCollectionSpService.saveAdvanceAmountCollectionSp(appConfig);
		if (baseDTO != null && baseDTO.getStatusCode()==0 ) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			log.info("advanceAmountCollectionSp Failure");
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getAdvanceAmount/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> getAdvanceAmount(@PathVariable("id") Long id) {
		log.info("<--- Received getAdvanceAmount  ---> ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = advanceAmountCollectionSpService.getPurchaseSalesAdvance(id);
		if (baseDTO != null && baseDTO.getStatusCode()==0 ) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	
/*	@RequestMapping(value = "/searchDataLazy", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BaseDTO> lazySearch(@RequestBody PaginationDTO request){
		BaseDTO baseDTO = new BaseDTO();
		log.info("<--- loadLazy AdvanceAmountCollectionSpDTO Service ---> ");
		baseDTO = advanceAmountCollectionSpService.lazySearch(request);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}*/
	
	@RequestMapping(value = "/searchDataLazy", method = RequestMethod.POST)
	public ResponseEntity<BaseDTO> lazySearch(@RequestBody  PaginationDTO paginationDTO){
		BaseDTO baseDTO = new BaseDTO();
		log.info("<--- loadLazy AdvanceAmountCollectionSpDTO Service ---> ");
		baseDTO = advanceAmountCollectionSpService.lazySearch(paginationDTO);
		if (baseDTO != null) {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	@RequestMapping(value = "/getPurchaseSalesAdvanceView/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> getPurchaseSalesAdvanceView(@PathVariable("id") Long id) {
		log.info("<--- Received getPurchaseSalesAdvanceView  ---> ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = advanceAmountCollectionSpService.viewAdvanceAmountCollection(id);
		if (baseDTO != null && baseDTO.getStatusCode()==0 ) {
			log.info("getPurchaseSalesAdvanceView");
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			log.info("getPurchaseSalesAdvanceView Failure");
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	@RequestMapping(value = "/deletePurchaseSalesAdvance/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> deletePurchaseSalesAdvance(@PathVariable("id") Long id) {
		log.info("<--- Received getPurchaseSalesAdvanceView  ---> ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = advanceAmountCollectionSpService.deleteAdvanceAmountCollection(id);
		if (baseDTO != null && baseDTO.getStatusCode()==0 ) {
			log.info("deletePurchaseSalesAdvance");
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			log.info("deletePurchaseSalesAdvance Failure");
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	@RequestMapping(value = "/getPurchaseSalesAdvanceEdit/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<BaseDTO> getPurchaseSalesAdvanceEdit(@PathVariable("id") Long id) {
		log.info("<--- Received getPurchaseSalesAdvanceView  ---> ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = advanceAmountCollectionSpService.editAdvanceAmountCollection(id);
		if (baseDTO != null && baseDTO.getStatusCode()==0 ) {
			log.info("getPurchaseSalesAdvanceEdit");
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			log.info("getPurchaseSalesAdvanceEdit Failure");
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	@RequestMapping(value = "/getPurchaseSalesAdvanceUpdateEdit", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<BaseDTO> getPurchaseSalesAdvanceUpdateEdit(@RequestBody AdvanceAmountCollectionSpDTO dto) {
		log.info("<--- Received getPurchaseSalesAdvanceUpdateEdit  ---> ");
		BaseDTO baseDTO = new BaseDTO();
		baseDTO = advanceAmountCollectionSpService.updateAdvanceAmountCollectionSp(dto);
		if (baseDTO != null && baseDTO.getStatusCode()==0 ) {
			log.info("getPurchaseSalesAdvanceUpdateEdit");
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		} else {
			log.info("getPurchaseSalesAdvanceUpdateEdit Failure");
			return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.NO_CONTENT);
		}
	}
	
	
	@RequestMapping(value = "/customertype", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> findAllCustomerTypeForContractExportPlan() {
		log.info("findAllCustomerTypeForContractExportPlan()");
		BaseDTO baseDto = advanceAmountCollectionSpService.findAllCustomerType();
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}
	@RequestMapping(value = "/customerByCustomerTypeId/{customerTypeId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> loadAllCustomerByCustomerType(@PathVariable("customerTypeId") Long customerTypeId) {
		log.info("loadAllCustomerByCustomer()[" + customerTypeId + "]");
		BaseDTO baseDto = advanceAmountCollectionSpService.loadAllCustomerByCustomerType(customerTypeId);
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}	
	@RequestMapping(value = "/salesorderbyCustomerId/{customerId}", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> loadAllSalesOrderByCustomerDetails(@PathVariable("customerId") Long customerId) {
		log.info("loadAllSalesOrderByCustomer()[" + customerId + "]");
		BaseDTO baseDto = advanceAmountCollectionSpService.salesorderbyCustomerId(customerId);
		return new ResponseEntity<BaseDTO>(baseDto, HttpStatus.OK);
	}
	@RequestMapping(value = "/loadgovtschemetypes", method = RequestMethod.GET)
	public ResponseEntity<BaseDTO> govtSchemeTypeList() {
		log.info("<--Starts FDSController .govtSchemeTypeList-->");
		BaseDTO baseDTO = advanceAmountCollectionSpService.govtSchemeTypeList();
		log.info("<--Ends FDSController .segovtSchemeTypeListarch-->");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	@RequestMapping(value = "/getAllGovtSchemePlan", method = RequestMethod.POST)
	public BaseDTO getAllGovtSchemePlan(@RequestBody GovtSchemeType govtSchemeType) {
		log.info("GovtSchemePlanController getAllGovtSchemePlan () started");
		log.info("GovtSchemePlanController GovtSchemeType " + govtSchemeType);
		BaseDTO baseDTO = advanceAmountCollectionSpService.getAllGovtSchemePlan(govtSchemeType);
		log.info("GovtSchemePlanController getAllGovtSchemePlan () end " + baseDTO);
		return baseDTO;

	}
}
