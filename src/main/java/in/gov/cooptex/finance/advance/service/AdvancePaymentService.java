package in.gov.cooptex.finance.advance.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.repository.BankMasterRepository;
import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.service.NotificationEmailService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.enums.VoucherStatus;
import in.gov.cooptex.core.accounts.enums.VoucherTypeDetails;
import in.gov.cooptex.core.accounts.model.PurchaseSalesAdvance;
import in.gov.cooptex.core.accounts.model.SocietyInvoiceAdjustment;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.VoucherDetails;
import in.gov.cooptex.core.accounts.model.VoucherLog;
import in.gov.cooptex.core.accounts.model.VoucherNote;
import in.gov.cooptex.core.accounts.model.VoucherType;
import in.gov.cooptex.core.accounts.repository.BankBranchMasterRepository;
import in.gov.cooptex.core.accounts.repository.EntityBankBranchRepository;
import in.gov.cooptex.core.accounts.repository.SupplierTypeMasterRepository;
import in.gov.cooptex.core.accounts.repository.VoucherDetailsRepository;
import in.gov.cooptex.core.accounts.repository.VoucherLogRepository;
import in.gov.cooptex.core.accounts.repository.VoucherNoteRepository;
import in.gov.cooptex.core.accounts.repository.VoucherRepository;
import in.gov.cooptex.core.accounts.repository.VoucherTypeRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.SequenceName;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.CircleMaster;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.SequenceConfig;
import in.gov.cooptex.core.model.SocietyClassMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.PaymentModeRepository;
import in.gov.cooptex.core.repository.PurchaseOrderRepository;
import in.gov.cooptex.core.repository.PurchaseOrderTypeRepository;
import in.gov.cooptex.core.repository.SequenceConfigRepository;
import in.gov.cooptex.core.repository.SocietyAdjustmentMasterRepository;
import in.gov.cooptex.core.repository.SocietyClassRepository;
import in.gov.cooptex.core.repository.SupplierMasterRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.finance.advance.repository.PurchaseSalesAdvanceRepository;
import in.gov.cooptex.finance.dto.AdvancePaymentDTO;
import in.gov.cooptex.finance.model.SocietyAdjustmentMaster;
import in.gov.cooptex.finance.repository.SocietyInvoiceAdjustmentRepository;
import in.gov.cooptex.operation.enums.SupplierType;
import in.gov.cooptex.operation.model.PurchaseOrder;
import in.gov.cooptex.operation.model.PurchaseOrderType;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.model.SupplierTypeMaster;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
@Transactional
public class AdvancePaymentService {

	@Autowired
	ResponseWrapper responseWrapper;

	@Autowired
	PurchaseSalesAdvanceRepository purchaseSalesAdvanceRepository;

	@Autowired
	VoucherRepository voucherRepository;

	@Autowired
	VoucherDetailsRepository voucherDetailsRepository;

	@Autowired
	VoucherNoteRepository voucherNoteRepository;

	@Autowired
	VoucherLogRepository voucherLogRepository;

	@Autowired
	LoginService loginService;

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	SequenceConfigRepository sequenceConfigRepository;

	@Autowired
	EntityBankBranchRepository entityBankBranchRepository;

	@Autowired
	EntityManager entityManager;

	@Autowired
	BankBranchMasterRepository bankBranchMasterRepository;

	@Autowired
	BankMasterRepository bankMasterRepository;

	@Autowired
	PurchaseOrderRepository purchaseOrderRepository;

	@Autowired
	SocietyClassRepository societyClassRepository;

	@Autowired
	EmployeeMasterRepository employeeMasterRepository;

	@Autowired
	VoucherTypeRepository voucherTypeRepository;

	@Autowired
	UserMasterRepository userMasterRepository;

	@Autowired
	SupplierMasterRepository supplierMasterRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	PaymentModeRepository paymentModeRepository;

	@Autowired
	SocietyInvoiceAdjustmentRepository societyInvoiceAdjustmentRepository;

	@Autowired
	SupplierTypeMasterRepository supplierTypeMasterRepository;

	@Autowired
	PurchaseOrderTypeRepository purchaseOrderTypeRepository;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	@Autowired
	NotificationEmailService notificationEmailService;

	@Autowired
	SocietyAdjustmentMasterRepository societyAdjustmentMasterRepository;

	@Transactional
	public BaseDTO saveAdvancePayment(AdvancePaymentDTO dto) {
		log.info("<--AdvancePaymentService() .saveAdvancePayment() Started-->");
		BaseDTO baseDTO = new BaseDTO();

		try {
			SequenceConfig sequenceConfig = sequenceConfigRepository
					.findBySequenceName(SequenceName.valueOf(SequenceName.FINANCE_ADVANCE_PAYMENT.name()));

			if (sequenceConfig == null) {
				log.info("SequenceConfig - 'FINANCE_ADVANCE_PAYMENT' NOT FOUND");
				throw new RestException(ErrorDescription.SEQUENCE_CONFIG_NOT_EXIST);
			}

			Long sequenceNumber = sequenceConfig.getCurrentValue();
			log.info("saveAdvancePayment :: sequenceNumber ::" + sequenceNumber);
			sequenceConfig.setCurrentValue(sequenceConfig.getCurrentValue() + 1);
			sequenceConfigRepository.save(sequenceConfig);

			PurchaseSalesAdvance purchaseSalesAdvance = new PurchaseSalesAdvance();
			Voucher voucher = new Voucher();
			VoucherDetails voucherDetails = new VoucherDetails();
			VoucherNote voucherNote = new VoucherNote();
			VoucherLog voucherLog = new VoucherLog();

			String entityCode = entityMasterRepository.getCodebyEntityID(dto.getEntityMasterID());

			UserMaster userMaster = userMasterRepository.findOne(loginService.getCurrentUser().getId());

			voucher.setPaymentMode(dto.getPaymentMode());
			voucher.setNetAmount(dto.getTotalAdvanceAmount());
			voucher.setFromDate(new Date());
			voucher.setToDate(new Date());
			voucher.setName(VoucherTypeDetails.ADVANCE_PAYMENT.toString());

			VoucherType voucherType = voucherTypeRepository.findByName(VoucherTypeDetails.Payment.toString());

			String refrenceNumberPrefix = entityCode + sequenceConfig.getSeparator() + sequenceConfig.getPrefix()
					+ AppUtil.getCurrentMonthString() + AppUtil.getCurrentYearString();

			voucher.setReferenceNumberPrefix(refrenceNumberPrefix);
			voucher.setReferenceNumber(sequenceNumber);
			voucher.setName(VoucherTypeDetails.ADVANCE_PAYMENT.toString());
			voucher.setVoucherType(voucherType);
			voucher.setEntityMaster(entityMasterRepository.findById(dto.getEntityMasterID()));
			voucher.setNarration(dto.getNarration());

			voucherDetails.setAmount(dto.getTotalAdvanceAmount());

			voucherNote.setNote(dto.getNote());
			voucherNote.setFinalApproval(dto.getForwardFor());
			UserMaster forwardTo = userMasterRepository.findOne((dto.getForwardTo().getId()));
			voucherNote.setForwardTo(forwardTo);
			voucherNote.setVoucherNote(voucherNote);

			voucherLog.setCreatedBy(userMaster);
			voucherLog.setCreatedByName(userMaster.getUsername());
			voucherLog.setCreatedDate(new Date());
			voucherLog.setStatus(VoucherStatus.SUBMITTED);
			voucherLog.setUserMaster(userMaster);
			purchaseSalesAdvance.setAdvanceType(dto.getAdvanceType());
			purchaseSalesAdvance.setAmount(dto.getTotalAdvanceAmount());
			if (dto.getEntityMaster()!=null && dto.getEntityMaster().getId() != null) {
				purchaseSalesAdvance.setEntityMaster(entityMasterRepository.findOne(dto.getEntityMaster().getId()));
			}
			Boolean isStaffAdvance = dto.getStaffAdvanceFlag();
			Boolean isSupplierAdvance = dto.getSupplierAdvanceFlag();
			if (isStaffAdvance) {
				log.info("StaffAdvance Type save processed ");
				purchaseSalesAdvance.setGovtSchemePlan(dto.getGovtSchemePlan());
				EmployeeMaster emp = employeeMasterRepository.findOne(dto.getEmployeeMasterID());
				purchaseSalesAdvance.setEmployeeMaster(emp);
				voucherDetails.setEmpMaster(emp);
			} else if (isSupplierAdvance) {
				log.info("SupplierAdvance Type save processed ");
				voucher.setSupplierType(dto.getSupplierTypeMaster());
				purchaseSalesAdvance
						.setSupplierMaster(supplierMasterRepository.findOne(dto.getSupplierMaster().getId()));
				purchaseSalesAdvance.setPurchaseOrder(purchaseOrderRepository.findOne(dto.getPurchaseOrder().getId())); //
			} else {
				log.error("::AdvancePaymentService -> saveAdvancePayment() ->Invalid Type :: " + dto.getAdvanceType());
			}

			Voucher voucherReturn = voucherRepository.save(voucher);
			log.info("<--AdvancePaymentService() Voucher Save Success  -->");

			voucherDetails.setVoucher(voucherReturn);
			voucherDetailsRepository.save(voucherDetails);
			log.info("<--AdvancePaymentService() VoucherDetails Save Success  -->");

			voucherNote.setVoucher(voucherReturn);
			voucherNoteRepository.save(voucherNote);
			log.info("<--AdvancePaymentService() voucherNote Save Success  -->");

			voucherLog.setVoucher(voucherReturn);
			voucherLogRepository.save(voucherLog);
			log.info("<--AdvancePaymentService() voucherLog Save Success  -->");

			purchaseSalesAdvance.setVoucher(voucherReturn);
			purchaseSalesAdvanceRepository.save(purchaseSalesAdvance);

			log.info("<--AdvancePaymentService() saveAdvancePayment  Save Success  -->");
			log.info("Voucher ID :" + voucherReturn.getId());
			if (dto.getSupplierMaster() != null) {
				if (dto.getSupplierTypeMaster().getCode().toString().equals(SupplierType.SOCIETY.toString())) {
					log.info("Society Type save processed ");
					SupplierTypeMaster supplierTypeMaster = supplierTypeMasterRepository
							.findByCode(SupplierType.SOCIETY.toString());
					SocietyAdjustmentMaster societyAdjustmentMaster = new SocietyAdjustmentMaster();
					if (dto.getSocietyAdjustmentMaster() != null) {
						societyAdjustmentMaster = societyAdjustmentMasterRepository
								.findOne(dto.getSocietyAdjustmentMaster().getId());
					}
					SocietyInvoiceAdjustment society = new SocietyInvoiceAdjustment();
					society.setVoucherId(voucherReturn.getId());
					society.setSocietyId(supplierTypeMaster.getId());
					society.setAdjustmentAmount(dto.getTotalAdvanceAmount());
					society.setSocietyAdjustment(societyAdjustmentMaster);
					societyInvoiceAdjustmentRepository.save(society);
					log.info("<--AdvancePaymentService() society Save Success  -->");
				}
			}
			baseDTO.setResponseContent(purchaseSalesAdvance);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());

			notificationEmailService.sendMailAndNotificationForForward(voucherNote, voucherLog, null);
		} catch (Exception e) {
			log.error("Exception in AdvancePaymentService() ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(baseDTO);
	}

	// edit rejected AdvancePayment update
	public BaseDTO udpateAdvancePayment(AdvancePaymentDTO dto) {
		log.info("<--AdvancePaymentService(). udpateAdvancePayment() Started-->");
		BaseDTO baseDTO = new BaseDTO();

		try {
			PurchaseSalesAdvance purchaseSalesAdvance = purchaseSalesAdvanceRepository
					.findOne(dto.getPurchaseSalesAdvanceID());
			// PurchaseSalesAdvance purchaseSalesAdvance = new PurchaseSalesAdvance();
			Voucher voucher = voucherRepository.findOne(dto.getVoucherID());
			VoucherDetails voucherDetails = voucherDetailsRepository.findOne(dto.getVoucherDetailsID());
			VoucherNote voucherNote = new VoucherNote();
			VoucherLog voucherLog = new VoucherLog();

			UserMaster userMaster = userMasterRepository.findOne(loginService.getCurrentUser().getId());

			voucher.setPaymentMode(dto.getPaymentMode());
			voucher.setNetAmount(dto.getTotalAdvanceAmount());
			/*
			 * voucher.setFromDate(new Date()); voucher.setToDate(new Date());
			 */
			voucher.setName(VoucherTypeDetails.ADVANCE_PAYMENT.toString());

			VoucherType voucherType = voucherTypeRepository.findByName(VoucherTypeDetails.Payment.toString());

			// voucher.setName(VoucherTypeDetails.ADVANCE_PAYMENT.toString());
			voucher.setVoucherType(voucherType);

			voucher.setNarration(dto.getNarration());
			voucher.setModifiedBy(userMaster);
			voucher.setModifiedDate(new Date());

			voucherDetails.setAmount(dto.getTotalAdvanceAmount());
			voucherDetails.setModifiedBy(userMaster);
			voucherDetails.setModifiedDate(new Date());

			voucherNote.setNote(dto.getNote());
			voucherNote.setFinalApproval(dto.getForwardFor());
			UserMaster forwardTo = userMasterRepository.findOne((dto.getForwardTo().getId()));
			voucherNote.setForwardTo(forwardTo);
			voucherNote.setModifiedBy(userMaster);
			voucherNote.setModifiedDate(new Date());
			voucherNote.setVoucherNote(voucherNote);

			voucherLog.setModifiedBy(userMaster);
			voucherLog.setModifiedDate(new Date());
			voucherLog.setStatus(VoucherStatus.SUBMITTED);

			voucherLog.setUserMaster(userMaster);

			purchaseSalesAdvance.setAdvanceType(dto.getAdvanceType());
			purchaseSalesAdvance.setModifiedBy(userMaster);

			purchaseSalesAdvance.setModifiedDate(new Date());
			// purchaseSalesAdvance.setVersion(0l);
			purchaseSalesAdvance.setAmount(dto.getTotalAdvanceAmount());

			voucher.setSupplierType(dto.getSupplierTypeMaster());
			purchaseSalesAdvance.setSupplierMaster(supplierMasterRepository.findOne(dto.getSupplierMaster().getId()));
			purchaseSalesAdvance.setPurchaseOrder(purchaseOrderRepository.findOne(dto.getPurchaseOrder().getId())); //

			Voucher voucherReturn = voucherRepository.save(voucher);
			log.info("<--AdvancePaymentService() Voucher update Success  -->");

			voucherDetails.setVoucher(voucherReturn);

			voucherDetailsRepository.save(voucherDetails);
			log.info("<--AdvancePaymentService() VoucherDetails Update Success  -->");

			voucherNote.setVoucher(voucherReturn);

			voucherNoteRepository.save(voucherNote);
			log.info("<--AdvancePaymentService() voucherNote Update Success  -->");

			voucherLog.setVoucher(voucherReturn);
			voucherLogRepository.save(voucherLog);
			log.info("<--AdvancePaymentService() voucherLog Update Success  -->");

			purchaseSalesAdvance.setVoucher(voucherReturn);
			purchaseSalesAdvanceRepository.save(purchaseSalesAdvance);

			log.info("<--AdvancePaymentService() saveAdvancePayment  Update Success  -->");
			log.info("Voucher ID :" + voucherReturn.getId());

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());

		} catch (Exception e) {
			log.error("Exception in AdvancePaymentService() ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(baseDTO);
	}

	// load PurchaseOrderList by PurchaseOrderType id
	public BaseDTO getPurchaseOrderListByID(Long purchaseOrderTypeid, Long supplierId) {
		log.info("<--AdvancePaymentService() .getPurchaseOrderListByID() Started-->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<PurchaseOrder> purchaseOrderList = purchaseOrderRepository
					.loadPurchaseOrderByPurchaseOrderTypeId(purchaseOrderTypeid, supplierId);
			baseDTO.setResponseContent(purchaseOrderList);
			log.info("<--AdvancePaymentService() fetch success-->");
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.error("Exception in AdvancePaymentService() ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(baseDTO);
	}

	// load SocietyClassMaster list by ActiveStatus = true;
	public BaseDTO getSocietyMasterListByStatus() {
		log.info("<--AdvancePaymentService() .getSocietyMasterListByStatus() Started-->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<SocietyClassMaster> societyClassMasterList = societyClassRepository.findByStatus(true);
			baseDTO.setResponseContent(societyClassMasterList);
			log.info("<--getSocietyMasterListByStatus() fetch success-->");
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.error("Exception in getSocietyMasterListByStatus() ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(baseDTO);
	}

	// load Active EmployeeMaster list
	public BaseDTO getActiveEmployeeMasterList(Long workingLocById) {
		log.info("<--AdvancePaymentService() .getActiveEmployeeMasterList() Started-->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			/*
			 * List<EmployeeMaster> employeeMasterList = employeeMasterRepository
			 * .findEmployeeListByWorkLocation(workingLocById);
			 */

			List<Object> employeeObjList = employeeMasterRepository.loadEmployeeListByWorkLocationId(workingLocById);
			List<EmployeeMaster> employeeMasterList = new ArrayList<>();
			log.info(employeeObjList);
			Iterator<?> employee = employeeObjList.iterator();
			while (employee.hasNext()) {
				Object ob[] = (Object[]) employee.next();
				EmployeeMaster employeeMasterObj = new EmployeeMaster();
				employeeMasterObj.setId(Long.valueOf((ob[0]).toString()));
				employeeMasterObj.setFirstName((String) ob[1]);
				employeeMasterObj.setLastName((String) ob[2]);
				employeeMasterObj.setEmpCode((String) ob[3]);
				employeeMasterObj.setMiddleName((String) ob[4]);
				employeeMasterList.add(employeeMasterObj);
			}
			if (employeeMasterList != null) {
				baseDTO.setResponseContent(employeeMasterList);
				log.info("<--getActiveEmployeeMasterList() fetch success-->");
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			}

		} catch (Exception e) {
			log.error("Exception in getActiveEmployeeMasterList() ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(baseDTO);
	}

	// This is method is used for LazySearch
	// getallDesignTargetlazy
	public BaseDTO getAdvancePaymentLazy(PaginationDTO paginationDto) {
		log.info(" getallDesignTargetlazy  called...");
		BaseDTO baseDTO = new BaseDTO();

		try {

			Integer total = 0;
			Integer start = paginationDto.getFirst(), pageSize = paginationDto.getPageSize();
			start = start * pageSize;

			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();

			String queryName = "FINANCE_ADVANCE_PAYMENT_LIST_QUERY";
			String queryNameCount = "FINANCE_ADVANCE_PAYMENT_LIST_QUERY_COUNT";

			ApplicationQuery applicationQuery = applicationQueryRepository.findByQueryName(queryName);

			String mainQuery = applicationQuery.getQueryContent().trim();

			applicationQuery = applicationQueryRepository.findByQueryName(queryNameCount);

			String queryCount = applicationQuery.getQueryContent().trim();

			// String mainQuery = query;
			if (paginationDto.getFilters().get("id") != null) {
				mainQuery += " and t.id='" + paginationDto.getFilters().get("id") + "'";
			}
			if (paginationDto.getFilters().get("advanceType") != null) {

				mainQuery += " and upper(advance_type) like upper('%" + paginationDto.getFilters().get("advanceType")
						+ "%')";
			}

			if (paginationDto.getFilters().get("employeeCodeName") != null) {

				if (AppUtil.isInteger(paginationDto.getFilters().get("employeeCodeName").toString())) {
					mainQuery += " and emp_code like '%" + paginationDto.getFilters().get("employeeCodeName") + "%'";
				} else {
					mainQuery += " and upper(first_name) like upper('%"
							+ paginationDto.getFilters().get("employeeCodeName") + "%')";
				}
			}
			if (paginationDto.getFilters().get("supplierCodeName") != null) {

				if (AppUtil.isInteger(paginationDto.getFilters().get("supplierCodeName").toString())) {
					mainQuery += " and s.code like '%" + paginationDto.getFilters().get("supplierCodeName") + "%'";
				} else {
					mainQuery += " and upper(s.name) like upper('%" + paginationDto.getFilters().get("supplierCodeName")
							+ "%')";

				}
			}
			if (paginationDto.getFilters().get("supplierTypeCodeName") != null) {
				mainQuery += " and supplier_type like '%" + paginationDto.getFilters().get("supplierTypeCodeName")
						+ "%'";
			}
			if (paginationDto.getFilters().get("purchaseOrderTypeCodeName") != null) {
				mainQuery += " and upper(pot.code) like upper('%"
						+ paginationDto.getFilters().get("purchaseOrderTypeCodeName") + "%') "
						+ " or upper(pot.name) like upper('%"
						+ paginationDto.getFilters().get("purchaseOrderTypeCodeName") + "%') ";
			}
			if (paginationDto.getFilters().get("status") != null) {
				mainQuery += " and upper(l.status) like upper('%" + paginationDto.getFilters().get("status") + "%')";
			}
			if (paginationDto.getFilters().get("createdDate") != null) {
				Date createdDate = new Date((Long) paginationDto.getFilters().get("createdDate"));
				mainQuery += " and t.created_date::date = date '" + createdDate + "'";
			}

			if (paginationDto.getFilters() == null || paginationDto.getFilters().isEmpty()) {
				mainQuery = mainQuery.replace("where t.advance_type in('Supplier Advance')", "");
			}
			
			
			if (paginationDto.getFilters().get("voucherno") != null) {
				mainQuery += " and reference_number_prefix like '%" + paginationDto.getFilters().get("voucherno")
						+ "%'";
			}
			
			if (paginationDto.getFilters().get("paymentModeName") != null) {
				mainQuery += " and payment_mode like '%" + paginationDto.getFilters().get("paymentModeName")
						+ "%'";
			}
			
			/*if (paginationDto.getFilters().get("amount") != null) {
				mainQuery += " and amount like '%" + paginationDto.getFilters().get("amount")
						+ "%'";
			}*/
			
			String countQuery = mainQuery.replace(
					"select t.id,t.advance_type,l.status,n.emp_id,ee.emp_code,ee.first_name,t.supplier_id,t.purchase_order_id,t.created_date, "
							+ "s.name,s.code,st.code as supplier_type,po.po_number_prefix,pot.code as supplierTypeCode,pot.name as supplierTypeName "
							+ ",vnt.forward_to",
					" select count(*) as count ");

			// String countQuery2 = countQuery.replace("where t.advance_type in('Supplier
			// Advance')", "");

			// countQuery2 = mainQuery.replace(queryCount, " select count(*) as count ");

			countData = jdbcTemplate.queryForList(countQuery);

			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					total = Integer.parseInt(data.get("count").toString().replace(",", ""));
			}

			if (paginationDto.getSortField() == null)
				mainQuery += " order by id desc limit " + pageSize + " offset " + start + ";";

			if (paginationDto.getSortField() != null && paginationDto.getSortOrder() != null) {

				if (paginationDto.getSortField().equals("id") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by id asc ";
				if (paginationDto.getSortField().equals("id") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by id desc ";

				if (paginationDto.getSortField().equals("advanceType")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by advance_type asc  ";
				if (paginationDto.getSortField().equals("advanceType")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by advance_type desc  ";

				if (paginationDto.getSortField().equals("status") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by l.status asc  ";
				if (paginationDto.getSortField().equals("status") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by l.status desc  ";

				if (paginationDto.getSortField().equals("employeeCodeName")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by emp_code asc  ";
				if (paginationDto.getSortField().equals("employeeCodeName")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by emp_code desc  ";

				if (paginationDto.getSortField().equals("supplierCodeName")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by code asc  ";
				if (paginationDto.getSortField().equals("supplierCodeName")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by code desc  ";

				if (paginationDto.getSortField().equals("supplierTypeCodeName")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by supplier_type asc  ";
				if (paginationDto.getSortField().equals("supplierTypeCodeName")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by supplier_type desc  ";

				if (paginationDto.getSortField().equals("purchaseOrderTypeCodeName")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by supplierTypeCode asc  ";
				if (paginationDto.getSortField().equals("purchaseOrderTypeCodeName")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by supplierTypeCode desc  ";

				if (paginationDto.getSortField().equals("createdDate")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by t.created_Date asc  ";
				if (paginationDto.getSortField().equals("createdDate")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by t.created_Date desc  ";
				
				
				if (paginationDto.getSortField().equals("voucherno")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by t.reference_number_prefix asc  ";
				if (paginationDto.getSortField().equals("voucherno")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by t.reference_number_prefix desc  ";
				
				
				if (paginationDto.getSortField().equals("voucherDate")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by v.created_Date asc  ";
				if (paginationDto.getSortField().equals("voucherDate")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by v.created_Date desc  ";
				
				
				if (paginationDto.getSortField().equals("payment_mode")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by pm.payment_mode asc  ";
				if (paginationDto.getSortField().equals("voucherDate")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by pm.payment_mode desc  ";
				
				
				if (paginationDto.getSortField().equals("amount")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by t.amount asc  ";
				if (paginationDto.getSortField().equals("amount")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by t.amount desc  ";
				

				mainQuery += " limit " + pageSize + " offset " + start + ";";
			}

			List<AdvancePaymentDTO> advancePaymentDTOList = new ArrayList<>();
			listofData = jdbcTemplate.queryForList(mainQuery);
			for (Map<String, Object> data : listofData) {
				AdvancePaymentDTO advancePaymentDTO = new AdvancePaymentDTO();
				if (data.get("id") != null)
					advancePaymentDTO.setId(Long.parseLong(data.get("id").toString()));
				if (data.get("advance_type") != null)
					advancePaymentDTO.setAdvanceType(data.get("advance_type").toString());
				if (data.get("status") != null)
					advancePaymentDTO.setStatus(data.get("status").toString());
				if (data.get("emp_id") != null) {
					advancePaymentDTO.setEmployeeCodeName(
							data.get("emp_code").toString().concat(" / ").concat(data.get("first_name").toString()));
				} else {
					advancePaymentDTO.setEmployeeCodeName(" - ");
				}
				if (data.get("supplier_id") != null) {
					advancePaymentDTO.setSupplierCodeName(
							data.get("code").toString().concat(" / ").concat(data.get("name").toString()));
				} else {
					advancePaymentDTO.setSupplierCodeName(" - ");
				}
				if (data.get("supplier_type") != null) {
					advancePaymentDTO.setSupplierTypeCodeName(data.get("supplier_type").toString());
				} else {
					advancePaymentDTO.setSupplierTypeCodeName(" - ");
				}

				if (data.get("supplierTypeCode") != null) {
					advancePaymentDTO.setPurchaseOrderTypeCodeName(data.get("supplierTypeCode").toString().concat(" / ")
							.concat(data.get("supplierTypeName").toString()));
				} else {
					advancePaymentDTO.setPurchaseOrderTypeCodeName(" - ");
				}
				if (data.get("po_number_prefix") != null) {
					advancePaymentDTO.setPurchaseOrderPONumberPrefix(data.get("po_number_prefix").toString());
				} else {
					advancePaymentDTO.setPurchaseOrderTypeCodeName(" - ");
				}

				if (data.get("voucherno") != null) {
					advancePaymentDTO.setVoucherNo(data.get("voucherno").toString());
				} else {
					advancePaymentDTO.setVoucherNo("-");
				}

				if (data.get("payment_mode") != null) {
					advancePaymentDTO.setPaymentModeName(data.get("payment_mode").toString());
				} else {
					advancePaymentDTO.setPaymentModeName("-");
				}

				if (data.get("voucherDate") != null)
					advancePaymentDTO.setVoucherDate((Date) data.get("voucherDate"));

				if (data.get("created_Date") != null)
					advancePaymentDTO.setCreatedDate((Date) data.get("created_Date"));

				if (data.get("amount") != null)
					advancePaymentDTO.setAmount(Double.valueOf(data.get("amount").toString()));

				if (data.get("forward_to") != null) {
					advancePaymentDTO.setForwardToUserID(Long.parseLong(data.get("forward_to").toString()));
				}
				advancePaymentDTOList.add(advancePaymentDTO);
			}
			baseDTO.setResponseContent(advancePaymentDTOList);
			baseDTO.setTotalRecords(countData.size());
			log.info("countQuery " + countQuery);
			log.info("mainQuery " + mainQuery);
			log.info("total " + total);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (Exception exp) {
			log.error("Exception Cause  : ", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	// view AdvancePayment By_PurhcaseSalesAdvance_ID
	public BaseDTO viewAdvancePaymentByID(Long id) {
		log.info("<--AdvancePaymentService() .viewAdvancePaymentByID() Started-->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			PurchaseSalesAdvance purchaseSalesAdvance = purchaseSalesAdvanceRepository.findOne(id);
			AdvancePaymentDTO advancePaymentDTO = new AdvancePaymentDTO();
			advancePaymentDTO.setId(purchaseSalesAdvance.getId());
			advancePaymentDTO.setCreatedBy(userMasterRepository.findOne(purchaseSalesAdvance.getCreatedBy().getId()));
			advancePaymentDTO.setModifiedBy(userMasterRepository.findOne(purchaseSalesAdvance.getModifiedBy().getId()));
			advancePaymentDTO.setTotalAdvanceAmount(purchaseSalesAdvance.getAmount());

			Voucher voucher = voucherRepository.findOne(purchaseSalesAdvance.getVoucher().getId());
			VoucherNote voucherNote = voucherNoteRepository.findByVoucherId(voucher.getId());
			VoucherLog voucherLog = voucherLogRepository.findByVoucherId(voucher.getId());
			VoucherDetails voucherDetails = voucherDetailsRepository.getVoucherDetailsByVoucherID(voucher.getId());

			advancePaymentDTO.setForwardTo(userMasterRepository.findOne(voucherNote.getForwardTo().getId()));
			advancePaymentDTO.setForwardFor(voucherNote.getFinalApproval());
			advancePaymentDTO.setPaymentMode(paymentModeRepository.findOne(voucher.getPaymentMode().getId()));
			advancePaymentDTO.setNote(voucherNote.getNote());
			advancePaymentDTO.setStatus(voucherLog.getStatus().toString());
			advancePaymentDTO.setAdvanceBasedOn(purchaseSalesAdvance.getAdvanceType());
			advancePaymentDTO.setVoucherID(voucher.getId());
			advancePaymentDTO.setVoucherDetailsID(voucherDetails.getId());
			advancePaymentDTO.setVoucherNoteID(voucherNote.getId());
			advancePaymentDTO.setAdvanceType(purchaseSalesAdvance.getAdvanceType());
			advancePaymentDTO.setPurchaseSalesAdvanceID(purchaseSalesAdvance.getId());
			advancePaymentDTO.setEntityCodeName(purchaseSalesAdvance.getEntityMaster()!=null ? 
					purchaseSalesAdvance.getEntityMaster().getCode() +"/"+
					purchaseSalesAdvance.getEntityMaster().getName():null);

			advancePaymentDTO.setEmployeeCodeName(purchaseSalesAdvance.getEmployeeMaster() != null
					? purchaseSalesAdvance.getEmployeeMaster().getEmpCode() + "/"
							+ purchaseSalesAdvance.getEmployeeMaster().getFirstName()
							+ purchaseSalesAdvance.getEmployeeMaster().getLastName()
					: null);
			advancePaymentDTO.setCreatedDate(purchaseSalesAdvance.getCreatedDate());
			advancePaymentDTO.setModifiedDate(voucherLog.getModifiedDate());

			if (purchaseSalesAdvance.getSupplierMaster() != null) {
				SupplierMaster supplierMaster = supplierMasterRepository
						.loadSupplierMasterBySupplierID(purchaseSalesAdvance.getSupplierMaster().getId());
				advancePaymentDTO.setSupplierTypeMaster(
						supplierTypeMasterRepository.findOne(supplierMaster.getSupplierTypeMaster().getId()));
				advancePaymentDTO.setSupplierMaster(supplierMaster);
			}
			/*
			 * PurchaseOrder purhcaseOrder = purchaseOrderRepository
			 * .findOne(purchaseSalesAdvance.getPurchaseOrder().getId()); PurchaseOrderType
			 * purchaseOrderType = purchaseOrderTypeRepository
			 * .findOne(purhcaseOrder.getPurchaseOrderType().getId());
			 * advancePaymentDTO.setPurchaseOrder(purhcaseOrder);
			 * advancePaymentDTO.setPurchaseOrderType(purchaseOrderType);
			 */
			advancePaymentDTO.setNarration(voucher.getNarration());

			// List<AdvancePaymentDTO> logList = getCreatedByNoteLog(voucher.getId());
			List<Map<String, Object>> employeeData = new ArrayList<Map<String, Object>>();
			if (voucher != null) {
				log.info("<<<:::::::voucher::::not Null::::>>>>" + voucher);
				ApplicationQuery applicationQueryForlog = applicationQueryRepository
						.findByQueryName("VOUCHER_LOG_EMPLOYEE_DETAILS");
				if (applicationQueryForlog == null || applicationQueryForlog.getId() == null) {
					log.info("Application Query For Log Details not found for query name : " + applicationQueryForlog);
					baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
					return baseDTO;
				}
				String logquery = applicationQueryForlog.getQueryContent().trim();
				log.info("<=========VOUCHER_LOG_EMPLOYEE_DETAILS Query Content ======>" + logquery);
				logquery = logquery.replace(":voucherId", "'" + voucher.getId().toString() + "'");
				log.info("Query Content For VOUCHER_LOG_EMPLOYEE_DETAILS After replaced plan id View query : "
						+ logquery);
				employeeData = jdbcTemplate.queryForList(logquery);
				log.info("<=========VOUCHER_LOG_EMPLOYEE_DETAILS Employee Data======>" + employeeData);
				baseDTO.setTotalListOfData(employeeData);
			} else {
				log.info("::::::::::voucher Object null::::::::::::");
			}
			// advancePaymentDTO.setVoucherLogList(logList);
			baseDTO.setResponseContent(advancePaymentDTO);
			log.info("purchaseSalesAdvance :" + purchaseSalesAdvance.getId());
			log.info("<--AdvancePaymentService() view success-->");
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.error("Exception in AdvancePaymentService() view ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO getAllSupplierTypeMasterAllByID(Long id) {

		log.info("<-Inside SERVICE-Starts SupplierMaster-->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<SupplierMaster> supplierMasterList = supplierMasterRepository.getBySupplierTypeMasterByID(id);
			baseDTO.setResponseContent(supplierMasterList);
			baseDTO.setTotalRecords(supplierMasterList.size());
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("<-SupplierMaster GetAll Data Success-->");
		} catch (Exception exception) {
			log.error("exception Occured SupplierMaster : ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	// update
	public BaseDTO updateAdvancePayment(AdvancePaymentDTO dto) {
		log.info("<--AdvancePaymentService() .udpateAdvancePayment() Started-->");
		BaseDTO baseDTO = new BaseDTO();
		try {

			VoucherLog voucherLog = new VoucherLog();

			UserMaster userMaster = userMasterRepository.findOne(loginService.getCurrentUser().getId());

			// voucherNote.setNote(dto.getNote());
			if (!dto.getStatus().equalsIgnoreCase(VoucherStatus.REJECTED.toString())) {
				VoucherNote voucherNote = new VoucherNote();
				voucherNote.setFinalApproval(dto.getForwardFor());
				UserMaster forwardTo = userMasterRepository.findOne((dto.getForwardTo().getId()));
				voucherNote.setForwardTo(forwardTo);
				voucherNote.setVoucherNote(voucherNoteRepository.findOne(dto.getVoucherNoteID()));
				voucherNote.setNote(dto.getNote());
				// voucherNote.setCreatedBy(userMaster);
				// voucherNote.setCreatedByName(userMaster.getUsername());
				// voucherNote.setCreatedDate(new Date());
				voucherNote.setVoucher(voucherRepository.findOne(dto.getVoucherID()));
				voucherNoteRepository.save(voucherNote);
				log.info("<--AdvancePaymentService() voucherNote Update Success  -->");
			}
			// voucherLog.setCreatedBy(userMaster);
			// voucherLog.setCreatedByName(userMaster.getUsername());
			// voucherLog.setCreatedDate(new Date());                                                                                                                                   
			voucherLog.setVoucher(voucherRepository.findOne(dto.getVoucherID()));
			VoucherStatus status = VoucherStatus.valueOf(dto.getStatus());
			voucherLog.setStatus(status);
			voucherLog.setRemarks(dto.getRemarks());
			voucherLog.setUserMaster(userMaster);
			voucherLogRepository.save(voucherLog);
			log.info("<--AdvancePaymentService() voucherLog Update Success  -->");

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());

		} catch (Exception e) {
			log.error("Exception in AdvancePaymentService() ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(baseDTO);
	}

	// Check Stage before Delete if Stage Conflict or not
	public BaseDTO deleteByIdAdvancePaymentCheckStatus(Long id) {
		log.info("AdvancePaymentService CheckStage  method started [" + id + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			VoucherLog voucherLog = voucherLogRepository.getVoucherLogByID(id);
			VoucherStatus status = VoucherStatus.valueOf(voucherLog.getStatus().toString());
			if (status.equals(VoucherStatus.FINALAPPROVED) || status.equals(VoucherStatus.APPROVED)) {
				baseDTO.setStatusCode(ErrorDescription.PRODUCT_DESIGN_DELETE_STAGE_WARNING.getErrorCode());
				log.info("AdvancePaymentService Delete Stage Confilict");
			} else {
				log.info("AdvancePaymentService Delete Second Processing");
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
			return baseDTO;
		} catch (RestException restException) {
			log.error("AdvancePaymentService deleteById CheckStage RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (DataIntegrityViolationException exception) {
			log.error("AdvancePaymentService deleteById CheckStage  DataIntegrityViolationException ", exception);
			if (exception.getCause().getCause() instanceof PSQLException) {
				baseDTO.setStatusCode(ErrorDescription.CANNOT_DELETE_REFERENCED_RECORD.getErrorCode());
			} else {
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			}
		} catch (Exception exception) {
			log.error("AdvancePaymentService deleteById CheckStage  Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("AdvancePaymentService deleteById CheckStage  method completed");
		return baseDTO;
	}

	// //Delete Confirm After User Second Verification
	public BaseDTO deleteByIdAdvancePayment(Long id) {
		log.info("AdvancePaymentService deleteById method started PurchaseSalesAdvance ID :[" + id + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			PurchaseSalesAdvance purchaseSalesAdvance = purchaseSalesAdvanceRepository.findOne(id);

			Long voucherID = purchaseSalesAdvance.getVoucher().getId();
			log.info("Delete Started Voucher ID is " + voucherID);
			voucherNoteRepository.deleteVoucherNoteByVoucherID(voucherID);
			log.info("Delete Voucher Note Success");
			voucherLogRepository.deleteVoucherLogByVoucherID(voucherID);
			log.info("Delete Voucher Log Success");
			voucherDetailsRepository.deleteVoucherDetailsByVoucherID(voucherID);
			log.info("Delete VoucherDetails Success");

			purchaseSalesAdvanceRepository.deletePurchaseSalesAdvanceBYID(id);
			log.info("Delete PurchaseSalesAdvance Success");

			voucherRepository.deleteVoucherBYID(id);
			log.info("Delete Voucher Success");

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("ProductDesignTargetService deleteById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (DataIntegrityViolationException exception) {
			log.error("ProductDesignTargetService deleteById DataIntegrityViolationException ", exception);
			if (exception.getCause().getCause() instanceof PSQLException) {
				baseDTO.setStatusCode(ErrorDescription.CANNOT_DELETE_REFERENCED_RECORD.getErrorCode());
			} else {
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			}
		} catch (Exception exception) {
			log.error("ProductDesignTargetService deleteById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("ProductDesignTargetService deleteById method completed");
		return responseWrapper.send(baseDTO);
	}

	public List<AdvancePaymentDTO> getCreatedByNoteLog(Long id) {
		log.info("received getLog list");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<VoucherNote> productDesignTargetNoteList = voucherNoteRepository.getAllVoucherNoteListByID(id);
			List<AdvancePaymentDTO> dtoList = new ArrayList<AdvancePaymentDTO>();

			for (VoucherNote note : productDesignTargetNoteList) {
				AdvancePaymentDTO dto = new AdvancePaymentDTO();
				dto.setCreatedDate(note.getCreatedDate());
				dto.setNote(dto.getNote());
				EmployeeMaster employeeMaster = employeeMasterRepository
						.findEmployeeByUser(note.getCreatedBy().getId());
				if (employeeMaster != null) {
					String designationName = employeeMaster.getPersonalInfoEmployment().getDesignation().getName();
					dto.setDesignation(designationName);
					UserMaster userMaster = userMasterRepository.getOne(note.getCreatedBy().getId());
					dto.setUserName(userMaster.getUsername());
				}
				dtoList.add(dto);

			}

			return dtoList;
		} catch (Exception exception) {
			log.error("ProductDesignTargetService getCreatedByNoteLog Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("ProductDesignTargetService getCreatedByNoteLog method completed");
		return null;
	}

	public BaseDTO getSupplierMasterListByCircleID(Long id) {

		log.info("<-Inside SERVICE-Starts getSupplierMasterListByCircleID-->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<SupplierMaster> supplierMasterList = supplierMasterRepository.getAllSupplierByCircleId(id);
			baseDTO.setResponseContent(supplierMasterList);
			baseDTO.setTotalRecords(supplierMasterList.size());
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("<-SupplierMaster GetAll Data Success-->");
		} catch (Exception exception) {
			log.error("exception Occured SupplierMaster : ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO getAllSupplierType() {
		log.info("<-Inside SERVICE-Starts SupplierMaster-->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<SupplierTypeMaster> intendList = supplierTypeMasterRepository.findAllOrderByCode();
			baseDTO.setResponseContent(intendList);
			baseDTO.setTotalRecords(intendList.size());
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("<-SupplierMaster GetAll Data Success-->");
		} catch (Exception exception) {
			log.error("exception Occured SupplierMaster : ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO getallPurchaseOrderType() {
		log.info("PurchaseOrderTypeService getAll method started ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<PurchaseOrderType> purchaseOrderTypeList = purchaseOrderTypeRepository.findAll();
			baseDTO.setResponseContent(purchaseOrderTypeList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("PurchaseOrderTypeService getAll method completed");
		} catch (RestException exception) {
			baseDTO.setStatusCode(exception.getStatusCode());
			log.error("PurchaseOrderTypeService getAll method RestException ", exception);
		} catch (Exception exception) {
			log.error("Exception occurred in PurchaseOrderTypeService getAll method -:", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO loadAllSocietyAdjustment() {
		log.info("< == Start Of loadAllActiveCircleMasters == >>>");
		BaseDTO baseDTO = new BaseDTO();
		List<SocietyAdjustmentMaster> societyAdjustmentMasterList = null;
		try {
			societyAdjustmentMasterList = societyAdjustmentMasterRepository.findAll();
			if (societyAdjustmentMasterList != null && societyAdjustmentMasterList.size() > 0) {
				log.info("societyAdjustmentMasterList size() " + societyAdjustmentMasterList.size());
				baseDTO.setResponseContent(societyAdjustmentMasterList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				log.info("circleMasterList is empty ");
				baseDTO.setStatusCode(ErrorDescription.ERROR_EMPTY_LIST.getCode());
			}
		} catch (Exception exception) {
			log.error("exception Occured in loadAllCircleMasters", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(baseDTO);
	}

}
