package in.gov.cooptex.finance.advance.service;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;


import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

import in.gov.cooptex.common.repository.BankMasterRepository;
import in.gov.cooptex.common.repository.OrganizationMasterRepository;
import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.enums.VoucherStatus;
import in.gov.cooptex.core.accounts.enums.VoucherTypeDetails;
import in.gov.cooptex.core.accounts.model.BankBranchMaster;
import in.gov.cooptex.core.accounts.model.EntityBankBranch;
import in.gov.cooptex.core.accounts.model.Payment;
import in.gov.cooptex.core.accounts.model.PaymentDetails;
import in.gov.cooptex.core.accounts.model.PurchaseSalesAdvance;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.VoucherDetails;
import in.gov.cooptex.core.accounts.model.VoucherLog;
import in.gov.cooptex.core.accounts.model.VoucherType;
import in.gov.cooptex.core.accounts.repository.BankBranchMasterRepository;
import in.gov.cooptex.core.accounts.repository.EntityBankBranchRepository;
import in.gov.cooptex.core.accounts.repository.PaymentDetailsRepository;
import in.gov.cooptex.core.accounts.repository.PaymentMethodRepository;
import in.gov.cooptex.core.accounts.repository.PaymentRepository;
import in.gov.cooptex.core.accounts.repository.PaymentTypeMasterRepository;
import in.gov.cooptex.core.accounts.repository.VoucherDetailsRepository;
import in.gov.cooptex.core.accounts.repository.VoucherLogRepository;
import in.gov.cooptex.core.accounts.repository.VoucherNoteRepository;
import in.gov.cooptex.core.accounts.repository.VoucherRepository;
import in.gov.cooptex.core.accounts.repository.VoucherTypeRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.PaymentCategory;
import in.gov.cooptex.core.enums.SequenceName;
import in.gov.cooptex.core.finance.service.PaymentFinanceService;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.BankMaster;
import in.gov.cooptex.core.model.CustomerMaster;
import in.gov.cooptex.core.model.CustomerTypeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.PaymentMode;
import in.gov.cooptex.core.model.SequenceConfig;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.CustomerMasterRepository;
import in.gov.cooptex.core.repository.CustomerTypeMasterRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.GovtSchemePlanRepository;
import in.gov.cooptex.core.repository.LoginRepository;
import in.gov.cooptex.core.repository.PaymentModeRepository;
import in.gov.cooptex.core.repository.SalesOrderRepository;
import in.gov.cooptex.core.repository.SequenceConfigRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.finance.advance.repository.PurchaseSalesAdvanceRepository;
import in.gov.cooptex.finance.dto.AdvanceAmountCollectionSpDTO;
import in.gov.cooptex.operation.model.GovtSchemePlan;
import in.gov.cooptex.operation.model.GovtSchemeType;
import in.gov.cooptex.operation.model.SalesOrder;
import in.gov.cooptex.operation.repository.GovtSchemeTypeRepository;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class AdvanceAmountCollectionSpService {
	
	@Autowired
	GovtSchemeTypeRepository govtSchemeTypeRepository;
	
	@Autowired
	ResponseWrapper responseWrapper;
	
	@Autowired
	PurchaseSalesAdvanceRepository purchaseSalesAdvanceRepository;
	
	@Autowired
	VoucherRepository voucherRepository;
	
	@Autowired
	VoucherDetailsRepository voucherDetailsRepository;
	
	@Autowired
	VoucherNoteRepository voucherNoteRepository;
	
	@Autowired
	VoucherLogRepository voucherLogRepository;
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	EntityMasterRepository entityMasterRepository;
	
	@Autowired
	SequenceConfigRepository sequenceConfigRepository;
	
	@Autowired
	PaymentRepository paymentRepository;
	
	@Autowired
	PaymentTypeMasterRepository paymentTypeMasterRepository;
	
	@Autowired	
	PaymentMethodRepository paymentMethodRepository;
	
	@Autowired	
	PaymentDetailsRepository paymentDetailsRepository;
	
	@Autowired
	EntityBankBranchRepository entityBankBranchRepository;
	
	@Autowired
	VoucherTypeRepository voucherTypeRepository;
	
	@Autowired
	SalesOrderRepository salesOrderRepository;
	
	@Autowired
	EntityManager entityManager;
	
	@Autowired
	CustomerMasterRepository customerMasterRepository;
	
	@Autowired
	CustomerTypeMasterRepository customerTypeMasterRepository;
	
	@Autowired
	PaymentModeRepository paymentModeRepository;
	
	@Autowired
	BankBranchMasterRepository bankBranchMasterRepository;
	
	@Autowired
	BankMasterRepository bankMasterRepository;
	
	@Autowired
	GovtSchemePlanRepository govtSchemePlanRepository;
	
	@Autowired
	LoginRepository loginRepository;
	
	@Autowired
	PaymentFinanceService paymentFinanceService;
	
	@Autowired
	UserMasterRepository userMasterRepository;
	
	@Autowired
	OrganizationMasterRepository organizationMasterRepository;
	
	
	@Transactional
	public BaseDTO saveAdvanceAmountCollectionSp(AdvanceAmountCollectionSpDTO dto) {
		log.info("<--AdvanceAmountCollectionSpService() .saveAdvanceAmountCollectionSp() Started-->");
		BaseDTO baseDTO = new BaseDTO();
		Date currentDate=new Date();
		Voucher voucherReturn = new Voucher();
		Payment payment = new Payment();
		try {
			PurchaseSalesAdvance purchaseSalesAdvance = new PurchaseSalesAdvance();
			Voucher voucher = new Voucher();
			VoucherDetails voucherDetails = new VoucherDetails();
			VoucherLog voucherLog = new VoucherLog();
			
			BankBranchMaster bankBranchMaster=new BankBranchMaster();
			PaymentDetails paymentDetails = new PaymentDetails();
			List<EntityBankBranch>  branchList=new ArrayList<>();

			voucher.setPaymentMode(dto.getPaymentMode());
			voucher.setNetAmount(dto.getTotalAdvanceAmount());
			voucher.setFromDate(currentDate);
			voucher.setToDate(currentDate);
//			voucher.setVersion(0l);
			voucher.setName(VoucherTypeDetails.ADVANCE_COLLECTION.toString());
			
//			SimpleDateFormat sdf = new SimpleDateFormat("MMMYYYSS");
			Long sequenceNumber=0L;
			SequenceConfig sequenceConfig = sequenceConfigRepository
					.findBySequenceName(SequenceName.valueOf(SequenceName.ADVANCE_AMOUNT_COLLECTION.name()));
			if (sequenceConfig == null) {
				throw new RestException(ErrorDescription.SEQUENCE_CONFIG_NOT_EXIST);
			}else {
				sequenceNumber = sequenceConfig.getCurrentValue();
			}
			
			sequenceConfig.setCurrentValue(sequenceConfig.getCurrentValue() + 1);
			sequenceConfigRepository.save(sequenceConfig);
			Long entityID = dto.getEntityMasterID();
			VoucherType voucherType=voucherTypeRepository.findByName(VoucherTypeDetails.Payment.toString());
//			Long seqNumber=voucherRepository.findMaxReferenceNumberForVoucher(voucherType.getId());
			String refrenceNumberPrefix = entityID.toString() + sequenceConfig.getSeparator() + sequenceConfig.getPrefix()
			+ AppUtil.getCurrentMonthString()+ AppUtil.getCurrentYearString() ;
			
			voucher.setReferenceNumberPrefix(refrenceNumberPrefix);
			voucher.setReferenceNumber(sequenceNumber);
			voucher.setName(VoucherTypeDetails.ADVANCE_COLLECTION.toString());
			voucher.setVoucherType(voucherType);
			
			voucher.setNarration(dto.getNarration());
			purchaseSalesAdvance.setAdvanceType(dto.getAdvanceCollectionType());
			
			if (dto.getCustomerMaster()!=null && dto.getCustomerMaster().getId()!=null) {
			purchaseSalesAdvance.setCustomerMaster(dto.getCustomerMaster());
			}
			if (dto.getSalesOrder()!=null && dto.getSalesOrder().getId()!=null) {
			purchaseSalesAdvance.setSalesOrder(salesOrderRepository.findOne(dto.getSalesOrder().getId()));
			}
			if (dto.getOrganizationMaster() != null && dto.getOrganizationMaster().getId() != null) {
				purchaseSalesAdvance.setOrganizationMaster(organizationMasterRepository.findOne(dto.getOrganizationMaster().getId()));
			}
			if (dto.getGovtSchemePlan()!=null && dto.getGovtSchemePlan().getId()!=null) {
				purchaseSalesAdvance.setGovtSchemePlan(govtSchemePlanRepository.findOne(dto.getGovtSchemePlan().getId()));
			}
			if (dto.getPaymentMode().getPaymentMode().equals("Cash")) {
				paymentDetails.setBankReferenceNumber("");
				paymentDetails.setDocumentDate(null);
			} else {
				paymentDetails.setBankReferenceNumber(dto.getChequeNumber());
				paymentDetails.setDocumentDate(dto.getChequeDate());
				bankBranchMaster = dto.getBankBranchMaster();
				if(bankBranchMaster != null) {
				branchList= entityBankBranchRepository.getAccountDetails(bankBranchMaster.getId());
				}
			}
			
			
			
//			voucher.setCreatedBy(loginService.getCurrentUser());
			voucher.setCreatedByName(loginService.getCurrentUser().getUsername());
//			voucher.setCreatedDate(currentDate);
			voucherReturn  = voucherRepository.save(voucher);
			
			voucherLog.setStatus(VoucherStatus.FINALAPPROVED);
			voucherLog.setVoucher(voucherReturn);
			voucherLog.setUserMaster(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
			voucherLogRepository.save(voucherLog);
			
			log.info("<--saveAdvanceAmountCollectionSp() Voucher Save Success  -->");
			voucherDetails.setAmount(dto.getTotalAdvanceAmount());
//			voucherDetails.setCreatedBy(loginService.getCurrentUser());
			voucherDetails.setCreatedByName(loginService.getCurrentUser().getUsername());
//			voucherDetails.setCreatedDate(currentDate);
			voucherDetails.setVoucher(voucherReturn);
			voucherDetailsRepository.save(voucherDetails);
			log.info("<--saveAdvanceAmountCollectionSp() VoucherDetails Save Success  -->");
			
//			purchaseSalesAdvance.setCreatedBy(loginService.getCurrentUser());
			purchaseSalesAdvance.setCreatedByName(loginService.getCurrentUser().getUsername());
//			purchaseSalesAdvance.setCreatedDate(currentDate);
			purchaseSalesAdvance.setVoucher(voucherReturn);
//			purchaseSalesAdvance.setVersion(0l);
			purchaseSalesAdvance.setAmount(dto.getTotalAdvanceAmount());
			
			EntityMaster loginUserEntity = entityMasterRepository.findByUserRegion(loginService.getCurrentUser().getId());
			
			if (loginUserEntity == null) {
				log.info("Login User Info Not Available");
				throw new RestException(ErrorDescription.LOGIN_USER_INFO_NOT_AVAILABLE);
			}
			payment.setEntityMaster(loginUserEntity);
			
			payment.setPaymentNumberPrefix(refrenceNumberPrefix);
			payment.setPaymentNumber(sequenceNumber);
			payment.setNarration(dto.getNarration());
//			payment.setCreatedDate(currentDate);
//			payment.setCreatedBy(loginService.getCurrentUser());
			payment.setCreatedByName(loginService.getCurrentUser().getUsername());
			payment = paymentRepository.save(payment);
			log.info("saveAdvanceAmountCollectionSp() Payment save success "+payment.getId());
			paymentDetails.setPayment(payment);
//			paymentDetails.setCreatedDate(currentDate);
//			paymentDetails.setCreatedBy(loginService.getCurrentUser());
			paymentDetails.setCreatedByName(loginService.getCurrentUser().getUsername());
			paymentDetails.setPaymentCategory(PaymentCategory.PAID_IN);
			paymentDetails.setPaymentTypeMaster(paymentTypeMasterRepository.getCustomerPaymentType()); // Customer type set
			paymentDetails.setPaymentMethod(paymentMethodRepository.findByCode(dto.getPaymentMode().getCode()));	
//			paymentDetails.setVersion(0l);
			paymentDetails.setVoucher(voucherReturn);
			paymentDetails.setAmount(dto.getTotalAdvanceAmount());
			if(branchList.size()!= 0) {
				paymentDetails.setEntityBankBranch(branchList.get(0));
			}else {
				log.warn("saveAdvanceAmountCollectionSp - > EntityBankBranch not available for Selected BranchID ::"+bankBranchMaster);
			}
			paymentDetailsRepository.save(paymentDetails);
			log.info("saveAdvanceAmountCollectionSp() paymentDetails save success ");
			/*
			 * Changes Advance Collection Posting
			 */
			if (branchList.size()!=0) {
				log.info(":::::::entityBankBranch.getGlAccount():::::::::::" + branchList.get(0).getGlAccount().getId());
				log.info(":::::::payment:::::::::::" + payment.getId());
				baseDTO=paymentFinanceService.postingBasedOnVoucherName(voucher, branchList.get(0).getGlAccount().getId(),
						payment.getId());
			}
			else {
				log.info("BankPaymentService() :: --> paymentFinanceService()  Called:::>>"+voucher.getName());
				baseDTO=paymentFinanceService.postingBasedOnVoucherName(voucher, null ,payment.getId());
			}
			/*
			End Advance Collection Posting
			*/
			
			if(baseDTO.getStatusCode().equals(ErrorDescription.SUCCESS_RESPONSE.getErrorCode())) {
				log.info("BankPaymentService() :: --> paymentFinanceService()  Processed Completed Successfully");
				purchaseSalesAdvanceRepository.save(purchaseSalesAdvance);
				log.info("<--saveAdvanceAmountCollectionSp() purchaseSalesAdvance  Save Success  -->");
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			}else {
				throw new Exception(baseDTO.getErrorDescription());
			}
		} catch (Exception e) {
			log.error("Exception in saveAdvanceAmountCollectionSp() ",e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			if(voucherReturn!=null && voucherReturn.getId()!=null) {
			voucherLogRepository.deleteVoucherLogByVoucherID(voucherReturn.getId());
			voucherNoteRepository.deleteVoucherNoteByVoucherID(voucherReturn.getId());
			voucherDetailsRepository.deleteVoucherDetailsByVoucherID(voucherReturn.getId());
			voucherRepository.deleteVoucherBYID(voucherReturn.getId());
			}
			if(payment!=null && payment.getId()!=null) {
				paymentRepository.deletePaymentBYID(payment.getId());
				paymentDetailsRepository.deletePaymentDetailBYpaymentID(payment.getId());
			}
		}
		return responseWrapper.send(baseDTO);
	}
	
	public BaseDTO getPurchaseSalesAdvance(Long id) {
		log.info("<--- AdvanceAmountCollectionSpService() :: received sumofPurchaseSalesAdvances --->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			AdvanceAmountCollectionSpDTO dto = new AdvanceAmountCollectionSpDTO();
			Double purchaseSalesAdvance = purchaseSalesAdvanceRepository.getSumBySalesOrderID(id);
			if(purchaseSalesAdvance==null) {
				dto.setSumOfSalesOrderValue(0.0);
			}else {
				dto.setSumOfSalesOrderValue(purchaseSalesAdvance);
			}
			log.info("<--- sumofPurchaseSalesAdvances get Success --->");
			baseDTO.setResponseContent(dto);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			baseDTO.setErrorDescription("Successfully fetched");
		} catch (RestException rex) {
			log.error("RestException at purchaseSalesAdvance() >>>> ", rex);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			baseDTO.setErrorDescription("Exception occurred");
		} catch (Exception ex) {
			log.error("Exception at purchaseSalesAdvance() >>>> ", ex);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			baseDTO.setErrorDescription("Exception occurred");
		}
		log.info("<--- End AdvanceAmountCollectionSpService purchaseSalesAdvance()--->");
		return baseDTO;
	}
	
	// Lazy Load AdvanceCollection
	
	@Autowired
	ApplicationQueryRepository applicationQueryRepository;
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
		public BaseDTO lazySearch(PaginationDTO paginationDTO) {
			log.info(" ::::::::::::::SERVICE lazySearch called:::::::::" + paginationDTO);
			BaseDTO baseDTO = new BaseDTO();
			try {
				Integer total = 0;
				Integer start = paginationDTO.getFirst(),
					pageSize = paginationDTO.getPageSize();
				start = start * pageSize;
				List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
				List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();
				ApplicationQuery applicationQuery = applicationQueryRepository
						.findByQueryName("PURCHASE_SALES_ADVANCE_LAZY_LIST_QUERY");

				String mainQuery = applicationQuery.getQueryContent().trim();

				if (paginationDTO.getFilters() != null) {

					if (paginationDTO.getFilters().get("referenceNumber") != null) {
						mainQuery += " and concat(v.reference_number_prefix,'-',v.reference_number) like '%"
								+ paginationDTO.getFilters().get("referenceNumber") + "%' ";
					}
					if (paginationDTO.getFilters().get("advanceCollectionType") != null) {

						mainQuery += " and upper(psa.advance_type) like upper('%"
								+ paginationDTO.getFilters().get("advanceCollectionType") + "%') ";
					}
					
					if (paginationDTO.getFilters().get("netAmount") != null) {

						mainQuery += " and v.net_amount >="
								+ paginationDTO.getFilters().get("netAmount")+" ";
					}

					if (paginationDTO.getFilters().get("createdDate") != null) {
						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
						Date createdDate = new Date((Long) paginationDTO.getFilters().get("createdDate"));
						mainQuery += " and v.created_date::date='" + format.format(createdDate) + "'";
					}
					if (paginationDTO.getFilters().get("stage") != null) {
						mainQuery += " and vl.status='" + paginationDTO.getFilters().get("stage") + "'";
					}
				}
				String countQuery = mainQuery.replace("SELECT psa.id AS purchaseOrderId, CONCAT( v.reference_number_prefix, '-', v.reference_number ) AS reference_number,psa.advance_type,v.net_amount, vl.status, DATE( v.created_date )",
						"select count(distinct v.id) as count ");
				log.info("count query... " + countQuery);
				countData = jdbcTemplate.queryForList(countQuery);
				for (Map<String, Object> data : countData) {
					if (data.get("count") != null)
						total = Integer.parseInt(data.get("count").toString().replace(",", ""));
				}
				log.info("total query... " + total);
				if (paginationDTO.getSortField() == null) {
					mainQuery += " order by purchaseOrderId desc limit " + pageSize + " offset " + start + ";";
				}
				if (paginationDTO.getSortField() != null && paginationDTO.getSortOrder() != null) {

					log.info("Sort Field:[" + paginationDTO.getSortField() + "] Sort Order:[" + paginationDTO.getSortOrder()
							+ "]");
					if (paginationDTO.getSortField().equals("advanceCollectionType")
							&& paginationDTO.getSortOrder().equals("ASCENDING"))
						mainQuery += " order by psa.advance_type asc ";
					if (paginationDTO.getSortField().equals("advanceCollectionType")
							&& paginationDTO.getSortOrder().equals("DESCENDING"))
						mainQuery += " order by psa.advance_type desc ";

					if (paginationDTO.getSortField().equals("referenceNumber")
							&& paginationDTO.getSortOrder().equals("ASCENDING"))
						mainQuery += " order by reference_number asc  ";
					if (paginationDTO.getSortField().equals("referenceNumber")
							&& paginationDTO.getSortOrder().equals("DESCENDING"))
						mainQuery += " order by reference_number desc  ";
					if (paginationDTO.getSortField().equals("netAmount")
							&& paginationDTO.getSortOrder().equals("ASCENDING"))
						mainQuery += " order by v.net_amount asc ";
					if (paginationDTO.getSortField().equals("netAmount")
							&& paginationDTO.getSortOrder().equals("DESCENDING"))
						mainQuery += " order by v.net_amount desc ";

				
					if (paginationDTO.getSortField().equals("createdDate")
							&& paginationDTO.getSortOrder().equals("ASCENDING"))
						mainQuery += " order by v.created_date asc ";
					if (paginationDTO.getSortField().equals("createdDate")
							&& paginationDTO.getSortOrder().equals("DESCENDING"))
						mainQuery += " order by v.created_date desc ";
				if (paginationDTO.getSortField().equals("stage")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
						mainQuery += " order by vl.status asc ";
				
				if (paginationDTO.getSortField().equals("stage")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
						mainQuery += " order by vl.status desc ";
					mainQuery += " limit " + pageSize + " offset " + start + ";";
				}
				log.info("Main Qury....." + mainQuery);
				List<AdvanceAmountCollectionSpDTO> paymentResponseList = new ArrayList<AdvanceAmountCollectionSpDTO>();
				listofData = jdbcTemplate.queryForList(mainQuery);
				for (Map<String, Object> data : listofData) {
					AdvanceAmountCollectionSpDTO advanceAmountCollectionSpDTOObj = new AdvanceAmountCollectionSpDTO();
					if (data.get("purchaseOrderId") != null) {
						advanceAmountCollectionSpDTOObj.setReferenceNumber(data.get("reference_number").toString());
						advanceAmountCollectionSpDTOObj.setId(Long.parseLong(data.get("purchaseOrderId").toString()));
						advanceAmountCollectionSpDTOObj.setNetAmount(Double.parseDouble(data.get("net_amount").toString()));
						advanceAmountCollectionSpDTOObj.setCreatedDate((Date) data.get("date"));
						
						advanceAmountCollectionSpDTOObj.setAdvanceCollectionType(data.get("advance_type").toString());
					}
					if (data.get("status") != null) {
						advanceAmountCollectionSpDTOObj.setStage(data.get("status").toString());
					}
					paymentResponseList.add(advanceAmountCollectionSpDTOObj);
				}
				log.info("Returning getAllRentCollectionlistlazy list...." + paymentResponseList.size());
				log.info("Total records present in getAllRentCollectionlistlazy..." + total);
				baseDTO.setResponseContent(paymentResponseList);
				baseDTO.setTotalRecords(total);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

			} catch (Exception exp) {
				log.error("Exception Cause  : ", exp);
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			}
			return baseDTO;
		}
	
	
	public Integer getProjectionCount(Criteria criteria) {
		
		ProjectionList projectionList = Projections.projectionList();
		
		projectionList.add(Projections.property("purchaseSalesAdvance.id"));
		projectionList.add(Projections.property("purchaseSalesAdvance.advanceType"));
		projectionList.add(Projections.property("customerTypeMaster.code"));
		projectionList.add(Projections.property("customerTypeMaster.name"));
		projectionList.add(Projections.property("customerMaster.code"));
		projectionList.add(Projections.property("customerMaster.name"));
		projectionList.add(Projections.property("salesOrder.soNumberPrefix"));
		criteria.setProjection(projectionList);
		
		List<?> resultList  = criteria.list();
		
		return resultList.size();
		
	}
	
	public BaseDTO viewAdvanceAmountCollection(Long id) {
		log.info("<--- viewAdvanceAmountCollection Service Long id View Details --->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			AdvanceAmountCollectionSpDTO dto = new AdvanceAmountCollectionSpDTO();
			PurchaseSalesAdvance purchaseSalesAdvance = purchaseSalesAdvanceRepository.findOne(id);
			
			dto.setCreatedBy(purchaseSalesAdvance.getCreatedDate());
			dto.setModifiedBy(purchaseSalesAdvance.getModifiedDate());
			dto.setCreatedByUser(purchaseSalesAdvance.getCreatedBy());
			dto.setModifiedByUser(purchaseSalesAdvance.getModifiedBy());
			if(purchaseSalesAdvance.getSalesOrder()!=null) {
				SalesOrder salesOrder = salesOrderRepository.findOne(purchaseSalesAdvance.getSalesOrder().getId());
				dto.setSalesOrderNumber(salesOrder.getPoRefNumber());
				dto.setTotalAdvanceAmount(salesOrder.getNetValue());
			}
			dto.setCollectionAgainst("Without Sales Order");
			if(purchaseSalesAdvance.getSalesOrder() != null) {
				dto.setCollectionAgainst("Sales Order");
			}
			dto.setAdvanceCollectionType(purchaseSalesAdvance.getAdvanceType());
			if(purchaseSalesAdvance.getCustomerMaster()!=null) {
			CustomerMaster customerMaster = customerMasterRepository.findOne(purchaseSalesAdvance.getCustomerMaster().getId());
			dto.setCustomerCodeName(customerMaster.getCode()+" / "+customerMaster.getName());
			CustomerTypeMaster customerTypeMaster = customerTypeMasterRepository.findOne(customerMaster.getCustomerTypeMaster().getId());
			dto.setCustomerTypeCodeName(customerTypeMaster.getCode()+" / "+customerTypeMaster.getName());
			}
			Voucher voucher =voucherRepository.findOne(purchaseSalesAdvance.getVoucher().getId());
			PaymentMode paymentMode = paymentModeRepository.findOne(voucher.getPaymentMode().getId());
			
			dto.setPaymentMode(paymentMode);
			dto.setChequeAmount(purchaseSalesAdvance.getAmount());
			
			List<PaymentDetails> paymentDetailsList = paymentDetailsRepository.getPaymentListByVoucher(voucher.getId());
			PaymentDetails paymentDetails =  paymentDetailsList.get(0);
			
			if (paymentDetails.getPaymentMethod().getName().equals("Cash")) {
				dto.setChequeNumber("");
				dto.setChequeDate(null);
				dto.setBankBranchMaster(null);
				dto.setBankMaster(null);
			} else {
				dto.setChequeNumber(paymentDetails.getBankReferenceNumber());
				dto.setChequeDate(paymentDetails.getDocumentDate());
				EntityBankBranch entityBankBranch = entityBankBranchRepository.findOne(paymentDetails.getEntityBankBranch().getId());
				BankBranchMaster bankBranchMaster = bankBranchMasterRepository.findOne(entityBankBranch.getBankBranchMaster().getId());
				BankMaster bankMaster = bankMasterRepository.findOne(bankBranchMaster.getBankMaster().getId());
				dto.setBankBranchMaster(bankBranchMaster);
				dto.setBankMaster(bankMaster);
			}
			baseDTO.setResponseContent(dto);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			baseDTO.setErrorDescription("Successfully fetched viewAdvanceAmountCollection");
		} catch (RestException rex) {
			log.error("RestException at viewAdvanceAmountCollection() >>>> ", rex);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			baseDTO.setErrorDescription("Exception occurred viewAdvanceAmountCollection()");
		} catch (Exception ex) {
			log.error("Exception at viewAdvanceAmountCollection() >>>> ", ex);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			baseDTO.setErrorDescription("Exception occurred viewAdvanceAmountCollection()");
		}
		log.info("<--- End Service viewAdvanceAmountCollection()--->");
		return baseDTO;
	}
	
	
	public BaseDTO deleteAdvanceAmountCollection(Long id) {
		log.info("<--- deleteAdvanceAmountCollection Service Long id View Details --->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			AdvanceAmountCollectionSpDTO dto = new AdvanceAmountCollectionSpDTO();
			PurchaseSalesAdvance purchaseSalesAdvance = purchaseSalesAdvanceRepository.findOne(id);
			 
			Voucher voucher =voucherRepository.findOne(purchaseSalesAdvance.getVoucher().getId());
			
			List<PaymentDetails> paymentDetailsList = paymentDetailsRepository.getPaymentListByVoucher(voucher.getId());
			PaymentDetails paymentDetails =  paymentDetailsList.get(0);
			Long voucherID = purchaseSalesAdvance.getVoucher().getId();
			Long paymentDetailsID = paymentDetails.getId();
			Long paymentID =  paymentDetails.getPayment().getId();
			List<VoucherDetails> voucherDetailsList = voucherDetailsRepository.findVoucherDetailsByVoucherId(voucher.getId());
			VoucherDetails voucherDetails = voucherDetailsList.get(0);
			
			paymentDetailsRepository.deletePaymentDetailBYID(paymentDetailsID);
			log.info("AdvanceAmountCollectionSpService() Payment Details Delete Success:");
			
			paymentRepository.deletePaymentBYID(paymentID);
			log.info("AdvanceAmountCollectionSpService() Payment Delete Success:");
			
			voucherDetailsRepository.deleteVoucherDetailsBYID(voucherDetails.getId());
			log.info("AdvanceAmountCollectionSpService() Voucher Details Delete Success:");
			
			purchaseSalesAdvanceRepository.deletePurchaseSalesAdvanceBYID(purchaseSalesAdvance.getId());
			log.info("AdvanceAmountCollectionSpService() purchaseSalesAdvance Delete Success:");
			voucherLogRepository.deleteVoucherLogByVoucherID(voucherID);
			voucherRepository.deleteVoucherBYID(voucherID);
			log.info("AdvanceAmountCollectionSpService() Voucher Delete Success:");
			
			
//			Long voucherDetailsID = 
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			baseDTO.setErrorDescription("Successfully fetched viewAdvanceAmountCollection");
		} catch (RestException rex) {
			log.error("RestException at viewAdvanceAmountCollection() >>>> ", rex);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			baseDTO.setErrorDescription("Exception occurred viewAdvanceAmountCollection()");
		} catch (Exception ex) {
			log.error("Exception at viewAdvanceAmountCollection() >>>> ", ex);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			baseDTO.setErrorDescription("Exception occurred viewAdvanceAmountCollection()");
		}
		log.info("<--- End Service viewAdvanceAmountCollection()--->");
		return baseDTO;
	}
	
	public BaseDTO editAdvanceAmountCollection(Long id) {
		log.info("<--- viewAdvanceAmountCollection Service Long id edit Details --->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			AdvanceAmountCollectionSpDTO dto = new AdvanceAmountCollectionSpDTO();
			PurchaseSalesAdvance purchaseSalesAdvance = purchaseSalesAdvanceRepository.findOne(id);
			
			dto.setCreatedBy(purchaseSalesAdvance.getCreatedDate());
			dto.setModifiedBy(purchaseSalesAdvance.getModifiedDate());
			dto.setCreatedByUser(purchaseSalesAdvance.getCreatedBy());
			dto.setModifiedByUser(purchaseSalesAdvance.getModifiedBy());
			SalesOrder salesOrder = new SalesOrder();
			if(purchaseSalesAdvance.getSalesOrder()!=null) {
				salesOrder = salesOrderRepository.findOne(purchaseSalesAdvance.getSalesOrder().getId());
				dto.setSalesOrderNumber(salesOrder.getPoRefNumber());
				dto.setTotalAdvanceAmount(salesOrder.getNetValue());
			}
			dto.setCollectionAgainst("Without Sales Order");
			if(purchaseSalesAdvance.getSalesOrder() != null) {
				dto.setCollectionAgainst("Sales Order");
			}
			dto.setAdvanceCollectionType(purchaseSalesAdvance.getAdvanceType());
			CustomerMaster customerMaster = new CustomerMaster();
			CustomerTypeMaster customerTypeMaster = new CustomerTypeMaster();
			if(purchaseSalesAdvance.getCustomerMaster()!=null) {
			customerMaster = customerMasterRepository.findOne(purchaseSalesAdvance.getCustomerMaster().getId());
			dto.setCustomerCodeName(customerMaster.getCode()+" / "+customerMaster.getName());
			customerTypeMaster = customerTypeMasterRepository.findOne(customerMaster.getCustomerTypeMaster().getId());
			dto.setCustomerTypeCodeName(customerTypeMaster.getCode()+" / "+customerTypeMaster.getName());
			}
			
			
			
			Voucher voucher =voucherRepository.findOne(purchaseSalesAdvance.getVoucher().getId());
			PaymentMode paymentMode = paymentModeRepository.findOne(voucher.getPaymentMode().getId());
			
			dto.setPaymentMode(paymentMode);
			dto.setChequeAmount(purchaseSalesAdvance.getAmount());
			
			List<PaymentDetails> paymentDetailsList = paymentDetailsRepository.getPaymentListByVoucher(voucher.getId());
			PaymentDetails paymentDetails =  paymentDetailsList.get(0);
			if (paymentDetails.getPaymentMethod().getName().equals("Cash")) {
				dto.setChequeNumber("");
				dto.setChequeDate(null);
				dto.setBankBranchMaster(null);
				dto.setBankMaster(null);
			} else {
				dto.setChequeNumber(paymentDetails.getBankReferenceNumber());
				dto.setChequeDate(paymentDetails.getDocumentDate());
				EntityBankBranch entityBankBranch = entityBankBranchRepository.findOne(paymentDetails.getEntityBankBranch().getId());
				BankBranchMaster bankBranchMaster = bankBranchMasterRepository.findOne(entityBankBranch.getBankBranchMaster().getId());
				BankMaster bankMaster = bankMasterRepository.findOne(bankBranchMaster.getBankMaster().getId());
				dto.setBankBranchMaster(bankBranchMaster);
				dto.setBankMaster(bankMaster);
			}
			if(purchaseSalesAdvance.getAdvanceType().equals("Government Scheme")) {
				GovtSchemePlan govtSchemePlan = govtSchemePlanRepository.findOne(purchaseSalesAdvance.getGovtSchemePlan().getId());
				dto.setGovtSchemePlan(govtSchemePlan);
				GovtSchemeType govtSchemeType = govtSchemeTypeRepository.findByCode(govtSchemePlan.getSchemeType().getCode());
				dto.setGovtSchemeType(govtSchemeType);
				dto.setAdvanceCollectionCategory(true);
			}else {
				dto.setAdvanceCollectionCategory(false);
				dto.setGovtSchemePlan(null);
				dto.setGovtSchemeType(null);
			}
			dto.setCustomerMaster(customerMaster);
			dto.setCustomerTypeMaster(customerTypeMaster);
			dto.setSalesOrder(salesOrder);
			
			Double sumOfPurchaseSalesAdvance = purchaseSalesAdvanceRepository.getSumBySalesOrderID(salesOrder.getId());
			dto.setTotalSalesOrderValue(salesOrder.getNetValue());
			if(sumOfPurchaseSalesAdvance!=null) {
				dto.setTotalAdvancesReceived(sumOfPurchaseSalesAdvance);
				dto.setBalanceSalesOrderValue(salesOrder.getNetValue() - sumOfPurchaseSalesAdvance);
			}else {
				dto.setTotalAdvancesReceived(0.0);
				dto.setBalanceSalesOrderValue(salesOrder.getNetValue());
			}
			dto.setTotalAdvanceAmount(purchaseSalesAdvance.getAmount());
			dto.setId(purchaseSalesAdvance.getId());
			dto.setVoucherID(voucher.getId());
			
			List<VoucherDetails> voucherDetailsList = voucherDetailsRepository.findVoucherDetailsByVoucherId(voucher.getId());
			VoucherDetails voucherDetails = voucherDetailsList.get(0);
			dto.setVoucherDetailsID(voucherDetails.getId());
			dto.setPaymentDetailsID(paymentDetails.getId());
			
			baseDTO.setResponseContent(dto);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			baseDTO.setErrorDescription("Successfully fetched viewAdvanceAmountCollection");
		} catch (RestException rex) {
			log.error("RestException at viewAdvanceAmountCollection() >>>> ", rex);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			baseDTO.setErrorDescription("Exception occurred viewAdvanceAmountCollection()");
		} catch (Exception ex) {
			log.error("Exception at viewAdvanceAmountCollection() >>>> ", ex);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			baseDTO.setErrorDescription("Exception occurred viewAdvanceAmountCollection()");
		}
		log.info("<--- End Service viewAdvanceAmountCollection()--->");
		return baseDTO;
	}
	
	public BaseDTO updateAdvanceAmountCollectionSp(AdvanceAmountCollectionSpDTO dto) {
		log.info("<--AdvanceAmountCollectionSpService() .updateAdvanceAmountCollectionSp() Started-->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			PurchaseSalesAdvance purchaseSalesAdvance = purchaseSalesAdvanceRepository.findOne(dto.getId());
			Voucher voucher = voucherRepository.findOne(dto.getVoucherID());
			VoucherDetails voucherDetails = voucherDetailsRepository.findOne(dto.getVoucherDetailsID());
			PaymentDetails paymentDetails = paymentDetailsRepository.getOne(dto.getPaymentDetailsID());
			
			UserMaster userMaster = loginRepository.findByUsername(loginService.getCurrentUser().getUsername());
			voucher.setPaymentMode(dto.getPaymentMode());
			voucher.setNetAmount(dto.getTotalAdvanceAmount());
			voucher.setId(dto.getVoucherID());
			voucher.setModifiedBy(userMaster);
			voucher.setModifiedDate(new Date());
			Voucher voucherReturn  = voucherRepository.saveAndFlush(voucher);
			
			log.info("Voucher update succcess "+voucherReturn.getId());
			
			purchaseSalesAdvance.setAdvanceType(dto.getAdvanceCollectionType());
			BankBranchMaster bankBranchMaster = dto.getBankBranchMaster();
			List<EntityBankBranch>  branchList= entityBankBranchRepository.getAccountDetails(bankBranchMaster.getId());
 
			voucherDetails.setAmount(dto.getTotalAdvanceAmount());
			voucherDetails.setModifiedBy(userMaster);
			voucherDetails.setModifiedDate(new Date());
			
			voucherDetailsRepository.saveAndFlush(voucherDetails);
			log.info("<-- VoucherDetails Update Success  -->");
			log.info("VoucherDetails update succcess "+voucherDetails.getId());
			
			purchaseSalesAdvance.setModifiedBy(userMaster);
			purchaseSalesAdvance.setModifiedDate(new Date());
			purchaseSalesAdvance.setAmount(dto.getTotalAdvanceAmount());
			
			purchaseSalesAdvanceRepository.saveAndFlush(purchaseSalesAdvance);
			log.info("<-- PurchaseSalesAdvance update Success  -->");
			
			EntityMaster loginUserEntity = entityMasterRepository.findByUserRegion(userMaster.getId());
			
			if (loginUserEntity == null) {
				log.info("Login User Info Not Available");
				throw new RestException(ErrorDescription.LOGIN_USER_INFO_NOT_AVAILABLE);
			}
			 
			paymentDetails.setModifiedDate(new Date());
			paymentDetails.setModifiedBy(userMaster);
			
			if(branchList.size()!= 0) {
				paymentDetails.setEntityBankBranch(branchList.get(0));
			}else {
				log.warn("updateAdvanceAmountCollectionSp - > EntityBankBranch not available for Selected BranchID ::"+bankBranchMaster.getId());
			}
			paymentDetails.setBankReferenceNumber(dto.getChequeNumber());
			paymentDetails.setDocumentDate(dto.getChequeDate());
			
			paymentDetailsRepository.saveAndFlush(paymentDetails);
			log.info("paymentDetails update success "+paymentDetails.getId());
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			
		} catch (Exception e) {
			log.error("Exception in update updateAdvanceAmountCollectionSp() ",e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(baseDTO);
	}
	
	public BaseDTO findAllCustomerType() {
		log.info("findAllCustomerTypeForContractExportPlan");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<CustomerTypeMaster> customerTypeList = salesOrderRepository.findAllCustomerTypeForContractExportPlan();
			baseDTO.setResponseContent(customerTypeList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception exception) {
			log.error("Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(baseDTO);
	}
	
	public BaseDTO loadAllCustomerByCustomerType(Long customerTypeId) {
		log.info("loadAllCustomerByCustomerType [" + customerTypeId + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<CustomerMaster> customerList = salesOrderRepository.loadAllCustomerByCustomerType(customerTypeId);
			baseDTO.setResponseContent(customerList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception exception) {
			log.error("Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(baseDTO);
	}
	
	public BaseDTO salesorderbyCustomerId(Long customerId) {
		log.info("loadAllSalesOrderByCustomer [" + customerId + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<SalesOrder> salesOrderList = salesOrderRepository.loadAllSalesOrderByCustomerID(customerId);
			baseDTO.setResponseContent(salesOrderList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception exception) {
			log.error("Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(baseDTO);
	}
	
	public BaseDTO govtSchemeTypeList() {
		log.info("<--FDSPlanService .govtSchemeTypeList() Started-->");
		BaseDTO baseDTO = new BaseDTO();
		List<GovtSchemeType> govtSchemeTypeList = new ArrayList<>();
		try {
			govtSchemeTypeList = govtSchemeTypeRepository.findAll();
			if (govtSchemeTypeList != null && govtSchemeTypeList.size() > 0) {
				log.info("govtSchemeTypeList size() " + govtSchemeTypeList.size());
				baseDTO.setResponseContent(govtSchemeTypeList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				log.info("govtSchemeTypeList is empty ");
				baseDTO.setStatusCode(ErrorDescription.ERROR_EMPTY_LIST.getCode());
			}

		} catch (Exception e) {
			log.error("Exception in govtSchemeTypeList() ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO getAllGovtSchemePlan(GovtSchemeType govtSchemeType) {
		log.info("GovtSchemePlanService getAllGovtSchemePlan() started");
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("GovtSchemePlanService  GovtSchemeType code" + govtSchemeType.getCode());
			List<GovtSchemePlan> govtSchemePlanList = new ArrayList<GovtSchemePlan>();
			govtSchemePlanList = govtSchemePlanRepository.findByGovtSchemeType(govtSchemeType.getCode());
			log.info("GovtSchemePlanService List{} " + govtSchemePlanList);
			baseDTO.setResponseContent(govtSchemePlanList);
		} catch (RestException re) {
			baseDTO.setStatusCode(re.getErrorCodeDescription().getErrorCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.info("GovtSchemeTypeService getAllGovtSchemeType() end");
		return baseDTO;
	}
}
