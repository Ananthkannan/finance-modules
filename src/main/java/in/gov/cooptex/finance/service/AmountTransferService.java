package in.gov.cooptex.finance.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.repository.CashMovementRepository;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.model.AmountTransfer;
import in.gov.cooptex.core.accounts.model.AmountTransferDetails;
import in.gov.cooptex.core.accounts.model.EntityBankBranch;
import in.gov.cooptex.core.accounts.repository.EntityBankBranchRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.repository.AmountTransferDetailsRepository;
import in.gov.cooptex.core.repository.AmountTransferRepository;
import in.gov.cooptex.core.repository.CacheRepository;
import in.gov.cooptex.core.repository.DistrictMasterRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.finance.BanktoBankAddDTO;
import in.gov.cooptex.finance.BanktoBankAddDetailsDTO;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class AmountTransferService {

	@Autowired
	AmountTransferRepository amountTransferRepository;

	@PersistenceContext
	EntityManager entityManager;

	@Autowired
	CacheRepository cacheRepository;

	@Autowired
	UserMasterRepository userMasterRepository;

	@Autowired
	DistrictMasterRepository districtMasterReository;

	@Autowired
	EntityManager em;

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	EmployeeMasterRepository employeeMasterRepository;

	@Autowired
	EntityBankBranchRepository entityBankBranchRepository;

	@Autowired
	AmountTransferDetailsRepository amountTransferDetailsRepository;

	@Autowired
	CashMovementRepository cashMovementRepository;

	@Autowired
	private ResponseWrapper responseWrapper;

	/**
	 * This method will create one BankBranch Object in DB
	 *
	 * @param
	 * @return the status of the transaction
	 */
//	public BaseDTO addBanktoBankDetails(BanktoBankAddDTO banktoBankAddDTO) {
//		BaseDTO response = new BaseDTO();
//		try {
//			log.info("==>> AmountTransferService Inside addBanktoBankDetails <<== Start ");
//
//			AmountTransfer amountTransfer = new AmountTransfer();
//
//			amountTransfer.setEntityMaster(entityMasterRepository.findById(banktoBankAddDTO.getEntityId()));
//			amountTransfer.setMovementType(banktoBankAddDTO.getMovementType());
//			amountTransfer.setTotalAmountTransfered(banktoBankAddDTO.getTotalAmountTransfered());
//			amountTransfer.setTransferedBy(employeeMasterRepository.findById(banktoBankAddDTO.getTransferredBy()));
//			amountTransfer.setRemarks(banktoBankAddDTO.getRemarks());
//
//			amountTransferRepository.save(amountTransfer);
//
//			EntityBankBranch fromEntityBankBranch = entityBankBranchRepository
//					.findOne(banktoBankAddDTO.getFromBranchId());
//			EntityBankBranch toEntityBankBranch = entityBankBranchRepository.findOne(banktoBankAddDTO.getToBranchId());
//
//			for (BanktoBankAddDetailsDTO banktoBankAddDetailsDTO : banktoBankAddDTO.getBanktoBankAddDetailsDTOS()) {
//
//				AmountTransferDetails amountTransferDetails = new AmountTransferDetails();
//				amountTransferDetails.setAmountTransfer(amountTransfer);
//				amountTransferDetails.setChallanAmount(banktoBankAddDetailsDTO.getChallanAmount());
//				amountTransferDetails.setChallanDate(banktoBankAddDetailsDTO.getChallanDate());
//				amountTransferDetails.setChallanNumber(banktoBankAddDetailsDTO.getChallanNumber());
//				amountTransferDetails.setFromBranch(fromEntityBankBranch);
//				amountTransferDetails.setToBranch(toEntityBankBranch);
//				amountTransferDetailsRepository.save(amountTransferDetails);
//			}
//
//			// } catch (DataIntegrityViolationException divEX) {
//			// log.warn("<<=== Error While Adding BankBranch ===>>", divEX);
//			// String exceptionCause = divEX.getCause().getCause().getMessage();
//			// if (exceptionCause.contains(bankBranchMaster.getBranchCode())) {
//			// response.setStatusCode(ErrorDescription.BANK_BRANCH_MASTER_BRANCH_CODE_ALREADY_EXISTS.getErrorCode());
//			// }
//			// } catch (RestException re) {
//			// log.warn("<<=== Error While Adding BankBranch ===>>", re);
//			// response.setStatusCode(re.getStatusCode());
//			// }
//		} catch (Exception e) {
//			log.error("Exception While Adding BankBranch ===>>", e);
//		}
//		log.info("==>> BankBranchMasterService Inside addBankBranch <<== End ");
//		return response;
//	}

	// public BaseDTO searchForCashtoBankAmount() {
	// BaseDTO response = new BaseDTO();
	// CashMovement cashMovement;
	// List<BankBranchMaster> bankBranchMasterList = null;
	// try {
	// log.info("==>> AmountTransferService Inside searchForCashtoBankAmount<<==
	// Start");
	// cashMovement = cashMovementRepository.findOneByCreatedDate();
	//// Validate.objectNotNull(bankBranchMasterList,
	// ErrorDescription.BANK_BRANCH_MASTER_NO_RECORDS_FOUND);
	//// response.setStatusCode(ErrorDescription.BANK_BRANCH_MASTER_FETCHED_SUCCESSFULLY.getErrorCode());
	// response.setResponseContents((List<?>) cashMovement);
	//
	// } catch (RestException re) {
	// log.warn("<<=== Error While search CashtoAmount Transfer ===>>", re);
	// response.setStatusCode(re.getStatusCode());
	// } catch (Exception e) {
	// log.error("Exception While searchForCashtoBankAmount ===>>" + e);
	// }
	// log.info("==>> AmountTransferService Inside searchForCashtoBankAmount<<==
	// End");
	// return response;
	// }

	//
	// /**
	// * This method for update the existing BankBranch in DB
	// * BANK_BRANCH_MASTER_BRANCH_LNAME_REQUIRED
	// * @param BankBranch
	// * @return status of the transaction
	// */
	// public BaseDTO updateBankBranch(BankBranchMaster bankBranchMaster) {
	// log.info("============== BankBranchMasterService Inside updateBankBranch()
	// ==============> Start"+bankBranchMaster);
	// log.info("============== BankBranchMasterService Inside updateBankBranch()
	// ==============>
	// bankBranchMaster.getBankBranchAddress"+bankBranchMaster.getBankBranchAddress());
	// log.info("============== BankBranchMasterService Inside updateBankBranch()
	// ==============>
	// bankBranchMaster.getBankBranchAddress().modifiedBy"+bankBranchMaster.getBankBranchAddress().modifiedBy);
	// BaseDTO response = new BaseDTO();
	// try {
	// Validate.objectNotNull(bankBranchMaster.getBranchCode(),
	// ErrorDescription.BANK_BRANCH_MASTER_BRANCH_CODE_REQUIRED);
	// Validate.objectNotNull(bankBranchMaster.getBranchName(),
	// ErrorDescription.BANK_BRANCH_MASTER_BRANCH_NAME_REQUIRED);
	// Validate.objectNotNull(bankBranchMaster.getLocalBranchName(),
	// ErrorDescription.BANK_BRANCH_MASTER_BRANCH_LNAME_REQUIRED);
	// Validate.objectNotNull(bankBranchMaster.getIfscCode(),
	// ErrorDescription.BANK_BRANCH_MASTER_IFSC_CODE_REQUIRED);
	// Validate.objectNotNull(bankBranchMaster.getActiveStatus(),
	// ErrorDescription.BANK_BRANCH_MASTER_ACTIVE_STATUS_REQUIRED);
	// //Validate.objectNotNull(bankBranchMaster.getCreatedBy(),
	// ErrorDescription.TALUK_CREATEDBY_REQUIRED);
	// //Validate.objectNotNull(bankBranchMaster.getCreatedBy(),
	// ErrorDescription.BANK_BRANCH_MASTER_CREATED_BY_REQUIRED);
	// //Validate.objectNotNull(bankBranchMaster.getModifiedBy(),
	// ErrorDescription.BANK_BRANCH_MASTER_MODIFIED_BY_REQUIRED);
	// Validate.objectNotNull(bankBranchMaster.getBankMaster(),
	// ErrorDescription.BANK_BRANCH_MASTER_BANK_MASTER_REQUIRED);
	// Validate.objectNotNull(bankBranchMaster.getBankBranchAddress(),
	// ErrorDescription.BANK_BRANCH_MASTER_ADDRESS_REQUIRED);
	//
	// BankBranchMaster existingStateMaster =
	// bankBranchMasterRepository.findOne(bankBranchMaster.getId());
	// Validate.objectNotNull(existingStateMaster,
	// ErrorDescription.BANK_BRANCH_MASTER_DOES_NOT_EXIST);
	//// bankBranchMaster.setDistrictMaster(districtMasterReository.findOne(bankBranchMaster.getDistrictMaster().getId()));
	// bankBranchMaster.setCreatedBy(existingStateMaster.getCreatedBy());
	// bankBranchMaster.setModifiedBy(userMasterRepository.findOne(bankBranchMaster.getModifiedBy().getId()));
	// bankBranchMaster.getBankBranchAddress().setCreatedBy(existingStateMaster.getBankBranchAddress().getCreatedBy());
	//
	// bankBranchMaster.getBankBranchAddress().setModifiedBy(userMasterRepository.findOne(bankBranchMaster.getBankBranchAddress().getModifiedBy().getId()));
	// if (existingStateMaster.getId().equals(bankBranchMaster.getId())) {
	// cacheRepository.remove(BankBranchMaster.class.getName(),
	// bankBranchMaster.getId());
	// cacheRepository.save(BankBranchMaster.class.getName(), bankBranchMaster,
	// bankBranchMasterRepository);
	// }
	// response.setStatusCode(ErrorDescription.BANK_BRANCH_MASTER_UPDATED_SUCCESSFULLY.getErrorCode());
	// } catch (DataIntegrityViolationException divEX) {
	// log.warn("<<=== Error While updating bankBranchMaster ===>>", divEX);
	// String exceptionCause = divEX.getCause().getCause().getMessage();
	// if (exceptionCause.contains(bankBranchMaster.getBranchCode())) {
	// response.setStatusCode(ErrorDescription.BANK_BRANCH_MASTER_BRANCH_CODE_ALREADY_EXISTS.getErrorCode());
	// }
	// } catch (ObjectOptimisticLockingFailureException lockEx) {
	// log.warn("====>> Error while updating bankBranchMaster <<====", lockEx);
	// response.setStatusCode(ErrorDescription.CANNOT_UPDATE_LOCKED_RECORD.getErrorCode());
	// } catch (JpaObjectRetrievalFailureException ObjFailEx) {
	// log.warn("====>> Error while updating bankBranchMaster <<====", ObjFailEx);
	// response.setStatusCode(ErrorDescription.CANNOT_UPDATE_DELETED_RECORD.getErrorCode());
	// } catch (RestException re) {
	// log.warn("====>> Error while updating bankBranchMaster <<====", re);
	// response.setStatusCode(re.getStatusCode());
	// } catch (Exception e) {
	// log.error("====>> Exception occurred <<====", e);
	// }
	// log.info("==>> BankBranchMasterService Inside updating bankBranchMaster()<<==
	// End");
	// return response;
	// }
	//
	// public BaseDTO deleteBankBranch(BankBranchMaster bankBranchMaster) {
	// log.info("============== BankBranchMasterService Inside deleteBankBranch()
	// ==============> Start");
	// BaseDTO baseDto = new BaseDTO();
	// try {
	// bankBranchMasterRepository.delete(bankBranchMaster.getId());
	//
	// baseDto.setStatusCode(ErrorDescription.BANK_BRANCH_MASTER_DELETED_SUCCESSFULLY.getErrorCode());
	// } catch (DataIntegrityViolationException diExp) {
	// log.error("Data Integrity Violation Exception while Deleting BankBranch " +
	// diExp);
	//
	// String exceptionCause = ExceptionUtils.getRootCauseMessage(diExp);
	// log.error("Exception Cause 1 : " + exceptionCause);
	//
	// baseDto.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
	// }
	// log.info("============== BankBranchMasterService Inside deleteBankBranch()
	// ==============> end");
	// return responseWrapper.send(baseDto);
	// }
	//
	// /**
	// * This method for load BankBranch based on id
	// *
	// * @param id
	// * @return
	// */
	// public BaseDTO getById(Long id) {
	// log.info("===== BankBranchMasterService Inside getById() =====> Start " +
	// id);
	// BaseDTO response = new BaseDTO();
	// try {
	// BankBranchMaster bankBranchMaster =
	// cacheRepository.get(BankBranchMaster.class.getName(), id,
	// bankBranchMasterRepository);
	// Validate.objectNotNull(bankBranchMaster,
	// ErrorDescription.BANK_BRANCH_MASTER_DOES_NOT_EXIST);
	// response.setStatusCode(ErrorDescription.BANK_BRANCH_MASTER_FETCHED_SUCCESSFULLY.getErrorCode());
	// response.setResponseContent(bankBranchMaster);
	// } catch (RestException re) {
	// log.warn("====>> Error while getById <<====", re);
	// response.setStatusCode(re.getStatusCode());
	// } catch (Exception e) {
	// log.error("====>> Exception occurred <<====", e);
	// }
	// log.info("===== BankBranchMasterService Inside getById() =====> End ");
	// return response;
	// }
	//
	// /**
	// * This method is to loadall records from the DB
	// *
	// * @return return list of all BankBranchs
	// */
	// public BaseDTO getAllBankBranchs() {
	// BaseDTO response = new BaseDTO();
	// List<BankBranchMaster> bankBranchMasterList = null;
	// try {
	// log.info("==>> BankBranchMasterService Inside getAllBankBranchs<<== Start");
	// bankBranchMasterList = bankBranchMasterRepository.findAll();
	// Validate.objectNotNull(bankBranchMasterList,
	// ErrorDescription.BANK_BRANCH_MASTER_NO_RECORDS_FOUND);
	// response.setStatusCode(ErrorDescription.BANK_BRANCH_MASTER_FETCHED_SUCCESSFULLY.getErrorCode());
	// response.setResponseContents(bankBranchMasterList);
	//
	// } catch (RestException re) {
	// log.warn("<<=== Error While getAllBankBranchs ===>>", re);
	// response.setStatusCode(re.getStatusCode());
	// } catch (Exception e) {
	// log.error("Exception While getAllBankBranchs ===>>" + e);
	// }
	// log.info("==>> BankBranchMasterService Inside getAllBankBranchs<<== End");
	// return response;
	// }
	//
	// /**
	// * This method is to loadall records from the DB
	// *
	// * @return return list of all BankBranchs
	// */
	// public BaseDTO getAllBankBranchsByBank(Long bankId) {
	// BaseDTO response = new BaseDTO();
	// List<BankBranchMaster> bankBranchMasterList = null;
	// try {
	// log.info("==>> BankBranchMasterService Inside getAllBankBranchs<<== Start");
	// bankBranchMasterList = bankBranchMasterRepository.getBranchList(bankId);
	// Validate.objectNotNull(bankBranchMasterList,
	// ErrorDescription.BANK_BRANCH_MASTER_NO_RECORDS_FOUND);
	// response.setStatusCode(ErrorDescription.BANK_BRANCH_MASTER_FETCHED_SUCCESSFULLY.getErrorCode());
	// response.setResponseContents(bankBranchMasterList);
	//
	// } catch (RestException re) {
	// log.warn("<<=== Error While getAllBankBranchs ===>>", re);
	// response.setStatusCode(re.getStatusCode());
	// } catch (Exception e) {
	// log.error("Exception While getAllBankBranchs ===>>" + e);
	// }
	// log.info("==>> BankBranchMasterService Inside getAllBankBranchs<<== End");
	// return response;
	// }
	//
	// /**
	// * This method is to search any BankBranch record in the DB
	// *
	// * @param BankBranch
	// * @return list of BankBranch
	// */
	/// * public BaseDTO searchBankBranch(BankBranchMaster bankBranchMaster) {
	// log.info("==>> BankBranchMasterService Inside searchBankBranch<<== Start");
	// BaseDTO response = new BaseDTO();
	// try {
	// String name = null;
	// String code = null;
	// String lbankBranchName = null;
	// Boolean status = true;
	// if (bankBranchMaster != null) {
	// if (!StringUtils.isEmpty(bankBranchMaster.getBranchName()) &&
	// bankBranchMaster.getBranchName().trim().length() > 0) {
	// name = bankBranchMaster.getBranchName();
	// }
	// if (!StringUtils.isEmpty(bankBranchMaster.getLBranchName())
	// && bankBranchMaster.getLBranchName().trim().length() > 0) {
	// lbankBranchName = bankBranchMaster.getLBranchName();
	// }
	// if (!StringUtils.isEmpty(bankBranchMaster.getBranchCode()) &&
	// bankBranchMaster.getBranchCode().trim().length() > 0) {
	// code = bankBranchMaster.getBranchCode();
	// }
	// status = bankBranchMaster.getActiveStatus();
	//
	// List<BankBranchMaster> bankBranchMasterList =
	// bankBranchMasterRepository.searchByParameters(name, lbankBranchName,
	// code, status);
	//
	// response.setStatusCode(ErrorDescription.STATE_RETRIEVED_SUCCESSFULLY.getErrorCode());
	// // response.setResponseContents(bankBranchMasterList);
	// }
	// } catch (RestException re) {
	// log.warn("<<=== Error While searchBankBranch ===>>", re);
	// response.setStatusCode(re.getStatusCode());
	// } catch (Exception e) {
	// log.error("====>> Exception occurred <<====", e);
	// }
	// log.info("==>> StateMasterService Inside searchState()<<== End");
	// return response;
	// }*/
	//
	//
	//
	// /**
	// * This method for load States on demand
	// *
	// * @param paginationRequest
	// * @return list of lazyloaded BankBranchs
	// */
	// public BaseDTO getAllLazyLoadBankBranchs(PaginationRequestData
	// paginationRequest) {
	// log.info("==>> BankBranchMasterService Inside getAllLazyLoadBankBranchs()<<==
	// Start");
	// BaseDTO response = new BaseDTO();
	// Validate.notNull(paginationRequest, ErrorCodeDescription.INVALID_DATA);
	// Pageable pageable = new PageRequest(paginationRequest.getPageNo(),
	// paginationRequest.getPaginationSize());
	// List<BankBranchMaster> bankBranchLazyList = new
	// ArrayList<BankBranchMaster>();
	// Page<BankBranchMaster> pageBankBranchList = null;
	// PaginationResponseData paginationResponseData = new PaginationResponseData();
	// try {
	// pageBankBranchList = bankBranchMasterRepository.findAll(pageable);
	// //Validate.objectNotNull(pageBankBranchList,
	// ErrorDescription.BANK_BRANCH_MASTER_NO_RECORDS_FOUND);
	// response.setStatusCode(ErrorDescription.BANK_BRANCH_MASTER_FETCHED_SUCCESSFULLY.getErrorCode());
	// if (pageBankBranchList != null) {
	// paginationResponseData.setTotalElements(pageBankBranchList.getTotalElements());
	// paginationResponseData.setNumberOfElements(pageBankBranchList.getNumberOfElements());
	// paginationResponseData.setTotalPages(pageBankBranchList.getTotalPages());
	// for (BankBranchMaster bankBranchMaster : pageBankBranchList) {
	// bankBranchLazyList.add(bankBranchMaster);
	// }
	// paginationResponseData.setContents(bankBranchLazyList);
	// } else {
	// paginationResponseData.setTotalElements(null);
	// paginationResponseData.setNumberOfElements(0);
	// paginationResponseData.setTotalPages(0);
	// }
	// response.setResponseContent(paginationResponseData);
	// } catch (RestException re) {
	// log.warn("====>> Error while getAllLazyLoadBankBranchs <<====", re);
	// response.setStatusCode(re.getStatusCode());
	// } catch (Exception e) {
	// log.error("====>> Exception occurred <<====", e);
	// }
	// log.info("===== BankBranchMasterService Inside getAllLazyLoadBankBranchs()
	// =====> End");
	// return response;
	// }
	//
	//
	// public BaseDTO searchForBankBranch(BankBranchMasterDTO request) {
	// log.info("BankBranchMasterService searchForBankBranch method started ");
	//
	// BaseDTO baseDTO = new BaseDTO();
	//
	// try {
	// Integer totalRecords = searchForBankBranchMasterCount(request);
	// List<BankBranchMasterDTO> bankBranchMasterResponseList =
	// searchForsearchForBankBranchMaster(request);
	// if (bankBranchMasterResponseList.isEmpty()) {
	// log.info("bankBranchMasterResponseList is empty");
	// throw new
	// RestException(ErrorDescription.BANK_BRANCH_MASTER_NO_RECORDS_FOUND);
	// }
	// baseDTO.setTotalRecords(totalRecords);
	// baseDTO.setResponseContent(bankBranchMasterResponseList);
	// baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
	// log.info("BankBranchMasterService search method completed");
	// } catch (RestException exception) {
	// baseDTO.setStatusCode(exception.getStatusCode());
	// log.error("BankBranchMasterService searchForBankBranch method RestException
	// ", exception);
	// } catch (Exception e) {
	// log.error("Exception occured ", e);
	// baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
	// }
	// return responseWrapper.send(baseDTO);
	// }
	//
	//
	// public Integer searchForBankBranchMasterCount(BankBranchMasterDTO request) {
	// Session session = em.unwrap(Session.class);
	// Criteria criteria = session.createCriteria(BankBranchMaster.class, "branch");
	// criteria.setProjection(Projections.rowCount());
	// return ((Long) criteria.uniqueResult()).intValue();
	// }
	//
	// public List<BankBranchMasterDTO>
	// searchForsearchForBankBranchMaster(BankBranchMasterDTO request) {
	// log.info("BankBranchMasterService searchForsearchForBankBranchMaster method
	// started");
	// log.info("created Date"+request.getCreatedDate());
	// List<BankBranchMasterDTO> bankBranchMasterResponseList = new ArrayList<>();
	// Session session = em.unwrap(Session.class);
	// Criteria criteria = session.createCriteria(BankBranchMaster.class, "branch");
	// criteria.createAlias("branch.bankMaster", "bank");
	// log.info("Criteria search started...");
	// if (StringUtils.isNotEmpty(request.getBankName())) {
	// log.info("BankBranchMaster BankName added to criteria :" +
	// request.getBankName());
	// // criteria.add(
	// // Restrictions.like("bank.bankName", "%" + request.getBankName() +
	// '%').ignoreCase());
	// criteria.add(Restrictions.or(
	// Restrictions.like("bank.bankName",
	// "%" + request.getBankName() + '%').ignoreCase(),
	// Restrictions
	// .like("bank.bankCode",
	// "%" + request.getBankName() + '%')
	// .ignoreCase()));
	// }
	// if (StringUtils.isNotEmpty(request.getBranchName())) {
	// log.info("BankBranchMaster BranchName added to criteria:" +
	// request.getBranchName());
	// // criteria.add(
	// // Restrictions.like("branch.branchName", "%" + request.getBranchName() +
	// '%').ignoreCase());
	// criteria.add(Restrictions.or(
	// Restrictions.like("branch.branchName",
	// "%" + request.getBranchName() + '%').ignoreCase(),
	// Restrictions
	// .like("branch.branchCode",
	// "%" + request.getBranchName() + '%')
	// .ignoreCase()));
	// }
	// if (StringUtils.isNotEmpty(request.getIfscCode())) {
	// log.info("BankBranchMaster fscCode added to criteria:" +
	// request.getIfscCode());
	// criteria.add(Restrictions.like("branch.ifscCode", "%" + request.getIfscCode()
	// + '%').ignoreCase());
	// }
	// if (StringUtils.isNotEmpty(request.getContactName())) {
	// log.info("BankBranchMaster ContactName added to criteria:" +
	// request.getContactName());
	// criteria.add(Restrictions.like("branch.primaryContactName", "%" +
	// request.getContactName() + '%').ignoreCase());
	// }
	// if (StringUtils.isNotEmpty(request.getContactNumber())) {
	// log.info("ProductCategory ContactNumber added to criteria:" +
	// request.getContactNumber());
	// criteria.add(Restrictions.like("branch.primaryContactNumber", "%" +
	// request.getContactNumber() + '%').ignoreCase());
	// }
	// if (request.getCreatedDate() != null) {
	// log.info("BankBranchMaster created date added to criteria:" + new
	// Date(request.getCreatedDate().getTime()));
	// java.sql.Date startDateTime = new java.sql.Date(
	// request.getCreatedDate().getTime());
	// java.sql.Date endDateTime = new java.sql.Date(
	// AppUtil.getEndDateTime(request.getCreatedDate()).getTime());
	//
	// criteria.add(Restrictions.between("branch.createdDate", startDateTime,
	// endDateTime));
	// }
	// if (request.getActiveStatus() != null) {
	// criteria.add(Restrictions.eq("branch.activeStatus",
	// request.getActiveStatus()));
	// }
	// if (request.getId() != null) {
	// log.info("BankBranchMaster Id :" + request.getId());
	// criteria.add(Restrictions.eq("branch.id", request.getId()));
	// }
	// ProjectionList projectionList = Projections.projectionList();
	// projectionList.add(Projections.property("branch.id"));
	// projectionList.add(Projections.property("bank.bankCode"));
	// projectionList.add(Projections.property("bank.bankName"));
	// projectionList.add(Projections.property("branch.branchCode"));
	// projectionList.add(Projections.property("branch.branchName"));
	// projectionList.add(Projections.property("branch.ifscCode"));
	// projectionList.add(Projections.property("branch.primaryContactName"));
	// projectionList.add(Projections.property("branch.primaryContactNumber"));
	// projectionList.add(Projections.property("branch.createdDate"));
	// projectionList.add(Projections.property("branch.activeStatus"));
	// criteria.setProjection(projectionList);
	// if (request.getPaginationDTO() != null) {
	// Integer pageNo = request.getPaginationDTO().getPageNo();
	// Integer pageSize = request.getPaginationDTO().getPageSize();
	// if (pageNo != null && pageSize != null) {
	// criteria.setFirstResult(pageNo * pageSize);
	// criteria.setMaxResults(pageSize);
	// log.info("PageNo : [" + pageNo + "] pageSize[" + pageSize + "]");
	// }
	// String sortField = request.getPaginationDTO().getSortField();
	// String sortOrder = request.getPaginationDTO().getSortOrder();
	// if (sortField != null && sortOrder != null) {
	// log.info("sortField : [" + sortField + "] sortOrder[" + sortOrder + "]");
	// if (sortField.equals("bankName")) {
	// sortField = "bank.bankName";
	// } else if (sortField.equals("branchName")) {
	// sortField = "branch.branchName";
	// } else if (sortField.equals("ifscCode")) {
	// sortField = "branch.ifscCode";
	// } else if (sortField.equals("contactName")) {
	// sortField = "branch.primaryContactName";
	// } else if (sortField.equals("contactNumber")) {
	// sortField = "branch.primaryContactNumber";
	// } else if (sortField.equals("createdDate")) {
	// sortField = "branch.createdDate";
	// } else if (sortField.equals("activeStatus")) {
	// sortField = "branch.activeStatus";
	//
	// }
	// if (sortOrder.equals("DESCENDING")) {
	// criteria.addOrder(Order.desc(sortField));
	// } else {
	// criteria.addOrder(Order.asc(sortField));
	// }
	// }
	// }
	// List<?> resultList = criteria.list();
	// if (resultList == null || resultList.isEmpty()) {
	// log.info("bankBranchMasterResponseList is null or empty ");
	// throw new
	// RestException(ErrorDescription.BANK_BRANCH_MASTER_NO_RECORDS_FOUND);
	// }
	// log.info("criteria list executed and the list size is : " +
	// resultList.size());
	// Iterator<?> it = resultList.iterator();
	// while (it.hasNext()) {
	// Object ob[] = (Object[]) it.next();
	// log.info("BankBranchMaster Id: >>>> " + ob[0]);
	// log.info("BankBranchMaster bank Code: >>>> " + ob[1]);
	// log.info("BankBranchMaster bank name: >>>> " + ob[2]);
	// log.info("BankBranchMaster branch code: >>>" + ob[3]);
	// log.info("BankBranchMaster branch name >>>> " + ob[4]);
	// log.info("BankBranchMaster ifsc code >>>> " + ob[5]);
	// log.info("BankBranchMaster contact name >>>> " + ob[6]);
	// log.info("BankBranchMaster contact no >>>> " + ob[7]);
	// log.info("BankBranchMaster created date >>>> " + ob[8]);
	// log.info("BankBranchMaster status >>>> " + ob[9]);
	//
	// BankBranchMasterDTO responseList = new BankBranchMasterDTO((Long) ob[0],
	// (String) ob[1],(String) ob[2],(String) ob[3],(String) ob[4],(String)
	// ob[5],(String) ob[6],(String)ob[7],(Date) ob[8],null,(Boolean) ob[9]);
	// bankBranchMasterResponseList.add(responseList);
	// }
	// return bankBranchMasterResponseList;
	// }

}
