/**
 * 
 */
package in.gov.cooptex.finance.service;

import java.awt.Checkbox;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.persistence.EntityManager;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.dto.VoucherResponseDTO;
import in.gov.cooptex.core.accounts.enums.VoucherStatus;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.VoucherDetails;
import in.gov.cooptex.core.accounts.model.VoucherNote;
import in.gov.cooptex.core.accounts.model.VoucherType;
import in.gov.cooptex.core.accounts.repository.VoucherDetailsRepository;
import in.gov.cooptex.core.accounts.repository.VoucherNoteRepository;
import in.gov.cooptex.core.accounts.repository.VoucherRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.PaymentMode;
import in.gov.cooptex.core.repository.AppQueryRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import lombok.extern.log4j.Log4j2;

/**
 * @author user
 *
 */
@Log4j2
@Service
public class VoucherListService {

	@Autowired
	EntityManager entityManager;

	@Autowired
	ResponseWrapper responseWrapper;
	
	@Autowired
	EmployeeMasterRepository employeeMasterRepository;
	@Autowired
	VoucherDetailsRepository voucherDetailsRepository;
	
	@Autowired
	VoucherRepository voucherRepository;
	
	@Autowired
	EntityMasterRepository entityMasterRepository;
	@Autowired
	VoucherNoteRepository voucherNoteRepository;
	
	@Autowired
	AppQueryRepository appQueryRepository;
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	LoginService loginService;

	public BaseDTO fetchAllVoucherListLazy(PaginationDTO paginationDto) {
		BaseDTO response = new BaseDTO();
		String mainQuery = null;
		String countQuery =  null;
		List<Voucher> voucherList = null;
		List<Map<String, Object>> listofData = null;
		List<Map<String, Object>> countData = null;
		Integer totalRecords = 0;
		ApplicationQuery applicationQuery = null;
		try {
			voucherList = new ArrayList<Voucher>();
			listofData = new ArrayList<Map<String, Object>>();
			countData = new ArrayList<Map<String, Object>>();
			
			Integer start = paginationDto.getFirst(), pageSize = paginationDto.getPageSize();
			start = start * pageSize;
			
			applicationQuery = appQueryRepository.findByQuery("VOUCHER_LAZY_LIST");
			if(applicationQuery == null) {
				log.info("----VOUCHER_LAZY_LIST query not found------");
				return null;
			}
			
			mainQuery = applicationQuery.getQueryContent();
			log.info("db query----------" + mainQuery);
			
			EntityMaster entityMaster = entityMasterRepository.findByUserRegion(loginService.getCurrentUser().getId());
			if(entityMaster != null) {
				mainQuery = mainQuery.replaceAll(":entityId", entityMaster.getId().toString());
			}
			
			if (paginationDto.getFilters() != null) {
				if (paginationDto.getFilters().get("entityMaster.name") != null) {
					mainQuery +=" and upper(em.name) like upper('%"+paginationDto.getFilters().get("entityMaster.name")+"%') ";
				}
				if (paginationDto.getFilters().get("name") != null) {
					mainQuery += " and v.name='" + paginationDto.getFilters().get("name") + "'";
				}
				if (paginationDto.getFilters().get("referenceNumberPrefix") != null) {
					mainQuery += " and v.reference_number_prefix='" + paginationDto.getFilters().get("referenceNumberPrefix") + "'";
				}
				if (paginationDto.getFilters().get("status") != null) {
					mainQuery += " and vl.status='" + paginationDto.getFilters().get("status") + "'";
				}
				if (paginationDto.getFilters().get("voucherType.name") != null) {
					mainQuery += " and vt.name='" + paginationDto.getFilters().get("voucherType.name") + "'";
				}
				if (paginationDto.getFilters().get("netAmount") != null) {
					mainQuery += " and v.net_amount='" + paginationDto.getFilters().get("netAmount") + "'";
				}
				if (paginationDto.getFilters().get("createdDate") != null) {
					Date createdDate=new Date((Long)paginationDto.getFilters().get("createdDate"));
					mainQuery += " and date(v.created_date)=date('"+createdDate+"')";
				}
				if (paginationDto.getFilters().get("paymentMode.paymentMode") != null) {
					mainQuery += " and pm.payment_mode='" + paginationDto.getFilters().get("paymentMode.paymentMode") + "'";
				}
			}
			
		 	applicationQuery = appQueryRepository.findByQuery("VOUCHER_LAZY_LIST_COUNT");
		 	if(applicationQuery == null) {
				log.info("----VOUCHER_LAZY_LIST_COUNT query not found------");
				return null;
			}
		 	
			countQuery = applicationQuery.getQueryContent();
			log.info("count query... " + countQuery);
			if(entityMaster != null) {
				countQuery = countQuery.replaceAll(":entityId", entityMaster.getId().toString());
			}
			countData = jdbcTemplate.queryForList(countQuery);

			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					totalRecords = Integer.parseInt(data.get("count").toString().replace(",", ""));
			}
			
			if (paginationDto.getSortField() == null) {
				mainQuery += " order by v.id desc limit " + pageSize + " offset " + start + ";";
			}
			
			if(paginationDto.getSortField()!=null && paginationDto.getSortOrder()!=null)
			{
				log.info("Sort Field:["+paginationDto.getSortField()+"] Sort Order:["+paginationDto.getSortOrder()+"]");
				if(paginationDto.getSortField().equals("entityMaster.name") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by em.name asc ";
				if(paginationDto.getSortField().equals("entityMaster.name") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by em.name desc ";
				
				if(paginationDto.getSortField().equals("name") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by v.name asc  ";
				if(paginationDto.getSortField().equals("name") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by v.name desc  ";
				
				if(paginationDto.getSortField().equals("referenceNumberPrefix") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by v.reference_number_prefix asc  ";
				if(paginationDto.getSortField().equals("referenceNumberPrefix") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by v.reference_number_prefix desc  ";
				
				if(paginationDto.getSortField().equals("createdDate") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by v.created_date asc ";
				if(paginationDto.getSortField().equals("createdDate") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by v.created_date desc ";
				
				if(paginationDto.getSortField().equals("status") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by vl.status asc ";
				if(paginationDto.getSortField().equals("status") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by vl.status desc ";
				
				if(paginationDto.getSortField().equals("voucherType.name") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by vt.name asc  ";
				if(paginationDto.getSortField().equals("voucherType.name") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by vt.name desc  ";
				
				if(paginationDto.getSortField().equals("netAmount") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by v.net_amount asc  ";
				if(paginationDto.getSortField().equals("netAmount") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by v.net_amount desc  ";
				
				if(paginationDto.getSortField().equals("paymentMode.paymentMode") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by pm.payment_mode asc  ";
				if(paginationDto.getSortField().equals("paymentMode.paymentMode") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by pm.payment_mode desc  ";

				mainQuery+=" limit "+pageSize+" offset "+start+";";
			}
			log.info("budget transfer main query--------"+mainQuery);
			
			listofData = jdbcTemplate.queryForList(mainQuery);
			
			if(listofData != null) {
				log.info("result list size--------"+listofData.size());
				for (Map<String, Object> data : listofData) {
					Voucher voucherObj = new Voucher();
					voucherObj.setId(Long.valueOf(data.get("id").toString()));
					voucherObj.setName(data.get("voucher_name").toString());
					voucherObj.setCreatedDate((Date)data.get("voucher_date"));
					VoucherType voucherType = new VoucherType();
					voucherType.setName(data.get("voucher_type").toString());
					voucherObj.setVoucherType(voucherType);
					PaymentMode paymentMode = new PaymentMode();
					paymentMode.setPaymentMode(data.get("payment_mode").toString());
					voucherObj.setPaymentMode(paymentMode);
					if(data.get("status") != null) {
					VoucherStatus voucherStatus = VoucherStatus.valueOf(data.get("status").toString());
					voucherObj.setStatus(voucherStatus);
					}
					voucherObj.setNetAmount(Double.valueOf(data.get("voucher_amount").toString()));
					EntityMaster entityMasterObj = new EntityMaster();
					if(data.get("entity_code")!=null && data.get("entity_name")!=null) {
						entityMasterObj.setCode(Integer.valueOf(data.get("entity_code").toString()));
						entityMasterObj.setName(data.get("entity_name").toString());
					}
					voucherObj.setEntityMaster(entityMasterObj);
					voucherObj.setReferenceNumber(Long.valueOf(data.get("reference_number").toString()));
					voucherObj.setReferenceNumberPrefix(data.get("reference_number_prefix").toString());
					voucherList.add(voucherObj);
				}
			}
			
			response.setResponseContents(voucherList);
			response.setTotalRecords(totalRecords);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("Exception occured in VoucherListService.fetchAllVoucherListLazy() method", e);
			response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<==== Ends VoucherListService.fetchAllVoucherListLazy method =====>");
		return responseWrapper.send(response);

	}
	
	public BaseDTO getVoucherDeatailsById(Long id) {
		log.info(" VoucherListService.getVoucherDeatails method start");
		BaseDTO baseDTO = new BaseDTO();
		VoucherResponseDTO response = new VoucherResponseDTO();
		try {
			Voucher voucher = new Voucher();
			if (id != null) {
				voucher = voucherRepository.findOne(id);
			}

			//log.info("entityId....." + voucher.getEntityMaster().getId());
			EntityMaster entityMaster = new EntityMaster();
			if (voucher.getEntityMaster() != null) {
				entityMaster = entityMasterRepository.findOne(voucher.getEntityMaster().getId());
			}
			List<VoucherDetails> voucherDetailsList = new ArrayList<>();
			if (voucher.getId() != null) {
				voucherDetailsList = voucherDetailsRepository.findVoucherDetailsByVoucherId(voucher.getId());
			}
			response.setTransactionAmount(voucherDetailsList.get(0).getAmount());
			response.setVoucher(voucher);
			response.setVoucherId(voucher.getId());
			response.setVoucherName(voucher.getName());
			response.setFromDate(voucher.getFromDate());
			response.setToDate(voucher.getToDate());
			response.setVoucherNumberPrfix(voucher.getReferenceNumberPrefix());
			response.setVoucherNumber(voucher.getReferenceNumber());
			response.setVoucherCreatedDate(voucher.getCreatedDate());
			response.setPaymentFor(voucher.getPaymentFor());
			response.setNarration(voucher.getNarration());
			response.setVoucherTypes(voucher.getVoucherType());
			response.setCreatedDate(voucher.createdDate);
			response.setCreatedBy(voucher.getCreatedBy());
			response.setEntityMaster(entityMaster);
			response.setTransactionAmount(voucherDetailsList.get(0).getAmount());
			response.setVoucherDetailsList(voucherDetailsList);
			VoucherNote  VoucherNote=voucherNoteRepository.getForwardto(voucher.getId());
			if(VoucherNote!=null) {
				EmployeeMaster employeeMaster=employeeMasterRepository.getEmployeeByUserId(VoucherNote.getForwardTo().getId());
				if(employeeMaster!=null)
				response.setForwardto(employeeMaster.getFirstName());	
			}
				
			
			
			response.setVoucherType(voucher.getVoucherType()!=null?voucher.getVoucherType().getName():"");
			baseDTO.setResponseContent(response);
		} catch (RestException exception) {

			log.error("VoucherListService getVoucherDeatailsById RestException ", exception);

			baseDTO.setStatusCode(exception.getStatusCode());

		} catch (Exception e) {

			log.error("Exception in VoucherListService  getVoucherDeatailsById method", e);

			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());

		}

		return responseWrapper.send(baseDTO);
	}
	

}
