package in.gov.cooptex.finance.service;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import in.gov.cooptex.core.accounts.model.BudgetConfig;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.IntensiveInspectionTestStockDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.finance.repository.BudgetConfigRepository;
import in.gov.cooptex.core.finance.repository.FinancialYearRepository;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EmployeeEducationQualification;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeePersonalInfoEmployment;
import in.gov.cooptex.core.model.EmployeePromotionDetails;
import in.gov.cooptex.core.model.EmployeeTransferDetails;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.FinancialYear;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.QualificationMaster;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.AppointmentOrderDetailRepository;
import in.gov.cooptex.core.repository.DesignationRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.ProductCategoryRepository;
import in.gov.cooptex.core.repository.ProductVarietyMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.FinanceErrorCode;
import in.gov.cooptex.finance.advance.repository.IntensiveInspectionAgedStockRepository;
import in.gov.cooptex.finance.advance.repository.IntensiveInspectionAuditRepository;
import in.gov.cooptex.finance.advance.repository.IntensiveInspectionClosingRepository;
import in.gov.cooptex.finance.advance.repository.IntensiveInspectionCreditDetailsRepository;
import in.gov.cooptex.finance.advance.repository.IntensiveInspectionDiscountStockRepository;
import in.gov.cooptex.finance.advance.repository.IntensiveInspectionEmployeeRepository;
import in.gov.cooptex.finance.advance.repository.IntensiveInspectionGeneralDetailsRepository;
import in.gov.cooptex.finance.advance.repository.IntensiveInspectionSilkStockRepository;
import in.gov.cooptex.finance.advance.repository.IntensiveInspectionStockRepository;
import in.gov.cooptex.finance.advance.repository.IntensiveInspectionTestCheckRepository;
import in.gov.cooptex.finance.advance.repository.IntensiveInspectionTestStockRepository;
import in.gov.cooptex.finance.dto.IntensiveInspectionAuditRequestDto;
import in.gov.cooptex.finance.dto.IntensiveInspectionBudgetDTO;
import in.gov.cooptex.finance.dto.IntensiveInspectionCreditSalesDTO;
import in.gov.cooptex.finance.dto.IntensiveInspectionDTO;
import in.gov.cooptex.finance.dto.StockDetailsDto;
import in.gov.cooptex.finance.dto.StockDetailsRequestDetails;
import in.gov.cooptex.operation.model.IntensiveInspectionAgedStock;
import in.gov.cooptex.operation.model.IntensiveInspectionAudit;
import in.gov.cooptex.operation.model.IntensiveInspectionClosing;
import in.gov.cooptex.operation.model.IntensiveInspectionCreditDetails;
import in.gov.cooptex.operation.model.IntensiveInspectionEmployee;
import in.gov.cooptex.operation.model.IntensiveInspectionGeneralDetails;
import in.gov.cooptex.operation.model.IntensiveInspectionSilkStock;
import in.gov.cooptex.operation.model.IntensiveInspectionStock;
import in.gov.cooptex.operation.model.IntensiveInspectionTestCheck;
import in.gov.cooptex.operation.model.IntensiveInspectionTestStock;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class IntensiveInspectionReportService {

	@Autowired
	IntensiveInspectionAuditRepository intensiveInspectionAuditRepository;

	@Autowired
	IntensiveInspectionCreditDetailsRepository intensiveInspectionCreditDetailsRepository;

	@Autowired
	IntensiveInspectionEmployeeRepository intensiveInspectionEmployeeRepository;

	@Autowired
	IntensiveInspectionGeneralDetailsRepository intensiveInspectionGeneralDetailsRepository;

	@PersistenceContext
	EntityManager entityManager;

	@Autowired
	ProductVarietyMasterRepository productVarietyMasterRepository;

	@Autowired
	EmployeeMasterRepository employeeMasterRepository;

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	FinancialYearRepository financialYearRepository;
	
	@Autowired
	BudgetConfigRepository budgetConfigRepository;

	@Autowired
	DesignationRepository designationRepository;

	@Autowired
	IntensiveInspectionClosingRepository intensiveInspectionClosingRepository;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	@Autowired
	IntensiveInspectionStockRepository intensiveInspectionStockRepository;

	@Autowired
	IntensiveInspectionTestCheckRepository intensiveInspectionTestCheckRepository;

	@Autowired
	IntensiveInspectionTestStockRepository intensiveInspectionTestStockRepository;

	@Autowired
	IntensiveInspectionDiscountStockRepository intensiveInspectionDiscountStockRepository;

	@Autowired
	IntensiveInspectionSilkStockRepository intensiveInspectionSilkStockRepository;

	@Autowired
	IntensiveInspectionAgedStockRepository intensiveInspectionAgedStockRepository;

	@Autowired
	AppointmentOrderDetailRepository appointmentOrderDetailRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	ProductCategoryRepository productCategoryRepository;

	private static final String DECIMAL_FORMAT = "#0.00";

	/**
	 * 
	 * @param intensiveInspectionAudit
	 * @return
	 */
	public BaseDTO saveOrUpdateIntensiveInspectionAudit(IntensiveInspectionAudit intensiveInspectionAudit) {
		log.info("IntensiveInspectionReportService. saveOrUpdateIntensiveInspectionAudit() - START");
		BaseDTO baseDTO = new BaseDTO();
		Long showroomId = null;
		try {

			if (intensiveInspectionAudit != null) {
				if (intensiveInspectionAudit.getId() == null) {
					showroomId = intensiveInspectionAudit.getShowroom() != null
							? intensiveInspectionAudit.getShowroom().getId()
							: null;
					log.info("IntensiveInspectionReportService. saveOrUpdateIntensiveInspectionAudit() - showroomId: "
							+ showroomId);

					Long existCount = intensiveInspectionAuditRepository.getExistCountByShowroomIdAndInspectionPeriod(
							showroomId, intensiveInspectionAudit.getInspectionDate());
					existCount = existCount != null ? existCount.longValue() : 0;
					if (existCount > 0) {
						baseDTO.setStatusCode(
								ErrorDescription.getError(FinanceErrorCode.AUDIT_DETAILS_ALREADY_EXIST).getErrorCode());
						return baseDTO;
					} else {
						if (showroomId != null) {
							intensiveInspectionAudit.setShowroom(entityMasterRepository.findOne(showroomId));
						}

						intensiveInspectionAudit.setStatus(ApprovalStage.INITIATED);
						IntensiveInspectionAudit intensiveInspectionAuditObj = intensiveInspectionAuditRepository
								.save(intensiveInspectionAudit);
						baseDTO.setResponseContent(intensiveInspectionAuditObj.getId());
						baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
					}
				} else {
					IntensiveInspectionAudit intensiveInspectionAuditObj = intensiveInspectionAuditRepository
							.findOne(intensiveInspectionAudit.getId());
					intensiveInspectionAuditObj.setInspectionDate(intensiveInspectionAudit.getInspectionDate());
					intensiveInspectionAuditObj.setInspectionPeriod(intensiveInspectionAudit.getInspectionPeriod());
					intensiveInspectionAuditRepository.save(intensiveInspectionAuditObj);
					baseDTO.setResponseContent(intensiveInspectionAuditObj.getId());
					baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
				}
			}

		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Exception at saveOrUpdateIntensiveInspectionAudit()", e);
		}
		log.info("IntensiveInspectionReportService. saveOrUpdateIntensiveInspectionAudit() - END");
		return baseDTO;
	}

	public BaseDTO getFinanceYear() {
		log.info("IntensiveInspectionReportService. getFinanceYear() - START");
		BaseDTO baseDto = new BaseDTO();
		try {
			List<FinancialYear> financialYearList = financialYearRepository.getAllFinancialYear();
			baseDto.setResponseContents(financialYearList);
			baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("Exception at getFinanceYear()", e);
		}
		log.info("IntensiveInspectionReportService. getFinanceYear() - END");
		return baseDto;
	}

	/**
	 * 
	 * @param entityId
	 * @return
	 */
	public BaseDTO getEmployeeListByEntityId(Long showroomId, Long auditId) {
		log.info("IntensiveInspectionReportService. getEmployeeListByEntityId() - START");
		BaseDTO baseDTO = new BaseDTO();
		List<EmployeeMaster> employeeList = null;
		try {
			log.info("IntensiveInspectionReportService. getEmployeeListByEntityId() - showroomId: " + showroomId);

			List<IntensiveInspectionEmployee> intensiveInspectionEmployeeList = new ArrayList<>();
			if (auditId != null) {
				intensiveInspectionEmployeeList = intensiveInspectionEmployeeRepository
						.getEmployeeListByAuditId(auditId);
			}

			if (!CollectionUtils.isEmpty(intensiveInspectionEmployeeList)) {
				for (IntensiveInspectionEmployee intensiveInspectionEmployee : intensiveInspectionEmployeeList) {
					EmployeeMaster employee = intensiveInspectionEmployee.getStaff();
					if (employee != null) {
						EmployeeMaster employeeMaster = new EmployeeMaster();
						employeeMaster.setId(employee.getId());
						employeeMaster.setFirstName(employee.getFirstName());
						employeeMaster.setLastName(employee.getLastName());
						intensiveInspectionEmployee.setStaff(null);
						intensiveInspectionEmployee.setStaff(employeeMaster);
					}
				}
				baseDTO.setResponseContent(intensiveInspectionEmployeeList);
				baseDTO.setResponseContents(null);
			} else {
				List<EmployeeMaster> employeeMasterList = employeeMasterRepository
						.getAllEmployeeByShowroomId(showroomId);
				int employeeMasterListSize = employeeMasterList != null ? employeeMasterList.size() : 0;
				log.info("IntensiveInspectionReportService. getEmployeeListByEntityId() - employeeMasterListSize: "
						+ employeeMasterListSize);

				if (!CollectionUtils.isEmpty(employeeMasterList)) {
					employeeList = new ArrayList<>();

					for (EmployeeMaster employeeMaster : employeeMasterList) {

						EmployeeMaster employeeMasterObj = new EmployeeMaster();
						employeeMasterObj.setId(employeeMaster.getId());
						employeeMasterObj.setFirstName(employeeMaster.getFirstName());
						employeeMasterObj.setLastName(employeeMaster.getLastName());

						EmployeePersonalInfoEmployment personalInfoEmployment = employeeMaster
								.getPersonalInfoEmployment();
						if (personalInfoEmployment != null) {
							EmployeePersonalInfoEmployment employeePersonalInfoEmployment = new EmployeePersonalInfoEmployment();
							employeePersonalInfoEmployment
									.setDateOfAppointment(personalInfoEmployment.getDateOfAppointment());

							Designation designation = personalInfoEmployment.getCurrentDesignation();
							Designation designationMaster = new Designation();
							designationMaster.setId(designation.getId());
							designationMaster.setName(designation.getName());
							designationMaster.setCode(designation.getCode());
							employeePersonalInfoEmployment.setCurrentDesignation(designationMaster);

							employeeMasterObj.setPersonalInfoEmployment(employeePersonalInfoEmployment);
						}

						List<EmployeeEducationQualification> qualificationList = employeeMaster.getEducationList();

						if (!CollectionUtils.isEmpty(qualificationList)) {
							EmployeeEducationQualification employeeEducationQualification = qualificationList
									.get(qualificationList.size() - 1);
							QualificationMaster qualification = employeeEducationQualification.getQualificationMaster();
							if (qualification != null) {
								QualificationMaster qualificationMaster = new QualificationMaster();
								qualificationMaster.setQualificationId(qualification.getQualificationId());
								qualificationMaster.setQualificationLevel(qualification.getQualificationLevel());
								EmployeeEducationQualification employeeEducationQualificationObj = new EmployeeEducationQualification();
								employeeEducationQualificationObj.setQualificationMaster(qualificationMaster);
								employeeMasterObj.getEducationList().add(employeeEducationQualificationObj);
							}
						}

						List<EmployeePromotionDetails> promotionDetailsList = employeeMaster
								.getEmployeePromotionDetails();
						if (!CollectionUtils.isEmpty(promotionDetailsList)) {
							EmployeePromotionDetails employeePromotionDetails = promotionDetailsList
									.get(promotionDetailsList.size() - 1);
							if (employeePromotionDetails != null) {
								EmployeePromotionDetails employeePromotionDetailsObj = new EmployeePromotionDetails();
								employeePromotionDetailsObj
										.setDateOfJoining(employeePromotionDetails.getDateOfJoining());
								employeeMasterObj.getEmployeePromotionDetails().add(employeePromotionDetailsObj);
							}
						}

						List<EmployeeTransferDetails> transferDetailsList = employeeMaster.getTransferDetailsList();
						if (!CollectionUtils.isEmpty(transferDetailsList)) {
							List<EmployeeTransferDetails> showroomTransferDetailsList = transferDetailsList.stream()
									.filter(o -> o.getTransferTo() != null
											&& o.getTransferTo().getId().longValue() == showroomId.longValue())
									.collect(Collectors.toList());
							if (!CollectionUtils.isEmpty(showroomTransferDetailsList)) {
								EmployeeTransferDetails employeeTransferDetails = showroomTransferDetailsList
										.get(showroomTransferDetailsList.size() - 1);
								if (employeeTransferDetails != null) {
									EmployeeTransferDetails employeeTransferDetailsObj = new EmployeeTransferDetails();
									employeeTransferDetailsObj.setJoinedOn(employeeTransferDetails.getJoinedOn());
									employeeMasterObj.getTransferDetailsList().add(employeeTransferDetailsObj);
								}
							}
						}

						Double securityDepositAmount = appointmentOrderDetailRepository
								.getSecurityDepositAmountByEmpId(employeeMasterObj.getId());
						Department department = new Department();
						department.setSecurityDepositAmount(securityDepositAmount != null ? securityDepositAmount : 0D);
						employeeMasterObj.setDepartment(department);
						employeeList.add(employeeMasterObj);
					}
				}
				baseDTO.setResponseContents(employeeList);
				baseDTO.setResponseContent(null);
			}

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("Exception at getEmployeeListByEntityId()", e);
		}
		log.info("IntensiveInspectionReportService. getEmployeeListByEntityId() - END");
		return baseDTO;
	}

	public BaseDTO deleteIntensiveEmployeeId(Long auditEmployeeId) {
		log.info("IntensiveInspectionReportService. deleteIntensiveEmployeeId() - START");
		BaseDTO baseDTO = new BaseDTO();
		try {
			if (auditEmployeeId != null) {
				intensiveInspectionEmployeeRepository.deleteById(auditEmployeeId);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			}
		} catch (Exception e) {
			log.error("Exception at deleteIntensiveEmployeeId()", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("IntensiveInspectionReportService. deleteIntensiveEmployeeId() - END");
		return baseDTO;
	}

	public BaseDTO saveOrUpdateIntensiveInspectionEmployee(
			IntensiveInspectionAuditRequestDto intensiveInspectionAuditRequestDto) {
		log.info("IntensiveInspectionReportService. saveOrUpdateIntensiveInspectionEmployee() - START");
		BaseDTO baseDTO = new BaseDTO();
		IntensiveInspectionAudit intensiveInspectionAudit = null;
		try {

			Long auditId = intensiveInspectionAuditRequestDto.getIntensiveInspectionAuditId();
			if (auditId != null) {
				intensiveInspectionAudit = intensiveInspectionAuditRepository.findOne(auditId);
				intensiveInspectionEmployeeRepository.deleteByAuditId(auditId);
			}

			List<IntensiveInspectionEmployee> intensiveInspectionEmployeeList = intensiveInspectionAuditRequestDto
					.getIntensiveInspectionEmployeeList();

			if (!CollectionUtils.isEmpty(intensiveInspectionEmployeeList)) {

				for (IntensiveInspectionEmployee data : intensiveInspectionEmployeeList) {
					if (data.getStaff() != null && data.getStaff().getId() != null) {
						EmployeeMaster employeeMaster = employeeMasterRepository.findOne(data.getStaff().getId());
						data.setStaff(employeeMaster);
					}
					if (data.getDesignation() != null && data.getDesignation().getId() != null) {
						Designation designationMaster = designationRepository.findOne(data.getDesignation().getId());
						data.setDesignation(designationMaster);
					}
					if (intensiveInspectionAudit != null && intensiveInspectionAudit != null) {
						data.setIntensiveInspectionAudit(intensiveInspectionAudit);
					}
					intensiveInspectionEmployeeRepository.save(data);
				}
			}

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.error("Exception at saveOrUpdateIntensiveInspectionEmployee()", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("IntensiveInspectionReportService. saveOrUpdateIntensiveInspectionEmployee() - END");
		return baseDTO;
	}
	
	private List<IntensiveInspectionClosing> getClosingData(Long showroomId, Long startYear, Long endYear, 
			String queryContent) throws Exception {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		log.info("IntensiveInspectionReportService. getIntensiveInspectionClosingData() - showroomId: "
				+ showroomId);
		List<IntensiveInspectionClosing> closingDataList = new ArrayList<>();

		Date fromDate = null;
		Date toDate = null;
		FinancialYear financialYear = financialYearRepository.getFinancialYear(startYear, endYear);
		if (financialYear != null) {
			fromDate = financialYear.getStartDate();
			toDate = financialYear.getEndDate();
		}
		
		if (fromDate != null && toDate != null) {
			queryContent = StringUtils.isNotEmpty(queryContent) ? queryContent.trim() : null;
			queryContent = StringUtils.replace(queryContent, ":entityId", String.valueOf(showroomId));
			queryContent = StringUtils.replace(queryContent, ":fromDate",
					AppUtil.getValueWithSingleQuote(dateFormat.format(fromDate)));
			queryContent = StringUtils.replace(queryContent, ":toDate",
					AppUtil.getValueWithSingleQuote(dateFormat.format(toDate)));
			
			closingDataList = jdbcTemplate.query(queryContent,
					new BeanPropertyRowMapper<IntensiveInspectionClosing>(IntensiveInspectionClosing.class));
		}
		
		return closingDataList;
	}

	/**
	 * 
	 * @param showroomId
	 * @return
	 */
	public BaseDTO getIntensiveInspectionClosingData(Long showroomId, Long startYear, Long endYear) {
		log.info("IntensiveInspectionReportService. getIntensiveInspectionClosingData() - START");
		BaseDTO baseDTO = new BaseDTO();
		try {
			ApplicationQuery applicationQuery = applicationQueryRepository
					.findByQueryName("INTENSIVE_INSPECTION_CLOSING_DATA_SQL");
			if (applicationQuery == null) {
				baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getErrorCode());
				return baseDTO;
			}
			String queryContent = applicationQuery.getQueryContent();
			List<IntensiveInspectionClosing> closingDataList = getClosingData(showroomId, startYear, endYear, queryContent);
			baseDTO.setResponseContents(closingDataList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Exception at getIntensiveInspectionClosingData()", e);
		}
		log.info("IntensiveInspectionReportService. getIntensiveInspectionClosingData() - END");
		return baseDTO;
	}

	private Double getDouble(Map<String, Object> map, String key) {
		log.info("getDouble() - Key: " + key);
		try {
			Object obj = map.get(key);
			if (obj != null) {
				log.info("getDouble() - Value: " + obj);
				String valueStr = String.valueOf(obj);
				if (!StringUtils.isEmpty(valueStr)) {
					DecimalFormat decimalFormat = new DecimalFormat(DECIMAL_FORMAT);
					Double value = decimalFormat.parse(decimalFormat.format(Double.valueOf(valueStr))).doubleValue();
					return value;
				}
			}
		} catch (Exception ex) {
			log.error("Exception at getDouble()", ex);
		}
		return null;
	}

	/**
	 * 
	 * @param intensiveInspectionAuditRequestDto
	 * @return
	 */
	public BaseDTO saveOrUpdateIntensiveInspectionClosings(
			IntensiveInspectionAuditRequestDto intensiveInspectionAuditRequestDto) {
		log.info("IntensiveInspectionReportService. saveOrUpdateIntensiveInspectionClosings() - START");
		BaseDTO baseDTO = new BaseDTO();
		IntensiveInspectionAudit intensiveInspectionAudit = null;
		try {
			List<IntensiveInspectionClosing> intensiveInspectionClosingList = null;
			Long showroomId = null;
			Long startYear = null;
			Long endYear = null;
			Long auditId = intensiveInspectionAuditRequestDto.getIntensiveInspectionAuditId();
			
			ApplicationQuery applicationQuery = applicationQueryRepository
					.findByQueryName("INTENSIVE_INSPECTION_CLOSING_DATA_SQL");
			if (applicationQuery == null) {
				baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getErrorCode());
				return baseDTO;
			}
			String queryContent = applicationQuery.getQueryContent();
			
			
			if (auditId != null) {
				intensiveInspectionAudit = intensiveInspectionAuditRepository.findOne(auditId);
				if(intensiveInspectionAudit != null) {
					showroomId = intensiveInspectionAudit.getShowroom() != null ? 
							intensiveInspectionAudit.getShowroom().getId() : null;
					String inspectionPeriod = intensiveInspectionAudit.getInspectionPeriod();
					if(StringUtils.isNotEmpty(inspectionPeriod)) {
						String startYearStr = inspectionPeriod.split("-")[0];
						startYear = StringUtils.isNotEmpty(startYearStr) ? Long.valueOf(startYearStr.trim()) : null;
						String endYearStr = inspectionPeriod.split("-")[1];
						endYear = StringUtils.isNotEmpty(endYearStr) ? Long.valueOf(endYearStr.trim()) : null;
					}
				}
				intensiveInspectionClosingRepository.deleteByAuditId(auditId);
			}
			
			if(showroomId != null && startYear != null && endYear != null) {
				intensiveInspectionClosingList = getClosingData(showroomId, startYear, endYear, queryContent);
			}
			
			if (!CollectionUtils.isEmpty(intensiveInspectionClosingList)) {

				for (IntensiveInspectionClosing intensiveInspectionClosing : intensiveInspectionClosingList) {
					if (intensiveInspectionAudit != null && intensiveInspectionAudit.getId() != null) {
						intensiveInspectionClosing.setIntensiveInspectionAudit(intensiveInspectionAudit);
					}
				}
				intensiveInspectionClosingRepository.save(intensiveInspectionClosingList);
			}
			baseDTO.setResponseContent(intensiveInspectionAuditRequestDto);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Exception at saveOrUpdateIntensiveInspectionClosings()", e);
		}
		log.info("IntensiveInspectionReportService. saveOrUpdateIntensiveInspectionClosings() - END");
		return baseDTO;
	}

	public BaseDTO saveOrUpdateIntensiveGeneralDetails(
			IntensiveInspectionGeneralDetails intensiveInspectionGeneralDetails) {
		log.info("IntensiveInspectionReportService. saveOrUpdateIntensiveGeneralDetails() - START");
		BaseDTO baseDTO = new BaseDTO();
		IntensiveInspectionAudit intensiveInspectionAudit = null;
		try {
			IntensiveInspectionAudit intensiveInspectionAuditObj = intensiveInspectionGeneralDetails
					.getIntensiveInspectionAudit();
			if (intensiveInspectionAuditObj != null && intensiveInspectionAuditObj.getId() != null) {
				intensiveInspectionAudit = intensiveInspectionAuditRepository
						.findOne(intensiveInspectionAuditObj.getId());
				intensiveInspectionAudit.setStatus(ApprovalStage.SUBMITTED);
				intensiveInspectionGeneralDetails.setIntensiveInspectionAudit(intensiveInspectionAudit);
			}
			if (intensiveInspectionGeneralDetails.getPeriodOfInspection() != null
					&& intensiveInspectionGeneralDetails.getPeriodOfInspection().getId() != null) {
				intensiveInspectionGeneralDetails.setPeriodOfInspection(financialYearRepository
						.findOne(intensiveInspectionGeneralDetails.getPeriodOfInspection().getId()));
			}

			intensiveInspectionGeneralDetailsRepository.save(intensiveInspectionGeneralDetails);
			if (intensiveInspectionAudit != null && intensiveInspectionAudit.getId() != null) {
				intensiveInspectionAuditRepository.save(intensiveInspectionAudit);
			}
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Exception at saveOrUpdateIntensiveGeneralDetails()", e);
		}
		log.info("IntensiveInspectionReportService. saveOrUpdateIntensiveGeneralDetails() - END");
		return baseDTO;
	}

	public BaseDTO saveOrUpdateIntensiveCreditDetails(
			IntensiveInspectionCreditDetails intensiveInspectionCreditDetails) {
		log.info("IntensiveInspectionReportService. saveOrUpdateIntensiveCreditDetails() - START");
		BaseDTO baseDTO = new BaseDTO();
		IntensiveInspectionAudit intensiveInspectionAudit = null;
		try {
			IntensiveInspectionAudit intensiveInspectionAuditObj = intensiveInspectionCreditDetails
					.getIntensiveInspectionAudit();
			if (intensiveInspectionAuditObj != null && intensiveInspectionAuditObj.getId() != null) {
				intensiveInspectionAudit = intensiveInspectionAuditRepository
						.findOne(intensiveInspectionAuditObj.getId());
				IntensiveInspectionCreditDetails intensiveInspectionCreditDetailsObj = intensiveInspectionCreditDetailsRepository
						.getCreditDetailsByAuditId(intensiveInspectionAuditObj.getId());
				if (intensiveInspectionCreditDetailsObj != null
						&& intensiveInspectionCreditDetailsObj.getId() != null) {
					intensiveInspectionCreditDetailsObj.setIntensiveInspectionAudit(intensiveInspectionAudit);
					intensiveInspectionCreditDetailsObj
							.setOpeningBalance(intensiveInspectionCreditDetails.getOpeningBalance());
					intensiveInspectionCreditDetailsObj
							.setDueNetAmount(intensiveInspectionCreditDetails.getDueNetAmount());
					intensiveInspectionCreditDetailsObj
							.setTotalAmount(intensiveInspectionCreditDetails.getTotalAmount());
					intensiveInspectionCreditDetailsObj
							.setRealisedAmount(intensiveInspectionCreditDetails.getRealisedAmount());
					intensiveInspectionCreditDetailsObj
							.setClosingBalance(intensiveInspectionCreditDetails.getClosingBalance());
					intensiveInspectionCreditDetailsObj
							.setIsCreditSalesEffected(intensiveInspectionCreditDetails.getIsCreditSalesEffected());
					intensiveInspectionCreditDetailsObj
							.setDefectDetails(intensiveInspectionCreditDetails.getDefectDetails());
					intensiveInspectionCreditDetailsObj.setIsLegalActionTakenPendingCases(
							intensiveInspectionCreditDetails.getIsLegalActionTakenPendingCases());
					intensiveInspectionCreditDetailsObj
							.setPendingCaseDetails(intensiveInspectionCreditDetails.getPendingCaseDetails());
					intensiveInspectionCreditDetailsObj.setIsActionTakenTimeBarredCases(
							intensiveInspectionCreditDetails.getIsActionTakenTimeBarredCases());
					intensiveInspectionCreditDetailsObj
							.setTimeBarredDefectDetails(intensiveInspectionCreditDetails.getTimeBarredDefectDetails());
					intensiveInspectionCreditDetailsRepository.save(intensiveInspectionCreditDetailsObj);
				} else {
					intensiveInspectionCreditDetails.setIntensiveInspectionAudit(intensiveInspectionAudit);
					intensiveInspectionCreditDetailsRepository.save(intensiveInspectionCreditDetails);
				}
			}
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Exception at saveOrUpdateIntensiveCreditDetails()", e);
		}
		log.info("IntensiveInspectionReportService. saveOrUpdateIntensiveCreditDetails() - END");
		return baseDTO;
	}

	public BaseDTO saveOrUpdateBudgetDetails(IntensiveInspectionBudgetDTO intensiveInspectionBudgetDTO) {
		log.info("IntensiveInspectionReportService. saveOrUpdateBudgetDetails() - START");
		BaseDTO baseDTO = new BaseDTO();
		try {
			Long auditId = intensiveInspectionBudgetDTO.getIntensiveAuditId();
			if (auditId != null) {
				IntensiveInspectionAudit intensiveInspectionAudit = intensiveInspectionAuditRepository.findOne(auditId);
				intensiveInspectionAudit
						.setIsBudgetReviewReturnsSent(intensiveInspectionBudgetDTO.getIsBudgetReviewReturnsSent());
				intensiveInspectionAuditRepository.save(intensiveInspectionAudit);

			}
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Exception at saveOrUpdateBudgetDetails()", e);
		}
		log.info("IntensiveInspectionReportService. saveOrUpdateBudgetDetails() - END");
		return baseDTO;
	}

	public BaseDTO saveOrUpdateTestStockDetails(IntensiveInspectionTestStockDTO intensiveInspectionTestStockDTO) {
		log.info("IntensiveInspectionReportService. saveOrUpdateTestStockDetails() - START");
		BaseDTO baseDTO = new BaseDTO();
		IntensiveInspectionAudit intensiveInspectionAudit = null;
		List<IntensiveInspectionTestStock> intensiveInspectionTestStockList = null;
		try {
			if (intensiveInspectionTestStockDTO != null) {
				Long auditId = intensiveInspectionTestStockDTO.getIntensiveAuditId();
				List<IntensiveInspectionTestStockDTO> testStockList = intensiveInspectionTestStockDTO
						.getTestStockDetailsList();
				IntensiveInspectionTestCheck intensiveInspectionTestCheck = intensiveInspectionTestStockDTO
						.getIntensiveInspectionTestCheck();
				if (auditId != null) {
					intensiveInspectionTestStockRepository.deleteByAuditId(auditId);
					intensiveInspectionAudit = intensiveInspectionAuditRepository.findOne(auditId);

					if (!CollectionUtils.isEmpty(testStockList)) {
						intensiveInspectionTestStockList = new ArrayList<>();
						for (IntensiveInspectionTestStockDTO intensiveInspectionTestStockDTOObj : testStockList) {
							IntensiveInspectionTestStock intensiveInspectionTestStock = new IntensiveInspectionTestStock();
							if (intensiveInspectionTestStockDTOObj.getProductId() != null) {
								intensiveInspectionTestStock.setProductVarietyMaster(productVarietyMasterRepository
										.findOne(intensiveInspectionTestStockDTOObj.getProductId()));
							}

							intensiveInspectionTestStock.setIntensiveInspectionAudit(intensiveInspectionAudit);
							intensiveInspectionTestStock
									.setBookQty(intensiveInspectionTestStockDTOObj.getBookedQuantity());
							intensiveInspectionTestStock
									.setBookValue(intensiveInspectionTestStockDTOObj.getBookedValue());
							intensiveInspectionTestStock
									.setActualQty(intensiveInspectionTestStockDTOObj.getActualQuantity());
							intensiveInspectionTestStock
									.setActualValue(intensiveInspectionTestStockDTOObj.getActualValue());
							intensiveInspectionTestStock
									.setExcessQty(intensiveInspectionTestStockDTOObj.getExcessQuantity());
							intensiveInspectionTestStock
									.setExcessValue(intensiveInspectionTestStockDTOObj.getExcessValue());
							intensiveInspectionTestStock
									.setShortQty(intensiveInspectionTestStockDTOObj.getShortQuantity());
							intensiveInspectionTestStock
									.setShortValue(intensiveInspectionTestStockDTOObj.getShortValue());
							intensiveInspectionTestStock.setComments(intensiveInspectionTestStockDTOObj.getComments());
							intensiveInspectionTestStockList.add(intensiveInspectionTestStock);
						}
						if (!CollectionUtils.isEmpty(intensiveInspectionTestStockList)) {
							intensiveInspectionTestStockRepository.save(intensiveInspectionTestStockList);
						}
					}

					if (intensiveInspectionTestCheck != null && intensiveInspectionTestCheck.getId() != null) {
						IntensiveInspectionTestCheck intensiveInspectionTestCheckObj = intensiveInspectionTestCheckRepository
								.findOne(intensiveInspectionTestCheck.getId());

						intensiveInspectionTestCheckObj
								.setIsLedgerIssuePosted(intensiveInspectionTestCheck.getIsLedgerIssuePosted());
						intensiveInspectionTestCheckObj.setPostedDate(intensiveInspectionTestCheck.getPostedDate());
						intensiveInspectionTestCheckObj
								.setIsVerifiedPostingBills(intensiveInspectionTestCheck.getIsVerifiedPostingBills());
						intensiveInspectionTestCheckObj
								.setDefectUploadPath(intensiveInspectionTestCheck.getDefectUploadPath());
						intensiveInspectionTestCheckObj.setIsPackingRegisterMaterial(
								intensiveInspectionTestCheck.getIsPackingRegisterMaterial());
						intensiveInspectionTestCheckObj
								.setIsVerifiedReceipts(intensiveInspectionTestCheck.getIsVerifiedReceipts());
						intensiveInspectionTestCheckObj
								.setIsAllGoodsReceived(intensiveInspectionTestCheck.getIsAllGoodsReceived());
						intensiveInspectionTestCheckObj
								.setIsInwardRegister(intensiveInspectionTestCheck.getIsInwardRegister());
						intensiveInspectionTestCheckObj.setIsFillAllColumnCorrectly(
								intensiveInspectionTestCheck.getIsFillAllColumnCorrectly());
						intensiveInspectionTestCheckObj
								.setIsOutwardRegister(intensiveInspectionTestCheck.getIsOutwardRegister());
						intensiveInspectionTestCheckObj
								.setIsConsigneeAcknowledge(intensiveInspectionTestCheck.getIsConsigneeAcknowledge());
						intensiveInspectionTestCheckObj
								.setIsVerifyAllItems(intensiveInspectionTestCheck.getIsVerifyAllItems());
						intensiveInspectionTestCheckObj.setNote(intensiveInspectionTestCheck.getNote());
						intensiveInspectionTestCheckObj
								.setShortExcessDefectPath(intensiveInspectionTestCheck.getShortExcessDefectPath());
						intensiveInspectionTestCheckObj
								.setShortExcessAckPath(intensiveInspectionTestCheck.getShortExcessAckPath());
						intensiveInspectionTestCheckObj
								.setIsVerifyTransferNote(intensiveInspectionTestCheck.getIsVerifyTransferNote());
						intensiveInspectionTestCheckObj.setIntensiveInspectionAudit(intensiveInspectionAudit);
						intensiveInspectionTestCheckRepository.save(intensiveInspectionTestCheckObj);
					} else {
						intensiveInspectionTestCheck.setIntensiveInspectionAudit(intensiveInspectionAudit);
						intensiveInspectionTestCheckRepository.save(intensiveInspectionTestCheck);
					}
				}
			}
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Exception at saveOrUpdateBudgetDetails()", e);
		}
		log.info("IntensiveInspectionReportService. saveOrUpdateBudgetDetails() - END");
		return baseDTO;
	}

	public BaseDTO getLazyLoadData(PaginationDTO paginationDTO) {
		log.info("IntensiveInspectionReportService. getLazyLoadData() - START");
		BaseDTO baseDTO = new BaseDTO();
		try {
			Session session = entityManager.unwrap(Session.class);
			Criteria criteria = session.createCriteria(IntensiveInspectionAudit.class, "intensiveInspectionAudit");
			// criteria.createAlias("intensiveInspectionAudit.region", "region");
			criteria.createAlias("intensiveInspectionAudit.showroom", "showroom");
			log.info(":: Criteria search started ::");
			if (paginationDTO.getFilters() != null) {

				// String region = (String) paginationDTO.getFilters().get("region");
				// if (StringUtils.isNotEmpty(region)) {
				// Criterion firstCriteria = Restrictions.like("region.name", "%" +
				// region.trim() + '%').ignoreCase();
				// if (AppUtil.isInteger(region.trim())) {
				// Criterion secondCriteria = Restrictions.eq("region.code",
				// Integer.parseInt(region.trim()));
				// criteria.add(Restrictions.or(firstCriteria, secondCriteria));
				// } else {
				// criteria.add(firstCriteria);
				// }
				// }

				String showroom = (String) paginationDTO.getFilters().get("showroom");
				if (StringUtils.isNotEmpty(showroom)) {
					Criterion firstCriteria = Restrictions.like("showroom.name", "%" + showroom.trim() + '%')
							.ignoreCase();
					if (AppUtil.isInteger(showroom.trim())) {
						Criterion secondCriteria = Restrictions.eq("showroom.code", Integer.parseInt(showroom.trim()));
						criteria.add(Restrictions.or(firstCriteria, secondCriteria));
					} else {
						criteria.add(firstCriteria);
					}
				}

				// String unitName = (String) paginationDTO.getFilters().get("unitName");
				// if (StringUtils.isNotEmpty(unitName)) {
				// criteria.add(Restrictions.like("intensiveInspectionAudit.unitName", "%" +
				// unitName.trim() + "%")
				// .ignoreCase());
				// }
				String inspectionPeriod = (String) paginationDTO.getFilters().get("inspectionPeriod");
				if (StringUtils.isNotEmpty(inspectionPeriod)) {
					criteria.add(Restrictions
							.like("intensiveInspectionAudit.inspectionPeriod", "%" + inspectionPeriod.trim() + "%")
							.ignoreCase());
				}
				if (paginationDTO.getFilters().get("createdDate") != null) {
					Date date = new Date((long) paginationDTO.getFilters().get("createdDate"));
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = dateFormat.format(date);
					Date minDate = dateFormat.parse(strDate);
					Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
					criteria.add(Restrictions.conjunction()
							.add(Restrictions.ge("intensiveInspectionAudit.createdDate", minDate))
							.add(Restrictions.lt("intensiveInspectionAudit.createdDate", maxDate)));
				}
				if (paginationDTO.getFilters().get("inspectionDate") != null) {
					Date date = new Date((long) paginationDTO.getFilters().get("inspectionDate"));
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = dateFormat.format(date);
					Date minDate = dateFormat.parse(strDate);
					Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
					criteria.add(Restrictions.conjunction()
							.add(Restrictions.ge("intensiveInspectionAudit.inspectionDate", minDate))
							.add(Restrictions.lt("intensiveInspectionAudit.inspectionDate", maxDate)));
				}
				if (paginationDTO.getFilters().get("lastInspectionDate") != null) {
					Date date = new Date((long) paginationDTO.getFilters().get("lastInspectionDate"));
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = dateFormat.format(date);
					Date minDate = dateFormat.parse(strDate);
					Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
					criteria.add(Restrictions.conjunction()
							.add(Restrictions.ge("intensiveInspectionAudit.lastInspectionDate", minDate))
							.add(Restrictions.lt("intensiveInspectionAudit.lastInspectionDate", maxDate)));
				}
				String status = (String) paginationDTO.getFilters().get("status");
				if (StringUtils.isNotEmpty(status)) {
					criteria.add(Restrictions.like("intensiveInspectionAudit.status", "%" + status.trim() + "%")
							.ignoreCase());
				}
				criteria.setProjection(Projections.rowCount());
				Integer totalResult = ((Long) criteria.uniqueResult()).intValue();
				criteria.setProjection(null);

				ProjectionList projectionList = Projections.projectionList();
				projectionList.add(Projections.property("id"));
				// projectionList.add(Projections.property("region"));
				projectionList.add(Projections.property("showroom"));
				projectionList.add(Projections.property("inspectionDate"));
				projectionList.add(Projections.property("createdDate"));
				// projectionList.add(Projections.property("unitName"));
				projectionList.add(Projections.property("inspectionPeriod"));
				projectionList.add(Projections.property("lastInspectionDate"));
				projectionList.add(Projections.property("status"));

				criteria.setProjection(projectionList);

				if (paginationDTO.getFirst() != null) {
					Integer pageNo = paginationDTO.getFirst();
					Integer pageSize = paginationDTO.getPageSize();
					if (pageNo != null && pageSize != null) {
						criteria.setFirstResult(pageNo * pageSize);
						criteria.setMaxResults(pageSize);
						log.info("PageNo : [" + pageNo + "] pageSize[" + pageSize + "]");
					}

					String sortField = paginationDTO.getSortField();
					String sortOrder = paginationDTO.getSortOrder();
					log.info("sortField outside : [" + sortField + "] sortOrder[" + sortOrder + "]");
					if (paginationDTO.getSortField() != null && paginationDTO.getSortOrder() != null) {
						log.info("sortField : [" + paginationDTO.getSortField() + "] sortOrder["
								+ paginationDTO.getSortOrder() + "]");

						if (paginationDTO.getSortField().equals("inspectionDate")) {
							sortField = "intensiveInspectionAudit.inspectionDate";
						} else if (sortField.equals("createdDate")) {
							sortField = "intensiveInspectionAudit.createdDate";
						} else if (sortField.equals("region")) {
							sortField = "region.name";
							sortField = "region.code";
						} else if (sortField.equals("showroom")) {
							sortField = "showroom.code";
							sortField = "showroom.name";
						} else if (sortField.equals("id")) {
							sortField = "intensiveInspectionAudit.id";
						} else if (sortField.equals("unitName")) {
							sortField = "intensiveInspectionAudit.unitName";
						} else if (sortField.equals("inspectionPeriod")) {
							sortField = "intensiveInspectionAudit.inspectionPeriod";
						} else if (sortField.equals("lastInspectionDate")) {
							sortField = "intensiveInspectionAudit.lastInspectionDate";
						} else if (sortField.equals("status")) {
							sortField = "intensiveInspectionAudit.status";
						}

						if (sortOrder.equals("DESCENDING")) {
							criteria.addOrder(Order.desc(sortField));
						} else {
							criteria.addOrder(Order.asc(sortField));
						}
					} else {
						criteria.addOrder(Order.desc("intensiveInspectionAudit.modifiedDate"));
					}
				}
				List<?> resultList = criteria.list();
				log.info("criteria list executed and the list size is : " + resultList.size());
				if (resultList == null || resultList.isEmpty() || resultList.size() == 0) {
					log.info("intensiveInspectionAudit List is null or empty ");
				}
				List<IntensiveInspectionAudit> intensiveInspectionList = new ArrayList<>();
				Iterator<?> it = resultList.iterator();
				while (it.hasNext()) {
					Object ob[] = (Object[]) it.next();
					IntensiveInspectionAudit response = new IntensiveInspectionAudit();
					response.setId((Long) ob[0]);
					// EntityMaster regionEntity = ob[1] != null ? (EntityMaster) ob[1] : null;
					// EntityMaster regionObj = new EntityMaster();
					// regionObj.setId(regionEntity.getId());
					// regionObj.setCode(regionEntity.getCode());
					// regionObj.setName(regionEntity.getName());
					// response.setRegion(regionObj);

					EntityMaster showroomEntiy = ob[1] != null ? (EntityMaster) ob[1] : null;
					EntityMaster showroomObj = new EntityMaster();
					showroomObj.setId(showroomEntiy.getId());
					showroomObj.setCode(showroomEntiy.getCode());
					showroomObj.setName(showroomEntiy.getName());
					response.setShowroom(showroomObj);

					response.setInspectionDate(ob[2] != null ? (Date) ob[2] : null);
					response.setCreatedDate(ob[3] != null ? (Date) ob[3] : null);
					// response.setUnitName(ob[4] != null ? (String) ob[4] : null);
					response.setInspectionPeriod(ob[4] != null ? (String) ob[4] : null);
					response.setLastInspectionDate(ob[5] != null ? (Date) ob[5] : null);
					response.setStatus(ob[6] != null ? (String) ob[6] : null);
					intensiveInspectionList.add(response);

				}
				baseDTO.setResponseContents(intensiveInspectionList);
				baseDTO.setTotalRecords(totalResult);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (Exception exp) {
			log.error("Exception at getLazyLoadData() ", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("IntensiveInspectionReportService. getLazyLoadData() - END");
		return baseDTO;
	}

	public BaseDTO getById(Long auditId) {
		log.info("IntensiveInspectionReportService. getById() - START");
		BaseDTO baseDTO = new BaseDTO();
		IntensiveInspectionDTO intensiveInspectionDTO = new IntensiveInspectionDTO();
		List<IntensiveInspectionEmployee> intensiveEmployeeList = null;
		try {
			IntensiveInspectionAudit intensiveInspectionAudit = intensiveInspectionAuditRepository.findOne(auditId);
			if (intensiveInspectionAudit != null) {
				IntensiveInspectionAudit intensiveInspectionAuditObj = new IntensiveInspectionAudit();
				intensiveInspectionAuditObj.setId(intensiveInspectionAudit.getId());
				EntityMaster showroomEntity = intensiveInspectionAudit.getShowroom();
				if (showroomEntity != null) {
					EntityMaster entityMaster = new EntityMaster();
					entityMaster.setId(showroomEntity.getId());
					entityMaster.setCode(showroomEntity.getCode());
					entityMaster.setName(showroomEntity.getName());
					intensiveInspectionAuditObj.setShowroom(entityMaster);
				}
				intensiveInspectionAuditObj.setInspectionDate(intensiveInspectionAudit.getInspectionDate());
				intensiveInspectionAuditObj.setInspectionPeriod(intensiveInspectionAudit.getInspectionPeriod());
				intensiveInspectionAuditObj.setLastInspectionDate(intensiveInspectionAudit.getLastInspectionDate());
				intensiveInspectionAuditObj
						.setIsBudgetReviewReturnsSent(intensiveInspectionAudit.getIsBudgetReviewReturnsSent());
				String inspectionPeriod = intensiveInspectionAudit.getInspectionPeriod();
				if (StringUtils.isNotEmpty(inspectionPeriod)) {
					String startYear = inspectionPeriod.split(" - ")[0];
					String endYear = inspectionPeriod.split(" - ")[1];
					intensiveInspectionDTO.setStartYear(startYear);
					intensiveInspectionDTO.setEndYear(endYear);

				}

				List<IntensiveInspectionEmployee> intensiveInspectionEmployeeList = intensiveInspectionAudit
						.getIntensiveInspectionEmployeeList();
				if (!CollectionUtils.isEmpty(intensiveInspectionEmployeeList)) {
					intensiveEmployeeList = new ArrayList<>();
					for (IntensiveInspectionEmployee intensiveInspectionEmployee : intensiveInspectionEmployeeList) {
						IntensiveInspectionEmployee intensiveInspectionEmployeeObj = new IntensiveInspectionEmployee();
						intensiveInspectionEmployeeObj.setId(intensiveInspectionEmployee.getId());
						EmployeeMaster employee = intensiveInspectionEmployee.getStaff();
						if (employee != null) {
							EmployeeMaster staff = new EmployeeMaster();
							staff.setId(employee.getId());
							staff.setFirstName(employee.getFirstName());
							staff.setLastName(employee.getLastName());
							intensiveInspectionEmployeeObj.setStaff(staff);
						}
						Designation designation = intensiveInspectionEmployee.getDesignation();
						if (designation != null) {
							Designation designationMaster = new Designation();
							designationMaster.setId(designation.getId());
							designationMaster.setCode(designation.getCode());
							designationMaster.setName(designation.getName());
							intensiveInspectionEmployeeObj.setDesignation(designationMaster);
						}
						QualificationMaster qualification = intensiveInspectionEmployee.getQualification();
						if (qualification != null) {
							QualificationMaster qualificationMaster = new QualificationMaster();
							qualificationMaster.setQualificationId(qualification.getQualificationId());
							qualificationMaster.setQualificationLevel(qualification.getQualificationLevel());
							intensiveInspectionEmployeeObj.setQualification(qualificationMaster);
						}
						intensiveInspectionEmployeeObj.setIntensiveInspectionAudit(intensiveInspectionAuditObj);
						intensiveInspectionEmployeeObj
								.setDateOfAppointment(intensiveInspectionEmployee.getDateOfAppointment());
						intensiveInspectionEmployeeObj.setPostHeldFrom(intensiveInspectionEmployee.getPostHeldFrom());
						intensiveInspectionEmployeeObj.setSdFinished(intensiveInspectionEmployee.getSdFinished());
						intensiveInspectionEmployeeObj
								.setWorkingFromDate(intensiveInspectionEmployee.getWorkingFromDate());
						intensiveEmployeeList.add(intensiveInspectionEmployeeObj);
					}
				}
				List<IntensiveInspectionClosing> intensiveInspectionClosingStockList = intensiveInspectionAudit
						.getIntensiveInspectionClosingStockList();
				if (!CollectionUtils.isEmpty(intensiveInspectionClosingStockList)) {
					for (IntensiveInspectionClosing intensiveInspectionClosing : intensiveInspectionClosingStockList) {
						intensiveInspectionClosing.setIntensiveInspectionAudit(null);
						intensiveInspectionClosing.setIntensiveInspectionAudit(intensiveInspectionAuditObj);
					}
				}

				List<IntensiveInspectionTestStock> intensiveInspectionTestStockList = intensiveInspectionAudit
						.getIntensiveInspectionTestStockList();
				if (!CollectionUtils.isEmpty(intensiveInspectionTestStockList)) {
					for (IntensiveInspectionTestStock intensiveInspectionTestStock : intensiveInspectionTestStockList) {
						intensiveInspectionTestStock.setIntensiveInspectionAudit(null);
						ProductVarietyMaster productVariety = intensiveInspectionTestStock.getProductVarietyMaster();
						if (productVariety != null) {
							ProductVarietyMaster productVarietyMaster = new ProductVarietyMaster();
							productVarietyMaster.setId(productVariety.getId());
							productVarietyMaster.setName(productVariety.getName());
							productVarietyMaster.setCode(productVariety.getCode());
							intensiveInspectionTestStock.setProductVarietyMaster(null);
							intensiveInspectionTestStock.setProductVarietyMaster(productVarietyMaster);
						}
						intensiveInspectionTestStock.setIntensiveInspectionAudit(intensiveInspectionAuditObj);
					}
				}

				IntensiveInspectionTestCheck intensiveInspectionTestCheck = intensiveInspectionTestCheckRepository
						.getTestStockByAuditId(auditId);
				if (intensiveInspectionTestCheck != null) {
					intensiveInspectionTestCheck.setIntensiveInspectionAudit(null);
				} else {
					intensiveInspectionTestCheck = new IntensiveInspectionTestCheck();
				}

				IntensiveInspectionCreditDetails intensiveInspectionCreditDetails = intensiveInspectionCreditDetailsRepository
						.getCreditDetailsByAuditId(auditId);
				if (intensiveInspectionCreditDetails != null) {
					intensiveInspectionCreditDetails.setIntensiveInspectionAudit(null);
					intensiveInspectionCreditDetails.setIntensiveInspectionAudit(intensiveInspectionAuditObj);
				} else {
					intensiveInspectionCreditDetails = new IntensiveInspectionCreditDetails();
				}

				IntensiveInspectionGeneralDetails intensiveInspectionGeneralDetails = intensiveInspectionGeneralDetailsRepository
						.getDetailsByAuditId(auditId);
				if (intensiveInspectionGeneralDetails != null) {
					intensiveInspectionGeneralDetails.setIntensiveInspectionAudit(null);
					intensiveInspectionGeneralDetails.setIntensiveInspectionAudit(intensiveInspectionAuditObj);
				} else {
					intensiveInspectionGeneralDetails = new IntensiveInspectionGeneralDetails();
				}

				intensiveInspectionAuditObj.setIntensiveInspectionClosingStockList(intensiveInspectionClosingStockList);
				intensiveInspectionAuditObj.setIntensiveInspectionEmployeeList(intensiveEmployeeList);
				intensiveInspectionAuditObj.setIntensiveInspectionTestStockList(intensiveInspectionTestStockList);
				intensiveInspectionDTO.setIntensiveInspectionAudit(intensiveInspectionAuditObj);
				intensiveInspectionDTO.setIntensiveInspectionGeneralDetails(intensiveInspectionGeneralDetails);
				intensiveInspectionDTO.setIntensiveInspectionCreditDetails(intensiveInspectionCreditDetails);
				intensiveInspectionDTO.setIntensiveInspectionTestCheck(intensiveInspectionTestCheck);
			}
			baseDTO.setResponseContent(intensiveInspectionDTO);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception exp) {
			log.error("Exception at getById() ", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("IntensiveInspectionReportService. getById() - END");
		return baseDTO;
	}

	/**
	 * 
	 * @param stockDetailsRequestDetails
	 * @return
	 */
	public BaseDTO saveOrUpdateIntensiveInspectionStock(StockDetailsRequestDetails stockDetailsRequestDetails) {
		log.info("IntensiveInspectionReportService. saveOrUpdateIntensiveInspectionStock() - START");
		BaseDTO baseDto = new BaseDTO();
		try {
			Long auditId = stockDetailsRequestDetails.getAuditId();
			log.info("IntensiveInspectionReportService. saveOrUpdateIntensiveInspectionStock() - auditId: " + auditId);
			if (auditId != null) {

				intensiveInspectionAgedStockRepository.deleteByAuditId(auditId);
				intensiveInspectionDiscountStockRepository.deleteByAuditId(auditId);
				intensiveInspectionSilkStockRepository.deleteByAuditId(auditId);
				intensiveInspectionStockRepository.deleteByAuditId(auditId);

				IntensiveInspectionAudit intensiveInspectionAudit = intensiveInspectionAuditRepository.findOne(auditId);

				List<StockDetailsDto> ageStockDetailsDTOList = stockDetailsRequestDetails
						.getAgedWiseStockDetailsDtoList();
				if (!CollectionUtils.isEmpty(ageStockDetailsDTOList)) {
					for (StockDetailsDto ageStockDTO : ageStockDetailsDTOList) {
						IntensiveInspectionAgedStock intensiveInspectionAgedStock = new IntensiveInspectionAgedStock();
						ProductCategory productCategory = productCategoryRepository
								.findByCodeIgnoreCase(ageStockDTO.getProductCategory());
						intensiveInspectionAgedStock.setProductCategory(productCategory);
						intensiveInspectionAgedStock.setBelowOneYear(ageStockDTO.getBelowOneYear());
						intensiveInspectionAgedStock.setAboveOneYear(ageStockDTO.getOverOneYearBelowTwoYear());
						intensiveInspectionAgedStock.setAboveTwoYear(ageStockDTO.getOverTwoBelowFiveYear());
						intensiveInspectionAgedStock.setAboveThreeYear(ageStockDTO.getOverThreeBelowFiveYear());
						intensiveInspectionAgedStock.setOverFiveYear(ageStockDTO.getOverFiveYear());
						intensiveInspectionAgedStock.setIntensiveInspectionAudit(intensiveInspectionAudit);
						intensiveInspectionAgedStockRepository.save(intensiveInspectionAgedStock);
					}
				}
				log.info(
						"IntensiveInspectionReportService. saveOrUpdateIntensiveInspectionStock() - Age stock saved successfully.");

				/**
				 * NOTE : Here I get Percentage Values By using StockDetailsDto
				 * belowOneYear,overOneYearBelowTwoYear,overTwoBelowFiveYear,overThreeBelowFiveYear
				 * overFiveYear Variables for reducing more DTO Creation.
				 */

//				List<StockDetailsDto> discountStockDetailsDTOList = stockDetailsRequestDetails
//						.getDiscountWiseStockDetailsDtoList();
//				if (!CollectionUtils.isEmpty(discountStockDetailsDTOList)) {
//					for (StockDetailsDto data : discountStockDetailsDTOList) {
//						IntensiveInspectionDiscountStock intensiveInspectionDiscountStock = new IntensiveInspectionDiscountStock();
//						intensiveInspectionDiscountStock.setAprilMonthValue(data.getBeginYearCertifiedGoodsValue());
//						intensiveInspectionDiscountStock
//								.setDuringInspectionValue(data.getDuringInspectionCertifiedGoodsValue());
//						intensiveInspectionDiscountStock.setTotalValue(data.getTotalValueOfCertifiedGoodsValue());
//						intensiveInspectionDiscountStock.setSoldDuringInspection(data.getSoldCertifiedGoodsValue());
//						intensiveInspectionDiscountStock.setMarchMonthValue(data.getEndYearCertifiedGoodsValue());
//						intensiveInspectionDiscountStock.setIntensiveInspectionAudit(intensiveInspectionAudit);
//						intensiveInspectionDiscountStock.setDiscountPercentage(data.getDiscountPercentage());
//						intensiveInspectionDiscountStockRepository.save(intensiveInspectionDiscountStock);
//					}
//				}
//				log.info(
//						"IntensiveInspectionReportService. saveOrUpdateIntensiveInspectionStock() - Discount stock saved successfully.");

				List<StockDetailsDto> intensiveInspectionSilkStockList = stockDetailsRequestDetails
						.getIntensiveInspectionSilkStockList();
				if (!CollectionUtils.isEmpty(intensiveInspectionSilkStockList)) {
					for (StockDetailsDto data : intensiveInspectionSilkStockList) {
						IntensiveInspectionSilkStock intensiveInspectionSilkStock = new IntensiveInspectionSilkStock();
						intensiveInspectionSilkStock
								.setProductVarietyMaster(productVarietyMasterRepository.findOne(data.getProductId()));
						intensiveInspectionSilkStock.setIntensiveInspectionAudit(intensiveInspectionAudit);
						intensiveInspectionSilkStock.setTotalQuantity(data.getSilkStockTotalQty());
						intensiveInspectionSilkStock.setTotalValue(data.getSilkStockTotalValues());
						intensiveInspectionSilkStockRepository.save(intensiveInspectionSilkStock);
					}
				}
				log.info(
						"IntensiveInspectionReportService. saveOrUpdateIntensiveInspectionStock() - Silk stock saved successfully.");

				IntensiveInspectionStock intensiveInspectionStock = stockDetailsRequestDetails
						.getIntensiveInspectionStock();
				intensiveInspectionStock.setIntensiveInspectionAudit(intensiveInspectionAudit);
				intensiveInspectionStockRepository.save(intensiveInspectionStock);
				log.info(
						"IntensiveInspectionReportService. saveOrUpdateIntensiveInspectionStock() - intensive stock saved successfully.");

				baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				baseDto.setErrorDescription("IntensiveInspectionAudit Cannot Be Empty");
			}
		} catch (Exception e) {
			log.error("Exception at IntensiveInspectionReportService.saveOrUpdateIntensiveInspectionStock()", e);
		}
		log.info("IntensiveInspectionReportService. saveOrUpdateIntensiveInspectionStock() - END");
		return baseDto;
	}

	/**
	 * 
	 * @return
	 */
	public BaseDTO getAllActiveShowrooms(String showroomCodeOrName) {
		log.info("IntensiveInspectionReportService. getAllActiveShowrooms() - START");
		BaseDTO response = new BaseDTO();
		try {
			List<EntityMaster> showroomList = entityMasterRepository.getAllActiveShowroomData(showroomCodeOrName);
			int showroomListSize = showroomList != null ? showroomList.size() : 0;
			log.info("IntensiveInspectionReportService. getAllActiveShowrooms() - showroomListSize: "
					+ showroomListSize);

			if (!CollectionUtils.isEmpty(showroomList)) {
				List<EntityMaster> responseList = new ArrayList<>();
				for (EntityMaster obj : showroomList) {
					EntityMaster entityMaster = new EntityMaster();
					entityMaster.setId(obj.getId());
					entityMaster.setCode(obj.getCode());
					entityMaster.setName(obj.getName());
					responseList.add(entityMaster);
				}
				response.setResponseContent(responseList);
			}
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.error("Exception at getAllActiveShowrooms()", e);
			response.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("IntensiveInspectionReportService. getAllActiveShowrooms() - END");
		return response;
	}

	/**
	 * 
	 * @param showroomId
	 * @return
	 */
	public BaseDTO getLastInspectionDateByShowroomId(Long showroomId) {
		log.info("IntensiveInspectionReportService. getLastInspectionDateByShowroomId() - START");
		BaseDTO response = new BaseDTO();
		Date lastInspectionDate = null;
		try {
			IntensiveInspectionAudit intensiveInspectionAudit = intensiveInspectionAuditRepository
					.getLastInspectionAuditByShowroomId(showroomId);
			if (intensiveInspectionAudit != null) {
				lastInspectionDate = intensiveInspectionAudit.getLastInspectionDate();
			} else {
				lastInspectionDate = null;
			}
			response.setResponseContent(lastInspectionDate);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.error("Exception at getLastInspectionDateByShowroomId()", e);
			response.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("IntensiveInspectionReportService. getLastInspectionDateByShowroomId() - END");
		return response;
	}

	/**
	 * 
	 * @param showroomId
	 * @return
	 */
	public BaseDTO getBudgetReviewData(Long showroomId, Long startYear, Long endYear) {
		log.info("IntensiveInspectionReportService. getBudgetReviewData() - START");
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("IntensiveInspectionReportService. getBudgetReviewData() - showroomId: " + showroomId);
			List<IntensiveInspectionBudgetDTO> budgetDataList = new ArrayList<>();
			ApplicationQuery applicationQuery = applicationQueryRepository
					.findByQueryName("INTENSIVE_INSPECTION_BUDGET_DATA_SQL");
			if (applicationQuery == null) {
				baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getErrorCode());
				return baseDTO;
			}
			
			BudgetConfig budgetConfig = budgetConfigRepository.getByFromAndToYear(startYear, endYear);
			if (budgetConfig != null) {
				
				String queryContent = applicationQuery.getQueryContent();
				queryContent = StringUtils.isNotEmpty(queryContent) ? queryContent.trim() : null;
				queryContent = StringUtils.replace(queryContent, "${showroomId}", String.valueOf(showroomId));
				queryContent = StringUtils.replace(queryContent, ":budgetConfigId", String.valueOf(budgetConfig.getId()));

				budgetDataList = jdbcTemplate.query(queryContent,
						new BeanPropertyRowMapper<IntensiveInspectionBudgetDTO>(IntensiveInspectionBudgetDTO.class));
			}

			baseDTO.setResponseContent(budgetDataList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Exception at getBudgetReviewData()", e);
		}
		log.info("IntensiveInspectionReportService. getBudgetReviewData() - END");
		return baseDTO;
	}

	private List<IntensiveInspectionCreditSalesDTO> getCreditSalesQueryData(String queryName, Long showroomId,
			Long startYear, Long endYear) throws Exception {
		List<IntensiveInspectionCreditSalesDTO> resultList = new ArrayList<>();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		ApplicationQuery applicationQuery = applicationQueryRepository.findByQueryName(queryName);
		if (applicationQuery == null) {
			throw new Exception("Application query is not found: " + queryName);
		}

		Date fromDate = null;
		Date toDate = null;
		FinancialYear financialYear = financialYearRepository.getFinancialYear(startYear, endYear);
		if (financialYear != null) {
			fromDate = financialYear.getStartDate();
			toDate = financialYear.getEndDate();
		}

		if (fromDate != null && toDate != null) {
			String queryContent = applicationQuery.getQueryContent();
			queryContent = StringUtils.isNotEmpty(queryContent) ? queryContent.trim() : null;
			queryContent = StringUtils.replace(queryContent, "${showroomId}", String.valueOf(showroomId));
			queryContent = StringUtils.replace(queryContent, "${fromDate}",
					AppUtil.getValueWithSingleQuote(dateFormat.format(fromDate)));
			queryContent = StringUtils.replace(queryContent, "${toDate}",
					AppUtil.getValueWithSingleQuote(dateFormat.format(toDate)));

			resultList = jdbcTemplate.query(queryContent, new BeanPropertyRowMapper<IntensiveInspectionCreditSalesDTO>(
					IntensiveInspectionCreditSalesDTO.class));
		}

		return resultList;
	}

	public BaseDTO getCreditSalesData(IntensiveInspectionCreditSalesDTO request) {
		log.info("IntensiveInspectionReportService. getCreditSalesData() - START");
		BaseDTO baseDTO = new BaseDTO();
		try {
			IntensiveInspectionCreditSalesDTO intensiveInspectionCreditSalesDTO = new IntensiveInspectionCreditSalesDTO();
			Long showroomId = request.getShowroomId();
			Long startYear = request.getStartYear();
			Long endYear = request.getEndYear();

			log.info("IntensiveInspectionReportService. getCreditSalesData() - showroomId: " + showroomId);
			log.info("IntensiveInspectionReportService. getCreditSalesData() - startYear: " + startYear);
			log.info("IntensiveInspectionReportService. getCreditSalesData() - endYear: " + endYear);

			List<IntensiveInspectionCreditSalesDTO> creditSalesDataList = getCreditSalesQueryData(
					"INTENSIVE_INSPECTION_CREDIT_SALES_DATA_SQL", showroomId, startYear, endYear);
			// List<IntensiveInspectionCreditSalesDTO> creditSalesAgeWiseDataList =
			// getCreditSalesQueryData("INTENSIVE_INSPECTION_CREDIT_SALES_AGE_WISE_DATA_SQL",
			// showroomId, startYear, endYear);

			intensiveInspectionCreditSalesDTO.setCreditSalesDataList(creditSalesDataList);
			// intensiveInspectionCreditSalesDTO.setCreditSalesAgetWiseOutStandingDataList(creditSalesAgeWiseDataList);
			baseDTO.setResponseContent(intensiveInspectionCreditSalesDTO);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			baseDTO.setErrorDescription(e.getMessage());
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Exception at getCreditSalesData()", e);
		}
		log.info("intensiveInspectionCreditSalesDTO. getCreditSalesData() - END");
		return baseDTO;
	}

	public BaseDTO getActiveProductVarietyForAutoComplete(String productCodeOrName) {
		log.info("IntensiveInspectionReportService. getActiveProductVarietyForAutoComplete() - START");
		BaseDTO response = new BaseDTO();
		try {
			List<ProductVarietyMaster> productVarietyList = productVarietyMasterRepository
					.getProductByCodeOrNameAutoComplete(productCodeOrName);
			response.setResponseContent(productVarietyList);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("Exception at getActiveProductVarietyForAutoComplete()", e);
			response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("IntensiveInspectionReportService. getActiveProductVarietyForAutoComplete() - END");
		return response;
	}

	public BaseDTO getBookedQtyValue(Long auditId, Long productId) {
		log.info("IntensiveInspectionReportService. getBookedQtyValue() - START");
		BaseDTO response = new BaseDTO();
		Long showroomId = null;
		String inspectionDateStr = null;
		IntensiveInspectionTestStockDTO intensiveInspectionTestStockDTO = new IntensiveInspectionTestStockDTO();
		try {

			ApplicationQuery applicationQuery = applicationQueryRepository
					.findByQueryName("INTENSIVE_INSPECTION_BOOKED_QTY_AND_VALUE_SQL");
			if (applicationQuery == null) {
				response.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getErrorCode());
				return response;
			}

			if (auditId != null) {
				IntensiveInspectionAudit intensiveInspectionAudit = intensiveInspectionAuditRepository.findOne(auditId);
				if (intensiveInspectionAudit != null) {
					showroomId = intensiveInspectionAudit.getShowroom() != null
							? intensiveInspectionAudit.getShowroom().getId()
							: null;
					Date inspectionDate = intensiveInspectionAudit.getInspectionDate();
					if (inspectionDate != null) {
						inspectionDateStr = AppUtil.REPORT_DATE_FORMAT.format(inspectionDate);
					}
				}
			}
			String queryContent = applicationQuery.getQueryContent();
			queryContent = StringUtils.isNotEmpty(queryContent) ? queryContent.trim() : null;
			queryContent = StringUtils.replace(queryContent, "${showroomId}", String.valueOf(showroomId));
			queryContent = StringUtils.replace(queryContent, "${productId}", String.valueOf(productId));
			queryContent = StringUtils.replace(queryContent, "${inspectionDate}",
					AppUtil.getValueWithSingleQuote(inspectionDateStr));
			List<Map<String, Object>> dataMapList = jdbcTemplate.queryForList(queryContent);
			if (!CollectionUtils.isEmpty(dataMapList)) {
				Map<String, Object> dataMap = dataMapList.get(0);
				intensiveInspectionTestStockDTO.setBookedQuantity(getDouble(dataMap, "booked_quantity"));
				intensiveInspectionTestStockDTO.setBookedValue(getDouble(dataMap, "booked_value"));
			}
			response.setResponseContent(intensiveInspectionTestStockDTO);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("Exception at getBookedQtyValue()", e);
			response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("IntensiveInspectionReportService. getBookedQtyValue() - END");
		return response;
	}

	public BaseDTO getStockVerificationDateValue(StockDetailsDto stockDetailsDto) {
		log.info("IntensiveInspectionReportService. getStockVerificationDateValue() - START");
		BaseDTO response = new BaseDTO();
		Long showroomId = null;
		StockDetailsDto stockDetailsDtoObj = new StockDetailsDto();
		try {

			ApplicationQuery applicationQuery = applicationQueryRepository
					.findByQueryName("INTENSIVE_INSPECTION_LAST_STOCK_VERIFICATION_DETAILS_SQL");
			if (applicationQuery == null) {
				response.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getErrorCode());
				return response;
			}

			showroomId = stockDetailsDto.getShowroomId();
			Long auditId = stockDetailsDto.getAuditId();
			
			if (showroomId != null && auditId != null) {
				
				IntensiveInspectionStock intensiveInspectionStock = intensiveInspectionStockRepository.getByAuditId(auditId);
				
				if(intensiveInspectionStock != null && intensiveInspectionStock.getId() != null) {
					stockDetailsDtoObj.setLastStockVerificationDate(intensiveInspectionStock.getLastVerificationDate());
					stockDetailsDtoObj
							.setLastStockVerificationValue(intensiveInspectionStock.getLastVerificationValue());
					stockDetailsDtoObj.setShowroomId(showroomId);
				} else {
					String queryContent = applicationQuery.getQueryContent();
					queryContent = StringUtils.isNotEmpty(queryContent) ? queryContent.trim() : null;
					queryContent = StringUtils.replace(queryContent, "${showroomId}", String.valueOf(showroomId));
					List<Map<String, Object>> dataMapList = jdbcTemplate.queryForList(queryContent);

					if (!CollectionUtils.isEmpty(dataMapList)) {
						Map<String, Object> dataMap = dataMapList.get(0);
						stockDetailsDtoObj.setLastStockVerificationDate(dataMap.get("last_stock_verification_date") != null
								? (Date) dataMap.get("last_stock_verification_date")
								: null);
						stockDetailsDtoObj
								.setLastStockVerificationValue(getDouble(dataMap, "last_stock_verification_value"));
						stockDetailsDtoObj.setShowroomId(showroomId);
					}
				}
			}
			response.setResponseContent(stockDetailsDtoObj);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("Exception at getStockVerificationDateValue()", e);
			response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("IntensiveInspectionReportService. getStockVerificationDateValue() - END");
		return response;
	}

	private String getQuery(String queryName, Long showroomId, String inspectionDateStr) throws Exception {
		ApplicationQuery applicationQuery = applicationQueryRepository.findByQueryName(queryName);
		if (applicationQuery == null) {
			throw new Exception("Application query is not found - ");
		}

		String queryContent = applicationQuery.getQueryContent();
		queryContent = StringUtils.isNotEmpty(queryContent) ? queryContent.trim() : null;
		queryContent = StringUtils.replace(queryContent, "${showroomId}", String.valueOf(showroomId));
		queryContent = StringUtils.replace(queryContent, "${stockDate}", AppUtil.getValueWithSingleQuote(inspectionDateStr));
		return queryContent;
	}

	/**
	 * 
	 * @param showroomId
	 * @return
	 */
	public BaseDTO getIntensiveStockData(Long showroomId, String inspectionDateStr) {
		log.info("IntensiveInspectionReportService. getIntensiveStockData() - START");
		BaseDTO baseDTO = new BaseDTO();
		try {
			StockDetailsDto stockDetailsDTO = new StockDetailsDto();
			log.info("IntensiveInspectionReportService. getIntensiveStockData() - showroomId: " + showroomId);

			List<StockDetailsDto> agedStockDataList = jdbcTemplate.query(
					getQuery("INTENSIVE_INSPECTION_AGED_STOCK_DATA_SQL", showroomId, inspectionDateStr),
					new BeanPropertyRowMapper<StockDetailsDto>(StockDetailsDto.class));
			stockDetailsDTO.setAgedWiseStockDetailsDtoList(agedStockDataList);

//			List<StockDetailsDto> discountWiseStockDetailsDataList = jdbcTemplate.query(
//					getQueryContent("INTENSIVE_INSPECTION_DISCOUNT_STOCK_DATA_SQL", showroomId),
//					new BeanPropertyRowMapper<StockDetailsDto>(StockDetailsDto.class));
//			stockDetailsDTO.setDiscountWiseStockDetailsDataList(discountWiseStockDetailsDataList);

			List<StockDetailsDto> silkStockDetailsDataList = jdbcTemplate.query(
					getQuery("INTENSIVE_INSPECTION_SILK_STOCK_DATA_SQL", showroomId, inspectionDateStr),
					new BeanPropertyRowMapper<StockDetailsDto>(StockDetailsDto.class));
			stockDetailsDTO.setSilkStockDetailsDataList(silkStockDetailsDataList);

			baseDTO.setResponseContent(stockDetailsDTO);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Exception at getIntensiveStockData()", e);
		}
		log.info("IntensiveInspectionReportService. getIntensiveStockData() - END");
		return baseDTO;
	}
}
