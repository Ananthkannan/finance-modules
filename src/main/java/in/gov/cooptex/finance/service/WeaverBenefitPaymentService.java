package in.gov.cooptex.finance.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.enums.VoucherStatus;
import in.gov.cooptex.core.accounts.enums.VoucherTypeDetails;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.VoucherDetails;
import in.gov.cooptex.core.accounts.model.VoucherLog;
import in.gov.cooptex.core.accounts.model.VoucherNote;
import in.gov.cooptex.core.accounts.model.VoucherType;
import in.gov.cooptex.core.accounts.model.WeaverBenefitPayment;
import in.gov.cooptex.core.accounts.repository.SupplierTypeMasterRepository;
import in.gov.cooptex.core.accounts.repository.VoucherDetailsRepository;
import in.gov.cooptex.core.accounts.repository.VoucherLogRepository;
import in.gov.cooptex.core.accounts.repository.VoucherNoteRepository;
import in.gov.cooptex.core.accounts.repository.VoucherRepository;
import in.gov.cooptex.core.accounts.repository.VoucherTypeRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.SequenceName;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.CircleMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.PaymentMode;
import in.gov.cooptex.core.model.RelationshipMaster;
import in.gov.cooptex.core.model.SequenceConfig;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.CircleMasterRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.PaymentModeRepository;
import in.gov.cooptex.core.repository.RelationshipMasterRepository;
import in.gov.cooptex.core.repository.SequenceConfigRepository;
import in.gov.cooptex.core.repository.SupplierMasterRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.LedgerPostingException;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.dto.WeaversBenefitPaymentDTO;
import in.gov.cooptex.operation.enums.SupplierType;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.model.SupplierTypeMaster;
import in.gov.cooptex.weavers.model.WeaversBenefitScheme;
import in.gov.cooptex.weavers.repository.WeaverBenefitPaymentRepository;
import in.gov.cooptex.weavers.repository.WeaversBenefitSchemeRepository;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class WeaverBenefitPaymentService {
	
	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	WeaversBenefitSchemeRepository weaversBenefitSchemeRepository;

	@Autowired
	CircleMasterRepository circleMasterRepository;

	@Autowired
	RelationshipMasterRepository relationshipMasterRepository;

	@Autowired
	ResponseWrapper responseWrapper;

	@Autowired
	SupplierMasterRepository supplierMasterRepository;

	@Autowired
	SupplierTypeMasterRepository supplierTypeMasterRepository;

	@Autowired
	PaymentModeRepository paymentModeRepository;

	@Autowired
	VoucherRepository voucherRepository;

	@Autowired
	VoucherLogRepository voucherlogRepository;
	
	@Autowired
	UserMasterRepository userMasterRepository;

	@Autowired
	VoucherNoteRepository voucherNoteRepository;

	@Autowired
	VoucherTypeRepository voucherTypeRepository;

	@Autowired
	VoucherDetailsRepository voucherDetailsRepository;

	@Autowired
	SequenceConfigRepository sequenceConfigRepository;

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	LoginService loginService;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;
	
	@Autowired
	WeaverBenefitPaymentRepository weaverBenefitPaymentRepository;
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	public BaseDTO getActiveWeaversBenefitScheme() {
		log.info("WeaversBenefitSchemeService:getActiveWeaversBenefitScheme()");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<WeaversBenefitScheme> weaversBenefitSchemeList = weaversBenefitSchemeRepository
					.getAllActiveWeaversBenefitScheme();
			baseDTO.setResponseContent(weaversBenefitSchemeList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {

			log.error("WeaversBenefitSchemeService getActiveWeaversBenefitSchemeRestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {

			log.error("WeaversBenefitSchemeService getActiveWeaversBenefitSchemeException ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}

		log.info("WeaversBenefitSchemeService getActiveWeaversBenefitSchememethod completed");
		return baseDTO;

	}

	public BaseDTO loadAllCircleMasters() {
		log.info("< == Start Of getAllCircleMasterEntities == >>>");
		BaseDTO baseDTO = new BaseDTO();
		List<CircleMaster> circleMasterList = null;
		try {
			//circleMasterList = circleMasterRepository.findAll();
			circleMasterList=circleMasterRepository.findAllActiveCircleMasters();
			if (circleMasterList != null && circleMasterList.size() > 0) {
				log.info("circleMasterList size() " + circleMasterList.size());
				baseDTO.setResponseContent(circleMasterList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				log.info("circleMasterList is empty ");
				baseDTO.setStatusCode(ErrorDescription.ERROR_EMPTY_LIST.getCode());
			}
		} catch (Exception exception) {
			log.error("exception Occured in loadAllCircleMasters", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO getAllByCircleId(Long circleId) {
		log.info("<-== supplier Master Service getAllByCircleId ==-> " + circleId);
		BaseDTO baseDTO = new BaseDTO();
		try {
			Validate.objectNotNull(circleId, ErrorDescription.CIRCLE_ID_NOT_FOUND);
			SupplierTypeMaster suppliertype = supplierTypeMasterRepository.findByCode(SupplierType.SOCIETY);
			List<SupplierMaster> supplierMasterList = supplierMasterRepository.findByCircleIdAndSupplierType(circleId,
					suppliertype.getId());
			baseDTO.setResponseContent(supplierMasterList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("<-== supplier Master Service getAllByCircleId list ==->");
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.error("found exception in  getBankDetails", e);
		}
		log.info("<-== End supplier Master getAllByCircleId Service ==->");
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO loadAllrelationship() {
		log.info("< == Start Of loadAllrelationship == >>>");
		BaseDTO baseDTO = new BaseDTO();
		List<RelationshipMaster> relationshipList = null;
		try {
			relationshipList = relationshipMasterRepository.findAll();
			if (relationshipList != null && relationshipList.size() > 0) {
				log.info("relationshipList size() " + relationshipList.size());
				baseDTO.setResponseContent(relationshipList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				log.info("relationshipList is empty ");
				baseDTO.setStatusCode(ErrorDescription.ERROR_EMPTY_LIST.getCode());
			}
		} catch (Exception exception) {
			log.error("exception Occured in loadAllrelationship", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO loadPayments() {
		log.info(":: Start Service - Fetch All Payment Mode ::");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<PaymentMode> paymentModeList = paymentModeRepository.findAll();
			if (paymentModeList != null && paymentModeList.size() > 0) {
				baseDTO.setResponseContent(paymentModeList);
				baseDTO.setStatusCode(ErrorDescription.RECORDS_FOUND.getErrorCode());
			} else {
				throw new RestException(ErrorDescription.ERROR_RECORDS_NOT_FOUND);
			}
		} catch (RestException re) {
			log.error(":: Exception Occurs in Service - Fetch All Payment Mode :: ", re);
			baseDTO.setStatusCode(re.getStatusCode());
		} catch (Exception e) {
			log.error(":: Exception Occurs in Service - Fetch All Payment Mode :: ", e);
		}
		log.info(":: End Service - Fetch All Payment Mode ::" + baseDTO);
		return baseDTO;
	}

	public BaseDTO addWeaversbenefitpayment(WeaversBenefitPaymentDTO weaversBenefitPaymentDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("<<<<<<<======WeaverBenefitPaymentService WeaverBenefitPayment method Started=====>>"
					+ weaversBenefitPaymentDTO);
			Long voucher_id=0L;
			String refNumberPrefix = "";
//			Long sequence_value = null;
			WeaverBenefitPayment weaverBenefitPayment = new WeaverBenefitPayment();
			Voucher voucher = new Voucher();
			if(weaversBenefitPaymentDTO.getWeaverBenefitPayment()!=null &&
					weaversBenefitPaymentDTO.getWeaverBenefitPayment().getId()!=null && 
						weaversBenefitPaymentDTO.getWeaverBenefitPayment().getId()>0) {
				weaverBenefitPayment=weaverBenefitPaymentRepository.getOne(weaversBenefitPaymentDTO.getWeaverBenefitPayment().getId());
			}
			/*
			 * EntityBankBranch bankbranch = entityBankBranchRepository
			 * .findOne(cashCreditLoanLimit.getEntityBankBranch().getId());
			 */
			EntityMaster loginUserEntity = entityMasterRepository
					.findByUserRegion(loginService.getCurrentUser().getId());
			
			
			
			if (weaversBenefitPaymentDTO.getWeaverBenefitPayment().getVoucher() != null) {
				voucher_id = weaversBenefitPaymentDTO.getWeaverBenefitPayment().getVoucher().getId();
				voucher=voucherRepository.findOne(voucher_id);
				voucher.setNetAmount(weaversBenefitPaymentDTO.getWeaverBenefitPayment().getAmount());
				voucher.setNarration(VoucherTypeDetails.WEAVER_BENEFIT_PAYMENT.toString());
				voucher.setId(weaversBenefitPaymentDTO.getWeaverBenefitPayment().getVoucher().getId());
//				voucher_id = weaversBenefitPaymentDTO.getWeaverBenefitPayment().getVoucher().getId();
				VoucherDetails voucherDetails = new VoucherDetails();
				voucherDetails.setAmount(weaversBenefitPaymentDTO.getWeaverBenefitPayment().getAmount());
				voucher.setPaymentMode(paymentModeRepository.findById(weaversBenefitPaymentDTO.getPaymentmode().getId()));
				voucherDetailsRepository.deleteVoucherDetailsByVoucherID(voucher_id);
			} else {
				voucher.setName(VoucherTypeDetails.WEAVER_BENEFIT_PAYMENT.toString());
				VoucherType voucherType = voucherTypeRepository.findByName("Payment");
				voucher.setVoucherType(voucherType);
				voucher.setNetAmount(weaversBenefitPaymentDTO.getWeaverBenefitPayment().getAmount());
				voucher.setNarration(VoucherTypeDetails.WEAVER_BENEFIT_PAYMENT.toString());
				SequenceConfig sequenceConfig = sequenceConfigRepository
						.findBySequenceName(SequenceName.valueOf(SequenceName.WEAVER_BENEFIT_PAYMENT.name()));
				if (sequenceConfig == null) {
					throw new RestException(ErrorDescription.SEQUENCE_CONFIG_NOT_EXIST);
				}
				sequenceConfig.setCurrentValue(sequenceConfig.getCurrentValue() + 1);

				sequenceConfigRepository.save(sequenceConfig);
				refNumberPrefix = loginUserEntity.getCode() + sequenceConfig.getSeparator() + sequenceConfig.getPrefix()
						+ AppUtil.getCurrentYearString() + AppUtil.getCurrentMonthString()
						+ sequenceConfig.getCurrentValue();
//				sequence_value = sequenceConfig.getCurrentValue();
				log.info("Voucher Number Prefix  ", refNumberPrefix);
				voucher.setReferenceNumberPrefix(refNumberPrefix);
				voucher.setReferenceNumber(sequenceConfig.getCurrentValue());
				voucher.setFromDate(new Date());
				voucher.setToDate(new Date());
				voucher.setPaymentMode(paymentModeRepository.findById(weaversBenefitPaymentDTO.getPaymentmode().getId()));
			}
			
			voucher = voucherRepository.save(voucher);
			
			VoucherDetails voucherDetails = new VoucherDetails();
			/*
			 * VoucherDetails voucherDetail = voucherDetailsRepository
			 * .getfindVoucherDetailsByVoucherId(voucher.getId()); if(voucherDetail!=null) {
			 * voucherDetails = new VoucherDetails(); voucherDetails=voucherDetail;
			 * voucherDetails.setAmount(cashCreditLoanLimit.getSanctionedLoanLimit());
			 * voucherDetailsRepository.save(voucherDetails); } else {
			 */
			voucherDetails.setAmount(weaversBenefitPaymentDTO.getWeaverBenefitPayment().getAmount());
			voucherDetails.setVoucher(voucher);
			voucherDetailsRepository.save(voucherDetails);

			if (weaverBenefitPayment.getId() != null) {
				weaverBenefitPayment = weaverBenefitPaymentRepository.findOne(weaverBenefitPayment.getId());
			}
			weaverBenefitPayment.setAmount(weaversBenefitPaymentDTO.getWeaverBenefitPayment().getAmount());
			weaverBenefitPayment.setNomineeName(weaversBenefitPaymentDTO.getWeaverBenefitPayment().getNomineeName());
			weaverBenefitPayment.setWeaverDob(weaversBenefitPaymentDTO.getWeaverBenefitPayment().getWeaverDob());
			weaverBenefitPayment.setWeaverDod(weaversBenefitPaymentDTO.getWeaverBenefitPayment().getWeaverDod());
			weaverBenefitPayment
					.setWeaverMemberName(weaversBenefitPaymentDTO.getWeaverBenefitPayment().getWeaverMemberName());
			weaverBenefitPayment
					.setWeaverMemberNumber(weaversBenefitPaymentDTO.getWeaverBenefitPayment().getWeaverMemberNumber());
			RelationshipMaster relation = relationshipMasterRepository
					.findOne(weaversBenefitPaymentDTO.getWeaverBenefitPayment().getRelationshipMaster().getId());
			weaverBenefitPayment.setRelationshipMaster(relation);
			SupplierMaster supplier = supplierMasterRepository
					.findOne(weaversBenefitPaymentDTO.getWeaverBenefitPayment().getSupplierMaster().getId());
			weaverBenefitPayment.setSupplierMaster(supplier);
			WeaversBenefitScheme benefitscheme = weaversBenefitSchemeRepository
					.findOne(weaversBenefitPaymentDTO.getWeaverBenefitPayment().getWeaversBenefitScheme().getId());
			weaverBenefitPayment.setWeaversBenefitScheme(benefitscheme);
			if (weaversBenefitPaymentDTO.getWeaverBenefitPayment().getVoucher() != null) {
				weaverBenefitPayment.setVoucher(voucherRepository
						.findOne(weaversBenefitPaymentDTO.getWeaverBenefitPayment().getVoucher().getId()));
			} else {
				weaverBenefitPayment.setVoucher(voucher);
			}
			weaverBenefitPaymentRepository.save(weaverBenefitPayment);
			VoucherLog voucherlog = new VoucherLog();
			voucherlog.setStatus(VoucherStatus.SUBMITTED);
			UserMaster currentuser=userMasterRepository.findOne(loginService.getCurrentUser().getId());
			voucherlog.setUserMaster(currentuser);
			if (weaversBenefitPaymentDTO.getWeaverBenefitPayment().getVoucher() != null) {
				voucherlog.setVoucher(voucherRepository
						.findOne(weaversBenefitPaymentDTO.getWeaverBenefitPayment().getVoucher().getId()));
			} else {
				voucherlog.setVoucher(voucher);
			}
			voucherlogRepository.save(voucherlog);
			VoucherNote voucherNote = new VoucherNote();
			if (weaversBenefitPaymentDTO.getWeaverBenefitPayment().getVoucher() !=null) {
				voucherNote.setVoucher(voucherRepository
						.findOne(weaversBenefitPaymentDTO.getWeaverBenefitPayment().getVoucher().getId()));
			} else {
				voucherNote.setVoucher(voucher);
			}
			voucherNote.setNote(weaversBenefitPaymentDTO.getVouchernote().getNote());
			UserMaster user=userMasterRepository.findOne(weaversBenefitPaymentDTO.getVouchernote().getForwardTo().getId());
			voucherNote.setForwardTo(user);
			voucherNote.setFinalApproval(weaversBenefitPaymentDTO.getVouchernote().getFinalApproval());
			voucherNoteRepository.save(voucherNote);

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("<<<<<<<======WeaverBenefitPaymentService createWeaverBenefitPayment method Ended=====>>"
					+ weaverBenefitPayment);
			return baseDTO;
		} catch (Exception e) {
			log.info(
					"<<<<<<<======WeaverBenefitPaymentService createWeaverBenefitPayment method Ended with error=====>>");
			e.printStackTrace();
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			return null;
		}
	}
	
	
	public BaseDTO getweaverBenefitPaymentList(PaginationDTO paginationDTO) {
		BaseDTO baseDTO = new BaseDTO();
		List<WeaversBenefitPaymentDTO> responseDtoList = new ArrayList<>();
		try {
			log.info("<<<< ------- Start WeaverBenefitPaymentService.getweaverBenefitPaymentList ---------- >>>>>>>");
Long voucherid;
			log.info(":: WeaverBenefitPayment search started ::");
			Session session = entityManager.unwrap(Session.class);
			Criteria criteria = session.createCriteria(WeaverBenefitPayment.class, "weaverBenefitPayment");
			criteria.createAlias("weaverBenefitPayment.supplierMaster", "supplierMaster");
			criteria.createAlias("supplierMaster.circleMaster", "circleMaster");
			criteria.createAlias("weaverBenefitPayment.voucher", "voucher");

			log.info(":: Criteria search started ::");
			if (paginationDTO.getFilters() != null) {

				String circlename = (String) paginationDTO.getFilters().get("circlename");
				if (circlename != null) {
					criteria.add(Restrictions.like("circleMaster.name", "%" + circlename.trim() + "%").ignoreCase());
				}
				if (paginationDTO.getFilters().get("suppliername") != null) {
					String suppliername = paginationDTO.getFilters().get("suppliername").toString().trim();
					criteria.add(Restrictions
							.like("supplierMaster.name", "%" + suppliername.trim() + "%").ignoreCase());
				}
				if (paginationDTO.getFilters().get("weavername") != null) {
					String weavername = paginationDTO.getFilters().get("weavername").toString();
					criteria.add(Restrictions
							.like("weaverBenefitPayment.weaverMemberName", "%" + weavername.trim() + "%")
							.ignoreCase());

				}
				if (paginationDTO.getFilters().get("membershipnum") != null) {
					String membershipnum = paginationDTO.getFilters().get("membershipnum").toString();
					criteria.add(Restrictions.like("weaverBenefitPayment.weaverMemberNumber", "%" + membershipnum.trim() + "%")
							
							.ignoreCase());
				}
				
				if (paginationDTO.getFilters().get("benefitamount") != null) {
					String benefitamount = paginationDTO.getFilters().get("benefitamount").toString();
					criteria.add(Restrictions.like("weaverBenefitPayment.amount", "%" + benefitamount.trim() + "%")
							.ignoreCase());
				}

				/*if (paginationDTO.getFilters().get("createdDate") != null) {
					Date date = new Date((long) paginationDTO.getFilters().get("createdDate"));
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = dateFormat.format(date);
					Date minDate = dateFormat.parse(strDate);
					Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
					criteria.add(
							Restrictions.conjunction().add(Restrictions.ge("cashcreditloanlimit.createdDate", minDate))
									.add(Restrictions.lt("cashcreditloanlimit.createdDate", maxDate)));
				}*/

				criteria.setProjection(Projections.rowCount());
				Integer totalResult = ((Long) criteria.uniqueResult()).intValue();
				criteria.setProjection(null);

				ProjectionList projectionList = Projections.projectionList();
				projectionList.add(Projections.property("weaverBenefitPayment.id"));
				projectionList.add(Projections.property("circleMaster.name"));
				projectionList.add(Projections.property("supplierMaster.name"));
				projectionList.add(Projections.property("weaverBenefitPayment.weaverMemberName"));
				projectionList.add(Projections.property("weaverBenefitPayment.weaverMemberNumber"));
				projectionList.add(Projections.property("weaverBenefitPayment.amount"));
				projectionList.add(Projections.property("voucher.id"));
				

				criteria.setProjection(projectionList);

				if (paginationDTO.getFirst() != null) {
					Integer pageNo = paginationDTO.getFirst();
					Integer pageSize = paginationDTO.getPageSize();
					if (pageNo != null && pageSize != null) {
						criteria.setFirstResult(pageNo * pageSize);
						criteria.setMaxResults(pageSize);
						log.info("PageNo : [" + pageNo + "] pageSize[" + pageSize + "]");
					}

					String sortField = paginationDTO.getSortField();
					String sortOrder = paginationDTO.getSortOrder();
					log.info("sortField outside : [" + sortField + "] sortOrder[" + sortOrder + "]");
					if (paginationDTO.getSortField() != null && paginationDTO.getSortOrder() != null) {
						log.info("sortField : [" + paginationDTO.getSortField() + "] sortOrder["
								+ paginationDTO.getSortOrder() + "]");

						if (paginationDTO.getSortField().equals("circlename")) {
							sortField = "circleMaster.name";
						} else if (sortField.equals("suppliername")) {
							sortField = "supplierMaster.name";
						} else if (sortField.equals("weavername")) {
							sortField = "weaverBenefitPayment.weaverMemberName";
						} else if (sortField.equals("membershipnum")) {
							sortField = "weaverBenefitPayment.weaverMemberNumber";
						} else if (sortField.equals("benefitamount")) {
							sortField = "weaverBenefitPayment.amount";
						}
						if (sortOrder.equals("DESCENDING")) {
							criteria.addOrder(Order.desc(sortField));
						} else {
							criteria.addOrder(Order.asc(sortField));
						}
					} else {
						criteria.addOrder(Order.desc("weaverBenefitPayment.id"));
					}
				}

				List<?> resultList = criteria.list();

				log.info("criteria list executed and the list size is : " + resultList!=null?resultList.size():"Empty");

				if (resultList == null || resultList.isEmpty()) {
					log.info("weaverBenefitPayment List is null or empty ");
				}

				Iterator<?> it = resultList.iterator();
				while (it.hasNext()) {
					Object obj[] = (Object[]) it.next();

					WeaversBenefitPaymentDTO dto = new WeaversBenefitPaymentDTO();

					dto.setId((Long) obj[0]);
					log.info("Id::::::::" + dto.getId());
					dto.setCirclename((String) obj[1]);
					dto.setSuppliername((String) obj[2]);
					dto.setWeavername((String) obj[3]);
					dto.setMembershipnum((String) obj[4]);
					dto.setBenefitamount((Double) obj[5]);
					dto.setVoucherId((Long) obj[6]);
					voucherid=(Long) obj[6];
					
					
					VoucherLog voucherLog=voucherlogRepository.findByVoucherId(voucherid);
					VoucherNote voucherNote=voucherNoteRepository.findByVoucherId(voucherid);

					
					dto.setVoucherlog(voucherLog);
					dto.setStatus(voucherLog.getStatus().toString());
					dto.setVouchernote(voucherNote);
					log.info(":: List Response ::" + dto);
					responseDtoList.add(dto);
				}
				baseDTO.setResponseContents(responseDtoList);
				baseDTO.setTotalRecords(totalResult);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

			}
		} catch (RestException re) {
			log.warn("<<===  Exception While getweaverBenefitPaymentList ===>>", re);
			baseDTO.setStatusCode(re.getStatusCode());
		} catch (Exception e) {
			log.error("<<===  Exception Occured ===>>", e);
		}
		log.info("<<<< ------- End UserService.getweaverBenefitPaymentList ---------- >>>>>>>");
		return baseDTO;
	}
	
	
	public BaseDTO getDetailsById(Long id) {
		log.info("<---WeaverBenefitPaymentService.getDetailsById() starts---->" + id);
		BaseDTO response = new BaseDTO();
		
		WeaversBenefitPaymentDTO weaversBenefitPaymentDTO = new WeaversBenefitPaymentDTO();
		Voucher voucher=new Voucher();
		try {
			WeaverBenefitPayment weaverBenefitPayment = weaverBenefitPaymentRepository.getOne(id);
			SupplierMaster supplierMaster=weaverBenefitPayment!=null?weaverBenefitPayment.getSupplierMaster():null;
			SupplierMaster supplierMastertemp=new SupplierMaster();
			if(weaverBenefitPayment !=null && weaverBenefitPayment.getSupplierMaster()!=null) {
				log.info(supplierMaster);
				Long circlemasterID=supplierMasterRepository.getCircleIdBySupplierId(supplierMaster.getId());
				supplierMastertemp.setCircleMaster(circleMasterRepository.getOne(circlemasterID));
				supplierMastertemp.setName(weaverBenefitPayment.getSupplierMaster().getName());
				supplierMastertemp.setCode(weaverBenefitPayment.getSupplierMaster().getCode());
				supplierMastertemp.setId(weaverBenefitPayment.getSupplierMaster().getId());
				}
			if(weaverBenefitPayment !=null) {
				weaversBenefitPaymentDTO.setWeaverBenefitPayment(weaverBenefitPayment);
//				weaversBenefitPaymentDTO.getWeaverBenefitPayment().setSupplierMaster(new SupplierMaster());
				weaversBenefitPaymentDTO.getWeaverBenefitPayment().setSupplierMaster(supplierMastertemp);
				VoucherLog voucherLog=voucherlogRepository.findByVoucherId(weaverBenefitPayment.getVoucher()!=null ?weaverBenefitPayment.getVoucher().getId():null);
				VoucherNote vouchernote=voucherNoteRepository.findByVoucherId(weaverBenefitPayment.getVoucher().getId());
				weaversBenefitPaymentDTO.setVoucherlog(voucherLog);
				weaversBenefitPaymentDTO.setVouchernote(vouchernote);
				weaversBenefitPaymentDTO.setPaymentmode(weaverBenefitPayment.getVoucher().getPaymentMode());
				
				
				voucher= voucherRepository.findOne(weaverBenefitPayment.getVoucher().getId());
			}
		
			List<Map<String, Object>> employeeData = new ArrayList<Map<String, Object>>();
			if(voucher!=null) {
				log.info("<<<:::::::voucher::::not Null::::>>>>"+voucher!=null ? voucher.getId():"Null");
				 ApplicationQuery applicationQueryForlog = applicationQueryRepository.findByQueryName("VOUCHER_LOG_EMPLOYEE_DETAILS");
				 if (applicationQueryForlog == null || applicationQueryForlog.getId() == null) {
					 log.info("Application Query For Log Details not found for query name : " + applicationQueryForlog);
					 response.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
					 return response;
				 }
				 String logquery = applicationQueryForlog.getQueryContent().trim();
				 log.info("<=========VOUCHER_LOG_EMPLOYEE_DETAILS Query Content ======>"+logquery);
				 logquery = logquery.replace(":voucherId", "'"+voucher.getId().toString()+"'");
				 log.info("Query Content For VOUCHER_LOG_EMPLOYEE_DETAILS After replaced plan id View query : " + logquery);
				 employeeData = jdbcTemplate.queryForList(logquery);
				 log.info("<=========VOUCHER_LOG_EMPLOYEE_DETAILS Employee Data======>"+employeeData);
				 response.setTotalListOfData(employeeData);
			}
			
			
			response.setResponseContent(weaversBenefitPaymentDTO);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException e) {
			log.error(" Rest Exception Occured ", e);
			response.setStatusCode(e.getStatusCode());
		} catch (Exception e) {
			log.info("Error while retriving getDetailsById----->", e);
			response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<---WeaverBenefitPaymentService.getDetailsById() end---->");
		return responseWrapper.send(response);
	}
	
	
	@Transactional
	public BaseDTO approvePayment(WeaversBenefitPaymentDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("approvePayment method start=============>" + requestDTO.getId());
			Voucher voucher = voucherRepository.findOne(requestDTO.getVoucherId());

			VoucherNote voucherNote = new VoucherNote();
			voucherNote.setVoucher(voucher);
			voucherNote.setFinalApproval(requestDTO.getVouchernote().getFinalApproval());
			voucherNote.setNote(requestDTO.getVouchernote().getNote());
			voucherNote.setForwardTo(userMasterRepository.findOne(requestDTO.getVouchernote().getForwardTo().getId()));
			voucherNote.setCreatedBy(loginService.getCurrentUser());
			voucherNote.setCreatedByName(loginService.getCurrentUser().getUsername());
			voucherNote.setCreatedDate(new Date());
			voucherNoteRepository.save(voucherNote);
			log.info("approvePayment note inserted------");
			
			VoucherLog voucherLog = new VoucherLog();
			voucherLog.setVoucher(voucher);
			voucherLog.setStatus(requestDTO.getVoucherlog().getStatus());
			voucherLog.setCreatedBy(loginService.getCurrentUser());
			voucherLog.setCreatedByName(loginService.getCurrentUser().getUsername());
			voucherLog.setCreatedDate(new Date());
			UserMaster currentuser=userMasterRepository.findOne(loginService.getCurrentUser().getId());
			voucherLog.setUserMaster(currentuser);
			voucherLog.setRemarks(requestDTO.getVoucherlog().getRemarks());
			voucherlogRepository.save(voucherLog);
			log.info("approvePayment log inserted------");
			
			log.info("Approval status----------"+requestDTO.getVoucherlog().getStatus());
			
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		}catch (LedgerPostingException l) {
			log.error("approvePayment method LedgerPostingException----", l);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("approvePayment method exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO rejectPayment(WeaversBenefitPaymentDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("rejectChequeToBank method start=============>" + requestDTO.getId());
			Voucher voucher = voucherRepository.findOne(requestDTO.getVoucherId());
			VoucherLog voucherLog = new VoucherLog();
			voucherLog.setVoucher(voucher);
			voucherLog.setStatus(requestDTO.getVoucherlog().getStatus());
			voucherLog.setCreatedBy(loginService.getCurrentUser());
			voucherLog.setCreatedByName(loginService.getCurrentUser().getUsername());
			voucherLog.setCreatedDate(new Date());
			voucherLog.setRemarks(requestDTO.getVoucherlog().getRemarks());
			UserMaster currentuser=userMasterRepository.findOne(loginService.getCurrentUser().getId());
 			voucherLog.setUserMaster(currentuser);
			voucherlogRepository.save(voucherLog);

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("rejectPayment  log inserted------");
		} catch (Exception e) {
			log.error("rejectPayment method exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

}
