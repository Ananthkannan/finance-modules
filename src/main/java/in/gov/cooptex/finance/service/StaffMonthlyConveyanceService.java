package in.gov.cooptex.finance.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.service.EntityMasterService;
import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.core.accounts.dto.StaffMonthlyConveyanceDTO;
import in.gov.cooptex.core.accounts.dto.StaffMonthlyConveyanceResponseDTO;
import in.gov.cooptex.core.accounts.enums.VoucherStatus;
import in.gov.cooptex.core.accounts.enums.VoucherTypeDetails;
import in.gov.cooptex.core.accounts.model.StaffConveyancePayment;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.VoucherDetails;
import in.gov.cooptex.core.accounts.model.VoucherLog;
import in.gov.cooptex.core.accounts.model.VoucherNote;
import in.gov.cooptex.core.accounts.model.VoucherType;
import in.gov.cooptex.core.accounts.repository.VoucherDetailsRepository;
import in.gov.cooptex.core.accounts.repository.VoucherLogRepository;
import in.gov.cooptex.core.accounts.repository.VoucherNoteRepository;
import in.gov.cooptex.core.accounts.repository.VoucherRepository;
import in.gov.cooptex.core.accounts.repository.VoucherTypeRepository;
import in.gov.cooptex.core.accounts.util.VoucherTypeCons;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.finance.repository.StaffConveyanceConfigRepository;
import in.gov.cooptex.core.finance.repository.StaffConveyancePaymentRepository;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeePersonalInfoEmployment;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.CacheRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EmployeePersonalInfoEmploymentRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class StaffMonthlyConveyanceService implements VoucherTypeCons {
	@Autowired
	UserMasterRepository userMasterRepository;
	
	@Autowired
	EntityMasterService entityMasterService;
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	EntityMasterRepository entityMasterRepository;
	
	@Autowired
	EmployeeMasterRepository employeeMasterRepository;
	
	@Autowired
	StaffConveyanceConfigRepository staffConveyanceConfigRepository;
	
	@Autowired
	StaffConveyancePaymentRepository staffConveyancePaymentRepository;
	
	@Autowired
	EmployeePersonalInfoEmploymentRepository employeePersonalInfoEmploymentRepository;
	
	@Autowired
	VoucherTypeRepository voucherTypeRepository;
	
	@Autowired
	VoucherRepository voucherRepository;
	
	@Autowired
	VoucherDetailsRepository voucherDetailsRepository;
	
	@Autowired
	VoucherNoteRepository voucherNoteRepository;
	
	@Autowired
	VoucherLogRepository voucherLogRepository;
	
	@Autowired
	CacheRepository cacheRepository;
	
	@PersistenceContext
	EntityManager entityManager;
	
	@Autowired
	ApplicationQueryRepository appQueryRepository;
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	public BaseDTO getEmployeeByLocationId(Long locationId) {
		log.info("<===== Start StaffMonthlyConveyanceService.getEmployeeByLocationId ======>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<EmployeePersonalInfoEmployment> employeePersonalInfoEmployment = employeePersonalInfoEmploymentRepository
					.findByLocationId(locationId);
			List<EmployeePersonalInfoEmployment> employeePersonalInfoEmployment1 = new ArrayList<>();
			StaffMonthlyConveyanceDTO staff = new StaffMonthlyConveyanceDTO();
			if (employeePersonalInfoEmployment != null) {
				for (EmployeePersonalInfoEmployment emp : employeePersonalInfoEmployment) {
					emp.setEmployeeMasterId(emp.getEmployeeMaster().getId());
					emp.setDesignation(emp.getDesignation());
					employeePersonalInfoEmployment1.add(emp);
				}
			}
			staff.setEmployeePersonalInfoEmploymentList(employeePersonalInfoEmployment1);
			baseDTO.setResponseContent(staff);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.info("<===== Exception in StaffMonthlyConveyanceService.getEmployeeByLocationId ======>", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<===== End StaffMonthlyConveyanceService.getEmployeeByLocationId ======>");
		return baseDTO;
	}

	public BaseDTO createStaffConveyancePayment(StaffMonthlyConveyanceDTO staffMonthlyConveyanceDTO) {
		log.info("<===== Start StaffMonthlyConveyanceService.createStaffConveyancePayment ======>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<StaffMonthlyConveyanceDTO> staffMonthlyConveyanceDTOList = new ArrayList<>();
			Map<Long, Double> designationAmount = new HashMap<>();
			if (staffMonthlyConveyanceDTO != null) {
				staffMonthlyConveyanceDTOList = staffMonthlyConveyanceDTO.getSelectstaffMonthlyConveyanceDTOList();
				designationAmount = staffMonthlyConveyanceDTO.getDesignationAmount();
			}
			if (staffMonthlyConveyanceDTOList != null) {
				String setReferenceNumber = REFNUM;
				VoucherType voucherType = voucherTypeRepository.findByName(PAYMENTSMALL);
				UserMaster umaster = userMasterRepository.findOne((loginService.getCurrentUser().getId()));
				Long seqNumber = voucherRepository.findMaxReferenceNumberForVoucher(voucherType.getId());
				Voucher vo = new Voucher();
				String month = staffMonthlyConveyanceDTO.getMonth().substring(0, 3);
				setReferenceNumber = setReferenceNumber + month + staffMonthlyConveyanceDTO.getYear();
				vo.setReferenceNumberPrefix(setReferenceNumber);
				vo.setReferenceNumber(seqNumber + 1);
				vo.setName(VoucherTypeDetails.STAFF_MONTHLY_CONVEYANCE.toString());
				vo.setVoucherType(voucherType);
				vo.setNetAmount(staffMonthlyConveyanceDTO.getTotalAmountToPaid());
				vo.setNarration(VoucherTypeDetails.STAFF_MONTHLY_CONVEYANCE.toString());
				vo.setPaymentMode(staffMonthlyConveyanceDTO.getPaymentMode());
				vo.setFromDate(staffMonthlyConveyanceDTO.getFromDate());
				vo.setToDate(staffMonthlyConveyanceDTO.getToDate());
				log.info("<===== Voucher created sucessfully ======>");

				VoucherDetails vd = new VoucherDetails();
				vd.setVoucher(vo);
				vd.setAmount(staffMonthlyConveyanceDTO.getTotalAmountToPaid());
				vo.getVoucherDetailsList().add(vd);
				log.info("<===== VoucherDetails created sucessfully ======>");

				VoucherNote vn = new VoucherNote();
				vn.setVoucher(vo);
				vn.setNote(staffMonthlyConveyanceDTO.getVoucherNote());
				UserMaster forwardTo = userMasterRepository.findOne((staffMonthlyConveyanceDTO.getForwardTo().getId()));
				vn.setForwardTo(forwardTo);
				if (staffMonthlyConveyanceDTO.getForwardFor().equals(VoucherStatus.FINALAPPROVED.toString()))
					vn.setFinalApproval(true);
				else
					vn.setFinalApproval(false);
				vo.getVoucherNote().add(vn);
				log.info("<===== VoucherNote created sucessfully ======>");

				VoucherLog vl = new VoucherLog();
				vl.setVoucher(vo);
				vl.setUserMaster(umaster);
				vl.setStatus(VoucherStatus.SUBMITTED);
				
				vo.getVoucherLogList().add(vl);
				voucherRepository.save(vo);
				log.info("<===== VoucherLog created sucessfully ======>");

				for (StaffMonthlyConveyanceDTO emp : staffMonthlyConveyanceDTOList) {
					log.info("<===== Iterate the staff details ======>");
					StaffConveyancePayment staffConveyancePayment = staffConveyancePaymentRepository
							.findByYearMonthUserId(staffMonthlyConveyanceDTO.getYear(),
									staffMonthlyConveyanceDTO.getMonth(), emp.getEmpMaster().getId());
					if (staffConveyancePayment == null) {
						EmployeeMaster employee = employeeMasterRepository.findById(emp.getEmpMaster().getId());
						StaffConveyancePayment staff = new StaffConveyancePayment();
						staff.setYear(staffMonthlyConveyanceDTO.getYear());
						staff.setMonth(staffMonthlyConveyanceDTO.getMonth());
						staff.setEmpMaster(employee);
						if (designationAmount.get(emp.getDesignationId()) != null) {
							staff.setEligibleAmount(designationAmount.get(emp.getDesignationId()));
							staff.setNetAmount(designationAmount.get(emp.getDesignationId()));
						} else {
							staff.setEligibleAmount((double) 0);
							staff.setNetAmount((double) 0);
						}
						staff.setAmountDeducted((double) 0);
						staff.setActiveStatus(true);
						staff.setVoucher(vo);
						staffConveyancePaymentRepository.save(staff);
						log.info("<===== save the staff details ======>");
					}
				}
			}
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restexp) {
			log.error("StaffMonthlyConveyanceController.createStaffConveyancePayment RestException", restexp);
			baseDTO.setStatusCode(restexp.getErrorCodeDescription().getErrorCode());
		} catch (DataIntegrityViolationException divEX) {
			log.warn("<<===  Error While StaffMonthlyConveyanceController.createStaffConveyancePayment ===>>", divEX);
		} catch (Exception exception) {
			log.error("Error while Creating  StaffMonthlyConveyanceController.createStaffConveyancePayment ",
					exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<===== End StaffMonthlyConveyanceController.createStaffConveyancePayment ======>");
		return baseDTO;
	}

	public BaseDTO showStaffConveyancePayment(StaffMonthlyConveyanceDTO staffMonthlyConveyanceDTO) {
		log.info("<===== Start StaffMonthlyConveyanceService.createStaffConveyancePayment ======>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			Double eligibleAmount = (double) 0;
			Double amountDeducted = (double) 0;
			Double netAmount = (double) 0;
			List<EmployeePersonalInfoEmployment> employeePersonalInfoEmploymentList = new ArrayList<>();
			Map<Long, Double> designationAmount = new HashMap<>();
			List<StaffMonthlyConveyanceDTO> staffMonthlyConveyanceDTOList = new ArrayList<>();
			if (staffMonthlyConveyanceDTO != null) {
				employeePersonalInfoEmploymentList = staffMonthlyConveyanceDTO.getEmployeePersonalInfoEmploymentList();
				designationAmount = staffMonthlyConveyanceDTO.getDesignationAmount();
			}
			log.info("<===== employeePersonalInfoEmploymentList======>", employeePersonalInfoEmploymentList);
			log.info("<===== designation and Amount ======>", designationAmount);
			if (employeePersonalInfoEmploymentList != null) {
				for (EmployeePersonalInfoEmployment emp : employeePersonalInfoEmploymentList) {
					StaffConveyancePayment staffConveyancePayment = staffConveyancePaymentRepository
							.findByYearMonthUserId(staffMonthlyConveyanceDTO.getYear(),
									staffMonthlyConveyanceDTO.getMonth(), emp.getEmployeeMasterId());
					EmployeeMaster employee = employeeMasterRepository.findById(emp.getEmployeeMasterId());
					StaffMonthlyConveyanceDTO staff = new StaffMonthlyConveyanceDTO();
					staff.setEmpMaster(employee);
					staff.setDesignation(emp.getDesignation()!=null?emp.getDesignation().getName():"");
					staff.setDesignationId(emp.getDesignation()!=null?emp.getDesignation().getId():0L);
					
					staff.setAmountDeducted((double) 0);

					if (staffConveyancePayment == null) {

						staff.setActiveStatus(false);
					} else if (staffConveyancePayment != null) {

						staff.setActiveStatus(true);
					}
					staff.setEmployeePersonalInfoEmploymentList(null);
					
					if (emp.getDesignation()!=null && designationAmount.get(emp.getDesignation().getId()) != null) {
						staff.setEligibleAmount(designationAmount.get(emp.getDesignation().getId()));
						eligibleAmount = eligibleAmount + designationAmount.get(emp.getDesignation().getId());
						staff.setNetAmount(designationAmount.get(emp.getDesignation().getId()));
						netAmount = netAmount + designationAmount.get(emp.getDesignation().getId());
						staffMonthlyConveyanceDTOList.add(staff);
					}
//					else {
//						staff.setEligibleAmount((double) 0);
//						staff.setNetAmount((double) 0);
//					}
					
				}
			}
			staffMonthlyConveyanceDTO.setStaffMonthlyConveyanceDTOList(staffMonthlyConveyanceDTOList);
			staffMonthlyConveyanceDTO.setEligibleAmount(eligibleAmount);
			staffMonthlyConveyanceDTO.setAmountDeducted(amountDeducted);
			staffMonthlyConveyanceDTO.setNetAmount(netAmount);
			baseDTO.setResponseContent(staffMonthlyConveyanceDTO);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restexp) {
			log.error("StaffMonthlyConveyanceController.showStaffConveyancePayment RestException", restexp);
			baseDTO.setStatusCode(restexp.getErrorCodeDescription().getErrorCode());
		} catch (DataIntegrityViolationException divEX) {
			log.warn("<<===  Error While StaffMonthlyConveyanceController.showStaffConveyancePayment ===>>", divEX);
		} catch (Exception exception) {
			log.error("Error while  StaffMonthlyConveyanceController.showStaffConveyancePayment ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<===== End StaffMonthlyConveyanceController.createStaffConveyancePayment ======>");
		return baseDTO;
	}

	public BaseDTO searchLazyList(PaginationDTO paginationDTO) {
		log.info("<===== Start StaffMonthlyConveyanceController.searchLazyList ======>");
		BaseDTO baseDTO = null;
		Integer totalRecords = 0;
		List<StaffMonthlyConveyanceResponseDTO> resultList = null;
		List<Map<String, Object>> listofData = null;
		try {
			resultList = new ArrayList<StaffMonthlyConveyanceResponseDTO>();
			baseDTO = new BaseDTO();
			Integer start = paginationDTO.getFirst(), pageSize = paginationDTO.getPageSize();
			start = start * pageSize;

			listofData = new ArrayList<Map<String, Object>>();

			ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("STAFF_CONVEYANCE_AMOUNT");
			String mainQuery = applicationQuery.getQueryContent();
			log.info("db query----------" + mainQuery);

			mainQuery = mainQuery.replaceAll(";", "");
			log.info("after replace query------" + mainQuery);

			if (paginationDTO.getFilters() != null) {
				mainQuery += " where:";
				if (paginationDTO.getFilters().get("staffName") != null) {
					log.info(" staffName Filter : " + paginationDTO.getFilters().get("staffName"));
					mainQuery += " and upper(t.Staff_name) like upper('%" + paginationDTO.getFilters().get("staffName")
							+ "%')";
				}
				if (paginationDTO.getFilters().get("voucherNumber") != null) {
					log.info(" voucherNumber Filter : " + paginationDTO.getFilters().get("voucherNumber"));
					mainQuery += " and upper(t.refer) like upper('%" + paginationDTO.getFilters().get("voucherNumber")
							+ "%')";
				}
				if (paginationDTO.getFilters().get("voucherDate") != null) {
					Date voucherDate = new Date((long) paginationDTO.getFilters().get("voucherDate"));
					log.info(" voucherDate Filter : " + voucherDate);
					mainQuery += " and date(t.create_date)='" + voucherDate + "'";
				}
				if (paginationDTO.getFilters().get("year") != null) {
					log.info(" year Filter : " + paginationDTO.getFilters().get("year"));
					mainQuery += " and cast(t.years as varchar) like ('%"
							+ paginationDTO.getFilters().get("year").toString() + "%')";
				}
				if (paginationDTO.getFilters().get("month") != null) {
					log.info(" month Filter : " + paginationDTO.getFilters().get("month"));
					mainQuery += " and upper(t.months) like upper('%"
							+ paginationDTO.getFilters().get("month").toString() + "%')";
				}
				if (paginationDTO.getFilters().get("amountPaid") != null) {
					log.info(" amountPaid Filter : " + paginationDTO.getFilters().get("amountPaid"));
					mainQuery += " and cast(t.net_amount as varchar) like ('%"
							+ paginationDTO.getFilters().get("amountPaid").toString() + "%')";
				}
				if (paginationDTO.getFilters().get("status") != null) {
					log.info(" status Filter : " + paginationDTO.getFilters().get("status"));
					mainQuery += " and upper(t.status) like upper('%"
							+ paginationDTO.getFilters().get("status").toString() + "%')";
				}
				mainQuery = mainQuery.replaceAll("where: and", "where");
				String where = mainQuery.substring(mainQuery.length() - 6);
				if (where.equals("where:"))
					mainQuery = mainQuery.replaceAll(" where:", "");
			}

			String countQuery = mainQuery.replace(
					"t.id,t.Staff_name,t.refer,t.create_date,t.years,t.months,t.net_amount,t.status",
					"count(*) as count");
			log.info("count query--------" + countQuery);

			totalRecords = jdbcTemplate.queryForObject(countQuery, Integer.class);

			log.info("count for STAFF_CONVEYANCE_AMOUNT------->" + totalRecords);

			if (paginationDTO.getSortField() != null && paginationDTO.getSortOrder() != null) {
				log.info("Sort Field:[" + paginationDTO.getSortField() + "] Sort Order:[" + paginationDTO.getSortOrder()
						+ "]");
				if (paginationDTO.getSortField().equals("staffName")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by t.Staff_name asc  ";
				else if (paginationDTO.getSortField().equals("staffName")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by t.Staff_name desc  ";
				if (paginationDTO.getSortField().equals("voucherNumber")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by t.refer asc ";
				else if (paginationDTO.getSortField().equals("voucherNumber")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by t.refer desc ";
				if (paginationDTO.getSortField().equals("voucherDate")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by t.create_date asc ";
				else if (paginationDTO.getSortField().equals("voucherDate")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by t.create_date desc ";
				if (paginationDTO.getSortField().equals("year") && paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by t.years asc ";
				else if (paginationDTO.getSortField().equals("year")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by t.years desc ";
				if (paginationDTO.getSortField().equals("month") && paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by t.months asc ";
				else if (paginationDTO.getSortField().equals("month")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by t.months desc ";
				if (paginationDTO.getSortField().equals("amountPaid")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by t.net_amount asc ";
				else if (paginationDTO.getSortField().equals("amountPaid")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by t.net_amount desc ";
				if (paginationDTO.getSortField().equals("status") && paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by t.status asc ";
				else if (paginationDTO.getSortField().equals("status")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by t.status desc ";

				mainQuery += " order by t.id desc limit " + pageSize + " offset " + start;
			} else {
				mainQuery += " order by t.id desc limit " + pageSize + " offset " + start;
			}

			listofData = jdbcTemplate.queryForList(mainQuery);
			log.info("amount transfer list size-------------->" + listofData.size());
			for (Map<String, Object> data : listofData) {
				StaffMonthlyConveyanceResponseDTO response = new StaffMonthlyConveyanceResponseDTO();
				response.setId((Long.valueOf(data.get("id").toString())));
				response.setVoucherId((Long.valueOf(data.get("id").toString())));
				response.setStaffName((String) data.get("staff_name"));
				response.setVoucherNumber((String) data.get("refer"));
				response.setVoucherDate((Date) data.get("create_date"));
				response.setYear(Long.valueOf(data.get("years").toString()));
				response.setMonth((String) data.get("months"));
				response.setAmountPaid((Double.valueOf(data.get("net_amount").toString())));
				response.setStatus((String) data.get("status"));
				resultList.add(response);
			}
			log.info("final list size---------------" + resultList.size());
			baseDTO.setTotalRecords(totalRecords);
			baseDTO.setResponseContents(resultList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception ex) {
			log.error("inside lazy method exception------", ex);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<===== End StaffMonthlyConveyanceController.searchLazyList ======>");
		return baseDTO;
	}

	public BaseDTO getStaffConveyanceId(Long voucherId) {
		log.info("<===== Start StaffMonthlyConveyanceService.getEmployeeByLocationId ======>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<StaffConveyancePayment> staffConveyancePayment = staffConveyancePaymentRepository
					.staffConveyancePaymentListVoucher(voucherId);
			StaffMonthlyConveyanceResponseDTO staffMonthlyConveyanceResponseDTO = new StaffMonthlyConveyanceResponseDTO();
			staffMonthlyConveyanceResponseDTO.setStaffConveyancePaymentList(staffConveyancePayment);
			Voucher voucher = voucherRepository.findOne(voucherId);
			VoucherNote voucherNote = voucherNoteRepository.findByVoucherId(voucherId);
			VoucherLog voucherLog = voucherLogRepository.findByVoucherId(voucherId);
			staffMonthlyConveyanceResponseDTO.setVoucherLog(voucherLog);
			staffMonthlyConveyanceResponseDTO.setVoucherNote(voucherNote);
			staffMonthlyConveyanceResponseDTO.setVoucher(voucher);

			List<Map<String, Object>> employeeData = new ArrayList<Map<String, Object>>();
			log.info("staff monthly conveyance voucherid----------" + voucherId);
			ApplicationQuery applicationQueryForlog = applicationQueryRepository
					.findByQueryName("VOUCHER_LOG_EMPLOYEE_DETAILS");
			if (applicationQueryForlog == null || applicationQueryForlog.getId() == null) {
				log.info("Application Query For Log Details not found for query name : " + applicationQueryForlog);
				baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
				return baseDTO;
			}
			String logquery = applicationQueryForlog.getQueryContent().trim();
			logquery = logquery.replace(":voucherId", voucherId.toString());
			log.info("Query staff monthly conveyance log query : " + logquery);
			employeeData = jdbcTemplate.queryForList(logquery);
			log.info("staff monthly conveyance log Employee Data======>" + employeeData.size());
			baseDTO.setTotalListOfData(employeeData);

			baseDTO.setResponseContent(staffMonthlyConveyanceResponseDTO);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.warn("<<===  Error While StaffMonthlyConveyanceController.getStaffConveyanceId ===>>", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<===== End StaffMonthlyConveyanceService.getEmployeeByLocationId ======>");
		return baseDTO;
	}

	public BaseDTO staffConveyanceApprove(StaffMonthlyConveyanceResponseDTO staffMonthlyConveyanceResponseDTO) {
		log.info("<===== Start StaffMonthlyConveyanceService.staffConveyanceApprove ======>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			UserMaster umaster = userMasterRepository.findOne((loginService.getCurrentUser().getId()));
			if (staffMonthlyConveyanceResponseDTO != null) {
				if (staffMonthlyConveyanceResponseDTO.getVoucher() != null) {
					Voucher	voucher = voucherRepository.findOne(staffMonthlyConveyanceResponseDTO.getVoucher().getId());
				if (staffMonthlyConveyanceResponseDTO.getAction()) {
						VoucherNote vn = new VoucherNote();
						vn.setVoucher(voucher);
						vn.setNote(staffMonthlyConveyanceResponseDTO.getVoucherNote().getNote());
						UserMaster forwardTo = userMasterRepository.findOne(staffMonthlyConveyanceResponseDTO
								.getStaffMonthlyConveyanceDTO().getForwardTo().getId());
						vn.setForwardTo(forwardTo);
						if (staffMonthlyConveyanceResponseDTO.getStaffMonthlyConveyanceDTO().getForwardFor()
								.equals(VoucherStatus.FINALAPPROVED.toString()))
							vn.setFinalApproval(true);
						else
							vn.setFinalApproval(false);
					voucherNoteRepository.save(vn);
					log.info("<===== VoucherNote created sucessfully ======>");
					VoucherLog vl = new VoucherLog();
					vl.setVoucher(voucher);
					vl.setUserMaster(umaster);
					if (staffMonthlyConveyanceResponseDTO.getStaffMonthlyConveyanceDTO().getButtonLable()
							.equals("Approve")) {
						vl.setStatus(VoucherStatus.APPROVED);
					}
					else if (staffMonthlyConveyanceResponseDTO.getStaffMonthlyConveyanceDTO().getButtonLable()
							.equals("Final Approve")) {
						vl.setStatus(VoucherStatus.FINALAPPROVED);
					}
					voucherLogRepository.save(vl);
					log.info("<===== VoucherLog updated sucessfully ======>");
				} else {
					VoucherLog vl = new VoucherLog();
					vl.setVoucher(voucher);
					vl.setUserMaster(umaster);
					vl.setStatus(VoucherStatus.REJECTED);
					voucherLogRepository.save(vl);
					log.info("<===== VoucherLog updated sucessfully ======>");
				}
				}
			}
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restexp) {
			log.error("StaffMonthlyConveyanceController.createStaffConveyancePayment RestException", restexp);
			baseDTO.setStatusCode(restexp.getErrorCodeDescription().getErrorCode());
		} catch (DataIntegrityViolationException divEX) {
			log.warn("<<===  Error While StaffMonthlyConveyanceController.staffConveyanceApprove ===>>", divEX);
		} catch (Exception exception) {
			log.error("Error while Creating  StaffMonthlyConveyanceController.staffConveyanceApprove ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<===== End StaffMonthlyConveyanceController.staffConveyanceApprove ======>");
		return baseDTO;
	}
}
