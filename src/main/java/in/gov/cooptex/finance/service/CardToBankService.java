package in.gov.cooptex.finance.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.core.accounts.model.EntityBankBranch;
import in.gov.cooptex.core.accounts.repository.EntityBankBranchRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.AppQueryRepository;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.finance.CardToBankDTO;
import in.gov.cooptex.finance.FlatCardToBankDTO;
import in.gov.cooptex.finance.dto.StockDetailsDto;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class CardToBankService {

	@Autowired
	JdbcTemplate jdbcTemplate;	

	@Autowired
	LoginService loginService;
	
	@Autowired
	ApplicationQueryRepository applicationQueryRepository;
	
	@Autowired
	EntityBankBranchRepository entityBankBranchRepository;

	public BaseDTO getCardDetails(Long entityCode, String fromDate, String toDate) {
		log.info("<<====   CardToBankService ---  getCardDetails ====## STARTS");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<FlatCardToBankDTO> resultList = new ArrayList<FlatCardToBankDTO>();
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			String cardDetailsQuery = "select fctb.id,fctb.opening_balance,fctb.gross_amount,fctb.net_amount,fctb.discount_amount,fctb.tax_amount,fctb.deposited_amount,fctb.closing_balance,fctb.deposit_status,fctb.closed_date from flat_card_to_bank fctb where fctb.entity_code="
					+ entityCode + " and fctb.closed_date between '" + fromDate + "' and '" + toDate
					+ "' group by fctb.closed_date,fctb.opening_balance,fctb.gross_amount,fctb.net_amount,fctb.discount_amount,fctb.tax_amount,fctb.deposited_amount,fctb.closing_balance,fctb.deposit_status,fctb.id order by fctb.closed_date";
			log.info(" cardDetailsQuery -------------->" + cardDetailsQuery);
			listofData = jdbcTemplate.queryForList(cardDetailsQuery);
			log.info(" Size :  -------------->" + listofData.size());
			for (Map<String, Object> data : listofData) {
				FlatCardToBankDTO flatCardToBankDTO = new FlatCardToBankDTO();
				String id = data.get("id") == null ? "0" : data.get("id").toString();
				String opbalance = data.get("opening_balance") == null ? "0" : data.get("opening_balance").toString();
				String grossAmount = data.get("gross_amount") == null ? "0" : data.get("gross_amount").toString();
				String netAmount = data.get("net_amount") == null ? "0" : data.get("net_amount").toString();
				String discountAmount = data.get("discount_amount") == null ? "0"
						: data.get("discount_amount").toString();
				String taxAmount = data.get("tax_amount") == null ? "0" : data.get("tax_amount").toString();
				String depositedAmount = data.get("deposited_amount") == null ? "0"
						: data.get("deposited_amount").toString();
				String closingBalance = data.get("closing_balance") == null ? "0"
						: data.get("closing_balance").toString();
				String closedDate = data.get("closed_date") == null ? "-" : data.get("closed_date").toString();
				if (closedDate != null) {
					closedDate = AppUtil.changeStringDateFormat("dd-MMM-yyyy", closedDate);
				}
				String depositStatus = data.get("deposit_status") == null ? "Deposited"
						: data.get("deposit_status").toString();
				// String
				// closedDate=data.get("closed_date")!=null?"0":data.get("closed_date").toString();
				flatCardToBankDTO.setId(Long.valueOf(id));
				flatCardToBankDTO.setDispalyClosedDate(closedDate);
				flatCardToBankDTO.setOpeningBalance(Double.valueOf(opbalance));
				flatCardToBankDTO.setGrossAmount(Double.valueOf(grossAmount));
				flatCardToBankDTO.setNetAmount(Double.valueOf(netAmount));
				flatCardToBankDTO.setDiscountAmount(Double.valueOf(discountAmount));
				flatCardToBankDTO.setTaxAmount(Double.valueOf(taxAmount));
				flatCardToBankDTO.setDepositedAmount(Double.valueOf(depositedAmount));
				// flatCardToBankDTO.setDepositStatus(Boolean.valueOf(depositStatus));
				flatCardToBankDTO.setDisplayDepositStatus(depositStatus.toUpperCase());
				flatCardToBankDTO.setClosingBalance(Double.valueOf(closingBalance));
				log.info(" Id : " + id);
				log.info(" opening balance : " + opbalance);
				log.info("  closingBalance: " + closingBalance);
				log.info(" depositStatus : " + depositStatus);
				log.info(" depositedAmount : " + depositedAmount);
				log.info(" taxAmount : " + taxAmount);
				log.info(" discountAmount : " + discountAmount);
				log.info(" netAmount : " + netAmount);
				log.info(" grossAmount : " + grossAmount);
				log.info(" Date : " + flatCardToBankDTO.getDispalyClosedDate());
				resultList.add(flatCardToBankDTO);

			}
			baseDTO.setResponseContents(resultList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			// TODO: handle exception
			log.error("Exception in CardToBankService getCardDetails Method------");
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO updateDepositedStatus(FlatCardToBankDTO requestData) {
		log.info("<<====   CardToBankService ---  updateDepositedStatus ====## STARTS");
		BaseDTO baseDTO = new BaseDTO();
		try {
			String selectedId = requestData.getDispalyClosedDate();
			String despositStatus = requestData.getDisplayDepositStatus();
			String remarks = requestData.getRemarks();
			log.info("################## SelectedID ############### : " + selectedId + " status : " + despositStatus);
			UserMaster currentUser = loginService.getCurrentUser();
			Long userId = currentUser != null ? currentUser.getId() : null;

			if ("ACKNOWLEDGED".equals(despositStatus)) {
				if (StringUtils.isNotEmpty(remarks)) {
					String updateStatusSql = "update flat_card_to_bank set deposit_status=?,remarks=?,entity_bank_branch_id=?,"
							+ "verified_date=?,verified_by=? where id in (${selectedId})";
					updateStatusSql = StringUtils.replace(updateStatusSql, "${selectedId}", selectedId);
					jdbcTemplate.update(updateStatusSql, new Object[] { despositStatus, remarks,
							requestData.getEntityBankBranchId(), requestData.getCreditedDate(), userId });
				} else {
					String updateStatusSql = "update flat_card_to_bank set deposit_status=?,entity_bank_branch_id=?,"
							+ "verified_date=?,verified_by=? where id in (${selectedId})";
					updateStatusSql = StringUtils.replace(updateStatusSql, "${selectedId}", selectedId);
					jdbcTemplate.update(updateStatusSql, new Object[] { despositStatus,
							requestData.getEntityBankBranchId(), requestData.getCreditedDate(), userId });
				}
			} else if("CANCEL_ACKNOWLEDGEMENT".equals(despositStatus)) {
				String updateStatusSql = "UPDATE flat_card_to_bank SET deposit_status='DEPOSITED',remarks=NULL,entity_bank_branch_id=NULL,verified_by=NULL,verified_date=NULL where id in(" + selectedId + ")";
				jdbcTemplate.execute(updateStatusSql);
			} else {
				String updateStatusSql = "update flat_card_to_bank set deposit_status='"
						+ requestData.getDisplayDepositStatus() + "',verified_date=now(),remarks='"
						+ requestData.getRemarks() + "',verified_by="+ userId +" where id in(" + selectedId + ")";
				jdbcTemplate.execute(updateStatusSql);
			}

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			// TODO: handle exception
			log.error("Exception in CardToBankService updateDepositedStatus Method------", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO getCardDetailsByRegionCode(Long regionCode, String fromDate, String toDate) {
		log.info("<<====   CardToBankService ---  getCardDetailsByRegionCode ====## STARTS");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<FlatCardToBankDTO> resultList = new ArrayList<FlatCardToBankDTO>();
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			String cardDetailsQuery = "select fctb.id,fctb.opening_balance,fctb.gross_amount,fctb.net_amount,fctb.discount_amount,fctb.tax_amount,fctb.deposited_amount,fctb.closing_balance,fctb.deposit_status,fctb.closed_date from flat_card_to_bank fctb where substring(entity_code::varchar,1,2)='"
					+ regionCode + "' and closed_date between '" + fromDate + "' and '" + toDate
					+ "' group by fctb.closed_date,fctb.opening_balance,fctb.gross_amount,fctb.net_amount,fctb.discount_amount,fctb.tax_amount,fctb.deposited_amount,fctb.closing_balance,fctb.deposit_status,fctb.id order by fctb.closed_date";
			log.info(" cardDetailsQuery -------------->" + cardDetailsQuery);
			listofData = jdbcTemplate.queryForList(cardDetailsQuery);
			log.info(" Size :  -------------->" + listofData.size());
			for (Map<String, Object> data : listofData) {
				FlatCardToBankDTO flatCardToBankDTO = new FlatCardToBankDTO();
				String id = data.get("id") == null ? "0" : data.get("id").toString();
				String opbalance = data.get("opening_balance") == null ? "0" : data.get("opening_balance").toString();
				String grossAmount = data.get("gross_amount") == null ? "0" : data.get("gross_amount").toString();
				String netAmount = data.get("net_amount") == null ? "0" : data.get("net_amount").toString();
				String discountAmount = data.get("discount_amount") == null ? "0"
						: data.get("discount_amount").toString();
				String taxAmount = data.get("tax_amount") == null ? "0" : data.get("tax_amount").toString();
				String depositedAmount = data.get("deposited_amount") == null ? "0"
						: data.get("deposited_amount").toString();
				String closingBalance = data.get("closing_balance") == null ? "0"
						: data.get("closing_balance").toString();
				String closedDate = data.get("closed_date") == null ? "-" : data.get("closed_date").toString();
				if (closedDate != null) {
					closedDate = AppUtil.changeStringDateFormat("dd-MMM-yyyy", closedDate);
				}
				String depositStatus = data.get("deposit_status") == null ? "Deposited"
						: data.get("deposit_status").toString();
				// String
				// closedDate=data.get("closed_date")!=null?"0":data.get("closed_date").toString();
				flatCardToBankDTO.setId(Long.valueOf(id));
				flatCardToBankDTO.setDispalyClosedDate(closedDate);
				flatCardToBankDTO.setOpeningBalance(Double.valueOf(opbalance));
				flatCardToBankDTO.setGrossAmount(Double.valueOf(grossAmount));
				flatCardToBankDTO.setNetAmount(Double.valueOf(netAmount));
				flatCardToBankDTO.setDiscountAmount(Double.valueOf(discountAmount));
				flatCardToBankDTO.setTaxAmount(Double.valueOf(taxAmount));
				flatCardToBankDTO.setDepositedAmount(Double.valueOf(depositedAmount));
				// flatCardToBankDTO.setDepositStatus(Boolean.valueOf(depositStatus));
				flatCardToBankDTO.setDisplayDepositStatus(depositStatus.toUpperCase());
				flatCardToBankDTO.setClosingBalance(Double.valueOf(closingBalance));
				log.info(" Id : " + id);
				log.info(" opening balance : " + opbalance);
				log.info("  closingBalance: " + closingBalance);
				log.info(" depositStatus : " + depositStatus);
				log.info(" depositedAmount : " + depositedAmount);
				log.info(" taxAmount : " + taxAmount);
				log.info(" discountAmount : " + discountAmount);
				log.info(" netAmount : " + netAmount);
				log.info(" grossAmount : " + grossAmount);
				log.info(" Date : " + flatCardToBankDTO.getDispalyClosedDate());
				resultList.add(flatCardToBankDTO);

			}
			baseDTO.setResponseContents(resultList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			// TODO: handle exception
			log.error("Exception in CardToBankService getCardDetails Method------");
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	/**
	 * 
	 * @param cardToBankId
	 * @return
	 */
	public BaseDTO getCardToBankDetails(Long cardToBankId) {
		log.info("getCardToBankDetails. getCardToBankDetails() - START");
		BaseDTO baseDTO = new BaseDTO();
		ApplicationQuery applicationQuery = null;
		try {
			log.info("getCardToBankDetails. getCardToBankDetails() - cardToBankId: "+cardToBankId);
			if(cardToBankId != null) {
				
				CardToBankDTO cardToBankDTO = new CardToBankDTO();

				final String queryName = "FLAT_CARD_TO_BANK_BANK_DETAILS_SQL";
				applicationQuery = applicationQueryRepository.findByQuery(queryName);
				if (applicationQuery == null) {
					log.info("getCardToBankDetails. getCardToBankDetails() - Query is not found "+queryName);
					baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getErrorCode());
					return baseDTO;
				}

				String queryContent = applicationQuery.getQueryContent() != null ? applicationQuery.getQueryContent().trim() : null;
				queryContent = StringUtils.replace(queryContent, ":cardToBankId", String.valueOf(cardToBankId));
				cardToBankDTO = jdbcTemplate.queryForObject(queryContent, null,
						new BeanPropertyRowMapper<CardToBankDTO>(CardToBankDTO.class));
				if(cardToBankDTO != null) {
					if(cardToBankDTO.getEntityBankBranchId() != null) {
						EntityBankBranch entityBankBranch = entityBankBranchRepository.findOne(cardToBankDTO.getEntityBankBranchId());
						cardToBankDTO.setEntityBankBranch(entityBankBranch);
						if(entityBankBranch != null) {
							cardToBankDTO.setBankAccountType(entityBankBranch.getBankAccountType());
							cardToBankDTO.setBankBranchMaster(entityBankBranch.getBankBranchMaster());
							if(entityBankBranch.getBankBranchMaster() != null) {
								cardToBankDTO.setBankMaster(entityBankBranch.getBankBranchMaster().getBankMaster());
							}
						}
					}
				}
				baseDTO.setResponseContent(cardToBankDTO);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}  else{
				log.error("Flat card to bank id is empty");
			}
		} catch (Exception e) {
			log.error("Exceptin at getCardToBankDetails()", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("getCardToBankDetails. getCardToBankDetails() - END");
		return baseDTO;
	}

}
