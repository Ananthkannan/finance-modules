package in.gov.cooptex.finance.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.dto.ClaimStatementDto;
import in.gov.cooptex.finance.dto.ClaimStatementDtoList;
import in.gov.cooptex.finance.dto.ClaimStatementRequestDto;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class ClaimStatementService {
	
	
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	ApplicationQuery appQuery = null;
	String queryContent = null;

	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public BaseDTO getClaimData(ClaimStatementRequestDto claimStatementRequestDto) {
		
		    log.info("Start ClaimStatementService :" + claimStatementRequestDto);
		    BaseDTO baseDto=new BaseDTO();
		    try
		    {
		    	if(claimStatementRequestDto != null)
		    	{
		    		List<ClaimStatementDtoList> dataList=new ArrayList<ClaimStatementDtoList>();
		    		Validate.notNullOrEmpty(claimStatementRequestDto.getCategoryCodeList(), ErrorDescription.CLAIM_STATEMENT_CATEGORYCODE_EMPTY);
					Validate.notNullOrEmpty(claimStatementRequestDto.getRegionCodeList(), ErrorDescription.CLAIM_STATEMENT_REGIONCODE_EMPTY);
					Validate.notNull(claimStatementRequestDto.getFromDate(),
							ErrorDescription.CLAIMSTATEMENT_FROMDATE_EMPTY);
					Validate.notNull(claimStatementRequestDto.getToDate(),
							ErrorDescription.CLAIMSTATEMENT_TODATE_EMPTY);
					appQuery = applicationQueryRepository.findByQueryName("CLAIM_STATEMENT_LOAD_DETAILS");
					
					if (appQuery != null) {
						queryContent = appQuery.getQueryContent();
						log.info("CLAIM_STATEMENT_DETAILS Query  :: " + queryContent);
						if (queryContent != null && !queryContent.isEmpty()) {

							queryContent = queryContent.replace(":startDate",
									"'" + claimStatementRequestDto.getFromDate().toString() + "'");
							queryContent = queryContent.replace(":endDate",
									"'" + claimStatementRequestDto.getToDate().toString() + "'");

							String categoryTypeList = StringUtils
									.join(claimStatementRequestDto.getCategoryCodeList(), ',');
							queryContent = queryContent.replace(":categoryId", "" + categoryTypeList + "");
							String regionList = StringUtils
									.join(claimStatementRequestDto.getRegionCodeList(), ',');
							queryContent = queryContent.replace(":regionId", "" + regionList + "");

							dataList = jdbcTemplate.query(queryContent, new BeanPropertyRowMapper(ClaimStatementDtoList.class));
							log.info(" Query Data List :: " + dataList);

						}
					}
				    if(dataList != null)
				    {
				    	ClaimStatementDto claimStatementDto=new ClaimStatementDto();
				    	Double totalGrossAmount=dataList.stream().mapToDouble(ClaimStatementDtoList::getGrossSales).sum();
				    	Double totalDiscountAmount=dataList.stream().mapToDouble(ClaimStatementDtoList::getDiscountAmount).sum();
				    	Double totalRebateAmount=dataList.stream().mapToDouble(ClaimStatementDtoList::getRebateAmount).sum();
				    	Double totalbalanceAmount=dataList.stream().mapToDouble(ClaimStatementDtoList::getBalanceAmount).sum();
				    	claimStatementDto.setClaimStatementDtoList(dataList);
				    	claimStatementDto.setTotalRebateAmount(totalRebateAmount);
				    	claimStatementDto.setTotalBalanceAmount(totalbalanceAmount);
				    	claimStatementDto.setTotalDiscountAmount(totalDiscountAmount);
				    	claimStatementDto.setTotalGrossAmount(totalGrossAmount);
				    	baseDto.setResponseContent(claimStatementDto);
				    	baseDto.setStatusCode(0);
				    }
		    	}
		  	
		    }catch (RestException re) {
	            log.error("RestException occured in ClaimStatementService ", re);
	            baseDto.setStatusCode(re.getStatusCode());
	        } catch (Exception e) {
	            log.error("Exception occured in ClaimStatementService ", e);
	            baseDto.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
	        }
		   log.info("End ClaimStatementService baseDTO:" + baseDto);
		return baseDto;
	}

}
