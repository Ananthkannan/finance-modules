package in.gov.cooptex.finance.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.apache.commons.lang.Validate;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.repository.AppConfigRepository;
import in.gov.cooptex.common.service.ApproveRejectCommentsCommonService;
import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.service.NotificationEmailService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.enums.AppConfigEnum;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.enums.PPType;
import in.gov.cooptex.core.enums.SequenceName;
import in.gov.cooptex.core.finance.repository.FinancialYearRepository;
import in.gov.cooptex.core.finance.repository.PPEligibilityRegisterRepository;
import in.gov.cooptex.core.finance.repository.PPSalesRequestLogRepository;
import in.gov.cooptex.core.finance.repository.PPSalesRequestNoteRepository;
import in.gov.cooptex.core.finance.repository.PPTypeMasterRepository;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeePersonalInfoEmployment;
import in.gov.cooptex.core.model.SequenceConfig;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.AppQueryRepository;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.PPSalesRequestRepository;
import in.gov.cooptex.core.repository.SalesInvoiceRepository;
import in.gov.cooptex.core.repository.SequenceConfigRepository;
import in.gov.cooptex.core.repository.SystemNotificationRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.finance.dto.StaffPPPermitInfoDTO;
import in.gov.cooptex.finance.dto.StaffPPPermitSearchDTO;
import in.gov.cooptex.finance.dto.StaffPPSalesRequestDTO;
import in.gov.cooptex.operation.model.PPEligibilityRegister;
import in.gov.cooptex.operation.model.PPSalesRequest;
import in.gov.cooptex.operation.model.PPSalesRequestLog;
import in.gov.cooptex.operation.model.PPSalesRequestNote;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
// @Scope("session")
public class StaffPPPermitRequestService {

	@Autowired
	PPSalesRequestRepository pPSalesRequestRepository;

	@Autowired
	SalesInvoiceRepository salesInvoiceRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	PPEligibilityRegisterRepository pPEligibilityRegisterRepository;

	@Autowired
	FinancialYearRepository financialYearRepository;

	@Autowired
	EmployeeMasterRepository employeeMasterRepository;

	@Autowired
	SequenceConfigRepository sequenceConfigRepository;

	@PersistenceContext
	EntityManager entityManager;

	@Autowired
	PPSalesRequestLogRepository pPSalesRequestLogRepository;

	@Autowired
	PPSalesRequestNoteRepository pPSalesRequestNoteRepository;

	@Autowired
	PPTypeMasterRepository pPTypeMasterRepository;

	@Autowired
	ApproveRejectCommentsCommonService approveRejectCommentsCommonService;

	@Autowired
	SystemNotificationRepository systemNotificationRepository;

	@Autowired
	NotificationEmailService notificationEmailService;

	@Autowired
	AppConfigRepository appConfigRepository;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	private static final String STAFF_PP_REQUEST_VIEW_PAGE = "/pages/accounts/staffPpPermit/viewStaffPpPermitRequest.xhtml?faces-redirect=true";

	/*
	 * @Autowired LoginBean loginBean;
	 */

	@Autowired
	UserMasterRepository userMasterRepository;

	public BaseDTO create(StaffPPSalesRequestDTO dto) {
		log.info("<<============ StaffPPPermitRequestService ---- create  =====##STARTS");
		BaseDTO baseDTO = new BaseDTO();
		try {
			Validate.notNull(dto);

			log.info("<<====  DTO :::" + dto);
			PPSalesRequest request = dto.getPPSalesRequest();

			request.setCreatedDate(new Date());
			request.setEmpMaster(employeeMasterRepository.findOne(request.getEmpMaster().getId()));
			request.setPpTypeMaster(pPTypeMasterRepository.findOne(request.getPpTypeMaster().getId()));
			request.setPpRequestNumber(generateRequestNumber(request.getEmpMaster().getId()));

			request.setRequestedDate(request.getRequestedDate());

			if (PPType.RET_PP_TYPE.equals(request.getPpTypeMaster().getPpCode())) {
				PPEligibilityRegister ppEligibilityRegister = pPEligibilityRegisterRepository
						.getEligibleRegisterId(request.getEmpMaster().getId());
				request.setPpEligibilityRegister(ppEligibilityRegister);
			} else {
				request.setPpEligibilityRegister(request.getPpEligibilityRegister() != null
						? pPEligibilityRegisterRepository.findOne(request.getPpEligibilityRegister().getId())
						: null);
			}

			request.setRequestedAmount(request.getRequestedAmount());
			request.setApprovedAmount(0D);

			request.setValidityDate(request.getValidityDate());
			log.info("<<====  BEFORE SAVE::: ");
			request = pPSalesRequestRepository.save(request);

			PPSalesRequestNote note = new PPSalesRequestNote();
			note.setFinalApproval(dto.getFinalApproval());
			note.setForwardTo(userMasterRepository.findOne(dto.getForWardTo().getId()));
			note.setNote(dto.getNote());
			note.setPPSalesRequest(request);
			pPSalesRequestNoteRepository.save(note);

			PPSalesRequestLog log = new PPSalesRequestLog();
			log.setStage(ApprovalStage.SUBMITTED);
			log.setPPSalesRequest(request);
			pPSalesRequestLogRepository.save(log);

			/*
			 * Send Notification Mail
			 */

			if (request != null && request.getId() != null) {
				Map<String, Object> additionalData = new HashMap<>();
				Long empId = request.getEmpMaster() == null ? null : request.getEmpMaster().getId();
				additionalData.put("Url",
						STAFF_PP_REQUEST_VIEW_PAGE + "&requestId=" + request.getId() + "&empId=" + empId + "&");
				notificationEmailService.sendMailAndNotificationForForward(note, log, additionalData);
			}

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (Exception e) {
			log.error("<<=====  ERROR IN CREATE:::: ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<<============ StaffPPPermitRequestService ---- create  =====##ENDS");
		return baseDTO;
	}

	public BaseDTO getByPPType(Long typeId) {
		log.info("<<============ StaffPPPermitRequestService ---- create  =====##STARTS");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<PPSalesRequest> requestList = pPSalesRequestRepository.findByPpTypeMasterId(typeId);
			baseDTO.setResponseContents(requestList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("<<=====  ERROR IN CREATE:::: ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<<============ StaffPPPermitRequestService ---- create  =====##ENDS");
		return baseDTO;
	}

	public BaseDTO calculateAmountDetails(Long empId, String ppTypeName) {
		log.info("<<============ StaffPPPermitRequestService ---- create  =====##STARTS  " + empId + " PPType : "
				+ ppTypeName);
		BaseDTO baseDTO = new BaseDTO();
		Double amount = 0D;
		try {
			StaffPPPermitInfoDTO dto = new StaffPPPermitInfoDTO();

			if (PPType.RET_PP_TYPE.equals(ppTypeName)) {
				amount = pPEligibilityRegisterRepository.getEligibleAmount(empId);
			} else {
				ApplicationQuery applicationQuery = applicationQueryRepository
						.findByQueryName("GET_EMPLOYEE_GROSS_PAY");
				if (applicationQuery == null || applicationQuery.getId() == null) {
					log.info("Application Query not found for query name : GET_EMPLOYEE_GROSS_PAY");
					baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
					return baseDTO;
				}
				String query = applicationQuery.getQueryContent().trim();
				query = StringUtils.replace(query, ":EMP_ID", String.valueOf(empId));
				log.info("Get Employee Gross Pay Query :" + query);

				amount = jdbcTemplate.queryForObject(query, Double.class);
			}

			amount = amount != null ? amount.doubleValue() : 0D;
			log.info("Employee Gross Pay :" + amount);

			ApplicationQuery appQuery = applicationQueryRepository.findByQueryName("GET_PP_PERMIT_REQUEST_USED_AMOUNT");
			if (appQuery == null || appQuery.getId() == null) {
				log.info("Application Query not found for query name : GET_PP_PERMIT_REQUEST_USED_AMOUNT");
				baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
				return baseDTO;
			}
			String usedAmountQry = appQuery.getQueryContent().trim();
//			log.info("Get PP Permit Request Used Amount Query vvvvvvv :" + usedAmountQry);
			usedAmountQry = StringUtils.replace(usedAmountQry, ":empId", String.valueOf(empId));
			usedAmountQry = StringUtils.replace(usedAmountQry, ":ppType", "'" + ppTypeName + "'");
			log.info("Get PP Permit Request Used Amount Query :" + usedAmountQry);
			Double usedAmount = 0.0;
//			Double approvedAmount = 0.0;
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(usedAmountQry);
			for (Map<String, Object> row : rows) {
				if (row.get("total_sale") != null)
					usedAmount = Double.parseDouble(row.get("total_sale").toString());
				else
					usedAmount = 0.0;
				/*
				 * if (row.get("approved_amount") != null) approvedAmount =
				 * Double.parseDouble(row.get("approved_amount").toString()); else
				 * approvedAmount = 0.0;
				 */
			}

			log.info("Employee Used Amount :" + usedAmount);
//			log.info("Employee Approved Amount :" + approvedAmount);
//			approvedAmount = approvedAmount != null ? approvedAmount.doubleValue() : 0D; 
			usedAmount = usedAmount != null ? usedAmount.doubleValue() : 0D;
//			usedAmount = usedAmount >= approvedAmount ? approvedAmount : usedAmount;

			dto.setEligibleAmount(amount);
			dto.setUsedAmount(usedAmount);

			Double pendingEligibleAmount = amount - usedAmount;
			dto.setPendingEligibleAmount(pendingEligibleAmount < 0D ? 0D : pendingEligibleAmount);
//			dto.setApprovedAmount(approvedAmount);

			ApplicationQuery checkPreviosRequesyUtilizedQuery = applicationQueryRepository
					.findByQueryName("CHECK_PREVIOUS_PP_REQUEST_UTILIZED");
			if (appQuery == null || appQuery.getId() == null) {
				log.info("Application Query not found for query name : CHECK_PREVIOUS_PP_REQUEST_UTILIZED");
				baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
				return baseDTO;
			}
			String countQuery = checkPreviosRequesyUtilizedQuery.getQueryContent().trim();
			countQuery = StringUtils.replace(countQuery, ":empId", String.valueOf(empId));
			countQuery = StringUtils.replace(countQuery, ":ppTypeName", String.valueOf(ppTypeName));

			Integer ppRequestCount = jdbcTemplate.queryForObject(countQuery, Integer.class);

			if (ppRequestCount == null || ppRequestCount == 0) {
				dto.setRequestedAllowed(true);
			} else {
				dto.setRequestedAllowed(false);
			}
			log.info("<<=====  DTO::: " + dto);
			baseDTO.setResponseContent(dto);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (Exception e) {
			log.error("<<=====  ERROR IN CREATE:::: ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<<============ StaffPPPermitRequestService ---- create  =====##ENDS");
		return baseDTO;

	}

	private String generateRequestNumber(Long id) {
		log.info("<<======  StaffPPPermitRequestService ---- generateRequestNumber  =====##STARTS");
		String number = "";
		try {
			SequenceConfig seq = sequenceConfigRepository.findBySequenceName(SequenceName.PP_SALE_REQUEST_NUMBER);
			number = seq.getPrefix() + "" + id + "" + Calendar.getInstance().get(Calendar.MONTH) + 1 + ""
					+ Calendar.getInstance().get(Calendar.YEAR) + "" + String.format("%04d", seq.getCurrentValue() + 1);
			seq.setCurrentValue(seq.getCurrentValue() + 1);
			sequenceConfigRepository.save(seq);
			log.info("<<======  CREATED VALUE::: " + number);
		} catch (Exception e) {
			log.error("<<====  ERROR :: ", e);
		}
		log.info("<<======  StaffPPPermitRequestService ---- generateRequestNumber  =====##ENDS");
		return number;
	}

	@Autowired
	AppQueryRepository appQueryRepository;

	@Autowired
	PPSalesRequestLogRepository ppSalesRequestLogRepository;

	public BaseDTO search(StaffPPPermitSearchDTO request) {
		log.info("<<====   StaffPPPermitRequestService ---  search ====## STARTS");
		BaseDTO baseDTO = null;
		Integer total = 0;
		List<StaffPPPermitSearchDTO> resultList = null;
		try {
			resultList = new ArrayList<StaffPPPermitSearchDTO>();
			baseDTO = new BaseDTO();

			Integer start = request.getPaginationDTO().getFirst() != null ? request.getPaginationDTO().getFirst() : 0,
					pageSize = request.getPaginationDTO().getPageSize();
			start = start * pageSize;

			Long currentUserId = loginService.getCurrentUser().getId();

			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();
			ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("STAFFPP_PERMIT_REQUEST_LIST");
			String mainQuery = applicationQuery.getQueryContent();
			log.info("db query----------" + mainQuery);

			mainQuery = mainQuery.replaceAll(":CURRENTUSERID", currentUserId.toString());
			log.info("after replace query------" + mainQuery);
			if (request != null) {

				if (!StringUtils.isEmpty(request.getPermitType())) {
					mainQuery += " and upper(ptm.pp_name) like upper('%" + request.getPermitType() + "%')";
				}

				if (!StringUtils.isEmpty(request.getStaffName())) {
					mainQuery += " and upper(concat(emp.first_name,' ',emp.last_name)) like upper('%"
							+ request.getStaffName() + "%')";
				}

				if (request.getCreatedDate() != null) {
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					log.info(" CreatedDate Filter : " + request.getCreatedDate());
					mainQuery += "and psr.requested_date::date='" + format.format(request.getCreatedDate()) + "'";
				}

				if (request.getRequestAmount() != null) {
					mainQuery += " and (cast(psr.requested_amount as varchar)) like '%" + request.getRequestAmount()
							+ "%'";
				}

				if (request.getRequestNumber() != null) {
					mainQuery += " and upper(psr.pp_request_number) like upper('%" + request.getRequestNumber() + "%')";
				}

				if (request.getStage() != null && request.getStage().trim().length() > 0) {
					if ("SALES_COMPLETED".equals(request.getStage())) {
						mainQuery += " and si.net_total is not null";
					} else if ("PPSALESREQUEST_HOLD".equals(request.getStage())) {
						mainQuery += " and si.net_total is null and psrl.stage='" + request.getStage() + "'";
					} else {
						mainQuery += " and psrl.stage='" + request.getStage() + "'";
					}
				}
			}
			String countQuery = "select count(*) from (" + mainQuery + ")t";
			log.info("count query... " + countQuery);
			countData = jdbcTemplate.queryForList(countQuery);
			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					total = Integer.parseInt(data.get("count").toString().replace(",", ""));
			}
			log.info("total query... " + total + "::pageSize::>>>" + pageSize + "::start::" + start);

			String sortField = request.getPaginationDTO().getSortField();
			String sortOrder = request.getPaginationDTO().getSortOrder();
			if (sortField != null && sortOrder != null) {
				if (sortField.equals("permitType") && sortOrder.equals("ASCENDING"))
					mainQuery += " order by ptm.pp_name asc ";
				if (sortField.equals("permitType") && sortOrder.equals("DESCENDING"))
					mainQuery += " order by ptm.pp_name desc ";
				if (sortField.equals("staffName") && sortOrder.equals("ASCENDING"))
					mainQuery += " order by emp.first_name asc ";
				if (sortField.equals("staffName") && sortOrder.equals("DESCENDING"))
					mainQuery += " order by emp.first_name desc ";
				if (sortField.equals("createdDate") && sortOrder.equals("ASCENDING"))
					mainQuery += " order by psr.requested_date asc ";
				if (sortField.equals("createdDate") && sortOrder.equals("DESCENDING"))
					mainQuery += " order by psr.requested_date desc ";
				if (sortField.equals("requestAmount") && sortOrder.equals("ASCENDING"))
					mainQuery += " order by psr.requested_amount asc ";
				if (sortField.equals("requestAmount") && sortOrder.equals("DESCENDING"))
					mainQuery += " order by psr.requested_amount desc ";
				if (sortField.equals("requestNumber") && sortOrder.equals("ASCENDING"))
					mainQuery += " order by psr.pp_request_number asc ";
				if (sortField.equals("requestNumber") && sortOrder.equals("DESCENDING"))
					mainQuery += " order by psr.pp_request_number desc ";
				if (sortField.equals("stage") && sortOrder.equals("ASCENDING"))
					mainQuery += " order by psrl.stage asc ";
				if (sortField.equals("stage") && sortOrder.equals("DESCENDING"))
					mainQuery += " order by psrl.stage desc ";

				mainQuery += " limit " + pageSize + " offset " + start + ";";
			} else {
				mainQuery += " order by id desc limit " + pageSize + " offset " + start + ";";
			}

			log.info("filter query----------" + mainQuery);
			listofData = jdbcTemplate.queryForList(mainQuery);

			log.info(" Staff PP Permit Request list size-------------->" + listofData.size());
			for (Map<String, Object> data : listofData) {
				StaffPPPermitSearchDTO response = new StaffPPPermitSearchDTO();
				response.setId(Long.valueOf(data.get("id").toString()));
				response.setPermitType(data.get("pp_name").toString());
				response.setStaffName(data.get("first_name").toString() + " " + data.get("last_name").toString());
				response.setCreatedDate((Date) data.get("requested_date"));
				response.setRequestedAmount(Double.valueOf(data.get("requested_amount").toString()));
				if(data.get("stage")!=null) {
					response.setStage(data.get("stage").toString());	
				}
				
				response.setEmployeeId(Long.valueOf(data.get("empid").toString()));
				response.setRequestNumber(
						data.get("pp_request_number") == null ? "" : String.valueOf(data.get("pp_request_number")));
				resultList.add(response);
			}

			log.info("final list size---------------" + resultList.size());
			log.info("Total records present in getAllStaffPPPermitSearchlazy..." + total);
			baseDTO.setTotalRecords(total);
			baseDTO.setResponseContent(resultList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("<<====   StaffPPPermitService ---  getAll ====## ENDS");
		} catch (Exception ex) {
			log.error("inside lazy method exception------", ex);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO calculateAmountDetails(StaffPPPermitSearchDTO requestDTO) {
		log.info("<<============ StaffPPPermitRequestService ---- create  =====##STARTS  ");
		BaseDTO baseDTO = new BaseDTO();
		Long empId = null;
		Long salesRequestId = null;
		Long notificationId = null;
		Double amount = 0D;
		try {
			empId = requestDTO == null ? null : requestDTO.getEmployeeId();
			salesRequestId = requestDTO == null ? null : requestDTO.getId();
			notificationId = requestDTO == null ? null : requestDTO.getNotificationId();
			StaffPPPermitInfoDTO dto = new StaffPPPermitInfoDTO();

			if (PPType.RET_PP_TYPE.equals(requestDTO.getPermitType())) {
				amount = pPEligibilityRegisterRepository.getEligibleAmount(empId);
			} else {
				ApplicationQuery applicationQuery = applicationQueryRepository
						.findByQueryName("GET_EMPLOYEE_GROSS_PAY");
				if (applicationQuery == null || applicationQuery.getId() == null) {
					log.info("Application Query not found for query name : GET_EMPLOYEE_GROSS_PAY");
					baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
					return baseDTO;
				}
				String query = applicationQuery.getQueryContent().trim();
				query = StringUtils.replace(query, ":EMP_ID", String.valueOf(empId));
				log.info("Get Employee Gross Pay Query :" + query);

				amount = jdbcTemplate.queryForObject(query, Double.class);
			}
			amount = amount == null ? 0D : amount;

			log.info("Employee Gross Pay :" + amount);

			ApplicationQuery appQuery = applicationQueryRepository
					.findByQueryName("GET_PP_PERMIT_PER_REQUEST_USED_AMOUNT");
			if (appQuery == null || appQuery.getId() == null) {
				log.info("Application Query not found for query name : GET_PP_PERMIT_PER_REQUEST_USED_AMOUNT");
				baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
				return baseDTO;
			}

			String usedAmountQry = appQuery.getQueryContent();

			usedAmountQry = StringUtils.replace(usedAmountQry, ":empId", String.valueOf(empId));
			usedAmountQry = StringUtils.replace(usedAmountQry, ":ppType", "'" + requestDTO.getPermitType() + "'");
			usedAmountQry = StringUtils.replace(usedAmountQry, ":requestId", String.valueOf(salesRequestId));

			log.info("Get PP Permit Request Used Amount Query :" + usedAmountQry);

			Double usedAmount = 0.0;
//			Double approvedAmount = 0.0;

			List<Map<String, Object>> dataMapList = jdbcTemplate.queryForList(usedAmountQry);
			int dataMapListSize = dataMapList != null ? dataMapList.size() : 0;
			if (dataMapListSize > 0) {
				for (Map<String, Object> row : dataMapList) {
					if (row.get("total_sale") != null)
						usedAmount = Double.parseDouble(row.get("total_sale").toString());
					else
						usedAmount = 0.0;
//					if (row.get("approved_amount") != null)
//						approvedAmount = Double.parseDouble(row.get("approved_amount").toString());
//					else
//						approvedAmount = 0.0;
				}
			}

			log.info("Employee Used Amount :" + usedAmount);
//			log.info("Employee Approved Amount :" + approvedAmount);

			dto.setEligibleAmount(amount);

//			approvedAmount = approvedAmount != null ? approvedAmount.doubleValue() : 0D; 
			usedAmount = usedAmount != null ? usedAmount.doubleValue() : 0D;
//			usedAmount = usedAmount >= approvedAmount ? approvedAmount : usedAmount;

			dto.setUsedAmount(usedAmount);
			Double pendingAmount = amount - usedAmount;
			dto.setPendingEligibleAmount(pendingAmount < 0D ? 0D : pendingAmount);

			PPSalesRequest ppSalesRequest = pPSalesRequestRepository.findOne(salesRequestId);
			if (ppSalesRequest != null) {
				dto.setApprovedAmount(
						ppSalesRequest.getApprovedAmount() == null ? 0D : ppSalesRequest.getApprovedAmount());
				dto.setStaffName(ppSalesRequest.getEmpMaster() == null ? "NA"
						: ppSalesRequest.getEmpMaster().getFirstName() + " "
								+ ppSalesRequest.getEmpMaster().getLastName());
				dto.setEmpmaster(ppSalesRequest.getEmpMaster() == null ? null : ppSalesRequest.getEmpMaster());
				dto.setRequestedDate(ppSalesRequest.getRequestedDate());
				dto.setPpPermitType(
						ppSalesRequest.getPpTypeMaster() == null ? "NA" : ppSalesRequest.getPpTypeMaster().getPpName());
				dto.setPpTypeMaster(ppSalesRequest.getPpTypeMaster() == null ? null : ppSalesRequest.getPpTypeMaster());
				dto.setRequestedAmount(ppSalesRequest.getRequestedAmount());
				dto.setValidityDate(ppSalesRequest.getValidityDate());
				dto.setRequestNumber(ppSalesRequest.getPpRequestNumber() == null ? "" : ppSalesRequest.getPpRequestNumber());
			}

			Double requestUsedAmount = salesInvoiceRepository.findSalesInvoiceAmountByReqId(salesRequestId);
			requestUsedAmount = requestUsedAmount != null ? requestUsedAmount : 0D;
			Double requestApprovedAmount = ppSalesRequest.getApprovedAmount() != null
					? ppSalesRequest.getApprovedAmount()
					: 0D;
			dto.setRequestUsedAmount(
					requestUsedAmount.doubleValue() <= requestApprovedAmount.doubleValue() ? requestUsedAmount
							: requestApprovedAmount);

			PPSalesRequestLog ppSalesRequestLog = ppSalesRequestLogRepository
					.ppSalesRequestLogBySalesId(salesRequestId);
			PPSalesRequestNote ppSalesRequestNote = pPSalesRequestNoteRepository
					.ppSalesRequestNoteBySalesId(salesRequestId);

			if (ppSalesRequestLog != null) {
				PPSalesRequestLog ppSalesRequestLogtemp = ppSalesRequestLog;
				ppSalesRequestLog = new PPSalesRequestLog();
				ppSalesRequestLog.setId(ppSalesRequestLogtemp.getId());
				ppSalesRequestLogtemp.getPPSalesRequest().setPPSalesRequestLogList(null);
				ppSalesRequestLogtemp.getPPSalesRequest().setCreatedBy(null);
				ppSalesRequestLogtemp.getPPSalesRequest().setEmpMaster(null);
				ppSalesRequestLogtemp.getPPSalesRequest().setModifiedBy(null);
				ppSalesRequestLog.setPPSalesRequest(ppSalesRequestLogtemp.getPPSalesRequest());
				ppSalesRequestLog.setStage(ppSalesRequestLogtemp.getStage());
			}
			if (ppSalesRequestNote != null) {
				PPSalesRequestNote ppSalesRequestNotetemp = ppSalesRequestNote;
				ppSalesRequestNote = new PPSalesRequestNote();
				ppSalesRequestNote.setId(ppSalesRequestNotetemp.getId());
				ppSalesRequestNote.setFinalApproval(ppSalesRequestNotetemp.getFinalApproval());
				ppSalesRequestNote.setForwardTo(ppSalesRequestNotetemp.getForwardTo());
				ppSalesRequestNote.setNote(ppSalesRequestNotetemp.getNote());
			}
			dto.setPpsalesRequestLog(ppSalesRequestLog);
			dto.setPpsalesRequestNote(ppSalesRequestNote);

			if (salesRequestId != null) {
				Object[] approveRejectCommentData = approveRejectCommentsCommonService
						.getApproveRejectComments("pp_sales_request", "pp_sales_log", "pp_sales_id", salesRequestId);
				baseDTO.setCommentAndLogList(approveRejectCommentData);
			}

			if (notificationId != null) {
				String updateSQL = "UPDATE system_notification SET notification_read=true WHERE id=" + notificationId;
				jdbcTemplate.execute(updateSQL);
			}

			log.info("<<=====  DTO::: ");
			baseDTO.setResponseContent(dto);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (Exception e) {
			log.error("<<=====  ERROR IN CREATE:::: ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<<============ StaffPPPermitRequestService ---- create  =====##ENDS");
		return baseDTO;

	}

	// Stage Forward Method

	@Autowired
	LoginService loginService;

	@Transactional
	public BaseDTO ppPermitForwardStage(StaffPPPermitInfoDTO staffPPPermitInfoDTO) {
		log.info("StaffPPPermitRequestService. ppPermitForwardStage() Starts ");
		BaseDTO response = new BaseDTO();
		Long requestId = null;
		try {
			requestId = staffPPPermitInfoDTO.getPpsalesRequestId();
			if (requestId != null) {
				String stage = staffPPPermitInfoDTO.getPpsalesRequestLog().getStage();
				if (stage.equals(ApprovalStage.APPROVED)) {
					in.gov.cooptex.exceptions.Validate.objectNotNull(
							staffPPPermitInfoDTO.getPpsalesRequestNote().getForwardTo(),
							ErrorDescription.RENT_COLLECTION_FORWARD_TO_NULL);
				}
				PPSalesRequest ppsalesRequest = pPSalesRequestRepository
						.findOne(staffPPPermitInfoDTO.getPpsalesRequestId());
				ppsalesRequest.setApprovedAmount(staffPPPermitInfoDTO.getApprovedAmount());
				ppsalesRequest.setValidityDate(staffPPPermitInfoDTO.getValidityDate());
				PPSalesRequestNote ppsalesNote = null;
				if (stage.equals(ApprovalStage.APPROVED)) {
					ppsalesNote = new PPSalesRequestNote();
					ppsalesNote.setNote(staffPPPermitInfoDTO.getPpsalesRequestNote().getNote());
					ppsalesNote.setForwardTo(userMasterRepository
							.findOne(staffPPPermitInfoDTO.getPpsalesRequestNote().getForwardTo().getId()));
					ppsalesNote.setFinalApproval(staffPPPermitInfoDTO.getPpsalesRequestNote().getFinalApproval());
					ppsalesNote.setPPSalesRequest(ppsalesRequest);
					pPSalesRequestNoteRepository.save(ppsalesNote);
				}
				PPSalesRequestLog ppsaleslog = new PPSalesRequestLog();
				ppsaleslog.setStage(staffPPPermitInfoDTO.getPpsalesRequestLog().getStage());
				ppsaleslog.setRemarks(staffPPPermitInfoDTO.getPpsalesRequestLog().getRemarks());
				ppsaleslog.setPPSalesRequest(ppsalesRequest);
				ppSalesRequestLogRepository.save(ppsaleslog);
				pPSalesRequestRepository.save(ppsalesRequest);
				response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());

				Long empId = ppsalesRequest.getEmpMaster() == null ? null : ppsalesRequest.getEmpMaster().getId();
				Long toUserId = null;
				UserMaster fromUser = loginService.getCurrentUser();
				String urlPath = STAFF_PP_REQUEST_VIEW_PAGE + "&requestId=" + ppsalesRequest.getId() + "&empId=" + empId
						+ "&";
				if (stage.equals(ApprovalStage.APPROVED.toString())) {
					toUserId = staffPPPermitInfoDTO.getPpsalesRequestNote().getForwardTo() == null ? null
							: staffPPPermitInfoDTO.getPpsalesRequestNote().getForwardTo().getId();
					notificationEmailService.saveIndividualNotification(fromUser, toUserId, urlPath,
							"PP Permit Request", "PP Permit Request Received");
				} else {
					List<Long> toUserIdList = new ArrayList<>();
					String query = "SELECT um.id as userId FROM pp_sales_note ppn JOIN pp_sales_request ppr ON ppn.pp_sales_id=ppr.id "
							+ "JOIN user_master um ON um.id=ppn.forward_to WHERE ppr.id=:ppSalesId AND ppn.forward_to<>:currentUserId";
					query = query.replace(":ppSalesId", String.valueOf(ppsalesRequest.getId()));
					query = query.replace(":currentUserId", String.valueOf(fromUser.getId()));
					List<Map<String, Object>> mapList = jdbcTemplate.queryForList(query);
					int mapListSize = mapList == null ? 0 : mapList.size();
					if (mapListSize > 0) {
						for (Map<String, Object> dataMap : mapList) {
							toUserIdList.add(dataMap.get("userId") == null ? null
									: Long.valueOf(dataMap.get("userId").toString()));
						}
					}
					Long createdUserId = ppsalesRequest.getCreatedBy() == null ? null
							: ppsalesRequest.getCreatedBy().getId();
					toUserIdList.add(createdUserId);

					int toUserIdListSize = toUserIdList == null ? 0 : toUserIdList.size();
					if (toUserIdListSize > 0) {

						for (Long toUser : toUserIdList) {
							String msg = "";
							if (stage.equals(ApprovalStage.REJECTED.toString())) {
								msg = "PP Permit Request - " + ppsalesRequest.getPpRequestNumber()
										+ " has been rejected";
							} else {
								msg = "PP Permit Request - " + ppsalesRequest.getPpRequestNumber()
										+ " final approval has been completed";
							}
							notificationEmailService.saveIndividualNotification(fromUser, toUser, urlPath,
									"PP Permit Request", msg);
						}
					}
				}
			}
		} catch (RestException re) {
			log.error("RestException occured at StaffPPPermitRequestService. ppPermitForwardStage()", re);
			response.setStatusCode(re.getStatusCode());
		} catch (Exception e) {
			log.error("Exception occured at StaffPPPermitRequestService. ppPermitForwardStage()", e);
		}
		log.info("StaffPPPermitRequestService. ppPermitForwardStage() Ends ");
		return response;
	}

	// Get Active Employee and Name Order by ASC
	public BaseDTO getActiveEmployeeNameOrderList() {
		log.info("StaffPPPermitRequestService.getActiveEmployeeNameOrderList() Started ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<EmployeeMaster> employeeList = employeeMasterRepository.getActiveEmployeeNameOrderList();
			if (employeeList != null && employeeList.size() > 0) {
				baseDTO.setResponseContent(employeeList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				log.info("StaffPPPermitRequestService.getAllEmployee() Completed");
			}
		} catch (Exception exception) {
			log.error("Exception occurred in StaffPPPermitRequestService.getAllEmployee() -:", exception);
			baseDTO.setStatusCode(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		log.info("StaffPPPermitRequestService.getActiveEmployeeNameOrderList() end ");
		return baseDTO;
	}

	/**
	 * @return
	 */
	public BaseDTO getRetirementEmployeeNameOrderList() {
		log.info("StaffPPPermitRequestService.getRetirementEmployeeNameOrderList() Started ");
		BaseDTO baseDTO = new BaseDTO();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		List<EmployeeMaster> employeeList = null;
		try {
			Date currentdate = new Date();
			currentdate = formatter.parse(formatter.format(currentdate));

			String currentDateStr = formatter.format(currentdate);
			//
			// List<EmployeeMaster> employeeList = employeeMasterRepository
			// .getRetirementEmployeeNameOrderList(currentdate);

			String sql = "SELECT em.id,em.emp_code,em.first_name,em.last_name,epie.retirement_date,epie.pf_number FROM emp_master em JOIN emp_personal_info_employment epie ON epie.emp_id=em.id WHERE epie.retirement_date<${currentDate} ORDER BY em.first_name;";
			sql = sql.replace("${currentDate}", AppUtil.getValueWithSingleQuote(currentDateStr));

			List<Map<String, Object>> mapList = jdbcTemplate.queryForList(sql);

			int mapListSize = mapList != null ? mapList.size() : 0;

			if (mapListSize > 0) {
				employeeList = new ArrayList<EmployeeMaster>();
				for (Map<String, Object> map : mapList) {
					Long id = Long.valueOf(String.valueOf(map.get("id")));
					String firstName = String.valueOf(map.get("first_name"));
					String lastName = String.valueOf(map.get("last_name"));
					String pfNumber = String.valueOf(map.get("pf_number"));
					EmployeeMaster employeeMaster = new EmployeeMaster();
					employeeMaster.setId(id);
					employeeMaster.setFirstName(firstName);
					employeeMaster.setLastName(lastName);
					//
					EmployeePersonalInfoEmployment personalInfoEmployment = new EmployeePersonalInfoEmployment();
					personalInfoEmployment.setPfNumber(pfNumber);
					//
					employeeMaster.setPersonalInfoEmployment(personalInfoEmployment);
					//
					employeeList.add(employeeMaster);	
				}
			}

			//employeeList = employeeMasterRepository.getRetiredEmployeeList(currentdate);
			//
			if (employeeList != null && employeeList.size() > 0) {
				baseDTO.setResponseContent(employeeList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				log.info("StaffPPPermitRequestService.getAllEmployee() Completed");
			}
		} catch (Exception exception) {
			log.error("Exception occurred in StaffPPPermitRequestService.getRetirementEmployeeNameOrderList() -:",
					exception);
			baseDTO.setStatusCode(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		log.info("StaffPPPermitRequestService.getRetirementEmployeeNameOrderList() end ");
		return baseDTO;
	}

	public BaseDTO findEmployeeDetailsByLoggedInUserDetails(Long userId) {
		log.info(" findEmployeeDetailsByLoggedInUserDetails with user Id " + userId);
		BaseDTO baseDTO = new BaseDTO();
		try {

			EmployeeMaster empDetails = employeeMasterRepository.findEmployeeNameAndDesignationsByUserId(userId);
			if (empDetails != null) {
				log.info("Employee Details Exist for the User : " + userId);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				log.info("Employee Details Not Found for the User : " + userId);
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			}
			baseDTO.setResponseContent(empDetails);
		} catch (Exception exp) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Employee findEmployeeDetailsByLoggedInUserDetails failed: :", exp);
		}
		return baseDTO;
	}

	public BaseDTO getValueMultiplePPRequestAllowed() {
		BaseDTO baseDTO = new BaseDTO();
		boolean allowed = false;
		try {
			log.info("StaffPPPermitRequestService. getMuliplePPSalesRequestAllowedConfig Starts");
			String appValue = appConfigRepository
					.findValueByKey(AppConfigEnum.MULTIPLE_PP_SALES_REQUEST_ALLOWED.toString());
			log.info("multiple pp sales request appValue :" + appValue);
			String getValue = appValue != null ? appValue : "false";
			if ("false".equalsIgnoreCase(getValue)) {
				allowed = false;
			} else if ("true".equalsIgnoreCase(getValue)) {
				allowed = true;
			}
			baseDTO.setResponseContent(allowed);
		} catch (Exception e) {
			log.error("Exception at getMuliplePPSalesRequestAllowedConfig () ", e);
		}
		return baseDTO;
	}

	public BaseDTO update(StaffPPSalesRequestDTO dto) {
		log.info("<<============ StaffPPPermitRequestService ---- update  =====##STARTS");
		BaseDTO baseDTO = new BaseDTO();
		try {
			Validate.notNull(dto);

			log.info("<<====  DTO :::" + dto);
			PPSalesRequest request = pPSalesRequestRepository
					.findOne(dto.getPPSalesRequest() != null ? dto.getPPSalesRequest().getId() : 0);
			if (request != null) {
				request.setCreatedDate(new Date());
				request.setEmpMaster(employeeMasterRepository.findOne(request.getEmpMaster().getId()));
				request.setPpTypeMaster(pPTypeMasterRepository.findOne(request.getPpTypeMaster().getId()));
//				request.setPpRequestNumber(generateRequestNumber(request.getEmpMaster().getId()));

				request.setRequestedDate(request.getRequestedDate());

				request.setPpEligibilityRegister(request.getPpEligibilityRegister() != null
						? pPEligibilityRegisterRepository.findOne(request.getPpEligibilityRegister().getId())
						: null);
				request.setRequestedAmount(dto.getPPSalesRequest().getRequestedAmount());
				request.setApprovedAmount(0D);

				request.setValidityDate(request.getValidityDate());
				log.info("<<====  BEFORE update::: ");
				request = pPSalesRequestRepository.save(request);

				PPSalesRequestNote note = new PPSalesRequestNote();
				note.setFinalApproval(dto.getFinalApproval());
				note.setForwardTo(userMasterRepository.findOne(dto.getForWardTo().getId()));
				note.setNote(dto.getNote());
				note.setPPSalesRequest(request);
				pPSalesRequestNoteRepository.save(note);

				PPSalesRequestLog log = new PPSalesRequestLog();
				log.setStage(ApprovalStage.SUBMITTED);
				log.setPPSalesRequest(request);
				pPSalesRequestLogRepository.save(log);

				/*
				 * Send Notification Mail
				 */

				if (request != null && request.getId() != null) {
					Map<String, Object> additionalData = new HashMap<>();
					Long empId = request.getEmpMaster() == null ? null : request.getEmpMaster().getId();
					additionalData.put("Url",
							STAFF_PP_REQUEST_VIEW_PAGE + "&requestId=" + request.getId() + "&empId=" + empId + "&");
					notificationEmailService.sendMailAndNotificationForForward(note, log, additionalData);
				}

				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (Exception e) {
			log.error("<<=====  ERROR IN Update:::: ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<<============ StaffPPPermitRequestService ---- update  =====##ENDS");
		return baseDTO;
	}
	
}
