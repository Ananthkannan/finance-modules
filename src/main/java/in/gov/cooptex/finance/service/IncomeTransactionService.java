package in.gov.cooptex.finance.service;


import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import in.gov.cooptex.admin.tapal.model.IncomingTapal;
import in.gov.cooptex.admin.tapal.model.TapalDeliveryType;
import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.core.accounts.model.ExpenseDetails;
import in.gov.cooptex.core.accounts.model.PurchaseSalesAdvance;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.SequenceName;
import in.gov.cooptex.core.model.CustomerMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ExpenseType;
import in.gov.cooptex.core.model.SequenceConfig;
import in.gov.cooptex.core.repository.CustomerMasterRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.GovtSchemePlanRepository;
import in.gov.cooptex.core.repository.SalesInvoiceRepository;
import in.gov.cooptex.core.repository.SequenceConfigRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.advance.repository.PurchaseSalesAdvanceRepository;
import in.gov.cooptex.finance.dto.IncomeTransactionDTO;
import in.gov.cooptex.finance.enums.InvoiceType;
import in.gov.cooptex.helpdesk.model.HelpdeskTicketFaq;
import in.gov.cooptex.operation.model.GovtSchemePlan;
import in.gov.cooptex.operation.model.GovtSchemeType;
import in.gov.cooptex.operation.model.SalesInvoice;
import in.gov.cooptex.operation.repository.ExpenseDetailsRepository;
import in.gov.cooptex.operation.repository.ExpenseTypeRepository;
import in.gov.cooptex.operation.repository.GovtSchemeTypeRepository;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class IncomeTransactionService {
	
	@Autowired
	ExpenseDetailsRepository expenseDetailsRepository;
	
	@Autowired
	GovtSchemePlanRepository govtSchemePlanRepository;
	
	@Autowired
	PurchaseSalesAdvanceRepository purchaseSalesAdvanceRepository;
	
	@Autowired 
	EntityMasterRepository entityMasterRepository;
	
	@Autowired
	SequenceConfigRepository sequenceConfigRepository;
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	SalesInvoiceRepository salesInvoiceRepository;
	
	@Autowired
	ExpenseTypeRepository expenseTypeRepository;
	
	@Autowired
	GovtSchemeTypeRepository govtSchemeTypeRepository;
	
	@Autowired
	CustomerMasterRepository customerMasterRepository;
	
	@Autowired
	EntityManager entityManager;
	
	public BaseDTO getGovtSchemeIncomeDetails(IncomeTransactionDTO incomeTransactionDTO) {
		log.info("<================ IncomeTransactionService:getGovtSchemeIncomeDetails =================>");
		BaseDTO baseDTO = new BaseDTO();
		Double expenseAmount=0.0;Double advanceCollectedAmount=0.0;
		try {
			Long planId=incomeTransactionDTO.getGovtSchemePlanId();
			Long expenseTypeId=incomeTransactionDTO.getExpenseType().getId();
			Validate.objectNotNull(planId, ErrorDescription.GOVT_SCHEME_PLAN_EMPTY);
			Validate.objectNotNull(expenseTypeId, ErrorDescription.SERVICE_INVOICE_EXPENSE_TYPE_EMPTY);
			if(planId!=null) {
				expenseAmount=expenseDetailsRepository.getExpenseAmountByGovtSchemePlanId(planId,expenseTypeId);
				log.info("expenseAmount================>"+expenseAmount);
				GovtSchemePlan govtSchemePlan=govtSchemePlanRepository.findOne(planId);
				if(govtSchemePlan!=null) {
					incomeTransactionDTO.setExpenseFromDate(govtSchemePlan.getPeriodFrom());
					incomeTransactionDTO.setExpenseToDate(govtSchemePlan.getPeriodTo());
				}
				advanceCollectedAmount=purchaseSalesAdvanceRepository.getAdvanceAmountBySchemePlanId(planId);
				log.info("advanceCollectedAmount================>"+advanceCollectedAmount);
				if(expenseAmount!=null) {
					incomeTransactionDTO.setExpenseAmount(expenseAmount);
				}else{
					incomeTransactionDTO.setExpenseAmount(0d);
					incomeTransactionDTO.setExpenseFromDate(null);
					incomeTransactionDTO.setExpenseToDate(null);
				}
				if(advanceCollectedAmount!=null) {
					incomeTransactionDTO.setTotalAdvanceCollectedAmount(advanceCollectedAmount);
				}else {
					incomeTransactionDTO.setTotalAdvanceCollectedAmount(0d);
				}
			}
			baseDTO.setResponseContent(incomeTransactionDTO);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		}catch(Exception exp){
			log.error("Exception Cause  : " , exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO ;
	}
	
	public BaseDTO create(SalesInvoice salesInvoice) {
		log.info("<================ IncomeTransactionService:create=================>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			Validate.objectNotNull(salesInvoice.getCustomerMaster(), ErrorDescription.SALES_INVOICE_CUSTOMER_NULL);
			Validate.objectNotNull(salesInvoice.getGovtSchemePlan(), ErrorDescription.GOVT_SCHEME_PLAN_EMPTY);
			EntityMaster entityMaster = entityMasterRepository.findByUserRegion(loginService.getCurrentUser().getId());
			if(salesInvoice.getId()==null) {
				SequenceConfig sequenceConfig = sequenceConfigRepository.findBySequenceName(SequenceName.valueOf(SequenceName.INCOME_TRANSACTION_REF_NUMBER.name()));
				if (sequenceConfig == null) {
					throw new RestException(ErrorDescription.SEQUENCE_CONFIG_NOT_EXIST);
				}
				String invoiceNumberPrefix=sequenceConfig.getPrefix() + AppUtil.getCurrentMonthString()+
						AppUtil.getCurrentYearString();
				Long invoicenumber=sequenceConfig.getCurrentValue();
				sequenceConfig.setCurrentValue(sequenceConfig.getCurrentValue() + 1);
				sequenceConfigRepository.save(sequenceConfig);
				salesInvoice.setInvoiceNumberPrefix(invoiceNumberPrefix);
				salesInvoice.setInvoiceNumber(invoicenumber);
				salesInvoice.setInvoiceType(InvoiceType.SERVICE.toString());
				salesInvoice.setEntityMaster(entityMaster);
				salesInvoice.setInvoiceDate(new Date());
				salesInvoiceRepository.save(salesInvoice);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}else {
				log.info("::Income Update Condition Started::salesInoviceID::>"+salesInvoice.getId());
				SalesInvoice salesInvoiceObj=salesInvoiceRepository.getById(salesInvoice.getId());
				log.info("salesInvoice obj:: "+salesInvoiceObj!=null?"is not null":"is null");
				salesInvoiceObj.setNetTotal(salesInvoice.getNetTotal());
				salesInvoiceObj.setGovtSchemePlan(salesInvoice.getGovtSchemePlan());
				salesInvoiceObj.setCustomerMaster(salesInvoice.getCustomerMaster());
				salesInvoiceObj.setEntityMaster(entityMaster);
				salesInvoiceRepository.save(salesInvoiceObj);
				log.info(":: Income Updated Successfully :::");
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		}catch(Exception exp){
			log.error("Exception Cause  : " , exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO ;
	}
	
	public BaseDTO getServiceInvoiceList(PaginationDTO paginationDTO) {
		log.info("IncomeTransactionService getServiceInvoiceList method started");
		BaseDTO baseDTO = new BaseDTO();
		try {
			Session session = entityManager.unwrap(Session.class);
			Criteria criteria = session.createCriteria(SalesInvoice.class, "salesInvoice");
			criteria.createAlias("salesInvoice.customerMaster", "customerMaster",JoinType.LEFT_OUTER_JOIN);
			criteria.createAlias("salesInvoice.govtSchemePlan", "govtSchemePlan",JoinType.LEFT_OUTER_JOIN);
			criteria.add(Restrictions.eq("salesInvoice.invoiceType", InvoiceType.SERVICE.toString()));
			if(paginationDTO.getFilters()!=null) {
				if (paginationDTO.getFilters().get("invoiceNumberPrefix")!=null) {
					Criterion firstCriteria = Restrictions.like("salesInvoice.invoiceNumberPrefix", "%"+paginationDTO.getFilters().get("invoiceNumberPrefix")+'%').ignoreCase();
					Criterion secondCriteria = Restrictions.sqlRestriction("cast(this_.invoice_number as varchar) like '%"+ paginationDTO.getFilters().get("invoiceNumberPrefix")+"%'");
					criteria.add(Restrictions.or(firstCriteria, secondCriteria));
				}
				if(paginationDTO.getFilters().get("invoiceDate")!=null) {
					Date invoiceDate = new Date((long) paginationDTO.getFilters().get("invoiceDate"));
					log.info(" invoiceDate Filter : " + invoiceDate);
					criteria.add(Restrictions.eq("salesInvoice.invoiceDate", new java.sql.Date(invoiceDate.getTime())));
				}
				if(paginationDTO.getFilters().get("netTotal")!=null) {
					criteria.add(Restrictions.sqlRestriction("cast(this_.net_total as varchar) like '%"+ paginationDTO.getFilters().get("netTotal")+"%'"));
				}
				if(paginationDTO.getFilters().get("customerCodeorName")!=null) {
					Criterion firstCriteria = Restrictions.like("customerMaster.name", "%"+paginationDTO.getFilters().get("customerCodeorName")+'%').ignoreCase();
					Criterion secondCriteria = Restrictions.like("customerMaster.code", "%"+paginationDTO.getFilters().get("customerCodeorName")+'%').ignoreCase();
					criteria.add(Restrictions.or(firstCriteria, secondCriteria));
				}
				if(paginationDTO.getFilters().get("govtPlanCodeorName")!=null) {
					Criterion firstCriteria = Restrictions.like("govtSchemePlan.planName", "%"+paginationDTO.getFilters().get("govtPlanCodeorName")+'%').ignoreCase();
					Criterion secondCriteria = Restrictions.like("govtSchemePlan.refNumber", "%"+paginationDTO.getFilters().get("govtPlanCodeorName")+'%').ignoreCase();
					criteria.add(Restrictions.or(firstCriteria, secondCriteria));
				}
				criteria.setProjection(Projections.rowCount());
				Integer totalResult = ((Long) criteria.uniqueResult()).intValue();
				criteria.setProjection(null);
				ProjectionList projectionList = Projections.projectionList();
				projectionList.add(Projections.property("salesInvoice.id"));
				
				projectionList.add(Projections.property("salesInvoice.invoiceNumberPrefix"));
				projectionList.add(Projections.property("salesInvoice.invoiceDate"));
				projectionList.add(Projections.property("salesInvoice.netTotal"));
				projectionList.add(Projections.property("customerMaster.name"));
				projectionList.add(Projections.property("customerMaster.code"));
				projectionList.add(Projections.property("govtSchemePlan.planName"));
				projectionList.add(Projections.property("govtSchemePlan.refNumber"));
				projectionList.add(Projections.property("salesInvoice.invoiceNumber"));
				criteria.setProjection(projectionList);
				if (paginationDTO != null) {
					Integer pageNo = paginationDTO.getFirst();
					Integer pageSize = paginationDTO.getPageSize();

					if (pageNo != null && pageSize != null) {
						criteria.setFirstResult(pageNo * pageSize);
						criteria.setMaxResults(pageSize);
						log.info("PageNo : [" + pageNo + "] pageSize[" + pageSize + "]");
					}
					String sortField = paginationDTO.getSortField();
					String sortOrder = paginationDTO.getSortOrder();
					if (sortField != null && sortOrder != null) {
						log.info("sortField : [" + sortField + "] sortOrder[" + sortOrder + "]");
						
						if(sortField.equals("invoiceNumberPrefix")) {
							sortField = "salesInvoice.invoiceNumberPrefix";
						}else if (sortField.equals("invoiceDate")) {
							sortField = "salesInvoice.invoiceDate";
						}else if(sortField.equals("netTotal")){
							sortField = "salesInvoice.netTotal";
						}else if(sortField.equals("customerCodeorName")){
							sortField = "customerMaster.name";
						}else if(sortField.equals("govtPlanCodeorName")){
							sortField = "govtSchemePlan.planName";
						}
						if (sortOrder.equals("DESCENDING")) {
							criteria.addOrder(Order.desc(sortField));
						} else {
							criteria.addOrder(Order.asc(sortField));
						}
					}else {
						criteria.addOrder(Order.desc("salesInvoice.id"));
					}
				}
				 
				log.info("Criteria Query  : ");
				
				List<?> resultList  = criteria.list();
				
				List<SalesInvoice> salesInvoiceList = new ArrayList<SalesInvoice>();

				Iterator<?> it = resultList.iterator();
				while(it.hasNext()) {
					Object obj[] = (Object[])it.next();
					SalesInvoice salesInvoice=new SalesInvoice();
					salesInvoice.setId((Long)obj[0]);
					salesInvoice.setInvoiceNumberPrefix((String)obj[1]);
					salesInvoice.setInvoiceNumber((Long)obj[8]);
					salesInvoice.setInvoiceDate((Date)obj[2]);
					salesInvoice.setNetTotal((Double)obj[3]);
					CustomerMaster customerMaster=new CustomerMaster();
					customerMaster.setName((String)obj[4]);
					customerMaster.setCode((String)obj[5]);
					salesInvoice.setCustomerMaster(customerMaster);
					GovtSchemePlan govtSchemePlan=new GovtSchemePlan();
					govtSchemePlan.setPlanName((String)obj[6]);
					govtSchemePlan.setRefNumber((String)obj[7]);
					salesInvoice.setGovtSchemePlan(govtSchemePlan);
					salesInvoiceList.add(salesInvoice);
				}
			    baseDTO.setResponseContent(salesInvoiceList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				baseDTO.setTotalRecords(totalResult);
			}
		} catch (RestException restException) {
			log.error("IncomeTransactionService getServiceInvoiceList RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("IncomeTransactionService getServiceInvoiceList Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("IncomeTransactionService getServiceInvoiceList method completed");
		return baseDTO;
	}
	
	public BaseDTO getById(Long id) {
		log.info("IncomeTransactionService getById method started [" + id + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			IncomeTransactionDTO incomeTransactionDTO=new IncomeTransactionDTO();
			Validate.objectNotNull(id, ErrorDescription.SERVICE_INVOICE_EMPTY);
			if(id!=null) {
				SalesInvoice salesInvoice = salesInvoiceRepository.getById(id);
				incomeTransactionDTO.setExpenseBalanceCollectedAmount(salesInvoice.getNetTotal());
				if(salesInvoice.getGovtSchemePlan().getId()!=null) {
					GovtSchemePlan govtSchemePlan=govtSchemePlanRepository.
							findOne(salesInvoice.getGovtSchemePlan().getId());
					incomeTransactionDTO.setGovtSchemePlan(govtSchemePlan);
					incomeTransactionDTO.setGovtSchemePlanId(govtSchemePlan.getId());
					incomeTransactionDTO.setGovtSchemeType(govtSchemePlan.getSchemeType());
					log.info("govtSchemePlan->SchemeType->Code"+govtSchemePlan.getSchemeType().getCode());
					ExpenseType expenseType=expenseTypeRepository.getByCode(govtSchemePlan.getSchemeType().getCode());
					incomeTransactionDTO.setExpenseType(expenseType);
					getGovtSchemeIncomeDetails(incomeTransactionDTO);
				}
				if(salesInvoice.getCustomerMaster().getId()!=null) {
					incomeTransactionDTO.setCustomerMaster(salesInvoice.getCustomerMaster());
					incomeTransactionDTO.setCustomerTypeMaster(salesInvoice.getCustomerMaster().getCustomerTypeMaster());
				}
			}
			baseDTO.setResponseContent(incomeTransactionDTO);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("IncomeTransactionService getById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("IncomeTransactionService getById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("IncomeTransactionService getById method completed");
		return baseDTO;
	}
	
	public BaseDTO getAllCustomersByTypeId(String customerName,Long typeId) {
		log.info("<---Starts CustomerMasterService .getAllCustomersByName--->");
		BaseDTO baseDTO = new BaseDTO();
		int customerListSize = 0;
		try {
			List<CustomerMaster> customerList = customerMasterRepository.findAllCustomerByTypeId(customerName,typeId);
			customerListSize = customerList == null ? 0 : customerList.size();
			log.info("<---getAllCustomersByType() - customerListSize ---> " + customerListSize);

			baseDTO.setResponseContent(customerList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException exception) {

			baseDTO.setStatusCode(exception.getStatusCode());

			log.error("CustomerMasterService.getAllCustomersByName method RestException ", exception);

		} catch (DataIntegrityViolationException exception) {

			log.error("Data Integrity Violation Exception in CustomerMasterService.getAllCustomersByName method",
					exception);

			String exceptionCause = ExceptionUtils.getRootCauseMessage(exception);
			log.error("Exception Cause 1 : " + exceptionCause);

			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());

		} catch (Exception exception) {

			log.error("Error in CustomerMasterService.getAllCustomersByName method ", exception);

			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}

		return baseDTO;
	}
	
}
