package in.gov.cooptex.finance.service;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.repository.AppQueryRepository;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.dto.InsuranceReportDTO;
import in.gov.cooptex.finance.dto.ProfitLossReportDTO;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class InsuranceReportService {

	@PersistenceContext
	EntityManager entityManager;
	
	@Autowired 
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	AppQueryRepository appQueryRepository;

	public BaseDTO gendrateInsuranceReport(Long regId) {
		log.info("InsuranceReportService gendrateInsuranceReport method started : ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			Validate.notNull(regId, ErrorDescription.REGIONAL_NAME_REQ);
			ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("INSURANCE_REPORT_QUERY");
			if (applicationQuery == null || applicationQuery.getId() == null
					|| applicationQuery.getQueryContent() == null || applicationQuery.getQueryContent().isEmpty()) {
				throw new RestException(ErrorDescription.QUERY_NOT_FOUND);
			}
			String query = applicationQuery.getQueryContent().trim();
			query = query.replace(":regId", regId.toString());
			log.info("gendrateInsuranceReport query==>" + query);
		    baseDTO.setResponseContent(jdbcTemplate.query(query, new RowMapper<InsuranceReportDTO>() {

				@Override
				public InsuranceReportDTO mapRow(ResultSet rs, int no) throws SQLException {
					InsuranceReportDTO profitLossReportDTO = new InsuranceReportDTO();
					profitLossReportDTO.setRegOfficeId(rs.getLong("entityId"));
					profitLossReportDTO.setRegOfficeName(rs.getString("name"));
					profitLossReportDTO.setRegOfficeAddressOne(rs.getString("line_one"));
					profitLossReportDTO.setRegOfficeAddressTwo(rs.getString("line_two"));
					profitLossReportDTO.setRegOfficeAddressThree(rs.getString("line_three"));
					profitLossReportDTO.setRegAreaName(rs.getString("areaName"));
					profitLossReportDTO.setRegCity(rs.getString("cityName"));
					profitLossReportDTO.setRegTaluk(rs.getString("talukName"));
					profitLossReportDTO.setRegDistrict(rs.getString("districtName"));
					profitLossReportDTO.setRegPincode(rs.getString("postal_code"));
					return profitLossReportDTO;
				}
			}));
			log.info("<--- Successfully fetched the records from DB ---> ");
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			
		}catch (RestException e) {
			log.info("<--- Exception in gendrateInsuranceReport() ---> ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}catch (Exception e) {
			log.info("<--- Exception in gendrateInsuranceReport() ---> ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return baseDTO;
	}
	
	
}
