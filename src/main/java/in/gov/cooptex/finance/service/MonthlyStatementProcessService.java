package in.gov.cooptex.finance.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import commonDataService.AppConfigKey;
import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.EntityMonthScheduleDTO;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.service.ScheduleService;
import in.gov.cooptex.core.util.JdbcUtil;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.MonthlyStatementKey;
import in.gov.cooptex.exceptions.RestException;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class MonthlyStatementProcessService {

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	LoginService loginService;

	@Autowired
	ScheduleService scheduleService;

	private static final Integer SYSTEM_STARTED_YEAR = 2019;

	private static final Integer SYSTEM_STARTED_MONTH = 4;

	private static final String INITIATED = "INITIATED";

	private static final String IN_PROGRESS = "IN_PROGRESS";

	private static final String COMPLETED = "COMPLETED";

	private static final Double SIXTY_MINUTES = 60D;

	private static final Double FIFTEEN_MINUTES = 15D;

	private static final String COMM_ERROR_MESSAGE = "Analysis not started. Please contact support";

	private static final String SELECT_NOR_ACCOUNT_ANALISYS_SCHEDULE_STATUS = "SELECT schedule_status, xl_file_path, pdf_file_path, DATE_PART('hour', NOW() - created_date) * 60 + DATE_PART('minute', NOW() - created_date) AS time_difference_in_minutes FROM entity_month_schedule WHERE entity_id=? AND year_number=? AND month_number=? AND schedule_name=?";

	private static final String SELECT_ENTITY_TYPE_CODE = "SELECT etm.code AS entity_type_code FROM entity_master em JOIN entity_type_master etm ON etm.id=em.entity_type_id WHERE em.id=?;";

	private static final String SELECT_NOR_LOCK_FOR_REGION = "SELECT count(*) as count FROM nor_lock WHERE region_id=?";

	private static final String SELECT_NOR_LOCK_FOR_REGION_MONTH_YEAR = "SELECT count(*) as count FROM nor_lock WHERE region_id=? AND month_number=? AND year_number=?";

	private static final String ALERT_MESSAGE = "NOR not completed for the month of ${defaultMonth} ${defaultYear}. Please complete before running for the month of ${monthNumber} ${yearNumber}.";

	/**
	 * @param entityMonthScheduleDTO
	 * @return
	 */
	public BaseDTO runMonthlyStatement(EntityMonthScheduleDTO entityMonthScheduleDTO) {
		log.info("----runMonthlyStatement() service method starts------");
		UserMaster userMaster = null;
		Long loggedInUserId = null;
		log.info("login userId ::::::" + loginService.getCurrentUser().getId());
		BaseDTO baseDTO = null;
		String errorMessage = null;
		try {
			userMaster = loginService.getCurrentUser();
			loggedInUserId = userMaster != null ? userMaster.getId() : null;
			baseDTO = validateAndTriggerMonthlyStatementSchedule(entityMonthScheduleDTO, loggedInUserId);
		} catch (RestException ex) {
			errorMessage = ex.getMessage();
		} catch (Exception ex) {
			errorMessage = "Unknown error. " + COMM_ERROR_MESSAGE;
			log.error("Exception at runMonthlyStatement()", ex);
		}

		log.info("errorMessage: " + errorMessage);

		if (errorMessage != null && !errorMessage.isEmpty()) {
			baseDTO = new BaseDTO();
			baseDTO.setStatusCode(1);
			baseDTO.setErrorDescription(errorMessage);
		}

		return baseDTO;
	}

	/**
	 * @param entityMonthScheduleDTO
	 * @return
	 */
	public BaseDTO lockNOR(EntityMonthScheduleDTO entityMonthScheduleDTO) {
		log.info("MonthlyStatementProcessService. lockNOR() - START");
		UserMaster userMaster = null;
		Long loggedInUserId = null;
		BaseDTO baseDTO = null;
		String errorMessage = null;
		try {
			userMaster = loginService.getCurrentUser();
			loggedInUserId = userMaster != null ? userMaster.getId() : null;
			log.info("login userId ::::::" + loggedInUserId);

			/**
			 * SET CREATED BY USER ID
			 */
			entityMonthScheduleDTO.setCreatedBy(loggedInUserId);

			/**
			 * SET CREATED DATE TIME
			 */
			Timestamp currentDateTime = new Timestamp(new java.util.Date().getTime());
			entityMonthScheduleDTO.setCreatedDate(currentDateTime);

			Long pkId = scheduleService.saveNORLock(entityMonthScheduleDTO);
			log.info("Nor Lock pkId: " + pkId);
			baseDTO = new BaseDTO();
			baseDTO.setStatusCode(0);
		} catch (RestException ex) {
			errorMessage = ex.getMessage();
		} catch (Exception ex) {
			errorMessage = "Unknown error. " + COMM_ERROR_MESSAGE;
			log.error("Exception at lockNOR()", ex);
		}

		log.info("errorMessage: " + errorMessage);

		if (errorMessage != null && !errorMessage.isEmpty()) {
			baseDTO = new BaseDTO();
			baseDTO.setStatusCode(1);
			baseDTO.setErrorDescription(errorMessage);
		}

		return baseDTO;
	}

	/**
	 * 
	 * @param entityMonthScheduleDTO
	 * @return
	 */
	public BaseDTO checkLockNOR(EntityMonthScheduleDTO entityMonthScheduleDTO) {
		log.info("MonthlyStatementProcessService. checkLockNOR() - START");
		BaseDTO baseDTO = null;
		String errorMessage = null;
		try {
			Integer regionId = entityMonthScheduleDTO.getEntityId();
			Integer monthNumber = entityMonthScheduleDTO.getMonthNumber();
			Integer yearNumber = entityMonthScheduleDTO.getYearNumber();

			log.info("MonthlyStatementProcessService. checkLockNOR() - regionId: " + regionId);
			log.info("MonthlyStatementProcessService. checkLockNOR() - monthNumber: " + monthNumber);
			log.info("MonthlyStatementProcessService. checkLockNOR() - yearNumber: " + yearNumber);

			boolean islocked = scheduleService.isLockNor(regionId, monthNumber, yearNumber);
			log.info("MonthlyStatementProcessService. checkLockNOR() - islocked: " + islocked);

			if (islocked) {
				throw new RestException("NOR is already locked for this month and year.");
			}

			Integer defaultStartMonthInt;
			Integer defaultStartYearInt;

			String defaultNorStartMonth = JdbcUtil.getAppConfigValue(jdbcTemplate,
					AppConfigKey.NOR_DEFAULT_START_MONTH);
			String defaultNorStartYear = JdbcUtil.getAppConfigValue(jdbcTemplate, AppConfigKey.NOR_DEFAULT_START_YEAR);

			log.info("defaultNorStartMonth: " + defaultNorStartMonth);
			log.info("defaultNorStartYear: " + defaultNorStartYear);

			if (StringUtils.isEmpty(defaultNorStartMonth)) {
				defaultStartMonthInt = SYSTEM_STARTED_MONTH;
			} else {
				defaultStartMonthInt = Integer.valueOf(defaultNorStartMonth);
			}

			if (StringUtils.isEmpty(defaultNorStartYear)) {
				defaultStartYearInt = SYSTEM_STARTED_YEAR;
			} else {
				defaultStartYearInt = Integer.valueOf(defaultNorStartYear);
			}

			if (monthNumber.intValue() == defaultStartMonthInt.intValue()
					&& yearNumber.intValue() == defaultStartYearInt.intValue()) {
				log.info("Month and year number and default month and year number is equal.");
			} else {
				Integer previousMonthNumber;
				Integer previousYearNumber;

				previousMonthNumber = monthNumber == 1 ? 12 : monthNumber - 1;
				previousYearNumber = monthNumber == 1 ? yearNumber - 1 : yearNumber;

				log.info("previousMonthNumber: " + previousMonthNumber);
				log.info("previousYearNumber: " + previousYearNumber);

				boolean isLockedPreviousMonth = scheduleService.isLockNor(regionId, previousMonthNumber,
						previousYearNumber);
				log.info("MonthlyStatementProcessService. checkLockNOR() - isLockedPreviousMonth: "
						+ isLockedPreviousMonth);

				if (!isLockedPreviousMonth) {
					String errorMsg = StringUtils.replace(ALERT_MESSAGE, "${defaultMonth}",
							AppUtil.getMonthName(previousMonthNumber));
					errorMsg = StringUtils.replace(errorMsg, "${defaultYear}", String.valueOf(previousYearNumber));
					errorMsg = StringUtils.replace(errorMsg, "${monthNumber}", AppUtil.getMonthName(monthNumber));
					errorMsg = StringUtils.replace(errorMsg, "${yearNumber}", String.valueOf(yearNumber));
					throw new RestException(errorMsg);
				}
			}

			baseDTO = new BaseDTO();
			baseDTO.setStatusCode(0);
		} catch (RestException ex) {
			errorMessage = ex.getMessage();
		} catch (Exception ex) {
			errorMessage = "Unknown error. " + COMM_ERROR_MESSAGE;
			log.error("Exception at checkLockNOR()", ex);
		}

		log.info("errorMessage: " + errorMessage);

		if (errorMessage != null && !errorMessage.isEmpty()) {
			baseDTO = new BaseDTO();
			baseDTO.setStatusCode(1);
			baseDTO.setErrorDescription(errorMessage);
		}

		return baseDTO;
	}

	/**
	 * @param entityMonthScheduleDTO
	 * @param loggedInUserId
	 * @return
	 * @throws Exception
	 */
	private BaseDTO validateAndTriggerMonthlyStatementSchedule(EntityMonthScheduleDTO entityMonthScheduleDTO,
			Long loggedInUserId) throws Exception {
		log.info("Inside MonthlyStatementProcessService - validateAndTriggerMonthlyStatementSchedule");
		BaseDTO baseDTO = new BaseDTO();
		Integer entityId = null;
		Integer yearNumber = null;
		Integer monthNumber = null;
		String scheduleName = null;
		Integer toYearNumber = null;
		Integer toMonthNumber = null;
		boolean checkToMonthYearValidation = false;

		final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		if (entityMonthScheduleDTO == null) {
			throw new RestException("Invalid Request");
		}
		entityId = entityMonthScheduleDTO.getEntityId();
		yearNumber = entityMonthScheduleDTO.getYearNumber();
		monthNumber = entityMonthScheduleDTO.getMonthNumber();
		scheduleName = entityMonthScheduleDTO.getScheduleName();
		toYearNumber = entityMonthScheduleDTO.getToYearNumber();
		toMonthNumber = entityMonthScheduleDTO.getToMonthNumber();

		if (MonthlyStatementKey.BALANCE_SHEET.equals(scheduleName)
				|| MonthlyStatementKey.RECEIPT_AND_CHARGES.equals(scheduleName)
				|| MonthlyStatementKey.PROFIT_AND_LOSS.equals(scheduleName)) {
			checkToMonthYearValidation = true;
		}

		log.info("MonthlyStatementProcessService - validateAndTriggerMonthlyStatementSchedule - entityId: " + entityId);
		log.info("MonthlyStatementProcessService - validateAndTriggerMonthlyStatementSchedule - yearNumber: "
				+ yearNumber);
		log.info("MonthlyStatementProcessService - validateAndTriggerMonthlyStatementSchedule - monthNumber: "
				+ monthNumber);

		if (entityId == null) {
			throw new RestException("Entity id is empty.");
		} else if (yearNumber == null) {
			throw new RestException("Year is empty.");
		} else if (monthNumber == null) {
			throw new RestException("Month is empty.");
		} else if (StringUtils.isEmpty(scheduleName)) {
			throw new RestException("ScheduleName is empty.");
		}

		if (checkToMonthYearValidation) {
			if (toYearNumber == null) {
				throw new RestException("To Year is empty.");
			} else if (toMonthNumber == null) {
				throw new RestException("To Month is empty.");
			}
		}

		Integer currentYear = AppUtil.stringToInteger(AppUtil.getCurrentYearString("yyyy"), null);

		Integer currentMonthNumber = AppUtil.stringToInteger(AppUtil.getCurrentMonthNumber(), null);

		if (yearNumber > currentYear) {
			throw new RestException("Future year cannot be selected.");
		} else if (yearNumber < SYSTEM_STARTED_YEAR) {
			throw new RestException("Year before " + SYSTEM_STARTED_YEAR + " cannot be selected.");
		} else if (yearNumber == currentYear && monthNumber > currentMonthNumber) {
			throw new RestException("Future month cannot be selected.");
		} else if (yearNumber == SYSTEM_STARTED_YEAR && monthNumber < SYSTEM_STARTED_MONTH) {
			throw new RestException("Month before" + SYSTEM_STARTED_MONTH + " cannot be selected.");
		}

		String fromDateStr = null;
		String toDateStr = null;
		if (checkToMonthYearValidation) {
			if (toYearNumber > currentYear) {
				throw new RestException("Future year cannot be selected.");
			} else if (toYearNumber < SYSTEM_STARTED_YEAR) {
				throw new RestException("Year before " + SYSTEM_STARTED_YEAR + " cannot be selected.");
			} else if (toYearNumber == currentYear && toMonthNumber > currentMonthNumber) {
				throw new RestException("Future month cannot be selected.");
			} else if (toYearNumber == SYSTEM_STARTED_YEAR && toMonthNumber < SYSTEM_STARTED_MONTH) {
				throw new RestException("Month before" + SYSTEM_STARTED_MONTH + " cannot be selected.");
			} else if (yearNumber == toYearNumber && toMonthNumber < monthNumber) {
				throw new RestException("To month should be greater than from month.");
			}

			/**
			 * From Date / To Date
			 */
			String monthNumStr = monthNumber < 10 ? "0" + monthNumber : String.valueOf(monthNumber);
			fromDateStr = yearNumber + "-" + monthNumStr + "-01";
			String toMonthNumStr = toMonthNumber < 10 ? "0" + toMonthNumber : String.valueOf(toMonthNumber);
			String toDateFirstDayStr = toYearNumber + "-" + toMonthNumStr + "-01";
			toDateStr = AppUtil.getLastDateOfMonth("yyyy-MM-dd", toDateFirstDayStr);

		} else {
			String monthNumStr = monthNumber < 10 ? "0" + monthNumber : String.valueOf(monthNumber);
			fromDateStr = yearNumber + "-" + monthNumStr + "-01";
			toDateStr = AppUtil.getLastDateOfMonth("yyyy-MM-dd", fromDateStr);
		}

		entityMonthScheduleDTO.setScheduleFromDate(simpleDateFormat.parse(fromDateStr));
		entityMonthScheduleDTO.setScheduleToDate(simpleDateFormat.parse(toDateStr));

		String entityTypeCode = null;

		Object entityTypeCodeObj = JdbcUtil.getValue(jdbcTemplate, SELECT_ENTITY_TYPE_CODE, new Object[] { entityId },
				"entity_type_code");

		if (entityTypeCodeObj != null) {
			entityTypeCode = String.valueOf(entityTypeCodeObj);
		}

		log.info("MonthlyStatementProcessService - validateAndTriggerMonthlyStatementSchedule - entityTypeCode: "
				+ entityTypeCode);

		if (entityTypeCode == null) {
			throw new RestException("Invalid entity type");
		} // else if (!"REGIONAL_OFFICE".equals(entityTypeCode)) {
			// throw new RestException("Analysis can be executed only for regional
			// offices.");
			// }

		if (MonthlyStatementKey.BANK_ANALYSIS.equals(scheduleName)
				|| MonthlyStatementKey.RIT_ANALYSIS.equals(scheduleName)
				|| MonthlyStatementKey.SUNDRY_ANALYSIS.equals(scheduleName)
				|| MonthlyStatementKey.NOR_SALES_CHECKLIST.equals(scheduleName)
				|| MonthlyStatementKey.NOR_COLLECTION_CHECKLIST.equals(scheduleName)
				|| MonthlyStatementKey.NOR_CHECKLIST.equals(scheduleName)
				|| MonthlyStatementKey.NOR_DATEWISE_CHECKLIST.equals(scheduleName)
				|| MonthlyStatementKey.NOR_JV_STATEMENT.equals(scheduleName)) {

			/**
			 * For alert - NOR LOCK
			 * 
			 */
			validateRunNorDefaultMonthYear(entityId, monthNumber, yearNumber, scheduleName);
		}

		String scheduleStatus = null;
		Double timeDifferenceInMinutes = 0D;

		List<Map<String, Object>> mapList = null;

		Object[] selectStatusValues = { entityId, yearNumber, monthNumber, entityMonthScheduleDTO.getScheduleName() };

		mapList = jdbcTemplate.queryForList(SELECT_NOR_ACCOUNT_ANALISYS_SCHEDULE_STATUS, selectStatusValues);

		String xlFilePath = null;
		String pdfFilePath = null;

		if (mapList != null && !mapList.isEmpty()) {
			Map<String, Object> map = mapList.get(0);
			scheduleStatus = JdbcUtil.getString(map, "schedule_status");
			timeDifferenceInMinutes = JdbcUtil.getDouble(map, "time_difference_in_minutes");
			xlFilePath = JdbcUtil.getString(map, "xl_file_path");
			pdfFilePath = JdbcUtil.getString(map, "pdf_file_path");
			//
			entityMonthScheduleDTO.setXlFilePath(xlFilePath);
			entityMonthScheduleDTO.setPdfFilePath(pdfFilePath);
		}

		/**
		 * SET CREATED BY USER ID
		 */
		entityMonthScheduleDTO.setCreatedBy(loggedInUserId);

		/**
		 * SET CREATED BY USER ID
		 */

		/**
		 * SET CREATED DATE TIME
		 */
		Timestamp currentDateTime = new Timestamp(new java.util.Date().getTime());
		entityMonthScheduleDTO.setCreatedDate(currentDateTime);

		if (scheduleStatus != null) {
			/**
			 * FOR SOME REASONS SCHEDULER MAY NOT PROCESS FOR LONG TIME. IN THAT SITUATION,
			 * SCHEDULE NEEDS TO BE RE-INITIATED
			 * 
			 * ALSO, COMPLETED SCHEDULE CAN BE RE-INITIATED TO REGENERATE THE ANALYSIS
			 */

			String processBreakInMins = JdbcUtil.getAppConfigValue(jdbcTemplate,
					"MONTHLY_STATEMENT_PROCESS_BREAK_IN_MINS");
			Double processBreakInMinsD = 0D;

			if (StringUtils.isNotEmpty(processBreakInMins)) {
				processBreakInMinsD = Double.valueOf(processBreakInMins);
			} else {
				processBreakInMinsD = SIXTY_MINUTES;
			}

			// boolean longTimeNoChange = (IN_PROGRESS.equals(scheduleStatus) &&
			// timeDifferenceInMinutes >= SIXTY_MINUTES);

			boolean longTimeNoChange = (IN_PROGRESS.equals(scheduleStatus)
					&& timeDifferenceInMinutes >= processBreakInMinsD);

			String processIntervalInMins = JdbcUtil.getAppConfigValue(jdbcTemplate,
					"MONTHLY_STATEMENT_PROCESS_INTEVERAL_IN_MINS");
			Double processIntervalInMinsD = 0D;

			if (StringUtils.isNotEmpty(processIntervalInMins)) {
				processIntervalInMinsD = Double.valueOf(processIntervalInMins);
			} else {
				processIntervalInMinsD = FIFTEEN_MINUTES;
			}

			// boolean completedSomeTimeBack = (COMPLETED.equals(scheduleStatus)
			// && timeDifferenceInMinutes >= FIFTEEN_MINUTES);

			boolean completedSomeTimeBack = (COMPLETED.equals(scheduleStatus)
					&& timeDifferenceInMinutes >= processIntervalInMinsD);

			if (longTimeNoChange || completedSomeTimeBack) {
				entityMonthScheduleDTO.setScheduleStatus(INITIATED);
				scheduleService.reinitiateEntityMonthSchedule(entityMonthScheduleDTO);
				scheduleStatus = INITIATED;
			}

		} else {
			entityMonthScheduleDTO.setScheduleStatus(INITIATED);
			Long pkId = scheduleService.saveEntityMonthSchedule(entityMonthScheduleDTO);
			log.info("PkId: " + pkId);
			scheduleStatus = INITIATED;
		}

		baseDTO.setStatusCode(0);
		baseDTO.setResponseContent(entityMonthScheduleDTO);
		baseDTO.setMessage(scheduleStatus);

		return baseDTO;
	}

	/**
	 * 
	 * @param entityMonthScheduleDTO
	 * @return
	 */
	public BaseDTO checkNORProgressStatus(EntityMonthScheduleDTO entityMonthScheduleDTO) {
		log.info("MonthlyStatementProcessService. checkNORProgressStatus() - START");
		BaseDTO baseDTO = null;
		String errorMessage = null;
		try {
			Integer regionId = entityMonthScheduleDTO.getEntityId();
			Integer monthNumber = entityMonthScheduleDTO.getMonthNumber();
			Integer yearNumber = entityMonthScheduleDTO.getYearNumber();
			String scheduleName = entityMonthScheduleDTO.getScheduleName();

			log.info("MonthlyStatementProcessService. checkLockNOR() - regionId: " + regionId);
			log.info("MonthlyStatementProcessService. checkLockNOR() - monthNumber: " + monthNumber);
			log.info("MonthlyStatementProcessService. checkLockNOR() - yearNumber: " + yearNumber);

			
			if(MonthlyStatementKey.LOCK_NOR.equals(scheduleName)) {
				baseDTO = checkLockNOR(entityMonthScheduleDTO);
			}else if (MonthlyStatementKey.BANK_ANALYSIS.equals(scheduleName)
					|| MonthlyStatementKey.RIT_ANALYSIS.equals(scheduleName)
					|| MonthlyStatementKey.SUNDRY_ANALYSIS.equals(scheduleName)
					|| MonthlyStatementKey.NOR_SALES_CHECKLIST.equals(scheduleName)
					|| MonthlyStatementKey.NOR_COLLECTION_CHECKLIST.equals(scheduleName)
					|| MonthlyStatementKey.NOR_CHECKLIST.equals(scheduleName)
					|| MonthlyStatementKey.NOR_DATEWISE_CHECKLIST.equals(scheduleName)
					|| MonthlyStatementKey.NOR_JV_STATEMENT.equals(scheduleName)) {
				validateRunNorDefaultMonthYear(regionId, monthNumber, yearNumber, scheduleName);
			}

			baseDTO = new BaseDTO();
			baseDTO.setStatusCode(0);
		} catch (RestException ex) {
			errorMessage = ex.getMessage();
		} catch (Exception ex) {
			errorMessage = "Unknown error. " + COMM_ERROR_MESSAGE;
			log.error("Exception at checkLockNOR()", ex);
		}

		log.info("errorMessage: " + errorMessage);

		if (errorMessage != null && !errorMessage.isEmpty()) {
			baseDTO = new BaseDTO();
			baseDTO.setStatusCode(1);
			baseDTO.setErrorDescription(errorMessage);
		}

		return baseDTO;
	}
	
	/**
	 * 
	 * @param entityId
	 * @param monthNumber
	 * @param yearNumber
	 * @param scheduleName
	 * @throws Exception
	 */
	private void validateRunNorDefaultMonthYear(Integer entityId, Integer monthNumber, Integer yearNumber,
			String scheduleName) throws Exception {

		Long count = JdbcUtil.getCount(jdbcTemplate, SELECT_NOR_LOCK_FOR_REGION, new Object[] { entityId }, "count");
		count = count == null ? 0 : count.longValue();
		log.info("NOR Lock count for region: " + count);

		Integer defaultStartMonthInt;
		Integer defaultStartYearInt;

		if (count == 0) {
			String defaultNorStartMonth = JdbcUtil.getAppConfigValue(jdbcTemplate,
					AppConfigKey.NOR_DEFAULT_START_MONTH);
			String defaultNorStartYear = JdbcUtil.getAppConfigValue(jdbcTemplate, AppConfigKey.NOR_DEFAULT_START_YEAR);

			log.info("defaultNorStartMonth: " + defaultNorStartMonth);
			log.info("defaultNorStartYear: " + defaultNorStartYear);

			if (StringUtils.isEmpty(defaultNorStartMonth)) {
				defaultStartMonthInt = SYSTEM_STARTED_MONTH;
			} else {
				defaultStartMonthInt = Integer.valueOf(defaultNorStartMonth);
			}

			if (StringUtils.isEmpty(defaultNorStartYear)) {
				defaultStartYearInt = SYSTEM_STARTED_YEAR;
			} else {
				defaultStartYearInt = Integer.valueOf(defaultNorStartYear);
			}

			if (monthNumber.intValue() == defaultStartMonthInt.intValue()
					&& yearNumber.intValue() == defaultStartYearInt.intValue()) {
				log.info("Default month and year is same to run month and year.");
			} else {
				String errorMsg = StringUtils.replace(ALERT_MESSAGE, "${defaultMonth}",
						AppUtil.getMonthName(defaultStartMonthInt));
				errorMsg = StringUtils.replace(errorMsg, "${defaultYear}", String.valueOf(defaultStartYearInt));
				errorMsg = StringUtils.replace(errorMsg, "${monthNumber}", AppUtil.getMonthName(monthNumber));
				errorMsg = StringUtils.replace(errorMsg, "${yearNumber}", String.valueOf(yearNumber));
				throw new RestException(errorMsg);
			}
		} else {

			Long alreadyLockCnt = JdbcUtil.getCount(jdbcTemplate, SELECT_NOR_LOCK_FOR_REGION_MONTH_YEAR,
					new Object[] { entityId, monthNumber, yearNumber }, "count");
			alreadyLockCnt = alreadyLockCnt == null ? 0 : alreadyLockCnt.longValue();
			log.info("NOR Lock count for month and year alreadyLockCnt: " + alreadyLockCnt);
			if (alreadyLockCnt > 0) {
				String errorMsg = "NOR has been locked now for this month ${monthNumber} ${yearNumber}. This record cannot be modified now.";
				errorMsg = StringUtils.replace(errorMsg, "${monthNumber}", AppUtil.getMonthName(monthNumber));
				errorMsg = StringUtils.replace(errorMsg, "${yearNumber}", String.valueOf(yearNumber));
				throw new RestException(errorMsg);
			}

			Integer previousMonthNumber;
			Integer previousYearNumber;

			previousMonthNumber = monthNumber == 1 ? 12 : monthNumber - 1;
			previousYearNumber = monthNumber == 1 ? yearNumber - 1 : yearNumber;

			log.info("previousMonthNumber: " + previousMonthNumber);
			log.info("previousYearNumber: " + previousYearNumber);

			Long lockCnt = JdbcUtil.getCount(jdbcTemplate, SELECT_NOR_LOCK_FOR_REGION_MONTH_YEAR,
					new Object[] { entityId, previousMonthNumber, previousYearNumber }, "count");
			lockCnt = lockCnt == null ? 0 : lockCnt.longValue();
			log.info("NOR Lock count for previous month and year lockCnt: " + lockCnt);

			if (lockCnt == 0) {
				String errorMsg = StringUtils.replace(ALERT_MESSAGE, "${defaultMonth}",
						AppUtil.getMonthName(previousMonthNumber));
				errorMsg = StringUtils.replace(errorMsg, "${defaultYear}", String.valueOf(previousYearNumber));
				errorMsg = StringUtils.replace(errorMsg, "${monthNumber}", AppUtil.getMonthName(monthNumber));
				errorMsg = StringUtils.replace(errorMsg, "${yearNumber}", String.valueOf(yearNumber));
				throw new RestException(errorMsg);
			} else {
				log.info("NOR Lock is completed for previous month and year ");
			}
		}

	}

	/**
	 * 
	 * @param entityMonthScheduleDTO
	 * @return
	 */
	public BaseDTO getStatementStatus(EntityMonthScheduleDTO entityMonthScheduleDTO) {
		BaseDTO baseDTO = new BaseDTO();
		Integer entityId = null;
		Integer yearNumber = null;
		Integer monthNumber = null;
		String scheduleName = null;
		Integer toYearNumber = null;
		Integer toMonthNumber = null;
		String fromDateStr = null;
		String toDateStr = null;
		Date fromDate = null;
		Date toDate = null;
		try {
			final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

			entityId = entityMonthScheduleDTO.getEntityId();
			yearNumber = entityMonthScheduleDTO.getYearNumber();
			monthNumber = entityMonthScheduleDTO.getMonthNumber();
			scheduleName = entityMonthScheduleDTO.getScheduleName();
			toMonthNumber = entityMonthScheduleDTO.getToMonthNumber();
			toYearNumber = entityMonthScheduleDTO.getToYearNumber();

			if (MonthlyStatementKey.BALANCE_SHEET.equals(scheduleName)
					|| MonthlyStatementKey.RECEIPT_AND_CHARGES.equals(scheduleName)
					|| MonthlyStatementKey.PROFIT_AND_LOSS.equals(scheduleName)) {
				/**
				 * From Date / To Date
				 */
				String monthNumStr = monthNumber < 10 ? "0" + monthNumber : String.valueOf(monthNumber);
				fromDateStr = yearNumber + "-" + monthNumStr + "-01";
				String toMonthNumStr = toMonthNumber < 10 ? "0" + toMonthNumber : String.valueOf(toMonthNumber);
				String toDateFirstDayStr = toYearNumber + "-" + toMonthNumStr + "-01";
				toDateStr = AppUtil.getLastDateOfMonth("yyyy-MM-dd", toDateFirstDayStr);
			} else {
				String monthNumStr = monthNumber < 10 ? "0" + monthNumber : String.valueOf(monthNumber);
				fromDateStr = yearNumber + "-" + monthNumStr + "-01";
				toDateStr = AppUtil.getLastDateOfMonth("yyyy-MM-dd", fromDateStr);
			}

			fromDate = simpleDateFormat.parse(fromDateStr);
			toDate = simpleDateFormat.parse(toDateStr);

			String scheduleStatus = null;
			String xlFilePath = null;
			String pdfFilePath = null;
			Object[] selectStatusValues = { entityId, yearNumber, monthNumber, scheduleName };

			List<Map<String, Object>> mapList = jdbcTemplate.queryForList(SELECT_NOR_ACCOUNT_ANALISYS_SCHEDULE_STATUS,
					selectStatusValues);

			if (mapList != null && !mapList.isEmpty()) {
				Map<String, Object> map = mapList.get(0);
				scheduleStatus = JdbcUtil.getString(map, "schedule_status");
				xlFilePath = JdbcUtil.getString(map, "xl_file_path");
				pdfFilePath = JdbcUtil.getString(map, "pdf_file_path");
				entityMonthScheduleDTO.setScheduleStatus(scheduleStatus);
				entityMonthScheduleDTO.setXlFilePath(xlFilePath);
				entityMonthScheduleDTO.setPdfFilePath(pdfFilePath);
			}

			baseDTO.setResponseContent(entityMonthScheduleDTO);
			baseDTO.setStatusCode(0);
			baseDTO.setMessage(scheduleStatus);

		} catch (Exception e) {
			log.error("Exception at MonthlyStatementProcessService.getStatementStatus()", e);
		}
		return baseDTO;
	}

}
