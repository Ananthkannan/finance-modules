package in.gov.cooptex.finance.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.enums.VoucherStatus;
import in.gov.cooptex.core.accounts.enums.VoucherTypeDetails;
import in.gov.cooptex.core.accounts.model.StaffBonusPayment;
import in.gov.cooptex.core.accounts.model.StaffBonusPaymentDetails;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.VoucherDetails;
import in.gov.cooptex.core.accounts.model.VoucherLog;
import in.gov.cooptex.core.accounts.model.VoucherNote;
import in.gov.cooptex.core.accounts.model.VoucherType;
import in.gov.cooptex.core.accounts.repository.StaffBonusPaymentDetailsRepository;
import in.gov.cooptex.core.accounts.repository.StaffBonusPaymentRepository;
import in.gov.cooptex.core.accounts.repository.VoucherDetailsRepository;
import in.gov.cooptex.core.accounts.repository.VoucherLogRepository;
import in.gov.cooptex.core.accounts.repository.VoucherNoteRepository;
import in.gov.cooptex.core.accounts.repository.VoucherRepository;
import in.gov.cooptex.core.accounts.repository.VoucherTypeRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.SequenceName;
import in.gov.cooptex.core.finance.repository.FinancialYearRepository;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.FinancialYear;
import in.gov.cooptex.core.model.SequenceConfig;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.SequenceConfigRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.finance.dto.StaffBonusPaymentDTO;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class StaffBonusPaymentService {

	@Autowired
	StaffBonusPaymentRepository staffBonusPaymentRepository;

	@Autowired
	FinancialYearRepository financialYearRepository;

	@Autowired
	ResponseWrapper responseWrapper;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	@Autowired
	SequenceConfigRepository sequenceConfigRepository;

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	VoucherTypeRepository voucherTypeRepository;

	@Autowired
	EmployeeMasterRepository employeeMasterRepository;

	@Autowired
	UserMasterRepository userMasterRepository;
	@Autowired
	StaffBonusPaymentDetailsRepository staffBonusPaymentDetailsRepository;

	@Autowired
	VoucherDetailsRepository voucherDetailsRepository;

	@Autowired
	VoucherNoteRepository voucherNoteRepository;
	
	@Autowired
	VoucherLogRepository voucherLogRepository;

	@Autowired
	LoginService loginService;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	VoucherRepository voucherRepository;

	public BaseDTO getById(Long id) {
		log.info("StaffBonusPaymentService getById method started [" + id + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			// Validate.notNull(id, ErrorDescription.STAFF_BONUS_PAYMENT_ID_EMPTY);
			StaffBonusPayment staffBonusPayment = staffBonusPaymentRepository.getOne(id);
			baseDTO.setResponseContent(staffBonusPayment);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("StaffBonusPaymentService getById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("StaffBonusPaymentService getById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("StaffBonusPaymentService getById method completed");
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO getByStaffBonusPaymentId(Long id) {
		log.info("StaffBonusPaymentService getById method started [" + id + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			// Validate.notNull(id, ErrorDescription.STAFF_BONUS_PAYMENT_ID_EMPTY);
			StaffBonusPayment staffBonusPayment = staffBonusPaymentRepository.getOne(id);
			StaffBonusPaymentDTO StaffBonusPaymentDTOObj = new StaffBonusPaymentDTO();
			StaffBonusPaymentDTOObj.setEntityMaster(staffBonusPayment.getEntityMaster());
			StaffBonusPaymentDTOObj.setFinYear(staffBonusPayment.getFinYear());
			baseDTO.setResponseContent(staffBonusPayment);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("StaffBonusPaymentService getById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("StaffBonusPaymentService getById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("StaffBonusPaymentService getByStaffBonusPaymentId method completed");
		return responseWrapper.send(baseDTO);
	}

	
	@Transactional
	public BaseDTO createStaffBonusPayment(StaffBonusPaymentDTO staffBonusPaymentDTO) {
		log.info("<============== StaffBonusPaymentService:create ================>");
		BaseDTO baseDTO = new BaseDTO();
		List<StaffBonusPayment> staffBonusPaymentList = new ArrayList<StaffBonusPayment>();
		List<VoucherDetails> voucherDetailsList = new ArrayList<>();
		List<VoucherNote> voucherNoteList = new ArrayList<>();
		List<StaffBonusPaymentDetails> staffBonusPaymentDetailsList = new ArrayList<>();
		try {
			if(staffBonusPaymentDTO.getId()==null) {
				if (staffBonusPaymentDTO != null) {
					EntityMaster entityMaster = null;
					if (loginService.getCurrentUser().getId() != null) {
						entityMaster = entityMasterRepository.findByUserRegion(loginService.getCurrentUser().getId());
					}
					SequenceConfig sequenceConfig = sequenceConfigRepository
							.findBySequenceName(SequenceName.valueOf(SequenceName.STAFF_BONUS_PAYMENT_REF_NUMBER.name()));
					if (sequenceConfig == null) {
						throw new RestException(ErrorDescription.SEQUENCE_CONFIG_NOT_EXIST);
					}
					String referenceNumber = entityMaster.getCode() + sequenceConfig.getSeparator()
							+ sequenceConfig.getPrefix() + AppUtil.getCurrentMonthString() + AppUtil.getCurrentYearString();
					Voucher voucher = new Voucher();
					voucher.setReferenceNumberPrefix(referenceNumber);
					voucher.setReferenceNumber(sequenceConfig.getCurrentValue());
					voucher.setName(VoucherTypeDetails.STAFF_BONUS_PAYMENT.toString());
					VoucherType voucherType = voucherTypeRepository
							.findByName(VoucherTypeDetails.Payment.toString());
	
					voucher.setVoucherType(voucherType);
					voucher.setNarration(VoucherTypeDetails.STAFF_BONUS_PAYMENT.toString());
					voucher.setNetAmount(staffBonusPaymentDTO.getAmountPaid());
		
	
					voucher.setPaymentFor("Staff Bonus Payment");
					voucher.setFromDate(new Date());
					voucher.setToDate(new Date());
					voucher.setPaymentMode(staffBonusPaymentDTO.getPaymentMode());
	
					StaffBonusPayment staffBonusPaymentObj = new StaffBonusPayment();
					staffBonusPaymentObj.setEntityMaster(entityMaster);
					FinancialYear finYear = null;
					if (staffBonusPaymentDTO.getFinYear().getId() != null) {
						finYear = financialYearRepository.findOne(staffBonusPaymentDTO.getFinYear().getId());
					}
					staffBonusPaymentObj.setFinYear(finYear);
					staffBonusPaymentObj.setVoucher(voucher);
					staffBonusPaymentObj.setActiveStatus(true);
	
					staffBonusPaymentList.add(staffBonusPaymentObj);
					for (StaffBonusPaymentDTO staffBonusPayment : staffBonusPaymentDTO.getStaffBonusAllocationList()) {
						log.info(staffBonusPayment.getSelect());
						if (staffBonusPayment.getSelect() == true) {
	
							StaffBonusPaymentDetails staffBonusPaymentDetailsobj = new StaffBonusPaymentDetails();
							log.info("....getEmpid()...." + staffBonusPayment.getEmpid());
							if (staffBonusPayment.getEmpid() != null) {
								staffBonusPaymentDetailsobj
										.setEmpMaster(employeeMasterRepository.findById(staffBonusPayment.getEmpid()));
	
							}
							staffBonusPaymentDetailsobj.setStaffBonusPayment(staffBonusPaymentObj);
							staffBonusPaymentDetailsobj.setAmount(staffBonusPaymentDTO.getAmountPaid());
							staffBonusPaymentDetailsList.add(staffBonusPaymentDetailsobj);
							// Voucher Details
							VoucherDetails voucherDetails = new VoucherDetails();
							voucherDetails.setVoucher(voucher);
							voucherDetails.setAmount(staffBonusPaymentDTO.getAmountPaid());
	
							voucherDetailsList.add(voucherDetails);
						}
					}
					log.info(voucherDetailsList.size());
					voucher.setVoucherDetailsList(voucherDetailsList);
					// Voucher Log
					VoucherLog voucherLog = new VoucherLog();
					voucherLog.setVoucher(voucher);
					voucherLog.setStatus(VoucherStatus.SUBMITTED);
					UserMaster userMaster = null;
					if (loginService.getCurrentUser().getId() != null) {
						userMaster = userMasterRepository.findOne(loginService.getCurrentUser().getId());
					}
					voucherLog.setUserMaster(userMaster);
					voucher.getVoucherLogList().add(voucherLog);
					// Voucher Note
					VoucherNote voucherNote = new VoucherNote();
					log.info("<==getNote()===" + staffBonusPaymentDTO.getNote());
					voucherNote.setNote(staffBonusPaymentDTO.getNote());
					voucherNote.setFinalApproval(staffBonusPaymentDTO.getFinalApproval());
					log.info(staffBonusPaymentDTO.getForwardTo());
					if (staffBonusPaymentDTO.getForwardTo() != null) {
						UserMaster forwardTo = userMasterRepository.findOne((staffBonusPaymentDTO.getForwardTo().getId()));
						voucherNote.setForwardTo(forwardTo);
					}
					voucherNote.setCreatedBy(userMaster);
					voucherNote.setCreatedByName(userMaster.getUsername());
					voucherNote.setCreatedDate(new Date());
					voucherNote.setVoucher(voucher);
					voucherNoteList.add(voucherNote);
					voucher.setVoucherNote(voucherNoteList);
					voucherRepository.save(voucher);
					staffBonusPaymentRepository.save(staffBonusPaymentList);
					staffBonusPaymentDetailsRepository.save(staffBonusPaymentDetailsList);
					sequenceConfig.setCurrentValue(sequenceConfig.getCurrentValue() + 1);
					sequenceConfigRepository.save(sequenceConfig);
					baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				}
			}else {
				baseDTO=updateStaffBonusPayment(staffBonusPaymentDTO);
				log.info("statuscode==="+baseDTO.getStatusCode());
			}
		} catch (RestException restException) {
			log.error("StaffBonusPaymentService  RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("StaffBonusPaymentService Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<======Ends======== StaffBonusPaymentService:create ================>");
		return baseDTO;
	}

	

	public BaseDTO getFinYear() {
		log.info("Start StaffBonusPaymentService:getFinYear");
		BaseDTO baseDTO = new BaseDTO();
		try {

			List<FinancialYear> financialYearList = financialYearRepository.findAll();
			baseDTO.setResponseContents(financialYearList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {

			log.error("StaffBonusPaymentService getFinYear ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {

			log.error("StaffBonusPaymentService getFinYear ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}

		log.info("End StaffBonusPaymentService:getFinYear");
		return baseDTO;

	}

	public BaseDTO getStaffBonuspaymentDetails(StaffBonusPaymentDTO request) {
		log.info("<==========Starts========StaffBonusPaymentService. getStaffBonuspaymentDetails() =====================>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			ApplicationQuery query = applicationQueryRepository.findByQuery("STAFF_DETAILS_QUERY");

			String count_query = query.getQueryContent();
			count_query = count_query.replace(":entityId", request.getEntityMaster().getId().toString());
			count_query = count_query.replace(":finId", request.getFinYear().getId().toString());
			log.info(count_query);
			List<StaffBonusPaymentDTO> staffDetailsList = jdbcTemplate.query(count_query,
					new BeanPropertyRowMapper<StaffBonusPaymentDTO>(StaffBonusPaymentDTO.class));
			log.info(staffDetailsList.size());
			baseDTO.setResponseContent(staffDetailsList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (Exception e) {
			log.error("<=====Inside Exception====>", e);
		}
		log.info("<==========ends========StaffBonusPaymentService. getStaffBonuspaymentDetails() =====================>");
		return baseDTO;
	}

	public BaseDTO getStaffBonusPaymentDetailsLazy(PaginationDTO paginationDto) {
		log.info("<==========Starts========StaffBonusPaymentService. getStaffBonusPaymentDetailsLazy() =====================>");
		BaseDTO baseDTO = new BaseDTO();

		try {

			Integer total = 3;
			Integer start = paginationDto.getFirst(), pageSize = paginationDto.getPageSize();
			start = start * pageSize;

			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			String queryName = "STAF_BONUS_PAYMENT_DETAILS_LIST";

			ApplicationQuery applicationQuery = applicationQueryRepository.findByQueryName(queryName);

			String query = applicationQuery.getQueryContent().trim();

			String mainQuery = query;
			/*
			 * if(paginationDto.getFilters().get("id")!=null) { mainQuery +=
			 * " and cb.id='"+paginationDto.getFilters().get("id")+"'"; }
			 */
			log.info(":: mainQuery ::"+mainQuery);
			if (paginationDto.getFilters().get("entityCodeOrName") != null) {

				if (AppUtil.isInteger(paginationDto.getFilters().get("entityCodeOrName").toString())) {
					mainQuery += " and entityCodeOrName like '%" + paginationDto.getFilters().get("entityCodeOrName")
							+ "%'";
				} else {
					mainQuery += " and upper(entityCodeOrName) like upper('%"
							+ paginationDto.getFilters().get("entityCodeOrName") + "%')";
				}
			}
			if (paginationDto.getFilters().get("finyear") != null) {

				if (AppUtil.isInteger(paginationDto.getFilters().get("finyear").toString())) {
					mainQuery += " and fy.start_year like '%" + paginationDto.getFilters().get("finyear") + "%'";
				} else {
					mainQuery += " and upper(fy.end_year) like upper('%"
							+ paginationDto.getFilters().get("bankBranchCodeOrName") + "%')";
				}
			}

			if (paginationDto.getFilters().get("voucherNumber") != null) {

				if (AppUtil.isInteger(paginationDto.getFilters().get("voucherNumber").toString())) {
					mainQuery += " and designation like '%" + paginationDto.getFilters().get("voucherNumber") + "%'";
				}
			}
			if (paginationDto.getFilters().get("voucherAmount") != null) {
				mainQuery += " and voucherAmount like '%" + paginationDto.getFilters().get("voucherAmount") + "%'";
			}
			if (paginationDto.getFilters().get("voucherDate") != null) {
				Date createdDate = new Date((Long) paginationDto.getFilters().get("voucherDate"));
				mainQuery += " and voucherDate = date '" + createdDate + "'";
			}
			if (paginationDto.getFilters().get("status") != null) {
				mainQuery += " and status like '%" + paginationDto.getFilters().get("status") + "%'";
			}

			// mainQuery += " group by concat(emp.emp_code,'/',emp.first_name,'
			// ',emp.last_name),concat(fy.start_year,' - ',fy.end_year),des.name,vl.status";

			if (paginationDto.getSortField() == null)
				mainQuery += " order by sbp.id desc limit " + pageSize + " offset " + start + ";";

			if (paginationDto.getSortField() != null && paginationDto.getSortOrder() != null) {

				/*
				 * if(paginationDto.getSortField().equals("id") &&
				 * paginationDto.getSortOrder().equals("ASCENDING"))
				 * mainQuery+=" order by id asc "; if(paginationDto.getSortField().equals("id")
				 * && paginationDto.getSortOrder().equals("DESCENDING"))
				 * mainQuery+=" order by id desc ";
				 */

				if (paginationDto.getSortField().equals("entityCodeOrName")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by entityCodeOrName asc  ";
				if (paginationDto.getSortField().equals("entityCodeOrName")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by entityCodeOrName desc  ";

				if (paginationDto.getSortField().equals("finyear") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by finyear asc  ";
				if (paginationDto.getSortField().equals("finyear") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by finyear desc  ";

				if (paginationDto.getSortField().equals("voucherNumber")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by voucherNumber asc  ";
				if (paginationDto.getSortField().equals("voucherNumber")
						&& paginationDto.getSortOrder().equals("designation"))
					mainQuery += " order by voucherNumber desc  ";

				if (paginationDto.getSortField().equals("voucherAmount")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by voucherAmount asc  ";
				if (paginationDto.getSortField().equals("voucherAmount")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by voucherAmount desc  ";

				if (paginationDto.getSortField().equals("status") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by status asc  ";
				if (paginationDto.getSortField().equals("status") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by status desc  ";

				if (paginationDto.getSortField().equals("createdDate")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by date desc  ";

				mainQuery += " limit " + pageSize + " offset " + start + ";";
			}
			log.info(":: after Filtered applied mainQuery ::"+mainQuery);
			List<StaffBonusPaymentDTO> staffBonusPaymentDTOList = new ArrayList<>();
			listofData = jdbcTemplate.queryForList(mainQuery);
			for (Map<String, Object> data : listofData) {
				StaffBonusPaymentDTO staffBonusPaymentDTO = new StaffBonusPaymentDTO();
				/*
				 * if(data.get("id")!=null)
				 * checkBookDetailsDTO.setCheckBookId(Long.parseLong(data.get("id").toString()))
				 * ;
				 */
				/*
				 * if(data.get("advance_type")!=null)
				 * advancePaymentDTO.setAdvanceType(data.get("advance_type").toString());
				 */
				if (data.get("id") != null) {
					staffBonusPaymentDTO.setId(Long.valueOf(data.get("id").toString()));
				}
				if (data.get("entityCodeOrName") != null) {
					staffBonusPaymentDTO.setEntityCodeOrName(data.get("entityCodeOrName").toString());
				} else {
					staffBonusPaymentDTO.setEntityCodeOrName(" - ");
				}
				if (data.get("finyear") != null) {
					staffBonusPaymentDTO.setFinyearr(data.get("finyear").toString());
				}
				if (data.get("voucherNumber") != null) {
					staffBonusPaymentDTO.setVoucherNumber(data.get("voucherNumber").toString());
				}
				if (data.get("voucherAmount") != null) {
					staffBonusPaymentDTO.setEligibleamount(Double.valueOf(data.get("voucherAmount").toString()));
				}
				if (data.get("voucherDate") != null)
					staffBonusPaymentDTO.setVoucherDate((Date) data.get("voucherDate"));
				if (data.get("status") != null) {
					staffBonusPaymentDTO.setStatus(data.get("status").toString());
				}

				staffBonusPaymentDTOList.add(staffBonusPaymentDTO);
			}

			baseDTO.setResponseContent(staffBonusPaymentDTOList);
			baseDTO.setTotalRecords(total);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (Exception exp) {
			log.error("Exception Cause  : ", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<==========Ends========StaffBonusPaymentService. getStaffBonusPaymentDetailsLazy() =====================>");
		return baseDTO;
	}

	public BaseDTO viewStaffBonusPaymentDetailsById(Long id) {
		log.info(" StaffBonusPaymentService viewStaffBonusPaymentDetailsById : " + id);
		BaseDTO baseDTO = new BaseDTO();
		StaffBonusPaymentDTO staffBonusPaymentDTO = new StaffBonusPaymentDTO();
		Voucher voucher = new Voucher();
		try {
			List<Map<String, Object>> queryData = new ArrayList<Map<String, Object>>();
			ApplicationQuery applicationQuery = applicationQueryRepository.findByQueryName("STAFF_BONUS_PAYMENT_VIEW");
			if (applicationQuery == null || applicationQuery.getId() == null) {
				log.info("Application Query not found for query name : " + applicationQuery);
				baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
				return baseDTO;
			}
			String query = applicationQuery.getQueryContent().trim();
			query = query.replace(":sbpid", id.toString());
			log.info("Query Content For Petty Cash Expense View query : " + query);
			queryData = jdbcTemplate.queryForList(query);
			List<StaffBonusPaymentDTO> staffBonusPaymentList = new ArrayList<>();

			if (id != null) {
				StaffBonusPayment staffBonusPaymentObj = staffBonusPaymentRepository.findOne(id);
				staffBonusPaymentDTO.setCreatedDate(staffBonusPaymentObj.getCreatedDate());
				staffBonusPaymentDTO.setModifiedDate(staffBonusPaymentObj.getModifiedDate());
				staffBonusPaymentDTO.setCreatedBy(staffBonusPaymentObj.getCreatedBy());
				staffBonusPaymentDTO.setModifiedBy(staffBonusPaymentObj.getModifiedBy());
				staffBonusPaymentDTO.setId(staffBonusPaymentObj.getId());
				if (staffBonusPaymentObj.getVoucher().getId() != null) {
					if (staffBonusPaymentObj.getVoucher().getId() != null) {
						voucher = voucherRepository.findOne(staffBonusPaymentObj.getVoucher().getId());
					}
					staffBonusPaymentDTO.setPaymentMode(voucher.getPaymentMode());
					log.info("payment mode============>" + staffBonusPaymentDTO.getPaymentMode());
					VoucherNote voucherNote = null;
					VoucherLog voucherlog = null;
					if (staffBonusPaymentObj.getVoucher().getId() != null) {
						voucherNote = voucherNoteRepository.findByVoucherId(staffBonusPaymentObj.getVoucher().getId());
						voucherlog = voucherLogRepository.findByVoucherId(staffBonusPaymentObj.getVoucher().getId());
					}
					if (voucherNote != null) {
						staffBonusPaymentDTO.setVoucherNote(voucherNote);
					}
					if (voucherlog!= null) {
						staffBonusPaymentDTO.setVoucherLog(voucherlog);
					}
					log.info("voucher note============>" + staffBonusPaymentDTO.getVoucherNote());
				}
			}

			if (!queryData.isEmpty()) {
				for (Map<String, Object> queryDataMap : queryData) {
					StaffBonusPaymentDTO staffBonusPaymentDTOObj = new StaffBonusPaymentDTO();
					if (queryDataMap.get("entityCodeOrName") != null) {
						staffBonusPaymentDTO.setEntityCodeOrName(queryDataMap.get("entityCodeOrName").toString());
					}
					if (queryDataMap.get("finYear") != null) {
						staffBonusPaymentDTO.setFinyearr(queryDataMap.get("finYear").toString());
						log.info(staffBonusPaymentDTO.getFinyearr());
					}
					if (queryDataMap.get("detailsid") != null) {
						staffBonusPaymentDTOObj.setId(Long.valueOf(queryDataMap.get("detailsid").toString()));
					}
					if (queryDataMap.get("empid") != null) {
						staffBonusPaymentDTOObj.setEmpid(Long.valueOf(queryDataMap.get("empid").toString()));
					}
					if (queryDataMap.get("empcodeorname") != null) {
						staffBonusPaymentDTOObj.setEmpcodeorname(queryDataMap.get("empcodeorname").toString());
						log.info("employee code or name======>"+staffBonusPaymentDTO.getEmpcodeorname());
					}
					if (queryDataMap.get("designation") != null) {
						staffBonusPaymentDTOObj.setDesignation(queryDataMap.get("designation").toString());
					}
					if (queryDataMap.get("eligibilityAmount") != null) {
						staffBonusPaymentDTOObj
								.setEligibilityAmount(Double.valueOf(queryDataMap.get("eligibilityAmount").toString()));
					}
					if (queryDataMap.get("amountDeducted") != null) {
						staffBonusPaymentDTOObj
								.setAmountDeducted(Double.valueOf(queryDataMap.get("amountDeducted").toString()));
					}
					if (queryDataMap.get("netAmount") != null) {
						staffBonusPaymentDTOObj.setNetAmount(Double.valueOf(queryDataMap.get("netAmount").toString()));
					}
					/*if(id!=null) {
						log.info(id);
					staffBonusPaymentDTOObj.setSelect(true);
					}*/
					staffBonusPaymentList.add(staffBonusPaymentDTOObj);
				}
				staffBonusPaymentDTO.setStaffBonusPaymentList(staffBonusPaymentList);
				staffBonusPaymentDTO.setAmountPaid(staffBonusPaymentList.stream().mapToDouble(StaffBonusPaymentDTO::getAmountPaid).sum());
				

//				Voucher voucher= voucherRepository.findOne(newInvestmentDTO.getNewInvestment().getVoucher().getId());
				
				List<Map<String, Object>> employeeData = new ArrayList<Map<String, Object>>();
				log.info(":: voucher value :: "+voucher !=null ?"Not Null":"Null");
				if(voucher!=null) {
					log.info("<<<:::::::voucher::::not Null::::>>>>"+voucher!=null ? voucher.getId():"Null");
					 ApplicationQuery applicationQueryForlog = applicationQueryRepository.findByQueryName("VOUCHER_LOG_EMPLOYEE_DETAILS");
					 if (applicationQueryForlog == null || applicationQueryForlog.getId() == null) {
						 log.info("Application Query For Log Details not found for query name : " + applicationQueryForlog);
						 baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
						 return baseDTO;
					 }
					 String logquery = applicationQueryForlog.getQueryContent().trim();
					 log.info("<=========VOUCHER_LOG_EMPLOYEE_DETAILS Query Content ======>"+logquery);
					 logquery = logquery.replace(":voucherId", "'"+voucher.getId().toString()+"'");
					 log.info("Query Content For VOUCHER_LOG_EMPLOYEE_DETAILS After replaced plan id View query : " + logquery);
					 employeeData = jdbcTemplate.queryForList(logquery);
					 log.info("<=========VOUCHER_LOG_EMPLOYEE_DETAILS Employee Data======>"+employeeData);
					 baseDTO.setTotalListOfData(employeeData);
				}
				
				
				baseDTO.setResponseContent(staffBonusPaymentDTO);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (RestException restException) {
			baseDTO.setStatusCode(restException.getStatusCode());
			log.error("RestException in SalesPaymentCollectionService ", restException);
		} catch (Exception exception) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.error("Exception in SalesPaymentCollectionService ", exception);
		}
		log.info("<==========Ends========StaffBonusPaymentService.viewStaffBonusPaymentDetailsById =====================>");
		return baseDTO;
	}

	public BaseDTO approveStaffBonusPayment(StaffBonusPaymentDTO staffBonusPaymentDTO) {
		BaseDTO baseDTO = new BaseDTO();
		log.info("<-----approveStaffBonusPayment method start------->");
		try {
			if (staffBonusPaymentDTO.getId() != null) {
				StaffBonusPayment staffBonusPayment = staffBonusPaymentRepository.findOne(staffBonusPaymentDTO.getId());
				Voucher voucher = voucherRepository.getOne(staffBonusPayment.getVoucher().getId());
				log.info("voucher id------------" + voucher);

				VoucherNote voucherNote = new VoucherNote();
				voucherNote.setForwardTo(userMasterRepository.findOne(staffBonusPaymentDTO.getForwardTo().getId()));
				log.info("voucherNote forwardTo user---------------->" + voucherNote.getForwardTo().getId());
				voucherNote.setFinalApproval(staffBonusPaymentDTO.getFinalApproval());
				voucherNote.setNote(staffBonusPaymentDTO.getVoucherNote().getNote());
				voucherNote.setVoucher(voucher);
				voucherNoteRepository.save(voucherNote);
				log.info("<------------voucherNote table Inserted---------------->");

				VoucherLog voucherLog = new VoucherLog();
				voucherLog.setRemarks(staffBonusPaymentDTO.getVoucherLog().getRemarks());
				voucherLog.setVoucher(voucher);
				voucherLog.setStatus(staffBonusPaymentDTO.getVoucherLog().getStatus());
				voucherLog.setUserMaster(userMasterRepository.findOne(staffBonusPaymentDTO.getForwardTo().getId()));
				// staffBonusPaymentRepository.save(voucherLog);
				voucherLogRepository.save(voucherLog);
				log.info("<------------voucherLog table Inserted---------------->");

				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (Exception e) {
			log.error("approveStaffBonusPayment exception ------>", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<-----approveStaffBonusPayment method end------->");
		return baseDTO;
	}

	public BaseDTO rejectStaffBonusPayment(StaffBonusPaymentDTO staffBonusPaymentDTO) {
		BaseDTO baseDTO = new BaseDTO();
		log.info("-----rejectStaffBonusPayment method start-------");
		try {
			if (staffBonusPaymentDTO.getId() != null) {
				StaffBonusPayment staffBonusPayment = staffBonusPaymentRepository.findOne(staffBonusPaymentDTO.getId());
				Voucher voucher = voucherRepository.getOne(staffBonusPayment.getVoucher().getId());
				log.info("rejectStaffBonusPayment voucher id------------" + staffBonusPaymentDTO.getId());

				VoucherLog voucherLog = new VoucherLog();
				voucherLog.setRemarks(staffBonusPaymentDTO.getVoucherLog().getRemarks());
				voucherLog.setVoucher(voucher);
				voucherLog.setStatus(staffBonusPaymentDTO.getVoucherLog().getStatus());
				voucherLog.setUserMaster(userMasterRepository.findOne(staffBonusPaymentDTO.getForwardTo().getId()));
				voucherLogRepository.save(voucherLog);
				log.info("<------------voucherLog table Inserted---------------->");

				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (Exception e) {
			log.error("rejectStaffBonusPayment exception ------>", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<-----rejectStaffBonusPayment method end------->");
		return baseDTO;
	}
	
	
	public BaseDTO updateStaffBonusPayment(StaffBonusPaymentDTO staffBonusPaymentDTO){
		
		log.info("<==============StaffBonusPaymentService:updateStaffBonusPayment ================>");
		log.info("staffBonusPaymentDTO.getId...."+staffBonusPaymentDTO.getId());
		BaseDTO baseDTO=new BaseDTO();Voucher voucher=null;
		List<VoucherDetails> voucherDetailsList=new ArrayList<>();
		try {
			List<StaffBonusPaymentDetails> staffBonusPaymentDetailsList=new ArrayList<>();
			log.info("staffBonusPaymentDTO.getId...."+staffBonusPaymentDTO.getId());
			if(staffBonusPaymentDTO.getId()!=null) {
				StaffBonusPayment staffBonusPaymentObj=staffBonusPaymentRepository.findOne(staffBonusPaymentDTO.getId());
				log.info(staffBonusPaymentObj);
				log.info("staffBonusPaymentObj.......Id"+staffBonusPaymentObj.getVoucher().getId());
				if(staffBonusPaymentObj.getVoucher().getId()!=null) {
					voucher=voucherRepository.findOne(staffBonusPaymentObj.getVoucher().getId());
					log.info(voucher);
					log.info("voucherId........."+voucher.getId());
				}
				UserMaster userMaster = userMasterRepository.findOne(loginService.getCurrentUser().getId());
				log.info(staffBonusPaymentDTO.getAmountPaid());
				voucher.setNetAmount(staffBonusPaymentDTO.getAmountPaid());
				voucher.setFromDate(new Date());
				voucher.setToDate(new Date());
				voucher.setCreatedBy(userMaster);
				
				voucher.setPaymentMode(staffBonusPaymentDTO.getPaymentMode());
				voucherRepository.save(voucher);
				
					/*for (StaffBonusPaymentDTO staffBonusPaymentdtoObj : staffBonusPaymentDTO.getStaffBonusPaymentList()) {
						log.info("staffBonusPaymentdtoObj id-------"+staffBonusPaymentdtoObj.getId());
						
					//staffBonusPaymentDetailsRepository.deleteByStaffBonusId(staffBonusPaymentdtoObj.getId());
						
					}*/
				
				
				List<VoucherDetails> voucherDetailsLists=voucherDetailsRepository
						.findVoucherDetailsByVoucherId(voucher.getId());
				if(voucherDetailsLists.size()>0) {
					voucherDetailsRepository.deleteVoucherDetailsByVoucherID(voucher.getId());
				}

				for (StaffBonusPaymentDTO staffBonusPayment : staffBonusPaymentDTO.getStaffBonusPaymentList()) {
					
					log.info("check box-----"+staffBonusPayment.getSelect());
					if (staffBonusPayment.getSelect() == true) {

						StaffBonusPaymentDetails staffBonusPaymentDetailsobj = staffBonusPaymentDetailsRepository.findOne(staffBonusPayment.getId());
						log.info("....getEmpid()...." + staffBonusPayment.getEmpid());
						if (staffBonusPayment.getEmpid() != null) {
							staffBonusPaymentDetailsobj
									.setEmpMaster(employeeMasterRepository.findById(staffBonusPayment.getEmpid()));

						}
						staffBonusPaymentDetailsobj.setStaffBonusPayment(staffBonusPaymentObj);
						staffBonusPaymentDetailsobj.setAmount(staffBonusPaymentDTO.getAmountPaid());
						staffBonusPaymentDetailsList.add(staffBonusPaymentDetailsobj);
						// Voucher Details
						VoucherDetails voucherDetails = new VoucherDetails();
						voucherDetails.setVoucher(voucher);
						voucherDetails.setAmount(staffBonusPaymentDTO.getAmountPaid());

						voucherDetailsList.add(voucherDetails);
					}
				}
				voucherDetailsRepository.save(voucherDetailsList);
				//Voucher Log
				VoucherLog voucherLog = new VoucherLog();
				voucherLog.setVoucher(voucher);
				voucherLog.setStatus(VoucherStatus.SUBMITTED);
			
				voucherLog.setUserMaster(userMaster);
				voucherLogRepository.save(voucherLog);
				//Voucher Note
				VoucherNote voucherNote = new VoucherNote();
				voucherNote.setNote(staffBonusPaymentDTO.getNote());
				voucherNote.setFinalApproval(staffBonusPaymentDTO.getFinalApproval());
				
				if(staffBonusPaymentDTO.getForwardTo()!=null) {
					UserMaster forwardTo=userMasterRepository.findOne((staffBonusPaymentDTO.getForwardTo().getId()));
					voucherNote.setForwardTo(forwardTo);
				}
				voucherNote.setCreatedBy(userMaster);
				voucherNote.setCreatedByName(userMaster.getUsername());
				voucherNote.setCreatedDate(new Date());
				voucherNote.setVoucher(voucher);
				voucherNoteRepository.save(voucherNote);
				
				staffBonusPaymentDetailsRepository.save(staffBonusPaymentDetailsList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				log.info("statusCode....."+baseDTO.getStatusCode());
				
			}
		}catch (RestException restException) {
			log.error("StaffBonusPaymentService updateStaffBonusPayment RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("StaffBonusPaymentService updateStaffBonusPayment Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}
}