package in.gov.cooptex.finance.service;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.dto.PfPaymentDTO;
import in.gov.cooptex.core.accounts.enums.VoucherStatus;
import in.gov.cooptex.core.accounts.enums.VoucherTypeDetails;
import in.gov.cooptex.core.accounts.model.PfPayment;
import in.gov.cooptex.core.accounts.model.PfPaymentDetails;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.VoucherDetails;
import in.gov.cooptex.core.accounts.model.VoucherLog;
import in.gov.cooptex.core.accounts.model.VoucherNote;
import in.gov.cooptex.core.accounts.model.VoucherType;
import in.gov.cooptex.core.accounts.repository.PfPaymentDetailsRepository;
import in.gov.cooptex.core.accounts.repository.PfPaymentRepository;
import in.gov.cooptex.core.accounts.repository.VoucherLogRepository;
import in.gov.cooptex.core.accounts.repository.VoucherNoteRepository;
import in.gov.cooptex.core.accounts.repository.VoucherRepository;
import in.gov.cooptex.core.accounts.repository.VoucherTypeRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.SequenceName;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.SequenceConfig;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.PaymentModeRepository;
import in.gov.cooptex.core.repository.SequenceConfigRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.FinanceErrorCode;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class PfPaymentService {

	@Autowired
	PfPaymentRepository pfPaymentRepository;

	@Autowired
	PfPaymentDetailsRepository pfPaymentDetailsRepository;

	@Autowired
	ResponseWrapper responseWrapper;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	SequenceConfigRepository sequenceConfigRepository;

	@Autowired
	EmployeeMasterRepository employeeMasterRepository;

	@Autowired
	LoginService loginService;

	@Autowired
	VoucherTypeRepository voucherTypeRepository;

	@Autowired
	VoucherRepository voucherRepository;

	@Autowired
	UserMasterRepository userMasterRepository;

	@Autowired
	PaymentModeRepository paymentModeRepository;

	@Autowired
	VoucherNoteRepository voucherNoteRepository;

	@Autowired
	VoucherLogRepository voucherLogRepository;

	public BaseDTO getById(Long id) {
		log.info("PfPaymentService getById method started [" + id + "]");
		BaseDTO baseDTO = new BaseDTO();
		String query = "";
		String queryName = "";
		try {
			Integer total = 0;
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();
			PfPaymentDTO pfPaymentDTO = new PfPaymentDTO();
			Validate.notNull(id, ErrorDescription.STAFF_PF_PAYMENT_ID_EMPTY);
			if (id != null) {
				PfPayment pfPayment = pfPaymentRepository.getOne(id);
				log.info("month==> " + pfPayment.getMonth());
				pfPaymentDTO.setMonth(getMonthForInt(Integer.valueOf(pfPayment.getMonth())));
				pfPaymentDTO.setYear(pfPayment.getYear());
				if (pfPayment.getVoucher().getId() != null) {
					Voucher voucher = voucherRepository.findOne(pfPayment.getVoucher().getId());
					pfPaymentDTO.setVoucherNarration(voucher.getNarration());
					pfPaymentDTO.setPaymentMode(voucher.getPaymentMode());
					log.info("payment mode============>" + pfPaymentDTO.getPaymentMode());
					VoucherNote voucherNote = voucherNoteRepository.findByVoucherId(pfPayment.getVoucher().getId());
					if (voucherNote != null) {
						pfPaymentDTO.setVoucherNote(voucherNote);
					}
					log.info("voucher note============>" + pfPaymentDTO.getVoucherNote());
				}
				queryName = "STAFF_PF_PAYMENT_VIEW_QUERY";
				query = getQueryContent(queryName);
				query = query.replace(":pfpaymentId", "'" + id + "'");
				List<PfPaymentDTO> entityWisePfPaymentDetailsList = jdbcTemplate.query(query,
						new BeanPropertyRowMapper<PfPaymentDTO>(PfPaymentDTO.class));
				queryName = "STAFF_PF_PAYMENT_FIND_REGION_TYPE";
				query = getQueryContent(queryName);
				query = query.replace(":pfpaymentId", "'" + id + "'");
				log.info("regionTypeQuery query... " + query);
				countData = jdbcTemplate.queryForList(query);
				for (Map<String, Object> data : countData) {
					if (data.get("rcount") != null)
						total = Integer.parseInt(data.get("rcount").toString().replace(",", ""));
				}
				log.info("total region type count------->" + total);
				if (total == 1) {
					pfPaymentDTO.setRegionType(entityWisePfPaymentDetailsList.get(0).getRegionType());
					pfPaymentDTO.setRegionName(entityWisePfPaymentDetailsList.get(0).getRegionName());
				} else {
					pfPaymentDTO.setRegionType("All");
				}
				log.info("<================= Staff Pf Payment View entityWisePfPaymentDetailsList size ===============>"
						+ entityWisePfPaymentDetailsList.size());
				pfPaymentDTO.setEntityWisePfPaymentDetailsList(entityWisePfPaymentDetailsList);
				baseDTO.setResponseContent(pfPaymentDTO);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (RestException restException) {
			log.error("PfPaymentService getById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("PfPaymentService getById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("PfPaymentService getById method completed");
		return responseWrapper.send(baseDTO);
	}

	private String getMonthForInt(int m) {
		String month = "invalid";
		DateFormatSymbols dfs = new DateFormatSymbols();
		String[] months = dfs.getMonths();
		if (m >= 0 && m <= 11) {
			m = m - 1;
			month = months[m];
		}
		return month;
	}

	public String getQueryContent(String queryName) {
		log.info("PfPaymentService getQueryContent method started ]" + "[ queryname ]" + queryName);
		String query = "";
		BaseDTO baseDTO = new BaseDTO();
		try {
			ApplicationQuery applicationQuery = applicationQueryRepository.findByQueryName(queryName);
			if (applicationQuery == null || applicationQuery.getId() == null) {
				log.info("Application Query not found for query name : " + applicationQuery);
				baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
			}
			query = applicationQuery.getQueryContent().trim();
			log.info("<================= getQueryContent query ===============>" + query);
		} catch (Exception exception) {
			log.error("PfPaymentService getQueryContent Exception ", exception);
		}
		return query;
	}

	public BaseDTO getStaffPfDetailsById(PfPaymentDTO pfPaymentDTO) {
		log.info("PfPaymentService getStaffPfDetailsById method started [" + pfPaymentDTO.getId() + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			Validate.notNull(pfPaymentDTO.getId(), ErrorDescription.STAFF_PF_PAYMENT_ID_EMPTY);
			if (pfPaymentDTO.getId() != null) {
				String queryName = "STAFF_PF_PAYMENT_REGIONWISE_VIEW_QUERY";
				String query = getQueryContent(queryName);
				query = query.replace(":pfpaymentId", "'" + pfPaymentDTO.getId() + "'");
				query = query.replace(":entityId", "'" + pfPaymentDTO.getEntityId() + "'");
				log.info("STAFF_PF_PAYMENT_REGIONWISE_VIEW_QUERY ------->" + query);
				List<PfPaymentDTO> regionWisePfPaymentDetailsList = jdbcTemplate.query(query,
						new BeanPropertyRowMapper<PfPaymentDTO>(PfPaymentDTO.class));
				pfPaymentDTO.setRegionWisePfPaymentDetailsList(regionWisePfPaymentDetailsList);
				log.info("Staff Pf Payment View regionWisePfPaymentDetailsList size------------>"
						+ regionWisePfPaymentDetailsList.size());
				baseDTO.setResponseContent(pfPaymentDTO);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (RestException restException) {
			log.error("PfPaymentService getStaffPfDetailsById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("PfPaymentService getStaffPfDetailsById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("PfPaymentService getStaffPfDetailsById method completed");
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO createPfPayment(PfPaymentDTO pfPaymentDTO) {
		log.info("PfPaymentService createPfPayment method started");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<PfPaymentDetails> pfPaymentDetailsList = new ArrayList<>();
			List<VoucherDetails> voucherDetailsList = new ArrayList<>();
			List<VoucherNote> voucherNoteList = new ArrayList<>();
			if (pfPaymentDTO.getId() == null) {
				Validate.objectNotNull(pfPaymentDTO.getNote(), ErrorDescription.POLICY_NOTE_PROCESS_CREATENOTE);
				Validate.objectNotNull(pfPaymentDTO.getFinalApproval(),
						ErrorDescription.INTEND_CONSOLIDATION_REQUIREMENT_FORWARDFOR_REQUIRED);
				Validate.objectNotNull(pfPaymentDTO.getForwardTo(),
						ErrorDescription.INTEND_CONSOLIDATION_REQUIREMENT_FORWARDTO_REQUIRED);
				Validate.objectNotNull(pfPaymentDTO.getVoucherNarration(),
						ErrorDescription.STAFF_PF_PAYMENT_NARRATION_EMPTY);
				Validate.objectNotNull(pfPaymentDTO.getPaymentMode().getPaymentMode(),
						ErrorDescription.VOUCHER_PAYMENT_MODE_IS_REQUIRED);
				EntityMaster entityMaster = entityMasterRepository
						.findByUserRegion(loginService.getCurrentUser().getId());
				SequenceConfig sequenceConfig = sequenceConfigRepository
						.findBySequenceName(SequenceName.valueOf(SequenceName.STAFF_PF_PAYMENT_NUMBER.name()));
				if (sequenceConfig == null) {
					throw new RestException(ErrorDescription.SEQUENCE_CONFIG_NOT_EXIST);
				}
				String referenceNumberPrefix = entityMaster.getCode() + sequenceConfig.getSeparator()
						+ sequenceConfig.getPrefix() + AppUtil.getCurrentYearString() + AppUtil.getCurrentMonthString();
				Long referenceNumber = sequenceConfig.getCurrentValue();
				sequenceConfig.setCurrentValue(sequenceConfig.getCurrentValue() + 1);
				sequenceConfigRepository.save(sequenceConfig);
				log.info(
						"<======================= Staff Pf Payment Sequence config Table Inserted =======================>");
				// Voucher
				Voucher voucher = new Voucher();
				voucher.setReferenceNumberPrefix(referenceNumberPrefix);
				voucher.setReferenceNumber(referenceNumber);
				voucher.setName(VoucherTypeDetails.Staff_PF_Payment.toString());
				VoucherType voucherType = voucherTypeRepository
						.findByName(VoucherTypeDetails.Staff_PF_Payment.toString());
				voucher.setVoucherType(voucherType);
				voucher.setNetAmount(pfPaymentDTO.getPfAccountTotalAmount());
				voucher.setNarration(pfPaymentDTO.getVoucherNarration());
				voucher.setFromDate(new Date());
				voucher.setToDate(new Date());
				voucher.setPaymentMode(pfPaymentDTO.getPaymentMode());
				// Voucher Details
				VoucherDetails voucherDetails = new VoucherDetails();
				voucherDetails.setVoucher(voucher);
				voucherDetails.setAmount(pfPaymentDTO.getPfAccountTotalAmount());
				voucherDetailsList.add(voucherDetails);
				voucher.setVoucherDetailsList(voucherDetailsList);
				log.info(
						"<======================= Staff Pf Payment Voucher Detail Table Inserted =======================>");
				// Pf Payment
				PfPayment pfPayment = new PfPayment();
				pfPayment.setMonth(pfPaymentDTO.getMonth());
				pfPayment.setYear(pfPaymentDTO.getYear());
				pfPayment.setAmount(pfPaymentDTO.getPfAccountTotalAmount());
				pfPayment.setVoucher(voucher);
				log.info("<======================= Staff Pf Payment PfPayment Table Inserted =======================>");
				// Voucher Log
				VoucherLog voucherLog = new VoucherLog();
				voucherLog.setVoucher(voucher);
				voucherLog.setStatus(VoucherStatus.SUBMITTED);
				UserMaster userMaster = userMasterRepository.findOne(loginService.getCurrentUser().getId());
				voucherLog.setUserMaster(userMaster);
				voucher.getVoucherLogList().add(voucherLog);
				log.info(
						"<======================= Staff Pf Payment Voucher Log Table Inserted =======================>");
				// Voucher Note
				VoucherNote voucherNote = new VoucherNote();
				voucherNote.setNote(pfPaymentDTO.getNote());
				voucherNote.setFinalApproval(pfPaymentDTO.getFinalApproval());
				if (pfPaymentDTO.getForwardTo() != null) {
					UserMaster forwardTo = userMasterRepository.findOne((pfPaymentDTO.getForwardTo().getId()));
					voucherNote.setForwardTo(forwardTo);
				}
				voucherNote.setCreatedBy(userMaster);
				voucherNote.setCreatedByName(userMaster.getUsername());
				voucherNote.setCreatedDate(new Date());
				voucherNote.setVoucher(voucher);
				voucherNoteList.add(voucherNote);
				voucher.setVoucherNote(voucherNoteList);
				log.info(
						"<======================= Staff Pf Payment Sequence Voucher Note Inserted =======================>");
				// Pf Payment Details
				if (pfPaymentDTO.getEntityWisePfPaymentDetailsList().size() > 0) {
					for (PfPaymentDTO pfPaymentDTOEntityObj : pfPaymentDTO.getEntityWisePfPaymentDetailsList()) {
						pfPaymentDTOEntityObj.setRegionType(pfPaymentDTO.getRegionType());
						pfPaymentDTOEntityObj.setMonth(pfPaymentDTO.getMonth());
						pfPaymentDTOEntityObj.setYear(pfPaymentDTO.getYear());
						pfPaymentDTOEntityObj.setEntityMaster(pfPaymentDTO.getEntityMaster());
						getRegionWisePfPaymentDetails(pfPaymentDTOEntityObj);
						log.info("create region list size------>"
								+ pfPaymentDTOEntityObj.getRegionWisePfPaymentDetailsList().size());
						for (PfPaymentDTO pfPaymentDTORegionObj : pfPaymentDTOEntityObj
								.getRegionWisePfPaymentDetailsList()) {
							PfPaymentDetails pfPaymentDetails = new PfPaymentDetails();
							pfPaymentDetails.setPfPayment(pfPayment);
							if (pfPaymentDTOEntityObj.getEntityId() != null) {
								pfPaymentDetails.setEntityMaster(
										entityMasterRepository.findOne(pfPaymentDTOEntityObj.getEntityId()));
							}
							if (pfPaymentDTORegionObj.getEmployeeId() != null) {
								pfPaymentDetails.setEmployee(
										employeeMasterRepository.findOne(pfPaymentDTORegionObj.getEmployeeId()));
							}
							pfPaymentDetails.setGrossWages(pfPaymentDTORegionObj.getGrossWages());
							pfPaymentDetails.setEpfWages(pfPaymentDTORegionObj.getEpfWages());
							pfPaymentDetails.setEpsWages(pfPaymentDTORegionObj.getEpsWages());
							pfPaymentDetails.setEdliWages(pfPaymentDTORegionObj.getEdliWages());
							pfPaymentDetails.setEeShareRemitted(pfPaymentDTORegionObj.getEeShareRemitted());
							pfPaymentDetails.setEpsContribution(pfPaymentDTORegionObj.getEpsContribution());
							pfPaymentDetails.setErShareRemitted(pfPaymentDTORegionObj.getErShareRemitted());
							pfPaymentDetails.setNcpDays(pfPaymentDTORegionObj.getNcpDays());
							pfPaymentDetails.setRefundAdvance(pfPaymentDTORegionObj.getRefundAdvance());
							pfPaymentDetailsList.add(pfPaymentDetails);
						}
					}
					log.info("<========= StaffPfPayment pfPaymentDetailsList size() =========>"
							+ pfPaymentDetailsList.size());
				}
				pfPaymentDetailsRepository.save(pfPaymentDetailsList);
				log.info(
						"<======================= Staff Pf Payment Pfpayment Details Table Inserted =======================>");
				voucherRepository.save(voucher);
				log.info("<======================= Staff Pf Payment Voucher Table Inserted =======================>");
				pfPaymentRepository.save(pfPayment);
				log.info("<======================= Staff Pf Payment Pfpayment Table Inserted =======================>");
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (RestException restException) {
			log.error("PfPaymentService createPfPayment RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (DataIntegrityViolationException exception) {
			log.error("PfPaymentService createPfPayment DataIntegrityViolationException ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		} catch (Exception exception) {
			log.error("PfPaymentService createPfPayment Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("PfPaymentService createPfPayment method completed");
		return baseDTO;
	}

	public BaseDTO getEntityWisePfPaymentDetails(PfPaymentDTO pfPaymentDTO) {
		log.info("PfPaymentService getEntityWisePfPaymentDetails method started");
		BaseDTO baseDTO = new BaseDTO();
		try {
			ApplicationQuery applicationQuery = applicationQueryRepository
					.findByQueryName("ENTITYWISE_PF_PAYMENT_DETAILS_QUERY");
			if (applicationQuery == null || applicationQuery.getId() == null) {
				log.info("Application Query not found for query name : " + applicationQuery);
				baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
				return baseDTO;
			}
			String query = "";
			if (pfPaymentDTO.getRegionType().equalsIgnoreCase("All")) {
				log.info("-------------Inside All Pf details EntityWise-------------------------");
				StringBuilder queryBuilder = new StringBuilder().append(applicationQuery.getQueryContent().trim());
				queryBuilder = queryBuilder.append(
						" where month=:monthValue and year=:yearValue group by epd.emp_id,em.id,em.name,gl.name,gl.code)t group by t.entityCodeOrName,t.entityId) x");
				query = queryBuilder.toString();
				query = query.replace(":monthValue", "'" + pfPaymentDTO.getMonth() + "'");
				query = query.replace(":yearValue", "'" + pfPaymentDTO.getYear() + "'");
			} else if (pfPaymentDTO.getRegionType().equalsIgnoreCase("Head Office")) {
				log.info("-------------Inside Head Office Pf details EntityWise-------------------------");
				StringBuilder queryBuilder = new StringBuilder().append(applicationQuery.getQueryContent().trim());
				queryBuilder = queryBuilder.append(
						" where et1.name in (:entityTypeName) and month=:monthValue and year=:yearValue group by epd.emp_id,em.id,em.name,gl.name,gl.code)t \n"
								+ "group by t.entityCodeOrName,t.entityId) x");
				query = queryBuilder.toString();
				query = query.replace(":entityTypeName", "'" + pfPaymentDTO.getRegionType() + "'");
				query = query.replace(":monthValue", "'" + pfPaymentDTO.getMonth() + "'");
				query = query.replace(":yearValue", "'" + pfPaymentDTO.getYear() + "'");
			} else if (pfPaymentDTO.getRegionType().equalsIgnoreCase("Regional Office")) {
				log.info("-------------Inside Regional Office Pf details EntityWise-------------------------");
				StringBuilder queryBuilder = new StringBuilder().append(applicationQuery.getQueryContent().trim());
				queryBuilder = queryBuilder.append(" where et1.name in (:entityTypeName)\n"
						+ "and em1.name = :entityName and month=:monthValue and year=:yearValue group by epd.emp_id,em.id,em.name,gl.name,gl.code)t \n"
						+ "group by t.entityCodeOrName,t.entityId) x");
				query = queryBuilder.toString();
				query = query.replace(":entityTypeName", "'" + pfPaymentDTO.getRegionType() + "'");
				query = query.replace(":entityName", "'" + pfPaymentDTO.getEntityMaster().getName() + "'");
				query = query.replace(":monthValue", "'" + pfPaymentDTO.getMonth() + "'");
				query = query.replace(":yearValue", "'" + pfPaymentDTO.getYear() + "'");
			}
			log.info("query================>" + query);
			List<PfPaymentDTO> regionWisePfPaymentDetailsList = jdbcTemplate.query(query,
					new BeanPropertyRowMapper<PfPaymentDTO>(PfPaymentDTO.class));
			int regionWisePfPaymentDetailsListSize = regionWisePfPaymentDetailsList == null ? 0
					: regionWisePfPaymentDetailsList.size();
			if(regionWisePfPaymentDetailsListSize == 0) {
				baseDTO.setStatusCode(ErrorDescription.getError(FinanceErrorCode.PAYROLL_DATA_NOT_AVAILABLE).getCode());
				return baseDTO;
			}else {
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
			String existQuery = query
					.concat(" WHERE x.entityId NOT IN (SELECT DISTINCT pd.entity_id FROM pf_payment_details pd "
							+ "JOIN pf_payment p ON pd.pf_payment_id=p.id " + "JOIN voucher v ON p.voucher_id=v.id "
							+ "JOIN voucher_log vl ON v.id=vl.voucher_id " + "WHERE vl.status<>'REJECTED' AND p.month='"
							+ pfPaymentDTO.getMonth() + "' AND p.year='" + pfPaymentDTO.getYear() + "')");
			List<PfPaymentDTO> alreadyPaidegionWisePfPaymentDetailsList = jdbcTemplate.query(existQuery,
					new BeanPropertyRowMapper<PfPaymentDTO>(PfPaymentDTO.class));
			int alreadyPaidPfPaymentDetailsListSize = alreadyPaidegionWisePfPaymentDetailsList == null ? 0
					: alreadyPaidegionWisePfPaymentDetailsList.size();
			if(alreadyPaidPfPaymentDetailsListSize == 0) {
				baseDTO.setStatusCode(ErrorDescription.getError(FinanceErrorCode.PF_PAYMENT_ALREADY_PAID).getCode());
				return baseDTO;
			}else {
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
			
			pfPaymentDTO.setEntityWisePfPaymentDetailsList(regionWisePfPaymentDetailsList);
			log.info(pfPaymentDTO.getEntityWisePfPaymentDetailsList());
			baseDTO.setResponseContent(pfPaymentDTO);
		} catch (RestException restException) {
			log.error("PfPaymentService getEntityWisePfPaymentDetails RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("PfPaymentService getEntityWisePfPaymentDetails Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO getRegionWisePfPaymentDetails(PfPaymentDTO pfPaymentDTO) {
		log.info("PfPaymentService getRegionWisePfPaymentDetails method started");
		BaseDTO baseDTO = new BaseDTO();
		try {
			ApplicationQuery applicationQuery = applicationQueryRepository
					.findByQueryName("REGIONWISE_PF_PAYMENT_DETAILS_QUERY");
			if (applicationQuery == null || applicationQuery.getId() == null) {
				log.info("Application Query not found for query name : " + applicationQuery);
				baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
				return baseDTO;
			}
			String query = "";
			StringBuilder queryBuilder = new StringBuilder().append(applicationQuery.getQueryContent().trim());
			queryBuilder = queryBuilder.append(
					" WHERE em.id=:entityId AND month=:monthValue AND year=:yearValue GROUP BY epie.emp_id,epie.pf_number,gl.name,gl.code)t "
							+ "GROUP BY t.emp_id,t.pf_number) x JOIN emp_master emp ON emp.id=x.emp_id");
			query = queryBuilder.toString();
			query = query.replace(":entityId", "'" + pfPaymentDTO.getEntityId() + "'");
			query = query.replace(":monthValue", "'" + pfPaymentDTO.getMonth() + "'");
			query = query.replace(":yearValue", "'" + pfPaymentDTO.getYear() + "'");

			log.info("query================>" + query);
			List<PfPaymentDTO> regionWisePfPaymentDetailsList = jdbcTemplate.query(query,
					new BeanPropertyRowMapper<PfPaymentDTO>(PfPaymentDTO.class));
			pfPaymentDTO.setRegionWisePfPaymentDetailsList(regionWisePfPaymentDetailsList);
			baseDTO.setResponseContent(pfPaymentDTO);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("PfPaymentService getRegionWisePfPaymentDetails RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("PfPaymentService getRegionWisePfPaymentDetails Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO lazyloadlist(PaginationDTO request) {
		log.info("<====================== PfPaymentService lazyloadlist method started ==================>");
		BaseDTO baseDTO = new BaseDTO();
		try {

			Integer total = 0;
			Integer start = request.getFirst(), pageSize = request.getPageSize();
			start = start * pageSize;
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();
			ApplicationQuery applicationQuery = applicationQueryRepository.findByQueryName("PF_PAYMENT_LIST_QUERY");

			String mainQuery = applicationQuery.getQueryContent().trim();
			if (request.getFilters() != null) {
				if (request.getFilters().get("voucherNumber") != null) {
					mainQuery += " and v.reference_number_prefix like '%" + request.getFilters().get("voucherNumber")
							+ "%'";
				}
				if (request.getFilters().get("month") != null) {
					// mainQuery += " and pfp.month like '%"+request.getFilters().get("month")+"%'";
					mainQuery += " and upper(to_char(to_timestamp (pfp.month, 'MM'), 'Month')) like upper('%"
							+ request.getFilters().get("month") + "%')";
				}
				if (request.getFilters().get("year") != null) {
					mainQuery += " and (cast(pfp.year as varchar)) like '%" + request.getFilters().get("year") + "%'";
				}
				if (request.getFilters().get("status") != null) {
					mainQuery += " and upper(vl.status) like upper('%" + request.getFilters().get("status") + "%')";
				}
			}
			String countQuery = "select count(*) as count from pf_payment";
			log.info("count query... " + countQuery);
			countData = jdbcTemplate.queryForList(countQuery);
			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					total = Integer.parseInt(data.get("count").toString().replace(",", ""));
			}

			if (request.getSortField() == null)
				mainQuery += " order by pfpayment_id desc limit " + pageSize + " offset " + start + ";";

			if (request.getSortField() != null && request.getSortOrder() != null) {
				log.info("Sort Field:[" + request.getSortField() + "] Sort Order:[" + request.getSortOrder() + "]");
				if (request.getSortField().equals("voucherNumber") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by v.reference_number_prefix asc ";
				if (request.getSortField().equals("voucherNumber") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by v.reference_number_prefix desc ";
				if (request.getSortField().equals("month") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by pfp.month asc  ";
				if (request.getSortField().equals("month") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by pfp.month desc  ";
				if (request.getSortField().equals("year") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by pfp.year asc ";
				if (request.getSortField().equals("year") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by pfp.year desc ";
				if (request.getSortField().equals("status") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by vl.status asc  ";
				if (request.getSortField().equals("status") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by vl.status desc  ";
				mainQuery += " limit " + pageSize + " offset " + start + ";";
			}
			log.info("Main Qury....." + mainQuery);
			List<PfPaymentDTO> pfPaymentDTOList = new ArrayList<>();
			listofData = jdbcTemplate.queryForList(mainQuery);
			for (Map<String, Object> data : listofData) {
				PfPaymentDTO pfPaymentDTO = new PfPaymentDTO();
				if (data.get("pfpayment_id") != null) {
					pfPaymentDTO.setId(Long.parseLong(data.get("pfpayment_id").toString()));
				}
				if (data.get("voucher_no") != null) {
					pfPaymentDTO.setVoucherNumber(data.get("voucher_no").toString());
				}
				if (data.get("pf_month") != null) {

					log.info("monthName==> " + data.get("pf_month").toString());
					pfPaymentDTO.setMonth(data.get("pf_month").toString());
				}
				if (data.get("pf_year") != null) {
					pfPaymentDTO.setYear(Long.valueOf(data.get("pf_year").toString()));
				}
				if (data.get("status") != null) {
					pfPaymentDTO.setStatus(data.get("status").toString());
				}
				pfPaymentDTOList.add(pfPaymentDTO);
			}
			log.info("Returning PfPaymentDTO list...." + pfPaymentDTOList);
			log.info("Total records present in PfPaymentDTO..." + total);
			baseDTO.setResponseContent(pfPaymentDTOList);
			baseDTO.setTotalRecords(total);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception exp) {
			log.error("Exception Cause  : ", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO approveStaffPfPayment(PfPaymentDTO pfPaymentDTO) {
		BaseDTO baseDTO = new BaseDTO();
		log.info("<==================== approveStaffPfPayment method start =====================>");
		try {
			if (pfPaymentDTO.getId() != null) {
				PfPayment pfPayment = pfPaymentRepository.getOne(pfPaymentDTO.getId());
				if (pfPayment.getVoucher().getId() != null) {
					pfPayment.setVoucher(pfPayment.getVoucher());
					log.info("voucher id------------" + pfPayment.getVoucher());
				}
				UserMaster userMaster = null;
				if (pfPaymentDTO.getVoucherNote().getForwardTo().getId() != null) {
					userMaster = userMasterRepository.findOne(pfPaymentDTO.getVoucherNote().getForwardTo().getId());
				}
				VoucherNote voucherNote = new VoucherNote();
				voucherNote.setForwardTo(userMaster);
				log.info("voucherNote forwardTo user---------------->" + voucherNote.getForwardTo().getId());
				voucherNote.setFinalApproval(pfPaymentDTO.getFinalApproval());
				voucherNote.setNote(pfPaymentDTO.getVoucherNote().getNote());
				voucherNote.setVoucher(pfPayment.getVoucher());
				voucherNoteRepository.save(voucherNote);
				log.info("<============= voucherNote table Inserted ==================>");
				VoucherLog voucherLog = new VoucherLog();
				voucherLog.setRemarks(pfPaymentDTO.getVoucherLog().getRemarks());
				voucherLog.setVoucher(pfPayment.getVoucher());
				voucherLog.setStatus(pfPaymentDTO.getVoucherLog().getStatus());
				voucherLog.setUserMaster(userMaster);
				voucherLogRepository.save(voucherLog);
				log.info("<=============== voucherLog table Inserted ==================>");
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (RestException restException) {
			log.error("PfPaymentService getRegionWisePfPaymentDetails RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception e) {
			log.error("approvePettyCashExpensePayment exception ------", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("-----approvePettyCashExpensePayment method end-------");
		return baseDTO;
	}

	public BaseDTO rejectStaffPfPayment(PfPaymentDTO pfPaymentDTO) {
		BaseDTO baseDTO = new BaseDTO();
		log.info("-----rejectStaffPfPayment method start-------");
		try {
			log.info("forward to------------------->" + pfPaymentDTO.getVoucherNote().getForwardTo());
			if (pfPaymentDTO.getId() != null) {
				PfPayment pfPayment = pfPaymentRepository.getOne(pfPaymentDTO.getId());
				if (pfPayment.getVoucher().getId() != null) {
					pfPayment.setVoucher(pfPayment.getVoucher());
					log.info("voucher id------------" + pfPayment.getVoucher());
				}
				log.info("rejectStaffPfPayment voucher id------------" + pfPayment.getVoucher().getId());

				VoucherLog voucherLog = new VoucherLog();
				voucherLog.setRemarks(pfPaymentDTO.getVoucherLog().getRemarks());
				voucherLog.setVoucher(pfPayment.getVoucher());
				voucherLog.setStatus(pfPaymentDTO.getVoucherLog().getStatus());
				if (pfPaymentDTO.getVoucherNote().getForwardTo().getId() != null) {
					voucherLog.setUserMaster(
							userMasterRepository.findOne(pfPaymentDTO.getVoucherNote().getForwardTo().getId()));
				}
				voucherLogRepository.save(voucherLog);
				log.info("<============== voucherLog table Inserted ==================>");

				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (RestException restException) {
			log.error("PfPaymentService getRegionWisePfPaymentDetails RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception e) {
			log.error("rejectPettyCashExpensePayment exception ------", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("-----rejectStaffPfPayment method end-------");
		return baseDTO;
	}

}