package in.gov.cooptex.finance.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.model.MarketingInspectionEmployee;
import in.gov.cooptex.core.accounts.model.MarketingInspectionEntity;
import in.gov.cooptex.core.accounts.model.MarketingInspectionInventory;
import in.gov.cooptex.core.accounts.model.MarketingInspectionModernization;
import in.gov.cooptex.core.accounts.model.MarketingInspectionProfitability;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.AddressMaster;
import in.gov.cooptex.core.model.BuildingType;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeePersonalInfoEmployment;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.repository.AddressMasterRepository;
import in.gov.cooptex.core.repository.BuildingTypeRepository;
import in.gov.cooptex.core.repository.DesignationRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EmployeePersonalInfoEmploymentRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.ProductVarietyMasterRepository;
import in.gov.cooptex.core.util.JdbcUtil;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.advance.repository.MarketingInspectionEmployeeRepository;
import in.gov.cooptex.finance.advance.repository.MarketingInspectionInventoryRepository;
import in.gov.cooptex.finance.advance.repository.MarketingInspectionModernizationRepository;
import in.gov.cooptex.finance.advance.repository.MarketingInspectionProfitabilityRepository;
import in.gov.cooptex.finance.advance.repository.MarketingInspectionRepository;
import in.gov.cooptex.finance.dto.MarketingAuditRequestDto;
import in.gov.cooptex.finance.dto.MarketingAuditResponseDto;
import in.gov.cooptex.finance.dto.MarketingInspectionInventoryDtlsDto;
import in.gov.cooptex.finance.dto.MarketingInspectionProfitabilityDtlsDto;
import in.gov.cooptex.finance.dto.MarketingInspectionSalesDtlsDto;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class MarketingAuditService {

	@Autowired
	EmployeeMasterRepository employeeMasterRepository;

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	MarketingInspectionRepository marketingInspectionRepository;

	@Autowired
	MarketingInspectionEmployeeRepository marketingInspectionEmployeeRepository;
	
	@Autowired
	AddressMasterRepository addressMasterRepository;

	@Autowired
	ResponseWrapper responseWrapper;

	@Autowired
	ProductVarietyMasterRepository productVarietyMasterRepository;

	@Autowired
	MarketingInspectionProfitabilityRepository marketingInspectionProfitabilityRepository;

	@Autowired
	MarketingInspectionInventoryRepository marketingInspectionInventoryRepository;

	@Autowired
	MarketingInspectionModernizationRepository marketingInspectionModernizationRepository;

	@Autowired
	BuildingTypeRepository buildingTypeRepository;

	@PersistenceContext
	EntityManager entityManager;
	
	@Autowired
	JdbcTemplate jdbcTemplate;

	public BaseDTO getEmployeeByAutoComplete(String employeeSearchName) {
		log.info("=========START MarketingAuditService.getEmployeeByAutoComplete=====");
		log.info("=========employeeSearchName is=====" + employeeSearchName);
		BaseDTO baseDto = new BaseDTO();
		try {
			if (employeeSearchName != null) {
				List<EmployeeMaster> employeeList = employeeMasterRepository
						.getEmployeeByAutoComplete(employeeSearchName);
				baseDto.setResponseContents(employeeList);
				baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			}
		} catch (Exception e) {
			log.info("=========Exception Occured in MarketingAuditService.getEmployeeByAutoComplete=====");
			log.info("=========Exception is=====" + e.getMessage());
		}
		log.info("=========END MarketingAuditService.getEmployeeByAutoComplete=====");
		return baseDto;
	}

	@Autowired
	DesignationRepository designationRepository;
	
	public BaseDTO saveOrUpdateMarkettingAudit(MarketingInspectionEntity marketingInspectionEntity) {
		log.info("=========START MarketingAuditService.saveOrUpdateMarkettingAudit=====");
		BaseDTO baseDto = new BaseDTO();
		try {
			Validate.notNullOrEmpty(marketingInspectionEntity.getInspectedBy().getId(),ErrorDescription.getError(MastersErrorCode.INSPECTION_EMP_EMPTY));
			Validate.notNullOrEmpty(marketingInspectionEntity.getInspectionDate(),ErrorDescription.getError(MastersErrorCode.INSPECTION_DATE_EMPTY));
			Validate.notNullOrEmpty(marketingInspectionEntity.getEntityMaster().getId(),ErrorDescription.getError(MastersErrorCode.INSPECTION_ENTITY_EMPTY));

			if (marketingInspectionEntity.getEntityAddress() != null
					&& marketingInspectionEntity.getEntityAddress().getId() != null) {
				log.info("=========ADDRESS Id is=====" + marketingInspectionEntity.getEntityAddress().getId());
				AddressMaster addressMaster = addressMasterRepository
						.findOne(marketingInspectionEntity.getEntityAddress().getId());
				marketingInspectionEntity.setEntityAddress(addressMaster);
			}
			log.info("=========Inspected By Id is=====" + marketingInspectionEntity.getInspectedBy().getId());
			EmployeeMaster inspectedBy = employeeMasterRepository
					.findOne(marketingInspectionEntity.getInspectedBy().getId());
			log.info("=========Entity Id is=====" + marketingInspectionEntity.getEntityMaster().getId());
			EntityMaster entityMaster = entityMasterRepository
					.findOne(marketingInspectionEntity.getEntityMaster().getId());
			marketingInspectionEntity.setEntityMaster(entityMaster);
			marketingInspectionEntity.setInspectedBy(inspectedBy);
			marketingInspectionEntity.setVersion((long) 0);
			marketingInspectionEntity = marketingInspectionRepository.save(marketingInspectionEntity);
			
			List<MarketingInspectionEmployee> marketingInspectionEmployeeList = new ArrayList<>();
			
			List<Object> employeeObjList = employeeMasterRepository.loadEmployeeListByShowroomId(entityMaster.getId());
			Iterator<?> employee = employeeObjList.iterator();
			while (employee.hasNext()) {
				Object ob[] = (Object[]) employee.next();
				MarketingInspectionEmployee marketingInspectionEmployeeObj = new MarketingInspectionEmployee();
				
				marketingInspectionEmployeeObj.setMarketingInspectionEntity(marketingInspectionEntity);
				marketingInspectionEmployeeObj
						.setShowroomEmployee(employeeMasterRepository.findOne(Long.valueOf((ob[0]).toString())));
				marketingInspectionEmployeeObj.setDesignation(designationRepository.findById(Long.valueOf((ob[6]).toString())));
				marketingInspectionEmployeeObj.setWorkingFromDate((Date)((ob[5])));
				marketingInspectionEmployeeObj.setVersion((long) 0);
				marketingInspectionEmployeeList.add(marketingInspectionEmployeeObj);
			}
			marketingInspectionEmployeeRepository.save(marketingInspectionEmployeeList);
			
//			---------------------------------

			/*List<MarketingInspectionSalesDtlsDto> marketingInspectionSalesDtlsDtoList = new ArrayList<>();
			marketingInspectionSalesDtlsDtoList.add(loadAllSalesDetailsDatatable(entityMaster.getId()));*/
			
			log.info("=========MarketingAuditEntity Saved SuccessFully=====");
			baseDto.setResponseContent(marketingInspectionEntity);
			/*baseDto.setResponseContents(marketingInspectionSalesDtlsDtoList);*/
			baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.info("=========Exception Occured in MarketingAuditService.saveOrUpdateMarkettingAudit=====",e);
			log.info("=========Exception is=====" + e.getMessage());
		}
		log.info("=========END MarketingAuditService.saveOrUpdateMarkettingAudit=====");
		return baseDto;
	}

	
	public BaseDTO loadAllSalesDetailsDatatable(Long showroomId) {
		BaseDTO baseDto = new BaseDTO();
		try {
			MarketingInspectionSalesDtlsDto marketingInspectionSalesDtlsDto = new MarketingInspectionSalesDtlsDto();
			marketingInspectionSalesDtlsDto.setSalesPerformanceList(new ArrayList<>());
			marketingInspectionSalesDtlsDto.setVarietywisePerformanceList(new ArrayList<>());
			marketingInspectionSalesDtlsDto.setVarietywiseProgressiveSalesList(new ArrayList<>());
			marketingInspectionSalesDtlsDto.setSeasonalSalesList(new ArrayList<>());
			marketingInspectionSalesDtlsDto.setGovernmentContractLocalBodySalesList(new ArrayList<>());
			marketingInspectionSalesDtlsDto.setAdditionalGovDepConDuringVisitList(new ArrayList<>());
			marketingInspectionSalesDtlsDto.setSalesDtlsCreditSalesFirstList(new ArrayList<>());
			marketingInspectionSalesDtlsDto.setSalesDtlsCreditSalesSecondList(new ArrayList<>());
			marketingInspectionSalesDtlsDto.setDepWiseDuesAndColDtlsList(new ArrayList<>());
			
			marketingInspectionSalesDtlsDto.getSalesPerformanceList()
					.addAll(commonDataFetchmethod(showroomId, "MARKETING_INSPECTION_SALES_PERFORMANCE"));

			marketingInspectionSalesDtlsDto.getVarietywisePerformanceList()
					.addAll(commonDataFetchmethod(showroomId, "VARIETYWISE_SALES_PARTICULARS"));

			marketingInspectionSalesDtlsDto.getVarietywiseProgressiveSalesList()
					.addAll(commonDataFetchmethod(showroomId, "VARIETYWISE_PROGRESSIVE_SALES"));
			
			marketingInspectionSalesDtlsDto.getSeasonalSalesList()
			.addAll(commonDataFetchmethod(showroomId, "SEASONAL_SALES"));
			
			marketingInspectionSalesDtlsDto.getGovernmentContractLocalBodySalesList()
			.addAll(commonDataFetchmethod(showroomId, "GOVERNMENT_CONTRACT_LOCALBODY_SALES"));
			
			marketingInspectionSalesDtlsDto.getAdditionalGovDepConDuringVisitList()
			.addAll(commonDataFetchmethod(showroomId, "DTLS_ADDITIONAL_GOV_DEP_CONTACTED_DURING_VISIT"));
			
			marketingInspectionSalesDtlsDto.getSalesDtlsCreditSalesFirstList()
			.addAll(commonDataFetchmethod(showroomId, "CREDIT_SALES_BASED_ON_YEARS"));
			
			marketingInspectionSalesDtlsDto.getSalesDtlsCreditSalesSecondList()
			.addAll(commonDataFetchmethod(showroomId, "CREDIT_SALES_BASED_OUTSTANDING_AMOUNT"));

			marketingInspectionSalesDtlsDto.getDepWiseDuesAndColDtlsList()
					.addAll(commonDataFetchmethod(showroomId, "DEPT_WISE_DUES_COLLECTION_DETAILS"));

			baseDto.setResponseContent(marketingInspectionSalesDtlsDto);
			baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			
		} catch (Exception e) {
			log.info("=========Exception Occured in MarketingAuditService.loadAllSalesDetailsDatatable=====", e);
			log.info("=========Exception is=====" + e.getMessage());
		}

		return baseDto;
	}
	
	
	public BaseDTO loadAllInventoryDtlsTable(Long showroomId) {
		BaseDTO baseDto = new BaseDTO();
		try {
			MarketingInspectionInventoryDtlsDto marketingInspectionInventoryDtlsDto = new MarketingInspectionInventoryDtlsDto();
			marketingInspectionInventoryDtlsDto.setInventoryTurnoverList(new ArrayList<>()); 
			marketingInspectionInventoryDtlsDto.setStockPositionDateInspectionList(new ArrayList<>());
			marketingInspectionInventoryDtlsDto.setFastMovingVarietiesDetailsList(new ArrayList<>());
			marketingInspectionInventoryDtlsDto.setSlowMovingStagnatedVarietiesList(new ArrayList<>());
			marketingInspectionInventoryDtlsDto.getInventoryTurnoverList()
					.addAll(commonDataFetchmethod(showroomId, "MARKETINGAUDIT_INVENTORY_TURN_OVER"));

			marketingInspectionInventoryDtlsDto.getStockPositionDateInspectionList()
					.addAll(commonDataFetchmethod(showroomId, "MARKETINGAUDIT_STOCK_POSITION_DATE_INSPECTION"));

			marketingInspectionInventoryDtlsDto.getFastMovingVarietiesDetailsList()
					.addAll(commonDataFetchmethod(showroomId, "MARKETINGAUDIT_FASTMOVING_VARIETIES_DETAILS"));

			marketingInspectionInventoryDtlsDto.getSlowMovingStagnatedVarietiesList()
					.addAll(commonDataFetchmethod(showroomId, "MARKETINGAUDIT_SLOWMOVING_STAGNATED_VARIETIES"));

			baseDto.setResponseContent(marketingInspectionInventoryDtlsDto);
			baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());

		} catch (Exception e) {
			log.info("=========Exception Occured in MarketingAuditService.loadAllInventoryDtlsTable=====", e);
			log.info("=========Exception is=====" + e.getMessage());
		}

		return baseDto;
	}
	
	public BaseDTO loadProfitabilityDtlsTable(Long showroomId) {
		BaseDTO baseDto = new BaseDTO();
		try {
			MarketingInspectionProfitabilityDtlsDto marketingInspectionProfitabilityDtlsDto = new MarketingInspectionProfitabilityDtlsDto();
//			marketingInspectionProfitabilityDtlsDto.setProfitabilityLatComplFinYr(new Map<K, V>());
			marketingInspectionProfitabilityDtlsDto.setProfitabilityLatComplFinYrList(new ArrayList<>());
			
			marketingInspectionProfitabilityDtlsDto.getProfitabilityLatComplFinYrList()
					.addAll(commonDataFetchmethod(showroomId, "PROFITABILITY_LATEST_COMPLETED_FINYEAR"));

			baseDto.setResponseContent(marketingInspectionProfitabilityDtlsDto);
			baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());

		} catch (Exception e) {
			log.info("=========Exception Occured in MarketingAuditService.loadAllInventoryDtlsTable=====", e);
			log.info("=========Exception is=====" + e.getMessage());
		}

		return baseDto;
	}
	private List<Map<String, Object>> commonDataFetchmethod(Long showroomId, String queryName) throws Exception{
		String queryContent = JdbcUtil.getApplicationQuery(jdbcTemplate, queryName);
		queryContent=queryContent.trim().replace(":entityID", showroomId.toString());
		List<Map<String, Object>> mapList = jdbcTemplate.queryForList(queryContent);
		return mapList;
	}

	public BaseDTO getAllShowroomForRegion(Long regionId) {
		log.info("getAllShowroomForRegion regionId [" + regionId + "]");
		BaseDTO response = new BaseDTO();
		try {
			List<EntityMaster> showroomListForRegion = entityMasterRepository.getAllShowroomForRegion(regionId);
			if (showroomListForRegion != null) {
				log.info("getAllShowroomForRegion :: showroomListForRegion.size==> " + showroomListForRegion.size());
			} else {
				log.error("Showroom not found");
			}
			response.setResponseContent(showroomListForRegion);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());

		} catch (Exception e) {
			log.error("getAllShowroomForRegion ", e);
			response.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(response);
	}

	/**
	 * @return
	 */
	public BaseDTO getAllActiveRegions() {
		log.info("<--- Get all active regions service called --->");
		BaseDTO response = new BaseDTO();
		try {
			List<EntityMaster> regionList = entityMasterRepository.findActiveRegionalOffices();
			List<EntityMaster> responseList = new ArrayList<>();
			for (EntityMaster reg : regionList) {
				/*
				 * reg.getCreatedBy().setRegion(null); if (reg.getModifiedBy() != null)
				 * reg.getModifiedBy().setRegion(null);
				 */
				reg.setEntityTypeMaster(null);
				responseList.add(reg);
			}
			log.info("<---Region list size--->" + responseList.size());
			response.setResponseContents(responseList);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.error("Error while retiving active regions based on state----->", e);
			response.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(response);
	}

	public BaseDTO getProductVarietyNameAutoComplete(String productName) {
		log.info("=======START MarketingAuditService.getProductVarietyNameAutoComplete======");
		log.info("=======productName is======" + productName);
		BaseDTO baseDto = new BaseDTO();
		try {
			if (productName != null) {
				List<ProductVarietyMaster> productVarietyMasterList = productVarietyMasterRepository
						.getProductByCodeOrNameAutoComplete(productName);
				baseDto.setResponseContents(productVarietyMasterList);
				baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			}
		} catch (Exception e) {
			log.info("=======Exception Occured in MarketingAuditService.getProductVarietyNameAutoComplete======");
			log.info("=======Exception is======" + e.getMessage());
		}
		log.info("=======END MarketingAuditService.getProductVarietyNameAutoComplete======");
		return baseDto;
	}

	public BaseDTO saveOrUpdateProfitability(MarketingInspectionProfitability marketingInspectionProfitability) {
		log.info("========START MarketingAuditService.saveOrUpdateProfitability========");
		BaseDTO baseDto = new BaseDTO();
		try {
			if (marketingInspectionProfitability.getMarketingInspectionEntity() != null
					&& marketingInspectionProfitability.getMarketingInspectionEntity().getId() != null) {
				 MarketingInspectionEntity marketingInspectionEntity=marketingInspectionRepository.findOne(marketingInspectionProfitability.getMarketingInspectionEntity().getId());
				 marketingInspectionProfitability.setMarketingInspectionEntity(marketingInspectionEntity);
			}
			
			marketingInspectionProfitability.setVersion((long) 0);
			marketingInspectionProfitability = marketingInspectionProfitabilityRepository.save(marketingInspectionProfitability);
			baseDto.setResponseContent(marketingInspectionProfitability);
			baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.info("========Exception Occured in MarketingAuditService.saveOrUpdateProfitability========",e);
			log.info("========Exception is========" + e.getMessage());
		}
		log.info("========END MarketingAuditService.saveOrUpdateProfitability========");

		return baseDto;
	}

	public BaseDTO saveOrUpdateMarketingInspectionInventory(MarketingInspectionInventory marketingInspectionInventory) {
		log.info("=======START MarketingAuditService.saveOrUpdateMarketingInspectionInventory======");
		BaseDTO baseDto = new BaseDTO();
		try {
			if (marketingInspectionInventory.getMarketingInspectionEntity() != null
					&& marketingInspectionInventory.getMarketingInspectionEntity().getId() != null) {
				MarketingInspectionEntity marketingInspectionEntity=marketingInspectionRepository.findOne(marketingInspectionInventory.getMarketingInspectionEntity().getId());
				 marketingInspectionInventory.setMarketingInspectionEntity(marketingInspectionEntity);
			}
			marketingInspectionInventory.setVersion((long) 0);
			marketingInspectionInventory = marketingInspectionInventoryRepository.save(marketingInspectionInventory);
			baseDto.setResponseContent(marketingInspectionInventory);
			baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.info(
					"=======Exception Occured in MarketingAuditService.saveOrUpdateMarketingAuditStockArrangement======");
			log.info("=======Exception is======" + e.getMessage());
		}
		log.info("=======END MarketingAuditService.saveOrUpdateMarketingAuditStockArrangement======");
		return baseDto;
	}

	public BaseDTO saveOrUpdateModernization(MarketingInspectionModernization marketingInspectionModernization) {
		log.info("========START MarketingAuditService.saveOrUpdateModernization========");
		BaseDTO baseDto = new BaseDTO();
		try {
			if (marketingInspectionModernization.getMarketingInspectionEntity() != null
					&& marketingInspectionModernization.getMarketingInspectionEntity().getId() != null) {
				 MarketingInspectionEntity marketingInspectionEntity=marketingInspectionRepository.findOne(marketingInspectionModernization.getMarketingInspectionEntity().getId());
				 marketingInspectionModernization.setMarketingInspectionEntity(marketingInspectionEntity);
			}
			
			marketingInspectionModernization.setVersion((long) 0);
			marketingInspectionModernization = marketingInspectionModernizationRepository.save(marketingInspectionModernization);
			baseDto.setResponseContent(marketingInspectionModernization);
			baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.info("========Exception Occured in MarketingAuditService.saveOrUpdateModernization========");
			log.info("========Exception is========" + e.getMessage());
		}
		log.info("========END MarketingAuditService.saveOrUpdateModernization========");

		return baseDto;
	}

	public BaseDTO getAllBuildType() {
		log.info("========START MarketingAuditService.getAllBuildType========");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<BuildingType> listBuildingType = buildingTypeRepository.getAll();
			baseDTO.setResponseContents(listBuildingType);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("<<======== ERROR BuildingTypeService---- getAll ::" + e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("========END MarketingAuditService.getAllBuildType========");
		return baseDTO;
	}

	public BaseDTO getLazyLoadData(PaginationDTO paginationDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {

			log.info("========START MarketingAuditService.getLazyLoadData========");
			Session session = entityManager.unwrap(Session.class);
			Criteria criteria = session.createCriteria(MarketingInspectionEntity.class, "marketingInspectionEntity");
			criteria.createAlias("marketingInspectionEntity.inspectedBy", "inspectBy");
			criteria.createAlias("marketingInspectionEntity.entityMaster", "region");
			log.info(":: Criteria search started ::");
			if (paginationDTO.getFilters() != null) {

				if (paginationDTO.getFilters().get("lastDateVisit") != null) {
					Date date = new Date((long) paginationDTO.getFilters().get("lastDateVisit"));
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = dateFormat.format(date);
					Date minDate = dateFormat.parse(strDate);
					Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
					criteria.add(Restrictions.conjunction()
							.add(Restrictions.ge("marketingInspectionEntity.lastDateVisit", minDate))
							.add(Restrictions.lt("marketingInspectionEntity.lastDateVisit", maxDate)));
				}

				if (paginationDTO.getFilters().get("inspectionDate") != null) {
					Date date = new Date((long) paginationDTO.getFilters().get("inspectionDate"));
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = dateFormat.format(date);
					Date minDate = dateFormat.parse(strDate);
					Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
					criteria.add(Restrictions.conjunction()
							.add(Restrictions.ge("marketingInspectionEntity.inspectionDate", minDate))
							.add(Restrictions.lt("marketingInspectionEntity.inspectionDate", maxDate)));
				}

				String entityMasterCode = (String) paginationDTO.getFilters().get("entityMasterCode");
				if (entityMasterCode != null) {
					if (AppUtil.isInteger(entityMasterCode)) {
						EntityMaster entityMasterCodeValue = entityMasterRepository
								.findByCode(Integer.parseInt(entityMasterCode));
						criteria.add(Restrictions.sqlRestriction("cast(this_.entity_id  as varchar) like '%"
								+ entityMasterCodeValue.getId().toString().trim() + "%'"));
					} else {
						criteria.add(Restrictions.or(
								Restrictions.like("region.name", "%" + entityMasterCode.trim() + "%").ignoreCase()));
					}
				}
				// ----
				String inspectBy = (String) paginationDTO.getFilters().get("inspectBy");
				if (inspectBy != null) {
					criteria.add(Restrictions.or(
							Restrictions.like("inspectBy.firstName", "%" + inspectBy.trim() + "%").ignoreCase(),
							Restrictions.like("inspectBy.lastName", "%" + inspectBy.trim() + "%").ignoreCase(),
							Restrictions.like("inspectBy.empCode", "%" + inspectBy.trim() + "%").ignoreCase())

					);
				}

				String lName = (String) paginationDTO.getFilters().get("locName");
				if (lName != null) {
					criteria.add(
							Restrictions.like("marketingInspectionEntity.locName", "%" + lName.trim() + "%").ignoreCase());
				}

				if (paginationDTO.getFilters().get("status") != null) {
					Boolean status = Boolean.parseBoolean(paginationDTO.getFilters().get("status").toString());
					criteria.add(Restrictions.eq("marketingInspectionEntity.status", status));
				}

				if (paginationDTO.getFilters().get("createdDate") != null) {
					Date date = new Date((long) paginationDTO.getFilters().get("createdDate"));
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = dateFormat.format(date);
					Date minDate = dateFormat.parse(strDate);
					Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
					criteria.add(
							Restrictions.conjunction().add(Restrictions.ge("marketingInspectionEntity.createdDate", minDate))
									.add(Restrictions.lt("marketingInspectionEntity.createdDate", maxDate)));
				}

				criteria.setProjection(Projections.rowCount());
				Integer totalResult = ((Long) criteria.uniqueResult()).intValue();
				criteria.setProjection(null);

				ProjectionList projectionList = Projections.projectionList();
				projectionList.add(Projections.property("id"));
				projectionList.add(Projections.property("lastDateVisit"));
				projectionList.add(Projections.property("entityMaster"));
				projectionList.add(Projections.property("inspectionDate"));
				projectionList.add(Projections.property("createdDate"));

				criteria.setProjection(projectionList);

				if (paginationDTO.getFirst() != null) {
					Integer pageNo = paginationDTO.getFirst();
					Integer pageSize = paginationDTO.getPageSize();
					if (pageNo != null && pageSize != null) {
						criteria.setFirstResult(pageNo * pageSize);
						criteria.setMaxResults(pageSize);
						log.info("PageNo : [" + pageNo + "] pageSize[" + pageSize + "]");
					}

					String sortField = paginationDTO.getSortField();
					String sortOrder = paginationDTO.getSortOrder();
					log.info("sortField outside : [" + sortField + "] sortOrder[" + sortOrder + "]");
					if (paginationDTO.getSortField() != null && paginationDTO.getSortOrder() != null) {
						log.info("sortField : [" + paginationDTO.getSortField() + "] sortOrder["
								+ paginationDTO.getSortOrder() + "]");

						if (paginationDTO.getSortField().equals("inspectedDate")) {
							sortField = "marketingInspectionEntity.inspectionDate";
						} else if (sortField.equals("lastDateVisit")) {
							sortField = "marketingInspectionEntity.lastDateVisit";
						} else if (sortField.equals("entityMasterCode")) {
							sortField = "entityMasterCode.code";
							sortField = "entityMasterCode.name";
						} else if (sortField.equals("id")) {
							sortField = "marketingInspectionEntity.id";
						}
						if (sortOrder.equals("DESCENDING")) {
							criteria.addOrder(Order.desc(sortField));
						} else {
							criteria.addOrder(Order.asc(sortField));
						}
					} else {
						criteria.addOrder(Order.desc("marketingInspectionEntity.modifiedDate"));
					}
				}
				List<?> resultList = criteria.list();
				log.info("criteria list executed and the list size is : " + resultList.size());
				if (resultList == null || resultList.isEmpty() || resultList.size() == 0) {
					log.info("marketingInspectionEntity List is null or empty ");
				}
				List<MarketingInspectionEntity> marketingInspectionEntityList = new ArrayList<>();
				Iterator<?> it = resultList.iterator();
				while (it.hasNext()) {
					Object ob[] = (Object[]) it.next();
					MarketingInspectionEntity response = new MarketingInspectionEntity();
					/*"id","lastDateVisit","entityMaster","inspectionDate","createdDate"*/
					response.setId((Long) ob[0]);
					log.info("Id::::::::" + response.getId());
//					EmployeeMaster employeeMaster = (EmployeeMaster) ob[1];
					EntityMaster entityMaster = (EntityMaster) ob[2];
					response.setLastDateVisit((Date) ob[1]);
					response.setEntityMaster(entityMaster);
					response.setInspectionDate((Date) ob[3]);
					response.setCreatedDate((Date) ob[4]);
					log.info(":: List Response ::" + response);
					marketingInspectionEntityList.add(response);
				}
				baseDTO.setResponseContents(marketingInspectionEntityList);
				baseDTO.setTotalRecords(totalResult);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (Exception exp) {
			log.error("Exception Cause : ", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("========END MarketingAuditService.getLazyLoadData========");
		return baseDTO;
	}

	public BaseDTO getMarketingAuditById(Long id) {
		log.info("========START MarketingAuditService.getMarketingAuditById========");
		log.info("========Id is========" + id);
		BaseDTO baseDto = new BaseDTO();
		MarketingAuditResponseDto marketingAuditResponseDto = new MarketingAuditResponseDto();
		try {
			if (id != null) {
				 MarketingInspectionEntity marketingInspectionEntity=marketingInspectionRepository.findOne(id);
				MarketingInspectionInventory marketingInspectionInventory = marketingInspectionInventoryRepository
						.findByMarketinInspectionId(id);
				MarketingInspectionModernization marketingInspectionModernization = marketingInspectionModernizationRepository.findByMarketinInspectionId(id);
				MarketingInspectionProfitability marketingInspectionProfitability = marketingInspectionProfitabilityRepository.findByMarketinInspectionId(id);
				
				marketingAuditResponseDto.setMarketingInspectionEntity(marketingInspectionEntity);
				marketingAuditResponseDto.setMarketingInspectionInventory(marketingInspectionInventory);
				marketingAuditResponseDto.setMarketingInspectionModernization(marketingInspectionModernization);
				marketingAuditResponseDto.setMarketingInspectionProfitability(marketingInspectionProfitability);

				
				if (marketingInspectionEntity.getEntityMaster() != null
						&& marketingInspectionEntity.getEntityMaster().getEntityMasterRegion() != null
						&& marketingInspectionEntity.getEntityMaster().getEntityMasterRegion().getId() != null) {
					EntityMaster entityMaster = entityMasterRepository
							.findOne(marketingInspectionEntity.getEntityMaster().getEntityMasterRegion().getId());
					marketingAuditResponseDto.setEntityMaster(entityMaster);
				}
				 
				log.info("========marketingAuditEntity is========" + marketingAuditResponseDto.toString());
				baseDto.setResponseContent(marketingAuditResponseDto);
				baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			} else {
				throw new Exception("Id Cannot Be Empty");
			}

		} catch (Exception e) {
			log.info("========Exception Occured in MarketingAuditService.getMarketingAuditById========");
			log.info("========Exception is========" + e.getMessage());
		}
		log.info("========End MarketingAuditService.getMarketingAuditById========");
		return baseDto;
	}

	// ---------------------------------------
	@Autowired
	EmployeePersonalInfoEmploymentRepository employeePersonalInfoEmploymentRepository;

	public BaseDTO getAllEmplistbyShowroomId(Long showroomId) {
		log.info("getAllEmplistbyShowroomId showroomId [" + showroomId + "]");
		BaseDTO response = new BaseDTO();
		try {
			List<EmployeeMaster> employeeMasterList = new ArrayList<>();
			List<Object> employeeObjList = employeeMasterRepository.loadEmployeeListByShowroomId(showroomId);
			Iterator<?> employee = employeeObjList.iterator();
			while (employee.hasNext()) {
				Object ob[] = (Object[]) employee.next();
				EmployeeMaster employeeMasterObj = new EmployeeMaster();
				EmployeePersonalInfoEmployment employeePersonalInfoEmployment = new EmployeePersonalInfoEmployment();
				Designation designation = new Designation();
				employeeMasterObj.setId(Long.valueOf((ob[0]).toString()));
				employeeMasterObj.setFirstName((String) ob[1]);
				employeeMasterObj.setLastName((String) ob[2]);
				employeeMasterObj.setEmpCode((String) ob[3]);
				employeeMasterObj.setMiddleName((String) ob[4]);
				employeePersonalInfoEmployment.setDateOfJoining((Date)((ob[5])));
				designation.setId(Long.valueOf((ob[6]).toString()));
				designation.setName((String) ob[7]);
				employeePersonalInfoEmployment.setDesignation(designation);
				employeeMasterObj.setPersonalInfoEmployment(employeePersonalInfoEmployment);
				employeeMasterList.add(employeeMasterObj);
			}
			log.info("getAllEmplistbyShowroomId :: getAllEmplistbyShowroomId.size==> " + employeeMasterList.size());
			response.setResponseContents(employeeMasterList);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());

		} catch (Exception e) {
			log.error("getAllEmplistbyShowroomId ", e);
			response.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(response);
	}
}
