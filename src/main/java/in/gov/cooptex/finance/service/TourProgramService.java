package in.gov.cooptex.finance.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import in.gov.cooptex.common.service.EntityMasterService;
import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.service.NotificationEmailService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.model.TourProgramLog;
import in.gov.cooptex.core.accounts.model.TourProgramNote;
import in.gov.cooptex.core.accounts.repository.EntityBankBranchRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.finance.service.OperationService;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.SystemNotification;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.AmountTransferRepository;
import in.gov.cooptex.core.repository.AppQueryRepository;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EmployeePersonalInfoEmploymentRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.SystemNotificationRepository;
import in.gov.cooptex.core.repository.TourProgramDetailsRepository;
import in.gov.cooptex.core.repository.TourProgramLogRepository;
import in.gov.cooptex.core.repository.TourProgramNoteRepository;
import in.gov.cooptex.core.repository.TourProgramRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.ui.EntityType;
import in.gov.cooptex.core.utilities.TourProgramConstant;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.finance.TourProgramResponseDTO;
import in.gov.cooptex.finance.TourProgramSaveDto;
import in.gov.cooptex.operation.model.TourProgram;
import in.gov.cooptex.operation.model.TourProgramDetails;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class TourProgramService {

	@Autowired
	private EntityManager em;

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private AmountTransferRepository amountTransferRepository;

	@Autowired
	private TourProgramRepository tourProgramRepository;

	@Autowired
	private TourProgramLogRepository tourProgramLogRepository;

	@Autowired
	private TourProgramNoteRepository tourProgramNoteRepository;

	@Autowired
	private TourProgramDetailsRepository tourProgramDetailsRepository;

	@Autowired
	SystemNotificationRepository systemNotificationRepository;

	@Autowired
	private EntityMasterRepository entityMasterRepository;

	@Autowired
	private UserMasterRepository userMasterRepository;

	@Autowired
	EmployeeMasterRepository employeeMasterRepository;

	@Autowired
	EntityBankBranchRepository entityBankBranchRepository;

	@Autowired
	EntityMasterService entityMasterService;

	@Autowired
	LoginService loginService;

	@Autowired
	EmployeePersonalInfoEmploymentRepository employeePersonalInfoEmploymentRepository;

	@Autowired
	OperationService operationService;

	@Autowired
	ResponseWrapper responseWrapper;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	@Autowired
	AppQueryRepository appQueryRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	NotificationEmailService notificationEmailService;

	private static final String TOUR_PROGRAME_VIEW_URL = "/pages/accounts/tourProgram/viewTentativeTourProgram.xhtml?faces-redirect=true";

	public BaseDTO getTourProgramDetails(PaginationDTO request) {
		log.info("<<====   TourProgramService ---  getTourProgramDetails ====## STARTS");
		BaseDTO baseDTO = null;
		Integer total = 0;
		List<TourProgramResponseDTO> resultList = null;
		try {
			resultList = new ArrayList<TourProgramResponseDTO>();
			baseDTO = new BaseDTO();
			Integer start = request.getFirst(), pageSize = request.getPageSize();
			start = start * pageSize;

			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();
			ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("TENTATIVE_TOUR_PROGRAM_LIST");
			String mainQuery = applicationQuery.getQueryContent();
			log.info("db query----------" + mainQuery);

			String searchcriteria = "";

			UserMaster userMaster = loginService.getCurrentUser();
			Long loginUserId = userMaster == null ? null : userMaster.getId();

			EntityMaster workLocation = entityMasterRepository.getEntityInfoByLoggedInUser(loginUserId);
			EntityTypeMaster entityTypeMaster = workLocation == null ? null : workLocation.getEntityTypeMaster();
			String entityTypeCode = entityTypeMaster == null ? null : entityTypeMaster.getEntityCode();
			Long workLocationId = workLocation != null ? workLocation.getId() : null;
			
			if(!EntityType.HEAD_OFFICE.equals(entityTypeCode)) {
				if (workLocationId != null) {
					searchcriteria = "and tp.entity_id = " + workLocationId + "";
				}
			}
			
			if (request.getFilters() != null) {
				if (request.getFilters().get("tourName") != null) {
					searchcriteria += " and upper(concat(tp.tour_name)) like upper('%"
							+ request.getFilters().get("tourName") + "%')";
				}
				if (request.getFilters().get("month") != null) {
					searchcriteria += " and upper(concat(tp.program_month)) like upper('%"
							+ request.getFilters().get("month") + "%')";
				}
				if (request.getFilters().get("year") != null) {
					searchcriteria += " and tp.program_year >=" + request.getFilters().get("year");
				}
				if (request.getFilters().get("entityMaster.name") != null) {
					searchcriteria += " and upper(concat(em.name)) like upper('%"
							+ request.getFilters().get("entityMaster.name") + "%')";
				}
				if (request.getFilters().get("status") != null) {
					searchcriteria += " and tpl.stage ='" + request.getFilters().get("status") + "'";
				}

				mainQuery = mainQuery.replace(":searchcriteria", searchcriteria);

			}
//			String countQuery = mainQuery.replace(
//					"SELECT tp.id,tp.tour_name as tourName,tp.program_month,tp.program_year,em.name as entityName,tpl.stage,tpn.note,tpn.final_approval,tpn.forward_to",
//					"SELECT count(distinct(tp.id)) as count ");
//			countQuery = countQuery.replace(
//					"GROUP BY tp.id,tp.tour_name,tp.program_month,tp.program_year,em.name,tpl.stage,tpn.note,tpn.final_approval,tpn.forward_to",
//					" ");

			String countQuery = "select count(*) as count from (" + mainQuery + ")t";
			log.info("count query... " + countQuery);
			countData = jdbcTemplate.queryForList(countQuery);
			if (countData != null && !countData.isEmpty()) {
				for (Map<String, Object> data : countData) {
					if (data.get("count") != null)
						total = Integer.parseInt(data.get("count").toString().replace(",", ""));
				}
			}
			/*
			 * for (Map<String, Object> data : countData) { if (data.get("count") != null) {
			 * //total = Integer.parseInt(data.get("count").toString().replace(",", "")); }
			 * }
			 */
			log.info("total query... " + total + "::pageSize::>>>" + pageSize + "::start::" + start);

			if (request.getSortField() != null && request.getSortOrder() != null) {
				log.info("Sort Field:[" + request.getSortField() + "] Sort Order:[" + request.getSortOrder() + "]");
				if (request.getSortField().equals("tourName") && request.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by tp.tour_name asc  ";
				}
				if (request.getSortField().equals("tourName") && request.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by tp.tour_name desc  ";
				}
				if (request.getSortField().equals("month") && request.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by tp.program_month asc  ";
				}
				if (request.getSortField().equals("month") && request.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by tp.program_month desc  ";
				}
				if (request.getSortField().equals("year") && request.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by tp.program_year asc ";
				}
				if (request.getSortField().equals("year") && request.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by tp.program_year desc ";
				}
				if (request.getSortField().equals("entityMaster.name") && request.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by em.name asc ";
				}
				if (request.getSortField().equals("entityMaster.name") && request.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by em.name desc ";
				}
				if (request.getSortField().equals("status") && request.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by tpl.stage asc ";
				}
				if (request.getSortField().equals("status") && request.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by tpl.stage desc ";
				}
				mainQuery += " limit " + pageSize + " offset " + start + ";";
			} else {
				mainQuery += " order by tp.id desc limit " + pageSize + " offset " + start + ";";
			}
			log.info("filter query----------" + mainQuery);
			// mainQuery += "order by att.created_date desc";
			listofData = jdbcTemplate.queryForList(mainQuery);
			// totalRecords = listofData.size();
			log.info("TourPrograms list size-------------->" + listofData.size());

			for (Map<String, Object> data : listofData) {

				TourProgramResponseDTO response = new TourProgramResponseDTO();
				EntityMaster entityMaster = new EntityMaster();
				entityMaster.setName(data.get("name").toString());
				response.setId(Long.valueOf(data.get("id").toString()));
				response.setTourName(data.get("tourname").toString());
				response.setMonth(data.get("program_month").toString());
				response.setYear(Long.valueOf(data.get("program_year").toString()));
				response.setEntityMaster(entityMaster);
				response.setStatus(data.get("stage").toString());
				response.setForwardTo(Long.valueOf(data.get("forward_to").toString()));
				response.setNote(data.get("note").toString());
				response.setForwardFor(Boolean.valueOf(data.get("final_approval").toString()));
				resultList.add(response);
			}
			log.info("final list size---------------" + resultList.size());
			log.info("Total records present in BankToCash..." + total);
			baseDTO.setTotalRecords(total);
			// baseDTO.setTotalRecords(totalRecords);
			baseDTO.setResponseContents(resultList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("<<====   BanktoCashService ---  getAll ====## ENDS");
		} catch (Exception ex) {
			log.error("inside lazy method exception------> " + ex);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;

	}

	@Transactional
	public BaseDTO saveTourProgramDetails(TourProgramSaveDto data) {
		BaseDTO baseDTO = new BaseDTO();
		TourProgramLog result = new TourProgramLog();
		log.info("<==== Starts TourProgramService.save Function Entered =====>");
		TourProgram tourProgram = new TourProgram();
		TourProgramNote note = new TourProgramNote();
		try {
			UserMaster user = loginService.getCurrentUser();
			BaseDTO basedto = entityMasterService.getEntityInfoByLoggedInUser(user.getId());
			EntityMaster entity = (EntityMaster) basedto.getResponseContent();
			if (entity != null) {
				data.setEntityId(entity.getId());
				log.info("entityId:" + entity.getId());
			}
			if (data != null) {

				if (data.getTourProgram().getId() != null) {
					log.info("tourProgramId:" + data.getTourProgram().getId());
					tourProgram = tourProgramRepository.findOne(data.getTourProgram().getId());
				}

				tourProgram.setEmployeeId(data.getTourProgram().getEmployeeId());
				tourProgram.setTourName(data.getTourProgram().getTourName());
				tourProgram.setProgramMonth(data.getTourProgram().getProgramMonth());
				tourProgram.setProgramYear(data.getTourProgram().getProgramYear());
				tourProgram.setEntityMaster(entity);
				tourProgram.setStatusCode("Active");

				try {
					tourProgram = tourProgramRepository.save(tourProgram);
				} catch (Exception e) {
					log.info("tour Prgoram saved successfully:" + tourProgram);
				}

				log.info("tour Prgoram details list size:" + data.getTourProgramDetailsList().size());

				for (TourProgramDetails tourDeatils : data.getTourProgramDetailsList()) {
					TourProgramDetails tourProgramDetail = new TourProgramDetails();
					log.info("<<<--- Tour Program details....." + tourDeatils);
					if (tourDeatils.getProgramDate() != null && tourDeatils.getPlannedVisitTypeId() != null
							&& tourDeatils.getInspectionTypePlanned() != null
							&& tourDeatils.getPlaceOfVisitPlanned() != null
							&& (tourDeatils.getVisitOfficeNamePlanned() != null
									|| tourDeatils.getPlannedEntity() != null)
							&& tourDeatils.getVisitPurposePlanned() != null) {

						if (tourDeatils.getId() != null) {
							tourProgramDetail = tourProgramDetailsRepository.findOne(tourDeatils.getId());
						}
						tourProgramDetail.setProgramDate(tourDeatils.getProgramDate());
						tourProgramDetail.setPlaceOfVisitPlanned(tourDeatils.getPlaceOfVisitPlanned().trim());
						tourProgramDetail.setPlannedVisitTypeId(tourDeatils.getPlannedVisitTypeId());
						tourProgramDetail.setInspectionTypePlanned(tourDeatils.getInspectionTypePlanned().trim());
						// tourProgramDetail.setVisitOfficeNamePlanned(tourDeatils.getVisitOfficeNamePlanned().trim());
						tourProgramDetail.setVisitPurposePlanned(tourDeatils.getVisitPurposePlanned().trim());
						tourProgramDetail.setTourProgram(tourProgram);

						if (tourDeatils.getPlannedEntity() != null && tourDeatils.getPlaceOfVisitPlanned() != null
								&& tourDeatils.getPlaceOfVisitPlanned().trim().equalsIgnoreCase(
										TourProgramConstant.TOUR_PLACE_OF_VISIT_INTRAOFFICE.toString())) {
							tourProgramDetail.setPlannedEntity(
									entityMasterRepository.findOne(tourDeatils.getPlannedEntity().getId()));
						}

						try {
							if (tourDeatils.getPlannedEntity() != null) {
								tourProgramDetail.setPlannedEntity(
										entityMasterRepository.findOne(tourDeatils.getPlannedEntity().getId()));
								tourProgramDetail.setVisitOfficeNamePlanned(null);
							} else {
								tourProgramDetail
										.setVisitOfficeNamePlanned(tourDeatils.getVisitOfficeNamePlanned().trim());
								tourProgramDetail.setPlannedEntity(null);
							}

							tourProgramDetailsRepository.save(tourProgramDetail);
							log.info("<---- tourProgramDetails saved successfully:");
						} catch (Exception ex) {
							log.info("<-- TourProgramDetails Table Service/ exception :- -->" + ex);
						}
					} else {
						log.info("<-- Some empty records in list :- -->");
					}
				}

				if (data.getRemoveTourProgramDetailsList() != null) {
					for (TourProgramDetails tourDeatils : data.getRemoveTourProgramDetailsList()) {
						try {
							log.info("DeleteTourProgramDetails  removedId:" + tourDeatils.getId());
							if (tourDeatils.getId() != null) {
								tourProgramDetailsRepository.deleteTourProgramDetailsById(tourDeatils.getId());
								log.info("Delete TourProgramDetails successfully");
							} else {
								log.info("Tour Detail Id is null");
							}
						} catch (Exception ex) {
							log.info("<-- Delete TourProgram Detail list delete Service/ exception :--->" + ex);
						}
					}
				}

				// UserMaster userMaster =
				// userMasterRepository.getOne(data.getForwardTo());
				UserMaster userMaster = null;
				if (data.getForwardToUser() != null && data.getForwardToUser().getId() != null) {
					userMaster = userMasterRepository.getOne(data.getForwardToUser().getId());
				}
				if (data.getNote() != null) {

					log.info("tourProgram :" + tourProgram);
					note.setTourProgram(tourProgram);
					note.setNote(data.getNote());
					note.setFinalApproval(data.getForwardFor());
					note.setForwardTo(userMaster);
					try {
						note = tourProgramNoteRepository.save(note);
						log.info("TourProgram note saved successfully " + note);
					} catch (Exception ex) {
						log.info("<-- TourProgramNote Table Save / exception :- -->" + ex);
					}
				}

				TourProgramLog tourProgramLog = new TourProgramLog();
				tourProgramLog.setTourProgram(tourProgram);
				tourProgramLog.setStage(TourProgramConstant.TTP_SUBMITTED);
				tourProgramLog.setCreatedBy(loginService.getCurrentUser());
				tourProgramLog.setCreatedByName(loginService.getCurrentUser().getUsername());
				tourProgramLog.setCreatedDate(new Date());
				if (data.getRemarks() != null) {
					tourProgramLog.setRemarks(data.getRemarks());
				}
				if (data.getVersion() != null) {
					tourProgramLog.setVersion(data.getVersion());
				}
				try {
					tourProgramLog = tourProgramLogRepository.save(tourProgramLog);
					log.info("TourProgram Log saved successfully " + tourProgramLog);
				} catch (Exception ex) {
					log.info("<-- TourProgramLog Table Save / exception :- -->" + ex);
				}

				result = tourProgramLog;
				/*
				 * Send Notification Mail
				 */

				Map<String, Object> additionalData = new HashMap<>();
				additionalData.put("Url", TOUR_PROGRAME_VIEW_URL + "&tourProgramId=" + tourProgram.getId() + "&");
				notificationEmailService.sendMailAndNotificationForForward(note, tourProgramLog, additionalData);
				baseDTO.setResponseContent(result);
				baseDTO.setTotalRecords(1);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

			} else {
				log.info("<-- Empty of TourProgramSaveDto/ exception :- -->");
			}
		} catch (Exception ex) {
			log.info("<-- Exception in TourProgramService/ exception :- -->" + ex);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			return baseDTO;
		}
		return baseDTO;
	}

	public BaseDTO saveDeeviationTourProgramDetails(TourProgramSaveDto data) {
		BaseDTO baseDTO = new BaseDTO();
		TourProgramLog result = new TourProgramLog();
		log.info("<==== Starts TourProgramService.save Function Entered =====>");
		TourProgram tourProgram = new TourProgram();
		TourProgramNote note = new TourProgramNote();
		try {
			UserMaster user = loginService.getCurrentUser();
			BaseDTO basedto = entityMasterService.getEntityInfoByLoggedInUser(user.getId());
			EntityMaster entity = (EntityMaster) basedto.getResponseContent();
			if (entity != null) {
				data.setEntityId(entity.getId());
				log.info("entityId:" + entity.getId());
			}
			if (data != null) {

				if (data.getTourProgram().getId() != null) {
					log.info("tourProgramId:" + data.getTourProgram().getId());
					tourProgram = tourProgramRepository.findOne(data.getTourProgram().getId());
				}

				/*
				 * tourProgram.setEmployeeId(data.getTourProgram().getEmployeeId());
				 * tourProgram.setTourName(data.getTourProgram().getTourName());
				 * tourProgram.setProgramMonth(data.getTourProgram().getProgramMonth());
				 * tourProgram.setProgramYear(data.getTourProgram().getProgramYear());
				 * tourProgram.setEntityMaster(entity); tourProgram.setStatusCode("Active");
				 * 
				 * try { tourProgram = tourProgramRepository.save(tourProgram); } catch
				 * (Exception e) { log.info("tour Prgoram saved successfully:" + tourProgram); }
				 */

				log.info("tour Prgoram details list size:" + data.getTourProgramDetailsList().size());

				for (TourProgramDetails tourDeatils : data.getTourProgramDetailsList()) {
					TourProgramDetails tourProgramDetail = new TourProgramDetails();
					log.info("<<<--- Tour Program details....." + tourDeatils);
					if (tourDeatils.getProgramDate() != null && tourDeatils.getPlannedVisitTypeId() != null
							&& tourDeatils.getInspectionTypePlanned() != null
							&& tourDeatils.getPlaceOfVisitPlanned() != null
							&& (tourDeatils.getVisitOfficeNamePlanned() != null
									|| tourDeatils.getPlannedEntity() != null)
							&& tourDeatils.getVisitPurposePlanned() != null) {

						if (tourDeatils.getId() != null) {
							tourProgramDetail = tourProgramDetailsRepository.findOne(tourDeatils.getId());
						}
						tourProgramDetail.setProgramDate(tourDeatils.getProgramDate());
						tourProgramDetail.setDeviationRemarks(data.getNote());
						tourProgramDetail.setPlaceOfVisitActual(tourDeatils.getPlaceOfVisitPlanned().trim());
						// tourProgramDetail.setPlaceOfVisitPlanned(tourDeatils.getPlaceOfVisitPlanned().trim());
						tourProgramDetail.setActualVisitTypeId(tourDeatils.getPlannedVisitTypeId());
						// tourProgramDetail.setPlannedVisitTypeId(tourDeatils.getPlannedVisitTypeId());
						tourProgramDetail.setInspectionTypeActual(tourDeatils.getInspectionTypePlanned().trim());
						// tourProgramDetail.setInspectionTypePlanned(tourDeatils.getInspectionTypePlanned().trim());
						tourProgramDetail.setVisitPurposeActual(tourDeatils.getVisitPurposePlanned().trim());
						// tourProgramDetail.setVisitPurposePlanned(tourDeatils.getVisitPurposePlanned().trim());
						tourProgramDetail.setTourProgram(tourProgram);

						if (tourDeatils.getPlannedEntity() != null && tourDeatils.getPlaceOfVisitPlanned() != null
								&& tourDeatils.getPlaceOfVisitPlanned().trim().equalsIgnoreCase(
										TourProgramConstant.TOUR_PLACE_OF_VISIT_INTRAOFFICE.toString())) {
							tourProgramDetail.setActualEntity(
									entityMasterRepository.findOne(tourDeatils.getPlannedEntity().getId()));
							// tourProgramDetail.setPlannedEntity(
							// entityMasterRepository.findOne(tourDeatils.getPlannedEntity().getId()));
						}

						try {
							if (tourDeatils.getPlannedEntity() != null) {
								tourProgramDetail.setActualEntity(
										entityMasterRepository.findOne(tourDeatils.getPlannedEntity().getId()));
								// tourProgramDetail.setPlannedEntity(
								// entityMasterRepository.findOne(tourDeatils.getPlannedEntity().getId()));
								tourProgramDetail.setVisitOfficeNameActual(null);
								// tourProgramDetail.setVisitOfficeNamePlanned(null);
							} else {
								tourProgramDetail
										.setVisitOfficeNameActual(tourDeatils.getVisitOfficeNamePlanned().trim());
								// tourProgramDetail
								// .setVisitOfficeNamePlanned(tourDeatils.getVisitOfficeNamePlanned().trim());
								tourProgramDetail.setActualEntity(null);
								// tourProgramDetail.setPlannedEntity(null);
							}

							tourProgramDetailsRepository.save(tourProgramDetail);
							log.info("<---- tourProgramDetails saved successfully:");
						} catch (Exception ex) {
							log.info("<-- TourProgramDetails Table Service/ exception :- -->" + ex);
						}
					} else {
						log.info("<-- Some empty records in list :- -->");
					}
				}

				if (data.getRemoveTourProgramDetailsList() != null) {
					for (TourProgramDetails tourDeatils : data.getRemoveTourProgramDetailsList()) {
						try {
							log.info("DeleteTourProgramDetails  removedId:" + tourDeatils.getId());
							if (tourDeatils.getId() != null) {
								tourProgramDetailsRepository.deleteTourProgramDetailsById(tourDeatils.getId());
								log.info("Delete TourProgramDetails successfully");
							} else {
								log.info("Tour Detail Id is null");
							}
						} catch (Exception ex) {
							log.info("<-- Delete TourProgram Detail list delete Service/ exception :--->" + ex);
						}
					}
				}

				/*
				 * // UserMaster userMaster = //
				 * userMasterRepository.getOne(data.getForwardTo()); UserMaster userMaster =
				 * null; if (data.getForwardToUser() != null && data.getForwardToUser().getId()
				 * != null) { userMaster =
				 * userMasterRepository.getOne(data.getForwardToUser().getId()); } if
				 * (data.getNote() != null) {
				 * 
				 * log.info("tourProgram :" + tourProgram); note.setTourProgram(tourProgram);
				 * note.setNote(data.getNote()); note.setFinalApproval(data.getForwardFor());
				 * note.setForwardTo(userMaster); try { note =
				 * tourProgramNoteRepository.save(note);
				 * log.info("TourProgram note saved successfully " + note); } catch (Exception
				 * ex) { log.info("<-- TourProgramNote Table Save / exception :- -->" + ex); } }
				 * 
				 * TourProgramLog tourProgramLog = new TourProgramLog();
				 * tourProgramLog.setTourProgram(tourProgram);
				 * tourProgramLog.setStage(TourProgramConstant.TTP_SUBMITTED);
				 * tourProgramLog.setCreatedBy(loginService.getCurrentUser());
				 * tourProgramLog.setCreatedByName(loginService.getCurrentUser().getUsername());
				 * tourProgramLog.setCreatedDate(new Date()); if (data.getRemarks() != null) {
				 * tourProgramLog.setRemarks(data.getRemarks()); } if (data.getVersion() !=
				 * null) { tourProgramLog.setVersion(data.getVersion()); } try { tourProgramLog
				 * = tourProgramLogRepository.save(tourProgramLog);
				 * log.info("TourProgram Log saved successfully " + tourProgramLog); } catch
				 * (Exception ex) { log.info("<-- TourProgramLog Table Save / exception :- -->"
				 * + ex); }
				 */

				// result = tourProgramLog;
				/*
				 * Send Notification Mail
				 */

				/*
				 * Map<String, Object> additionalData = new HashMap<>();
				 * additionalData.put("Url", TOUR_PROGRAME_VIEW_URL + "&tourProgramId=" +
				 * tourProgram.getId() + "&");
				 * notificationEmailService.sendMailAndNotificationForForward(note,
				 * tourProgramLog, additionalData); baseDTO.setResponseContent(result);
				 * baseDTO.setTotalRecords(1);
				 */
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

			} else {
				log.info("<-- Empty of TourProgramSaveDto/ exception :- -->");
			}
		} catch (Exception ex) {
			log.info("<-- Exception in TourProgramService/ exception :- -->" + ex);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			return baseDTO;
		}
		return baseDTO;
	}

	public BaseDTO getTourEditById(Long id, Long notificationId) {
		log.info("<--- TourProgram.getTourEditById() starts---->" + id);
		BaseDTO response = new BaseDTO();

		TourProgramSaveDto tourDTO = new TourProgramSaveDto();
		try {

			TourProgram tourProgram = tourProgramRepository.findOne(id);
			tourDTO.setTourProgram(tourProgram);
			tourDTO.setTourProgramDetailsList(tourProgramDetailsRepository.getTourProgramDetailList(id));
			tourDTO.setTourProgramLog(tourProgramLogRepository.getTourProgamLogDetailsById(id));
			tourDTO.setTourProgramNote(tourProgramNoteRepository.getTourProgamNoteDetailsById(id));
			TourProgramNote tourProgramNote = tourProgramNoteRepository.getTourProgamNoteDetailsById(id);
			tourDTO.setForwardFor(tourProgramNote.getFinalApproval());
			tourDTO.setForwardTo(tourProgramNote.getForwardTo().getId());
			tourDTO.setNote(tourProgramNote.getNote());

			for (TourProgramDetails tourProgramDetails : tourDTO.getTourProgramDetailsList()) {
				tourProgramDetails.setInspectionTypePlanned(tourProgramDetails.getInspectionTypePlanned().trim());
				tourProgramDetails.setPlaceOfVisitPlanned(tourProgramDetails.getPlaceOfVisitPlanned().trim());
			}

			List<Map<String, Object>> employeeData = new ArrayList<Map<String, Object>>();
			if (id != null) {
				// log.info("<<<:::::::tourprogram::::not Null::::>>>>"+tourProgram!=null ?
				// tourProgram.getId():"Null");
				ApplicationQuery applicationQueryForlog = applicationQueryRepository
						.findByQueryName("TOURPROGRAM_LOG_EMPLOYEE_DETAILS");
				if (applicationQueryForlog == null || applicationQueryForlog.getId() == null) {
					log.info("Application Query For Log Details not found for query name : " + applicationQueryForlog);
					response.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
					return response;
				}
				String logquery = applicationQueryForlog.getQueryContent().trim();
				log.info("<=========TOURPROGRAM_LOG_EMPLOYEE_DETAILS Query Content ======>" + logquery);
				logquery = logquery.replace(":tourProgramId", id + "");
				log.info("Query Content For TOURPROGRAM_LOG_EMPLOYEE_DETAILS After replaced plan id View query : "
						+ logquery);
				employeeData = jdbcTemplate.queryForList(logquery);
				log.info("<=========TOURPROGRAM_LOG_EMPLOYEE_DETAILS Employee Data======>" + employeeData);
				response.setTotalListOfData(employeeData);
			}
			if (notificationId != null && notificationId > 0) {
				SystemNotification systemNotification = systemNotificationRepository.findOne(notificationId);
				systemNotification.setNotificationRead(true);
				systemNotificationRepository.save(systemNotification);
			}
			response.setResponseContent(tourDTO);

			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (RestException e) {
			log.error(" Rest Exception Occured ", e);
			response.setStatusCode(e.getStatusCode());
		} catch (Exception e) {
			log.info("Error while retriving getTourProgramEditById----->", e);
			response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<---TourProgramService.getTourProgramEditById() end---->");
		return responseWrapper.send(response);
	}

	public BaseDTO getAllEmployee() {
		log.info("EmployeeService.getAllEmployee() Started ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<EmployeeMaster> employeeList = employeeMasterRepository.getAllEmployee();
			if (employeeList != null && employeeList.size() > 0) {
				baseDTO.setResponseContent(employeeList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				log.info("EmployeeService.getAllEmployee() Completed");
			}
		} catch (Exception exception) {
			log.error("Exception occurred in EmployeeService.getAllEmployee() -:", exception);
			baseDTO.setStatusCode(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO approveTourProgram(TourProgramResponseDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();

		try {
			log.info("approveTourProgram method start=============>" + requestDTO.getId());
			TourProgram tourProgram = tourProgramRepository.findOne(requestDTO.getId());

			TourProgramNote tourProgramNote = new TourProgramNote();
			tourProgramNote.setTourProgram(tourProgram);
			tourProgramNote.setFinalApproval(requestDTO.getForwardFor());
			tourProgramNote.setNote(requestDTO.getNote());
			tourProgramNote.setForwardTo(userMasterRepository.findOne(requestDTO.getForwardTo()));
			tourProgramNote.setCreatedBy(loginService.getCurrentUser());
			tourProgramNote.setCreatedByName(loginService.getCurrentUser().getUsername());
			tourProgramNote.setCreatedDate(new Date());
			tourProgramNoteRepository.save(tourProgramNote);
			log.info("approveTourProgram note inserted------");

			TourProgramLog tourProgramLog = new TourProgramLog();
			tourProgramLog.setTourProgram(tourProgram);
			tourProgramLog.setStage(requestDTO.getStatus());
			tourProgramLog.setCreatedBy(loginService.getCurrentUser());
			tourProgramLog.setCreatedByName(loginService.getCurrentUser().getUsername());
			tourProgramLog.setCreatedDate(new Date());
			tourProgramLog.setRemarks(requestDTO.getRemarks());
			tourProgramLogRepository.save(tourProgramLog);
			/*
			 * Send Notification Mail
			 * 
			 * 
			 * 
			 */

			if (TourProgramConstant.TTP_FINAL_APPROVED.equals(requestDTO.getStatus())) {
				String urlPath = TOUR_PROGRAME_VIEW_URL + "&tourProgramId=" + tourProgram.getId() + "&";
				Long toUserId = tourProgram.getCreatedBy().getId();
				notificationEmailService.saveIndividualNotification(loginService.getCurrentUser(), toUserId, urlPath,
						"Tentative Tour", " Tentative Tour Program  has been final approved");
			} else {
				Map<String, Object> additionalData = new HashMap<>();
				additionalData.put("Url", TOUR_PROGRAME_VIEW_URL + "&tourProgramId=" + requestDTO.getId() + "&");
				notificationEmailService.sendMailAndNotificationForForward(tourProgramNote, tourProgramLog,
						additionalData);
			}

			log.info("approveTourProgram log inserted------");
			log.info("Approval status----------" + requestDTO.getStatus());
			log.info("Tour posting End--------");
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("approveTourProgram method exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO rejectTourProgram(TourProgramResponseDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("rejectTourProgram method start=============>" + requestDTO.getId());

			TourProgram tourProgram = tourProgramRepository.findOne(requestDTO.getId());

			TourProgramNote tourProgramNote = new TourProgramNote();
			tourProgramNote.setTourProgram(tourProgram);
			tourProgramNote.setFinalApproval(requestDTO.getForwardFor());
			tourProgramNote.setNote(requestDTO.getNote());
			tourProgramNote.setForwardTo(userMasterRepository.findOne(requestDTO.getForwardTo()));
			tourProgramNote.setCreatedBy(loginService.getCurrentUser());
			tourProgramNote.setCreatedByName(loginService.getCurrentUser().getUsername());
			tourProgramNote.setCreatedDate(new Date());
			tourProgramNoteRepository.save(tourProgramNote);
			log.info("approveTourProgram note inserted------");

			TourProgramLog tourProgramLog = new TourProgramLog();
			tourProgramLog.setTourProgram(tourProgram);
			tourProgramLog.setStage(requestDTO.getStatus());
			tourProgramLog.setCreatedBy(loginService.getCurrentUser());
			tourProgramLog.setCreatedByName(loginService.getCurrentUser().getUsername());
			tourProgramLog.setCreatedDate(new Date());
			tourProgramLog.setRemarks(requestDTO.getRemarks());
			tourProgramLogRepository.save(tourProgramLog);

			Map<String, Object> additionalData = new HashMap<>();
			additionalData.put("Url", TOUR_PROGRAME_VIEW_URL + "&tourProgramId=" + tourProgram.getId() + "&");
			notificationEmailService.sendMailAndNotificationForForward(tourProgramNote, tourProgramLog, additionalData);

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("rejectTourProgram  log inserted------");
		} catch (Exception e) {
			log.error("rejectTourProgram method exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}
}
