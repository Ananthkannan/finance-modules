package in.gov.cooptex.finance.service;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import in.gov.cooptex.core.accounts.model.MarketingInspectionEntity;
import in.gov.cooptex.core.accounts.model.SurpriseInspection;
import in.gov.cooptex.core.accounts.model.SurpriseinspectionCashDetails;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.IntensiveInspectionTestStockDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.finance.repository.FinancialYearRepository;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EmployeeEducationQualification;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeePersonalInfoEmployment;
import in.gov.cooptex.core.model.EmployeePromotionDetails;
import in.gov.cooptex.core.model.EmployeeTransferDetails;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.FinancialYear;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.QualificationMaster;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.AppointmentOrderDetailRepository;
import in.gov.cooptex.core.repository.DesignationRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.ProductCategoryRepository;
import in.gov.cooptex.core.repository.ProductVarietyMasterRepository;
import in.gov.cooptex.core.util.JdbcUtil;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.FinanceErrorCode;
import in.gov.cooptex.finance.advance.repository.IntensiveInspectionAgedStockRepository;
import in.gov.cooptex.finance.advance.repository.IntensiveInspectionAuditRepository;
import in.gov.cooptex.finance.advance.repository.IntensiveInspectionClosingRepository;
import in.gov.cooptex.finance.advance.repository.IntensiveInspectionCreditDetailsRepository;
import in.gov.cooptex.finance.advance.repository.IntensiveInspectionDiscountStockRepository;
import in.gov.cooptex.finance.advance.repository.IntensiveInspectionEmployeeRepository;
import in.gov.cooptex.finance.advance.repository.IntensiveInspectionGeneralDetailsRepository;
import in.gov.cooptex.finance.advance.repository.IntensiveInspectionSilkStockRepository;
import in.gov.cooptex.finance.advance.repository.IntensiveInspectionStockRepository;
import in.gov.cooptex.finance.advance.repository.IntensiveInspectionTestCheckRepository;
import in.gov.cooptex.finance.advance.repository.IntensiveInspectionTestStockRepository;
import in.gov.cooptex.finance.advance.repository.SurpriseInspectionCashRepository;
import in.gov.cooptex.finance.advance.repository.SurpriseInspectionRepository;
import in.gov.cooptex.finance.dto.IntensiveInspectionAuditRequestDto;
import in.gov.cooptex.finance.dto.IntensiveInspectionBudgetDTO;
import in.gov.cooptex.finance.dto.IntensiveInspectionCreditSalesDTO;
import in.gov.cooptex.finance.dto.IntensiveInspectionDTO;
import in.gov.cooptex.finance.dto.MarketingInspectionSalesDtlsDto;
import in.gov.cooptex.finance.dto.StockDetailsDto;
import in.gov.cooptex.finance.dto.StockDetailsRequestDetails;
import in.gov.cooptex.finance.dto.SurpriseInspectionCashDTO;
import in.gov.cooptex.finance.dto.SurpriseInspectionDTO;
import in.gov.cooptex.finance.dto.SurpriseInspectionOtherDtlsDto;
import in.gov.cooptex.finance.dto.SurpriseInspectionSalesDtlsDto;
import in.gov.cooptex.finance.dto.SurpriseInspectionStockDtlsDto;
import in.gov.cooptex.operation.model.ConcurrentAudit;
import in.gov.cooptex.operation.model.IntensiveInspectionAgedStock;
import in.gov.cooptex.operation.model.IntensiveInspectionAudit;
import in.gov.cooptex.operation.model.IntensiveInspectionClosing;
import in.gov.cooptex.operation.model.IntensiveInspectionCreditDetails;
import in.gov.cooptex.operation.model.IntensiveInspectionDiscountStock;
import in.gov.cooptex.operation.model.IntensiveInspectionEmployee;
import in.gov.cooptex.operation.model.IntensiveInspectionGeneralDetails;
import in.gov.cooptex.operation.model.IntensiveInspectionSilkStock;
import in.gov.cooptex.operation.model.IntensiveInspectionStock;
import in.gov.cooptex.operation.model.IntensiveInspectionTestCheck;
import in.gov.cooptex.operation.model.IntensiveInspectionTestStock;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class SurpriseInspectionReportService {

	@Autowired
	IntensiveInspectionAuditRepository intensiveInspectionAuditRepository;

	@Autowired
	IntensiveInspectionCreditDetailsRepository intensiveInspectionCreditDetailsRepository;

	@Autowired
	IntensiveInspectionEmployeeRepository intensiveInspectionEmployeeRepository;

	@Autowired
	IntensiveInspectionGeneralDetailsRepository intensiveInspectionGeneralDetailsRepository;

	@PersistenceContext
	EntityManager entityManager;

	@Autowired
	ProductVarietyMasterRepository productVarietyMasterRepository;

	@Autowired
	EmployeeMasterRepository employeeMasterRepository;

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	FinancialYearRepository financialYearRepository;
	
	@Autowired
	SurpriseInspectionRepository surpriseInspectionRepository;
	
	@Autowired
	SurpriseInspectionCashRepository surpriseinspectionCashRepository;

	@Autowired
	DesignationRepository designationRepository;

	@Autowired
	IntensiveInspectionClosingRepository intensiveInspectionClosingRepository;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	@Autowired
	IntensiveInspectionStockRepository intensiveInspectionStockRepository;

	@Autowired
	IntensiveInspectionTestCheckRepository intensiveInspectionTestCheckRepository;

	@Autowired
	IntensiveInspectionTestStockRepository intensiveInspectionTestStockRepository;

	@Autowired
	IntensiveInspectionDiscountStockRepository intensiveInspectionDiscountStockRepository;

	@Autowired
	IntensiveInspectionSilkStockRepository intensiveInspectionSilkStockRepository;

	@Autowired
	IntensiveInspectionAgedStockRepository intensiveInspectionAgedStockRepository;

	@Autowired
	AppointmentOrderDetailRepository appointmentOrderDetailRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	ProductCategoryRepository productCategoryRepository;

	private static final String DECIMAL_FORMAT = "#0.00";

	public BaseDTO getLazyLoadData(PaginationDTO paginationDTO) {
		log.info("IntensiveInspectionReportService. getLazyLoadData() - START");
		BaseDTO baseDTO = new BaseDTO();
		try {
			Session session = entityManager.unwrap(Session.class);
			Criteria criteria = session.createCriteria(SurpriseInspection.class, "surpriseInspection");
			criteria.createAlias("surpriseInspection.entityMaster", "entityMaster");
			log.info(":: Criteria search started ::");
			if (paginationDTO.getFilters() != null) {

				String showroom = (String) paginationDTO.getFilters().get("showroom");
				if (StringUtils.isNotEmpty(showroom)) {
					Criterion firstCriteria = Restrictions.like("showroom.name", "%" + showroom.trim() + '%')
							.ignoreCase();
					if (AppUtil.isInteger(showroom.trim())) {
						Criterion secondCriteria = Restrictions.eq("showroom.code", Integer.parseInt(showroom.trim()));
						criteria.add(Restrictions.or(firstCriteria, secondCriteria));
					} else {
						criteria.add(firstCriteria);
					}
				}
				String inspectionPeriod = (String) paginationDTO.getFilters().get("inspectionPeriod");
				if (StringUtils.isNotEmpty(inspectionPeriod)) {
					criteria.add(Restrictions
							.like("surpriseInspection.inspectionPeriod", "%" + inspectionPeriod.trim() + "%")
							.ignoreCase());
				}
				if (paginationDTO.getFilters().get("createdDate") != null) {
					Date date = new Date((long) paginationDTO.getFilters().get("createdDate"));
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = dateFormat.format(date);
					Date minDate = dateFormat.parse(strDate);
					Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
					criteria.add(
							Restrictions.conjunction().add(Restrictions.ge("surpriseInspection.createdDate", minDate))
									.add(Restrictions.lt("surpriseInspection.createdDate", maxDate)));
				}
				if (paginationDTO.getFilters().get("inspectionDate") != null) {
					Date date = new Date((long) paginationDTO.getFilters().get("inspectionDate"));
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = dateFormat.format(date);
					Date minDate = dateFormat.parse(strDate);
					Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
					criteria.add(Restrictions.conjunction()
							.add(Restrictions.ge("surpriseInspection.inspectionDate", minDate))
							.add(Restrictions.lt("surpriseInspection.inspectionDate", maxDate)));
				}
				if (paginationDTO.getFilters().get("lastInspectionDate") != null) {
					Date date = new Date((long) paginationDTO.getFilters().get("lastInspectionDate"));
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = dateFormat.format(date);
					Date minDate = dateFormat.parse(strDate);
					Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
					criteria.add(Restrictions.conjunction()
							.add(Restrictions.ge("surpriseInspection.lastInspectionDate", minDate))
							.add(Restrictions.lt("surpriseInspection.lastInspectionDate", maxDate)));
				}
				String status = (String) paginationDTO.getFilters().get("status");
				if (StringUtils.isNotEmpty(status)) {
					criteria.add(
							Restrictions.like("surpriseInspection.status", "%" + status.trim() + "%").ignoreCase());
				}
				criteria.setProjection(Projections.rowCount());
				Integer totalResult = ((Long) criteria.uniqueResult()).intValue();
				criteria.setProjection(null);

				ProjectionList projectionList = Projections.projectionList();
				projectionList.add(Projections.property("id"));
				projectionList.add(Projections.property("entityMaster"));
				projectionList.add(Projections.property("inspectionDate"));
				projectionList.add(Projections.property("inspectionFrom"));
				projectionList.add(Projections.property("inspectionTo"));

				criteria.setProjection(projectionList);

				if (paginationDTO.getFirst() != null) {
					Integer pageNo = paginationDTO.getFirst();
					Integer pageSize = paginationDTO.getPageSize();
					if (pageNo != null && pageSize != null) {
						criteria.setFirstResult(pageNo * pageSize);
						criteria.setMaxResults(pageSize);
						log.info("PageNo : [" + pageNo + "] pageSize[" + pageSize + "]");
					}

					String sortField = paginationDTO.getSortField();
					String sortOrder = paginationDTO.getSortOrder();
					log.info("sortField outside : [" + sortField + "] sortOrder[" + sortOrder + "]");
					if (paginationDTO.getSortField() != null && paginationDTO.getSortOrder() != null) {
						log.info("sortField : [" + paginationDTO.getSortField() + "] sortOrder["
								+ paginationDTO.getSortOrder() + "]");

						if (paginationDTO.getSortField().equals("inspectionDate")) {
							sortField = "surpriseInspection.inspectionDate";
						} else if (sortField.equals("createdDate")) {
							sortField = "surpriseInspection.createdDate";
						} else if (sortField.equals("region")) {
							sortField = "region.name";
							sortField = "region.code";
						} else if (sortField.equals("showroom")) {
							sortField = "showroom.code";
							sortField = "showroom.name";
						} else if (sortField.equals("id")) {
							sortField = "surpriseInspection.id";
						} else if (sortField.equals("unitName")) {
							sortField = "surpriseInspection.unitName";
						} else if (sortField.equals("inspectionPeriod")) {
							sortField = "surpriseInspection.inspectionPeriod";
						} else if (sortField.equals("lastInspectionDate")) {
							sortField = "surpriseInspection.lastInspectionDate";
						} else if (sortField.equals("status")) {
							sortField = "surpriseInspection.status";
						}

						if (sortOrder.equals("DESCENDING")) {
							criteria.addOrder(Order.desc(sortField));
						} else {
							criteria.addOrder(Order.asc(sortField));
						}
					} else {
						criteria.addOrder(Order.desc("surpriseInspection.modifiedDate"));
					}
				}
				List<?> resultList = criteria.list();
				log.info("criteria list executed and the list size is : " + resultList.size());
				if (resultList == null || resultList.isEmpty() || resultList.size() == 0) {
					log.info("SurpriseInspection List is null or empty ");
				}
				List<SurpriseInspection> surpriseInspectionList = new ArrayList<>();
				Iterator<?> it = resultList.iterator();
				while (it.hasNext()) {
					Object ob[] = (Object[]) it.next();
					SurpriseInspection response = new SurpriseInspection();
					response.setId((Long) ob[0]);
					EntityMaster showroomEntiy = ob[1] != null ? (EntityMaster) ob[1] : null;
					EntityMaster showroomObj = new EntityMaster();
					showroomObj.setId(showroomEntiy.getId());
					showroomObj.setCode(showroomEntiy.getCode());
					showroomObj.setName(showroomEntiy.getName());
					response.setEntityMaster(showroomObj);

					response.setInspectionDate(ob[2] != null ? (Date) ob[2] : null);
					response.setInspectionFrom(ob[3] != null ? (Date) ob[3] : null);
					response.setInspectionTo(ob[4] != null ? (Date) ob[4] : null);
					surpriseInspectionList.add(response);

				}
				baseDTO.setResponseContents(surpriseInspectionList);
				baseDTO.setTotalRecords(totalResult);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (Exception exp) {
			log.error("Exception at getLazyLoadData() ", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("IntensiveInspectionReportService. getLazyLoadData() - END");
		return baseDTO;
	}

	// ==============================================================================================
	/**
	 * 
	 * @param intensiveInspectionAudit
	 * @return
	 */
	public BaseDTO saveOrUpdateIntensiveInspectionAudit(IntensiveInspectionAudit intensiveInspectionAudit) {
		log.info("IntensiveInspectionReportService. saveOrUpdateIntensiveInspectionAudit() - START");
		BaseDTO baseDTO = new BaseDTO();
		Long showroomId = null;
		try {

			if (intensiveInspectionAudit != null) {
				if (intensiveInspectionAudit.getId() == null) {
					showroomId = intensiveInspectionAudit.getShowroom() != null
							? intensiveInspectionAudit.getShowroom().getId()
							: null;
					log.info("IntensiveInspectionReportService. saveOrUpdateIntensiveInspectionAudit() - showroomId: "
							+ showroomId);

					Long existCount = intensiveInspectionAuditRepository.getExistCountByShowroomIdAndInspectionPeriod(
							showroomId, intensiveInspectionAudit.getInspectionDate());
					existCount = existCount != null ? existCount.longValue() : 0;
					if (existCount > 0) {
						baseDTO.setStatusCode(
								ErrorDescription.getError(FinanceErrorCode.AUDIT_DETAILS_ALREADY_EXIST).getErrorCode());
						return baseDTO;
					} else {
						if (showroomId != null) {
							intensiveInspectionAudit.setShowroom(entityMasterRepository.findOne(showroomId));
						}

						intensiveInspectionAudit.setStatus(ApprovalStage.INITIATED);
						IntensiveInspectionAudit intensiveInspectionAuditObj = intensiveInspectionAuditRepository
								.save(intensiveInspectionAudit);
						baseDTO.setResponseContent(intensiveInspectionAuditObj.getId());
						baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
					}
				} else {
					IntensiveInspectionAudit intensiveInspectionAuditObj = intensiveInspectionAuditRepository
							.findOne(intensiveInspectionAudit.getId());
					intensiveInspectionAuditObj.setInspectionDate(intensiveInspectionAudit.getInspectionDate());
					intensiveInspectionAuditObj.setInspectionPeriod(intensiveInspectionAudit.getInspectionPeriod());
					intensiveInspectionAuditRepository.save(intensiveInspectionAuditObj);
					baseDTO.setResponseContent(intensiveInspectionAuditObj.getId());
					baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
				}
			}

		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Exception at saveOrUpdateIntensiveInspectionAudit()", e);
		}
		log.info("IntensiveInspectionReportService. saveOrUpdateIntensiveInspectionAudit() - END");
		return baseDTO;
	}

	public BaseDTO getFinanceYear() {
		log.info("IntensiveInspectionReportService. getFinanceYear() - START");
		BaseDTO baseDto = new BaseDTO();
		try {
			List<FinancialYear> financialYearList = financialYearRepository.getAllFinancialYear();
			baseDto.setResponseContents(financialYearList);
			baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("Exception at getFinanceYear()", e);
		}
		log.info("IntensiveInspectionReportService. getFinanceYear() - END");
		return baseDto;
	}

	/**
	 * 
	 * @param entityId
	 * @return
	 */
	public BaseDTO getEmployeeListByEntityId(Long showroomId, Long auditId) {
		log.info("IntensiveInspectionReportService. getEmployeeListByEntityId() - START");
		BaseDTO baseDTO = new BaseDTO();
		List<EmployeeMaster> employeeList = null;
		try {
			log.info("IntensiveInspectionReportService. getEmployeeListByEntityId() - showroomId: " + showroomId);

			List<IntensiveInspectionEmployee> intensiveInspectionEmployeeList = new ArrayList<>();
			if (auditId != null) {
				intensiveInspectionEmployeeList = intensiveInspectionEmployeeRepository
						.getEmployeeListByAuditId(auditId);
			}

			if (!CollectionUtils.isEmpty(intensiveInspectionEmployeeList)) {
				for (IntensiveInspectionEmployee intensiveInspectionEmployee : intensiveInspectionEmployeeList) {
					EmployeeMaster employee = intensiveInspectionEmployee.getStaff();
					if (employee != null) {
						EmployeeMaster employeeMaster = new EmployeeMaster();
						employeeMaster.setId(employee.getId());
						employeeMaster.setFirstName(employee.getFirstName());
						employeeMaster.setLastName(employee.getLastName());
						intensiveInspectionEmployee.setStaff(null);
						intensiveInspectionEmployee.setStaff(employeeMaster);
					}
				}
				baseDTO.setResponseContent(intensiveInspectionEmployeeList);
				baseDTO.setResponseContents(null);
			} else {
				List<EmployeeMaster> employeeMasterList = employeeMasterRepository
						.getAllEmployeeByShowroomId(showroomId);
				int employeeMasterListSize = employeeMasterList != null ? employeeMasterList.size() : 0;
				log.info("IntensiveInspectionReportService. getEmployeeListByEntityId() - employeeMasterListSize: "
						+ employeeMasterListSize);

				if (!CollectionUtils.isEmpty(employeeMasterList)) {
					employeeList = new ArrayList<>();

					for (EmployeeMaster employeeMaster : employeeMasterList) {

						EmployeeMaster employeeMasterObj = new EmployeeMaster();
						employeeMasterObj.setId(employeeMaster.getId());
						employeeMasterObj.setFirstName(employeeMaster.getFirstName());
						employeeMasterObj.setLastName(employeeMaster.getLastName());

						EmployeePersonalInfoEmployment personalInfoEmployment = employeeMaster
								.getPersonalInfoEmployment();
						if (personalInfoEmployment != null) {
							EmployeePersonalInfoEmployment employeePersonalInfoEmployment = new EmployeePersonalInfoEmployment();
							employeePersonalInfoEmployment
									.setDateOfAppointment(personalInfoEmployment.getDateOfAppointment());

							Designation designation = personalInfoEmployment.getCurrentDesignation();
							Designation designationMaster = new Designation();
							designationMaster.setId(designation.getId());
							designationMaster.setName(designation.getName());
							designationMaster.setCode(designation.getCode());
							employeePersonalInfoEmployment.setCurrentDesignation(designationMaster);

							employeeMasterObj.setPersonalInfoEmployment(employeePersonalInfoEmployment);
						}

						List<EmployeeEducationQualification> qualificationList = employeeMaster.getEducationList();

						if (!CollectionUtils.isEmpty(qualificationList)) {
							EmployeeEducationQualification employeeEducationQualification = qualificationList
									.get(qualificationList.size() - 1);
							QualificationMaster qualification = employeeEducationQualification.getQualificationMaster();
							if (qualification != null) {
								QualificationMaster qualificationMaster = new QualificationMaster();
								qualificationMaster.setQualificationId(qualification.getQualificationId());
								qualificationMaster.setQualificationLevel(qualification.getQualificationLevel());
								EmployeeEducationQualification employeeEducationQualificationObj = new EmployeeEducationQualification();
								employeeEducationQualificationObj.setQualificationMaster(qualificationMaster);
								employeeMasterObj.getEducationList().add(employeeEducationQualificationObj);
							}
						}

						List<EmployeePromotionDetails> promotionDetailsList = employeeMaster
								.getEmployeePromotionDetails();
						if (!CollectionUtils.isEmpty(promotionDetailsList)) {
							EmployeePromotionDetails employeePromotionDetails = promotionDetailsList
									.get(promotionDetailsList.size() - 1);
							if (employeePromotionDetails != null) {
								EmployeePromotionDetails employeePromotionDetailsObj = new EmployeePromotionDetails();
								employeePromotionDetailsObj
										.setDateOfJoining(employeePromotionDetails.getDateOfJoining());
								employeeMasterObj.getEmployeePromotionDetails().add(employeePromotionDetailsObj);
							}
						}

						List<EmployeeTransferDetails> transferDetailsList = employeeMaster.getTransferDetailsList();
						if (!CollectionUtils.isEmpty(transferDetailsList)) {
							List<EmployeeTransferDetails> showroomTransferDetailsList = transferDetailsList.stream()
									.filter(o -> o.getTransferTo() != null
											&& o.getTransferTo().getId().longValue() == showroomId.longValue())
									.collect(Collectors.toList());
							if (!CollectionUtils.isEmpty(showroomTransferDetailsList)) {
								EmployeeTransferDetails employeeTransferDetails = showroomTransferDetailsList
										.get(showroomTransferDetailsList.size() - 1);
								if (employeeTransferDetails != null) {
									EmployeeTransferDetails employeeTransferDetailsObj = new EmployeeTransferDetails();
									employeeTransferDetailsObj.setJoinedOn(employeeTransferDetails.getJoinedOn());
									employeeMasterObj.getTransferDetailsList().add(employeeTransferDetailsObj);
								}
							}
						}

						Double securityDepositAmount = appointmentOrderDetailRepository
								.getSecurityDepositAmountByEmpId(employeeMasterObj.getId());
						Department department = new Department();
						department.setSecurityDepositAmount(securityDepositAmount != null ? securityDepositAmount : 0D);
						employeeMasterObj.setDepartment(department);
						employeeList.add(employeeMasterObj);
					}
				}
				baseDTO.setResponseContents(employeeList);
				baseDTO.setResponseContent(null);
			}

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("Exception at getEmployeeListByEntityId()", e);
		}
		log.info("IntensiveInspectionReportService. getEmployeeListByEntityId() - END");
		return baseDTO;
	}

	public BaseDTO deleteIntensiveEmployeeId(Long auditEmployeeId) {
		log.info("IntensiveInspectionReportService. deleteIntensiveEmployeeId() - START");
		BaseDTO baseDTO = new BaseDTO();
		try {
			if (auditEmployeeId != null) {
				intensiveInspectionEmployeeRepository.deleteById(auditEmployeeId);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			}
		} catch (Exception e) {
			log.error("Exception at deleteIntensiveEmployeeId()", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("IntensiveInspectionReportService. deleteIntensiveEmployeeId() - END");
		return baseDTO;
	}

	public BaseDTO saveOrUpdateIntensiveInspectionEmployee(
			IntensiveInspectionAuditRequestDto intensiveInspectionAuditRequestDto) {
		log.info("IntensiveInspectionReportService. saveOrUpdateIntensiveInspectionEmployee() - START");
		BaseDTO baseDTO = new BaseDTO();
		IntensiveInspectionAudit intensiveInspectionAudit = null;
		try {

			List<IntensiveInspectionEmployee> intensiveInspectionEmployeeList = intensiveInspectionAuditRequestDto
					.getIntensiveInspectionEmployeeList();

			if (!CollectionUtils.isEmpty(intensiveInspectionEmployeeList)) {
				Long auditId = intensiveInspectionAuditRequestDto.getIntensiveInspectionAuditId();
				Long inspectionEmployeeId = intensiveInspectionEmployeeList.get(0).getId();
				if (auditId != null) {
					intensiveInspectionAudit = intensiveInspectionAuditRepository.findOne(auditId);
				}

				if (inspectionEmployeeId != null) {
					intensiveInspectionEmployeeRepository.deleteByAuditId(auditId);
				}

				for (IntensiveInspectionEmployee data : intensiveInspectionEmployeeList) {
					if (data.getStaff() != null && data.getStaff().getId() != null) {
						EmployeeMaster employeeMaster = employeeMasterRepository.findOne(data.getStaff().getId());
						data.setStaff(employeeMaster);
					}
					if (data.getDesignation() != null && data.getDesignation().getId() != null) {
						Designation designationMaster = designationRepository.findOne(data.getDesignation().getId());
						data.setDesignation(designationMaster);
					}
					if (intensiveInspectionAudit != null && intensiveInspectionAudit != null) {
						data.setIntensiveInspectionAudit(intensiveInspectionAudit);
					}
					intensiveInspectionEmployeeRepository.save(data);
				}
			}

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.error("Exception at saveOrUpdateIntensiveInspectionEmployee()", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("IntensiveInspectionReportService. saveOrUpdateIntensiveInspectionEmployee() - END");
		return baseDTO;
	}

	/**
	 * 
	 * @param showroomId
	 * @return
	 */
	public BaseDTO getIntensiveInspectionClosingData(Long showroomId) {
		log.info("IntensiveInspectionReportService. getIntensiveInspectionClosingData() - START");
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("IntensiveInspectionReportService. getIntensiveInspectionClosingData() - showroomId: "
					+ showroomId);
			List<IntensiveInspectionClosing> closingDataList = new ArrayList<>();
			ApplicationQuery applicationQuery = applicationQueryRepository
					.findByQueryName("INTENSIVE_INSPECTION_CLOSING_DATA_SQL");
			if (applicationQuery == null) {
				baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getErrorCode());
				return baseDTO;
			}

			String queryContent = applicationQuery.getQueryContent();
			queryContent = StringUtils.isNotEmpty(queryContent) ? queryContent.trim() : null;
			queryContent = StringUtils.replace(queryContent, "${showroomId}", String.valueOf(showroomId));

			closingDataList = jdbcTemplate.query(queryContent,
					new BeanPropertyRowMapper<IntensiveInspectionClosing>(IntensiveInspectionClosing.class));

			baseDTO.setResponseContents(closingDataList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Exception at getIntensiveInspectionClosingData()", e);
		}
		log.info("IntensiveInspectionReportService. getIntensiveInspectionClosingData() - END");
		return baseDTO;
	}

	private Double getDouble(Map<String, Object> map, String key) {
		log.info("getDouble() - Key: " + key);
		try {
			Object obj = map.get(key);
			if (obj != null) {
				log.info("getDouble() - Value: " + obj);
				String valueStr = String.valueOf(obj);
				if (!StringUtils.isEmpty(valueStr)) {
					DecimalFormat decimalFormat = new DecimalFormat(DECIMAL_FORMAT);
					Double value = decimalFormat.parse(decimalFormat.format(Double.valueOf(valueStr))).doubleValue();
					return value;
				}
			}
		} catch (Exception ex) {
			log.error("Exception at getDouble()", ex);
		}
		return null;
	}

	/**
	 * 
	 * @param intensiveInspectionAuditRequestDto
	 * @return
	 */
	public BaseDTO saveOrUpdateIntensiveInspectionClosings(
			IntensiveInspectionAuditRequestDto intensiveInspectionAuditRequestDto) {
		log.info("IntensiveInspectionReportService. saveOrUpdateIntensiveInspectionClosings() - START");
		BaseDTO baseDTO = new BaseDTO();
		IntensiveInspectionAudit intensiveInspectionAudit = null;
		try {

			List<IntensiveInspectionClosing> intensiveInspectionClosingList = intensiveInspectionAuditRequestDto
					.getIntensiveInspectionClosingList();

			if (!CollectionUtils.isEmpty(intensiveInspectionClosingList)) {

				Long auditId = intensiveInspectionAuditRequestDto.getIntensiveInspectionAuditId();
				Long inspectionClosingId = intensiveInspectionClosingList.get(0).getId();
				if (auditId != null) {
					intensiveInspectionAudit = intensiveInspectionAuditRepository.findOne(auditId);
				}

				if (inspectionClosingId != null) {
					intensiveInspectionEmployeeRepository.deleteByAuditId(auditId);
				}

				for (IntensiveInspectionClosing intensiveInspectionClosing : intensiveInspectionClosingList) {
					if (intensiveInspectionAudit != null && intensiveInspectionAudit.getId() != null) {
						intensiveInspectionClosing.setIntensiveInspectionAudit(intensiveInspectionAudit);
					}
				}
				intensiveInspectionClosingRepository.save(intensiveInspectionClosingList);
			}
			baseDTO.setResponseContent(intensiveInspectionAuditRequestDto);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Exception at saveOrUpdateIntensiveInspectionClosings()", e);
		}
		log.info("IntensiveInspectionReportService. saveOrUpdateIntensiveInspectionClosings() - END");
		return baseDTO;
	}

	public BaseDTO saveOrUpdateIntensiveGeneralDetails(
			IntensiveInspectionGeneralDetails intensiveInspectionGeneralDetails) {
		log.info("IntensiveInspectionReportService. saveOrUpdateIntensiveGeneralDetails() - START");
		BaseDTO baseDTO = new BaseDTO();
		IntensiveInspectionAudit intensiveInspectionAudit = null;
		try {
			IntensiveInspectionAudit intensiveInspectionAuditObj = intensiveInspectionGeneralDetails
					.getIntensiveInspectionAudit();
			if (intensiveInspectionAuditObj != null && intensiveInspectionAuditObj.getId() != null) {
				intensiveInspectionAudit = intensiveInspectionAuditRepository
						.findOne(intensiveInspectionAuditObj.getId());
				intensiveInspectionAudit.setStatus(ApprovalStage.SUBMITTED);
				intensiveInspectionGeneralDetails.setIntensiveInspectionAudit(intensiveInspectionAudit);
			}
			if (intensiveInspectionGeneralDetails.getPeriodOfInspection() != null
					&& intensiveInspectionGeneralDetails.getPeriodOfInspection().getId() != null) {
				intensiveInspectionGeneralDetails.setPeriodOfInspection(financialYearRepository
						.findOne(intensiveInspectionGeneralDetails.getPeriodOfInspection().getId()));
			}

			intensiveInspectionGeneralDetailsRepository.save(intensiveInspectionGeneralDetails);
			if (intensiveInspectionAudit != null && intensiveInspectionAudit.getId() != null) {
				intensiveInspectionAuditRepository.save(intensiveInspectionAudit);
			}
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Exception at saveOrUpdateIntensiveGeneralDetails()", e);
		}
		log.info("IntensiveInspectionReportService. saveOrUpdateIntensiveGeneralDetails() - END");
		return baseDTO;
	}

	public BaseDTO saveOrUpdateIntensiveCreditDetails(
			IntensiveInspectionCreditDetails intensiveInspectionCreditDetails) {
		log.info("IntensiveInspectionReportService. saveOrUpdateIntensiveCreditDetails() - START");
		BaseDTO baseDTO = new BaseDTO();
		IntensiveInspectionAudit intensiveInspectionAudit = null;
		try {
			IntensiveInspectionAudit intensiveInspectionAuditObj = intensiveInspectionCreditDetails
					.getIntensiveInspectionAudit();
			if (intensiveInspectionAuditObj != null && intensiveInspectionAuditObj.getId() != null) {
				intensiveInspectionAudit = intensiveInspectionAuditRepository
						.findOne(intensiveInspectionAuditObj.getId());
				IntensiveInspectionCreditDetails intensiveInspectionCreditDetailsObj = intensiveInspectionCreditDetailsRepository
						.getCreditDetailsByAuditId(intensiveInspectionAuditObj.getId());
				if (intensiveInspectionCreditDetailsObj != null
						&& intensiveInspectionCreditDetailsObj.getId() != null) {
					intensiveInspectionCreditDetailsObj.setIntensiveInspectionAudit(intensiveInspectionAudit);
					intensiveInspectionCreditDetailsObj
							.setOpeningBalance(intensiveInspectionCreditDetails.getOpeningBalance());
					intensiveInspectionCreditDetailsObj
							.setDueNetAmount(intensiveInspectionCreditDetails.getDueNetAmount());
					intensiveInspectionCreditDetailsObj
							.setTotalAmount(intensiveInspectionCreditDetails.getTotalAmount());
					intensiveInspectionCreditDetailsObj
							.setRealisedAmount(intensiveInspectionCreditDetails.getRealisedAmount());
					intensiveInspectionCreditDetailsObj
							.setClosingBalance(intensiveInspectionCreditDetails.getClosingBalance());
					intensiveInspectionCreditDetailsObj
							.setIsCreditSalesEffected(intensiveInspectionCreditDetails.getIsCreditSalesEffected());
					intensiveInspectionCreditDetailsObj
							.setDefectDetails(intensiveInspectionCreditDetails.getDefectDetails());
					intensiveInspectionCreditDetailsObj.setIsLegalActionTakenPendingCases(
							intensiveInspectionCreditDetails.getIsLegalActionTakenPendingCases());
					intensiveInspectionCreditDetailsObj
							.setPendingCaseDetails(intensiveInspectionCreditDetails.getPendingCaseDetails());
					intensiveInspectionCreditDetailsObj.setIsActionTakenTimeBarredCases(
							intensiveInspectionCreditDetails.getIsActionTakenTimeBarredCases());
					intensiveInspectionCreditDetailsObj
							.setTimeBarredDefectDetails(intensiveInspectionCreditDetails.getTimeBarredDefectDetails());
					intensiveInspectionCreditDetailsRepository.save(intensiveInspectionCreditDetailsObj);
				} else {
					intensiveInspectionCreditDetails.setIntensiveInspectionAudit(intensiveInspectionAudit);
					intensiveInspectionCreditDetailsRepository.save(intensiveInspectionCreditDetails);
				}
			}
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Exception at saveOrUpdateIntensiveCreditDetails()", e);
		}
		log.info("IntensiveInspectionReportService. saveOrUpdateIntensiveCreditDetails() - END");
		return baseDTO;
	}

	public BaseDTO saveOrUpdateBudgetDetails(IntensiveInspectionBudgetDTO intensiveInspectionBudgetDTO) {
		log.info("IntensiveInspectionReportService. saveOrUpdateBudgetDetails() - START");
		BaseDTO baseDTO = new BaseDTO();
		try {
			Long auditId = intensiveInspectionBudgetDTO.getIntensiveAuditId();
			if (auditId != null) {
				IntensiveInspectionAudit intensiveInspectionAudit = intensiveInspectionAuditRepository.findOne(auditId);
				intensiveInspectionAudit
						.setIsBudgetReviewReturnsSent(intensiveInspectionBudgetDTO.getIsBudgetReviewReturnsSent());
				intensiveInspectionAuditRepository.save(intensiveInspectionAudit);

			}
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Exception at saveOrUpdateBudgetDetails()", e);
		}
		log.info("IntensiveInspectionReportService. saveOrUpdateBudgetDetails() - END");
		return baseDTO;
	}

	public BaseDTO saveOrUpdateTestStockDetails(IntensiveInspectionTestStockDTO intensiveInspectionTestStockDTO) {
		log.info("IntensiveInspectionReportService. saveOrUpdateTestStockDetails() - START");
		BaseDTO baseDTO = new BaseDTO();
		IntensiveInspectionAudit intensiveInspectionAudit = null;
		List<IntensiveInspectionTestStock> intensiveInspectionTestStockList = null;
		try {
			if (intensiveInspectionTestStockDTO != null) {
				Long auditId = intensiveInspectionTestStockDTO.getIntensiveAuditId();
				List<IntensiveInspectionTestStockDTO> testStockList = intensiveInspectionTestStockDTO
						.getTestStockDetailsList();
				IntensiveInspectionTestCheck intensiveInspectionTestCheck = intensiveInspectionTestStockDTO
						.getIntensiveInspectionTestCheck();
				if (auditId != null) {
					intensiveInspectionTestStockRepository.deleteByAuditId(auditId);
					intensiveInspectionAudit = intensiveInspectionAuditRepository.findOne(auditId);

					if (!CollectionUtils.isEmpty(testStockList)) {
						intensiveInspectionTestStockList = new ArrayList<>();
						for (IntensiveInspectionTestStockDTO intensiveInspectionTestStockDTOObj : testStockList) {
							IntensiveInspectionTestStock intensiveInspectionTestStock = new IntensiveInspectionTestStock();
							if (intensiveInspectionTestStockDTOObj.getProductId() != null) {
								intensiveInspectionTestStock.setProductVarietyMaster(productVarietyMasterRepository
										.findOne(intensiveInspectionTestStockDTOObj.getProductId()));
							}

							intensiveInspectionTestStock.setIntensiveInspectionAudit(intensiveInspectionAudit);
							intensiveInspectionTestStock
									.setBookQty(intensiveInspectionTestStockDTOObj.getBookedQuantity());
							intensiveInspectionTestStock
									.setBookValue(intensiveInspectionTestStockDTOObj.getBookedValue());
							intensiveInspectionTestStock
									.setActualQty(intensiveInspectionTestStockDTOObj.getActualQuantity());
							intensiveInspectionTestStock
									.setActualValue(intensiveInspectionTestStockDTOObj.getActualValue());
							intensiveInspectionTestStock
									.setExcessQty(intensiveInspectionTestStockDTOObj.getExcessQuantity());
							intensiveInspectionTestStock
									.setExcessValue(intensiveInspectionTestStockDTOObj.getExcessValue());
							intensiveInspectionTestStock
									.setShortQty(intensiveInspectionTestStockDTOObj.getShortQuantity());
							intensiveInspectionTestStock
									.setShortValue(intensiveInspectionTestStockDTOObj.getShortValue());
							intensiveInspectionTestStock.setComments(intensiveInspectionTestStockDTOObj.getComments());
							intensiveInspectionTestStockList.add(intensiveInspectionTestStock);
						}
						if (!CollectionUtils.isEmpty(intensiveInspectionTestStockList)) {
							intensiveInspectionTestStockRepository.save(intensiveInspectionTestStockList);
						}
					}

					if (intensiveInspectionTestCheck != null && intensiveInspectionTestCheck.getId() != null) {
						IntensiveInspectionTestCheck intensiveInspectionTestCheckObj = intensiveInspectionTestCheckRepository
								.findOne(intensiveInspectionTestCheck.getId());

						intensiveInspectionTestCheckObj
								.setIsLedgerIssuePosted(intensiveInspectionTestCheck.getIsLedgerIssuePosted());
						intensiveInspectionTestCheckObj.setPostedDate(intensiveInspectionTestCheck.getPostedDate());
						intensiveInspectionTestCheckObj
								.setIsVerifiedPostingBills(intensiveInspectionTestCheck.getIsVerifiedPostingBills());
						intensiveInspectionTestCheckObj
								.setDefectUploadPath(intensiveInspectionTestCheck.getDefectUploadPath());
						intensiveInspectionTestCheckObj.setIsPackingRegisterMaterial(
								intensiveInspectionTestCheck.getIsPackingRegisterMaterial());
						intensiveInspectionTestCheckObj
								.setIsVerifiedReceipts(intensiveInspectionTestCheck.getIsVerifiedReceipts());
						intensiveInspectionTestCheckObj
								.setIsAllGoodsReceived(intensiveInspectionTestCheck.getIsAllGoodsReceived());
						intensiveInspectionTestCheckObj
								.setIsInwardRegister(intensiveInspectionTestCheck.getIsInwardRegister());
						intensiveInspectionTestCheckObj.setIsFillAllColumnCorrectly(
								intensiveInspectionTestCheck.getIsFillAllColumnCorrectly());
						intensiveInspectionTestCheckObj
								.setIsOutwardRegister(intensiveInspectionTestCheck.getIsOutwardRegister());
						intensiveInspectionTestCheckObj
								.setIsConsigneeAcknowledge(intensiveInspectionTestCheck.getIsConsigneeAcknowledge());
						intensiveInspectionTestCheckObj
								.setIsVerifyAllItems(intensiveInspectionTestCheck.getIsVerifyAllItems());
						intensiveInspectionTestCheckObj.setNote(intensiveInspectionTestCheck.getNote());
						intensiveInspectionTestCheckObj
								.setShortExcessDefectPath(intensiveInspectionTestCheck.getShortExcessDefectPath());
						intensiveInspectionTestCheckObj
								.setShortExcessAckPath(intensiveInspectionTestCheck.getShortExcessAckPath());
						intensiveInspectionTestCheckObj
								.setIsVerifyTransferNote(intensiveInspectionTestCheck.getIsVerifyTransferNote());
						intensiveInspectionTestCheckObj.setIntensiveInspectionAudit(intensiveInspectionAudit);
						intensiveInspectionTestCheckRepository.save(intensiveInspectionTestCheckObj);
					} else {
						intensiveInspectionTestCheck.setIntensiveInspectionAudit(intensiveInspectionAudit);
						intensiveInspectionTestCheckRepository.save(intensiveInspectionTestCheck);
					}
				}
			}
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Exception at saveOrUpdateBudgetDetails()", e);
		}
		log.info("IntensiveInspectionReportService. saveOrUpdateBudgetDetails() - END");
		return baseDTO;
	}

	public BaseDTO getById(Long auditId) {
		log.info("SurpriseInspection. getById() - START");
		BaseDTO baseDTO = new BaseDTO();
		//IntensiveInspectionDTO intensiveInspectionDTO = new IntensiveInspectionDTO();
		SurpriseInspection intensiveInspectionAuditObj = new SurpriseInspection();
		try {
			SurpriseInspection supinsp = surpriseInspectionRepository.getById(auditId);
			if (supinsp != null) {
				
				intensiveInspectionAuditObj.setId(supinsp.getId());
				EntityMaster showroomEntity = supinsp.getEntityMaster();
				if (showroomEntity != null) {
					EntityMaster entityMaster = new EntityMaster();
					entityMaster.setId(showroomEntity.getId());
					entityMaster.setCode(showroomEntity.getCode());
					entityMaster.setName(showroomEntity.getName());
					entityMaster.setAddressMaster(showroomEntity.getAddressMaster()!=null?showroomEntity.getAddressMaster():null);
					intensiveInspectionAuditObj.setEntityMaster(entityMaster);
				}
				if (showroomEntity.getEntityMasterRegion() != null
						&& showroomEntity.getEntityMasterRegion().getId()!=null) {
					EntityMaster regionentityMaster = new EntityMaster();
					regionentityMaster.setId(showroomEntity.getEntityMasterRegion().getId());
					regionentityMaster.setCode(showroomEntity.getEntityMasterRegion().getCode());
					regionentityMaster.setName(showroomEntity.getEntityMasterRegion().getName());
					intensiveInspectionAuditObj.setRegionEntity(regionentityMaster);
				}
				intensiveInspectionAuditObj.setInspectionDate(supinsp.getInspectionDate());
				intensiveInspectionAuditObj.setInspectionFrom(supinsp.getInspectionFrom());
				intensiveInspectionAuditObj.setInspectionTo(supinsp.getInspectionTo());
				intensiveInspectionAuditObj.setGeneralDesc(supinsp.getGeneralDesc());
				}
			baseDTO.setResponseContent(intensiveInspectionAuditObj);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception exp) {
			log.error("Exception at getById() ", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("SurpriseInspection. getById() - END");
		return baseDTO;
	}

	/**
	 * 
	 * @param stockDetailsRequestDetails
	 * @return
	 */
	public BaseDTO saveOrUpdateIntensiveInspectionStock(StockDetailsRequestDetails stockDetailsRequestDetails) {
		log.info("IntensiveInspectionReportService. saveOrUpdateIntensiveInspectionStock() - START");
		BaseDTO baseDto = new BaseDTO();
		try {
			Long auditId = stockDetailsRequestDetails.getAuditId();
			log.info("IntensiveInspectionReportService. saveOrUpdateIntensiveInspectionStock() - auditId: " + auditId);
			if (auditId != null) {

				intensiveInspectionAgedStockRepository.deleteByAuditId(auditId);
				intensiveInspectionDiscountStockRepository.deleteByAuditId(auditId);
				intensiveInspectionSilkStockRepository.deleteByAuditId(auditId);
				intensiveInspectionStockRepository.deleteByAuditId(auditId);

				IntensiveInspectionAudit intensiveInspectionAudit = intensiveInspectionAuditRepository.findOne(auditId);

				List<StockDetailsDto> ageStockDetailsDTOList = stockDetailsRequestDetails
						.getAgedWiseStockDetailsDtoList();
				if (!CollectionUtils.isEmpty(ageStockDetailsDTOList)) {
					for (StockDetailsDto ageStockDTO : ageStockDetailsDTOList) {
						IntensiveInspectionAgedStock intensiveInspectionAgedStock = new IntensiveInspectionAgedStock();
						ProductCategory productCategory = productCategoryRepository
								.findByCodeIgnoreCase(ageStockDTO.getProductCategory());
						intensiveInspectionAgedStock.setProductCategory(productCategory);
						intensiveInspectionAgedStock.setBelowOneYear(ageStockDTO.getBelowOneYear());
						intensiveInspectionAgedStock.setAboveOneYear(ageStockDTO.getOverOneYearBelowTwoYear());
						intensiveInspectionAgedStock.setAboveTwoYear(ageStockDTO.getOverTwoBelowFiveYear());
						intensiveInspectionAgedStock.setAboveThreeYear(ageStockDTO.getOverThreeBelowFiveYear());
						intensiveInspectionAgedStock.setOverFiveYear(ageStockDTO.getOverFiveYear());
						intensiveInspectionAgedStock.setIntensiveInspectionAudit(intensiveInspectionAudit);
						intensiveInspectionAgedStockRepository.save(intensiveInspectionAgedStock);
					}
				}
				log.info(
						"IntensiveInspectionReportService. saveOrUpdateIntensiveInspectionStock() - Age stock saved successfully.");

				/**
				 * NOTE : Here I get Percentage Values By using StockDetailsDto
				 * belowOneYear,overOneYearBelowTwoYear,overTwoBelowFiveYear,overThreeBelowFiveYear
				 * overFiveYear Variables for reducing more DTO Creation.
				 */

				List<StockDetailsDto> discountStockDetailsDTOList = stockDetailsRequestDetails
						.getDiscountWiseStockDetailsDtoList();
				if (!CollectionUtils.isEmpty(discountStockDetailsDTOList)) {
					for (StockDetailsDto data : discountStockDetailsDTOList) {
						IntensiveInspectionDiscountStock intensiveInspectionDiscountStock = new IntensiveInspectionDiscountStock();
						intensiveInspectionDiscountStock.setAprilMonthValue(data.getBeginYearCertifiedGoodsValue());
						intensiveInspectionDiscountStock
								.setDuringInspectionValue(data.getDuringInspectionCertifiedGoodsValue());
						intensiveInspectionDiscountStock.setTotalValue(data.getTotalValueOfCertifiedGoodsValue());
						intensiveInspectionDiscountStock.setSoldDuringInspection(data.getSoldCertifiedGoodsValue());
						intensiveInspectionDiscountStock.setMarchMonthValue(data.getEndYearCertifiedGoodsValue());
						intensiveInspectionDiscountStock.setIntensiveInspectionAudit(intensiveInspectionAudit);
						intensiveInspectionDiscountStock.setDiscountPercentage(data.getDiscountPercentage());
						intensiveInspectionDiscountStockRepository.save(intensiveInspectionDiscountStock);
					}
				}
				log.info(
						"IntensiveInspectionReportService. saveOrUpdateIntensiveInspectionStock() - Discount stock saved successfully.");

				List<StockDetailsDto> intensiveInspectionSilkStockList = stockDetailsRequestDetails
						.getIntensiveInspectionSilkStockList();
				if (!CollectionUtils.isEmpty(intensiveInspectionSilkStockList)) {
					for (StockDetailsDto data : intensiveInspectionSilkStockList) {
						IntensiveInspectionSilkStock intensiveInspectionSilkStock = new IntensiveInspectionSilkStock();
						intensiveInspectionSilkStock
								.setProductVarietyMaster(productVarietyMasterRepository.findOne(data.getProductId()));
						intensiveInspectionSilkStock.setIntensiveInspectionAudit(intensiveInspectionAudit);
						intensiveInspectionSilkStock.setTotalQuantity(data.getSilkStockTotalQty());
						intensiveInspectionSilkStock.setTotalValue(data.getSilkStockTotalValues());
						intensiveInspectionSilkStockRepository.save(intensiveInspectionSilkStock);
					}
				}
				log.info(
						"IntensiveInspectionReportService. saveOrUpdateIntensiveInspectionStock() - Silk stock saved successfully.");

				IntensiveInspectionStock intensiveInspectionStock = stockDetailsRequestDetails
						.getIntensiveInspectionStock();
				intensiveInspectionStock.setIntensiveInspectionAudit(intensiveInspectionAudit);
				intensiveInspectionStockRepository.save(intensiveInspectionStock);
				log.info(
						"IntensiveInspectionReportService. saveOrUpdateIntensiveInspectionStock() - intensive stock saved successfully.");

				baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				baseDto.setErrorDescription("IntensiveInspectionAudit Cannot Be Empty");
			}
		} catch (Exception e) {
			log.error("Exception at IntensiveInspectionReportService.saveOrUpdateIntensiveInspectionStock()", e);
		}
		log.info("IntensiveInspectionReportService. saveOrUpdateIntensiveInspectionStock() - END");
		return baseDto;
	}

	/**
	 * 
	 * @return
	 */
	public BaseDTO getAllActiveShowrooms(String showroomCodeOrName) {
		log.info("IntensiveInspectionReportService. getAllActiveShowrooms() - START");
		BaseDTO response = new BaseDTO();
		try {
			List<EntityMaster> showroomList = entityMasterRepository.getAllActiveShowroomData(showroomCodeOrName);
			int showroomListSize = showroomList != null ? showroomList.size() : 0;
			log.info("IntensiveInspectionReportService. getAllActiveShowrooms() - showroomListSize: "
					+ showroomListSize);

			if (!CollectionUtils.isEmpty(showroomList)) {
				List<EntityMaster> responseList = new ArrayList<>();
				for (EntityMaster obj : showroomList) {
					EntityMaster entityMaster = new EntityMaster();
					entityMaster.setId(obj.getId());
					entityMaster.setCode(obj.getCode());
					entityMaster.setName(obj.getName());
					responseList.add(entityMaster);
				}
				response.setResponseContent(responseList);
			}
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.error("Exception at getAllActiveShowrooms()", e);
			response.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("IntensiveInspectionReportService. getAllActiveShowrooms() - END");
		return response;
	}

	/**
	 * 
	 * @param showroomId
	 * @return
	 */
	public BaseDTO getLastInspectionDateByShowroomId(Long showroomId) {
		log.info("IntensiveInspectionReportService. getLastInspectionDateByShowroomId() - START");
		BaseDTO response = new BaseDTO();
		Date lastInspectionDate = null;
		try {
			IntensiveInspectionAudit intensiveInspectionAudit = intensiveInspectionAuditRepository
					.getLastInspectionAuditByShowroomId(showroomId);
			if (intensiveInspectionAudit != null) {
				lastInspectionDate = intensiveInspectionAudit.getLastInspectionDate();
			} else {
				lastInspectionDate = null;
			}
			response.setResponseContent(lastInspectionDate);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.error("Exception at getLastInspectionDateByShowroomId()", e);
			response.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("IntensiveInspectionReportService. getLastInspectionDateByShowroomId() - END");
		return response;
	}

	/**
	 * 
	 * @param showroomId
	 * @return
	 */
	public BaseDTO getBudgetReviewData(Long showroomId) {
		log.info("IntensiveInspectionReportService. getBudgetReviewData() - START");
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("IntensiveInspectionReportService. getBudgetReviewData() - showroomId: " + showroomId);
			List<IntensiveInspectionBudgetDTO> budgetDataList = new ArrayList<>();
			ApplicationQuery applicationQuery = applicationQueryRepository
					.findByQueryName("INTENSIVE_INSPECTION_BUDGET_DATA_SQL");
			if (applicationQuery == null) {
				baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getErrorCode());
				return baseDTO;
			}

			String queryContent = applicationQuery.getQueryContent();
			queryContent = StringUtils.isNotEmpty(queryContent) ? queryContent.trim() : null;
			queryContent = StringUtils.replace(queryContent, "${showroomId}", String.valueOf(showroomId));

			budgetDataList = jdbcTemplate.query(queryContent,
					new BeanPropertyRowMapper<IntensiveInspectionBudgetDTO>(IntensiveInspectionBudgetDTO.class));

			baseDTO.setResponseContent(budgetDataList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Exception at getBudgetReviewData()", e);
		}
		log.info("IntensiveInspectionReportService. getBudgetReviewData() - END");
		return baseDTO;
	}

	private List<IntensiveInspectionCreditSalesDTO> getCreditSalesQueryData(String queryName, Long showroomId,
			Long startYear, Long endYear) throws Exception {
		List<IntensiveInspectionCreditSalesDTO> resultList = new ArrayList<>();
		ApplicationQuery applicationQuery = applicationQueryRepository.findByQueryName(queryName);
		if (applicationQuery == null) {
			throw new Exception("Application query is not found: " + queryName);
		}

		String queryContent = applicationQuery.getQueryContent();
		queryContent = StringUtils.isNotEmpty(queryContent) ? queryContent.trim() : null;
		queryContent = StringUtils.replace(queryContent, "${showroomId}", String.valueOf(showroomId));
		queryContent = StringUtils.replace(queryContent, "${startYear}", String.valueOf(startYear));
		queryContent = StringUtils.replace(queryContent, "${endYear}", String.valueOf(endYear));

		resultList = jdbcTemplate.query(queryContent,
				new BeanPropertyRowMapper<IntensiveInspectionCreditSalesDTO>(IntensiveInspectionCreditSalesDTO.class));
		return resultList;
	}

	public BaseDTO getCreditSalesData(IntensiveInspectionCreditSalesDTO request) {
		log.info("IntensiveInspectionReportService. getCreditSalesData() - START");
		BaseDTO baseDTO = new BaseDTO();
		try {
			IntensiveInspectionCreditSalesDTO intensiveInspectionCreditSalesDTO = new IntensiveInspectionCreditSalesDTO();
			Long showroomId = request.getShowroomId();
			Long startYear = request.getStartYear();
			Long endYear = request.getEndYear();

			log.info("IntensiveInspectionReportService. getCreditSalesData() - showroomId: " + showroomId);
			log.info("IntensiveInspectionReportService. getCreditSalesData() - startYear: " + startYear);
			log.info("IntensiveInspectionReportService. getCreditSalesData() - endYear: " + endYear);

			List<IntensiveInspectionCreditSalesDTO> creditSalesDataList = getCreditSalesQueryData(
					"INTENSIVE_INSPECTION_CREDIT_SALES_DATA_SQL", showroomId, startYear, endYear);
			List<IntensiveInspectionCreditSalesDTO> creditSalesAgeWiseDataList = getCreditSalesQueryData(
					"INTENSIVE_INSPECTION_CREDIT_SALES_AGE_WISE_DATA_SQL", showroomId, startYear, endYear);

			intensiveInspectionCreditSalesDTO.setCreditSalesDataList(creditSalesDataList);
			intensiveInspectionCreditSalesDTO.setCreditSalesAgetWiseOutStandingDataList(creditSalesAgeWiseDataList);
			baseDTO.setResponseContent(intensiveInspectionCreditSalesDTO);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			baseDTO.setErrorDescription(e.getMessage());
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Exception at getCreditSalesData()", e);
		}
		log.info("intensiveInspectionCreditSalesDTO. getCreditSalesData() - END");
		return baseDTO;
	}

	public BaseDTO getActiveProductVarietyForAutoComplete(String productCodeOrName) {
		log.info("IntensiveInspectionReportService. getActiveProductVarietyForAutoComplete() - START");
		BaseDTO response = new BaseDTO();
		try {
			List<ProductVarietyMaster> productVarietyList = productVarietyMasterRepository
					.getProductByCodeOrNameAutoComplete(productCodeOrName);
			response.setResponseContent(productVarietyList);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("Exception at getActiveProductVarietyForAutoComplete()", e);
			response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("IntensiveInspectionReportService. getActiveProductVarietyForAutoComplete() - END");
		return response;
	}

	public BaseDTO getBookedQtyValue(Long auditId, Long productId) {
		log.info("IntensiveInspectionReportService. getBookedQtyValue() - START");
		BaseDTO response = new BaseDTO();
		Long showroomId = null;
		String inspectionDateStr = null;
		IntensiveInspectionTestStockDTO intensiveInspectionTestStockDTO = new IntensiveInspectionTestStockDTO();
		try {

			ApplicationQuery applicationQuery = applicationQueryRepository
					.findByQueryName("INTENSIVE_INSPECTION_BOOKED_QTY_AND_VALUE_SQL");
			if (applicationQuery == null) {
				response.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getErrorCode());
				return response;
			}

			if (auditId != null) {
				IntensiveInspectionAudit intensiveInspectionAudit = intensiveInspectionAuditRepository.findOne(auditId);
				if (intensiveInspectionAudit != null) {
					showroomId = intensiveInspectionAudit.getShowroom() != null
							? intensiveInspectionAudit.getShowroom().getId()
							: null;
					Date inspectionDate = intensiveInspectionAudit.getInspectionDate();
					if (inspectionDate != null) {
						inspectionDateStr = AppUtil.REPORT_DATE_FORMAT.format(inspectionDate);
					}
				}
			}
			String queryContent = applicationQuery.getQueryContent();
			queryContent = StringUtils.isNotEmpty(queryContent) ? queryContent.trim() : null;
			queryContent = StringUtils.replace(queryContent, "${showroomId}", String.valueOf(showroomId));
			queryContent = StringUtils.replace(queryContent, "${productId}", String.valueOf(productId));
			queryContent = StringUtils.replace(queryContent, "${inspectionDate}",
					AppUtil.getValueWithSingleQuote(inspectionDateStr));
			List<Map<String, Object>> dataMapList = jdbcTemplate.queryForList(queryContent);
			if (!CollectionUtils.isEmpty(dataMapList)) {
				Map<String, Object> dataMap = dataMapList.get(0);
				intensiveInspectionTestStockDTO.setBookedQuantity(getDouble(dataMap, "booked_quantity"));
				intensiveInspectionTestStockDTO.setBookedValue(getDouble(dataMap, "booked_value"));
			}
			response.setResponseContent(intensiveInspectionTestStockDTO);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("Exception at getBookedQtyValue()", e);
			response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("IntensiveInspectionReportService. getBookedQtyValue() - END");
		return response;
	}

	public BaseDTO getStockVerificationDateValue(StockDetailsDto stockDetailsDto) {
		log.info("IntensiveInspectionReportService. getStockVerificationDateValue() - START");
		BaseDTO response = new BaseDTO();
		Long showroomId = null;
		StockDetailsDto stockDetailsDtoObj = new StockDetailsDto();
		try {

			ApplicationQuery applicationQuery = applicationQueryRepository
					.findByQueryName("INTENSIVE_INSPECTION_LAST_STOCK_VERIFICATION_DETAILS_SQL");
			if (applicationQuery == null) {
				response.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getErrorCode());
				return response;
			}

			showroomId = stockDetailsDto.getShowroomId();
			if (showroomId != null) {
				String queryContent = applicationQuery.getQueryContent();
				queryContent = StringUtils.isNotEmpty(queryContent) ? queryContent.trim() : null;
				queryContent = StringUtils.replace(queryContent, "${showroomId}", String.valueOf(showroomId));
				List<Map<String, Object>> dataMapList = jdbcTemplate.queryForList(queryContent);

				if (!CollectionUtils.isEmpty(dataMapList)) {
					Map<String, Object> dataMap = dataMapList.get(0);
					stockDetailsDtoObj.setLastStockVerificationDate(dataMap.get("last_stock_verification_date") != null
							? (Date) dataMap.get("last_stock_verification_date")
							: null);
					stockDetailsDtoObj
							.setLastStockVerificationValue(getDouble(dataMap, "last_stock_verification_value"));
					stockDetailsDtoObj.setShowroomId(showroomId);
				}

			}
			response.setResponseContent(stockDetailsDtoObj);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("Exception at getStockVerificationDateValue()", e);
			response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("IntensiveInspectionReportService. getStockVerificationDateValue() - END");
		return response;
	}

	/**
	 * 
	 * @param queryName
	 * @param showroomId
	 * @param inspectionFromDate
	 * @param inspectionToDate
	 * @param showroomCode
	 * @return
	 * @throws Exception
	 */
	private String getQueryContent(String queryName, Long showroomId, Date inspectionFromDate, Date inspectionToDate,
			Integer showroomCode) throws Exception {
		log.info("SurpriseInspectionReportService. getQueryContent() - START");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		ApplicationQuery applicationQuery = applicationQueryRepository.findByQueryName(queryName);
		if (applicationQuery == null) {
			throw new Exception("Application query is not found - " + queryName);
		}

		String queryContent = applicationQuery.getQueryContent();
		queryContent = StringUtils.isNotEmpty(queryContent) ? queryContent.trim() : null;
		queryContent = StringUtils.replace(queryContent, "${showroomId}", String.valueOf(showroomId));
		queryContent = StringUtils.replace(queryContent, "${fromDate}",
				AppUtil.getValueWithSingleQuote(sdf.format(inspectionFromDate)));
		queryContent = StringUtils.replace(queryContent, "${toDate}",
				AppUtil.getValueWithSingleQuote(sdf.format(inspectionToDate)));
		queryContent = StringUtils.replace(queryContent, "${showroomCode}", String.valueOf(showroomCode));
		log.info("SurpriseInspectionReportService. getQueryContent() - END");
		return queryContent;
	}

	/**
	 * 
	 * @param surpriseInspectionDTO
	 * @return
	 */
	public BaseDTO loadCashBalanceData(SurpriseInspectionDTO surpriseInspectionDTO) {
		log.info("SurpriseInspectionReportService. loadCashBalanceData() - START");
		BaseDTO baseDTO = new BaseDTO();
		try {
			Long showroomId = surpriseInspectionDTO.getShowroomId();
			Date inspectionFromDate = surpriseInspectionDTO.getInspectionFromDate();
			Date inspectionToDate = surpriseInspectionDTO.getInspectionToDate();
			Integer showroomCode = surpriseInspectionDTO.getShowroomCode();
			log.info("SurpriseInspectionReportService. loadCashBalanceData() - showroomId: " + showroomId);
			log.info("SurpriseInspectionReportService. loadCashBalanceData() - inspectionFromDate: "
					+ inspectionFromDate);
			log.info("SurpriseInspectionReportService. loadCashBalanceData() - inspectionToDate: " + inspectionToDate);
			log.info("SurpriseInspectionReportService. loadCashBalanceData() - showroomCode: " + showroomCode);

			String queryContent = getQueryContent("SURPRISE_INSPECTION_CASH_BALANCE_DATA_SQL", showroomId,
					inspectionFromDate, inspectionToDate, showroomCode);
			List<SurpriseInspectionDTO> cashBalanceDataList = jdbcTemplate.query(queryContent,
					new BeanPropertyRowMapper<SurpriseInspectionDTO>(SurpriseInspectionDTO.class));

			baseDTO.setResponseContent(cashBalanceDataList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			baseDTO.setErrorDescription(e.getMessage());
			log.error("Exception at loadCashBalanceData()", e);
		}
		log.info("SurpriseInspectionReportService. loadCashBalanceData() - END");
		return baseDTO;
	}

	public BaseDTO saveSurpriseVisit(SurpriseInspection surpriseInspection) {
		log.info("=========Starts saveSurpriseVisitt=====");
 		BaseDTO baseDto = new BaseDTO();
		try {
			if (surpriseInspection.getId() == null) {
				if (surpriseInspection.getEntityMaster() != null
						&& surpriseInspection.getEntityMaster().getId() != null) {
					log.info("============Entity Id is=======" + surpriseInspection.getEntityMaster().getId());
					EntityMaster entityMaster = entityMasterRepository
							.findOne(surpriseInspection.getEntityMaster().getId());
					surpriseInspection.setEntityMaster(entityMaster);
				}
				surpriseInspection.setVersion((long) 0);
				surpriseInspection.setInspectionDate(surpriseInspection.getInspectionDate());
				surpriseInspection.setInspectionFrom(surpriseInspection.getInspectionFrom());
				surpriseInspection.setInspectionTo(surpriseInspection.getInspectionTo());
				surpriseInspection.setGeneralDesc(surpriseInspection.getGeneralDesc());
				SurpriseInspection surpriseInspectionobj = surpriseInspectionRepository.save(surpriseInspection);
				log.info("============surpriseInspection Save or Update Succesfully ======="
						+ surpriseInspectionobj.getId());
				baseDto.setResponseContent(surpriseInspectionobj.getId());
				baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				SurpriseInspection surpriseInspectionObj = surpriseInspectionRepository
						.findOne(surpriseInspection.getId());
				surpriseInspectionObj.setInspectionDate(surpriseInspection.getInspectionDate());
				surpriseInspectionObj.setInspectionFrom(surpriseInspection.getInspectionFrom());
				surpriseInspectionObj.setInspectionTo(surpriseInspection.getInspectionTo());
				if (surpriseInspection.getEntityMaster() != null
						&& surpriseInspection.getEntityMaster().getId() != null) {
					log.info("============Entity Id is=======" + surpriseInspection.getEntityMaster().getId());
					EntityMaster entityMaster = entityMasterRepository
							.findOne(surpriseInspection.getEntityMaster().getId());
					surpriseInspectionObj.setEntityMaster(entityMaster);
				}
				surpriseInspectionRepository.save(surpriseInspectionObj);
				baseDto.setResponseContent(surpriseInspectionObj.getId());
				baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());

			}
		} catch (Exception e) {
			log.info("=========Exception Occured in saveSurpriseVisit=====", e);
			log.info("=========Exception is=====" + e.getMessage());
		}
		log.info("=========END saveSurpriseVisitt=====");

		return baseDto;
	}

	public BaseDTO saveDenoDetails(SurpriseInspectionCashDTO surpriseInspectionCashDTO) {
		
		BaseDTO baseDto = new BaseDTO();
		SurpriseinspectionCashDetails req = new SurpriseinspectionCashDetails();
		if (surpriseInspectionCashDTO.getSurpriseInspection() != null && surpriseInspectionCashDTO.getSurpriseInspection().getId() != null) {
			log.info("============Entity Id is=======" + surpriseInspectionCashDTO.getSurpriseInspection().getId());
			SurpriseInspection entityMaster = surpriseInspectionRepository.findOne(surpriseInspectionCashDTO.getSurpriseInspection().getId());
			req.setSurpriseInspection(entityMaster);
		}
		req.setAmount(surpriseInspectionCashDTO.getAmount());
		req.setDenomination(surpriseInspectionCashDTO.getDenomination());
		req.setNoOfNotes(surpriseInspectionCashDTO.getNoOfNotes());
		SurpriseinspectionCashDetails surpriseInspectionCashobj = surpriseinspectionCashRepository.save(req);
		baseDto.setResponseContent(surpriseInspectionCashobj.getId());
		baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		
		// TODO Auto-generated method stub
		return baseDto;
	}
	
	public BaseDTO updateDenoDetails(List<SurpriseInspectionCashDTO> surpriseInspectionCashDTOList) {
		log.info("=======START updateDenoDetails======");
		BaseDTO baseDto = new BaseDTO();
		try {
			SurpriseInspection surpriseInspection = new SurpriseInspection();
			
			if (surpriseInspectionCashDTOList != null
					&& surpriseInspectionCashDTOList.get(0).getSurpriseInspection() != null
					&& surpriseInspectionCashDTOList.get(0).getSurpriseInspection().getId() != null) {
				surpriseinspectionCashRepository
						.deleteByInspId(surpriseInspectionCashDTOList.get(0).getSurpriseInspection().getId());
			}
			for (SurpriseInspectionCashDTO surpriseInspectionCashDTO : surpriseInspectionCashDTOList) {
				SurpriseinspectionCashDetails req = new SurpriseinspectionCashDetails();
				if (surpriseInspection == null || surpriseInspection.getId() == null) {
					
					surpriseInspection = surpriseInspectionRepository
							.findOne(surpriseInspectionCashDTO.getSurpriseInspection().getId());
				}
				if(surpriseInspectionCashDTO.getNoOfNotes()!=null && surpriseInspectionCashDTO.getNoOfNotes()>0) {
				req.setSurpriseInspection(surpriseInspection);
				req.setAmount(surpriseInspectionCashDTO.getAmount());
				req.setDenomination(surpriseInspectionCashDTO.getDenomination());
				req.setNoOfNotes(surpriseInspectionCashDTO.getNoOfNotes());
				req.setVersion((long) 0);
				surpriseinspectionCashRepository.save(req);
				}
			}
			baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.info("=======Exception Occured in updateDenoDetails======",e);
			log.info("=======Exception is======" + e.getMessage());
		}
		log.info("=======END updateDenoDetailst======");
		return baseDto;
	}
	

	
	public BaseDTO loadAllStockDetailsDatatable(Long showroomId) {
		BaseDTO baseDto = new BaseDTO();
		showroomId=64L;
		try {
			SurpriseInspectionStockDtlsDto surpriseInspectionStockDtlsDto = new SurpriseInspectionStockDtlsDto();
			surpriseInspectionStockDtlsDto.setTestCheckofStocksList(new ArrayList<>());
			surpriseInspectionStockDtlsDto.setTestCheckofCodeNumbersList(new ArrayList<>());
			surpriseInspectionStockDtlsDto.setTestCheckofCodeNumIssuLed(new ArrayList<>());
			surpriseInspectionStockDtlsDto.setStockListForStat(new ArrayList<>());
			surpriseInspectionStockDtlsDto.setRemmitances(new ArrayList<>());
			
			surpriseInspectionStockDtlsDto.getTestCheckofStocksList()
			.addAll(commonDataFetchmethod(showroomId, "SURPRISE_INSPECTION_TESTCHKOFSTCKS"));

			surpriseInspectionStockDtlsDto.getTestCheckofCodeNumbersList()
			.addAll(commonDataFetchmethod(showroomId, "SURPRISE_INSPECTION_TESTCHKOFCODENUM"));
			
			surpriseInspectionStockDtlsDto.getTestCheckofCodeNumIssuLed()
			.addAll(commonDataFetchmethod(showroomId, "SURPRISE_INSPECTION_TESTCHKOFCODEISSLED"));

			surpriseInspectionStockDtlsDto.getStockListForStat()
			.addAll(commonDataFetchmethod(showroomId, "SURPRISE_INSPECTION_STKLISTSTAT"));
			
			surpriseInspectionStockDtlsDto.getRemmitances()
			.addAll(commonDataFetchmethod(showroomId, "SURPRISE_INSPECTION_REMMIT"));

			baseDto.setResponseContent(surpriseInspectionStockDtlsDto);
			baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			
		} catch (Exception e) {
			log.info("=========Exception Occured in MarketingAuditService.loadAllSalesDetailsDatatable=====", e);
			log.info("=========Exception is=====" + e.getMessage());
		}

		return baseDto;
	}
	
	private List<Map<String, Object>> commonDataFetchmethod(Long showroomId, String queryName) throws Exception{
		String queryContent = JdbcUtil.getApplicationQuery(jdbcTemplate, queryName);
		queryContent=queryContent.trim().replace(":entityID", showroomId.toString());
		List<Map<String, Object>> mapList = jdbcTemplate.queryForList(queryContent);
		return mapList;
	}

	public BaseDTO loadAllSalesDtlsTable(Long showroomId) {
		BaseDTO baseDto = new BaseDTO();
		// TODO Auto-generated method stub
		SurpriseInspectionSalesDtlsDto surpriseInspectionSalesDtlsDto = new SurpriseInspectionSalesDtlsDto();
		surpriseInspectionSalesDtlsDto.setCreditSalesRemmit(new ArrayList<>());
		surpriseInspectionSalesDtlsDto.setDtlscreditSales(new ArrayList<>());
		surpriseInspectionSalesDtlsDto.setConfrmCrdtSales(new ArrayList<>());
		showroomId=64L;
		try {
			surpriseInspectionSalesDtlsDto.getCreditSalesRemmit().addAll(commonDataFetchmethod(showroomId, "SURPRISE_INSPECTION_CDTSALES"));
			surpriseInspectionSalesDtlsDto.getDtlscreditSales().addAll(commonDataFetchmethod(showroomId, "SURPRISE_INSPECTION_DTLSSALE"));
			surpriseInspectionSalesDtlsDto.getConfrmCrdtSales().addAll(commonDataFetchmethod(showroomId, "SURPRISE_INSPECTION_CNFRSALE"));
		baseDto.setResponseContent(surpriseInspectionSalesDtlsDto);
		baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return baseDto;
	}

	public BaseDTO loadAllOtherDtlsTable(Long showroomId) {
		BaseDTO baseDto = new BaseDTO();
		// TODO Auto-generated method stub
		SurpriseInspectionOtherDtlsDto surpriseInspectionOtherDtlsDto = new SurpriseInspectionOtherDtlsDto();
		surpriseInspectionOtherDtlsDto.setWrhPkgReg(new ArrayList<>());
		surpriseInspectionOtherDtlsDto.setTstChkMat(new ArrayList<>());
		surpriseInspectionOtherDtlsDto.setBooksForms(new ArrayList<>());
		showroomId=64L;
		try {
			surpriseInspectionOtherDtlsDto.getWrhPkgReg().addAll(commonDataFetchmethod(showroomId, "SURPRISE_INSPECTION_WRHPKGREGT"));
			surpriseInspectionOtherDtlsDto.getTstChkMat().addAll(commonDataFetchmethod(showroomId, "SURPRISE_INSPECTION_TSTCHKMAT"));
			surpriseInspectionOtherDtlsDto.getBooksForms().addAll(commonDataFetchmethod(showroomId, "SURPRISE_INSPECTION_BOOKFORM"));
		baseDto.setResponseContent(surpriseInspectionOtherDtlsDto);
		baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		}catch(Exception e) {
			log.info("=========Exception is=====" + e.getMessage());
		}
		return baseDto;
	}

	public BaseDTO getByInspId(Long auditId) {
		// TODO Auto-generated method stub
		
		BaseDTO baseDto = new BaseDTO();
		List<SurpriseinspectionCashDetails> surpriseInspectionCashobj = surpriseinspectionCashRepository.getByInspId(auditId);
		baseDto.setResponseContents(surpriseInspectionCashobj);
		baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		
		return baseDto;
	}
	
}
