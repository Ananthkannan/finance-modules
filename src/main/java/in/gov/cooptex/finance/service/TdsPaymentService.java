package in.gov.cooptex.finance.service;

import in.gov.cooptex.common.service.EntityMasterService;
import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.dto.TdsPaymentRequestDTO;
import in.gov.cooptex.core.accounts.dto.TdsPaymentResponseDTO;
import in.gov.cooptex.core.accounts.dto.TdsPaymentViewDTO;
import in.gov.cooptex.core.accounts.enums.VoucherStatus;
import in.gov.cooptex.core.accounts.model.CashCreditLimitInterest;
import in.gov.cooptex.core.accounts.model.CashCreditLoanLimit;
import in.gov.cooptex.core.accounts.model.EntityBankBranch;
import in.gov.cooptex.core.accounts.model.PurchaseInvoice;
import in.gov.cooptex.core.accounts.model.TdsPayment;
import in.gov.cooptex.core.accounts.model.TourProgramLog;
import in.gov.cooptex.core.accounts.model.TourProgramNote;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.VoucherDetails;
import in.gov.cooptex.core.accounts.model.VoucherLog;
import in.gov.cooptex.core.accounts.model.VoucherNote;
import in.gov.cooptex.core.accounts.model.VoucherType;
import in.gov.cooptex.core.accounts.repository.EntityBankBranchRepository;
import in.gov.cooptex.core.accounts.repository.VoucherDetailsRepository;
import in.gov.cooptex.core.accounts.repository.VoucherLogRepository;
import in.gov.cooptex.core.accounts.repository.VoucherNoteRepository;
import in.gov.cooptex.core.accounts.repository.VoucherRepository;
import in.gov.cooptex.core.accounts.repository.VoucherTypeRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.SequenceName;
import in.gov.cooptex.core.finance.service.OperationService;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.SequenceConfig;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.AmountTransferDetailsRepository;
import in.gov.cooptex.core.repository.AmountTransferRepository;
import in.gov.cooptex.core.repository.AppQueryRepository;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.PaymentModeRepository;
import in.gov.cooptex.core.repository.PurchaseInvoiceRepository;
import in.gov.cooptex.core.repository.SequenceConfigRepository;
import in.gov.cooptex.core.repository.SupplierMasterRepository;
import in.gov.cooptex.core.repository.TdsPaymentRepository;
import in.gov.cooptex.core.repository.TourProgramDetailsRepository;
import in.gov.cooptex.core.repository.TourProgramLogRepository;
import in.gov.cooptex.core.repository.TourProgramNoteRepository;
import in.gov.cooptex.core.repository.TourProgramRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.TourProgramConstant;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.LedgerPostingException;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.*;
import in.gov.cooptex.finance.repository.AmountTransferLogRepository;
import in.gov.cooptex.finance.repository.AmountTransferNoteRepository;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.model.TourProgram;
import in.gov.cooptex.operation.model.TourProgramDetails;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Log4j2
@Service
public class TdsPaymentService {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private TourProgramRepository tourProgramRepository;
    
    @Autowired
    private TourProgramLogRepository tourProgramLogRepository;
    
    @Autowired
    private TourProgramNoteRepository tourProgramNoteRepository;
    
    @Autowired
    private TourProgramDetailsRepository tourProgramDetailsRepository;
    
    
    @Autowired
    private EntityMasterRepository entityMasterRepository;

    @Autowired
    private UserMasterRepository userMasterRepository;
    
    @Autowired
    EmployeeMasterRepository  employeeMasterRepository;
    
    @Autowired
    EntityBankBranchRepository entityBankBranchRepository;
    
    @Autowired
    VoucherNoteRepository voucherNoteRepository;
    
    @Autowired
    VoucherLogRepository voucherLogRepository;
    
    @Autowired
    EntityMasterService entityMasterService;
    
    @Autowired
    LoginService loginService;
    
    @Autowired
	OperationService operationService;
    
    @Autowired
    ApplicationQueryRepository applicationQueryRepository;
    
    @Autowired
	ResponseWrapper responseWrapper;
    
    @Getter
	@Setter
	EmployeeMaster empMaster;
    
    @Getter
    @Setter
    EmployeeMasterRepository empRepository;
    
    @Autowired
    SupplierMasterRepository supplierMasterRepository;
    
    @Autowired
    TdsPaymentRepository tdsPaymentRepository;
    
    @Autowired
	AppQueryRepository appQueryRepository;
    
    @Autowired
	JdbcTemplate jdbcTemplate;
    
    @Autowired
	SequenceConfigRepository sequenceConfigRepository;
    
    @Autowired
	VoucherRepository voucherRepository;

	@Autowired
	VoucherDetailsRepository voucherDetailsRepository;
	
	@Autowired
	PaymentModeRepository paymentModeRepository;
	
	@Autowired
	VoucherTypeRepository voucherTypeRepository;
	
	@Autowired
	PurchaseInvoiceRepository purchaseInvoiceRepository;
   


    public BaseDTO getTdsPaymentDetails(PaginationDTO request) {
        BaseDTO baseDTO = new BaseDTO();
       
        log.info("<<====  GetTDSPaymentService ---  getAll ====## STARTS");
        try
        {
            if (request == null) {
                throw new RestException(ErrorDescription.REQUEST_OBJECT_SHOULD_NOT_EMPTY);
            }
            Integer totalRecords = 0;
            Criteria criteria = null;

            Session session = entityManager.unwrap(Session.class);
            criteria = session.createCriteria(TdsPayment.class, "tdsPayment");
            criteria.createAlias("tdsPayment.entityMaster", "entityMaster");
            //criteria.createAlias("tdsPayment.purchaseInvoice", "purchaseInvoice");
            criteria.createAlias("tdsPayment.supplierMaster", "supplierMaster");
            criteria.createAlias("tdsPayment.voucher", "voucher");
            log.info(" Criterai Search Started ");
            
            if(request.getFilters().get("entityCode") != null && !request.getFilters().get("entityCode").toString().trim().isEmpty()){
            	log.info("entityCode and name add to criteria:" + request.getFilters().get("entityCode"));
                String codeOrName =(String)request.getFilters().get("entityCode");
            	
				Criterion entityCode = Restrictions
						.like("entityMaster.code", "%" + codeOrName.trim() + '%').ignoreCase();
				Criterion entityName = Restrictions
						.like("entityMaster.name", "%" + codeOrName.trim() + '%').ignoreCase();
				criteria.add(Restrictions.or(entityCode, entityName));
			}

            
            if(request.getFilters().get("supplierCode") != null && !request.getFilters().get("supplierCode").toString().trim().isEmpty()){
                log.info("supplierCode and supplierName add to criteria:" + request.getFilters().get("supplierCode"));
                String codeOrName =(String)request.getFilters().get("supplierCode");
            	
				Criterion supCode = Restrictions
						.like("supplierMaster.code", "%" + codeOrName.trim() + '%').ignoreCase();
				Criterion supName = Restrictions
						.like("supplierMaster.name", "%" + codeOrName.trim() + '%').ignoreCase();
				criteria.add(Restrictions.or(supCode, supName));
			}
            
            
            if(request.getFilters().get("month") != null && !request.getFilters().get("month").toString().trim().isEmpty()){
            	log.info("paymentMonth add to criteria:"+request.getFilters().get("month"));
            	String month =(String)request.getFilters().get("month");
				criteria.add(Restrictions.like("tdsPayment.month", "%" + month.trim() + '%').ignoreCase());
			}
            
            
            if(request.getFilters().get("year") != null && !request.getFilters().get("year").toString().trim().isEmpty()){
                log.info("payment year add to criteria:"+request.getFilters().get("year"));
                String year =(String)request.getFilters().get("year");
            	
				criteria.add(Restrictions.eq("tdsPayment.year", year));
			}
            
            
            if(request.getFilters().get("voucherNumber") != null && !request.getFilters().get("voucherNumber").toString().trim().isEmpty()){
                log.info("voucherName to criteria:"+request.getFilters().get("voucherNumber"));
                String voucherNumber =(String)request.getFilters().get("voucherNumber");
                
				criteria.add(Restrictions.eq("voucher.referenceNumber", voucherNumber));
			}
            
            if (request.getFilters().get("voucherDate") != null && !request.getFilters().get("voucherDate").toString().trim().isEmpty()) {
                log.info("created date added to criteria:" + request.getFilters().get("voucherDate"));
                Date date = new Date((long) request.getFilters().get("voucherDate"));
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                String strDate = dateFormat.format(date);
                Date minDate = dateFormat.parse(strDate);
                Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
                criteria.add(Restrictions.conjunction().add(Restrictions.ge("voucher.createdDate", minDate))
                        .add(Restrictions.lt("voucher.createdDate", maxDate)));
            }
			
            criteria.setProjection(Projections.rowCount());
            totalRecords = ((Long) criteria.uniqueResult()).intValue();
            criteria.setProjection(null);
            
            log.info("tdspayment total records:"+totalRecords);

            Integer pageNo = request.getPageNo();
            Integer pageSize = request.getPageSize();

            if (pageNo != null && pageSize != null) {
                criteria.setFirstResult(pageNo * pageSize);
                criteria.setMaxResults(pageSize);
                log.info("PageNo : [" + pageNo + "] pageSize[" + pageSize + "]");
            }
            
            ProjectionList projectionList = Projections.projectionList();
            projectionList.add(Projections.property("tdsPayment.id"));
            projectionList.add(Projections.property("tdsPayment.month"));
            projectionList.add(Projections.property("tdsPayment.year"));
            projectionList.add(Projections.property("entityMaster.code"));
            projectionList.add(Projections.property("entityMaster.name"));
            projectionList.add(Projections.property("supplierMaster.code"));
            projectionList.add(Projections.property("supplierMaster.name"));
            projectionList.add(Projections.property("voucher.id"));
            projectionList.add(Projections.property("voucher.referenceNumberPrefix"));
            projectionList.add(Projections.property("voucher.createdDate"));
            criteria.setProjection(projectionList);
            
            

            List<?> resultList = criteria.list();
            if (resultList == null || resultList.isEmpty() || resultList.size() == 0) {
                log.info("TdsPayment is null or empty ");
                baseDTO.setStatusCode(ErrorDescription.PRODUCT_VARIETY_LIST_EMPTY.getCode());
                return baseDTO;
            }

            log.info("Tour Program criteria list executed and the list size is  : " + resultList.size());
            List<TdsPaymentResponseDTO> results = new ArrayList<TdsPaymentResponseDTO>();
            Iterator<?> it =  resultList.iterator();
            
            ArrayList<Long> transferID=new ArrayList<Long>();
            while(it.hasNext()) {
                Object ob[] = (Object[]) it.next();

                    TdsPaymentResponseDTO response = new TdsPaymentResponseDTO();
                    response.setId((Long) ob[0]);
                    response.setMonth((String) ob[1]);
                    response.setYear((Long) ob[2]);
                    response.setEntityCode((Integer) ob[3]);
                    response.setEntityName((String) ob[4]);
                    response.setSupplierCode((String) ob[5]);
                    response.setSupplierName((String) ob[6]);
                    response.setVoucherId((Long) ob[7]);
                    response.setVoucherNumber((String) ob[8]);
                    response.setVoucherDate((Date) ob[9]);
    				
                    log.info("voucherNote id:"+response.getVoucherId());
                    VoucherNote voucherNote;
                    VoucherLog voucherLog;
                    try {
                    
                    voucherNote = voucherNoteRepository.findByVoucherIdForTDS(response.getVoucherId());
                    voucherLog =voucherLogRepository.findByVoucherId(response.getVoucherId());
                    
	                    if (voucherLog!=null)
	                    {
	                    	log.info("<<====   TdsPaymentService VoucherLogstatus set and filter =====>>");
	                        String request_status = (String) request.getFilters().get("status");
	                        
	                        if (request_status!=null && !request_status.isEmpty()) {
	                        	log.info("request status :"+request_status);
	                            if (request_status.equalsIgnoreCase(voucherLog.getStatus().toString())) {
	                                response.setStatus(request_status);
	        						response.setRemarks(voucherLog.getRemarks());
	        						if(voucherNote!=null) {
	        							response.setForwardTo(voucherNote.getForwardTo().getId());
	            						response.setNote(voucherNote.getNote());
	            						response.setForwardFor(voucherNote.getFinalApproval());
	            						response.setVoucherStatus(voucherLog.getStatus());
	            						response.setStatus(voucherLog.getStatus().toString());
	        						}
	        						results.add(response);
	                            }
	                        } else {
	                            response.setStatus(voucherLog.getStatus().toString());
	                            if(voucherNote!=null) {
	                            	response.setForwardTo(voucherNote.getForwardTo().getId());
	        						response.setNote(voucherNote.getNote());
	        						response.setForwardFor(voucherNote.getFinalApproval());
	        						response.setVoucherStatus(voucherLog.getStatus());
	        						response.setStatus(voucherLog.getStatus().toString());
	    						}
	                            results.add(response);
	                        }
	                    }
                    }catch(Exception e) {
                    	log.info("Exception while tdsPayment vouchers status");
                    }
                    
            }
            log.info("<<====  GetTDSPaymentService ---  getAll ====## END ");
            baseDTO.setTotalRecords(results.size());
            baseDTO.setResponseContents(results);
            baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
            return baseDTO;
        } catch (Exception ex) { 
        	log.info("Exception while tdsPayment getAll lazy:...."+ex);
        	return null;
        }
    }

    
    public BaseDTO getAllActiveEntityMaster() {
		BaseDTO baseDto = new BaseDTO();
		log.info("<<====  GetTDSPaymentService ---  getAllActiveEntityMaster ====## Starts ");
		try {
			List<EntityMaster> regonList = entityMasterRepository.findEntityByStatus();
			List<EntityMaster> responseList = new ArrayList<>();
			for (EntityMaster reg : regonList) {
				/*reg.getCreatedBy().setRegion(null);
				if (reg.getModifiedBy() != null)
					reg.getModifiedBy().setRegion(null);*/
				reg.setEntityTypeMaster(null);
				responseList.add(reg);
			}
			baseDto.setResponseContents(responseList);
			baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("<<====  GetTDSPaymentService ---  getAllActiveEntityMaster successfull====## End ");
		} catch (Exception exp) {
			log.info("Exception found in entity of TdsPaymentService", exp);
			baseDto.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return baseDto;
	}
    
    
    
    public BaseDTO getSupplierDetails(Long id) {
		BaseDTO baseDto = new BaseDTO();
		log.info("<---- Tds Payment supplier details getfrom tdsPaymentService -->");
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		Integer totalRecords = 0;
		TdsPaymentRequestDTO responseList=new TdsPaymentRequestDTO();
		List<SupplierMaster> supplierList=new ArrayList<>();
		List<String> monthList=new ArrayList<>();
		List<Long> yearList=new ArrayList<>();
		try {

			ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("SUPPLIER_DETAILS_BY_VOUCHER");
			String mainQuery = applicationQuery.getQueryContent();
			log.info("db query----------" + mainQuery);
			
			if(id !=null) {
				log.info("entityId----------" + id);
				mainQuery=mainQuery.replace(":entityid", "'"+ id +"'");
			}
			listofData = jdbcTemplate.queryForList(mainQuery);
			totalRecords = listofData.size();
			log.info("totalRecords from query ----------" + totalRecords);
			for (Map<String, Object> data : listofData) {
		       SupplierMaster supplierMaster=new SupplierMaster();
		       
		       supplierMaster.setName((String) data.get("name"));
		       supplierMaster.setCode((String) data.get("code"));
		       Integer supplierId=(Integer) data.get("id");
		       Long supId=supplierId.longValue();
		       supplierMaster.setId(supId);
		       supplierList.add(supplierMaster);
		      }
			log.info("voucher id:......"+id);
			List<?> dateList=tdsPaymentRepository.getVoucherDate(id);
			Iterator<?> it =  dateList.iterator();
            
			log.info("totalRecords from voucher records ----------" + dateList.size());
			
            while(it.hasNext()) {
                Object ob[] = (Object[]) it.next();
                
                String month=(String) ob[1];
                String years=(String) ob[2];
                Long year=new Long(years);
                monthList.add(month);
                yearList.add(year);
            }
            responseList.setSupplierList(supplierList);
            responseList.setMonthList(monthList);
            responseList.setYearList(yearList);
			baseDto.setResponseContent(responseList);
			
			baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception exp) {
			log.info("Exception found in TdsPayment while getsupplierdetails()", exp);
			baseDto.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return baseDto;
	}
    
    
    
    public BaseDTO getPurchaseDetails(TdsPaymentRequestDTO tdsPaymentDto) {
		BaseDTO baseDto = new BaseDTO();
		log.info("<--- starts getPurchaseDetails() details getfrom tdsPaymentService -->");
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		Integer totalRecords = 0;
		List<TdsPaymentRequestDTO> responseList=new ArrayList<>();
		List<PurchaseInvoice> purchaseList=new ArrayList<>();
		
		try {
			Long entityid=105l;
			String month=tdsPaymentDto.getMonth();
			Long year=tdsPaymentDto.getYear();
			log.info("given month and year......."+month+'-'+year);
			ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("PURCHASE_DETAILS_FOR_TDS");
			String mainQuery = applicationQuery.getQueryContent();
			log.info("db query----------" + mainQuery);
			if(tdsPaymentDto !=null) {
				mainQuery=mainQuery.replace(":entityid", "'"+ tdsPaymentDto.getEntityId() +"'");
				mainQuery=mainQuery.replace(":supid", "'"+ tdsPaymentDto.getSupplierId() +"'");
				log.info("given entityId......."+tdsPaymentDto.getEntityId());
				log.info("given supplierId......."+tdsPaymentDto.getSupplierId());
				if(!month.equals("") && year!=null) {
					String date=month.toUpperCase()+'-'+year;
					log.info("given month and year......."+date);
					mainQuery=mainQuery.replace(":date", "'"+ date +"'");
				}
			}
			listofData = jdbcTemplate.queryForList(mainQuery);
			totalRecords = listofData.size();
			log.info("totalRecords from query ----------" + totalRecords);
			for (Map<String, Object> data : listofData) {
				TdsPaymentRequestDTO requestDto=new TdsPaymentRequestDTO();
				PurchaseInvoice purchase=new PurchaseInvoice();
				Integer id=(Integer) data.get("id");
				Long purId=id.longValue();
				requestDto.setPurcahseId(purId);
				BigDecimal grndTotal=(BigDecimal)data.get("grand_total");
				Double gntotal=grndTotal.doubleValue();
				requestDto.setGrandTotal(gntotal);
				requestDto.setCreatedDate((Date) data.get("created_date"));
				requestDto.setInvoiceNumber((Integer) data.get("invoice_number"));
				requestDto.setInvoiceNumberPrefix((String) data.get("invoice_number_prefix"));
				BigDecimal tdsTotal=(BigDecimal) data.get("sum");
				Double tdstotal=tdsTotal.doubleValue();
				requestDto.setTotalTdsAmount(tdstotal);
				purchaseList.add(purchase);
				responseList=getPurchaseDeatils(tdsPaymentDto);
				
				baseDto.setResponseContents(responseList);
				baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			  }
		}catch (Exception exp) {
			log.info("Exception found in TdsPaymentSerice while getPurchaseDetails() ", exp);
			baseDto.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		
		return baseDto;
	}
    
    public List<TdsPaymentRequestDTO> getPurchaseDeatils(TdsPaymentRequestDTO tdsPaymentDto){
    	List<TdsPaymentRequestDTO> responseList=new ArrayList<>();
		List<PurchaseInvoice> purchaseList=new ArrayList<>();
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		Integer totalRecords = 0;
		
		try {
			Long entityid=105l;
			String month=tdsPaymentDto.getMonth();
			Long year=tdsPaymentDto.getYear();
			log.info("given month and year......."+month+'-'+year);
			ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("PURCHASE_DETAILS_FOR_TDS");
			String mainQuery = applicationQuery.getQueryContent();
			log.info("db query----------" + mainQuery);
			if(tdsPaymentDto !=null) {
				mainQuery=mainQuery.replace(":entityid", "'"+ tdsPaymentDto.getEntityId() +"'");
				mainQuery=mainQuery.replace(":supid", "'"+ tdsPaymentDto.getSupplierId() +"'");
				log.info("given entityId......."+tdsPaymentDto.getEntityId());
				log.info("given supplierId......."+tdsPaymentDto.getSupplierId());
				if(!month.equals("") && year!=null) {
					String date=month.toUpperCase()+'-'+year;
					log.info("given month and year......."+date);
					mainQuery=mainQuery.replace(":date", "'"+ date +"'");
				}
			}
			listofData = jdbcTemplate.queryForList(mainQuery);
			totalRecords = listofData.size();
			log.info("totalRecords from query ----------" + totalRecords);
			for (Map<String, Object> data : listofData) {
				TdsPaymentRequestDTO requestDto=new TdsPaymentRequestDTO();
				PurchaseInvoice purchase=new PurchaseInvoice();
				Integer id=(Integer) data.get("id");
				Long purId=id.longValue();
				requestDto.setPurcahseId(purId);
				BigDecimal grndTotal=(BigDecimal)data.get("grand_total");
				Double gntotal=grndTotal.doubleValue();
				requestDto.setGrandTotal(gntotal);
				requestDto.setCreatedDate((Date) data.get("created_date"));
				requestDto.setInvoiceNumber((Integer) data.get("invoice_number"));
				requestDto.setInvoiceNumberPrefix((String) data.get("invoice_number_prefix"));
				BigDecimal tdsTotal=(BigDecimal) data.get("sum");
				Double tdstotal=tdsTotal.doubleValue();
				requestDto.setTotalTdsAmount(tdstotal);
				purchaseList.add(purchase);
				responseList.add(requestDto);
			  }
		}catch(Exception e) {
			log.info("<--- Exception in the getpurchase invoice -->");
		}
			return responseList;
    }
    
    @Transactional
    public BaseDTO saveTdsPaymentDetails(TdsPaymentRequestDTO data)
    {
    	log.info("<==== Starts TdsPaymentService.save Function Entered =====>");
        BaseDTO baseDTO=new BaseDTO();
        List<CashCreditLimitInterest> result=new ArrayList<>();
        String refNumberPrefix = "";
        Date vFromDate=new Date();
        Date vToDate=new Date();
        Long sequence_value = null;
        try {
        	log.info("<==== TdsPaymentService  Start saveFunction Entered =====>");
            UserMaster user = loginService.getCurrentUser();
            BaseDTO basedto = entityMasterService.getEntityInfoByLoggedInUser(user.getId());
            EntityMaster entity = (EntityMaster) basedto.getResponseContent();
            if(entity!=null) {
                data.setEntityId(entity.getId());
            }
            
            TdsPayment tdsPayment=new TdsPayment();
            
            Voucher voucher = new Voucher();
            
            EntityMaster loginUserEntity = entityMasterRepository
					.findByUserRegion(loginService.getCurrentUser().getId());
            
            SequenceConfig sequenceConfig = sequenceConfigRepository.findBySequenceName(SequenceName.valueOf(SequenceName.TDS_PAYMENT.name()));
            
			if (sequenceConfig == null) {
				throw new RestException(ErrorDescription.SEQUENCE_CONFIG_NOT_EXIST);
			}

			refNumberPrefix = loginUserEntity.getCode() + sequenceConfig.getSeparator() + sequenceConfig.getPrefix()
					 + AppUtil.getCurrentMonthString()+ AppUtil.getCurrentYearString();

			sequence_value = sequenceConfig.getCurrentValue();
			
			sequenceConfig.setCurrentValue(sequenceConfig.getCurrentValue() + 1);

			sequenceConfigRepository.save(sequenceConfig);

			log.info("Voucher Number Prefix  ", refNumberPrefix);
			
			
            if (data!=null) {
            	/*if(data.getTdsId()!=null)
            		tdsPayment = tdsPaymentRepository.findOne(data.getTdsId());*/
            	
            			tdsPayment.setMonth(data.getMonth());
            			tdsPayment.setYear(data.getYear());
            			tdsPayment.setTotalAmount(data.getTotalTdsAmount());
            			tdsPayment.setEntityMaster(entityMasterRepository.getOne(data.getEntityId()));
            			tdsPayment.setSupplierMaster(supplierMasterRepository.getOne(data.getSupplierId()));
            	         
		                voucher.setName("TDS_PAYMENT");
						VoucherType voucherType = voucherTypeRepository.findByName("Payment");
						voucher.setVoucherType(voucherType);
						voucher.setNarration("TDS PAYMENT");
		                voucher.setNetAmount(data.getTotalTdsAmount());
		                
	    				voucher.setReferenceNumberPrefix(refNumberPrefix);
	    				voucher.setReferenceNumber(sequenceConfig.getCurrentValue());
	    				voucher.setFromDate(vFromDate);
	    				voucher.setToDate(vToDate);
	    				voucher.setPaymentMode(data.getPaymentMode());
		    				
		                voucher = voucherRepository.save(voucher);
		                log.info("voucherRepository save:"+voucher);
		                
		                try {
		                	 tdsPayment.setVoucher(voucher);
		                	 tdsPaymentRepository.save(tdsPayment);
		                	 log.info("TdsPaymentRepository saved successfully");
	                    }catch(Exception ex) {
	                    	log.info("<--Exception TdsPaymentRepository PaymentService/ exception :- -->"+ex);
	                    }
		                
		                List<Double> tdsAmnt=data.getTdsTotalAmtList();
		                int i=1;
		                for (PurchaseInvoice invoice : data.getPurchaseInvoiceList()) {
			                	VoucherDetails voucherDetails = new VoucherDetails();
			                	
			                	voucherDetails.setPurchaseInvoice(purchaseInvoiceRepository.getOne(invoice.getId()));
			                	//voucherDetails.setGlAccount(id);
				    			voucherDetails.setAmount(invoice.getNetTotal()); //Set tds total amount set here
								voucherDetails.setVoucher(voucher);
								i=i++;
								try {
									voucherDetailsRepository.save(voucherDetails);
									log.info("voucherDetailsRepository saved successfully");
								}catch(Exception ex) {
									log.info("<--Exception voucherDetailsRepository TdsPaymentService/ exception :- -->"+ex);
								}
		                 }
		                
		    			
						log.info("login id:"+loginUserEntity.getId());
						
						VoucherLog voucherLog = new VoucherLog();
						voucherLog.setVoucher(voucher);
						voucherLog.setStatus(VoucherStatus.SUBMITTED);
						voucherLog.setUserMaster(userMasterRepository.findOne(user.getId()));
						try {
							voucherLogRepository.save(voucherLog);
							log.info("voucherLogRepository saved successfully");
						}catch(Exception ex) {
							log.info("<--Exception voucherLogRepository TdsPaymentService/ exception :- -->"+ex);
						}
						
						
						log.info("voucherLogRepository save:"+voucher);
						VoucherNote voucherNote = new VoucherNote();
						voucherNote.setVoucher(voucher);
						voucherNote.setNote(data.getNote());
						voucherNote.setFinalApproval(data.getForwardFor());
						voucherNote.setForwardTo(userMasterRepository.findOne(data.getForwardTo()));
						try {
							voucherNoteRepository.save(voucherNote);
							log.info("voucherNoteRepository saved successfully");
						}catch(Exception ex) {
							log.info("<--Exception voucherNoteRepository TdsPaymentService/ exception :- -->"+ex);
						}
						
                    baseDTO.setResponseContents(result);
                    baseDTO.setTotalRecords(1);

            }
            baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
            return  baseDTO;
        }
        catch (Exception ex)
        {
            log.info("<-- Exception Tdspayment Service :- -->"+ex);
            baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
            return  baseDTO;
        }
    }

    
   
    
    public BaseDTO getTdsPaymentEditById(Long id) {
		log.info("<--- TdsPaymentServie.getTdsPaymentEditById() starts---->" + id);
		BaseDTO response = new BaseDTO();
		
		TdsPaymentViewDTO tdsPaymentDto=new TdsPaymentViewDTO();
		try {
			
			TdsPayment tdsPayment = tdsPaymentRepository.findOne(id);
			
			String supplierCode=tdsPayment.getSupplierMaster().getCode();
			String supplierName=tdsPayment.getSupplierMaster().getName();
			tdsPayment.setSupplierMaster(null);
			if(tdsPayment!=null) {
				tdsPaymentDto.setTdsPayment(tdsPayment);
				Voucher voucher=voucherRepository.getOne(tdsPayment.getVoucher().getId());
				List<VoucherDetails> voucherDetailsList=voucherDetailsRepository.findVoucherDetailsByVoucherId(voucher.getId());
				VoucherLog voucherLog=voucherLogRepository.findByVoucherId(voucher.getId());
				VoucherNote voucherNote=voucherNoteRepository.findByVoucherId(voucher.getId());
				
				tdsPaymentDto.setSupplierCode(supplierCode);
				tdsPaymentDto.setSupplierName(supplierName);
				tdsPaymentDto.setEntityCode(tdsPayment.getEntityMaster().getCode());
				tdsPaymentDto.setEntityName(tdsPayment.getEntityMaster().getName());
				tdsPaymentDto.setMonth(tdsPayment.getMonth());
				tdsPaymentDto.setYear(tdsPayment.getYear());
				tdsPaymentDto.setTotalTdsAmount(tdsPayment.getTotalAmount());
				tdsPaymentDto.setForwardFor(voucherNote.getFinalApproval());
				tdsPaymentDto.setForwardTo(voucherNote.getForwardTo().getId());
				tdsPaymentDto.setVoucherStatus(voucherLog.getStatus());
				tdsPaymentDto.setStatus(voucherLog.getStatus().toString());
				tdsPaymentDto.setPaymentMode(voucher.getPaymentMode().getPaymentMode());
				tdsPaymentDto.setVoucherLog(voucherLog);
				tdsPaymentDto.setVoucherNote(voucherNote);
				//paymentLsit=getPurchaseDeatils(tdsPaymentDto);
				List<PurchaseInvoice> purchaseList=new ArrayList<>();
				List<Double> amtLsit=new ArrayList<>();
				for(VoucherDetails obj:voucherDetailsList) {
					PurchaseInvoice purchase=new PurchaseInvoice();
					purchase= purchaseInvoiceRepository.getOne(obj.getPurchaseInvoice().getId());
					PurchaseInvoice purchaseObj=new PurchaseInvoice();
					purchaseObj.setInvoiceNumber(purchase.getInvoiceNumber());
					purchaseObj.setId(purchase.getId());
					purchaseObj.setCreatedDate(purchase.getCreatedDate());
					purchaseObj.setInvoiceDate(purchase.getInvoiceDate());
					purchaseObj.setTdsAmount(purchase.getTdsAmount());
					purchaseObj.setInvoiceNumberPrefix(purchase.getInvoiceNumberPrefix());
					purchaseObj.setGrandTotal(purchase.getGrandTotal());
					purchaseList.add(purchaseObj);
					Double amt=obj.getAmount();
					amtLsit.add(amt);
				}
				log.info("phase list:"+purchaseList);
				tdsPaymentDto.setPurchaseInvoiceList(purchaseList);
				tdsPaymentDto.setTdsTotalAmtList(amtLsit);

				List<Map<String, Object>> employeeData = new ArrayList<Map<String, Object>>();
				if(voucher!=null) {
					log.info("<<<:::::::voucher::::not Null::::>>>>"+voucher!=null ? voucher.getId():"Null");
					 ApplicationQuery applicationQueryForlog = applicationQueryRepository.findByQueryName("VOUCHER_LOG_EMPLOYEE_DETAILS");
					 if (applicationQueryForlog == null || applicationQueryForlog.getId() == null) {
						 log.info("Application Query For Log Details not found for query name : " + applicationQueryForlog);
						 response.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
						 return response;
					 }
					 String logquery = applicationQueryForlog.getQueryContent().trim();
					 log.info("<=========VOUCHER_LOG_EMPLOYEE_DETAILS Query Content ======>"+logquery);
					 logquery = logquery.replace(":voucherId", "'"+voucher.getId().toString()+"'");
					 log.info("Query Content For VOUCHER_LOG_EMPLOYEE_DETAILS After replaced plan id View query : " + logquery);
					 employeeData = jdbcTemplate.queryForList(logquery);
					 log.info("<=========VOUCHER_LOG_EMPLOYEE_DETAILS Employee Data======>"+employeeData);
					 response.setTotalListOfData(employeeData);
				}
				
			}
			response.setResponseContent(tdsPaymentDto);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (RestException e) {
			log.error(" Rest Exception Occured ", e);
			response.setStatusCode(e.getStatusCode());
		} catch (Exception e) {
			log.info("Error while retriving getTdsPaymentEditById----->", e);
			response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		
		log.info("<---TdsPaymentService.getTdsPaymentEditById() end---->");
		return responseWrapper.send(response);
	}
    
    
	public BaseDTO getAllEmployee() {
		log.info("EmployeeService.getAllEmployee() Started ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<EmployeeMaster> employeeList = employeeMasterRepository.getAllEmployee();
			if (employeeList != null && employeeList.size()>0) {
				baseDTO.setResponseContent(employeeList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				log.info("TourJournalService.getAllEmployee() Completed");
			}
		} catch (Exception exception) {
			log.error("Exception occurred in EmployeeService.getAllEmployee() -:", exception);
			baseDTO.setStatusCode(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return baseDTO;
	}
	
	
	
	public BaseDTO approveTdsPayment(TdsPaymentViewDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("approveInterestPayment method start=============>" + requestDTO);
			TdsPayment tdsPayment=tdsPaymentRepository.findOne(requestDTO.getTdsPayment().getId());
			Long id=loginService.getCurrentUser().getId();
			Voucher voucher=requestDTO.getTdsPayment().getVoucher();
			VoucherLog voucherLog = new VoucherLog();
			voucherLog.setVoucher(voucherRepository.findOne(tdsPayment.getVoucher().getId()));
			voucherLog.setStatus(requestDTO.getVoucherStatus());
			voucherLog.setRemarks(requestDTO.getRemarks());
			voucherLog.setUserMaster(userMasterRepository.findOne(id));
			voucherLogRepository.save(voucherLog);
			log.info("voucherLog inserted------");
			
			
			
			VoucherNote voucherNote = new VoucherNote();
			voucherNote.setVoucher(voucherRepository.findOne(tdsPayment.getVoucher().getId()));
			voucherNote.setNote(requestDTO.getRemarks());
			voucherNote.setFinalApproval(requestDTO.getForwardFor());
			voucherNote.setForwardTo(userMasterRepository.findOne(requestDTO.getForwardTo()));
			voucherNoteRepository.save(voucherNote);
			log.info("voucherNote inserted------");
	
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (LedgerPostingException l) {
			log.error("approveBankToCash method LedgerPostingException----", l);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("approveBankToCash method exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}
	
	public BaseDTO rejectTdsPayment(TdsPaymentViewDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("rejectInterestPayment method start=============>" + requestDTO);
	
			TdsPayment tdsPayment=tdsPaymentRepository.findOne(requestDTO.getTdsPayment().getId());
			Long id=loginService.getCurrentUser().getId();
			Voucher voucher=tdsPayment.getVoucher();
			VoucherLog voucherLog = new VoucherLog();
			voucherLog.setVoucher(voucherRepository.findOne(tdsPayment.getVoucher().getId()));
			voucherLog.setStatus(requestDTO.getVoucherStatus());
			voucherLog.setRemarks(requestDTO.getRemarks());
			voucherLog.setUserMaster(userMasterRepository.findOne(id));
			voucherLogRepository.save(voucherLog);
			log.info("voucherLog inserted------");
			
			VoucherNote voucherNote = new VoucherNote();
			voucherNote.setVoucher(voucher);
			voucherNote.setNote(requestDTO.getRemarks());
			voucherNote.setFinalApproval(requestDTO.getForwardFor());
			voucherNote.setForwardTo(userMasterRepository.findOne(requestDTO.getForwardTo()));
			voucherNoteRepository.save(voucherNote);
			log.info("voucherNote inserted------");
	
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("rejectInterestPayment  log inserted------");
		} catch (Exception e) {
			log.error("rejectInterestPayment method exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}
}
