package in.gov.cooptex.finance.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.model.GlAccount;
import in.gov.cooptex.core.accounts.model.TaBill;
import in.gov.cooptex.core.accounts.model.TaBillDetails;
import in.gov.cooptex.core.accounts.model.TaBillLog;
import in.gov.cooptex.core.accounts.model.TaBillNote;
import in.gov.cooptex.core.accounts.repository.GlAccountRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.finance.repository.TaBillDetailsRepository;
import in.gov.cooptex.core.finance.repository.TaBillLogRepository;
import in.gov.cooptex.core.finance.repository.TaBillNoteRepository;
import in.gov.cooptex.core.finance.repository.TaBillRepository;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EmployeeIncrementDetails;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeePayDetails;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.AppQueryRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EmployeePayDetailRepository;
import in.gov.cooptex.core.repository.TourProgramRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.ui.GlAccountName;
import in.gov.cooptex.core.utilities.TourProgramConstant;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.LedgerPostingException;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.finance.TaBillDTO;
import in.gov.cooptex.operation.model.TourProgram;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class TaBillService {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	TourProgramRepository tourProgramRepository;

	@Autowired
	EmployeeMasterRepository employeeMasterRepository;

	@Autowired
	TaBillRepository taBillRepository;

	@Autowired
	TaBillDetailsRepository taBillDetailsRepository;

	@Autowired
	TaBillNoteRepository taBillNoteRepository;

	@Autowired
	LoginService loginService;

	@Autowired
	TaBillLogRepository taBillLogRepository;

	@Autowired
	TourProgramRepository tourprogramRepository;

	@Autowired
	UserMasterRepository userMasterRepositor;

	@Autowired
	AppQueryRepository appQueryRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	ResponseWrapper responseWrapper;

	@Autowired
	GlAccountRepository glAccountRepository;

	@Autowired
	EmployeePayDetailRepository employeePayDetailRepository;

	public BaseDTO getAllplancode() {
		log.info("TaBillService:getAllplancode()");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<TourProgram> glAccountList = tourProgramRepository.findAll();
			baseDTO.setResponseContent(glAccountList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {

			log.error("TaBillService getAllplancode RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {

			log.error("TaBillService getAllplancode Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}

		log.info("TaBillService getAllGlAccounts method completed");
		return baseDTO;
	}

	public BaseDTO getAllplancodeByStage() {
		log.info("TaBillService:getAllplancodeByStage()");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<TourProgram> tourProgramList = tourProgramRepository
					.getTourProgramListbyStage(TourProgramConstant.TTP_FINAL_APPROVED);
			baseDTO.setResponseContent(tourProgramList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {

			log.error("TaBillService getAllplancode RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {

			log.error("TaBillService getAllplancodeByStage Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}

		log.info("TaBillService getAllplancodeByStage  method completed");
		return baseDTO;
	}

	public BaseDTO getempById(Long id) {
		log.info("TaBillService:getempById()");
		BaseDTO baseDTO = new BaseDTO();
		try {
			if (id != null) {
				EmployeeMaster empMaster = employeeMasterRepository.findOne(id);

				EmployeeIncrementDetails empincrementdetails = new EmployeeIncrementDetails();

				GlAccount glaccount = glAccountRepository.findByName(GlAccountName.BASIC_PAY);
				if (glaccount != null) {
					EmployeePayDetails employeePayDetail = employeePayDetailRepository.getLatestBasicPay(id,
							glaccount.getId());
					empincrementdetails.setBasicPay(employeePayDetail.getAmount());
				}
				empMaster.getIncrementDetailsList().add(empincrementdetails);

				baseDTO.setResponseContent(empMaster);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (RestException restException) {

			log.error("TaBillService getempById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {

			log.error("TaBillService getempById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}

		log.info("TaBillService getempById method completed");
		return baseDTO;
	}

	public BaseDTO createtabill(TaBillDTO taBillDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("<<<<<<<======tabillService createtabill method Started=====>>");

			TaBill tabill = new TaBill();

			tabill = taBillDTO.getTabill();

			String tourname = taBillDTO.getTabill().getTourProgram().getTourName();
			List<TourProgram> tabillList = tourProgramRepository.getDuplicatetourProgrammePlanCode(tourname);
			if (tabillList != null && tabillList.size() > 0) {
				baseDTO.setStatusCode(MastersErrorCode.DUPLICATE_TA_BILL_PLAN_CODE);
			}
			TourProgram tourpragram = tourprogramRepository.findOne(tabill.getTourProgram().getId());
			if (tabill.getEmpMaster() != null) {
				EmployeeMaster empmaster = employeeMasterRepository.findOne(tabill.getEmpMaster().getId());
				tabill.setEmpMaster(empmaster);
			}
			tabill.setTourProgram(tourpragram);

			tabill = taBillRepository.save(tabill);

			for (TaBillDetails tabilldetail : taBillDTO.getTaBillDetailsList()) {
				tabilldetail.setTaBill(tabill);
				taBillDetailsRepository.save(tabilldetail);
			}

			TaBillNote tabillnote = taBillDTO.getTabillnote();
			tabillnote.setTaBill(tabill);
			// tabillnote.setNote("x");
			UserMaster frwdto = userMasterRepositor.findOne(taBillDTO.getTabillnote().getForwardTo().getId());
			tabillnote.setForwardTo(frwdto);
			taBillNoteRepository.save(tabillnote);
			TaBillLog tabilllog = new TaBillLog();

			tabilllog.setTaBill(tabill);

			tabilllog.setStage(ApprovalStage.SUBMITTED);
			taBillLogRepository.save(tabilllog);
			if (baseDTO.getStatusCode()!=223445860) {
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());	
			}
			
			log.info("<<<<<<<======tabillService createtabill method Ended=====>>" + tabill.getId());
			return baseDTO;
		} catch (Exception e) {
			log.info("<<<<<<<======tabillService createtabill method Ended with error=====>>", e);
			e.printStackTrace();
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			return null;
		}
	}

	public BaseDTO getAll(PaginationDTO paginationDTO) {
		log.info("<<====   TaBillService ---  getAll ====## STARTS");
		BaseDTO baseDTO = null;
		Integer totalRecords = 0;
		List<TaBillDTO> resultList = null;
		try {
			resultList = new ArrayList<TaBillDTO>();
			baseDTO = new BaseDTO();
			Integer start = paginationDTO.getFirst(), pageSize = paginationDTO.getPageSize();
			start = start * pageSize;
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();

			ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("TA_BILL_CLAIM");
			String mainQuery = applicationQuery.getQueryContent();
			log.info("db query----------" + mainQuery);

			// mainQuery = mainQuery.replaceAll(":CHEQUE_TO_BANK",
			// AmountMovementType.CHEQUE_TO_BANK);
			// log.info("after replace query------" + mainQuery);

			if (paginationDTO.getFilters() != null) {
				if (paginationDTO.getFilters().get("status") != null) {
					mainQuery += " and t1l.stage='" + paginationDTO.getFilters().get("status") + "'";
				}
				if (paginationDTO.getFilters().get("taBillType") != null) {
					mainQuery += /*
									 * " and t1.ta_bill_type>= '" + paginationDTO.getFilters().get("taBillType") +
									 * "'";
									 */
							" and upper(t1.ta_bill_type) like upper('%" + paginationDTO.getFilters().get("taBillType")
									+ "%')";
				}
				if (paginationDTO.getFilters().get("tourplancode") != null) {
					mainQuery += " and upper(tp.tour_name) like upper('%"
							+ paginationDTO.getFilters().get("tourplancode") + "%')";
				}
				if (paginationDTO.getFilters().get("claimname") != null) {
					mainQuery += " and upper(t1.claim_name) like upper('%" + paginationDTO.getFilters().get("claimname")
							+ "%')";
				}
				if (paginationDTO.getFilters().get("empCode") != null) {
					mainQuery += " and upper(concat(emp.emp_code,'/',emp.first_name,' ',emp.last_name)) like upper('%"
							+ paginationDTO.getFilters().get("empCode") + "%')";

				}
				if (paginationDTO.getFilters().get("totclaimAmount") != null) {
					mainQuery +=

							" and t1.claim_travel_amount>= '" + paginationDTO.getFilters().get("totclaimAmount") + "'";
				}

				/*
				 * if (paginationDTO.getFilters().get("createdDate") != null) { SimpleDateFormat
				 * format=new SimpleDateFormat("yyyy-MM-dd"); Date fromDate = new Date((long)
				 * paginationDTO.getFilters().get("createdDate"));
				 * log.info(" fromDate Filter : " + fromDate); mainQuery +=
				 * " and att.created_date::date='" + format.format(fromDate) + "'"; }
				 */
			}

			String countQuery = mainQuery.replace(
					"select t1.id,t1.ta_bill_type,tp.tour_name,t1.claim_name,concat(emp.emp_code,'/',emp.first_name,' ',emp.last_name) as emp_code_name,t1.claim_travel_amount,t1.claim_contg_amount,t1l.stage",
					"select count(distinct(t1.id)) as count ");
			log.info("count query... " + countQuery);
			countData = jdbcTemplate.queryForList(countQuery);

			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					totalRecords = Integer.parseInt(data.get("count").toString().replace(",", ""));
			}

			if (paginationDTO.getSortField() == null)
				mainQuery += " order by t1.id desc limit " + pageSize + " offset " + start + ";";

			if (paginationDTO.getSortField() != null && paginationDTO.getSortOrder() != null) {
				log.info("Sort Field:[" + paginationDTO.getSortField() + "] Sort Order:[" + paginationDTO.getSortOrder()
						+ "]");
				if (paginationDTO.getSortField().equals("taBillType")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by t1.ta_bill_type asc  ";
				if (paginationDTO.getSortField().equals("taBillType")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by t1.ta_bill_type desc  ";
				if (paginationDTO.getSortField().equals("tourplancode")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by tp.tour_name asc ";
				if (paginationDTO.getSortField().equals("tourplancode")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by tp.tour_name desc ";
				if (paginationDTO.getSortField().equals("claimname")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by t1.claim_name asc ";
				if (paginationDTO.getSortField().equals("claimname")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by t1.claim_name desc ";
				if (paginationDTO.getSortField().equals("empCode") && paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by emp_code_name ";
				if (paginationDTO.getSortField().equals("empCode") && paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by emp_code_name desc ";
				if (paginationDTO.getSortField().equals("totclaimAmount")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by t1.claim_travel_amount asc ";
				if (paginationDTO.getSortField().equals("totclaimAmount")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by t1.claim_travel_amount desc ";
				if (paginationDTO.getSortField().equals("status") && paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by stage asc ";
				if (paginationDTO.getSortField().equals("status") && paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by stage desc ";
				mainQuery += " limit " + pageSize + " offset " + start + ";";
			}

			log.info("filter query----------" + mainQuery);

			listofData = jdbcTemplate.queryForList(mainQuery);
			log.info("amount transfer list size-------------->" + listofData.size());

			for (Map<String, Object> data : listofData) {
				TaBillDTO response = new TaBillDTO();
				response.setId(Long.valueOf(data.get("id").toString()));
				response.setTaBillType(data.get("ta_bill_type").toString());
				response.setTourplancode(data.get("tour_name").toString());
				response.setTotclaimAmount(Double.valueOf(data.get("claim_travel_amount").toString()));
				if (data.get("claim_name") != null) {
					response.setClaimname(data.get("claim_name").toString());
				}
				response.setEmpCode(data.get("emp_code_name").toString());
				response.setStatus(data.get("stage").toString());

				TaBillNote tabillnote = taBillNoteRepository.getTaBillNoteByTaID(response.getId());

				response.setTabillnote(tabillnote);

				resultList.add(response);
			}

			log.info("final list size---------------" + resultList.size());
			baseDTO.setTotalRecords(totalRecords);
			baseDTO.setResponseContents(resultList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("<<====   ChequeToBankService ---  getAll ====## ENDS");
		} catch (Exception ex) {
			log.error("inside lazy method exception------");
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO getDetailsById(Long id) {
		log.info("<---TaBillService.getDetailsById() starts---->" + id);
		BaseDTO response = new BaseDTO();
		TaBillDTO taBillDTO = new TaBillDTO();
		try {
			TaBill tabill = taBillRepository.findOne(id);
			taBillDTO.setTabill(tabill);
			TaBillLog tabilllog = taBillLogRepository.getTaBillLogByID(id);
			TaBillNote tabillnote = taBillNoteRepository.getTaBillNoteByTaID(id);

			List<TaBillDetails> taBillDetailsList = new ArrayList<TaBillDetails>();
			taBillDetailsList = taBillDetailsRepository.getDetailsListByTaID(id);
			taBillDTO.setTaBillDetailsList(taBillDetailsList);
			taBillDTO.setTabilllog(tabilllog);
			taBillDTO.setTabillnote(tabillnote);
			response.setResponseContent(taBillDTO);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException e) {
			log.error(" Rest Exception Occured ", e);
			response.setStatusCode(e.getStatusCode());
		} catch (Exception e) {
			log.info("Error while retriving getDetailsById----->", e);
			response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<---TaBillService.getDetailsById() end---->");
		return responseWrapper.send(response);
	}

	@Transactional
	public BaseDTO approvetabill(TaBillDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("approvetabill method start=============>" + requestDTO.getId());
			TaBill tabill = taBillRepository.findOne(requestDTO.getId());

			tabill.setApprovedTravelAmount(requestDTO.getTabill().getApprovedTravelAmount());
			tabill.setApprovedContgAmount(requestDTO.getTabill().getApprovedContgAmount());
			taBillRepository.save(tabill);

			TaBillNote taBillNote = new TaBillNote();
			taBillNote.setTaBill(tabill);
			taBillNote.setFinalApproval(requestDTO.getTabillnote().getFinalApproval());
			taBillNote.setNote(requestDTO.getTabillnote().getNote());
			taBillNote.setForwardTo(userMasterRepositor.findOne(requestDTO.getTabillnote().getForwardTo().getId()));
			taBillNoteRepository.save(taBillNote);
			log.info("approvetabill note inserted------");

			TaBillLog taBillLog = new TaBillLog();
			taBillLog.setTaBill(tabill);
			taBillLog.setStage(requestDTO.getTabilllog().getStage());
			taBillLog.setCreatedBy(loginService.getCurrentUser());
			taBillLog.setCreatedByName(loginService.getCurrentUser().getUsername());
			taBillLog.setCreatedDate(new Date());
			taBillLog.setRemarks(requestDTO.getTabilllog().getRemarks());
			taBillLogRepository.save(taBillLog);
			log.info("approvetabill log inserted------");

			log.info("Approval status----------" + requestDTO.getTabilllog().getStage());

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (LedgerPostingException l) {
			log.error("approvetabill method Exception----", l);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("approvetabill method exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO rejecttabill(TaBillDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("rejecttabill method start=============>" + requestDTO.getId());
			TaBill tabill = taBillRepository.findOne(requestDTO.getId());

			TaBillLog taBillLog = new TaBillLog();
			taBillLog.setTaBill(tabill);
			taBillLog.setStage(requestDTO.getTabilllog().getStage());
			taBillLog.setCreatedBy(loginService.getCurrentUser());
			taBillLog.setCreatedByName(loginService.getCurrentUser().getUsername());
			taBillLog.setCreatedDate(new Date());
			taBillLog.setRemarks(requestDTO.getTabilllog().getRemarks());
			taBillLogRepository.save(taBillLog);

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("rejecttabill  log inserted------");
		} catch (Exception e) {
			log.error("rejecttabill method exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

}
