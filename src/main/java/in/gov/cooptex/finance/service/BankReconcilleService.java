package in.gov.cooptex.finance.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.repository.AppConfigRepository;
import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.enums.VoucherTypeDetails;
import in.gov.cooptex.core.accounts.model.PaymentDetails;
import in.gov.cooptex.core.accounts.model.VoucherType;
import in.gov.cooptex.core.accounts.repository.PaymentDetailsRepository;
import in.gov.cooptex.core.accounts.repository.VoucherTypeRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.AppConfig;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.dto.AdvancePaymentDTO;
import in.gov.cooptex.finance.dto.BankReconcilleDTO;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class BankReconcilleService {

	@Autowired
	ResponseWrapper responseWrapper;

	@Autowired
	LoginService loginService;

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	PaymentDetailsRepository paymentDetailsRepository;

	@Autowired
	VoucherTypeRepository voucherTypeRepository;

	@Autowired
	UserMasterRepository userMasterRepository;

	@Autowired
	EntityManager entityManager;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	@Autowired
	AppConfigRepository appConfigRepository;

	public BaseDTO getListofChequeDetails(String chequeType, String fromDate, String toDate) {
		log.info("<--BankReconcilleService() .getListofChequeDetails() Started-->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<PaymentDetails> paymentDetails = null;// paymentDetailsRepository.getPaymentDetailsBetweenCreatedDate(chequeType,
														// fromDate, toDate);
			baseDTO.setResponseContent(paymentDetails);
			log.info("<--getListofChequeDetails() fetch success-->");
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.error("Exception in getListofChequeDetails() ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO getBounceDurationHours() {
		log.info("<--BankReconcilleService() .getListofVoucherTypeList() Started-->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			AppConfig appConfig = appConfigRepository.findByKey("BOUNCE_CHECK_UDPATE_DURATION_HOURS");
			log.info("BOUNCE_CHECK_UDPATE_DURATION_HOURS : " + appConfig.getAppValue());
			baseDTO.setResponseContent(appConfig);
			log.info("<--getListofVoucherTypeList() fetch success-->");
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.error("Exception in getListofVoucherTypeList() ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO getListofVoucherTypeList() {
		log.info("<--BankReconcilleService() .getListofVoucherTypeList() Started-->");
		BaseDTO baseDTO = new BaseDTO();
		try {
//			List<VoucherType> paymentDetails = voucherTypeRepository.findAll();
			List<VoucherType> paymentDetails = voucherTypeRepository.findPaymentAndReciept();
			baseDTO.setResponseContent(paymentDetails);
			log.info("<--getListofVoucherTypeList() fetch success-->");
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.error("Exception in getListofVoucherTypeList() ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO updatePaymentChequeDetails(BankReconcilleDTO bankReconcilleDTO) {
		log.info("<--BankReconcilleService() .update updatePaymentChequeDetails() Started-->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			UserMaster userMaster = userMasterRepository.findOne(loginService.getCurrentUser().getId());
			log.info("<-- paymentDetails Update Start ID is-->" + bankReconcilleDTO.getId() + ":");
			PaymentDetails paymentDetailsReturn = paymentDetailsRepository.findOne(bankReconcilleDTO.getId());

			paymentDetailsReturn.setEffectiveStatus(bankReconcilleDTO.getEffectiveStatus());
			paymentDetailsReturn.setEffectiveDate(bankReconcilleDTO.getEffectiveDate());
			paymentDetailsReturn.setModifiedBy(userMaster);
			paymentDetailsRepository.save(paymentDetailsReturn);

			log.info("<-- paymentDetails Update Successfully -->");
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.error("Exception in updatePaymentChequeDetails() ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO getAdvancePaymentLazy(BankReconcilleDTO paginationDto) {
		log.info(" getallDesignTargetlazy  called...");
		BaseDTO baseDTO = new BaseDTO();
		double totalamountDeposite=0.0;
		try {
			Integer total = 0;
			Integer start = paginationDto.getFirst(), pageSize = paginationDto.getPagesize();
			start = start * pageSize;
			Boolean generateStatus = paginationDto.getGenerateStatus();
			List<Map<String, Object>> subListofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();

			AppConfig appConfig = appConfigRepository.findByKey("BOUNCE_CHECK_UDPATE_DURATION_HOURS");

			Long hoursAppConfig = Long.parseLong(appConfig.getAppValue());
			Date todatyDate = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a");
			int numberOfChequesCleared = 0;
			int numberOfChequesBounced = 0;
			int numberOfChequesDeposited = 0;
			int numberOfChequesNotDeposited = 0;
			

			String queryName = "BANK_CHEQUE_CLEARANCE_LIST";
			String queryNameCount = "BANK_CHEQUE_CLEARANCE_LIST_COUNT_REPLACE";

			ApplicationQuery applicationQuery = applicationQueryRepository.findByQueryName(queryName);
			String query = applicationQuery.getQueryContent().trim();

			applicationQuery = applicationQueryRepository.findByQueryName(queryNameCount);
			String queryCount = applicationQuery.getQueryContent().trim();

			String voucherType = paginationDto.getVoucherType();
			
			if(VoucherTypeDetails.Receipt.toString().equals(voucherType)) {
				query += " where vt.name in ("+"'"+VoucherTypeDetails.Sales.toString()+"','"+
						VoucherTypeDetails.SalesPayment.toString()+"','"+voucherType+"') ";
			} else {
				query += " where vt.name = '" + voucherType + "' ";
			}

			String mainQuery = query;
			String chequeStatusQuery = query;

			Object[] resultObj = entityMasterRepository
					.gettypeCodeIdRegionIdByLoggedInUser(loginService.getCurrentUser().getId());
			Object ob[] = (Object[]) resultObj[0];
			String entityMasterTypeCode = (String) (ob[0] != null ? ob[0] : "");
			Long entityId = ob[1] != null ? Long.valueOf(ob[1].toString()) : null;
			Long regionId = ob[2] != null ? Long.valueOf(ob[2].toString()) : null;

			if (entityMasterTypeCode.equalsIgnoreCase("REGIONAL_OFFICE") && entityId != null) {

				mainQuery += " and em.id in (select id from entity_master em1 where em1.region_id=" + entityId + ")";
				if (regionId != null && regionId > 0) {
					mainQuery += " or em.id in (select id from entity_master em1 where em1.region_id=" + regionId + ")";
				}
			} else if (entityMasterTypeCode.equalsIgnoreCase("HEAD_OFFICE")) {
				log.info("Entity Type Head Office");
			} else {
				return null;
			}

			if (paginationDto != null) {
				if (paginationDto.getId() != null) {
					mainQuery += " and t.id='" + paginationDto.getId() + "'";
				}
				if (paginationDto.getBankReferenceNumber() != null) {
					mainQuery += " and upper(bank_reference_number) like upper('%"
							+ paginationDto.getBankReferenceNumber() + "%')";
				}
				if (paginationDto.getEntityCodeName() != null) {
					mainQuery += " and upper(concat(em.name,' ',em.code)) like upper('%"
							+ paginationDto.getEntityCodeName() + "%')";
				}

				if (paginationDto.getDocumentDate() != null) {
					Date createdDate = paginationDto.getDocumentDate();
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					mainQuery += " and document_date::date = date '" + dateFormat.format(createdDate) + "'";
				}
				if (paginationDto.getNetAmount() != null) {
					Double value = Double.parseDouble(paginationDto.getNetAmount().toString());
					Integer intValue = value.intValue();
					mainQuery += " and cast(net_amount as varchar) like '%" + intValue + "%' ";
				}
				if (paginationDto.getBankName() != null) {
					mainQuery += " and upper(bank.bank_name) like upper('%" + paginationDto.getBankName() + "%')";
				}
				if (paginationDto.getDepositedDate() != null) {
					Date createdDate = paginationDto.getDepositedDate();
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					mainQuery += " and deposited_date::date = date '" + dateFormat.format(createdDate) + "'";
				}
				if(paginationDto.getDepositedYear()!=null) {
					mainQuery +=" and to_char(pd.created_date,'yyyy')='" + paginationDto.getDepositedYear() +"'";
				}
				if(paginationDto.getDepositedMonth()!=null) {
					mainQuery +=" and to_char(pd.created_date,'Mon')='" + paginationDto.getDepositedMonth() +"'";
				}
				if (paginationDto.getBankNameDeposited() != null) {
					mainQuery += " and upper(bbm.branch_name) like upper('%" + paginationDto.getBankNameDeposited()
							+ "%')";
				}
				if (paginationDto.getEffectiveDate() != null) {
					Date createdDate = paginationDto.getEffectiveDate();
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					mainQuery += " and effective_date::date = date '" + dateFormat.format(createdDate) + "'";
				}
				if (paginationDto.getChequeCollectionDate() != null) {
					Date chequeCollectionDate = paginationDto.getChequeCollectionDate();
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					mainQuery += " and pd.created_date::date = date '" + dateFormat.format(chequeCollectionDate) + "'";
				}
				if (paginationDto.getEffectiveStatus() != null) {
					String status = paginationDto.getEffectiveStatus().toString();
					if (status.equalsIgnoreCase("Not Deposited")) {
						mainQuery += " and effective_status is null ";
					} else if (status.equalsIgnoreCase("Deposited")) {
						mainQuery += " and effective_status is null and deposited_date is not null";
					} else {
						mainQuery += " and upper(effective_status) like upper('%" + paginationDto.getEffectiveStatus()
								+ "%')";
					}
				}
			}
			mainQuery += " and date(pd.created_date) between '" + paginationDto.getFromDate() + "' and '"
					+ paginationDto.getToDate() + "' and pm.code='CHEQUE' ";

			List<BankReconcilleDTO> advancePaymentDTOList = new ArrayList<>();
			// String countQuery=mainQuery.replace(queryCount, " select count(*) as count
			// ");
			String countQuery = "select count(*) AS count  from (" + mainQuery + ")t";

			countData = jdbcTemplate.queryForList(countQuery);
			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					total = Integer.parseInt(data.get("count").toString().replace(",", ""));
			}
			chequeStatusQuery = mainQuery;
			String subTotalQuery = mainQuery;
			subListofData = jdbcTemplate.queryForList(subTotalQuery);
			for(Map<String, Object> object:subListofData) {
				if(object.get("net_amount")!=null ) {
//					System.out.println("error aisila dekh bhakua"+object.get("net_amount"));
					double dd=Double.parseDouble(object.get("net_amount").toString());
					totalamountDeposite=totalamountDeposite+dd;
				}
			}
			if (paginationDto.getSortField() == null)
				mainQuery += " order by id desc limit " + pageSize + " offset " + start + ";";

			if (paginationDto.getSortField() != null && paginationDto.getSortOrder() != null) {

				if (paginationDto.getSortField().equals("id") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by id asc ";
				if (paginationDto.getSortField().equals("id") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by id desc ";

				if (paginationDto.getSortField().equals("bankReferenceNumber")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by bank_reference_number asc  ";
				if (paginationDto.getSortField().equals("bankReferenceNumber")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by bank_reference_number desc  ";

				if (paginationDto.getSortField().equals("documentDate")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by document_date asc  ";
				if (paginationDto.getSortField().equals("documentDate")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by document_date desc  ";

				if (paginationDto.getSortField().equals("netAmount")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by net_amount asc  ";
				if (paginationDto.getSortField().equals("netAmount")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by net_amount desc  ";

				if (paginationDto.getSortField().equals("bankName") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by bank_name asc  ";
				if (paginationDto.getSortField().equals("bankName")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by bank_name desc  ";

				if (paginationDto.getSortField().equals("depositedDate")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by deposited_date asc  ";
				if (paginationDto.getSortField().equals("depositedDate")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by deposited_date desc  ";

				if (paginationDto.getSortField().equals("bankNameDeposited")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by bankNameDeposited asc  ";
				if (paginationDto.getSortField().equals("bankNameDeposited")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by bankNameDeposited desc  ";

				if (paginationDto.getSortField().equals("effectiveDate")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by effective_date asc  ";
				if (paginationDto.getSortField().equals("effectiveDate")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by effective_date desc  ";

				if (paginationDto.getSortField().equals("chequeCollectionDate")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by created_date asc  ";
				if (paginationDto.getSortField().equals("chequeCollectionDate")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by created_date desc  ";
				
				if (paginationDto.getSortField().equals("effectiveStatus")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by effective_status asc  ";
				if (paginationDto.getSortField().equals("effectiveStatus")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by effective_status desc  ";
				
				if (paginationDto.getSortField().equals("depositedMonth")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by to_char(pd.created_date,'Mon') asc  ";
				if (paginationDto.getSortField().equals("depositedMonth")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by to_char(pd.created_date,'Mon') desc  ";
				
				if (paginationDto.getSortField().equals("depositedYear")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by to_char(pd.created_date,'yyyy') asc  ";
				if (paginationDto.getSortField().equals("depositedYear")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by to_char(pd.created_date,'yyyy') desc  ";

				mainQuery += " limit " + pageSize + " offset " + start + ";";
			}

			log.info("mainQuery " + mainQuery);
			listofData = jdbcTemplate.queryForList(mainQuery);
			for (Map<String, Object> data : listofData) {
				BankReconcilleDTO advancePaymentDTO = new BankReconcilleDTO();
				advancePaymentDTO.setEditFlag(true);
				advancePaymentDTO.setAddFlag(true);
				advancePaymentDTO.setEffectiveStatus("Not Deposited");
				if (data.get("id") != null)
					advancePaymentDTO.setId(Long.parseLong(data.get("id").toString()));
				if (data.get("bank_reference_number") != null)
					advancePaymentDTO.setBankReferenceNumber(data.get("bank_reference_number").toString());
				if (data.get("document_date") != null)
					advancePaymentDTO.setDocumentDate((Date) data.get("document_date"));
				if (data.get("net_amount") != null) {
					advancePaymentDTO.setNetAmount(Double.parseDouble(data.get("net_amount").toString()));
				}
				if (data.get("bank_name") != null) {
					advancePaymentDTO.setBankName(data.get("bank_name").toString());
				}

				if (data.get("bankNameDeposited") != null) {
					advancePaymentDTO.setBankNameDeposited(data.get("bankNameDeposited").toString());
				}
				if (data.get("effective_date") != null) {
					advancePaymentDTO.setEffectiveDate((Date) data.get("effective_date"));
				} else {
					advancePaymentDTO.setEffectiveDate(null);
				}
				if (data.get("created_date") != null) {
					advancePaymentDTO.setCreatedDate((Date) data.get("created_date"));
					advancePaymentDTO.setChequeCollectionDate((Date) data.get("created_date"));
				}
				if (data.get("deposited_date") != null) {
					advancePaymentDTO.setDepositedDate((Date) data.get("deposited_date"));
					advancePaymentDTO.setAddFlag(false);
				}
				if (data.get("year") != null) {
					advancePaymentDTO.setDepositedYear(data.get("year").toString());
				}
				if (data.get("month") != null) {
					advancePaymentDTO.setDepositedMonth(data.get("Month").toString());
				}

				if (data.get("effective_status") != null) {
					Date dateEffectiveDate = (Date) data.get("effective_date");
					String effectiveStatus = data.get("effective_status").toString();
					advancePaymentDTO.setEffectiveStatus(effectiveStatus);
					advancePaymentDTO.setAddFlag(true);
					long diff = todatyDate.getTime() - dateEffectiveDate.getTime();
					long diffHours = diff / (60 * 60 * 1000);
					if (hoursAppConfig <= diffHours) {
						advancePaymentDTO.setEditFlag(false);
					}

				}
				if (data.get("effective_status") == null && data.get("deposited_date") != null) {
					advancePaymentDTO.setEffectiveStatus("Deposited");
				}
				if (data.get("entityName") != null && data.get("entityCode") != null) {
					advancePaymentDTO.setEntityCodeName(
							data.get("entityCode").toString() + "/" + data.get("entityName").toString());
				}
				if (data.get("voucherDate") != null) {
					Date voucherDate = (Date) data.get("voucherDate");
					if(voucherDate != null) {
						advancePaymentDTO.setVoucherDate(AppUtil.getFirstDateOfMonth(voucherDate));
					}
				}
				advancePaymentDTOList.add(advancePaymentDTO);
			}

			BankReconcilleDTO advancePaymentDTO = new BankReconcilleDTO();
			if (generateStatus == true) {
				listofData = jdbcTemplate.queryForList(chequeStatusQuery);
				for (Map<String, Object> data : listofData) {
					BankReconcilleDTO DTO = new BankReconcilleDTO();
					if (data.get("effective_status") != null) {
						String effectiveStatus = data.get("effective_status").toString();
						if (effectiveStatus.equalsIgnoreCase("Cleared")) {
							numberOfChequesCleared++;
						} else if (effectiveStatus.equalsIgnoreCase("Bounced")) {
							numberOfChequesBounced++;
						}
					} else if (data.get("effective_status") == null && data.get("deposited_date") != null) {
						numberOfChequesDeposited++;
					} else {
						numberOfChequesNotDeposited++;
					}
				}
				advancePaymentDTO.setNumberOfChequesBounced(numberOfChequesBounced);
				advancePaymentDTO.setNumberOfChequesCleared(numberOfChequesCleared);
				advancePaymentDTO.setNumberOfChequesDeposited(numberOfChequesDeposited);
				advancePaymentDTO.setNumberOfChequesNotDeposited(numberOfChequesNotDeposited);
				advancePaymentDTO.setTotalNumberOfCheques(total);
			}
			advancePaymentDTO.setAdvancePaymentDTOList(advancePaymentDTOList);
			baseDTO.setResponseContent(advancePaymentDTO);
			baseDTO.setTotalRecords(total);
			baseDTO.setSumTotal(totalamountDeposite);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (Exception exp) {
			log.error("Exception Cause  : ", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

}
