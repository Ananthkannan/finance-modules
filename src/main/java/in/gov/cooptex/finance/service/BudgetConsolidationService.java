package in.gov.cooptex.finance.service;

import java.math.BigDecimal;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.joda.time.DateTime;
import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.service.NotificationEmailService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.model.BudgetConfig;
import in.gov.cooptex.core.accounts.model.BudgetConsolidation;
import in.gov.cooptex.core.accounts.model.BudgetConsolidationDetails;
import in.gov.cooptex.core.accounts.model.BudgetConsolidationLog;
import in.gov.cooptex.core.accounts.model.BudgetConsolidationNote;
import in.gov.cooptex.core.accounts.model.BudgetRequest;
import in.gov.cooptex.core.accounts.repository.GlAccountRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.finance.repository.BudgetConfigRepository;
import in.gov.cooptex.core.finance.repository.BudgetConsolidationDetailsRepository;
import in.gov.cooptex.core.finance.repository.BudgetConsolidationLogRepository;
import in.gov.cooptex.core.finance.repository.BudgetConsolidationNoteRepository;
import in.gov.cooptex.core.finance.repository.BudgetConsolidationRepository;
import in.gov.cooptex.core.finance.repository.BudgetRequestRepository;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.model.SystemNotification;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.AppQueryRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.SectionMasterRepository;
import in.gov.cooptex.core.repository.SystemNotificationRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.finance.dto.BudgetConsolidationDTO;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class BudgetConsolidationService {

	@Autowired
	BudgetConsolidationRepository budgetConsolidationRepository;

	@Autowired
	ResponseWrapper responseWrapper;

	@Autowired
	BudgetConfigRepository budgetConfigRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	AppQueryRepository appQueryRepository;

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	BudgetConsolidationDetailsRepository budgetConsolidationDetailsRepository;

	@Autowired
	BudgetConsolidationNoteRepository budgetConsolidationNoteRepository;

	@Autowired
	BudgetConsolidationLogRepository budgetConsolidationLogRepository;

	@Autowired
	GlAccountRepository glAccountRepository;

	@Autowired
	SectionMasterRepository sectionMasterRepository;

	@Autowired
	UserMasterRepository userMasterRepository;

	@Autowired
	BudgetRequestRepository budgetRequestRepository;

	@Autowired
	EntityManager entityManager;
	@Autowired
	LoginService loginService;

	@Autowired
	EmployeeMasterRepository employeeMasterRepository;
	
	@Autowired
	SystemNotificationRepository systemNotificationRepository;

	private static final String VIEW_PAGE = "/pages/accounts/budget/viewBudgetRequestConsolidated.xhtml?faces-redirect=true";

	@Autowired
	NotificationEmailService notificationEmailService;

	public BaseDTO getById(BudgetConsolidationDTO budgetConsolidationDTO) {
		log.info("BudgetConsolidationService getById method started [" + budgetConsolidationDTO.getId() + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			
			Long id = budgetConsolidationDTO.getId();
			Long notificationId = budgetConsolidationDTO.getNotificationId();
			if (id != null) {
				BudgetConsolidationDTO consolidationDTO = new BudgetConsolidationDTO();
				BudgetConsolidation budgetConsolidation = budgetConsolidationRepository.getOne(id);
				log.info("budgetConsolidation id-----------" + budgetConsolidation.getId());

				BudgetConsolidationLog budgetConsolidationLog = budgetConsolidationLogRepository
						.fingByConsolidateId(budgetConsolidation.getId());
				BudgetConsolidationNote budgetConsolidationNote = budgetConsolidationNoteRepository
						.fingByConsolidateId(budgetConsolidation.getId());

				consolidationDTO = getBudgetConsolidateDetails(budgetConsolidation.getBudgetConfig().getId());
				log.info("dto value--------" + consolidationDTO);

				consolidationDTO.setStage(budgetConsolidationLog.getStage());
				consolidationDTO.setNote(budgetConsolidationNote.getNote());
				consolidationDTO.setFinalApproval(budgetConsolidationNote.getFinalApproval());
				consolidationDTO.setUserMaster(budgetConsolidationNote.getForwardTo());
				consolidationDTO.setBudgetConfig(budgetConsolidation.getBudgetConfig());
				String fromMonth = getMonthName(Integer.valueOf(budgetConsolidation.getBudgetConfig().getFromMonth()));
				String toMonth = getMonthName(Integer.valueOf(budgetConsolidation.getBudgetConfig().getToMonth()));
				consolidationDTO
						.setBudgetFromDate(fromMonth + "-" + budgetConsolidation.getBudgetConfig().getFromYear());
				consolidationDTO.setBudgetToDate(toMonth + "-" + budgetConsolidation.getBudgetConfig().getToYear());
				consolidationDTO.setId(budgetConsolidation.getId());
				consolidationDTO.setBudgetName(budgetConsolidation.getBudgetConfig().getCode() + "/"
						+ budgetConsolidation.getBudgetConfig().getName());

				List<BudgetConsolidationNote> consolidationNoteList = budgetConsolidationNoteRepository
						.getAllBYConsolidateId(budgetConsolidation.getId());
				consolidationDTO.setConsolidationNoteList(new ArrayList<>());
				log.info("consolidation note list-----------" + consolidationNoteList.size());
				for (BudgetConsolidationNote note : consolidationNoteList) {
					EmployeeMaster employeeMaster = employeeMasterRepository.findByUserId(note.getForwardTo().getId());
					if (employeeMaster != null) {
						if (employeeMaster.getPersonalInfoEmployment() != null) {
							note.setDesignation(employeeMaster.getPersonalInfoEmployment().getDesignation() != null
									? employeeMaster.getPersonalInfoEmployment().getDesignation().getName()
									: null);
						}
					}
					consolidationDTO.getConsolidationNoteList().add(note);
				}

				if (notificationId != null) {
					SystemNotification systemNotification = systemNotificationRepository.findOne(notificationId);
					systemNotification.setNotificationRead(true);
					systemNotificationRepository.save(systemNotification);
				}
				baseDTO.setResponseContent(consolidationDTO);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (RestException restException) {
			log.error("BudgetConsolidationService getById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("BudgetConsolidationService getById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("BudgetConsolidationService getById method completed");
		return responseWrapper.send(baseDTO);
	}

	private BudgetConsolidationDTO getBudgetConsolidateDetails(Long budgetConfigId) {
		log.info("Starts BudgetConsolidation Service getBudgetConsolidateDetails method---- ");
		List<BudgetConsolidationDTO> consolidationFixedList = null;
		List<BudgetConsolidationDTO> consolidationVariableList = null;
		Long totalCount = null;
		BudgetConsolidationDTO budgetConsolidationDTO = new BudgetConsolidationDTO();
		try {
			if (budgetConfigId != null)
				consolidationFixedList = new ArrayList<>();
			consolidationVariableList = new ArrayList<>();
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			budgetConsolidationDTO.setFixedEntityList(new ArrayList<>());
			budgetConsolidationDTO.setVariableEntityList(new ArrayList<>());

			totalCount = entityMasterRepository.getReginalOfficeCount();
			log.info("total regional office Count--------" + totalCount);
			budgetConsolidationDTO.setReginalOfficeCount(totalCount);

			ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("BUDGET_REQUEST_CONSOLIDATION");
			String mainQuery = applicationQuery.getQueryContent();
			log.info("database Query-----" + mainQuery);
			mainQuery = mainQuery.replaceAll("':budgetConfigId'", budgetConfigId.toString());
			log.info("after replace id query--------" + mainQuery);

			listofData = jdbcTemplate.queryForList(mainQuery);
			log.info("list data size----------" + listofData.size());
			for (Map<String, Object> budget : listofData) {
				BudgetConsolidationDTO consolidationDTO = new BudgetConsolidationDTO();
				if (budget.get("expense_category") != null) {
					if (budget.get("expense_category").equals("FIXED")) {
						consolidationDTO.setHeadOfAccount(budget.get("head_of_account").toString());

						Array array = (Array) budget.get("entity");
						Integer[] stringList = (Integer[]) array.getArray();
						List<Integer> fixedEntityList = new ArrayList<>();
						fixedEntityList.addAll(Arrays.asList(stringList));
						consolidationDTO.setFixedEntityList(fixedEntityList);
						budgetConsolidationDTO.getFixedEntityList().addAll(fixedEntityList);

						Array array1 = (Array) budget.get("budget_amount");
						BigDecimal[] doubleList = (BigDecimal[]) array1.getArray();
						List<Double> fixedBudgetAmountList = new ArrayList<Double>();
						for (BigDecimal amount : doubleList) {
							fixedBudgetAmountList.add(amount.doubleValue());
						}
						consolidationDTO.setFixedBudgetAmountList(fixedBudgetAmountList);

						Double totalAmount = 0.0;
						Map<Integer, Double> columnMap = new HashMap<Integer, Double>();
						for (int i = 0; i < fixedEntityList.size(); i++) {
							columnMap.put(fixedEntityList.get(i), fixedBudgetAmountList.get(i));
							totalAmount = totalAmount + fixedBudgetAmountList.get(i);
						}
						consolidationDTO.setColumnMap(columnMap);
						consolidationDTO.setTotalBudgetAmount(totalAmount);
						consolidationFixedList.add(consolidationDTO);
					} else if (budget.get("expense_category").equals("VARIABLE")) {
						consolidationDTO.setHeadOfAccount(budget.get("head_of_account").toString());

						Array array = (Array) budget.get("entity");
						Integer[] stringList = (Integer[]) array.getArray();
						List<Integer> variableEntityList = new ArrayList<>();
						variableEntityList.addAll(Arrays.asList(stringList));
						consolidationDTO.setVariableEntityList(variableEntityList);
						budgetConsolidationDTO.getVariableEntityList().addAll(variableEntityList);

						Array array1 = (Array) budget.get("budget_amount");
						BigDecimal[] doubleList = (BigDecimal[]) array1.getArray();
						List<Double> variableBudgetAmountList = new ArrayList<Double>();
						for (BigDecimal amount : doubleList) {
							variableBudgetAmountList.add(amount.doubleValue());
						}
						consolidationDTO.setVariableBudgetAmountList(variableBudgetAmountList);

						Double totalAmount = 0.0;
						Map<Integer, Double> columnMap = new HashMap<Integer, Double>();
						for (int i = 0; i < variableEntityList.size(); i++) {
							columnMap.put(variableEntityList.get(i), variableBudgetAmountList.get(i));
							totalAmount = totalAmount + variableBudgetAmountList.get(i);
						}
						consolidationDTO.setColumnMap(columnMap);
						consolidationDTO.setTotalBudgetAmount(totalAmount);
						consolidationVariableList.add(consolidationDTO);
					}
				}

			}
			Set<Integer> fixedDublicateList = new HashSet<>();
			fixedDublicateList.addAll(budgetConsolidationDTO.getFixedEntityList());
			budgetConsolidationDTO.getFixedEntityList().clear();
			budgetConsolidationDTO.getFixedEntityList().addAll(fixedDublicateList);

			Set<Integer> variableDublicateList = new HashSet<>();
			variableDublicateList.addAll(budgetConsolidationDTO.getVariableEntityList());
			budgetConsolidationDTO.getVariableEntityList().clear();
			budgetConsolidationDTO.getVariableEntityList().addAll(variableDublicateList);

			log.info("Fixed Expense list--------------" + consolidationFixedList.size());
			log.info("Variable Expense list--------------" + consolidationVariableList.size());
			budgetConsolidationDTO.setConsolidationFixedList(consolidationFixedList);
			budgetConsolidationDTO.setConsolidationVariableList(consolidationVariableList);

		} catch (Exception e) {
			log.error("getBudgetConsolidateDetails method exception----", e);
		}
		return budgetConsolidationDTO;
	}

	public BaseDTO deleteById(Long id) {
		log.info("BudgetConsolidationService deleteById method started [" + id + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			// Validate.notNull(id, ErrorDescription.BUDGET_CONSOLIDATION_ID_EMPTY);
			budgetConsolidationRepository.delete(id);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("BudgetConsolidationService deleteById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (DataIntegrityViolationException exception) {
			log.error("BudgetConsolidationService deleteById DataIntegrityViolationException ", exception);
			if (exception.getCause().getCause() instanceof PSQLException) {
				baseDTO.setStatusCode(ErrorDescription.CANNOT_DELETE_REFERENCED_RECORD.getErrorCode());
			} else {
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			}
		} catch (Exception exception) {
			log.error("BudgetConsolidationService deleteById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("BudgetConsolidationService deleteById method completed");
		return responseWrapper.send(baseDTO);
	}

	@Transactional
	public BaseDTO createBudgetConsolidation(BudgetConsolidationDTO budgetConsolidationDTO) {
		log.info("---------createBudgetConsolidation method starts---------");
		BaseDTO baseDTO = new BaseDTO();
		List<BudgetConsolidationDetails> budgetConsolidationDetailsList = null;
		try {
			budgetConsolidationDetailsList = new ArrayList<BudgetConsolidationDetails>();
			if (budgetConsolidationDTO.getBudgetConfig() != null) {
				BudgetConsolidation budgetConsolidation = new BudgetConsolidation();
				budgetConsolidation.setActiveStatus(true);
				budgetConsolidation.setBudgetConfig(
						budgetConfigRepository.findOne(budgetConsolidationDTO.getBudgetConfig().getId()));
				budgetConsolidationRepository.save(budgetConsolidation);
				log.info("BudgetConsolidation inserted-----" + budgetConsolidation.getId());

				for (BudgetConsolidationDTO consolidationDTO : budgetConsolidationDTO.getConsolidationFixedList()) {
					for (int i = 0; i < consolidationDTO.getFixedEntityList().size(); i++) {
						BudgetConsolidationDetails budgetConsolidationDetails = new BudgetConsolidationDetails();
						budgetConsolidationDetails.setGlAccount(
								glAccountRepository.findByAccountName(consolidationDTO.getHeadOfAccount()));
						EntityMaster entityMaster = entityMasterRepository
								.findByCode(consolidationDTO.getFixedEntityList().get(i));
						log.info("entity id----------------" + entityMaster.getId());
						budgetConsolidationDetails.setEntityMaster(entityMaster);
						if (entityMaster.getEntityTypeMaster().getEntityCode().equals("HEAD_OFFICE")) {
							BudgetRequest budgetRequest = budgetRequestRepository.getBudgetConfigId(
									budgetConsolidationDTO.getBudgetConfig().getId(), entityMaster.getId());
							SectionMaster sectionMaster = budgetRequest.getSectionMaster();
							log.info("section id----------" + sectionMaster.getId());
							budgetConsolidationDetails.setSectionMaster(sectionMaster);
						}
						budgetConsolidationDetails.setBudgetAmount(consolidationDTO.getFixedBudgetAmountList().get(i));
						budgetConsolidationDetails.setBudgetConsolidation(budgetConsolidation);
						budgetConsolidationDetailsList.add(budgetConsolidationDetails);
					}
				}

				for (BudgetConsolidationDTO consolidationDTO : budgetConsolidationDTO.getConsolidationVariableList()) {
					for (int i = 0; i < consolidationDTO.getVariableEntityList().size(); i++) {
						BudgetConsolidationDetails budgetConsolidationDetails = new BudgetConsolidationDetails();
						budgetConsolidationDetails.setGlAccount(
								glAccountRepository.findByAccountName(consolidationDTO.getHeadOfAccount()));
						EntityMaster entityMaster = entityMasterRepository
								.findByCode(consolidationDTO.getVariableEntityList().get(i));
						log.info("entity id----------------" + entityMaster.getId());
						budgetConsolidationDetails.setEntityMaster(entityMaster);
						if (entityMaster.getEntityTypeMaster().getEntityCode().equals("HEAD_OFFICE")) {
							BudgetRequest budgetRequest = budgetRequestRepository.getBudgetConfigId(
									budgetConsolidationDTO.getBudgetConfig().getId(), entityMaster.getId());
							SectionMaster sectionMaster = budgetRequest.getSectionMaster();
							log.info("section id----------" + sectionMaster.getId());
							budgetConsolidationDetails.setSectionMaster(sectionMaster);
						}
						budgetConsolidationDetails
								.setBudgetAmount(consolidationDTO.getVariableBudgetAmountList().get(i));
						budgetConsolidationDetails.setBudgetConsolidation(budgetConsolidation);
						budgetConsolidationDetailsList.add(budgetConsolidationDetails);
					}
				}
				log.info("budgetConsolidationDetailsList list size========>" + budgetConsolidationDetailsList.size());
				if (budgetConsolidationDetailsList.size() > 0) {
					budgetConsolidationDetailsRepository.save(budgetConsolidationDetailsList);
					log.info("--------budgetConsolidationDetails saved-----------");
				}

				BudgetConsolidationNote budgetConsolidationNote = new BudgetConsolidationNote();
				budgetConsolidationNote.setBudgetConsolidation(budgetConsolidation);
				budgetConsolidationNote.setFinalApproval(budgetConsolidationDTO.getFinalApproval());
				budgetConsolidationNote
						.setForwardTo(userMasterRepository.findOne(budgetConsolidationDTO.getUserMaster().getId()));
				budgetConsolidationNote.setNote(budgetConsolidationDTO.getNote());
				budgetConsolidationNoteRepository.save(budgetConsolidationNote);
				log.info("---------budgetConsolidationNote saved--------");

				BudgetConsolidationLog budgetConsolidationLog = new BudgetConsolidationLog();
				budgetConsolidationLog.setBudgetConsolidation(budgetConsolidation);
				budgetConsolidationLog.setStage("SUBMITTED");
				budgetConsolidationLogRepository.save(budgetConsolidationLog);
				log.info("---------budgetConsolidationLog saved--------");

				/*
				 * Send Notification Mail
				 */

				if (budgetConsolidation != null && budgetConsolidation.getId() != null) {
					Map<String, Object> additionalData = new HashMap<>();
					// Long empId = request.getEmpMaster() == null ? null :
					// request.getEmpMaster().getId();
					additionalData.put("Url", VIEW_PAGE + "&budgetConsolidationId=" + budgetConsolidation.getId() + "&");
					notificationEmailService.sendMailAndNotificationForForward(budgetConsolidationNote, budgetConsolidationLog,
							additionalData);
				}

				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (DataIntegrityViolationException exception) {
			log.error("BudgetConsolidationService createBudgetConsolidation DataIntegrityViolationException ",
					exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("BudgetConsolidationService createBudgetConsolidation exception ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO getActiveBudgetConsolidation() {
		log.info("BudgetConsolidationService:getActiveBudgetConsolidation()");
		BaseDTO baseDTO = new BaseDTO();
		try {

			List<BudgetConsolidation> budgetConsolidationList = budgetConsolidationRepository
					.getAllActiveBudgetConsolidation();
			baseDTO.setResponseContent(budgetConsolidationList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {

			log.error("BudgetConsolidationService getActiveBudgetConsolidationRestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {

			log.error("BudgetConsolidationService getActiveBudgetConsolidationException ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}

		log.info("BudgetConsolidationServicegetActiveBudgetConsolidationmethod completed");
		return baseDTO;

	}

	public BaseDTO getBudgetNameList() {
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<BudgetConfig> budgetConfigList = budgetConfigRepository.getAllActiveBudgetConfig();
			baseDTO.setResponseContents(budgetConfigList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("BudgetConsolidationService getBudgetNameList ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	/**
	 * Get BudgetRequest Values By BudgetConfig Id
	 */
	public BaseDTO getBudgetDetails(Long budgetConfigId) {
		log.info("Starts BudgetConsolidation Service getBudgetDetails method---- ");
		BaseDTO baseDTO = null;
		List<BudgetConsolidationDTO> consolidationFixedList = null;
		List<BudgetConsolidationDTO> consolidationVariableList = null;
		Long totalCount = null;
		try {
			if (budgetConfigId != null)
				baseDTO = new BaseDTO();
			consolidationFixedList = new ArrayList<>();
			consolidationVariableList = new ArrayList<>();
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			BudgetConsolidationDTO budgetConsolidationDTO = new BudgetConsolidationDTO();
			budgetConsolidationDTO.setFixedEntityList(new ArrayList<>());
			budgetConsolidationDTO.setVariableEntityList(new ArrayList<>());

			BudgetConsolidation budgetConsolidation = budgetConsolidationRepository
					.findByBudgetConfigId(budgetConfigId);
			log.info("BudgetConsolidation objcet value---------" + budgetConsolidation);
			if (budgetConsolidation == null) {
				totalCount = entityMasterRepository.getReginalOfficeCount();
				log.info("total regional office Count--------" + totalCount);
				budgetConsolidationDTO.setReginalOfficeCount(totalCount);

				ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("BUDGET_REQUEST_CONSOLIDATION");
				String mainQuery = applicationQuery.getQueryContent();
				log.info("database Query-----" + mainQuery);
				mainQuery = mainQuery.replaceAll("':budgetConfigId'", budgetConfigId.toString());
				log.info("after replace id query--------" + mainQuery);

				listofData = jdbcTemplate.queryForList(mainQuery);
				log.info("list data size----------" + listofData.size());
				for (Map<String, Object> budget : listofData) {
					BudgetConsolidationDTO consolidationDTO = new BudgetConsolidationDTO();
					if (budget.get("expense_category") != null) {
						if (budget.get("expense_category").equals("FIXED")) {
							consolidationDTO.setHeadOfAccount(budget.get("head_of_account").toString());

							Array array = (Array) budget.get("entity");
							Integer[] stringList = (Integer[]) array.getArray();
							List<Integer> fixedEntityList = new ArrayList<>();
							fixedEntityList.addAll(Arrays.asList(stringList));
							consolidationDTO.setFixedEntityList(fixedEntityList);
							budgetConsolidationDTO.getFixedEntityList().addAll(fixedEntityList);

							Array array1 = (Array) budget.get("budget_amount");
							BigDecimal[] doubleList = (BigDecimal[]) array1.getArray();
							List<Double> fixedBudgetAmountList = new ArrayList<Double>();
							for (BigDecimal amount : doubleList) {
								fixedBudgetAmountList.add(amount.doubleValue());
							}
							consolidationDTO.setFixedBudgetAmountList(fixedBudgetAmountList);

							Double totalAmount = 0.0;
							Map<Integer, Double> columnMap = new HashMap<Integer, Double>();
							for (int i = 0; i < fixedEntityList.size(); i++) {
								columnMap.put(fixedEntityList.get(i), fixedBudgetAmountList.get(i));
								totalAmount = totalAmount + fixedBudgetAmountList.get(i);
							}
							consolidationDTO.setColumnMap(columnMap);
							consolidationDTO.setTotalBudgetAmount(totalAmount);
							consolidationFixedList.add(consolidationDTO);
						} else if (budget.get("expense_category").equals("VARIABLE")) {
							consolidationDTO.setHeadOfAccount(budget.get("head_of_account").toString());

							Array array = (Array) budget.get("entity");
							Integer[] stringList = (Integer[]) array.getArray();
							List<Integer> variableEntityList = new ArrayList<>();
							variableEntityList.addAll(Arrays.asList(stringList));
							consolidationDTO.setVariableEntityList(variableEntityList);
							budgetConsolidationDTO.getVariableEntityList().addAll(variableEntityList);

							Array array1 = (Array) budget.get("budget_amount");
							BigDecimal[] doubleList = (BigDecimal[]) array1.getArray();
							List<Double> variableBudgetAmountList = new ArrayList<Double>();
							for (BigDecimal amount : doubleList) {
								variableBudgetAmountList.add(amount.doubleValue());
							}
							consolidationDTO.setVariableBudgetAmountList(variableBudgetAmountList);

							Double totalAmount = 0.0;
							Map<Integer, Double> columnMap = new HashMap<Integer, Double>();
							for (int i = 0; i < variableEntityList.size(); i++) {
								columnMap.put(variableEntityList.get(i), variableBudgetAmountList.get(i));
								totalAmount = totalAmount + variableBudgetAmountList.get(i);
							}
							consolidationDTO.setColumnMap(columnMap);
							consolidationDTO.setTotalBudgetAmount(totalAmount);
							consolidationVariableList.add(consolidationDTO);
						}
					}

				}
				Set<Integer> fixedDublicateList = new HashSet<>();
				fixedDublicateList.addAll(budgetConsolidationDTO.getFixedEntityList());
				budgetConsolidationDTO.getFixedEntityList().clear();
				budgetConsolidationDTO.getFixedEntityList().addAll(fixedDublicateList);

				Set<Integer> variableDublicateList = new HashSet<>();
				variableDublicateList.addAll(budgetConsolidationDTO.getVariableEntityList());
				budgetConsolidationDTO.getVariableEntityList().clear();
				budgetConsolidationDTO.getVariableEntityList().addAll(variableDublicateList);

				log.info("Fixed Expense list--------------" + consolidationFixedList.size());
				log.info("Variable Expense list--------------" + consolidationVariableList.size());
				budgetConsolidationDTO.setConsolidationFixedList(consolidationFixedList);
				budgetConsolidationDTO.setConsolidationVariableList(consolidationVariableList);
				baseDTO.setResponseContent(budgetConsolidationDTO);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			} else {
				baseDTO.setStatusCode(ErrorDescription.BUDGET_CONSOLIDATION_EXISTS.getErrorCode());
			}
		} catch (Exception e) {
			log.error("getBudgetDetails method exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO lazyLoadBudgetConsolidation(PaginationDTO paginationDTO) {
		log.info("------------lazyLoadBudgetConsolidation service method starts---------------");
		BaseDTO baseDTO = new BaseDTO();
		List<BudgetConsolidationDTO> budgetConsolidationDTOList = null;
		Integer totalRecords = 0;
		try {
			budgetConsolidationDTOList = new ArrayList<BudgetConsolidationDTO>();
			Integer start = paginationDTO.getFirst(), pageSize = paginationDTO.getPageSize();
			start = start * pageSize;
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();
			
			UserMaster userMaster = loginService.getCurrentUser();
			Long loginUserId = userMaster == null ? null : userMaster.getId();

			ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("BUDGET_CONSOLIDATION_DETAILS");
			String mainQuery = applicationQuery.getQueryContent();
			
			mainQuery = StringUtils.replace(mainQuery, ":userId", String.valueOf(loginUserId));
			log.info("db query----------" + mainQuery);

			if (paginationDTO.getFilters() != null) {
				if (paginationDTO.getFilters().get("budgetName") != null) {
					mainQuery += " and bc.code='" + paginationDTO.getFilters().get("budgetName") + "'";
				}
				if (paginationDTO.getFilters().get("stage") != null) {
					mainQuery += " and bcl.stage='" + paginationDTO.getFilters().get("stage") + "'";
				}
				if (paginationDTO.getFilters().get("budgetFromDate") != null) {
					Integer month = 0;
					String monthName = paginationDTO.getFilters().get("budgetFromDate").toString();
					monthName = monthName.substring(0, 1).toUpperCase() + monthName.substring(1);
					Map<String, Integer> monthNames = Util.getMonthIdsByName();
					for (Map.Entry<String, Integer> entry : monthNames.entrySet()) {
						if (entry.getKey().contains(monthName)) {
							month = entry.getValue();
						}
					}
					mainQuery += " and bc.from_month='" + month + "'";
				}
				if (paginationDTO.getFilters().get("budgetToDate") != null) {
					Integer month = 0;
					String monthName = paginationDTO.getFilters().get("budgetToDate").toString();
					monthName = monthName.substring(0, 1).toUpperCase() + monthName.substring(1);
					Map<String, Integer> monthNames = Util.getMonthIdsByName();
					for (Map.Entry<String, Integer> entry : monthNames.entrySet()) {
						if (entry.getKey().contains(monthName)) {
							month = entry.getValue();
						}
					}
					mainQuery += " and bc.to_month='" + month + "'";
				}
			}
			
			String countQuery = "select count(*) as count from (" + mainQuery + ")t ";

			/*
			 * String countQuery = mainQuery.replace(
			 * "select brc.id as id,bc.code as budget_code,bc.name as budget_name,bc.from_month as from_month,bc.to_month as to_month,bc.from_year as from_year,bc.to_year as to_year,bcl.stage as stage"
			 * , "select count(distinct(brc.id)) as count ");
			 */
			log.info("count query... " + countQuery);
			countData = jdbcTemplate.queryForList(countQuery);

			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					totalRecords = Integer.parseInt(data.get("count").toString().replace(",", ""));
			}
			if (paginationDTO.getSortField() == null)
				mainQuery += " order by brc.id desc limit " + pageSize + " offset " + start + ";";

			if (paginationDTO.getSortField() != null && paginationDTO.getSortOrder() != null) {
				log.info("Sort Field:[" + paginationDTO.getSortField() + "] Sort Order:[" + paginationDTO.getSortOrder()
						+ "]");
				if (paginationDTO.getSortField().equals("budgetName")
						&& paginationDTO.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by bc.code asc  ";
				}
				if (paginationDTO.getSortField().equals("budgetName")
						&& paginationDTO.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by bc.code desc  ";
				}
				if (paginationDTO.getSortField().equals("budgetFromDate")
						&& paginationDTO.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by bc.from_month asc ";
				}
				if (paginationDTO.getSortField().equals("budgetFromDate")
						&& paginationDTO.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by bc.from_month desc ";
				}
				if (paginationDTO.getSortField().equals("budgetToDate")
						&& paginationDTO.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by bc.to_month asc  ";
				}
				if (paginationDTO.getSortField().equals("budgetToDate")
						&& paginationDTO.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by bc.to_month desc  ";
				}
				if (paginationDTO.getSortField().equals("stage") && paginationDTO.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by bcl.stage asc ";
				}
				if (paginationDTO.getSortField().equals("stage") && paginationDTO.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by bcl.stage desc ";
				}
				mainQuery += " limit " + pageSize + " offset " + start + ";";
			}

			log.info("filter query----------" + mainQuery);

			listofData = jdbcTemplate.queryForList(mainQuery);
			log.info("budgetconsolidation list size-------------->" + listofData.size());

			for (Map<String, Object> data : listofData) {
				BudgetConsolidationDTO consolidationDTO = new BudgetConsolidationDTO();
				consolidationDTO.setId(Long.valueOf(data.get("id").toString()));
				consolidationDTO.setStage(data.get("stage").toString());
				consolidationDTO
						.setBudgetName(data.get("budget_code").toString() + "/" + data.get("budget_name").toString());
				String fromMonthName = getMonthName(Integer.valueOf(data.get("from_month").toString()));
				consolidationDTO.setBudgetFromDate(fromMonthName + "-" + data.get("from_year").toString());
				String toMonthName = getMonthName(Integer.valueOf(data.get("to_month").toString()));
				consolidationDTO.setBudgetToDate(toMonthName + "-" + data.get("to_year").toString());
				budgetConsolidationDTOList.add(consolidationDTO);
			}

			log.info("final list size------" + budgetConsolidationDTOList.size());
			baseDTO.setResponseContents(budgetConsolidationDTOList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			baseDTO.setTotalRecords(totalRecords);
		} catch (Exception e) {
			log.error("lazyLoadBudgetConsolidation Inside service Exception ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("-------------lazyLoadBudgetConsolidation service method Ends---------------");
		return baseDTO;
	}

	private String getMonthName(int monthNo) {
		String monthName = DateTime.now().withMonthOfYear(monthNo).toString("MMM");
		return monthName;
	}

	public BaseDTO approveBudgetConsolidate(BudgetConsolidationDTO budgetConsolidationDTO) {
		log.info("approveBudgetConsolidate service method starts------");
		BaseDTO baseDTO = null;
		try {
			baseDTO = new BaseDTO();
			if (budgetConsolidationDTO.getId() != null) {
				BudgetConsolidation budgetConsolidation = budgetConsolidationRepository
						.getOne(budgetConsolidationDTO.getId());
				log.info("budgetConsolidation id-----------" + budgetConsolidation.getId());
				BudgetConsolidationNote budgetConsolidationNote = new BudgetConsolidationNote();
				budgetConsolidationNote.setBudgetConsolidation(budgetConsolidation);
				budgetConsolidationNote.setFinalApproval(budgetConsolidationDTO.getFinalApproval());
				budgetConsolidationNote
						.setForwardTo(userMasterRepository.findOne(budgetConsolidationDTO.getUserMaster().getId()));
				budgetConsolidationNote.setNote(budgetConsolidationDTO.getNote());
				budgetConsolidationNoteRepository.save(budgetConsolidationNote);
				log.info("---------budgetConsolidationNote saved--------");

				BudgetConsolidationLog budgetConsolidationLog = new BudgetConsolidationLog();
				budgetConsolidationLog.setBudgetConsolidation(budgetConsolidation);
				budgetConsolidationLog.setStage(budgetConsolidationDTO.getStage());
				budgetConsolidationLog.setRemarks(budgetConsolidationDTO.getRemarks());
				budgetConsolidationLogRepository.save(budgetConsolidationLog);
				log.info("---------budgetConsolidationLog saved--------");

				if (ApprovalStage.FINALAPPROVED.equals(budgetConsolidationDTO.getStage())) {
					String urlPath = VIEW_PAGE + "&budgetConsolidationId=" + budgetConsolidationDTO.getId()
							+ "&";
					Long toUserId = budgetConsolidation.getCreatedBy().getId();
					notificationEmailService.saveIndividualNotification(loginService.getCurrentUser(), toUserId,
							urlPath, "Budget Consolidation", "Budget Consalidation has been final approved");
				} else {
					Map<String, Object> additionalData = new HashMap<>();
					additionalData.put("Url",
							VIEW_PAGE + "&budgetConsolidationId=" + budgetConsolidationDTO.getId() + "&");
					notificationEmailService.sendMailAndNotificationForForward(budgetConsolidationNote, budgetConsolidationLog,
							additionalData);
				}

				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (Exception e) {
			log.error("approveBudgetConsolidate service method exception---", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO rejectBudgetConsolidate(BudgetConsolidationDTO budgetConsolidationDTO) {
		log.info("rejectBudgetConsolidate service method starts------");
		BaseDTO baseDTO = null;
		try {
			baseDTO = new BaseDTO();
			if (budgetConsolidationDTO.getId() != null) {
				BudgetConsolidation budgetConsolidation = budgetConsolidationRepository
						.getOne(budgetConsolidationDTO.getId());
				log.info("budgetConsolidation id-----------" + budgetConsolidation.getId());

				BudgetConsolidationLog budgetConsolidationLog = new BudgetConsolidationLog();
				budgetConsolidationLog.setBudgetConsolidation(budgetConsolidation);
				budgetConsolidationLog.setStage(budgetConsolidationDTO.getStage());
				budgetConsolidationLog.setRemarks(budgetConsolidationDTO.getRemarks());
				budgetConsolidationLogRepository.save(budgetConsolidationLog);
				log.info("---------budgetConsolidationLog saved--------");

				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (Exception e) {
			log.error("rejectBudgetConsolidate service method exception---", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}
}