package in.gov.cooptex.finance.service;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;

import in.gov.cooptex.common.repository.AppConfigRepository;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.GeneratePdfDTO;
import in.gov.cooptex.core.dto.TaxInvoiceDTO;
import in.gov.cooptex.core.enums.AppConfigEnum;
import in.gov.cooptex.core.enums.ReportNames;
import in.gov.cooptex.core.enums.TaxTypes;
import in.gov.cooptex.core.enums.TemplateCode;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeePersonalInfoEmployment;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.repository.AppQueryRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.SalesInvoiceRepository;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.MisPdfUtil;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.VelocityStringUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.finance.GstPaymentDTO;
import in.gov.cooptex.finance.enums.TaxStatementNames;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class GstPaymentService {

	@Autowired
	SalesInvoiceRepository salesInvoiceRepository;

	@Autowired
	AppQueryRepository appQueryRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	AppConfigRepository appConfigRepository;

	@Autowired
	EntityMasterRepository entityMasterRepository;
	
	@Getter
	@Setter
	EmployeeMaster employee;
	
	@Getter
	@Setter
	String downloadPath;
	
	@Getter
	@Setter
	TaxInvoiceDTO taxInvoiceDTO;
	
	private static final String GST_NUMBER_LIST_BY_ENTITY_LIST_QUERY = "SELECT gst_number FROM entity_master WHERE gst_number IS NOT NULL";

	private static final double TAX_FIVE_PERCENT_VALUE = 0.05;
	private static final double TAX_TWELVE_PERCENT_VALUE = 0.12;
	private static final double TAX_EIGHTEEN_PERCENT_VALUE = 0.18;
	private static final String IS_SUPPLIER_REGISTERED = "YES";

	public BaseDTO getstatment1(GstPaymentDTO gstPaymentDTO) {
		log.info("GstPaymentService:getstatment1()================>Started");
		BaseDTO baseDTO = new BaseDTO();
		GstPaymentDTO reponse = new GstPaymentDTO();
		List<GstPaymentDTO> statement1AList = new ArrayList<>();
		List<GstPaymentDTO> statement1BList = new ArrayList<>();
		List<GstPaymentDTO> statement1CList = new ArrayList<>();
		List<GstPaymentDTO> statement1DList = new ArrayList<>();

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		log.info("Getstatment1()================>Creating Map for Setting Table Column and Value");

		// main is for getting Array Values from DB
		List<Map<String, Object>> main = new ArrayList<Map<String, Object>>();

		// columnSize for Know and set the Column Headers
		List<Map<String, Object>> columnSize = new ArrayList<Map<String, Object>>();

		// Value for Setting the statement==1 values
		List<Map<String, Object>> value = new ArrayList<Map<String, Object>>();
		try {

			String startdate = df.format(gstPaymentDTO.getStartingDate());
			String enddate = df.format(gstPaymentDTO.getEndDate());

			ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("GST_PAYMENT_ST1_MAIN");

			String mainQuery = applicationQuery.getQueryContent();

			log.info("Getstatment1()================>GST_PAYMENT_ST1_MAIN  Query====>>>" + mainQuery);

			log.info("Getstatment1()==========>Starting Date==========>>>" + startdate);
			log.info("Getstatment1()==========>Ending Date=============>>>" + enddate);
			log.info("Getstatment1()===========>Entity ID==============>>>"
					+ gstPaymentDTO.getEntitymaster().getId().toString());
			String mainquery1 = mainQuery.replace(":startdate", startdate);
			String mainquery2 = mainquery1.replace(":enddate", enddate);
			String mainquery3 = mainquery2.replace(":entid", gstPaymentDTO.getEntitymaster().getId().toString());

			main = jdbcTemplate.queryForList(mainquery3);
			log.info("Getstatment1()==========>Main Query Executed=============>>>" + enddate);

			applicationQuery = appQueryRepository.findByQueryName("GST_PAYMENT_ST1_SIZE");
			String sizeQuery = applicationQuery.getQueryContent();

			log.info(
					"Getstatment1()==========>Size QueGstPaymentService:getstatment1ry for define Array Size=============>>>"
							+ sizeQuery);
			String sizeQuery1 = sizeQuery.replace(":startdate", startdate);
			String sizeQuery2 = sizeQuery1.replace(":enddate", enddate);
			String sizeQuery3 = sizeQuery2.replace(":entid", gstPaymentDTO.getEntitymaster().getId().toString());
			columnSize = jdbcTemplate.queryForList(sizeQuery3);
			log.info("Getstatment1()==========>Size Query Executed=============>>>");
			int orgSize = columnSize.size();
			orgSize = orgSize * 3;
			orgSize = orgSize + 5;
			String[] header = new String[columnSize.size() * 3];
			Double[] footer = new Double[columnSize.size() * 3 + 1];
			log.info("Getstatment1()==========>Size for Array Setted=============>>>" + orgSize);

			int old = 0;
			for (Map<String, Object> data : columnSize) {
				header[old] = data.get("tax_percent").toString() + "%-Taxable";
				footer[old] = 0D;
				old++;
				header[old] = TaxTypes.CGST;
				footer[old] = 0D;
				old++;
				header[old] = TaxTypes.SGST;
				footer[old] = 0D;
				old++;
			}
			footer[old] = 0D;
			log.info("Getstatment1()==========>Header Created=============>>>" + header);

			applicationQuery = appQueryRepository.findByQueryName("GST_PAYMENT_ST1_VALUE");
			String valueQuery = applicationQuery.getQueryContent();

			log.info("Getstatment1()==========>Query For getting the CGST,SGST=============>>>" + valueQuery);
			String valueQuery1 = valueQuery.replace(":startdate", startdate);
			String valueQuery2 = valueQuery1.replace(":enddate", enddate);
			String valueQuery3 = valueQuery2.replace(":entid", gstPaymentDTO.getEntitymaster().getId().toString());

			log.info("Getstatment1()==========>Initiating Variables for calculate CGST SGST============>>>"
					+ valueQuery);
			int old2 = 4;
			Double cgstvalue = 0D;
			Double sgstvalue = 0D;
			Double totalvalue = 0D;
			int count = 0, foot = 0;
			for (Map<String, Object> datarow : main) {
				String[] datavalue = new String[orgSize + 1];
				count++;

				old2 = 4;
				cgstvalue = 0D;
				sgstvalue = 0D;
				totalvalue = 0D;
				foot = 0;
				datavalue[0] = Integer.toString(count);
				datavalue[1] = datarow.get("date").toString();
				datavalue[2] = "";
				datavalue[3] = datarow.get("material_value").toString();

				String valueQuery4 = valueQuery3.replace("dates", datarow.get("date").toString());
				footer[foot] = footer[foot] + Double.parseDouble(datarow.get("material_value").toString());

				foot++;
				for (Map<String, Object> datapercent : columnSize) {
					cgstvalue = 0D;
					sgstvalue = 0D;
					totalvalue = 0D;

					String percentage = datapercent.get("tax_percent").toString();

					String valueQuery5 = valueQuery4.replace("percentage", percentage);

					String cgstQuery = valueQuery5.replace("codes", TaxTypes.CGST);
					value = new ArrayList<Map<String, Object>>();
					value = jdbcTemplate.queryForList(cgstQuery);

					int v = value.size();

					if (value.size() > 0) {
						Map<String, Object> obj = value.get(0);
						cgstvalue = Double.parseDouble(obj.get("tax_value").toString());
					} else {
						cgstvalue = 0D;
					}

					String sgstQuery = valueQuery5.replace("codes", TaxTypes.SGST);
					value = new ArrayList<Map<String, Object>>();
					value = jdbcTemplate.queryForList(sgstQuery);
					if (value.size() > 0) {
						Map<String, Object> obj = value.get(0);
						sgstvalue = Double.parseDouble(obj.get("tax_value").toString());
					} else {
						sgstvalue = 0D;
					}
					totalvalue = cgstvalue + sgstvalue;

					datavalue[old2] = Double.toString(totalvalue);
					footer[foot] = footer[foot] + totalvalue;
					foot++;
					old2++;
					datavalue[old2] = Double.toString(cgstvalue);
					footer[foot] = footer[foot] + cgstvalue;
					foot++;
					old2++;
					datavalue[old2] = Double.toString(sgstvalue);
					footer[foot] = footer[foot] + sgstvalue;
					foot++;
					old2++;
				}

				datavalue[old2] = datarow.get("show_room").toString();
				old2++;
				datavalue[old2] = datarow.get("region").toString();

				reponse.getProperty().add(datavalue);
			}
			reponse.setTotalstatement1(footer);
			reponse.setHeader(header);

			// ===========================STATEMENT 1 A================

			applicationQuery = appQueryRepository.findByQueryName("GST_PAYMENT_ST1A");
			String statement1A = applicationQuery.getQueryContent();
			String statement1A_1 = statement1A.replace(":startdate", startdate);
			String statement1A_2 = statement1A_1.replace(":enddate", enddate);
			String statement1a_3 = statement1A_2.replace(":entid", gstPaymentDTO.getEntitymaster().getId().toString());

			List<Map<String, Object>> statement1AMap = jdbcTemplate.queryForList(statement1a_3);
			Double totalstatement1AValue = 0d;
			if (statement1AMap != null) {
				for (Map<String, Object> statemnet1a : statement1AMap) {
					GstPaymentDTO detailstoadd = new GstPaymentDTO();
					detailstoadd.setShowroomName(statemnet1a.get("name").toString());
					detailstoadd.setFromInvoiceName(statemnet1a.get("from_invoice").toString());
					detailstoadd.setToInvoiceName(statemnet1a.get("to_invoice").toString());
					detailstoadd.setTotalValuebyShowroomWise(Double.parseDouble((statemnet1a.get("total").toString())));
					totalstatement1AValue = totalstatement1AValue + detailstoadd.getTotalValuebyShowroomWise();
					statement1AList.add(detailstoadd);
				}
			}

			applicationQuery = appQueryRepository.findByQueryName("GST_PAYMENT_ST1B");
			String statement1B = applicationQuery.getQueryContent();
			String statement1B_1 = statement1B.replace(":startdate", startdate);
			String statement1B_2 = statement1B_1.replace(":enddate", enddate);
			String statement1B_3 = statement1B_2.replace(":entid", gstPaymentDTO.getEntitymaster().getId().toString());
			List<Map<String, Object>> statement1BMap = jdbcTemplate.queryForList(statement1B_3);
			if (statement1BMap != null) {
				for (Map<String, Object> statemnet1b : statement1BMap) {
					GstPaymentDTO detailstoadd = new GstPaymentDTO();
					detailstoadd.setRentedShowroomName(statemnet1b.get("name").toString());
					detailstoadd.setInvoiceNumber(statemnet1b.get("reference_number").toString());
					detailstoadd.setRentAmount(Double.parseDouble(statemnet1b.get("amount_paid").toString()));
					detailstoadd.setInvoiceDate(df.parse(statemnet1b.get("voucher_date").toString()));
					statement1BList.add(detailstoadd);
				}
			}

			log.info("Getstatment1()==========>Initiating Statement 1 C ============>>>" + valueQuery);
			applicationQuery = appQueryRepository.findByQueryName("GST_PAYMENT_ST1C");
			String statement1C = applicationQuery.getQueryContent();
			String statement1C_1 = statement1C.replace(":startdate", startdate);
			String statement1C_2 = statement1C_1.replace(":enddate", enddate);
			String statement1C_3 = statement1C_2.replace(":entid", gstPaymentDTO.getEntitymaster().getId().toString());
			List<Map<String, Object>> statement1CMap = jdbcTemplate.queryForList(statement1C_3);
			if (statement1CMap != null) {
				for (Map<String, Object> statemnet1c : statement1CMap) {
					GstPaymentDTO detailstoadd = new GstPaymentDTO();
					if (statemnet1c.get("hsn_code") != null) {
						detailstoadd.setCode(statemnet1c.get("hsn_code").toString());
					}
					if (statemnet1c.get("description") != null) {
						detailstoadd.setDescription(statemnet1c.get("description").toString());
					}
					if (statemnet1c.get("uom") != null) {
						detailstoadd.setUom(statemnet1c.get("uom").toString());
					}
					if (statemnet1c.get("amount_paid") != null) {
						detailstoadd.setQty(Double.parseDouble(statemnet1c.get("amount_paid").toString()));
					}
					if (statemnet1c.get("item_qty") != null) {
						detailstoadd.setInvoiceValue(Double.parseDouble(statemnet1c.get("item_qty").toString()));
					}
					if (statemnet1c.get("material_value") != null) {
						detailstoadd.setTaxableValue(Double.parseDouble(statemnet1c.get("material_value").toString()));
					}
					if (statemnet1c.get("cgst") != null) {
						detailstoadd.setCgst(Double.parseDouble(statemnet1c.get("cgst").toString()));
					}
					if (statemnet1c.get("sgst") != null) {
						detailstoadd.setSgst(Double.parseDouble(statemnet1c.get("sgst").toString()));
					}
					if (statemnet1c.get("igst") != null) {
						detailstoadd.setIgst(Double.parseDouble(statemnet1c.get("igst").toString()));
					}
					statement1CList.add(detailstoadd);
				}
			}

			log.info("Getstatment1()==========>Initiating Statement 1 D ============>>>" + valueQuery);
			applicationQuery = appQueryRepository.findByQueryName("GST_PAYMENT_ST1D");
			String statement1D = applicationQuery.getQueryContent();
			String statement1D_1 = statement1D.replace(":startdate", startdate);
			String statement1D_2 = statement1D_1.replace(":enddate", enddate);
			String statement1D_3 = statement1D_2.replace(":entid", gstPaymentDTO.getEntitymaster().getId().toString());
			List<Map<String, Object>> statement1DMap = jdbcTemplate.queryForList(statement1D_3);
			if (statement1DMap != null) {
				for (Map<String, Object> statemnet1c : statement1DMap) {
					GstPaymentDTO detailstoadd = new GstPaymentDTO();
					if (statemnet1c.get("code") != null) {
						detailstoadd.setCode(statemnet1c.get("code").toString());
					}
					if (statemnet1c.get("description") != null) {
						detailstoadd.setDescription(statemnet1c.get("description").toString());
					}
					if (statemnet1c.get("uom") != null) {
						detailstoadd.setUom(statemnet1c.get("uom").toString());
					}
					if (statemnet1c.get("amount_paid") != null) {
						detailstoadd.setQty(Double.parseDouble(statemnet1c.get("amount_paid").toString()));
					}
					if (statemnet1c.get("item_qty") != null) {
						detailstoadd.setInvoiceValue(Double.parseDouble(statemnet1c.get("item_qty").toString()));
					}
					if (statemnet1c.get("material_value") != null) {
						detailstoadd.setTaxableValue(Double.parseDouble(statemnet1c.get("material_value").toString()));
					}
					if (statemnet1c.get("cgst") != null) {
						detailstoadd.setCgst(Double.parseDouble(statemnet1c.get("cgst").toString()));
					}
					if (statemnet1c.get("sgst") != null) {
						detailstoadd.setSgst(Double.parseDouble(statemnet1c.get("sgst").toString()));
					}
					if (statemnet1c.get("igst") != null) {
						detailstoadd.setIgst(Double.parseDouble(statemnet1c.get("igst").toString()));
					}
					statement1DList.add(detailstoadd);
				}
			}

			reponse.setStatement1AList(statement1AList);
			reponse.setStatement1BList(statement1BList);
			reponse.setStatement1CList(statement1CList);
			reponse.setStatement1DList(statement1DList);

			reponse.setTotalValueforShowroomWise(totalstatement1AValue);
			baseDTO.setResponseContent(reponse);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {

			log.error("GstPaymentService:getstatment1 RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {

			log.error("GstPaymentService:getstatment1 Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("GstPaymentService:getstatment1 method completed");
		return baseDTO;
	}

	public BaseDTO getstatment2(GstPaymentDTO gstPaymentDTO) {
		log.info("GstPaymentService:getstatment2()================>Started");
		BaseDTO baseDTO = new BaseDTO();
		GstPaymentDTO reponse = new GstPaymentDTO();
		List<GstPaymentDTO> statement2List = new ArrayList<>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
			String startdate = df.format(gstPaymentDTO.getStartingDate());
			String enddate = df.format(gstPaymentDTO.getEndDate());
			log.info("Getstatment2()==========>Initiating Statement 2 ============>>>");
			ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("GST_PAYMENT_ST2");
			String statement2 = applicationQuery.getQueryContent();
			String statement2_1 = statement2.replace(":startdate", startdate);
			String statement2_2 = statement2_1.replace(":enddate", enddate);
			String statement2_3 = statement2_2.replace(":entid", gstPaymentDTO.getEntitymaster().getId().toString());
			List<Map<String, Object>> statement2Map = jdbcTemplate.queryForList(statement2_3);
			if (statement2Map != null) {
				for (Map<String, Object> statemnet2 : statement2Map) {
					GstPaymentDTO detailstoadd = new GstPaymentDTO();
					if (statemnet2.get("gst_number") != null) {
						detailstoadd.setGstNumber(statemnet2.get("gst_number").toString());
					}
					if (statemnet2.get("reference_number") != null) {
						detailstoadd.setInvoiceNumber(statemnet2.get("reference_number").toString());
					}
					if (statemnet2.get("date") != null) {
						detailstoadd.setInvoiceDate(df.parse(statemnet2.get("date").toString()));
					}
					if (statemnet2.get("item_amount") != null) {
						detailstoadd.setInvoiceValue(Double.parseDouble(statemnet2.get("item_amount").toString()));
					}
					if (statemnet2.get("hsn_code") != null) {
						detailstoadd.setHsnCode(statemnet2.get("hsn_code").toString());
					}
					if (statemnet2.get("dispatched_qty") != null) {
						detailstoadd.setQty(Double.parseDouble(statemnet2.get("dispatched_qty").toString()));
					}
					if (statemnet2.get("name") != null) {
						detailstoadd.setPlaceofsupply(statemnet2.get("name").toString());
					}
					if (statemnet2.get("tax_percent") != null) {
						detailstoadd.setRate(Double.parseDouble(statemnet2.get("tax_percent").toString()));
					}
					if (statemnet2.get("tax_value") != null) {
						detailstoadd.setTaxableValue(Double.parseDouble(statemnet2.get("tax_value").toString()));
					}
					if (statemnet2.get("igst") != null) {
						detailstoadd.setIgst(Double.parseDouble(statemnet2.get("igst").toString()));
					}
					statement2List.add(detailstoadd);
				}
				reponse.setStatement2List(statement2List);
			}
			baseDTO.setResponseContent(reponse);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {

			log.error("GstPaymentService:getstatment2 RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {

			log.error("GstPaymentService:getstatment2 Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("GstPaymentService:getstatment2 method completed");
		return baseDTO;
	}

	public BaseDTO getstatment5(GstPaymentDTO gstPaymentDTO) {
		log.info("GstPaymentService:getstatment5()================>Started");
		BaseDTO baseDTO = new BaseDTO();
		GstPaymentDTO reponse = new GstPaymentDTO();
		List<GstPaymentDTO> statement5List = new ArrayList<>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		double taxable = 0d;
		try {
			String startdate = df.format(gstPaymentDTO.getStartingDate());
			String enddate = df.format(gstPaymentDTO.getEndDate());
			log.info("Getstatment5()==========>Initiating Statement 5 ============>>>");
			ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("GST_PAYMENT_ST5");
			String statement5 = applicationQuery.getQueryContent();
			String statement5_1 = statement5.replace(":startdate", startdate);
			String statement5_2 = statement5_1.replace(":enddate", enddate);
			String statement5_3 = statement5_2.replace(":entid", gstPaymentDTO.getEntitymaster().getId().toString());
			List<Map<String, Object>> statement5Map = jdbcTemplate.queryForList(statement5_3);
			if (statement5Map != null) {
				for (Map<String, Object> statemnet5 : statement5Map) {
					GstPaymentDTO detailstoadd = new GstPaymentDTO();
					// if (statemnet5.get("gst_number") != null) {
					// detailstoadd.setNameofsociety(statemnet5.get("gst_number").toString());
					// }
					if (statemnet5.get("material_value") != null) {
						detailstoadd.setInvoiceValue(Double.parseDouble(statemnet5.get("material_value").toString()));
					}

					if (statemnet5.get("gst_number") != null) {
						detailstoadd.setGstNumber(statemnet5.get("gst_number").toString());
						detailstoadd.setSupplierRegistered(true);
					} else {
						detailstoadd.setSupplierRegistered(false);
					}
					if (statemnet5.get("invoice_number") != null) {
						detailstoadd.setInvoiceNumber(statemnet5.get("invoice_number").toString());
					}
					if (statemnet5.get("date") != null) {
						detailstoadd.setInvoiceDate(df.parse(statemnet5.get("date").toString()));
					}
					if (statemnet5.get("cgst_value") != null) {
						taxable = Double.parseDouble(statemnet5.get("cgst_value").toString());
						detailstoadd.setSgst(Double.parseDouble(statemnet5.get("cgst_value").toString()));
					}
					if (statemnet5.get("sgst_value") != null) {
						detailstoadd.setSgst(Double.parseDouble(statemnet5.get("sgst_value").toString()));
						taxable = taxable + Double.parseDouble(statemnet5.get("sgst_value").toString());
					}
					if (statemnet5.get("igst_value") != null) {
						detailstoadd.setIgst(Double.parseDouble(statemnet5.get("igst_value").toString()));
						taxable = taxable + Double.parseDouble(statemnet5.get("igst_value").toString());
					}
					if (statemnet5.get("cgst_value") != null) {
						detailstoadd.setTaxableValue(Double.parseDouble(statemnet5.get("cgst_value").toString()));
					}
					if (statemnet5.get("igst") != null) {
						detailstoadd.setRate(Double.parseDouble(statemnet5.get("igst").toString()));
					}
					detailstoadd.setTaxableValue(taxable);

					statement5List.add(detailstoadd);
				}
				reponse.setStatement5List(statement5List);
			}
			baseDTO.setResponseContent(reponse);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {

			log.error("GstPaymentService:getstatment5 RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {

			log.error("GstPaymentService:getstatment5 Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("GstPaymentService:getstatment5 method completed");
		return baseDTO;
	}

	private List<TaxInvoiceDTO> getStatementsReport(List<Map<String, Object>> dataMapList, String statementName) {
		log.info("GstPaymentService. getStatementsReport() Starts");
		List<TaxInvoiceDTO> taxInvoiceList = new ArrayList<>();
		try {
			for (Map<String, Object> dataMap : dataMapList) {
				TaxInvoiceDTO taxInvoiceDTO = new TaxInvoiceDTO();
				taxInvoiceDTO.setRegionCode(dataMap.get("Region Code") == null ? null
						: Integer.valueOf(dataMap.get("Region Code").toString()));
				taxInvoiceDTO.setShowroomCode(dataMap.get("Entity Code") == null ? null
						: Integer.valueOf(dataMap.get("Entity Code").toString()));
				taxInvoiceDTO.setGstNumber(dataMap.get("GSTN") == null ? null : String.valueOf(dataMap.get("GSTN")));
				taxInvoiceDTO.setInvoiceDate(
						dataMap.get("Invoice Date") == null ? null : (Date) (dataMap.get("Invoice Date")));
				taxInvoiceDTO.setInvoiceNumber(
						dataMap.get("Invoice Number") == null ? null : String.valueOf(dataMap.get("Invoice Number")));
				taxInvoiceDTO.setInvoiceValue(dataMap.get("Net Value") == null ? 0D
						: Double.parseDouble(dataMap.get("Net Value").toString()));
				taxInvoiceDTO.setTwoPointFivePercentCGSTTaxable(
						dataMap.get("CGST-2.5") == null ? 0D : Double.parseDouble(dataMap.get("CGST-2.5").toString()));
				taxInvoiceDTO.setTwoPointFivePercentSGSTTaxable(
						dataMap.get("SGST-2.5") == null ? 0D : Double.parseDouble(dataMap.get("SGST-2.5").toString()));
				taxInvoiceDTO.setSixPercentCGSTTaxable(
						dataMap.get("CGST-6") == null ? 0D : Double.parseDouble(dataMap.get("CGST-6").toString()));
				taxInvoiceDTO.setSixPercentSGSTTaxable(
						dataMap.get("SGST-6") == null ? 0D : Double.parseDouble(dataMap.get("SGST-6").toString()));
				taxInvoiceDTO.setNinePercentSGSTTaxable(
						dataMap.get("SGST-9") == null ? 0D : Double.parseDouble(dataMap.get("SGST-9").toString()));
				taxInvoiceDTO.setNinePercentCGSTTaxable(
						dataMap.get("CGST-9") == null ? 0D : Double.parseDouble(dataMap.get("CGST-9").toString()));

				if (TaxStatementNames.STATEMENT1.equals(statementName)
						|| TaxStatementNames.STATEMENT1C_HSN_VALUE_BREAKUP.equals(statementName)) {

					taxInvoiceDTO.setHsnCode(dataMap.get("HSN") == null ? "" : dataMap.get("HSN").toString());
					taxInvoiceDTO.setTaxPercentage(dataMap.get("Rate of %") == null ? 0D
							: Double.parseDouble(dataMap.get("Rate of %").toString()));
					taxInvoiceDTO.setQuantity(
							dataMap.get("Qty") == null ? 0D : Double.parseDouble(dataMap.get("Qty").toString()));

					taxInvoiceDTO.setTotalCGSTValue(taxInvoiceDTO.getTwoPointFivePercentCGSTTaxable()
							+ taxInvoiceDTO.getSixPercentCGSTTaxable() + taxInvoiceDTO.getNinePercentCGSTTaxable());

					taxInvoiceDTO.setTotalSGSTValue(taxInvoiceDTO.getTwoPointFivePercentSGSTTaxable()
							+ taxInvoiceDTO.getSixPercentSGSTTaxable() + taxInvoiceDTO.getNinePercentSGSTTaxable());

					double totalFiveTaxValue = taxInvoiceDTO.getTwoPointFivePercentCGSTTaxable()
							+ taxInvoiceDTO.getTwoPointFivePercentSGSTTaxable();

					double totalTwelveTaxValue = taxInvoiceDTO.getSixPercentCGSTTaxable()
							+ taxInvoiceDTO.getSixPercentSGSTTaxable();

					double totalEighteenTaxable = taxInvoiceDTO.getNinePercentCGSTTaxable()
							+ taxInvoiceDTO.getNinePercentSGSTTaxable();

					/*
					 * Taxable Value = CGST 2.5+SGST 2.5 / 5% + CGST 6+SGST 6 / 12% + CGST 9+SGST 9
					 * / 18%
					 */

					double fivePercentTaxable = totalFiveTaxValue / TAX_FIVE_PERCENT_VALUE;
					double twelvePercentTaxable = totalTwelveTaxValue / TAX_TWELVE_PERCENT_VALUE;
					double eighteenPercentTaxable = totalEighteenTaxable / TAX_EIGHTEEN_PERCENT_VALUE;
					double totalTaxableValue = (fivePercentTaxable + twelvePercentTaxable + eighteenPercentTaxable);

					taxInvoiceDTO.setTotalTaxableValue(totalTaxableValue);

					taxInvoiceDTO.setFivePercentTaxable(fivePercentTaxable);
					taxInvoiceDTO.setTwelvePercentTaxable(twelvePercentTaxable);
					taxInvoiceDTO.setEighteenPercentTaxable(eighteenPercentTaxable);
					/*
					 * Invoice Value = Taxable Value + CGST 2.5 + SGST 2.5 + CGST 6 + SGST 6 + CGST
					 * 9 + SGST 9
					 */
					double totalInvoiceValue = totalTaxableValue + taxInvoiceDTO.getTotalCGSTValue()
							+ taxInvoiceDTO.getTotalSGSTValue();
					taxInvoiceDTO.setInvoiceValue(totalInvoiceValue);
				} else if (TaxStatementNames.STATEMENT5_SOCIETY_PURCHASE.equals(statementName)) {
					taxInvoiceDTO.setShowroomName(
							dataMap.get("Warehouse Name") == null ? "" : dataMap.get("Warehouse Name").toString());
					taxInvoiceDTO.setSocietyName(
							dataMap.get("Society Name") == null ? "" : dataMap.get("Society Name").toString());
					taxInvoiceDTO.setBillValue(dataMap.get("Bill Value") == null ? 0D
							: Double.parseDouble(dataMap.get("Bill Value").toString()));
					taxInvoiceDTO.setIsSupplierRegistered(IS_SUPPLIER_REGISTERED);
					taxInvoiceDTO.setTotalCGSTValue(
							dataMap.get("CGST") == null ? 0D : Double.parseDouble(dataMap.get("CGST").toString()));
					taxInvoiceDTO.setTotalSGSTValue(
							dataMap.get("SGST") == null ? 0D : Double.parseDouble(dataMap.get("SGST").toString()));
					taxInvoiceDTO.setTotalTaxableValue(dataMap.get("Taxable Value") == null ? 0D
							: Double.parseDouble(dataMap.get("Taxable Value").toString()));
					taxInvoiceDTO.setTaxPercentage(dataMap.get("Tax Percent") == null ? 0D
							: Double.parseDouble(dataMap.get("Tax Percent").toString()));
				} else if (TaxStatementNames.IGST_STATEMENT.equals(statementName)) {
					taxInvoiceDTO.setBillValue(dataMap.get("Invoice Value") == null ? 0D
							: Double.parseDouble(dataMap.get("Invoice Value").toString()));
					taxInvoiceDTO.setHsnCode(dataMap.get("HSN Code") == null ? "" : dataMap.get("HSN Code").toString());
					taxInvoiceDTO.setQuantity(
							dataMap.get("Qty") == null ? 0D : Double.parseDouble(dataMap.get("Qty").toString()));
					taxInvoiceDTO.setPlaceOfSupply(
							dataMap.get("Place Of Supply") == null ? "" : dataMap.get("Place Of Supply").toString());
					taxInvoiceDTO.setTotalIGSTValue(
							dataMap.get("IGST") == null ? 0D : Double.parseDouble(dataMap.get("IGST").toString()));
					taxInvoiceDTO.setTotalTaxableValue(dataMap.get("Taxable Value") == null ? 0D
							: Double.parseDouble(dataMap.get("Taxable Value").toString()));
					taxInvoiceDTO.setInvoiceNumber(
							dataMap.get("referenceNo") == null ? "" : dataMap.get("referenceNo").toString());
					/*
					 * Find Tax percentage : (IGST Value / Taxable Value) * 100
					 */

					double igstValue = taxInvoiceDTO.getTotalIGSTValue();
					double taxableValue = taxInvoiceDTO.getTotalTaxableValue();
					taxInvoiceDTO.setTaxPercentage((igstValue / taxableValue) * 100);
				} else if (TaxStatementNames.HSN_INVOICE_BREAKUP.equals(statementName)) {
					taxInvoiceDTO.setFromInvoiceNumber(dataMap.get("From Invoice Number") == null ? ""
							: dataMap.get("From Invoice Number").toString());
					taxInvoiceDTO.setToInvoiceNumber(dataMap.get("To Invoice Number") == null ? ""
							: dataMap.get("To Invoice Number").toString());
					taxInvoiceDTO.setInvoiceCount(
							dataMap.get("Total") == null ? 0 : Integer.valueOf(dataMap.get("Total").toString()));
				}

				taxInvoiceList.add(taxInvoiceDTO);
			}
		} catch (Exception e) {
			log.error("Exception at GstPaymentService. getStatementsReport() ", e);
		}
		log.info("GstPaymentService. getStatementsReport() Starts");
		return taxInvoiceList;
	}

	public BaseDTO getTaxInvoiceListForStatementsOld(TaxInvoiceDTO taxInvoiceDTO) {
		log.info("GstPaymentService. getTaxInvoiceListForStatements() Starts");
		BaseDTO baseDTO = new BaseDTO();
		String queryName = null;
		try {
			if (taxInvoiceDTO != null) {

				String statementName = taxInvoiceDTO.getStatementName();

				Integer yearNumber = taxInvoiceDTO.getYear();
				Integer monthNumber = taxInvoiceDTO.getMonth();

				String fromDateStr = new StringBuilder().append(yearNumber).append("-")
						.append(monthNumber < 10 ? "0" + monthNumber : monthNumber).append("-").append("01").toString();
				java.util.Date fromDate = AppUtil.formatDateStringAsDate(fromDateStr, AppUtil.DATE_FORMAT_YYYY_MM_DD);
				String toDateStr = AppUtil.REPORT_DATE_FORMAT.format(AppUtil.getLastDateOfMonth(fromDate));

				taxInvoiceDTO.setFromDate(fromDate);
				taxInvoiceDTO.setToDate(AppUtil.getLastDateOfMonth(fromDate));

				if (TaxStatementNames.STATEMENT1.equals(statementName)) {
					queryName = "TAX_INOVICE_REPORT_QRY";
				} else if (TaxStatementNames.STATEMENT1C_HSN_VALUE_BREAKUP.equals(statementName)) {
					queryName = "STATEMENT1C_HSN_VALUE_BREAKUP_QUERY";
				} else if (TaxStatementNames.STATEMENT5_SOCIETY_PURCHASE.equals(statementName)) {
					queryName = "STATEMENT5_SOCIETY_PURCHASE_QUERY";
				} else if (TaxStatementNames.IGST_STATEMENT.equals(statementName)) {
					queryName = "IGST_STATEMENT_REPORT_QUERY";
				} else if (TaxStatementNames.HSN_INVOICE_BREAKUP.equals(statementName)) {
					queryName = "HSN_INVOICE_BREAKUP_REPORT_QUERY";
				}

				ApplicationQuery applicationQuery = appQueryRepository.findByQueryName(queryName);
				if (applicationQuery == null || applicationQuery.getId() == null) {
					log.info("Application Query not found for query name : " + queryName);
					baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
					return baseDTO;
				}
				String query = applicationQuery.getQueryContent().trim();
				if (StringUtils.isNotEmpty(query)) {

					if (TaxStatementNames.STATEMENT5_SOCIETY_PURCHASE.equals(statementName)) {

						if (StringUtils.isNotEmpty(fromDateStr)) {
							query = StringUtils.replace(query, "#{startDate}", "'" + fromDateStr + " 00:00:00'");
						}

						if (StringUtils.isNotEmpty(toDateStr)) {
							query = StringUtils.replace(query, "#{endDate}", "'" + toDateStr + " 23:59:59'");
						}

						String regionIds = null;
						int selectedEntityListSize = taxInvoiceDTO.getSelectedEntityIdList() == null ? 0
								: taxInvoiceDTO.getSelectedEntityIdList().size();
						if (selectedEntityListSize > 0) {
							regionIds = AppUtil.commaSeparatedIds(taxInvoiceDTO.getSelectedEntityIdList());
						} else {

							List<EntityMaster> headOfficeAndRegionOfficeList = entityMasterRepository
									.findHeadAndOffcieRegion();
							int headOfficeAndRegionOfficeListSize = headOfficeAndRegionOfficeList != null
									? headOfficeAndRegionOfficeList.size()
									: 0;
							log.info("headOfficeAndRegionOfficeListSize : " + headOfficeAndRegionOfficeListSize);
							if (CollectionUtils.isNotEmpty(headOfficeAndRegionOfficeList)) {
								List<Long> regionIdList = new ArrayList<>();
								for(EntityMaster entityMaster : headOfficeAndRegionOfficeList) {
									if(entityMaster != null && entityMaster.getId() != null) {
										regionIdList.add(entityMaster.getId());
									}
								}
								if(CollectionUtils.isNotEmpty(regionIdList)) {
									regionIds = AppUtil.commaSeparatedIds(regionIdList);
								}
							}
						}

						query = StringUtils.replace(query, ":regionIds", regionIds);

						log.info(statementName + " After replacing value : " + query);
						List<Map<String, Object>> dataMapList = jdbcTemplate.queryForList(query);
						int dataMapListSize = dataMapList == null ? 0 : dataMapList.size();
						if (dataMapListSize > 0) {
							List<TaxInvoiceDTO> taxInvoiceList = getStatementsReport(dataMapList, statementName);
							taxInvoiceDTO.setStatement1TaxInvoiceList(taxInvoiceList);
						}

					} else {
						if (StringUtils.isNotEmpty(fromDateStr)) {
							query = StringUtils.replace(query, "#{startDate}", "'" + fromDateStr + "'");
						}

						if (StringUtils.isNotEmpty(toDateStr)) {
							query = StringUtils.replace(query, "#{endDate}", "'" + toDateStr + "'");
						}

						if (TaxStatementNames.IGST_STATEMENT.equals(statementName)) {
							StringBuilder sb = new StringBuilder();
							sb.append(query);

							int selectedEntityListSize = taxInvoiceDTO.getSelectedEntityIdList() == null ? 0
									: taxInvoiceDTO.getSelectedEntityIdList().size();
							if (selectedEntityListSize > 0) {
								String entityIds = AppUtil.commaSeparatedIds(taxInvoiceDTO.getSelectedEntityIdList());
								sb.append(" AND emf.id in (select id from entity_master where region_id in ("
										+ entityIds + "))");
							}

							int selectedGstNumberListSize = taxInvoiceDTO.getSelectedGstNumberList() == null ? 0
									: taxInvoiceDTO.getSelectedGstNumberList().size();
							if (selectedGstNumberListSize > 0) {
								String gstNumbers = StringUtils.join(taxInvoiceDTO.getSelectedGstNumberList(), "','");
								sb.append(" AND emf.gst_number in (" + "'" + gstNumbers + "'" + ")");
							}
							sb.append(
									" )t group by t.hsn_code,t.stock_transfer_id,t.toentitycode,t.name,t.gst_number,t.fromentitycode,t.created_date,t.igst_rate;");

							log.info(statementName + " After replacing value : " + sb.toString());
							List<Map<String, Object>> dataMapList = jdbcTemplate.queryForList(sb.toString());
							int dataMapListSize = dataMapList == null ? 0 : dataMapList.size();
							if (dataMapListSize > 0) {
								List<TaxInvoiceDTO> taxInvoiceList = getStatementsReport(dataMapList, statementName);
								taxInvoiceDTO.setStatement1TaxInvoiceList(taxInvoiceList);
							}

						} else {
							StringBuilder sb = new StringBuilder();
							sb.append(query);

							int selectedEntityListSize = taxInvoiceDTO.getSelectedEntityIdList() == null ? 0
									: taxInvoiceDTO.getSelectedEntityIdList().size();
							if (selectedEntityListSize > 0) {
								String entityIds = AppUtil.commaSeparatedIds(taxInvoiceDTO.getSelectedEntityIdList());
								sb.append(" AND em.id in (" + entityIds + ")");
							}

							int selectedGstNumberListSize = taxInvoiceDTO.getSelectedGstNumberList() == null ? 0
									: taxInvoiceDTO.getSelectedGstNumberList().size();
							if (selectedGstNumberListSize > 0) {
								String gstNumbers = StringUtils.join(taxInvoiceDTO.getSelectedGstNumberList(), "','");
								sb.append(" AND em.gst_number in (" + "'" + gstNumbers + "'" + ")");
							}

							if (TaxStatementNames.STATEMENT1.equals(statementName)) {
								sb.append(" GROUP BY em1.code,em.id,ti.id ORDER BY em.code,ti.invoice_date");
							} else if (TaxStatementNames.STATEMENT1C_HSN_VALUE_BREAKUP.equals(statementName)) {
								sb.append(" GROUP BY pvm.hsn_code,em.gst_number,em.code,em1.code ORDER BY em.code");
							} else if (TaxStatementNames.HSN_INVOICE_BREAKUP.equals(statementName)) {
								sb.append(" GROUP BY em1.code,em.id,em.gst_number ORDER BY em.code");
							}

							log.info(statementName + " After replacing value : " + sb.toString());
							List<Map<String, Object>> dataMapList = jdbcTemplate.queryForList(sb.toString());
							int dataMapListSize = dataMapList == null ? 0 : dataMapList.size();
							if (dataMapListSize > 0) {
								List<TaxInvoiceDTO> taxInvoiceList = getStatementsReport(dataMapList, statementName);
								taxInvoiceDTO.setStatement1TaxInvoiceList(taxInvoiceList);
							}
						}
					}
				}
			}
			baseDTO.setResponseContent(taxInvoiceDTO);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("GstPaymentService. getTaxInvoiceListForStatements() RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("GstPaymentService. getTaxInvoiceListForStatements() Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("GstPaymentService. getTaxInvoiceListForStatements() Ends");
		return baseDTO;
	}

	public BaseDTO getGstNumberList(TaxInvoiceDTO taxInvoiceDTO) {
		log.info("GstPaymentService. getGstNumberList() Starts");
		BaseDTO baseDTO = new BaseDTO();
		String query = null;
		List<String> gstNumberList = null;
		try {
			if (taxInvoiceDTO != null) {

				int entityIdListSize = taxInvoiceDTO.getSelectedEntityIdList() == null ? 0
						: taxInvoiceDTO.getSelectedEntityIdList().size();

				StringBuilder sb = new StringBuilder();
				sb.append(GST_NUMBER_LIST_BY_ENTITY_LIST_QUERY);
				if (entityIdListSize > 0) {
					String entityIds = AppUtil.commaSeparatedIds(taxInvoiceDTO.getSelectedEntityIdList());
					sb.append(" AND (id in (:entityIds) OR region_id in (:entityIds)) GROUP BY gst_number;");
					query = sb.toString();
					query = StringUtils.replace(query, ":entityIds", entityIds);
				} else {
					sb.append(" GROUP BY gst_number;");
					query = sb.toString();
				}

				log.info("GST_NUMBER_LIST_BY_ENTITY_LIST_QUERY After replacing value : " + query);
				List<Map<String, Object>> dataMapList = jdbcTemplate.queryForList(query);
				int dataMapListSize = dataMapList == null ? 0 : dataMapList.size();
				if (dataMapListSize > 0) {
					gstNumberList = new ArrayList<>();
					for (Map<String, Object> dataMap : dataMapList) {
						if (dataMap.get("gst_number") != null) {
							gstNumberList.add(dataMap.get("gst_number").toString());
						}
					}
				}
			}
			baseDTO.setResponseContent(gstNumberList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("GstPaymentService. getGstNumberList() RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("GstPaymentService. getGstNumberList() Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("GstPaymentService. getGstNumberList() Ends");
		return baseDTO;
	}

	public BaseDTO getTaxInvoiceListForStatements(TaxInvoiceDTO taxInvoiceDTO) {
		log.info("GstPaymentService. getTaxInvoiceListForStatements() Starts");
		BaseDTO baseDTO = new BaseDTO();
		String queryName = null;
		String finalQuery = null;
		try {
			if (taxInvoiceDTO != null) {

				String statementName = taxInvoiceDTO.getStatementName();

				Integer yearNumber = taxInvoiceDTO.getYear();
				Integer monthNumber = taxInvoiceDTO.getMonth();

				String fromDateStr = new StringBuilder().append(yearNumber).append("-")
						.append(monthNumber < 10 ? "0" + monthNumber : monthNumber).append("-").append("01").toString();
				java.util.Date fromDate = AppUtil.formatDateStringAsDate(fromDateStr, AppUtil.DATE_FORMAT_YYYY_MM_DD);

				taxInvoiceDTO.setFromDate(fromDate);
				taxInvoiceDTO.setToDate(AppUtil.getLastDateOfMonth(fromDate));

				if (TaxStatementNames.STATEMENT1.equals(statementName)) {

					queryName = "TAX_INOVICE_REPORT_QRY";

					finalQuery = getStatement1Query(queryName, taxInvoiceDTO);
				} else if (TaxStatementNames.STATEMENT1C_HSN_VALUE_BREAKUP.equals(statementName)) {
					String taxInvoiceEnable = appConfigRepository.findValueByKey("STATEMENT_TAX_INVOICE_ENABLE");
					log.info("======================Tax Invoice Enable================= : " + taxInvoiceEnable);
					if ("true".equalsIgnoreCase(taxInvoiceEnable)) {
						queryName = "STATEMENT1C_HSN_VALUE_BREAKUP_QUERY_FLAT";
						finalQuery = getHsnValueBreakupStatement1CQuery_Latest(queryName, taxInvoiceDTO);
					} else {
						queryName = "STATEMENT1C_HSN_VALUE_BREAKUP_QUERY";
						finalQuery = getHsnValueBreakupStatement1CQuery(queryName, taxInvoiceDTO);
					}

				} else if (TaxStatementNames.STATEMENT5_SOCIETY_PURCHASE.equals(statementName)) {
					queryName = "STATEMENT5_SOCIETY_PURCHASE_QUERY";
					finalQuery = getSocietyPurchaseStatement5Query(queryName, taxInvoiceDTO);
				} else if (TaxStatementNames.IGST_STATEMENT.equals(statementName)) {
					queryName = "IGST_STATEMENT_REPORT_QUERY";
					finalQuery = getIGSTStatementQuery(queryName, taxInvoiceDTO);
				} else if (TaxStatementNames.HSN_INVOICE_BREAKUP.equals(statementName)) {
					queryName = "HSN_INVOICE_BREAKUP_REPORT_QUERY";
					finalQuery = getHsnInvoiceBreakupStatementQuery(queryName, taxInvoiceDTO);
				}

				log.info(statementName + " After replacing value : " + finalQuery);
				List<Map<String, Object>> dataMapList = jdbcTemplate.queryForList(finalQuery);
				int dataMapListSize = dataMapList == null ? 0 : dataMapList.size();
				if (dataMapListSize > 0) {
					List<TaxInvoiceDTO> taxInvoiceList = getStatementsReport(dataMapList, statementName);
					taxInvoiceDTO.setStatement1TaxInvoiceList(taxInvoiceList);
				}
				baseDTO.setResponseContent(taxInvoiceDTO);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			} else {
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
				log.error("Request Parameter is Null");
			}
		} catch (RestException restException) {
			log.error("GstPaymentService. getTaxInvoiceListForStatements() RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("GstPaymentService. getTaxInvoiceListForStatements() Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("GstPaymentService. getTaxInvoiceListForStatements() Ends");
		return baseDTO;
	}

	public String getIGSTStatementQuery(String queryName, TaxInvoiceDTO taxInvoiceDTO) throws Exception {
		Integer yearNumber = taxInvoiceDTO.getYear();
		Integer monthNumber = taxInvoiceDTO.getMonth();
		StringBuilder sb = new StringBuilder();
		String fromDateStr = new StringBuilder().append(yearNumber).append("-")
				.append(monthNumber < 10 ? "0" + monthNumber : monthNumber).append("-").append("01").toString();
		java.util.Date fromDate = AppUtil.formatDateStringAsDate(fromDateStr, AppUtil.DATE_FORMAT_YYYY_MM_DD);
		String toDateStr = AppUtil.REPORT_DATE_FORMAT.format(AppUtil.getLastDateOfMonth(fromDate));

		ApplicationQuery applicationQuery = appQueryRepository.findByQueryName(queryName);
		if (applicationQuery == null || applicationQuery.getId() == null) {
			log.info("Application Query not found for query name : " + queryName);
			throw new Exception("Application Query not found for query name : " + queryName);
		}
		String query = applicationQuery.getQueryContent().trim();
		if (StringUtils.isNotEmpty(query)) {

			if (StringUtils.isNotEmpty(fromDateStr)) {
				query = StringUtils.replace(query, "#{startDate}", "'" + fromDateStr + "'");
			}

			if (StringUtils.isNotEmpty(toDateStr)) {
				query = StringUtils.replace(query, "#{endDate}", "'" + toDateStr + "'");
			}

			sb.append(query);

			int selectedEntityListSize = taxInvoiceDTO.getSelectedEntityIdList() == null ? 0
					: taxInvoiceDTO.getSelectedEntityIdList().size();
			if (selectedEntityListSize > 0) {
				String entityIds = AppUtil.commaSeparatedIds(taxInvoiceDTO.getSelectedEntityIdList());
				sb.append(" AND emf.id in (select id from entity_master where region_id in (" + entityIds + "))");
			}

			int selectedGstNumberListSize = taxInvoiceDTO.getSelectedGstNumberList() == null ? 0
					: taxInvoiceDTO.getSelectedGstNumberList().size();
			if (selectedGstNumberListSize > 0) {
				String gstNumbers = StringUtils.join(taxInvoiceDTO.getSelectedGstNumberList(), "','");
				sb.append(" AND emf.gst_number in (" + "'" + gstNumbers + "'" + ")");
			}
			sb.append(
					" )t group by t.reference_number,t.hsn_code,t.stock_transfer_id,t.toentitycode,t.name,t.gst_number,t.fromentitycode,t.created_date,t.igst_rate;");

		} else {
			throw new Exception("Application Query not found for query name : " + queryName);
		}

		return sb.toString();
	}

	public String getStatement1Query(String queryName, TaxInvoiceDTO taxInvoiceDTO) throws Exception {
		Integer yearNumber = taxInvoiceDTO.getYear();
		Integer monthNumber = taxInvoiceDTO.getMonth();
		StringBuilder sb = new StringBuilder();
		String fromDateStr = new StringBuilder().append(yearNumber).append("-")
				.append(monthNumber < 10 ? "0" + monthNumber : monthNumber).append("-").append("01").toString();
		java.util.Date fromDate = AppUtil.formatDateStringAsDate(fromDateStr, AppUtil.DATE_FORMAT_YYYY_MM_DD);
		String toDateStr = AppUtil.REPORT_DATE_FORMAT.format(AppUtil.getLastDateOfMonth(fromDate));

		ApplicationQuery applicationQuery = appQueryRepository.findByQueryName(queryName);
		if (applicationQuery == null || applicationQuery.getId() == null) {
			log.info("Application Query not found for query name : " + queryName);
			throw new Exception("Application Query not found for query name : " + queryName);
		}
		String query = applicationQuery.getQueryContent().trim();
		if (StringUtils.isNotEmpty(query)) {

			if (StringUtils.isNotEmpty(fromDateStr)) {
				query = StringUtils.replace(query, "#{startDate}", "'" + fromDateStr + "'");
			}

			if (StringUtils.isNotEmpty(toDateStr)) {
				query = StringUtils.replace(query, "#{endDate}", "'" + toDateStr + "'");
			}

			sb.append(query);

			int selectedEntityListSize = taxInvoiceDTO.getSelectedEntityIdList() == null ? 0
					: taxInvoiceDTO.getSelectedEntityIdList().size();
			if (selectedEntityListSize > 0) {
				String entityIds = AppUtil.commaSeparatedIds(taxInvoiceDTO.getSelectedEntityIdList());
				sb.append(" AND em1.id  in (" + entityIds + ")");
			}

			int selectedGstNumberListSize = taxInvoiceDTO.getSelectedGstNumberList() == null ? 0
					: taxInvoiceDTO.getSelectedGstNumberList().size();
			if (selectedGstNumberListSize > 0) {
				String gstNumbers = StringUtils.join(taxInvoiceDTO.getSelectedGstNumberList(), "','");
				sb.append(" AND em.gst_number in (" + "'" + gstNumbers + "'" + ")");
			}
			sb.append(" GROUP BY em1.code,em.id,ti.id ORDER BY em.code,ti.invoice_date");
		} else {
			throw new Exception("Application Query not found for query name : " + queryName);
		}

		return sb.toString();
	}

	public String getHsnValueBreakupStatement1CQuery(String queryName, TaxInvoiceDTO taxInvoiceDTO) throws Exception {
		Integer yearNumber = taxInvoiceDTO.getYear();
		Integer monthNumber = taxInvoiceDTO.getMonth();
		StringBuilder sb = new StringBuilder();
		String fromDateStr = new StringBuilder().append(yearNumber).append("-")
				.append(monthNumber < 10 ? "0" + monthNumber : monthNumber).append("-").append("01").toString();
		java.util.Date fromDate = AppUtil.formatDateStringAsDate(fromDateStr, AppUtil.DATE_FORMAT_YYYY_MM_DD);
		String toDateStr = AppUtil.REPORT_DATE_FORMAT.format(AppUtil.getLastDateOfMonth(fromDate));

		ApplicationQuery applicationQuery = appQueryRepository.findByQueryName(queryName);
		if (applicationQuery == null || applicationQuery.getId() == null) {
			log.info("Application Query not found for query name : " + queryName);
			throw new Exception("Application Query not found for query name : " + queryName);
		}
		String query = applicationQuery.getQueryContent().trim();
		if (StringUtils.isNotEmpty(query)) {

			if (StringUtils.isNotEmpty(fromDateStr)) {
				query = StringUtils.replace(query, "#{startDate}", "'" + fromDateStr + "'");
			}

			if (StringUtils.isNotEmpty(toDateStr)) {
				query = StringUtils.replace(query, "#{endDate}", "'" + toDateStr + "'");
			}

			sb.append(query);

			int selectedEntityListSize = taxInvoiceDTO.getSelectedEntityIdList() == null ? 0
					: taxInvoiceDTO.getSelectedEntityIdList().size();
			if (selectedEntityListSize > 0) {
				String entityIds = AppUtil.commaSeparatedIds(taxInvoiceDTO.getSelectedEntityIdList());
				sb.append(" AND em1.id  in (" + entityIds + ")");
			}

			int selectedGstNumberListSize = taxInvoiceDTO.getSelectedGstNumberList() == null ? 0
					: taxInvoiceDTO.getSelectedGstNumberList().size();
			if (selectedGstNumberListSize > 0) {
				String gstNumbers = StringUtils.join(taxInvoiceDTO.getSelectedGstNumberList(), "','");
				sb.append(" AND em.gst_number in (" + "'" + gstNumbers + "'" + ")");
			}
			sb.append(" GROUP BY pvm.hsn_code,em.gst_number,em.code,em1.code ORDER BY em.code");

			log.info("===================== LST Query============== : " + sb.toString());
		} else {
			throw new Exception("Application Query not found for query name : " + queryName);
		}

		return sb.toString();
	}

	public String getHsnValueBreakupStatement1CQuery_Latest(String queryName, TaxInvoiceDTO taxInvoiceDTO)
			throws Exception {
		Integer yearNumber = taxInvoiceDTO.getYear();
		Integer monthNumber = taxInvoiceDTO.getMonth();

		String fromDateStr = new StringBuilder().append(yearNumber).append("-")
				.append(monthNumber < 10 ? "0" + monthNumber : monthNumber).append("-").append("01").toString();
		java.util.Date fromDate = AppUtil.formatDateStringAsDate(fromDateStr, AppUtil.DATE_FORMAT_YYYY_MM_DD);
		String toDateStr = AppUtil.REPORT_DATE_FORMAT.format(AppUtil.getLastDateOfMonth(fromDate));

		int selectedEntityListSize = taxInvoiceDTO.getSelectedEntityIdList() == null ? 0
				: taxInvoiceDTO.getSelectedEntityIdList().size();

		int selectedGstNumberListSize = taxInvoiceDTO.getSelectedGstNumberList() == null ? 0
				: taxInvoiceDTO.getSelectedGstNumberList().size();

		if (selectedEntityListSize > 0 && selectedGstNumberListSize > 0) {
			queryName = "STATEMENT1C_HSN_VALUE_BREAKUP_QUERY_FLAT_1";
		}
		if (selectedEntityListSize > 0 && selectedGstNumberListSize == 0) {
			queryName = "STATEMENT1C_HSN_VALUE_BREAKUP_QUERY_FLAT_2";
		}
		if (selectedEntityListSize == 0 && selectedGstNumberListSize > 0) {
			queryName = "STATEMENT1C_HSN_VALUE_BREAKUP_QUERY_FLAT_3";
		}

		ApplicationQuery applicationQuery = appQueryRepository.findByQueryName(queryName);
		if (applicationQuery == null || applicationQuery.getId() == null) {
			log.info("Application Query not found for query name : " + queryName);
			throw new Exception("Application Query not found for query name : " + queryName);
		}
		String query = applicationQuery.getQueryContent().trim();

		if (StringUtils.isNotEmpty(query)) {

			if (StringUtils.isNotEmpty(fromDateStr)) {
				query = StringUtils.replace(query, "#{startDate}", "'" + fromDateStr + "'");
			}

			if (StringUtils.isNotEmpty(toDateStr)) {
				query = StringUtils.replace(query, "#{endDate}", "'" + toDateStr + "'");
			}

			String entityIds = null;
			if (selectedEntityListSize > 0) {
				entityIds = AppUtil.commaSeparatedIds(taxInvoiceDTO.getSelectedEntityIdList());
			}
			String gstNumbers = null;

			if (selectedGstNumberListSize > 0) {
				gstNumbers = StringUtils.join(taxInvoiceDTO.getSelectedGstNumberList(), "','");
			}

			if (selectedEntityListSize > 0 && selectedGstNumberListSize > 0) {
				query = StringUtils.replace(query, "#{regionIds}", entityIds);
				query = StringUtils.replace(query, "#{gstNumbers}", AppUtil.getValueWithSingleQuote(gstNumbers));
			}
			if (selectedEntityListSize > 0 && selectedGstNumberListSize == 0) {
				query = StringUtils.replace(query, "#{regionIds}", entityIds);
			}
			if (selectedEntityListSize == 0 && selectedGstNumberListSize > 0) {
				query = StringUtils.replace(query, "#{gstNumbers}", AppUtil.getValueWithSingleQuote(gstNumbers));
			}

		} else {
			throw new Exception("Application Query not found for query name : " + queryName);
		}

		return query;
	}

	public String getSocietyPurchaseStatement5Query(String queryName, TaxInvoiceDTO taxInvoiceDTO) throws Exception {
		Integer yearNumber = taxInvoiceDTO.getYear();
		Integer monthNumber = taxInvoiceDTO.getMonth();

		String fromDateStr = new StringBuilder().append(yearNumber).append("-")
				.append(monthNumber < 10 ? "0" + monthNumber : monthNumber).append("-").append("01").toString();
		java.util.Date fromDate = AppUtil.formatDateStringAsDate(fromDateStr, AppUtil.DATE_FORMAT_YYYY_MM_DD);
		String toDateStr = AppUtil.REPORT_DATE_FORMAT.format(AppUtil.getLastDateOfMonth(fromDate));

		ApplicationQuery applicationQuery = appQueryRepository.findByQueryName(queryName);
		if (applicationQuery == null || applicationQuery.getId() == null) {
			log.info("Application Query not found for query name : " + queryName);
			throw new Exception("Application Query not found for query name : " + queryName);
		}
		String query = applicationQuery.getQueryContent().trim();
		if (StringUtils.isNotEmpty(query)) {

			if (StringUtils.isNotEmpty(fromDateStr)) {
				query = StringUtils.replace(query, "#{startDate}", "'" + fromDateStr + " 00:00:00'");
			}

			if (StringUtils.isNotEmpty(toDateStr)) {
				query = StringUtils.replace(query, "#{endDate}", "'" + toDateStr + " 23:59:59'");
			}

			String regionIds = null;
			int selectedEntityListSize = taxInvoiceDTO.getSelectedEntityIdList() == null ? 0
					: taxInvoiceDTO.getSelectedEntityIdList().size();
			if (selectedEntityListSize > 0) {
				regionIds = AppUtil.commaSeparatedIds(taxInvoiceDTO.getSelectedEntityIdList());
			} else {

				List<EntityMaster> headOfficeAndRegionOfficeList = entityMasterRepository
						.findHeadAndOffcieRegion();
				int headOfficeAndRegionOfficeListSize = headOfficeAndRegionOfficeList != null
						? headOfficeAndRegionOfficeList.size()
						: 0;
				log.info("headOfficeAndRegionOfficeListSize : " + headOfficeAndRegionOfficeListSize);
				if (CollectionUtils.isNotEmpty(headOfficeAndRegionOfficeList)) {
					List<Long> regionIdList = new ArrayList<>();
					for(EntityMaster entityMaster : headOfficeAndRegionOfficeList) {
						if(entityMaster != null && entityMaster.getId() != null) {
							regionIdList.add(entityMaster.getId());
						}
					}
					if(CollectionUtils.isNotEmpty(regionIdList)) {
						regionIds = AppUtil.commaSeparatedIds(regionIdList);
					}
				}
			}

			query = StringUtils.replace(query, ":regionIds", regionIds);

			log.info("Society purchase after replacing value : " + query);
		} else {
			throw new Exception("Application Query not found for query name : " + queryName);
		}

		return query;
	}

	public String getHsnInvoiceBreakupStatementQuery(String queryName, TaxInvoiceDTO taxInvoiceDTO) throws Exception {
		Integer yearNumber = taxInvoiceDTO.getYear();
		Integer monthNumber = taxInvoiceDTO.getMonth();
		StringBuilder sb = new StringBuilder();
		String fromDateStr = new StringBuilder().append(yearNumber).append("-")
				.append(monthNumber < 10 ? "0" + monthNumber : monthNumber).append("-").append("01").toString();
		java.util.Date fromDate = AppUtil.formatDateStringAsDate(fromDateStr, AppUtil.DATE_FORMAT_YYYY_MM_DD);
		String toDateStr = AppUtil.REPORT_DATE_FORMAT.format(AppUtil.getLastDateOfMonth(fromDate));

		ApplicationQuery applicationQuery = appQueryRepository.findByQueryName(queryName);
		if (applicationQuery == null || applicationQuery.getId() == null) {
			log.info("Application Query not found for query name : " + queryName);
			throw new Exception("Application Query not found for query name : " + queryName);
		}
		String query = applicationQuery.getQueryContent().trim();
		if (StringUtils.isNotEmpty(query)) {

			if (StringUtils.isNotEmpty(fromDateStr)) {
				query = StringUtils.replace(query, "#{startDate}", "'" + fromDateStr + "'");
			}

			if (StringUtils.isNotEmpty(toDateStr)) {
				query = StringUtils.replace(query, "#{endDate}", "'" + toDateStr + "'");
			}

			sb.append(query);

			int selectedEntityListSize = taxInvoiceDTO.getSelectedEntityIdList() == null ? 0
					: taxInvoiceDTO.getSelectedEntityIdList().size();
			if (selectedEntityListSize > 0) {
				String entityIds = AppUtil.commaSeparatedIds(taxInvoiceDTO.getSelectedEntityIdList());
				sb.append(" AND em1.id  in (" + entityIds + ")");
			}

			int selectedGstNumberListSize = taxInvoiceDTO.getSelectedGstNumberList() == null ? 0
					: taxInvoiceDTO.getSelectedGstNumberList().size();
			if (selectedGstNumberListSize > 0) {
				String gstNumbers = StringUtils.join(taxInvoiceDTO.getSelectedGstNumberList(), "','");
				sb.append(" AND em.gst_number in (" + "'" + gstNumbers + "'" + ")");
			}
			sb.append(" GROUP BY em1.code,em.id,em.gst_number ORDER BY em.code");
		} else {
			throw new Exception("Application Query not found for query name : " + queryName);
		}

		return sb.toString();
	}

	private static final String CHECK_NEXT_MONTH_TAX_INVOICE_RECORD_SQL = "SELECT COUNT(*) AS count FROM tax_invoice WHERE invoice_date>'${invoiceDate}' AND entity_id in (SELECT id FROM entity_master WHERE id in (${regionIds}) or region_id in (${regionIds}))";

	public BaseDTO checkTaxInvoiceRecordInNextMonth(TaxInvoiceDTO taxInvoiceDTO) {
		log.info("GstPaymentService. checkTaxInvoiceRecordInNextMonth() START");
		BaseDTO baseDTO = new BaseDTO();
		try {

			Integer monthNumber = taxInvoiceDTO.getMonth();
			Integer yearNumber = taxInvoiceDTO.getYear();
			List<Long> selectedEntityIdList = taxInvoiceDTO.getSelectedEntityIdList();
			int selectedEntityIdListSize = selectedEntityIdList != null ? selectedEntityIdList.size() : 0;
			log.info("GstPaymentService. checkTaxInvoiceRecordInNextMonth() - monthNumber: " + monthNumber);
			log.info("GstPaymentService. checkTaxInvoiceRecordInNextMonth() - yearNumber: " + yearNumber);
			log.info("GstPaymentService. checkTaxInvoiceRecordInNextMonth() - selectedEntityIdList: "
					+ selectedEntityIdList);
			log.info("GstPaymentService. checkTaxInvoiceRecordInNextMonth() - selectedEntityIdListSize: "
					+ selectedEntityIdListSize);

			String entityIds = AppUtil.commaSeparatedIds(selectedEntityIdList);
			log.info("GstPaymentService. checkTaxInvoiceRecordInNextMonth() - entityIds: " + entityIds);

			String firstDate = new StringBuilder().append(yearNumber).append("-")
					.append(monthNumber < 10 ? "0" + monthNumber : monthNumber).append("-").append("01").toString();
			java.util.Date dte = AppUtil.formatDateStringAsDate(firstDate, AppUtil.DATE_FORMAT_YYYY_MM_DD);
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(AppUtil.DATE_FORMAT_YYYY_MM_DD);
			String lastDate = simpleDateFormat.format(AppUtil.getLastDateOfMonth(dte));

			String SQL = StringUtils.replace(CHECK_NEXT_MONTH_TAX_INVOICE_RECORD_SQL, "${invoiceDate}", lastDate);
			SQL = StringUtils.replace(SQL, "${regionIds}", entityIds);

			Integer returnValue = jdbcTemplate.queryForObject(SQL, Integer.class);
			int existCount = returnValue != null ? Integer.parseInt(returnValue.toString()) : 0;
			log.info("GstPaymentService. checkTaxInvoiceRecordInNextMonth() - existCount: " + existCount);
			baseDTO.setTotalRecords(existCount);

		} catch (Exception e) {
			log.error("Exception at checkTaxInvoiceRecordInNextMonth()", e);
		}
		log.info("GstPaymentService. checkTaxInvoiceRecordInNextMonth() END");
		return baseDTO;
	}
	
	
	public BaseDTO downloadPDF(GeneratePdfDTO generatePdfDTO) {
		log.info("GstPaymentService. downloadPDF() Starts");
		BaseDTO baseDTO = new BaseDTO();
		employee = new EmployeeMaster();
		taxInvoiceDTO = new TaxInvoiceDTO();
		try {
			String content = generatePdfDTO.getContent();
			String finalContent = VelocityStringUtil.getFinalContent("", content, null);
			// log.info("finalContent: " + finalContent);
			String htmlData = generateHtml(generatePdfDTO.getTaxInvoiceDTOPdf(), generatePdfDTO.getResponse(), finalContent);
			 downloadPath = generatePdfDTO.getDownloadPath();
			log.info("===========downloadPath is=============" + downloadPath.toString());
			taxInvoiceDTO=generatePdfDTO.getTaxInvoiceDTO();
			employee= generatePdfDTO.getEmployee();
			String reportName = null;
			if (TaxStatementNames.STATEMENT1.equals(generatePdfDTO.getTaxInvoiceDTO().getStatementName())) {
				reportName = ReportNames.TAX_INVOICE_REPORT_FILE;
			} else if (TaxStatementNames.STATEMENT1C_HSN_VALUE_BREAKUP.equals(generatePdfDTO.getTaxInvoiceDTO().getStatementName())) {
				reportName = ReportNames.STATEMENT1C_HSN_WISE_TAX_INVOICE_REPORT_FILE;
			} else if (TaxStatementNames.STATEMENT5_SOCIETY_PURCHASE.equals(generatePdfDTO.getTaxInvoiceDTO().getStatementName())) {
				reportName = ReportNames.STATEMENT5_TAX_INVOICE_REPORT_FILE;
			} else if (TaxStatementNames.IGST_STATEMENT.equals(generatePdfDTO.getTaxInvoiceDTO().getStatementName())) {
				reportName = ReportNames.IGST_STATEMENT_REPORT_FILE;
			} else if (TaxStatementNames.HSN_INVOICE_BREAKUP.equals(generatePdfDTO.getTaxInvoiceDTO().getStatementName())) {
				reportName = ReportNames.HSN_INVOICE_BREAKUP_STATEMENT_REPORT_FILE;
			}
			String downloadFileName = AppUtil.getReportFileName(reportName, "pdf");
			createPDFlandscape(htmlData, downloadPath + "/" + downloadFileName, employee);

			baseDTO.setMessage(downloadPath + "/" + downloadFileName);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("GstPaymentService. downloadPDF() RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("GstPaymentService. downloadPDF() Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("GstPaymentService. downloadPDF() Ends");
		return baseDTO;
	}
	
	public void createPDFlandscape(String htmlInputContent, String pdfOutputFilePath, EmployeeMaster employeMaster) throws Exception {
		log.info("===========START MisPdfUtil.createPDF=============");
		DateFormat dateFormat = AppUtil.DATE_FORMAT;
		String region = "";
		String workLocation = "";
		
		String url = downloadPath;
		String logo = url + "assets/images/iconic-logo.png";
		EmployeePersonalInfoEmployment personalInfoEmployment = null;
		//EmployeeMaster employeMaster = loginBean.getEmployee();
		if (employeMaster != null) {
			personalInfoEmployment = employeMaster.getPersonalInfoEmployment();
			if (personalInfoEmployment != null) {
				if (personalInfoEmployment.getWorkLocation().getEntityMasterRegion() != null) {
					region = personalInfoEmployment.getWorkLocation().getEntityMasterRegion().getName();
				} else {
					region = personalInfoEmployment.getWorkLocation().getName();
				}
				if (personalInfoEmployment.getWorkLocation() != null) {
					workLocation = personalInfoEmployment.getWorkLocation().getName();
				} 
			}
		} else {
			log.error("EmployeeMaster is empty");
		}

		DateFormat preparedTimeFormat = new SimpleDateFormat("h:mm a");
		String preparedDate = dateFormat.format(new Date());
		String preparedTime = preparedTimeFormat.format(new Date());
		if (employeMaster != null) {
			String lastName = "";
			if (employeMaster.getLastName() != null)
				lastName = employeMaster.getLastName();

			htmlInputContent = htmlInputContent.replace("$preparedBy", employeMaster.getFirstName() + " " + lastName);
		}
		htmlInputContent = htmlInputContent.replace("$preparedDate", preparedDate);
		htmlInputContent = htmlInputContent.replace("$preparedTime", preparedTime);
		htmlInputContent = htmlInputContent.replace("$region", region);
		htmlInputContent = htmlInputContent.replace("$worklocation", workLocation);
		htmlInputContent = htmlInputContent.replace("$logo", logo);
		if (htmlInputContent == null || htmlInputContent.isEmpty()) {
			throw new Exception("HTML InputContent is empty");
		}

		if (pdfOutputFilePath == null || pdfOutputFilePath.isEmpty()) {
			throw new Exception("PDF OutputFilePath is empty");
		}

		// step 1
		Document document = new Document(PageSize.A4_LANDSCAPE.rotate(),10f,10f,10f,10f);
		// step 2
		FileOutputStream fout = new FileOutputStream(pdfOutputFilePath);
		PdfWriter writer = PdfWriter.getInstance(document, fout);
		writer.setInitialLeading(2);
		// step 3
		document.open();

		InputStream htmlInputStream = new ByteArrayInputStream(htmlInputContent.getBytes());
		// step 4
		try {
		log.debug("before XMLWorkerHelper.getInstance().parseXHtml() "+ new Date());
		XMLWorkerHelper.getInstance().parseXHtml(writer, document, htmlInputStream);
		log.debug("After XMLWorkerHelper.getInstance().parseXHtml() "+ new Date());
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		// step 5
		document.close();
		log.info("===========END MisPdfUtil.createPDF=============");
	}
	
	private String generateHtml(TaxInvoiceDTO responseDTO, List<Map<String, Object>> response, String finalContent) {
		log.info("===========START CashClosingBalanceBean.generateHtml =============");
		try {

			Long index = (long) 0;
			DateFormat dateFormat = AppUtil.DATE_FORMAT;
			String fromDate = dateFormat.format(responseDTO.getFromDate());
			String toDate = dateFormat.format(responseDTO.getToDate());
			String officeLocation = employee == null ? ""
					: employee.getPersonalInfoEmployment() == null ? ""
							: employee.getPersonalInfoEmployment().getWorkLocation() == null ? ""
									: employee.getPersonalInfoEmployment().getWorkLocation().getName();
			StringBuffer finalTableData = new StringBuffer(110);
			StringBuffer finalHeaderData = new StringBuffer(110);
			StringBuffer finalFooterData = new StringBuffer(110);
			String filterTypeData = "<tr> " + "<td style='width: 10%'>From Date</td> "
					+ "<td align='right' style='width: 1%'>:</td> " + "<td style='width: 22%'><b>" + fromDate
					+ "</b></td>" + "<td style='width: 10%'>To Date</td> "
					+ "<td align='right' style='width: 1%'>:</td> " + "<td style='width: 22%'><b>" + toDate
					+ "</b></td></tr> ";

			String[] stringHeaderNames = { "Region Code", "Showroom Code", "GSTN", "HSN Code", "Description", "UQC",
					"Invoice Date", "Invoice Number", "Society", "Is Supplier Registered", "Place Of Supply",
					"Reverse Charge", "E-Commerce GSTIN", "From", "To", "Total", "Warehouse Code / Name", "Rate of %" };

			Set<String> headerNames = new HashSet<String>();
			for (Map<String, Object> map : response) {
				StringBuffer list = new StringBuffer();
				index++;

				headerNames = map.keySet();
				String beginTr = "<tr> " + "<td class='bor-left'>" + index + "</td>";
				list.append(beginTr);
				Iterator<String> setItr = headerNames.iterator();
				while (setItr.hasNext()) {
					String keyString = setItr.next();
					String td = new String();
					boolean isMatch = Arrays.stream(stringHeaderNames).anyMatch(keyString::equals);
					if (!isMatch) {
						if (map.get(keyString) != null) {
							td = "<td class='text-right'>" + AppUtil.formatIndianCommaSeparated(
									Double.valueOf(AppUtil.getFormattedValue(map.get(keyString)))) + "</td>";
						} else {
							td = "<td class='text-right'>" + "</td>";
						}
					} else {
						if (map.get(keyString) == null) {
							td = "<td class='text-left'> - </td>";
						} else {
							td = "<td class='text-left'>" + (map.get(keyString)) + "</td>";
						}

					}
					list.append(td);

				}
				String endTr = "</tr>";
				list.append(endTr);
				finalTableData.append(list);
			}
			String headerData = "<th style='width: 3%' class='bor-left'>#</th>";
			finalHeaderData.append(headerData);
			Iterator<String> setItr = headerNames.iterator();
			while (setItr.hasNext()) {
				String keyString = setItr.next();
				keyString = keyString.replace("{", "");
				keyString = keyString.replace("}", "");
				headerData = "<th>" + keyString + "</th>";
				finalHeaderData.append(headerData);
			}

			Iterator<String> footerItr = headerNames.iterator();
			if (TaxStatementNames.STATEMENT1.equals(taxInvoiceDTO.getStatementName())) {
				prepareStatement1FooterStr(footerItr, finalFooterData);
				finalContent = finalContent.replace("$reportName", ReportNames.TAX_INVOICE_REPORT);
			} else if (TaxStatementNames.STATEMENT1C_HSN_VALUE_BREAKUP.equals(taxInvoiceDTO.getStatementName())) {
				prepareStatement1CHsnValueBreakupFooterStr(footerItr, finalFooterData);
				finalContent = finalContent.replace("$reportName",
						ReportNames.STATEMENT1C_HSN_WISE_VALUE_BREAKUP_REPORT);
			} else if (TaxStatementNames.STATEMENT5_SOCIETY_PURCHASE.equals(taxInvoiceDTO.getStatementName())) {
				prepareStatement5FooterStr(footerItr, finalFooterData);
				finalContent = finalContent.replace("$reportName", ReportNames.STATEMENT5_SOCIETY_PURCHASE_REPORT);
			} else if (TaxStatementNames.IGST_STATEMENT.equals(taxInvoiceDTO.getStatementName())) {
				prepareIGSTStatementFooterStr(footerItr, finalFooterData);
				finalContent = finalContent.replace("$reportName", ReportNames.IGST_STATEMENT_REPORT);
			} else if (TaxStatementNames.HSN_INVOICE_BREAKUP.equals(taxInvoiceDTO.getStatementName())) {
				prepareHsnInvoiceBreakupStatementFooterStr(footerItr, finalFooterData);
				finalContent = finalContent.replace("$reportName", ReportNames.HSN_INVOICE_BREAKUP_STATEMENT_REPORT);
			}

			finalContent = finalContent.replace("$filterType", filterTypeData);
			finalContent = finalContent.replace("$columnData", finalHeaderData.toString());
			finalContent = finalContent.replace("$worklocation", officeLocation);
			finalContent = finalContent.replace("$reportListData", finalTableData.toString());
			finalContent = finalContent.replace("$footerData", finalFooterData.toString());

		} catch (Exception e) {
			log.info("Exception at GstPaymentBean.generateHtml() ", e);
		}

		return finalContent;
	}
	
	private void prepareStatement1FooterStr(Iterator<String> footerItr, StringBuffer finalFooterData) {
		Long footerIndex = (long) 0;
		try {
			while (footerItr.hasNext()) {
				footerIndex++;
				StringBuffer tfoot = new StringBuffer(110);
				String keyString = footerItr.next();
				if (keyString.equals("Invoice Value")) {
					String tr = "<tfoot> " + "<tr> " + "<td colspan='" + 6 + "'>Total</td> ";
					tfoot.append(tr);
					String td = new String();
					td = "<td class='text-center'>" + taxInvoiceDTO.getTotalInvoiceValue() + "</td>";
					tfoot.append(td);
					td = "<td class='text-center'>" + taxInvoiceDTO.getTotalFivePercentageValue() + "</td>";
					tfoot.append(td);
					td = "<td class='text-center'>" + taxInvoiceDTO.getTotalTwoFiveCGSTValue() + "</td>";
					tfoot.append(td);
					td = "<td class='text-center'>" + taxInvoiceDTO.getTotalTwoFiveSGSTValue() + "</td>";
					tfoot.append(td);
					td = "<td class='text-center'>" + taxInvoiceDTO.getTotalTwelvePercentageValue() + "</td>";
					tfoot.append(td);
					td = "<td class='text-center'>" + taxInvoiceDTO.getTotalSixCGSTValue() + "</td>";
					tfoot.append(td);
					td = "<td class='text-center'>" + taxInvoiceDTO.getTotalSixSGSTValue() + "</td>";
					tfoot.append(td);
//					18 % 
					td = "<td class='text-center'>" + taxInvoiceDTO.getTotalEighteenPercentageValue() + "</td>";
					tfoot.append(td);
//					9 % CGST
					td = "<td class='text-center'>" + taxInvoiceDTO.getTotalNineCGSTValue() + "</td>";
					tfoot.append(td);
//					9 % SGST
					td = "<td class='text-center'>" + taxInvoiceDTO.getTotalNineSGSTValue() + "</td>";
					tfoot.append(td);
					String endTr = "</tr> " + "</tfoot>";
					tfoot.append(endTr);
					finalFooterData.append(tfoot);
				}
			}
		} catch (Exception e) {
			log.error("Exception at PrepareStatment1FooterData() ", e);
		}
	}

	private void prepareStatement1CHsnValueBreakupFooterStr(Iterator<String> footerItr, StringBuffer finalFooterData) {
		Long footerIndex = (long) 0;
		try {
			while (footerItr.hasNext()) {
				footerIndex++;
				StringBuffer tfoot = new StringBuffer(110);
				String keyString = footerItr.next();
				if (keyString.equals("Invoice Value")) {
					String tr = "<tfoot> " + "<tr> " + "<td colspan='" + 7 + "'>Total</td> ";
					tfoot.append(tr);
					String td = new String();
					td = "<td class='text-center'>" + taxInvoiceDTO.getTotalQuantity() + "</td>";
					tfoot.append(td);
					td = "<td class='text-center'>" + taxInvoiceDTO.getTotalInvoiceValue() + "</td>";
					tfoot.append(td);
					td = "<td class='text-center'>" + taxInvoiceDTO.getTotalTaxableValueStr() + "</td>";
					tfoot.append(td);
					td = "<td class='text-center'>" + taxInvoiceDTO.getTotalCGSTValueStr() + "</td>";
					tfoot.append(td);
					td = "<td class='text-center'>" + taxInvoiceDTO.getTotalSGSTValueStr() + "</td>";
					tfoot.append(td);
					td = "<td class='text-center'></td>";
					tfoot.append(td);
					td = "<td class='text-center'></td>";
					tfoot.append(td);
					String endTr = "</tr> " + "</tfoot>";
					tfoot.append(endTr);
					finalFooterData.append(tfoot);
				}
			}
		} catch (Exception e) {
			log.error("Exception at PrepareStatment1FooterData() ", e);
		}
	}

	private void prepareStatement5FooterStr(Iterator<String> footerItr, StringBuffer finalFooterData) {
		Long footerIndex = (long) 0;
		try {
			while (footerItr.hasNext()) {
				footerIndex++;
				StringBuffer tfoot = new StringBuffer(110);
				String keyString = footerItr.next();
				if (keyString.equals("Value of the Bill")) {
					String tr = "<tfoot> " + "<tr> " + "<td colspan='" + 8 + "'>Total</td> ";
					tfoot.append(tr);
					String td = new String();
					td = "<td class='text-center'>" + taxInvoiceDTO.getTotalInvoiceValue() + "</td>";
					tfoot.append(td);
					td = "<td class='text-center'>" + taxInvoiceDTO.getTotalCGSTValueStr() + "</td>";
					tfoot.append(td);
					td = "<td class='text-center'>" + taxInvoiceDTO.getTotalSGSTValueStr() + "</td>";
					tfoot.append(td);
					td = "<td class='text-center'></td>";
					tfoot.append(td);
					td = "<td class='text-center'>" + taxInvoiceDTO.getTotalTaxableValueStr() + "</td>";
					tfoot.append(td);
					td = "<td class='text-center'></td>";
					tfoot.append(td);
					String endTr = "</tr> " + "</tfoot>";
					tfoot.append(endTr);
					finalFooterData.append(tfoot);
				}
			}
		} catch (Exception e) {
			log.error("Exception at PrepareStatment1FooterData() ", e);
		}
	}

	private void prepareIGSTStatementFooterStr(Iterator<String> footerItr, StringBuffer finalFooterData) {
		Long footerIndex = (long) 0;
		try {
			while (footerItr.hasNext()) {
				footerIndex++;
				StringBuffer tfoot = new StringBuffer(110);
				String keyString = footerItr.next();
				if (keyString.equals("Quantity")) {
					String tr = "<tfoot> " + "<tr> " + "<td colspan='" + 9 + "'>Total</td> ";
					tfoot.append(tr);
					String td = new String();
					td = "<td class='text-center'>" + taxInvoiceDTO.getTotalQuantity() + "</td>";
					tfoot.append(td);
					td = "<td class='text-center'>" + taxInvoiceDTO.getTotalInvoiceValue() + "</td>";
					tfoot.append(td);
					td = "<td class='text-center'></td>";
					tfoot.append(td);
					td = "<td class='text-center'>" + taxInvoiceDTO.getTotalTaxableValueStr() + "</td>";
					tfoot.append(td);
					td = "<td class='text-center'>" + taxInvoiceDTO.getTotalIGSTValueStr() + "</td>";
					tfoot.append(td);
					td = "<td class='text-center'></td>";
					tfoot.append(td);
					td = "<td class='text-center'></td>";
					tfoot.append(td);
					String endTr = "</tr> " + "</tfoot>";
					tfoot.append(endTr);
					finalFooterData.append(tfoot);
				}
			}
		} catch (Exception e) {
			log.error("Exception at PrepareStatment1FooterData() ", e);
		}
	}

	private void prepareHsnInvoiceBreakupStatementFooterStr(Iterator<String> footerItr, StringBuffer finalFooterData) {
		Long footerIndex = (long) 0;
		try {
			while (footerItr.hasNext()) {
				footerIndex++;
				StringBuffer tfoot = new StringBuffer(110);
				String keyString = footerItr.next();
				if (keyString.equals("Total")) {
					String tr = "<tfoot> " + "<tr> " + "<td colspan='" + 6 + "'>Total</td> ";
					tfoot.append(tr);
					String td = new String();
					td = "<td class='text-center'>" + taxInvoiceDTO.getFormattedTotalInvoiceCountDouble() + "</td>";
					tfoot.append(td);
					String endTr = "</tr> " + "</tfoot>";
					tfoot.append(endTr);
					finalFooterData.append(tfoot);
				}
			}
		} catch (Exception e) {
			log.error("Exception at PrepareStatment1FooterData() ", e);
		}
	}
}

