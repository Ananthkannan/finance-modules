package in.gov.cooptex.finance.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Service;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.repository.AppQueryRepository;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.dto.ReceiptPaymentReportDTO;
import in.gov.cooptex.finance.dto.ReceiptPaymentRequestDTO;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class ReceiptAndPaymentService {

	@PersistenceContext
	EntityManager entityManager;
	
	@Autowired 
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	AppQueryRepository appQueryRepository;
	
	List<ReceiptPaymentReportDTO> receiptPaymentDTOList;
	
	ReceiptPaymentReportDTO receiptPaymentReportDTO;

	public BaseDTO getReceiptPaymentReport(ReceiptPaymentRequestDTO receiptPaymentRequestDTO) {
		log.info("RetailshowRoomSalesReportResponseDtoservice getFinalApprovedList method started : ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("receiptPaymentRequestDTO==>" + receiptPaymentRequestDTO);
			log.info("fromDate==>" + receiptPaymentRequestDTO.getFDate());
			log.info("toDate==>" + receiptPaymentRequestDTO.getTDate());
			
			validationReceiptPaymentReport(receiptPaymentRequestDTO);
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");  
		    String fDate= formatter.format(receiptPaymentRequestDTO.getFDate());  
		    String tDate= formatter.format(receiptPaymentRequestDTO.getTDate());
			
			log.info("fDate==>" + fDate);
			log.info("tDate==>" + tDate);
			
			/*String query = "select t.gl_group,t.gl_name,t.credit,t.debit from (select gag.name as gl_group,gag.display_order,ga.name as gl_name, "
					+ " sum(case when account_aspect='Credit' then coalesce(amount,0) else 0 end) as credit, sum(case when account_aspect='Debit' "
					+ " then coalesce(amount,0) else 0 end) as debit from account_transaction_details atd join gl_account ga on "
					+ " atd.gl_account_id=ga.id join gl_account_group gag on ga.gl_account_group_id=gag.id join gl_account_category gac on "
					+ " gag.category_id=gac.id where date(atd.created_date) between ':fDate' and ':tDate' and gac.name!='Asset' "
					+ " group by gag.id,gag.display_order,ga.id union all select gag.name,gag.display_order,ga.name, "
					+ " sum(case when account_aspect='Debit' then coalesce(amount,0) else 0 end) as credit, sum(case when account_aspect='Credit' "
					+ " then coalesce(amount,0) else 0 end) as debit from account_transaction_details atd join gl_account ga on "
					+ " atd.gl_account_id=ga.id join gl_account_group gag on ga.gl_account_group_id=gag.id join gl_account_category gac on "
					+ " gag.category_id=gac.id where date(atd.created_date) between ':fDate' and ':tDate' and "
					+ " gac.name='Asset' group by gag.id,gag.display_order,ga.id)t order by t.display_order;";*/
			ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("GEN_RECEIPT_PAYMENT_QUERY");
			String query = applicationQuery.getQueryContent();
			query = query.replace(":fDate",fDate);
			query = query.replace(":tDate",tDate);
			 
			log.info("after replace Query==>" + query);
			
			receiptPaymentDTOList = new ArrayList<ReceiptPaymentReportDTO>();
			receiptPaymentReportDTO = new ReceiptPaymentReportDTO();
			receiptPaymentDTOList = jdbcTemplate.query(query, new ReportExtractor());
			log.info("receiptPaymentDTOList size==>"+receiptPaymentDTOList.size());
			baseDTO.setResponseContent(receiptPaymentDTOList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (RestException e) {
			log.info("<--- Exception in getReceiptPaymentReport() ---> ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}catch (Exception e) {
			log.info("<--- Exception in getReceiptPaymentReport() ---> ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return baseDTO;
	}
	
	private void validationReceiptPaymentReport(ReceiptPaymentRequestDTO receiptPaymentRequestDTO) {
		
		Validate.notNull(receiptPaymentRequestDTO.getFDate(), ErrorDescription.INTEND_REQUEST_FROM_DATE_REQUIRED);
		Validate.notNull(receiptPaymentRequestDTO.getTDate(), ErrorDescription.INTEND_REQUEST_TO_DATE_REQUIRED);
		if(receiptPaymentRequestDTO.getFDate().after(receiptPaymentRequestDTO.getTDate())) {
			throw new RestException(ErrorDescription.TODATE_SHOULD_GREATER_THAN_FROMDATE);
		}
	}

	private class ReportExtractor implements ResultSetExtractor<List<ReceiptPaymentReportDTO>> {

		@Override
		public List<ReceiptPaymentReportDTO> extractData(ResultSet rs) throws SQLException, DataAccessException {
			
			while (rs.next()) {
				ReceiptPaymentReportDTO paymentReportDTODtos = new ReceiptPaymentReportDTO();
				paymentReportDTODtos.setGroupName(rs.getString("gl_group"));
				paymentReportDTODtos.setReportName(rs.getString("gl_name"));
				paymentReportDTODtos.setDepit(rs.getDouble("credit"));
				paymentReportDTODtos.setCredit(rs.getDouble("debit"));
				receiptPaymentDTOList.add(paymentReportDTODtos);
				log.info("receiptPaymentDTOList ==>" + receiptPaymentDTOList);
				
			}
			return receiptPaymentDTOList;
		}
	}

	
	
	
}
