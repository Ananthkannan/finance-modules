package in.gov.cooptex.finance.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.model.RebateClaim;
import in.gov.cooptex.core.accounts.model.RebateClaimDetails;
import in.gov.cooptex.core.accounts.model.RebateClaimNote;
import in.gov.cooptex.core.accounts.repository.RebateClaimDetailsRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.EntityTypeMasterRepository;
import in.gov.cooptex.core.repository.SalesInvoiceRepository;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.finance.dto.ClaimStatementDto;
import in.gov.cooptex.finance.dto.ClaimStatementDtoList;
import in.gov.cooptex.finance.repository.RebateClaimNoteRepository;
import in.gov.cooptex.operation.model.SalesInvoice;
import lombok.extern.log4j.Log4j2;

/**
 * 
 * @author Vimalsharma
 *
 */
@Log4j2
@Service
public class RebateClaimDetailsService {
	
	@Autowired
	RebateClaimDetailsRepository rebateClaimDetailsRepository;
	
	@Autowired
	EntityMasterRepository entityRepository;
	
	@Autowired
	EntityTypeMasterRepository entityTypeRepository;
	
	@Autowired
	SalesInvoiceRepository salesInvoiceRepository;
	
	@Autowired
	RebateClaimNoteRepository rebateClaimNoteRepository;
	
	@Autowired
	ResponseWrapper responseWrapper;
	
	
	public void saveRebateClaimDetailsData(ClaimStatementDto claimStatementDto,
			ClaimStatementDtoList claimStatementDtoList, RebateClaim rebateClaim) {
		
		log.error("Enter into saveRebateClaimDetailsData" + claimStatementDto!=null?"not null":null);
		BaseDTO baseDTO=new BaseDTO();
		try
		{
			RebateClaimDetails rebateClaimDetails=new RebateClaimDetails();
			if(claimStatementDto !=null && claimStatementDtoList !=null)
			{
				rebateClaimDetails.setCreatedBy(claimStatementDto.getCreatedBy());
				rebateClaimDetails.setCreatedDate(new Date());
				rebateClaimDetails.setModifiedBy(null);
				rebateClaimDetails.setModifiedDate(null);
				SalesInvoice saleInvoiceId=salesInvoiceRepository.getById(claimStatementDtoList.getSalesInvoiceId());
				EntityMaster regionId=entityRepository.getById(claimStatementDtoList.getRegionId());
				EntityMaster showroomId=entityRepository.getById(claimStatementDtoList.getShowroomId());
				rebateClaimDetails.setRegion(regionId);
				rebateClaimDetails.setShowroom(showroomId);
				rebateClaimDetails.setSalesInvoice(saleInvoiceId);
				rebateClaimDetails.setRebateClaim(rebateClaim);
				rebateClaimDetails.setVersion((long) 0);
				rebateClaimDetailsRepository.save(rebateClaimDetails);
			}
		}catch (RestException restexp) {
			log.error("RebateClaimService RestException", restexp);
			baseDTO.setStatusCode(restexp.getErrorCodeDescription().getErrorCode());
		} catch (DataIntegrityViolationException diExp) {
			log.error("Data Integrity Violation Exception while RebateClaimService group " + diExp);
        } catch (Exception exception) {
			log.error("Error while Creating RebateClaimDetail ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.error("Exit into saveRebateClaimDetailsData" + claimStatementDto);
	}


	/*public ClaimStatementDto getByRebateClaimId(Long rebateClaimId) {
		log.info("RebateClaimDetailsService getByRebateClaimId method started [" + rebateClaimId + "]");
		ClaimStatementDto claimStatementDto = new ClaimStatementDto();
		try {
			ClaimStatementDto claimDto=new ClaimStatementDto();
			List<RebateClaimDetails> rebateClaimDetailsList = rebateClaimDetailsRepository.getById(rebateClaimId);
			claimDto.setRebateClaimDetailsList(rebateClaimDetailsList);
			RebateClaimNote rebateClaimNote=rebateClaimNoteRepository.getById(rebateClaimId);
			claimDto.setRebateClaimNote(rebateClaimNote);
			baseDTO.setResponseContent(claimDto);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("RebateClaimService getById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("RebateClaimService getById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("RebateClaimService getById method completed");
		return responseWrapper.send(baseDTO);
	}*/


	
	

}
