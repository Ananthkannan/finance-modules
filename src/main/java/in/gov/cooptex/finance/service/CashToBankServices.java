package in.gov.cooptex.finance.service;

import in.gov.cooptex.common.repository.CashMovementRepository;
import in.gov.cooptex.common.service.EntityMasterService;
import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.model.*;
import in.gov.cooptex.core.accounts.repository.EntityBankBranchRepository;
import in.gov.cooptex.core.accounts.util.VoucherTypeCons;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.TenderDTO;
import in.gov.cooptex.core.enums.SequenceName;
import in.gov.cooptex.core.finance.service.OperationService;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeePersonalInfoEmployment;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.SequenceConfig;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.AmountTransferDetailsRepository;
import in.gov.cooptex.core.repository.AmountTransferRepository;
import in.gov.cooptex.core.repository.AppQueryRepository;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.SequenceConfigRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AmountMovementType;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.dto.pos.AddChallanInfoDTO;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.FinanceErrorCode;
import in.gov.cooptex.exceptions.LedgerPostingException;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.*;
import in.gov.cooptex.finance.repository.AmountTransferLogRepository;
import in.gov.cooptex.finance.repository.AmountTransferNoteRepository;
import lombok.extern.log4j.Log4j2;

import org.activiti.engine.impl.util.CollectionUtil;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.poi.hssf.record.OldLabelRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Log4j2
public class CashToBankServices {

	@Autowired
	CashMovementRepository cashMovementRepository;

	@Autowired
	AmountTransferRepository amountTransferRepository;

	@Autowired
	AmountTransferDetailsRepository amountTransferDetailsRepository;

	@Autowired
	EntityMasterRepository entityMasterRepository;
	@Autowired
	EmployeeMasterRepository employeeMasterRepository;
	@Autowired
	AmountTransferLogRepository amountTransferLogRepository;
	@Autowired
	EntityMasterService entityMasterService;
	@PersistenceContext
	EntityManager entityManager;
	@Autowired
	AmountTransferNoteRepository amountTransferNoteRepository;

	@Autowired
	EntityBankBranchRepository entityBankBranchRepository;
	@Autowired
	UserMasterRepository userMasterRepository;

	@Autowired
	AppQueryRepository appQueryRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	OperationService operationService;

	@Autowired
	SequenceConfigRepository sequenceConfigRepository;
	@Autowired
	LoginService loginService;

	private static Date getZeroTimeDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		date = calendar.getTime();
		return date;
	}

	public BaseDTO searchData(CashtoBankRequestDTO data) {
		log.info("cash to Bank searchData method Start  : " + data.getId());
		BaseDTO baseDTO = new BaseDTO();
		// List<CashMovement> cashMovementList = new ArrayList<CashMovement>();
		CashMovement cashMovement = new CashMovement();
		// String pattern = "yyyy-MM-dd";
		// SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		// String date = simpleDateFormat.format(data.getDate());
		String referenceNumber = "";
		Date currentDate = new Date();
		try {
			// baseDTO = entityMasterService.getEntityInfoByLoggedInUser(data.getId());
			// EntityMaster entityMaster =
			// entityMasterRepository.getEntityInfoByLoggedInUser(data.getId());
			// EntityMaster entityMaster = new EntityMaster();
			Object[] resultObj = entityMasterRepository.gettypeCodeIdRegionIdByLoggedInUser(data.getId());
			Object ob[] = (Object[]) resultObj[0];
			Long entityId = ob[1] != null ? Long.valueOf(ob[1].toString()) : null;
			String entityCode = ob[3] != null ? ob[3].toString() : "";

			// EntityMaster entity = (EntityMaster) baseDTO.getResponseContent();
			cashMovement = cashMovementRepository.SearchCashToBankData(entityId, data.getDate());
			/*
			 * log.info("cashMovementList ::"+cashMovementList.size()); for (CashMovement
			 * cashMovementobj : cashMovementList) { if
			 * (date.equalsIgnoreCase(simpleDateFormat.format(cashMovementobj.getClosedDate(
			 * )))) { cashMovement = cashMovementobj; } }
			 */
			if (cashMovement != null) {
				SequenceConfig sequenceConfig = sequenceConfigRepository.findBySequenceName(SequenceName.CASH_TO_BANK);
				if (sequenceConfig == null) {
					throw new RestException(ErrorDescription.SEQUENCE_CONFIG_NOT_EXIST);
				}
				log.info("current value-------------" + sequenceConfig.getCurrentValue());
				referenceNumber = entityCode + sequenceConfig.getSeparator() + AppUtil.getDay(currentDate)
						+ AppUtil.getFormattedCurrentDate("MM") + AppUtil.getCurrentYearString()
						+ sequenceConfig.getSeparator() + sequenceConfig.getPrefix() + sequenceConfig.getSeparator()
						+ sequenceConfig.getCurrentValue();
				log.info("reference number-----------" + referenceNumber);

				cashMovement.setReferenceNumber(referenceNumber);

				baseDTO.setResponseContent(cashMovement != null ? cashMovement : null);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (Exception ex) {
			log.info("Error In searchData ------------> : ", ex);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			return null;
		}
		log.info("cash to Bank searchData method End ------------> : ");
		return baseDTO;
	}

	public BaseDTO updateCashToBank(CashToBankAddRequestDTO request) {
		log.info("CashToBankService. updateCashToBank() - START");
		BaseDTO baseDTO = new BaseDTO();
		List<AmountTransferDetails> amountTransferDetailsList = new ArrayList<>();
		try {
			if (request != null) {
				Long amountTransferId = request.getAmountTransferId();
				log.info("CashToBankService. updateCashToBank() - amountTransferId: " + amountTransferId);
				if (amountTransferId != null) {

					amountTransferDetailsRepository.deletebyamountTransferId(amountTransferId);

					AmountTransfer amountTransfer = amountTransferRepository.findOne(amountTransferId);
					List<AddChallanInfoDTO> challanInfoList = request.getChallanaInformation();
					if (!CollectionUtil.isEmpty(challanInfoList)) {
						for (AddChallanInfoDTO addChallanInfoDTO : request.getChallanaInformation()) {
							AmountTransferDetails amountTransferDetails = new AmountTransferDetails();
							amountTransferDetails.setChallanAmount(addChallanInfoDTO.getChallanAmount());
							amountTransferDetails.setChallanDate(addChallanInfoDTO.getChallanDate());
							amountTransferDetails.setChallanNumber(addChallanInfoDTO.getChallanNumber());
							EntityBankBranch EntityBankBranch = entityBankBranchRepository
									.getOne(addChallanInfoDTO.getEntityBankBranchId());
							amountTransferDetails.setToBranch(EntityBankBranch);
							amountTransferDetails.setAmountTransfer(amountTransfer);
							amountTransferDetailsList.add(amountTransferDetails);
						}
						amountTransferDetailsRepository.save(amountTransferDetailsList);
					}
				}
			}
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception ex) {
			log.info("Exception at updateCashToBank()", ex);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("CashToBankService. updateCashToBank() - END");
		return baseDTO;
	}

	// @Transactional
	public BaseDTO createCashToBank(CashToBankAddRequestDTO cashTOBank) {

		log.info("CashToBank:createCashToBank [" + cashTOBank + "]");
		BaseDTO baseDTO = new BaseDTO();
		AmountTransfer amountTransfer = new AmountTransfer();
		List<AmountTransferDetails> amountTransferDetailsList = new ArrayList<AmountTransferDetails>();
		List<AmountTransferDetails> amountTransferDetailsObjList = new ArrayList<AmountTransferDetails>();
		AmountTransfer amountTransferObj = new AmountTransfer();
		AmountTransferLog amountTransferLog = new AmountTransferLog();
		// AmountTransferNote amountTransferNote=new AmountTransferNote();
		String sequenceNo = "";
		try {
			EntityMaster entityMaster = entityMasterRepository
					.getWorkLocationByUserId(loginService.getCurrentUser().getId());
			AmountTransfer amountTransferObj1 = amountTransferRepository
					.getAmountTransferByentityAndClosingDate(cashTOBank.getClosingDate(), entityMaster.getId());
			if (amountTransferObj1 != null) {
				CashMovement cashMovement = cashMovementRepository.findOne(amountTransferObj1.getId());
				if (cashMovement != null) {
					baseDTO.setStatusCode(MastersErrorCode.CASH_TO_BANK_ALREADY_EXIT);
					return baseDTO;
				}
			}

			log.info("CashToBank---:  set DTO to Entity");
			amountTransfer = setAmountTransferEntity(cashTOBank);

			amountTransferDetailsList = setAmountTransferDetailsEntity(cashTOBank);

			amountTransferLog = setAmountTransferLog(cashTOBank);

			// amountTransferNote=setAmountTransferNote(cashTOBank);

			/*
			 * if (cashTOBank.getRemainingchallanamt()>0 ) { baseDTO.
			 * setErrorDescription(" Challan Amount is less  than the Remaining Challan Amount to be Deposited "
			 * ); return baseDTO; }
			 */

			log.info("CashToBank---:   AmountTransfer before save");

			amountTransferObj = amountTransferRepository.save(amountTransfer);

			log.info("CashToBank---:   AmountTransfer saved");

			for (AmountTransferDetails amountTransferDetObj : amountTransferDetailsList) {

				amountTransferDetObj.setAmountTransfer(amountTransferObj);
				amountTransferDetailsObjList.add(amountTransferDetObj);

				sequenceNo = amountTransferDetObj.getChallanNumber().split("-")[3];
			}
			log.info("sequence number----------" + sequenceNo);
			log.info("CashToBank---:   AmountTransferDetails before save");

			amountTransferDetailsRepository.save(amountTransferDetailsObjList);

			log.info("CashToBank---:   AmountTransferDetails saved");

			amountTransferLog.setAmountTransfer(amountTransferObj);

			log.info("CashToBank---:   AmountTransferLog before save");

			amountTransferLogRepository.save(amountTransferLog);

			log.info("CashToBank---:   AmountTransferLog  saved");

			/*
			 * amountTransferNote.setAmountTransfer(amountTransferObj);
			 * 
			 * log.info("CashToBank---:   AmountTransferNote before save");
			 * 
			 * amountTransferNoteRepository.save(amountTransferNote);
			 * 
			 * log.info("CashToBank---:   AmountTransferNote  saved");
			 */

			log.info("cashTOBank Saved Successfully........");

			SequenceConfig sequenceConfig = sequenceConfigRepository.findBySequenceName(SequenceName.CASH_TO_BANK);
			sequenceConfig.setCurrentValue(sequenceNo != "" ? (Long.valueOf(Integer.valueOf(sequenceNo)))
					: sequenceConfig.getCurrentValue() + 1);
			sequenceConfigRepository.save(sequenceConfig);

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (LedgerPostingException ledgerEx) {
			log.error("Error Saving LedgerPosting", ledgerEx);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		} catch (DataIntegrityViolationException divEX) {
			log.warn("<<===  Error While updating  ===>>", divEX);
		} catch (Exception exception) {
			log.error("Error Saving ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public AmountTransfer setAmountTransferEntity(CashToBankAddRequestDTO cashTOBank) {
		log.info("<---CashToBank:setAmountTransferEntity Method Start--->");
		AmountTransfer amountTransfer = new AmountTransfer();
		BaseDTO baseDTO = new BaseDTO();
		try {
			Validate.notNull(cashTOBank.getTotalAmountTransferred(), ErrorDescription.TOTAL_AMOUNT_TRANSFER);
			if (cashTOBank.getAmountTransferId() != null) {
				amountTransfer = amountTransferRepository.findOne(cashTOBank.getAmountTransferId());
			}

			if (cashTOBank.getTransferBy() != null) {
				amountTransfer.setTransferedBy(employeeMasterRepository.findOne(cashTOBank.getTransferBy()));
			}

			if (cashTOBank.getCashMovement().getEntityMaster() != null) {
				amountTransfer.setEntityMaster(cashTOBank.getCashMovement().getEntityMaster());
			}
			amountTransfer.setCashMovement(cashTOBank.getCashMovement());
			if (cashTOBank.getTotalAmountTransferred() != null) {
				amountTransfer.setTotalAmountTransfered(cashTOBank.getTotalAmountTransferred());
			}
			amountTransfer.setOtherExpenseAmount(cashTOBank.getOtherExpenseAmount());
			amountTransfer.setMovementType(AmountMovementType.CASH_TO_BANK);
			amountTransfer.setRemarks(cashTOBank.getRemarks());
			amountTransfer.setDepositDate(cashTOBank.getDepositDate());
		} catch (Exception exception) {

			log.error("Error Saving ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());

		}
		log.info("<---CashToBank:setAmountTransferEntity Method End--->");
		return amountTransfer;

	}

	public List<AmountTransferDetails> setAmountTransferDetailsEntity(CashToBankAddRequestDTO cashTOBank) {
		log.info("<---CashToBank:setAmountTransferEntity Method Start--->");
		AmountTransferDetails amountTransferDetails = null;
		List<AmountTransferDetails> amountTransferDetailsList = new ArrayList<AmountTransferDetails>();
		List<AmountTransferDetails> amountTransferDetailsRemoveList = new ArrayList<AmountTransferDetails>();
		BaseDTO baseDTO = new BaseDTO();
		try {
			if (cashTOBank.getAmountTransferId() != null) {
				amountTransferDetailsRemoveList = amountTransferDetailsRepository
						.getAmountTransferList(cashTOBank.getAmountTransferId());
				log.info("<<====  List Size:: " + amountTransferDetailsRemoveList);
				amountTransferDetailsRepository.delete(amountTransferDetailsRemoveList);
			}

			log.info("<---CashToBank:set cashTOBank setAmountTransferDetailsEntity --->");
			for (AddChallanInfoDTO addChallanInfoDTO : cashTOBank.getChallanaInformation()) {
				amountTransferDetails = new AmountTransferDetails();
				amountTransferDetails.setChallanAmount(addChallanInfoDTO.getChallanAmount());
				amountTransferDetails.setChallanDate(addChallanInfoDTO.getChallanDate());
				amountTransferDetails.setChallanNumber(addChallanInfoDTO.getChallanNumber());
				// List<EntityBankBranch>
				// EntityBankBranch=entityBankBranchRepository.findByBankBranchMasterIdAndBankAccountTypeId(addChallanInfoDTO.getBranchId(),addChallanInfoDTO.getAccountTypeId());
				EntityBankBranch EntityBankBranch = entityBankBranchRepository
						.getOne(addChallanInfoDTO.getEntityBankBranchId());
				// amountTransferDetails.setFromBranch(EntityBankBranch);
				amountTransferDetails.setToBranch(EntityBankBranch);
				amountTransferDetailsList.add(amountTransferDetails);
			}

		} catch (Exception exception) {

			log.error("Error Saving ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());

		}
		log.info("<---CashToBank:setAmountTransferEntity Method End--->");
		return amountTransferDetailsList;

	}

	public AmountTransferLog setAmountTransferLog(CashToBankAddRequestDTO cashTOBank) {
		log.info("<---CashToBank:setAmountTransferLog Method Start--->");
		BaseDTO baseDTO = new BaseDTO();
		AmountTransferLog amountTransferLog = new AmountTransferLog();
		try {
			/*
			 * if(cashTOBank.getAmountTransferId()!=null) {
			 * amountTransferLog=amountTransferLogRepository.getAmountTransferLogDetails(
			 * cashTOBank.getAmountTransferId()); }
			 */
			log.info("<---CashToBank:set cashTOBank to AmountTransferLog Entity--->");
			amountTransferLog.setStage(AmountMovementType.SUBMITTED);
			amountTransferLog.setRemarks(cashTOBank.getApproveRemark());
		} catch (Exception exp) {
			log.error("Error Saving ", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<---CashToBank:setAmountTransferLog Method End--->");
		return amountTransferLog;

	}

	public AmountTransferNote setAmountTransferNote(CashToBankAddRequestDTO cashTOBank) {
		log.info("<---CashToBank:setAmountTransferNote Method Start--->");
		BaseDTO baseDTO = new BaseDTO();
		AmountTransferNote amountTransferNote = new AmountTransferNote();
		try {
			Validate.notNull(cashTOBank.getForwardTo(), ErrorDescription.CASH_TO_BANK_FORWARD_TO);
			Validate.notNull(cashTOBank.getNote(), ErrorDescription.CASH_TO_BANK_NOTES);

			/*
			 * if(cashTOBank.getAmountTransferId()!=null) {
			 * amountTransferNote=amountTransferNoteRepository.getAmountTransferNoteDetails(
			 * cashTOBank.getAmountTransferId()); }
			 */

			log.info("<---CashToBank:set cashTOBank AmountTransferNote --->");
			amountTransferNote.setFinalApproval(cashTOBank.getFinalApproval());
			if (cashTOBank.getForwardTo() != null) {
				amountTransferNote.setForwardTo(userMasterRepository.findOne(cashTOBank.getForwardTo()));
			} else {
				log.info("<---amountTransferNote:setForwardTo Value is null--->");
			}
			if (cashTOBank.getNote() != null) {
				amountTransferNote.setNote(cashTOBank.getNote());
			} else {
				log.info("<---amountTransferNote:Note Value is null--->");
			}
		} catch (Exception exp) {
			log.error("Error Saving ", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<---CashToBank:setAmountTransferNote Method End--->");
		return amountTransferNote;

	}

	public BaseDTO getAllEmployee() {
		log.info("EmployeeService.getAllEmployee() Started ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("getEmployee List---> ");
			List<EmployeeMaster> employeeList = employeeMasterRepository.getAllEmployee();
			if (employeeList != null && employeeList.size() > 0) {
				baseDTO.setResponseContent(employeeList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				log.info("EmployeeService.getAllEmployee() Completed");
			}
		} catch (Exception exception) {
			log.error("Exception occurred in EmployeeService.getAllEmployee() -:", exception);
			baseDTO.setStatusCode(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		log.info("EmployeeService.getAllEmployee() End ");
		return baseDTO;
	}

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	public BaseDTO getViewedCashToBankDetails(CashtoBankResponseDTO cashtoBankResponseDTO) {
		log.info("<------CashTOBank getViewedCashToBankDetails method started ------->");

		log.info("amountTansfer Id==>" + cashtoBankResponseDTO.getId());
		BaseDTO baseDTO = new BaseDTO();
		CashToBankAddRequestDTO cashToBankAddRequestDTO = new CashToBankAddRequestDTO();
		List<AmountTransferDetails> amountTransferList = new ArrayList<AmountTransferDetails>();
		List<AddChallanInfoDTO> challanaInformation = new ArrayList<AddChallanInfoDTO>();
		AmountTransferNote amountTransferNote = new AmountTransferNote();
		AmountTransfer amountTransfer = new AmountTransfer();
		String referenceNumber = "";
		Date currentDate = new Date();
		try {

			/*
			 * EntityMaster entityMaster = entityMasterRepository
			 * .getEntityInfoByLoggedInUser(cashtoBankResponseDTO.getLoggedInUserId());
			 */

			// EntityMaster entity = (EntityMaster) baseDTO.getResponseContent();

			Object[] resultObj = entityMasterRepository
					.gettypeCodeIdRegionIdByLoggedInUser(cashtoBankResponseDTO.getLoggedInUserId());
			Object ob[] = (Object[]) resultObj[0];
			String entityMasterTypeCode = (String) (ob[0] != null ? ob[0] : "");
			Long entityId = ob[1] != null ? Long.valueOf(ob[1].toString()) : null;
			Long regionId = ob[2] != null ? Long.valueOf(ob[2].toString()) : null;
			String entityCode = ob[3] != null ? ob[3].toString() : "";

			if (entityId != null) {
				log.info("entityMaster Id :" + entityId);
			}
			log.info("<=====get amount transferList==>");
			amountTransfer = amountTransferRepository.findOne(cashtoBankResponseDTO.getId());
			amountTransferList = amountTransferDetailsRepository.getAmountTransferList(cashtoBankResponseDTO.getId());
			// amountTransferNote=amountTransferNoteRepository.findByAmountTransferId(cashtoBankResponseDTO.getId());
			AmountTransferLog amountTransferLog = amountTransferLogRepository
					.findByAmountTransferId(amountTransfer != null ? amountTransfer.getId() : 0);

			/*
			 * Long amountTransferRegionId = (amountTransfer != null ?
			 * (amountTransfer.getEntityMaster() != null ?
			 * (amountTransfer.getEntityMaster().getEntityMasterRegion() != null ?
			 * amountTransfer.getEntityMaster().getEntityMasterRegion().getId() : 0) : 0) :
			 * 0);
			 */
			Long amountTransferRegionId = entityMasterRepository
					.getRegionIdByentityMasterId(amountTransfer.getEntityMaster().getId());
			amountTransferRegionId = amountTransferRegionId != null ? amountTransferRegionId : 0;
			if (entityId != null && regionId != null && regionId == amountTransferRegionId) {
				cashToBankAddRequestDTO.setButtonDisableFlag(true);
			} else if (entityId != null && regionId == null && entityId == amountTransferRegionId) {
				cashToBankAddRequestDTO.setButtonDisableFlag(true);
			} else if (entityId != null && entityMasterTypeCode.equalsIgnoreCase("HEAD_OFFICE")) {
				cashToBankAddRequestDTO.setButtonDisableFlag(true);
			} else {
				cashToBankAddRequestDTO.setButtonDisableFlag(false);
			}

			log.info("<=====get list Of challan details==>");
			for (AmountTransferDetails amountTransferDetails : amountTransferList) {
				AddChallanInfoDTO challanDto = new AddChallanInfoDTO();
				challanDto.setChallanAmount(amountTransferDetails.getChallanAmount());
				challanDto.setChallanDate(amountTransferDetails.getChallanDate());
				challanDto.setChallanNumber(amountTransferDetails.getChallanNumber());
				challanDto.setBankId(amountTransferDetails.getToBranch().getBankBranchMaster().getBankMaster().getId());
				challanDto.setEntityBankBranchId(amountTransferDetails.getToBranch().getId());
				challanDto.setBranchId(amountTransferDetails.getToBranch().getBankBranchMaster().getId());
				challanDto.setAccountTypeId(amountTransferDetails.getToBranch().getBankAccountType().getId());
				challanDto.setBranchName(amountTransferDetails.getToBranch().getBankBranchMaster().getBranchName());
				challanDto.setAccountNumber(amountTransferDetails.getToBranch().getAccountNumber());
				challanaInformation.add(challanDto);
			}

			SequenceConfig sequenceConfig = sequenceConfigRepository.findBySequenceName(SequenceName.CASH_TO_BANK);
			if (sequenceConfig == null) {
				throw new RestException(ErrorDescription.SEQUENCE_CONFIG_NOT_EXIST);
			}
			log.info("current value-------------" + sequenceConfig.getCurrentValue());
			referenceNumber = entityCode != null ? entityCode
					: "" + sequenceConfig.getSeparator() + AppUtil.getDay(currentDate)
							+ AppUtil.getFormattedCurrentDate("MM") + AppUtil.getCurrentYearString()
							+ sequenceConfig.getSeparator() + sequenceConfig.getPrefix() + sequenceConfig.getSeparator()
							+ sequenceConfig.getCurrentValue();
			log.info("reference number-----------" + referenceNumber);

			cashToBankAddRequestDTO.setReferenceNumber(referenceNumber);
			log.info("<=====set entity to cashToBankAddRequestDTO ==>");
			cashToBankAddRequestDTO.setAmountTransferId(amountTransfer.getId());
			// cashToBankAddRequestDTO.setNote(amountTransferNote.getNote());
			cashToBankAddRequestDTO.setTransferBy(amountTransfer.getTransferedBy().getId());
			// cashToBankAddRequestDTO.setForwardTo(amountTransferNote.getForwardTo().getId());
			// cashToBankAddRequestDTO.setFinalApproval(amountTransferNote.getFinalApproval());
			cashToBankAddRequestDTO.setChallanaInformation(challanaInformation);
			cashToBankAddRequestDTO.setCashMovement(amountTransfer.getCashMovement());
			cashToBankAddRequestDTO.setRemarks(amountTransfer.getRemarks());
			cashToBankAddRequestDTO.setOtherExpenseAmount(amountTransfer.getOtherExpenseAmount());
			cashToBankAddRequestDTO.setTotalAmountTransferred(amountTransfer.getTotalAmountTransfered());
			cashToBankAddRequestDTO.setStage(amountTransferLog.getStage());
			cashToBankAddRequestDTO.setApproveRemark(amountTransferLog.getRemarks());
			cashToBankAddRequestDTO.setRejectRemark(amountTransferLog.getRemarks());
			cashToBankAddRequestDTO.setDepositDate(amountTransfer.getDepositDate());
			cashToBankAddRequestDTO.setEntityCodeName((amountTransfer.getCashMovement().getEntityMaster() != null
					? amountTransfer.getCashMovement().getEntityMaster().getCode()
					: "")
					+ "/"
					+ (amountTransfer.getCashMovement().getEntityMaster() != null
							? amountTransfer.getCashMovement().getEntityMaster().getName()
							: ""));
			baseDTO.setResponseContent(cashToBankAddRequestDTO);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

			List<Map<String, Object>> employeeData = new ArrayList<Map<String, Object>>();
			if (amountTransfer != null) {
				log.info("<<<:::::::amountTransfer::::not Null::::>>>>" + amountTransfer);
				ApplicationQuery applicationQueryForlog = applicationQueryRepository
						.findByQueryName("AMOUNT_TRANSFER_LOG_EMPLOYEE_DETAILS");
				if (applicationQueryForlog == null || applicationQueryForlog.getId() == null) {
					log.info("Application Query For Log Details not found for query name : " + applicationQueryForlog);
					baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
					return baseDTO;
				}
				String logquery = applicationQueryForlog.getQueryContent().trim();
				log.info("<=========AMOUNT_TRANSFER_LOG_EMPLOYEE_DETAILS Query Content ======>" + logquery);
				logquery = logquery.replace(":amounttransId", "'" + amountTransfer.getId().toString() + "'");
				log.info("Query Content For AMOUNT_TRANSFER_LOG_EMPLOYEE_DETAILS After replaced plan id View query : "
						+ logquery);
				employeeData = jdbcTemplate.queryForList(logquery);
				log.info("<=========AMOUNT_TRANSFER_LOG_EMPLOYEE_DETAILS Employee Data======>" + employeeData);
				baseDTO.setTotalListOfData(employeeData);
			}
		} catch (RestException exception) {
			baseDTO.setStatusCode(exception.getStatusCode());
			log.error("CashTOBank getViewedCashToBankDetails method RestException ", exception);
		} catch (Exception exp) {
			log.info("<-----Error In  getViewedCashToBankDetails method------->", exp);
		}
		return baseDTO;

	}

	/**
	 * @param paginationDTO
	 * @param userID
	 * @return
	 */
	public BaseDTO getAllDetails(PaginationDTO paginationDTO, Long userID) {
		log.info("<<====   ChequeToBankService ---  getAll ====## STARTS");
		BaseDTO baseDTO = null;
		Integer total = 0;
		double totalamountDeposite = 0.0;
		List<CashtoBankResponseDTO> resultList = null;
		try {
			resultList = new ArrayList<CashtoBankResponseDTO>();
			baseDTO = new BaseDTO();
			Integer start = paginationDTO.getFirst(), pageSize = paginationDTO.getPageSize();
			start = start * pageSize;

			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> sublistofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();
			//
			ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("CASH_TO_BANK");
			//
			String mainQuery = applicationQuery.getQueryContent();
			//
			log.info("DB QUERY ---------- " + mainQuery);
			//
			Object[] resultObj = entityMasterRepository.gettypeCodeIdRegionIdByLoggedInUser(userID);
			Object ob[] = (Object[]) resultObj[0];
			//
			String entityMasterTypeCode = (String) (ob[0] != null ? ob[0] : "");
			Long entityId = ob[1] != null ? Long.valueOf(ob[1].toString()) : null;
			Long regionId = ob[2] != null ? Long.valueOf(ob[2].toString()) : null;

			String condition = "'" + AmountMovementType.CASH_TO_BANK + "'";

			if (entityMasterTypeCode.equalsIgnoreCase("HEAD_OFFICE")) {
				condition = condition + "";
			} else if (entityMasterTypeCode.equalsIgnoreCase("REGIONAL_OFFICE") && entityId != null) {
				condition = condition + " AND em.id IN (SELECT id FROM entity_master em1 WHERE em1.region_id="
						+ entityId + ")";
				if (regionId != null && regionId > 0) {
					condition = condition + " OR em.id IN (SELECT id FROM entity_master em1 WHERE em1.region_id="
							+ regionId + ")";
				}
			} else if (entityMasterTypeCode.equalsIgnoreCase("D_AND_P_OFFICE") && regionId != null) {
				condition = condition + " AND em.id IN (SELECT id FROM entity_master em1 WHERE em1.region_id="
						+ regionId + ")";
			} else {
				condition = condition + " and em.id=" + entityId;
			}
			
//			else if (entityMasterTypeCode.equalsIgnoreCase("SHOW_ROOM") && entityId != null) {
//				condition = condition + " and em.id=" + entityId;
//			} else if (entityMasterTypeCode.equalsIgnoreCase("INSTITUTIONAL_SALES_SHOWROOM") && entityId != null) {
//				condition = condition + " and em.id=" + entityId;
//			}

			mainQuery = mainQuery.replaceAll(":CASH_TO_BANK", condition);
			log.info("after replace query------" + mainQuery);

			if (paginationDTO.getFilters() != null) {
				if (paginationDTO.getFilters().get("closingDate") != null) {
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					Date closingDate = new Date((long) paginationDTO.getFilters().get("closingDate"));
					log.info(" closingDate Filter : " + closingDate);
					mainQuery += " and cash.closed_date::date='" + format.format(closingDate) + "'";
				}
				if (paginationDTO.getFilters().get("entityName") != null) {
					mainQuery += " and upper(concat(em.name,' ',em.code)) like upper('%"
							+ paginationDTO.getFilters().get("entityName") + "%')";
				}
				if (paginationDTO.getFilters().get("status") != null) {
					mainQuery += " and stage='" + paginationDTO.getFilters().get("status") + "'";
				}
				if (paginationDTO.getFilters().get("totalAmountDeposited") != null) {

					mainQuery += " and att.total_amount_transfered>= '"
							+ paginationDTO.getFilters().get("totalAmountDeposited") + "'";
				}
				if (paginationDTO.getFilters().get("amountTransferredBy") != null) {
					mainQuery += " and upper(concat(emp.first_name,' ',emp.last_name)) like upper('%"
							+ paginationDTO.getFilters().get("amountTransferredBy") + "%')";
				}
				if (paginationDTO.getFilters().get("createdDate") != null) {
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					Date fromDate = new Date((long) paginationDTO.getFilters().get("createdDate"));
					log.info(" fromDate Filter : " + fromDate);
					mainQuery += " and att.created_date::date='" + format.format(fromDate) + "'";
				}
				if (paginationDTO.getFilters().get("closingYear") != null) {
					mainQuery += " and to_char(cash.closed_date,'yyyy')='"
							+ paginationDTO.getFilters().get("closingYear") + "'";
				}
				if (paginationDTO.getFilters().get("closingMonth") != null) {
					mainQuery += " and to_char(cash.closed_date,'Mon')='"
							+ paginationDTO.getFilters().get("closingMonth") + "'";
				}
			}
			String countQuery = "select count(*) AS count  from (" + mainQuery + ")t";
			log.info("count query... " + countQuery);
			countData = jdbcTemplate.queryForList(countQuery);
			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					total = Integer.parseInt(data.get("count").toString().replace(",", ""));
			}
			log.info("total query... " + total + "::pageSize::>>>" + pageSize + "::start::" + start);

			if (paginationDTO.getSortField() != null && paginationDTO.getSortOrder() != null) {
				log.info("Sort Field:[" + paginationDTO.getSortField() + "] Sort Order:[" + paginationDTO.getSortOrder()
						+ "]");
				if (paginationDTO.getSortField().equals("closingDate")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by att.created_date asc  ";
				if (paginationDTO.getSortField().equals("closingDate")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by att.created_date desc  ";

				if (paginationDTO.getSortField().equals("entityName")
						&& paginationDTO.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by em.name asc ";
				}
				if (paginationDTO.getSortField().equals("entityName")
						&& paginationDTO.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by em.name desc ";
				}

				if (paginationDTO.getSortField().equals("totalAmountDeposited")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by att.total_amount_transfered asc  ";
				if (paginationDTO.getSortField().equals("totalAmountDeposited")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by att.total_amount_transfered desc  ";
				if (paginationDTO.getSortField().equals("amountTransferredBy")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by emp.first_name asc ";
				if (paginationDTO.getSortField().equals("amountTransferredBy")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by emp.first_name desc ";
				if (paginationDTO.getSortField().equals("createdDate")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by att.created_date asc ";
				if (paginationDTO.getSortField().equals("createdDate")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by att.created_date desc ";
				if (paginationDTO.getSortField().equals("status") && paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by stage asc ";
				if (paginationDTO.getSortField().equals("status") && paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by stage desc ";

				if (paginationDTO.getSortField().equals("closingMonth")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by to_char(cash.closed_date,'Mon') asc ";
				if (paginationDTO.getSortField().equals("closingMonth")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by to_char(cash.closed_date,'Mon') desc ";

				if (paginationDTO.getSortField().equals("closingYear")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by to_char(cash.closed_date,'yyyy') asc ";
				if (paginationDTO.getSortField().equals("closingYear")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by  to_char(cash.closed_date,'yyyy') desc ";

				String subMainquery = mainQuery;
				sublistofData = jdbcTemplate.queryForList(subMainquery);

				for (Map<String, Object> object : sublistofData) {
					if (object.get("totalamount") != null) {
						System.out.println("error aisila dekh bhakua" + object.get("totalamount"));
						double dd = Double.parseDouble(object.get("totalamount").toString());
						totalamountDeposite = totalamountDeposite + dd;
					}
				}
				mainQuery += " limit " + pageSize + " offset " + start + ";";
			} else {
				String subMainquery = mainQuery;
				sublistofData = jdbcTemplate.queryForList(subMainquery);

				for (Map<String, Object> object : sublistofData) {
					if (object.get("totalamount") != null) {
						double dd = Double.parseDouble(object.get("totalamount").toString());
						totalamountDeposite = totalamountDeposite + dd;
					}

				}
				mainQuery += " order by id desc limit " + pageSize + " offset " + start + ";";
			}

			log.info("filter query----------" + mainQuery);

			// mainQuery += "order by att.created_date desc";
			listofData = jdbcTemplate.queryForList(mainQuery);
			// totalRecords = listofData.size();

			log.info("amount transfer list size-------------->" + listofData.size());
			for (Map<String, Object> data : listofData) {
				CashtoBankResponseDTO response = new CashtoBankResponseDTO();
				response.setId(Long.valueOf(data.get("id").toString()));
				response.setTotalAmountDeposited(Double.valueOf(data.get("totalamount").toString()));
				response.setAmountTransferredBy(
						data.get("employeefirstname").toString() + " " + data.get("employeelastname").toString());
				response.setStatus(data.get("stage").toString());
				response.setCreatedDate((Date) data.get("createddate"));
				response.setClosingDate((Date) data.get("closeDate"));
				response.setClosingYear(data.get("year") != null ? data.get("year").toString() : "");
				response.setClosingMonth(data.get("Month") != null ? data.get("Month").toString() : "");
				response.setEntityName(data.get("code").toString() + " / " + data.get("name").toString());

				resultList.add(response);
			}

			log.info("final list size---------------" + resultList.size());
			log.info("Total records present in getAllSocietyRegistrationlazy..." + total);
			baseDTO.setTotalRecords(total);
			// baseDTO.setTotalRecords(totalRecords);
			baseDTO.setResponseContents(resultList);
			baseDTO.setSumTotal(totalamountDeposite);
			log.info("totalamountDeposite---------------" + totalamountDeposite);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("<<====   ChequeToBankService ---  getAll ====## ENDS");
		} catch (Exception ex) {
			log.error("inside lazy method exception------", ex);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	@Transactional
	public BaseDTO deleteCashToBank(CashtoBankResponseDTO selectedCtoB) {
		log.info("CashToBank====>: deleteCashToBank Start ---->" + selectedCtoB.getId());
		BaseDTO baseDTO = new BaseDTO();
		List<AmountTransferDetails> amountTransferList = new ArrayList<AmountTransferDetails>();
		AmountTransfer amountTransfer = new AmountTransfer();
		AmountTransferNote amountTransferNote = new AmountTransferNote();
		try {
			amountTransferList = amountTransferDetailsRepository.getAmountTransferList(selectedCtoB.getId());
			amountTransferNote = amountTransferNoteRepository.findByAmountTransferId(selectedCtoB.getId());
			amountTransfer = amountTransferRepository.findOne(selectedCtoB.getId());
			amountTransferRepository.delete(amountTransfer.getId());
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("CashToBank Delete deleted successfully....!!!");
		} catch (Exception exp) {
			String exceptionCause = ExceptionUtils.getRootCauseMessage(exp);
			log.error("Exception Cause----> : ", exp);
		}
		log.info("<-----Tender deleteCashToBank End ---->");
		return baseDTO;
	}

	@Transactional
	public BaseDTO approveCashToBank(CashToBankAddRequestDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("approveCashToBank method start=============>" + requestDTO.getAmountTransferId());
			if (AmountMovementType.NOT_CREDITED.equals(requestDTO.getStage())) {

				amountTransferLogRepository.deleteByAmountTransferId(requestDTO.getAmountTransferId(),
						AmountMovementType.FINAL_APPROVED);

			} else {
				AmountTransfer amountTransfer = amountTransferRepository.findOne(requestDTO.getAmountTransferId());

				/*
				 * AmountTransferNote amountTransferNote = new AmountTransferNote();
				 * amountTransferNote.setAmountTransfer(amountTransfer);
				 * amountTransferNote.setFinalApproval(requestDTO.getFinalApproval());
				 * amountTransferNote.setNote(requestDTO.getNote());
				 * amountTransferNote.setForwardTo(userMasterRepository.findOne(requestDTO.
				 * getForwardTo())); log.info("approveCashToBank note inserted------");
				 */

				AmountTransferLog amountTransferLog = new AmountTransferLog();
				amountTransferLog.setAmountTransfer(amountTransfer);
				amountTransferLog.setStage(requestDTO.getStage());
				amountTransferLog.setRemarks(requestDTO.getApproveRemark());
				log.info("approveCashToBank log inserted------");

				log.info("Account posting Start--------" + amountTransfer);
				log.info("Approval status----------" + requestDTO.getStage());

				if (requestDTO.getStage().equalsIgnoreCase(AmountMovementType.FINAL_APPROVED)
						|| requestDTO.getStage().equalsIgnoreCase(AmountMovementType.APPROVED)) {
					log.info("Inside Account Posting Condition :: stage ::" + requestDTO.getStage());
					baseDTO = operationService.amountTransferLedgerPosting(amountTransfer, requestDTO.getStage());
					log.info(":::baseDTO:::::>>>" + baseDTO);
					if (baseDTO != null
							&& baseDTO.getStatusCode().equals(ErrorDescription.SUCCESS_RESPONSE.getCode())) {
						// amountTransferNoteRepository.save(amountTransferNote);
						amountTransferLogRepository.save(amountTransferLog);
					} else {
						throw new LedgerPostingException();
					}
				} else {
					log.info(":::Not FinalApproved Stage::::::Stage:" + requestDTO.getStage());
					// amountTransferNoteRepository.save(amountTransferNote);
					amountTransferLogRepository.save(amountTransferLog);
				}
				log.info("Account posting End--------");

			}
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (LedgerPostingException l) {
			log.error("approveCashToBank method LedgerPostingException----", l);
			baseDTO.setStatusCode(ErrorDescription.getError(FinanceErrorCode.ACCOUNT_POSTING_ERROR).getErrorCode());
			return baseDTO;
		} catch (Exception e) {
			log.error("approveCashToBank method exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO rejectCashToBank(CashToBankAddRequestDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("rejectCashToBank method start=============>" + requestDTO.getAmountTransferId());

			AmountTransfer amountTransfer = amountTransferRepository.findOne(requestDTO.getAmountTransferId());

			AmountTransferLog amountTransferLog = new AmountTransferLog();
			amountTransferLog.setAmountTransfer(amountTransfer);
			amountTransferLog.setStage(requestDTO.getStage());
			amountTransferLog.setRemarks(requestDTO.getRejectRemark());
			amountTransferLogRepository.save(amountTransferLog);

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("rejectCashToBank  log inserted------");
		} catch (Exception e) {
			log.error("rejectCashToBank method exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	@Autowired
	ResponseWrapper responseWrapper;

	public BaseDTO getEmployeeListByEnityId(Long id) {
		log.info("<======Starts SocietyFieldVerificationService.getEmployeeListByEnityId=======>");
		BaseDTO baseDTO = new BaseDTO();
		List<EmployeeMaster> employeeMasterList = new ArrayList<EmployeeMaster>();
		try {
			List<Object[]> employeeMasterObjList = employeeMasterRepository.getEmployeesByWorkLocationId(id);

			employeeMasterObjList.forEach(empMaster -> {
				EmployeeMaster employeeMaster = new EmployeeMaster();
				employeeMaster.setId(new Long(empMaster[0].toString()));
				employeeMaster.setEmpCode((String) empMaster[1]);
				employeeMaster.setFirstName((String) empMaster[2]);
				employeeMaster.setLastName((String) empMaster[3]);
				employeeMaster.setMiddleName((String) empMaster[4]);

				EmployeePersonalInfoEmployment employeePersonalInfoEmployment = new EmployeePersonalInfoEmployment();
				employeePersonalInfoEmployment.setPfNumber((String) empMaster[5]);
				Designation designation = new Designation();
				designation.setId(new Long(empMaster[6].toString()));
				designation.setShortName((String) empMaster[7]);
				employeePersonalInfoEmployment.setCurrentDesignation(designation);
				employeeMaster.setPersonalInfoEmployment(employeePersonalInfoEmployment);
				employeeMaster.setEmpDisplayName((String) empMaster[8]);
				employeeMasterList.add(employeeMaster);
			});
			baseDTO.setResponseContent(employeeMasterList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("SocietyFieldVerificationService.getEmployeeListByEnityId RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("SocietyFieldVerificationService.getEmployeeListByEnityId Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<======Ends SocietyFieldVerificationService.getEmployeeListByEnityId=======>");
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO findEmployeeDetailsByLoggedInUser(Long userId) {
		log.info(" findEmployeeDetailsByLoggedInUser with user Id " + userId);
		BaseDTO baseDTO = new BaseDTO();
		try {

			EmployeeMaster empDetails = employeeMasterRepository.findEmployeeParticularDetailsByUserId(userId);
			if(empDetails == null) {
				empDetails = employeeMasterRepository.findEmployeeNameAndDesignationByUserId(userId);
			}
			if (empDetails != null) {
				log.info("Employee Details Exist for the User : " + userId);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				log.info("Employee Details Not Found for the User : " + userId);
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			}
			baseDTO.setResponseContent(empDetails);
		} catch (Exception exp) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Employee findEmployeeDetailsByLoggedInUser failed: :", exp);
		}
		return baseDTO;
	}
	
}