package in.gov.cooptex.finance.service;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.enums.VoucherStatus;
import in.gov.cooptex.core.accounts.enums.VoucherTypeDetails;
import in.gov.cooptex.core.accounts.model.StaffTelephonePayment;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.VoucherDetails;
import in.gov.cooptex.core.accounts.model.VoucherLog;
import in.gov.cooptex.core.accounts.model.VoucherNote;
import in.gov.cooptex.core.accounts.model.VoucherType;
import in.gov.cooptex.core.accounts.repository.GlAccountRepository;
import in.gov.cooptex.core.accounts.repository.StaffTelephonePaymentRepository;
import in.gov.cooptex.core.accounts.repository.VoucherDetailsRepository;
import in.gov.cooptex.core.accounts.repository.VoucherLogRepository;
import in.gov.cooptex.core.accounts.repository.VoucherNoteRepository;
import in.gov.cooptex.core.accounts.repository.VoucherRepository;
import in.gov.cooptex.core.accounts.repository.VoucherTypeRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.SequenceName;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.SequenceConfig;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.AppQueryRepository;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.PaymentModeRepository;
import in.gov.cooptex.core.repository.SequenceConfigRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.ApplicationConstants;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.finance.dto.StaffTelephonePaymentDTO;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class StaffTelephonePaymentService {

	@Autowired
	StaffTelephonePaymentRepository staffTelephonePaymentRepository;

	@Autowired
	ResponseWrapper responseWrapper;

	@Autowired
	EmployeeMasterRepository employeeMasterRepository;

	@Autowired
	SequenceConfigRepository sequenceConfigRepository;

	@Autowired
	VoucherTypeRepository voucherTypeRepository;

	@Autowired
	LoginService loginService;

	@Autowired
	AppQueryRepository appQueryRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	VoucherNoteRepository voucherNoteRepository;

	@Autowired
	VoucherLogRepository voucherLogRepository;

	@Autowired
	VoucherRepository voucherRepository;

	@Autowired
	UserMasterRepository userMasterRepository;

	@Autowired
	PaymentModeRepository paymentModeRepository;

	@Autowired
	VoucherDetailsRepository voucherDetailsRepository;

	@Autowired
	GlAccountRepository glAccountRepository;
	
	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	public BaseDTO getById(Long id) {
		log.info("StaffTelephonePaymentService getById method started [" + id + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			StaffTelephonePaymentDTO staffTelephonePaymentDTO = new StaffTelephonePaymentDTO();
			if (id != null) {
				StaffTelephonePayment staffTelephonePayment = staffTelephonePaymentRepository.getOne(id);
				log.info("staffTelephonePayment value------------" + staffTelephonePayment);

				EmployeeMaster employeeMaster = employeeMasterRepository
						.getOne(staffTelephonePayment.getEmpMaster().getId());
				log.info("employee value-----------------" + employeeMaster);

				Voucher voucher = voucherRepository.getOne(staffTelephonePayment.getVoucher().getId());
				log.info("voucher value----------------->" + voucher.getId());
				log.info("Payment mode----------------->" + voucher.getPaymentMode().getPaymentMode());

				VoucherLog voucherLog = voucherLogRepository.findByVoucherId(voucher.getId());
				VoucherNote voucherNote = voucherNoteRepository.findByVoucherId(voucher.getId());

				staffTelephonePaymentDTO.setVoucherLog(voucherLog);
				staffTelephonePaymentDTO.setVoucherNote(voucherNote);
				staffTelephonePaymentDTO.setUserMaster(voucherNote.getForwardTo());
				staffTelephonePaymentDTO.setPaymentMode(voucher.getPaymentMode());
				staffTelephonePaymentDTO.setEmployeeMaster(employeeMaster);
				staffTelephonePaymentDTO.setStaffTelephonePayment(staffTelephonePayment);
				staffTelephonePaymentDTO.setEligibileAmount(
						employeeMaster.getPersonalInfoEmployment().getPhonebillReimbursementLimit());
				staffTelephonePaymentDTO.setUsedAmount(staffTelephonePayment.getAmount());
				staffTelephonePaymentDTO.setBalanceAmount(
						staffTelephonePaymentDTO.getEligibileAmount() - staffTelephonePaymentDTO.getUsedAmount());
				
				
				List<Map<String, Object>> employeeData = new ArrayList<Map<String, Object>>();
				if(staffTelephonePayment!=null) {
					log.info("staffTelephone Expense voucherid----------"+staffTelephonePayment.getVoucher().getId());
					 ApplicationQuery applicationQueryForlog = applicationQueryRepository.findByQueryName("VOUCHER_LOG_EMPLOYEE_DETAILS");
					 if (applicationQueryForlog == null || applicationQueryForlog.getId() == null) {
						 log.info("Application Query For Log Details not found for query name : " + applicationQueryForlog);
						 baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
						 return baseDTO;
					 }
					 String logquery = applicationQueryForlog.getQueryContent().trim();
					 logquery = logquery.replace(":voucherId", staffTelephonePayment.getVoucher().getId().toString());
					 log.info("Query STAFF_TELEPHONE_EXPENSE_LOG_EMPLOYEE_DETAILS log query : " + logquery);
					 employeeData = jdbcTemplate.queryForList(logquery);
					 log.info("STAFF_TELEPHONE_EXPENSE_LOG_EMPLOYEE_DETAILS Employee Data======>"+employeeData.size());
					 baseDTO.setTotalListOfData(employeeData);
				}

				baseDTO.setResponseContent(staffTelephonePaymentDTO);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (RestException restException) {
			log.error("StaffTelephonePaymentService getById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("StaffTelephonePaymentService getById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("StaffTelephonePaymentService getById method completed");
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO deleteById(Long id) {
		log.info("StaffTelephonePaymentService deleteById method started [" + id + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			// Validate.notNull(id, ErrorDescription.STAFF_TELEPHONE_PAYMENT_ID_EMPTY);
			staffTelephonePaymentRepository.delete(id);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("StaffTelephonePaymentService deleteById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (DataIntegrityViolationException exception) {
			log.error("StaffTelephonePaymentService deleteById DataIntegrityViolationException ", exception);
			if (exception.getCause().getCause() instanceof PSQLException) {
				baseDTO.setStatusCode(ErrorDescription.CANNOT_DELETE_REFERENCED_RECORD.getErrorCode());
			} else {
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			}
		} catch (Exception exception) {
			log.error("StaffTelephonePaymentService deleteById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("StaffTelephonePaymentService deleteById method completed");
		return responseWrapper.send(baseDTO);
	}

	@Transactional
	public BaseDTO createStaffTelephonePayment(StaffTelephonePaymentDTO staffTelephonePaymentDTO) {
		log.info("StaffTelephonePaymentService save method start-------");
		BaseDTO baseDTO = new BaseDTO();
		Date date = new Date();
		String referenceNumber = "";
		try {
			if (staffTelephonePaymentDTO.getStaffTelephonePayment().getId() == null) {
				log.info("save method inside if-------");

				SequenceConfig sequenceConfig = sequenceConfigRepository
						.findBySequenceName(SequenceName.STAFF_TELEPHONE_PAYMENT);
				if (sequenceConfig == null) {
					throw new RestException(ErrorDescription.SEQUENCE_CONFIG_NOT_EXIST);
				}
				referenceNumber = staffTelephonePaymentDTO.getEntityMaster().getCode() + sequenceConfig.getSeparator()
						+ sequenceConfig.getPrefix() + AppUtil.getCurrentMonthString() + AppUtil.getCurrentYearString();
				log.info("voucher reference number-----------" + referenceNumber);
				log.info("current value-------------" + sequenceConfig.getCurrentValue());

				Voucher voucher = new Voucher();
				voucher.setReferenceNumberPrefix(referenceNumber);
				voucher.setReferenceNumber(sequenceConfig.getCurrentValue());
				voucher.setName(VoucherTypeDetails.STAFF_TELEPHONE_PAYMENT.toString());
				VoucherType voucherType = voucherTypeRepository.findByName(VoucherTypeDetails.Payment.toString());
				log.info("voucherType Id===============>" + voucherType.getId());
				voucher.setVoucherType(voucherType);
				voucher.setNarration(VoucherTypeDetails.Payment.toString());
				voucher.setFromDate(date);
				voucher.setToDate(date);
				voucher.setNetAmount(staffTelephonePaymentDTO.getStaffTelephonePayment().getAmount());
				voucher.setPaymentMode(
						paymentModeRepository.findOne(staffTelephonePaymentDTO.getPaymentMode().getId()));
				log.info("------------Voucher table added-------------");

				VoucherDetails voucherDetails = new VoucherDetails();
				voucherDetails.setVoucher(voucher);
				voucherDetails.setAmount(staffTelephonePaymentDTO.getStaffTelephonePayment().getAmount());
				voucherDetails.setEmpMaster(
						employeeMasterRepository.findOne(staffTelephonePaymentDTO.getEmployeeMaster().getId()));
				voucherDetails.setGlAccount(glAccountRepository.findByCode(ApplicationConstants.TELEPHONE_CHARGES));
				voucher.getVoucherDetailsList().add(voucherDetails);
				log.info("-------------VoucherDetails table added---------------");

				VoucherNote voucherNote = new VoucherNote();
				voucherNote
						.setForwardTo(userMasterRepository.findOne(staffTelephonePaymentDTO.getUserMaster().getId()));
				log.info("voucherNote forwardTo user---------------->" + voucherNote.getForwardTo().getId());
				voucherNote.setFinalApproval(staffTelephonePaymentDTO.getVoucherNote().getFinalApproval());
				voucherNote.setNote(staffTelephonePaymentDTO.getVoucherNote().getNote());
				voucherNote.setVoucher(voucher);
				voucher.getVoucherNote().add(voucherNote);
				log.info("------------voucherNote table added----------------");

				VoucherLog voucherLog = new VoucherLog();
				voucherLog.setVoucher(voucher);
				voucherLog.setStatus(VoucherStatus.SUBMITTED);
				UserMaster userMaster = userMasterRepository.findOne(loginService.getCurrentUser().getId());
				log.info("VoucherLog userMaster----------->" + userMaster.getId());
				voucherLog.setUserMaster(userMaster);
				voucher.getVoucherLogList().add(voucherLog);
				log.info("-----------VoucherLog table added---------------");

				voucherRepository.save(voucher);
				log.info("Voucher datas inserted----------------" + voucher.getId());

				sequenceConfig.setCurrentValue(sequenceConfig.getCurrentValue() + 1);
				sequenceConfigRepository.save(sequenceConfig);
				log.info("sequenceConfig value updated-----" + sequenceConfig.getCurrentValue());

				StaffTelephonePayment staffTelephonePayment = new StaffTelephonePayment();
				staffTelephonePayment.setVoucher(voucher);
				staffTelephonePayment.setAmount(staffTelephonePaymentDTO.getStaffTelephonePayment().getAmount());
				staffTelephonePayment.setActiveStatus(true);
				staffTelephonePayment.setEmpMaster(
						employeeMasterRepository.findOne(staffTelephonePaymentDTO.getEmployeeMaster().getId()));
				log.info("employee id--------------" + staffTelephonePayment.getEmpMaster().getId());
				staffTelephonePayment.setMonth(staffTelephonePaymentDTO.getStaffTelephonePayment().getMonth());
				staffTelephonePayment.setYear(staffTelephonePaymentDTO.getStaffTelephonePayment().getYear());
				staffTelephonePaymentRepository.save(staffTelephonePayment);
				log.info("---------staffTelephonePayment inserted-------");

			} else {
				log.info("save method inside else-------");
				StaffTelephonePayment staffTelephonePayment = updateStaffTelephonePayment(staffTelephonePaymentDTO);
				staffTelephonePaymentRepository.save(staffTelephonePayment);
				log.info("---------staffTelephonePayment updated-------");
			}
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.info("StaffTelephonePaymentService save method exception----", e);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		}
		log.info("StaffTelephonePaymentService save method end-------");
		return baseDTO;
	}

	private StaffTelephonePayment updateStaffTelephonePayment(StaffTelephonePaymentDTO staffTelephonePaymentDTO)
			throws Exception {
		log.info("updateStaffTelephonePayment method start---------");
		StaffTelephonePayment staffTelephonePayment = staffTelephonePaymentRepository
				.findOne(staffTelephonePaymentDTO.getStaffTelephonePayment().getId());
		staffTelephonePayment.setAmount(staffTelephonePaymentDTO.getStaffTelephonePayment().getAmount());
		staffTelephonePayment
				.setEmpMaster(employeeMasterRepository.findOne(staffTelephonePaymentDTO.getEmployeeMaster().getId()));
		log.info("employee id--------------" + staffTelephonePayment.getEmpMaster().getId());

		Voucher voucher = voucherRepository.findOne(staffTelephonePayment.getVoucher().getId());
		voucher.setNetAmount(staffTelephonePaymentDTO.getStaffTelephonePayment().getAmount());
		voucher.setPaymentMode(paymentModeRepository.findOne(staffTelephonePaymentDTO.getPaymentMode().getId()));
		voucherRepository.save(voucher);
		log.info("voucher id------------" + voucher.getId());

		VoucherDetails voucherDetails = voucherDetailsRepository.getVoucherDetailsByVoucherID(voucher.getId());
		voucherDetails.setAmount(staffTelephonePaymentDTO.getStaffTelephonePayment().getAmount());
		voucherDetails
				.setEmpMaster(employeeMasterRepository.findOne(staffTelephonePaymentDTO.getEmployeeMaster().getId()));
		voucherDetails.setGlAccount(glAccountRepository.findByCode(ApplicationConstants.TELEPHONE_CHARGES));
		voucherDetailsRepository.save(voucherDetails);
		log.info("voucher details--------" + voucherDetails.getId());

		VoucherNote voucherNote = new VoucherNote();
		voucherNote.setVoucher(voucher);
		voucherNote.setForwardTo(userMasterRepository.findOne(staffTelephonePaymentDTO.getUserMaster().getId()));
		log.info("voucherNote forwardTo user---------------->" + voucherNote.getForwardTo().getId());
		voucherNote.setFinalApproval(staffTelephonePaymentDTO.getVoucherNote().getFinalApproval());
		voucherNote.setNote(staffTelephonePaymentDTO.getVoucherNote().getNote());
		voucherNoteRepository.save(voucherNote);
		log.info("------------voucherNote table Inserted----------------");

		VoucherLog voucherLog = new VoucherLog();
		voucherLog.setVoucher(voucher);
		voucherLog.setStatus(VoucherStatus.SUBMITTED);
		UserMaster userMaster = userMasterRepository.findOne(loginService.getCurrentUser().getId());
		log.info("VoucherLog userMaster----------->" + userMaster.getId());
		voucherLog.setUserMaster(userMaster);
		voucherLogRepository.save(voucherLog);
		log.info("-----------VoucherLog table Inserted---------------");

		return staffTelephonePayment;
	}

	public BaseDTO getActiveStaffTelephonePayment() {
		log.info("StaffTelephonePaymentService:getActiveStaffTelephonePayment()");
		BaseDTO baseDTO = new BaseDTO();
		try {

			List<StaffTelephonePayment> staffTelephonePaymentList = staffTelephonePaymentRepository
					.getAllActiveStaffTelephonePayment();
			baseDTO.setResponseContent(staffTelephonePaymentList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {

			log.error("StaffTelephonePaymentService getActiveStaffTelephonePaymentRestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {

			log.error("StaffTelephonePaymentService getActiveStaffTelephonePaymentException ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}

		log.info("StaffTelephonePaymentService getActiveStaffTelephonePaymentmethod completed");
		return baseDTO;

	}

	public BaseDTO searchStaffPayment(StaffTelephonePaymentDTO staffTelephonePaymentDTO) {
		log.info("StaffTelephonePaymentService searchStaffPayment start");
		BaseDTO baseDTO = null;
		String month = "";
		Long year = null;
		Long employeeId = null;
		Double amount = 0.0;
		try {
			baseDTO = new BaseDTO();
			if (staffTelephonePaymentDTO != null) {
				month = staffTelephonePaymentDTO.getStaffTelephonePayment().getMonth();
				year = staffTelephonePaymentDTO.getStaffTelephonePayment().getYear();
				employeeId = staffTelephonePaymentDTO.getEmployeeMaster().getId();

				List<StaffTelephonePayment> staffTelephonePaymentList = staffTelephonePaymentRepository
						.findByEmployee(month, year, employeeId);
				log.info("staffTelephonePayment value-----" + staffTelephonePaymentList.size());

				if (staffTelephonePaymentList.size() > 0) {
					for (StaffTelephonePayment staff : staffTelephonePaymentList) {
						amount = amount + staff.getAmount();
					}
				}
				log.info("Staff already paid amount-----" + amount);

				EmployeeMaster employeeMaster = employeeMasterRepository.getOne(employeeId);
				log.info("employee value-----------------" + employeeMaster);

				staffTelephonePaymentDTO.setEmployeeMaster(employeeMaster);
				staffTelephonePaymentDTO.setEligibileAmount(
						employeeMaster.getPersonalInfoEmployment().getPhonebillReimbursementLimit());
				staffTelephonePaymentDTO.setUsedAmount(amount);
				baseDTO.setResponseContent(staffTelephonePaymentDTO);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (Exception e) {
			log.error("StaffTelephonePaymentService searchStaffPayment exception--------", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("StaffTelephonePaymentService searchStaffPayment completed");
		return baseDTO;
	}

	public BaseDTO lazyLoadStaffTelephone(PaginationDTO paginationDTO) {
		log.info("------------lazyLoadStaffTelephone service method starts---------------");
		BaseDTO baseDTO = new BaseDTO();
		List<StaffTelephonePayment> resultList = null;
		Integer totalRecords = 0;
		try {
			resultList = new ArrayList<StaffTelephonePayment>();
			Integer start = paginationDTO.getFirst(), pageSize = paginationDTO.getPageSize();
			start = start * pageSize;
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();

			ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("STAFF_TELEPHONE_EXPENSE_DETAILS");
			String mainQuery = applicationQuery.getQueryContent();
			log.info("db query----------" + mainQuery);

			if (paginationDTO.getFilters() != null) {
				if (paginationDTO.getFilters().get("month") != null) {
					mainQuery += " and upper(month) like upper('%" + paginationDTO.getFilters().get("month") + "%')";
				}
				if (paginationDTO.getFilters().get("year") != null) {
					mainQuery += " and year='" + paginationDTO.getFilters().get("year") + "'";
				}
				if (paginationDTO.getFilters().get("referenceNumber") != null) {
					mainQuery += " and upper(v.reference_number_prefix) like upper('%"
							+ paginationDTO.getFilters().get("referenceNumber") + "%')";
				}
				if (paginationDTO.getFilters().get("employeeName") != null) {
					mainQuery += " and upper(emp.first_name) like upper('%"
							+ paginationDTO.getFilters().get("employeeName") + "%')";
				}
				if (paginationDTO.getFilters().get("amount") != null) {
					mainQuery += " and amount='" + paginationDTO.getFilters().get("amount") + "'";
				}
				if (paginationDTO.getFilters().get("paymentMode") != null) {
					mainQuery += " and pm.payment_mode='" + paginationDTO.getFilters().get("paymentMode") + "'";
				}
				if (paginationDTO.getFilters().get("status") != null) {
					mainQuery += " and vl.status='" + paginationDTO.getFilters().get("status") + "'";
				}
			}

			String countQuery = mainQuery.replace(
					"select stp.id,v.id as voucher_id,concat(v.reference_number_prefix,v.reference_number) as reference_number,concat(emp.first_name,' ',emp.last_name ) as emp_name,month,year,amount,pm.payment_mode,vl.status",
					"select count(distinct(stp.id)) as count ");
			log.info("count query... " + countQuery);
			countData = jdbcTemplate.queryForList(countQuery);

			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					totalRecords = Integer.parseInt(data.get("count").toString().replace(",", ""));
			}
			if (paginationDTO.getSortField() == null)
				mainQuery += " order by stp.id desc limit " + pageSize + " offset " + start + ";";

			if (paginationDTO.getSortField() != null && paginationDTO.getSortOrder() != null) {
				log.info("Sort Field:[" + paginationDTO.getSortField() + "] Sort Order:[" + paginationDTO.getSortOrder()
						+ "]");
				if (paginationDTO.getSortField().equals("month") && paginationDTO.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by amount asc  ";
				}
				if (paginationDTO.getSortField().equals("month") && paginationDTO.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by amount desc  ";
				}
				if (paginationDTO.getSortField().equals("year") && paginationDTO.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by year asc ";
				}
				if (paginationDTO.getSortField().equals("year") && paginationDTO.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by year desc ";
				}
				if (paginationDTO.getSortField().equals("referenceNumber")
						&& paginationDTO.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by v.reference_number_prefix asc ";
				}
				if (paginationDTO.getSortField().equals("referenceNumber")
						&& paginationDTO.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by v.reference_number_prefix desc ";
				}
				if (paginationDTO.getSortField().equals("employeeName")
						&& paginationDTO.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by emp.first_name asc  ";
				}
				if (paginationDTO.getSortField().equals("employeeName")
						&& paginationDTO.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by emp.first_name desc  ";
				}
				if (paginationDTO.getSortField().equals("amount") && paginationDTO.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by amount asc  ";
				}
				if (paginationDTO.getSortField().equals("amount")
						&& paginationDTO.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by amount desc  ";
				}
				if (paginationDTO.getSortField().equals("paymentMode")
						&& paginationDTO.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by pm.payment_mode asc  ";
				}
				if (paginationDTO.getSortField().equals("paymentMode")
						&& paginationDTO.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by pm.payment_mode desc  ";
				}
				if (paginationDTO.getSortField().equals("status") && paginationDTO.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by vl.status asc ";
				}
				if (paginationDTO.getSortField().equals("status")
						&& paginationDTO.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by vl.status desc ";
				}
				mainQuery += " limit " + pageSize + " offset " + start + ";";
			}

			log.info("filter query----------" + mainQuery);

			listofData = jdbcTemplate.queryForList(mainQuery);
			log.info("Staff telephone expense list size-------------->" + listofData.size());

			for (Map<String, Object> data : listofData) {
				StaffTelephonePayment payment = new StaffTelephonePayment();
				payment.setId(Long.valueOf(data.get("id").toString()));
				payment.setMonth(data.get("month").toString());
				payment.setYear(Long.valueOf(data.get("year").toString()));
				payment.setAmount(Double.valueOf(data.get("amount").toString()));
				payment.setEmployeeName(data.get("emp_name").toString());
				payment.setPaymentMode(data.get("payment_mode").toString());
				payment.setReferenceNumber(data.get("reference_number").toString());
				payment.setStatus(data.get("status").toString());
				resultList.add(payment);
			}

			log.info("final list size------" + resultList.size());
			baseDTO.setResponseContents(resultList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			baseDTO.setTotalRecords(totalRecords);
		} catch (Exception e) {
			log.error("lazyLoadStaffTelephone Inside service Exception ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("-------------lazyLoadStaffTelephone service method Ends---------------");
		return baseDTO;
	}

	public BaseDTO approveStaffPayment(StaffTelephonePaymentDTO staffTelephonePaymentDTO) {
		BaseDTO baseDTO = new BaseDTO();
		log.info("-----approveStaffPayment method start-------");
		try {
			if (staffTelephonePaymentDTO.getStaffTelephonePayment().getId() != null) {
				StaffTelephonePayment staffTelephonePayment = staffTelephonePaymentRepository
						.getOne(staffTelephonePaymentDTO.getStaffTelephonePayment().getId());
				log.info("staffTelephonePayment value------------" + staffTelephonePayment);

				Voucher voucher = voucherRepository.getOne(staffTelephonePayment.getVoucher().getId());
				log.info("voucher value----------------->" + voucher.getId());

				VoucherNote voucherNote = new VoucherNote();
				voucherNote
						.setForwardTo(userMasterRepository.findOne(staffTelephonePaymentDTO.getUserMaster().getId()));
				log.info("voucherNote forwardTo user---------------->" + voucherNote.getForwardTo().getId());
				voucherNote.setFinalApproval(staffTelephonePaymentDTO.getVoucherNote().getFinalApproval());
				voucherNote.setNote(staffTelephonePaymentDTO.getVoucherNote().getNote());
				voucherNote.setVoucher(voucher);
				voucherNoteRepository.save(voucherNote);
				log.info("------------voucherNote table Inserted----------------");

				VoucherLog voucherLog = new VoucherLog();
				voucherLog.setRemarks(staffTelephonePaymentDTO.getVoucherLog().getRemarks());
				voucherLog.setVoucher(voucher);
				voucherLog.setStatus(staffTelephonePaymentDTO.getVoucherLog().getStatus());
				voucherLog
						.setUserMaster(userMasterRepository.findOne(staffTelephonePaymentDTO.getUserMaster().getId()));
				voucherLogRepository.save(voucherLog);
				log.info("------------voucherLog table Inserted----------------");

				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (Exception e) {
			log.error("approveStaffPayment exception ------", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("-----approveStaffPayment method end-------");
		return baseDTO;
	}

	public BaseDTO rejectStaffPayment(StaffTelephonePaymentDTO staffTelephonePaymentDTO) {
		BaseDTO baseDTO = new BaseDTO();
		log.info("-----rejectStaffPayment method start-------");
		try {
			if (staffTelephonePaymentDTO.getStaffTelephonePayment().getId() != null) {
				StaffTelephonePayment staffTelephonePayment = staffTelephonePaymentRepository
						.getOne(staffTelephonePaymentDTO.getStaffTelephonePayment().getId());
				log.info("staffTelephonePayment value------------" + staffTelephonePayment);

				Voucher voucher = voucherRepository.getOne(staffTelephonePayment.getVoucher().getId());
				log.info("voucher value----------------->" + voucher.getId());

				VoucherLog voucherLog = new VoucherLog();
				voucherLog.setRemarks(staffTelephonePaymentDTO.getVoucherLog().getRemarks());
				voucherLog.setVoucher(voucher);
				voucherLog.setStatus(staffTelephonePaymentDTO.getVoucherLog().getStatus());
				voucherLog
						.setUserMaster(userMasterRepository.findOne(staffTelephonePaymentDTO.getUserMaster().getId()));
				voucherLogRepository.save(voucherLog);
				log.info("------------voucherLog table Inserted----------------");

				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (Exception e) {
			log.error("rejectStaffPayment exception ------", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("-----rejectStaffPayment method end-------");
		return baseDTO;
	}

}