package in.gov.cooptex.finance.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.finance.repository.PPTypeMasterRepository;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.model.PPTypeMaster;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class PPTypeMasterService {
	
	@Autowired
	PPTypeMasterRepository pPTypeMasterRepository;
	
	public BaseDTO getAllTypes() {
		log.info("<<====== PPTypeMasterService ----  getAllTypes  ====## STARTS");
		BaseDTO baseDTO = new BaseDTO(); 
		try {
			List<PPTypeMaster> list = pPTypeMasterRepository.findAll();
			baseDTO.setResponseContents(list);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		}catch(Exception e) {
			log.error("<<=======  ERROR ::",e); 
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			}
		log.info("<<====== PPTypeMasterService ----  getAllTypes  ====## ENDS");
		return baseDTO;
	}

}
