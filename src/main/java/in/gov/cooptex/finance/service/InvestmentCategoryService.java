package in.gov.cooptex.finance.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.model.InvestmentCategoryMaster;
import in.gov.cooptex.core.accounts.repository.InvestmentCategoryRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class InvestmentCategoryService {

	@Autowired
	InvestmentCategoryRepository investmentCategoryRepository;

	@Autowired
	ResponseWrapper responseWrapper;

	public BaseDTO getById(Long id) {
		log.info("InvestmentCategoryService  getById method started [" + id + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			// Validate.notNull(id, ErrorDescription.INVESTMENT_CLOSING_ID_EMPTY);
			InvestmentCategoryMaster investmentCategory= investmentCategoryRepository.getOne(id);
			baseDTO.setResponseContent(investmentCategory);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("InvestmentCategoryService getById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("InvestmentCategoryService getById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("InvestmentCategoryService  getById method completed");
		return responseWrapper.send(baseDTO);
	}
	
	public BaseDTO getInvestmentCategoryList() {
		log.info("InvestmentCategoryService  getInvestmentCategory method started ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<InvestmentCategoryMaster> investmentCategoryList= investmentCategoryRepository.getAllActiveInvestmentCategoryMaster();
			baseDTO.setResponseContent(investmentCategoryList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("InvestmentCategoryService getInvestmentCategory RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("InvestmentCategoryService getInvestmentCategory Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("InvestmentCategoryService  getInvestmentCategory method completed");
		return responseWrapper.send(baseDTO);
	}

}