package in.gov.cooptex.finance.service;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.service.ApproveRejectCommentsCommonService;
import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.service.NotificationEmailService;
import in.gov.cooptex.core.accounts.model.ProfitAllocation;
import in.gov.cooptex.core.accounts.model.ProfitAllocationDetails;
import in.gov.cooptex.core.accounts.model.ProfitAllocationLog;
import in.gov.cooptex.core.accounts.model.ProfitAllocationNote;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.YearProfitLoss;
import in.gov.cooptex.core.accounts.repository.GlAccountRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.finance.repository.FinancialYearRepository;
import in.gov.cooptex.core.finance.repository.ProfitAllocationDetailsRepository;
import in.gov.cooptex.core.finance.repository.ProfitAllocationLogRepository;
import in.gov.cooptex.core.finance.repository.ProfitAllocationNoteRepository;
import in.gov.cooptex.core.finance.repository.ProfitAllocationRepository;
import in.gov.cooptex.core.finance.repository.YearProfitLossRepository;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.Community;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.FinancialYear;
import in.gov.cooptex.core.model.GenderMaster;
import in.gov.cooptex.core.model.JobApplication;
import in.gov.cooptex.core.model.JobApplicationAdditionalInfo;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.dto.ProfitAllocationViewDTO;
import in.gov.cooptex.finance.dto.SupplierPaymentViewDTO;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class ProfitAllocationService {
	
	@PersistenceContext
	EntityManager entityManager;
	
	@Autowired
	FinancialYearRepository financialYearRepository;
	
	@Autowired
	YearProfitLossRepository yearProfitLossRepository;
	
	@Autowired
	ProfitAllocationRepository profitAllocationRepository;
	
	@Autowired
	ProfitAllocationDetailsRepository profitAllocationDetailsRepository;
	
	@Autowired
	ProfitAllocationLogRepository profitAllocationLogRepository;
	
	@Autowired
	ProfitAllocationNoteRepository profitAllocationNoteRepository;
	
	@Autowired
	UserMasterRepository userMasterRepository;
	
	@Autowired
	GlAccountRepository glAccountRepository;
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	ApplicationQueryRepository applicationQueryRepository;
	
	public BaseDTO getFinancialYear() {
		log.info("ProfitAllocationService getFinancialYear starts-------");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<FinancialYear> financialYearList = financialYearRepository.findAll();
			log.info("ProfitAllocationService financialYearList--------"+financialYearList.size());
			baseDTO.setResponseContents(financialYearList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		}catch (Exception e) {
			log.error("ProfitAllocationService getFinancialYear exception----",e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("ProfitAllocationService getFinancialYear ends-------");
		return baseDTO;
	}

	public BaseDTO getNetAmountByFinancialId(Long financialYearId) {
		log.info("ProfitAllocationService getNetAmountByFinancialId starts-----");
		BaseDTO baseDTO = new BaseDTO();
		Double amount=0D;
		try {
			if(financialYearId!=null) {
//				YearProfitLoss yearProfitLoss = yearProfitLossRepository.getAmountByFinancialId(financialYearId);
				amount = yearProfitLossRepository.getAmountByFinId(financialYearId);
				/*if(yearProfitLoss!=null) {
				log.info("getNetAmountByFinancialId yearProfitLoss----------"+yearProfitLoss.getAmount());
				baseDTO.setResponseContent(yearProfitLoss.getAmount());
				}else {
				log.info("getNetAmountByFinancialId yearProfitLoss Null");
				baseDTO.setResponseContent(0D);
				}*/
				
				if (amount != null) {
					log.info("getAmountByFinId yearProfitLoss----------" + amount);
					baseDTO.setResponseContent(amount);
				} else {
					log.info("getAmountByFinId yearProfitLoss Null");
					baseDTO.setResponseContent(0D);
				}
				
//				baseDTO.setResponseContent(yearProfitLoss!=null?yearProfitLoss.getAmount():0D);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		}catch (Exception e) {
			log.error("ProfitAllocationService getNetAmountByFinancialId exception--",e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	@Autowired
	NotificationEmailService notificationEmailService;
	
	private static final String PROFIT_ALLOCATION_VIEW_PAGE = "/pages/accounts/profitAllocation/viewProfitAllocation.xhtml?faces-redirect=true";

	public BaseDTO saveProfitAllocation(ProfitAllocation profitAllocation) {
		log.info("ProfitAllocationService saveProfitAllocation starts-----");
		BaseDTO baseDTO = new BaseDTO();
		ProfitAllocationNote proNote = new ProfitAllocationNote();
		try {
			validateProfitAllocation(profitAllocation);
			if(profitAllocation.getFinYear() != null || profitAllocation.getFinYear().getId() != null) {
				profitAllocation.setFinYear(financialYearRepository.findOne(profitAllocation.getFinYear().getId()));
			}
			//--to save profit allocation details--
			if(profitAllocation.getAllocationDetailsList() != null) {
				for(ProfitAllocationDetails allDetails : profitAllocation.getAllocationDetailsList()) {
					allDetails.setGlAccount(glAccountRepository.findOne(allDetails.getGlAccount().getId()));
				}
			}
			//--to save profit allocation note--
			if(profitAllocation.getProfitAllocationNote() != null) {
				proNote = profitAllocation.getProfitAllocationNote();
				log.info("forward Id"+proNote.getUserMaster().getId());
				profitAllocation.getProfitAllocationNote().setUserMaster(userMasterRepository.findOne(proNote.getUserMaster().getId()));
			}
			ProfitAllocation allocation = profitAllocationRepository.save(profitAllocation);
			if (allocation != null && proNote != null) {
				

				Map<String, Object> additionalData = new HashMap<>();
				additionalData.put("Url",
						PROFIT_ALLOCATION_VIEW_PAGE + "&profitAllocationId=" + allocation.getId() +"&");
				notificationEmailService.sendMailAndNotificationForForward(proNote, profitAllocation.getProfitAllocationLogList().get(0), additionalData);
			
				
				log.info("< === save Profit Allocation successfully === >");
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				baseDTO.setResponseContent(allocation);
			}
		}catch (Exception e) {
			log.error("ProfitAllocationService saveProfitAllocation exception--",e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
		
	}
	
	private void validateProfitAllocation(ProfitAllocation profitAllocation) {
		log.info("<=call validateProfitAllocation=>");
		try {
			Validate.notNull(profitAllocation.getFinYear(), ErrorDescription.FINANCAL_YEAR_REQUIRED);
			Validate.notNull(profitAllocation.getRemarks(), ErrorDescription.LOG_REMARKS_EMPTY);
			for(ProfitAllocationDetails allDetails : profitAllocation.getAllocationDetailsList()) {
				Validate.notNull(allDetails.getGlAccount(), ErrorDescription.GLACCOUNT_EMPTY);
			}
			Validate.notNull(profitAllocation.getProfitAllocationNote().getUserMaster(), ErrorDescription.PLEASE_SELECT_FORWARD_TO);
			Validate.notNull(profitAllocation.getProfitAllocationNote().getFinalApproval(), ErrorDescription.PLEASE_SELECT_FORWARD_FOR);
			Validate.notNull(profitAllocation.getProfitAllocationNote().getNote(), ErrorDescription.FILE_MOVEMENT_NOTE_EMPTY);
		}catch (Exception e) {
			log.error("ProfitAllocationService saveProfitAllocation exception--",e);
		}
	}
	
	public BaseDTO updateProfitallocation(ProfitAllocation pAllocation) {
		log.info("ProfitAllocationService updateProfitAllocation starts-----");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<ProfitAllocationDetails> proDetailsList = new ArrayList<>();
			List<ProfitAllocationLog> proLogList = new ArrayList<>();
			ProfitAllocation profitAllocation = null;
			if (pAllocation.getId() != null) {
				log.info("pro id==>" + pAllocation.getId());
				profitAllocation = profitAllocationRepository.findOne(pAllocation.getId());

			} else {
				profitAllocation = new ProfitAllocation();
			}

			// ProfitAllocation pAllocation =
			// profitAllocationRepository.findOne(profitAllocation.getId());

			profitAllocation.setRemarks(pAllocation.getRemarks());
			if (profitAllocation.getFinYear() != null || profitAllocation.getFinYear().getId() != null) {
				profitAllocation.setFinYear(financialYearRepository.findOne(profitAllocation.getFinYear().getId()));
			}
			// --to update profit allocation details--
			if (pAllocation.getAllocationDetailsList() != null) {

				log.info("size==>" + pAllocation.getAllocationDetailsList().size());
				// for(int s=0;s<pAllocation.getAllocationDetailsList().size();s++) {
				for (ProfitAllocationDetails profitAllocationDetailsObj : pAllocation.getAllocationDetailsList()) {
					ProfitAllocationDetails pDetails = null;
					if (profitAllocationDetailsObj.getId() != null) {
						log.info("pro details id==>" + profitAllocationDetailsObj.getId());
						pDetails = profitAllocationDetailsRepository.findOne(profitAllocationDetailsObj.getId());
						// profitAllocation.getAllocationDetailsList().clear();
					} else {
						pDetails = new ProfitAllocationDetails();
						pDetails = profitAllocationDetailsObj;
					}
					pDetails.setProfitAllocation(profitAllocation);
					pDetails.setGlAccount(glAccountRepository.findOne(pDetails.getGlAccount().getId()));
					proDetailsList.add(pDetails);
				}
			}

			profitAllocation.getAllocationDetailsList().clear();
			profitAllocation.getAllocationDetailsList().addAll(proDetailsList);
			// profitAllocation.setAllocationDetailsList(proDetailsList);

			/*
			 * profitAllocation.getAllocationDetailsList().clear();
			 * profitAllocation.getAllocationDetailsList().addAll(proDetailsList);
			 */

			// --to update profit allocation log--
			/*
			 * if(profitAllocation.getProfitAllocationLogList() != null) { for(int
			 * l=0;l<profitAllocation.getProfitAllocationLogList().size();l++) {
			 * ProfitAllocationLog prolog = null;
			 * if(profitAllocation.getProfitAllocationLogList().get(l).getId() != null) {
			 * log.info("pro log id==>"+profitAllocation.getProfitAllocationLogList().get(l)
			 * .getId()); prolog = profitAllocationLogRepository.findOne(profitAllocation.
			 * getProfitAllocationLogList().get(l).getId());
			 * //profitAllocation.getProfitAllocationLogList().clear(); }else { prolog = new
			 * ProfitAllocationLog(); } prolog.setProfitAllocation(profitAllocation);
			 * proLogList.add(prolog); } }
			 */

			if (profitAllocation.getProfitAllocationLogList() != null
					&& !profitAllocation.getProfitAllocationLogList().isEmpty()) {
				ProfitAllocationLog profitAllocationLog = new ProfitAllocationLog();
				profitAllocationLog.setProfitAllocation(profitAllocation);
				profitAllocationLog.setStage(profitAllocation.getProfitAllocationLogList().get(0).getStage());
				profitAllocationLogRepository.save(profitAllocationLog);
				log.info("profit allocation log save-------");
			}
			// profitAllocation.setProfitAllocationLogList(proLogList);

			/*
			 * //--to save profit allocation note-- ProfitAllocationNote proNote = null;
			 * if(profitAllocation.getProfitAllocationNote() != null &&
			 * profitAllocation.getProfitAllocationNote().getId() != null) {
			 * log.info("pro not id==>"+profitAllocation.getProfitAllocationNote().getId());
			 * proNote = profitAllocationNoteRepository.findOne(profitAllocation.
			 * getProfitAllocationNote().getId());
			 * 
			 * }else { proNote = new ProfitAllocationNote(); }
			 * proNote.setProfitAllocation(profitAllocation);
			 * proNote.setUserMaster(userMasterRepository.findOne(proNote.getUserMaster().
			 * getId())); profitAllocation.setProfitAllocationNote(proNote);
			 */

			if (pAllocation.getProfitAllocationNote() != null
					&& pAllocation.getProfitAllocationNote().getFinalApproval() != null
					&& pAllocation.getProfitAllocationNote().getUserMaster() != null) {
				ProfitAllocationNote profitAllocationNote = new ProfitAllocationNote();
				profitAllocationNote.setFinalApproval(pAllocation.getProfitAllocationNote().getFinalApproval());
				profitAllocationNote.setProfitAllocation(profitAllocation);
				profitAllocationNote.setUserMaster(userMasterRepository
						.findOne(pAllocation.getProfitAllocationNote().getUserMaster().getId()));
				profitAllocationNote.setNote(pAllocation.getProfitAllocationNote().getNote());
				profitAllocationNoteRepository.save(profitAllocationNote);
				log.info("profit allocation note save-------");
			}
			profitAllocation.setProfitAllocationLogList(null);
			profitAllocation.setProfitAllocationNote(null);

			ProfitAllocation allocation = profitAllocationRepository.save(profitAllocation);
			if (allocation != null) {

				log.info("< === save Profit Allocation successfully === >");
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				baseDTO.setResponseContent(allocation);
			}
		} catch (Exception e) {
			log.error("ProfitAllocationService saveProfitAllocation exception--", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;

	}
	
	
	
	public BaseDTO search(ProfitAllocation profitAllocation) {
		log.info(" getallDesignTargetlazy  called...");
		BaseDTO baseDTO = new BaseDTO();
		String mainQuery = "";
		String countQuery ="";
		ApplicationQuery applicationQuery = null;
		try{
			log.info("profitAllocation==>"+profitAllocation);
		 	Integer total=0;
		 	Integer start = profitAllocation.getPaginationDTO().getFirst(),pageSize = profitAllocation.getPaginationDTO().getPageSize();
			start=start*pageSize;
			
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();
			
			applicationQuery = applicationQueryRepository.findByQueryName("PROFIT_ALLOCATION");
			if(applicationQuery != null) {
				mainQuery = applicationQuery.getQueryContent().trim();
			}else {
				Validate.notNull(applicationQuery,ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getErrorCode());
			}
				
			if(profitAllocation.getPaginationDTO().getFilters().get("financalYear")!=null) {
				mainQuery += " and f.id="+profitAllocation.getPaginationDTO().getFilters().get("financalYear").toString()+"";
			}
			if(profitAllocation.getPaginationDTO().getFilters().get("createdDate")!=null) {
				Date createdDate=new Date((Long)profitAllocation.getPaginationDTO().getFilters().get("createdDate"));
				mainQuery += " and p.created_date::date = date '"+createdDate+"'";
			}
			if(profitAllocation.getPaginationDTO().getFilters().get("amount")!=null) {
				
				mainQuery += " and CAST(prad.amount as TEXT) LIKE '%"+profitAllocation.getPaginationDTO().getFilters().get("amount").toString()+"%' ";
			}
			if(profitAllocation.getPaginationDTO().getFilters().get("stage")!=null) {
				mainQuery += " and pal.stage = '"+profitAllocation.getPaginationDTO().getFilters().get("stage")+"'";
			}
			
			applicationQuery = applicationQueryRepository.findByQueryName("PROFIT_ALLOCATION_COUNT");
			if(applicationQuery != null) {
				countQuery = applicationQuery.getQueryContent().trim();
			}else {
				Validate.notNull(applicationQuery,ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getErrorCode());
			}
									
			countData=jdbcTemplate.queryForList(countQuery);
			for(Map<String,Object> data:countData){
				if(data.get("count")!=null)
					total=Integer.parseInt(data.get("count").toString().replace(",", ""));
			}
			
			if(profitAllocation.getPaginationDTO().getSortField()==null)
				mainQuery+="group by p.id,p.created_date,pal.stage,f.start_year,f.end_year,f.id order by allocationId desc limit "+pageSize+" offset "+start+";";
			
			if(profitAllocation.getPaginationDTO().getSortField()!=null && profitAllocation.getPaginationDTO().getSortOrder()!=null){

				if(profitAllocation.getPaginationDTO().getSortField().equals("financalYear") && profitAllocation.getPaginationDTO().getSortOrder().equals("ASCENDING"))
						mainQuery+=" order by f.id asc ";
				if(profitAllocation.getPaginationDTO().getSortField().equals("financalYear") && profitAllocation.getPaginationDTO().getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by f.id desc ";
				
				if(profitAllocation.getPaginationDTO().getSortField().equals("createdDate") && profitAllocation.getPaginationDTO().getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by p.created_date asc  ";
				if(profitAllocation.getPaginationDTO().getSortField().equals("createdDate") && profitAllocation.getPaginationDTO().getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by p.created_date desc  ";
				
				if(profitAllocation.getPaginationDTO().getSortField().equals("amount") && profitAllocation.getPaginationDTO().getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by y.amount asc  ";
				if(profitAllocation.getPaginationDTO().getSortField().equals("amount") && profitAllocation.getPaginationDTO().getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by y.amount desc  ";
		
				mainQuery+=" limit "+pageSize+" offset "+start+";";
			}

			List<ProfitAllocationViewDTO> allocationDTOList=new ArrayList<>();
			listofData = jdbcTemplate.queryForList(mainQuery); 
			for(Map<String,Object> data:listofData){
				ProfitAllocationViewDTO advancePaymentDTO=new ProfitAllocationViewDTO();
				if(data.get("allocationId")!=null) {
					advancePaymentDTO.setProfitAllocationId(Long.parseLong(data.get("allocationId").toString()));
				}
				if(data.get("finId")!=null) {
					advancePaymentDTO.setFinanceId(Long.parseLong(data.get("finId").toString()));
				}
				if(data.get("start_year")!=null || data.get("end_year")!=null) {
					advancePaymentDTO.setFinancalYear((String)data.get("start_year").toString()+"-"+
							(String)data.get("end_year").toString());
				}
				if(data.get("amount")!=null) {
					 	BigDecimal netAmountDecimal = (BigDecimal)data.get("amount");
					    Double netAmount = netAmountDecimal.doubleValue();
					advancePaymentDTO.setAmount(netAmount);
				}
				if(data.get("created_date")!=null) {
					advancePaymentDTO.setCreatedDate((Date)data.get("created_date"));
				}
				if(data.get("status")!=null) {
					advancePaymentDTO.setStage(data.get("status").toString());
				}
				allocationDTOList.add(advancePaymentDTO);
			} 
			baseDTO.setResponseContent(allocationDTOList);
			baseDTO.setTotalRecords(total);
			log.info("countQuery "+countQuery);
			log.info("mainQuery "+mainQuery);
			log.info("total "+total);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		}catch(Exception exp){
			log.error("Exception Cause  : " , exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO ;
	}
	
	public BaseDTO deleteById(Long id) {
		log.info(" Delete Profit Allocation by id " + id);
		BaseDTO baseDTO = new BaseDTO();
		try {
			 Validate.notNull(id, ErrorDescription.PROFIT_ALLOCATION_ID_NOT_EXIST);
			 profitAllocationRepository.delete(id);
			 log.info(" Profit Allocation deleted successfully by id" + id);
			 baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			
		} catch (Exception e) {
			log.error(" Exception Occured while deleting Profit Allocation==>",e);
			baseDTO.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("< === == Delete Profit Allocation End === == > ");
		return baseDTO;
	}

	@Autowired
	ApproveRejectCommentsCommonService approveRejectCommentsCommonService;

	public BaseDTO getProfitAllocation(Long id, Long notificationId) {
		log.info(" Get get Profit Allocation by id " + id);
		BaseDTO baseDTO = new BaseDTO();
		try {
			Validate.notNull(id, ErrorDescription.PROFIT_ALLOCATION_ID_NOT_EXIST);
			ProfitAllocation profitAllocation = profitAllocationRepository.findOne(id);
			
			if (id != null) {
				Object[] approveRejectCommentData = approveRejectCommentsCommonService
						.getApproveRejectComments("profit_allocation", "profit_allocation_log", "profit_allocation_id", id);
				baseDTO.setCommentAndLogList(approveRejectCommentData);
			}
			
			if (profitAllocation != null) {
				baseDTO.setResponseContent(profitAllocation);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
			if (notificationId != null) {
				String updateSQL = "UPDATE system_notification SET notification_read=true WHERE id=" + notificationId;
				jdbcTemplate.execute(updateSQL);
			}
		} catch (Exception e) {
			log.error(" Exception Occured while getProfitAllocation==>",e);
			baseDTO.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO deleteAllocationDetails(Long profitAllocationDetailsId) {
		log.info(" Delete Profit Allocation details by id " + profitAllocationDetailsId);
		BaseDTO baseDTO = new BaseDTO();
		try {
			 Validate.notNull(profitAllocationDetailsId, ErrorDescription.PROFIT_ALLOCATION_ID_NOT_EXIST);
			 String query = "DELETE FROM profit_allocation_details WHERE id ="+profitAllocationDetailsId;
			 jdbcTemplate.execute(query);
			 log.info(" Profit Allocation details deleted successfully by id" + profitAllocationDetailsId);
			 baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			
		} catch (Exception e) {
			log.error(" Exception Occured while deleting Profit Allocation==>",e);
			baseDTO.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("< === == Delete Profit Allocation End === == > ");
		return baseDTO;
	}

	@Autowired
	LoginService loginService;

	public BaseDTO approveProfitAllocation(ProfitAllocationViewDTO profitAllocationViewDTO) {
		BaseDTO baseDTO = null;
		try {
			baseDTO = new BaseDTO();
			if(profitAllocationViewDTO.getProfitAllocationId() !=null) {
				ProfitAllocation profitAllocation = profitAllocationRepository.findOne(profitAllocationViewDTO.getProfitAllocationId());
				log.info("ProfitAllocation id----------"+profitAllocation.getId());
				
				ProfitAllocationNote profitAllocationNote = new ProfitAllocationNote();
				profitAllocationNote.setFinalApproval(profitAllocationViewDTO.getFinalApproval());
				profitAllocationNote.setProfitAllocation(profitAllocation);
				profitAllocationNote.setUserMaster(userMasterRepository.findOne(profitAllocationViewDTO.getUserId()));
				profitAllocationNote.setNote(profitAllocationViewDTO.getNote());
				profitAllocationNoteRepository.save(profitAllocationNote);
				log.info("profit allocation note save-------");
				
				ProfitAllocationLog  profitAllocationLog = new ProfitAllocationLog();
				profitAllocationLog.setProfitAllocation(profitAllocation);
				profitAllocationLog.setStage(profitAllocationViewDTO.getStage());
				profitAllocationLog.setRemarks(profitAllocationViewDTO.getRemarks());
				profitAllocationLogRepository.save(profitAllocationLog);
				log.info("profit allocation log save-------");
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				
				UserMaster fromUser = loginService.getCurrentUser();
				String urlPath = PROFIT_ALLOCATION_VIEW_PAGE + "&profitAllocationId=" + profitAllocation.getId() +"&";
				if (profitAllocationViewDTO.getStage().equals(ApprovalStage.APPROVED.toString())) {
					Long toUserId = profitAllocationNote.getUserMaster() == null ? null
							: profitAllocationNote.getUserMaster().getId();
					notificationEmailService.saveIndividualNotification(fromUser, toUserId, urlPath,
							"Profit Allocation", "Profit Allocation Received");
				} else {
					List<Long> toUserIdList = new ArrayList<>();
					String query = "SELECT distinct(um.id) as userId FROM profit_allocation_note pan JOIN profit_allocation pa ON pan.profit_allocation_id=pa.id \n"
							+ "JOIN user_master um ON um.id=pan.forward_to WHERE pa.id=:profitAllocationId AND pan.forward_to<>:currentUserId";
					query = query.replace(":profitAllocationId", String.valueOf(profitAllocation.getId()));
					query = query.replace(":currentUserId", String.valueOf(fromUser.getId()));
					List<Map<String, Object>> mapList = jdbcTemplate.queryForList(query);
					int mapListSize = mapList == null ? 0 : mapList.size();
					if (mapListSize > 0) {
						for (Map<String, Object> dataMap : mapList) {
							toUserIdList.add(dataMap.get("userId") == null ? null
									: Long.valueOf(dataMap.get("userId").toString()));
						}
					}
					Long createdUserId = profitAllocation.getCreatedBy() == null ? null
							: profitAllocation.getCreatedBy().getId();
					toUserIdList.add(createdUserId);

					int toUserIdListSize = toUserIdList == null ? 0 : toUserIdList.size();
					if (toUserIdListSize > 0) {
						for (Long toUser : toUserIdList) {
							String msg = "";
							msg = "Profit Allocation - final approval has been completed";
							notificationEmailService.saveIndividualNotification(fromUser, toUser, urlPath,
									"Profit Allocation", msg);
						}
					}
				}
				
			}
		}catch (Exception e) {
			log.info("approveProfitAllocation exception--------"+e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}
	
	public BaseDTO rejectProfitAllocation(ProfitAllocationViewDTO profitAllocationViewDTO) {
		BaseDTO baseDTO = null;
		try {
			baseDTO = new BaseDTO();
			if(profitAllocationViewDTO.getProfitAllocationId() !=null) {
				ProfitAllocation profitAllocation = profitAllocationRepository.findOne(profitAllocationViewDTO.getProfitAllocationId());
				log.info("ProfitAllocation id----------"+profitAllocation.getId());
				
				ProfitAllocationLog  profitAllocationLog = new ProfitAllocationLog();
				profitAllocationLog.setProfitAllocation(profitAllocation);
				profitAllocationLog.setStage(profitAllocationViewDTO.getStage());
				profitAllocationLog.setRemarks(profitAllocationViewDTO.getRemarks());
				profitAllocationLogRepository.save(profitAllocationLog);
				log.info("profit allocation log save-------");
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				
				UserMaster fromUser = loginService.getCurrentUser();
				String urlPath = PROFIT_ALLOCATION_VIEW_PAGE + "&profitAllocationId=" + profitAllocation.getId() +"&";
				if(profitAllocation!=null && profitAllocation.getId()!=null) {}
				List<Long> toUserIdList = new ArrayList<>();
				String query = "SELECT distinct(um.id) as userId FROM profit_allocation_note pan JOIN profit_allocation pa ON pan.profit_allocation_id=pa.id \n"
						+ "JOIN user_master um ON um.id=pan.forward_to WHERE pa.id=:profitAllocationId AND pan.forward_to<>:currentUserId";
				query = query.replace(":profitAllocationId", String.valueOf(profitAllocation.getId()));
				query = query.replace(":currentUserId", String.valueOf(fromUser.getId()));
				List<Map<String, Object>> mapList = jdbcTemplate.queryForList(query);
				int mapListSize = mapList == null ? 0 : mapList.size();
				if (mapListSize > 0) {
					for (Map<String, Object> dataMap : mapList) {
						toUserIdList.add(dataMap.get("userId") == null ? null
								: Long.valueOf(dataMap.get("userId").toString()));
					}
				}
				Long createdUserId = profitAllocation.getCreatedBy() == null ? null
						: profitAllocation.getCreatedBy().getId();
				toUserIdList.add(createdUserId);

				int toUserIdListSize = toUserIdList == null ? 0 : toUserIdList.size();
				if (toUserIdListSize > 0) {
					for (Long toUser : toUserIdList) {
						String msg = "";
						msg = "Profit Allocation - has been rejected";
						notificationEmailService.saveIndividualNotification(fromUser, toUser, urlPath,
								"Profit Allocation", msg);
					}
				}
			
				
			}
		}catch (Exception e) {
			log.info("approveProfitAllocation exception--------"+e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}
}
