package in.gov.cooptex.finance.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.repository.BankMasterRepository;
import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.model.BankBranchMaster;
import in.gov.cooptex.core.accounts.model.EntityBankBranch;
import in.gov.cooptex.core.accounts.model.JournalVoucher;
import in.gov.cooptex.core.accounts.model.Payment;
import in.gov.cooptex.core.accounts.model.PaymentDetails;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.repository.BankBranchMasterRepository;
import in.gov.cooptex.core.accounts.repository.EntityBankBranchRepository;
import in.gov.cooptex.core.accounts.repository.PaymentDetailsRepository;
import in.gov.cooptex.core.accounts.repository.VoucherRepository;
import in.gov.cooptex.core.accounts.repository.VoucherTypeRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.finance.repository.JournalVoucherRepository;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.BankMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.finance.dto.ECSClearanceDTO;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class ECSClearanceService {
	@Autowired
	ResponseWrapper responseWrapper;

	@Autowired
	LoginService loginService;

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	PaymentDetailsRepository paymentDetailsRepository;

	@Autowired
	VoucherTypeRepository voucherTypeRepository;

	@Autowired
	UserMasterRepository userMasterRepository;

	@Autowired
	EntityManager entityManager;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	VoucherRepository voucherRepository;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	@Autowired
	EntityBankBranchRepository entityBankBranchRepository;

	@Autowired
	BankBranchMasterRepository bankBranchMasterRepository;

	@Autowired
	BankMasterRepository bankMasterRepository;

	@Autowired
	JournalVoucherRepository journalVoucherRepository;

	public BaseDTO getAllECSClearance(ECSClearanceDTO eCSClearanceDTO) {

		log.info(" getallDesignTargetlazy  called...");
		BaseDTO baseDTO = new BaseDTO();
		try {
			Integer total = 0;
			double totalAmount = 0.0;
			Integer start = eCSClearanceDTO.getFirst(), pageSize = eCSClearanceDTO.getPagesize();
			start = start * pageSize;
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> suLlistofData = new ArrayList<Map<String, Object>>();

			//Date todatyDate = new Date();
			//SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss a");

			String queryName = "ECS_CLEARANCE_DETAILS_LIST";

			ApplicationQuery applicationQuery = applicationQueryRepository.findByQueryName(queryName);
			String query = applicationQuery.getQueryContent().trim();

			String mainQuery = query;
			//String subQuery = null;

			Object[] resultObj = entityMasterRepository
					.gettypeCodeIdRegionIdByLoggedInUser(loginService.getCurrentUser().getId());
			Object ob[] = (Object[]) resultObj[0];
			String entityMasterTypeCode = (String) (ob[0] != null ? ob[0] : "");
			Long entityId = ob[1] != null ? Long.valueOf(ob[1].toString()) : null;
			Long regionId = ob[2] != null ? Long.valueOf(ob[2].toString()) : null;

			if (entityMasterTypeCode.equalsIgnoreCase("REGIONAL_OFFICE") && entityId != null) {

				mainQuery += " and em.id in (select id from entity_master em1 where em1.region_id=" + entityId
						+ " or em1.id=" + entityId + " )";
				if (regionId != null && regionId > 0) {
					mainQuery += " or em.id in (select id from entity_master em1 where em1.region_id=" + regionId + ")";
				}
			} else if (entityMasterTypeCode.equalsIgnoreCase("HEAD_OFFICE")) {
				log.info("Entity Type Head Office");
			} else {
				return null;
			}

			mainQuery = mainQuery.replace(":ecsType", "'" + eCSClearanceDTO.getVoucherType() + "'");
			mainQuery = mainQuery.replace(":from_date", "'" + eCSClearanceDTO.getFromDate() + "'");
			mainQuery = mainQuery.replace(":to_date", "'" + eCSClearanceDTO.getToDate() + "'");

			if (eCSClearanceDTO != null) {

				if (eCSClearanceDTO.getReceipt_Number() != null) {
					mainQuery += " and upper(concat(v.reference_number_prefix,'-',v.reference_number)) like upper('%"
							+ eCSClearanceDTO.getReceipt_Number() + "%')";
				}
				if (eCSClearanceDTO.getTransaction_Name() != null) {
					mainQuery += " and v.name like upper('%" + eCSClearanceDTO.getTransaction_Name() + "%')";
				}

				if (eCSClearanceDTO.getRecietp_date() != null) {
					Date createdDate = eCSClearanceDTO.getRecietp_date();
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					mainQuery += " and v.created_date::date = date '" + dateFormat.format(createdDate) + "'";
				}
				if (eCSClearanceDTO.getAmount() != null) {
					Double value = Double.parseDouble(eCSClearanceDTO.getAmount().toString());
					Integer intValue = value.intValue();
					mainQuery += " and cast(v.net_amount as varchar) like '%" + intValue + "%' ";
				}
				if (eCSClearanceDTO.getShowromm_code_name() != null) {
					mainQuery += " and upper(concat(em.code,'/',em.name)) like upper('%"
							+ eCSClearanceDTO.getShowromm_code_name() + "%')";
				}

				if (eCSClearanceDTO.getMode_payment() != null) {
					mainQuery += " and upper(pm.code) like upper('%" + eCSClearanceDTO.getMode_payment() + "%')";
				}

				if (eCSClearanceDTO.getEffectiveStatus() != null) {
					String status = eCSClearanceDTO.getEffectiveStatus().toString();
					if (status.equalsIgnoreCase("Not Credited")) {

						mainQuery += " and (upper(pd.effective_status) like upper('"
								+ eCSClearanceDTO.getEffectiveStatus() + "') or pd.effective_status is null) ";
					} else if (status.equalsIgnoreCase("Credited") || status.equalsIgnoreCase("ADJUSTED")) {
						mainQuery += " and upper(pd.effective_status) like upper('"
								+ eCSClearanceDTO.getEffectiveStatus() + "') ";
					}

				}

			}

			List<ECSClearanceDTO> ecsClearanceDTOList = new ArrayList<>();
			String countQuery = "select count(*) AS count  from (" + mainQuery + ")t";

			countData = jdbcTemplate.queryForList(countQuery);
			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					total = Integer.parseInt(data.get("count").toString().replace(",", ""));
			}

			if (eCSClearanceDTO.getSortField() != null && eCSClearanceDTO.getSortOrder() != null) {

				if (eCSClearanceDTO.getSortField().equals("showromm_code_name")
						&& eCSClearanceDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by em.code asc  ";
				if (eCSClearanceDTO.getSortField().equals("showromm_code_name")
						&& eCSClearanceDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by em.code desc  ";

				if (eCSClearanceDTO.getSortField().equals("receipt_Number")
						&& eCSClearanceDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by reference_number asc  ";
				if (eCSClearanceDTO.getSortField().equals("receipt_Number")
						&& eCSClearanceDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by reference_number desc  ";

				if (eCSClearanceDTO.getSortField().equals("amount")
						&& eCSClearanceDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by net_amount asc  ";
				if (eCSClearanceDTO.getSortField().equals("amount")
						&& eCSClearanceDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by net_amount desc  ";

				if (eCSClearanceDTO.getSortField().equals("transaction_Name")
						&& eCSClearanceDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by name asc  ";
				if (eCSClearanceDTO.getSortField().equals("transaction_Name")
						&& eCSClearanceDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by name desc  ";

				if (eCSClearanceDTO.getSortField().equals("recietp_date")
						&& eCSClearanceDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by created_date asc  ";
				if (eCSClearanceDTO.getSortField().equals("recietp_date")
						&& eCSClearanceDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by created_date desc  ";

				if (eCSClearanceDTO.getSortField().equals("mode_payment")
						&& eCSClearanceDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by pm.code asc  ";
				if (eCSClearanceDTO.getSortField().equals("mode_payment")
						&& eCSClearanceDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by pm.code desc  ";

				if (eCSClearanceDTO.getSortField().equals("effectiveStatus")
						&& eCSClearanceDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by effective_status asc  ";
				if (eCSClearanceDTO.getSortField().equals("effectiveStatus")
						&& eCSClearanceDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by effective_status desc  ";

				mainQuery += " limit " + pageSize + " offset " + start + ";";

			}
			//subQuery = mainQuery;
			suLlistofData = jdbcTemplate.queryForList(mainQuery);
			for (Map<String, Object> obj : suLlistofData) {
				if (obj.get("amount") != null) {
					totalAmount = totalAmount + Double.parseDouble(obj.get("amount").toString());

				}
			}

			mainQuery += " order by v.id,v.created_date,em.name desc limit " + pageSize + " offset " + start + ";";

			// mainQuery=mainQuery+" order by v.id,v.created_date,em.name desc ";
			log.info("mainQuery " + mainQuery);
			listofData = jdbcTemplate.queryForList(mainQuery);
			for (Map<String, Object> data : listofData) {

				ECSClearanceDTO ecsClearanceDTO = new ECSClearanceDTO();

				if (data.get("Showroom_code_name") != null) {
					ecsClearanceDTO.setShowromm_code_name(data.get("Showroom_code_name").toString());
				}

				if (data.get("receipt_no") != null) {
					ecsClearanceDTO.setReceipt_Number(data.get("receipt_no").toString());
				}

				if (data.get("amount") != null) {
					ecsClearanceDTO.setAmount(Double.parseDouble(data.get("amount").toString()));

				}
				if (data.get("transaction_name") != null) {
					ecsClearanceDTO.setTransaction_Name(data.get("transaction_name").toString());
				}
				if (data.get("receipt_date") != null) {
					ecsClearanceDTO.setRecietp_date((Date) data.get("receipt_date"));
				} else {
					ecsClearanceDTO.setRecietp_date(null);
				}

				if (data.get("bankReferenceNumber") != null) {
					ecsClearanceDTO.setBankReferenceNumber(data.get("bankReferenceNumber").toString());
				}
				if (data.get("created_date") != null) {
					ecsClearanceDTO.setCreatedDate((Date) data.get("created_date"));
				}

				if (data.get("mode_of_receipt") != null) {
					ecsClearanceDTO.setMode_payment(data.get("mode_of_receipt").toString());
				}
				if (data.get("status") == null) {
					ecsClearanceDTO.setEffectiveStatus("Not Credited");
				} else {
					ecsClearanceDTO.setEffectiveStatus(data.get("status").toString());
				}
				if (data.get("created_date") != null) {
					ecsClearanceDTO.setDepositeDate((Date) data.get("created_date"));
				}
				if (data.get("payment_details_id") != null) {
					ecsClearanceDTO.setPaymentDetailsId(Long.valueOf(data.get("payment_details_id").toString()));
				}

				ecsClearanceDTOList.add(ecsClearanceDTO);

			}
			baseDTO.setSumTotal(totalAmount);
			baseDTO.setResponseContents(ecsClearanceDTOList);
			baseDTO.setTotalRecords(total);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (Exception exp) {
			log.error("Exception Cause  : ", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;

	}

	public BaseDTO updateECSClearanceDetails(ECSClearanceDTO paymentDetails) {
		log.info("<--BankReconcilleService() .update updateECSClearanceDetails() Started-->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("<-- paymentDetails Update Start ID is-->" + paymentDetails.getPaymentDetailsId() + ":");
			Voucher voucher = voucherRepository.getVoucherDetailsByReferenceNo(paymentDetails.getReceipt_Number());
			if (voucher != null) {
				PaymentDetails paymentDetailsObj = null;
				if (paymentDetails.getPaymentDetailsId() != null) {
					paymentDetailsObj = paymentDetailsRepository.findOne(paymentDetails.getPaymentDetailsId());
				} else {
					paymentDetailsObj = paymentDetailsRepository.getPaymentDetailsByVoucherId(voucher.getId());
				}

				if (paymentDetailsObj != null) {

					if ("CANCEL_CREDIT".equals(paymentDetails.getStatus())) {
						paymentDetailsObj.setEffectiveStatus(null);
					}

					EntityMaster entityMaster = entityMasterRepository
							.getWorkLocationByUserId(loginService.getCurrentUser().getId());
					if (paymentDetailsObj.getEffectiveStatus() != null) {
						List<JournalVoucher> JournalVoucherList = journalVoucherRepository
								.getJournalDetailsAccToEntityAndECSDate(paymentDetails.getEce_to_date(),
										entityMaster.getId());
						if (JournalVoucherList != null && JournalVoucherList.size() > 0) {
							baseDTO.setStatusCode(MastersErrorCode.ECS_CLEARANCE_VOUCHER_IS_ALREADY_CREATED);
							return baseDTO;
						}
					}
					paymentDetailsObj.setEffectiveDate(paymentDetails.getEce_to_date());
					paymentDetailsObj.setBankMaster(paymentDetails.getBankMaster());
					if ("CANCEL_CREDIT".equals(paymentDetails.getStatus())) {
						paymentDetailsObj.setEffectiveStatus(null);
						paymentDetailsObj.setDepositedDate(null);
						paymentDetailsObj.setEntityBankBranch(null);
					} else {
						paymentDetailsObj.setEffectiveStatus(paymentDetails.getStatus());
						paymentDetailsObj.setDepositedDate(voucher.getCreatedDate());

						if (paymentDetails.getEntityBankBranche() != null
								&& paymentDetails.getEntityBankBranche().getId() != null) {
							paymentDetailsObj.setEntityBankBranch(
									entityBankBranchRepository.findOne(paymentDetails.getEntityBankBranche().getId()));
						} else {
							EntityBankBranch entityBankBranch = entityBankBranchRepository.findByBranchIdEntityId(
									paymentDetails.getBankBranchMaster().getId(), entityMaster.getId(),
									paymentDetails.getEntityBankBranche().getAccountNumber());
							if (entityBankBranch != null) {
								paymentDetailsObj.setEntityBankBranch(entityBankBranch);
							}
						}
					}

					// String ecsDate=dft.format(paymentDetails.getEce_to_date());

				}
				paymentDetailsRepository.save(paymentDetailsObj);
				log.info("<-- paymentDetails Update Successfully -->");
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				log.info("<-- paymentDetails Update Successfully -->");
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			}

		} catch (Exception e) {
			log.error("Exception in updatePaymentChequeDetails() ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(baseDTO);

	}

	public BaseDTO viewECSClearanceDetails(ECSClearanceDTO eCSClearanceDTO) {
		log.info("<--BankReconcilleService() .update updateECSClearanceDetails() Started-->");
		BaseDTO baseDTO = new BaseDTO();
		ECSClearanceDTO viewECSClearanceDTO = new ECSClearanceDTO();
		try {
			EntityMaster entityMaster = entityMasterRepository
					.getWorkLocationByUserId(loginService.getCurrentUser().getId());
			Long entityId = entityMaster != null ? entityMaster.getId() : null;

			Voucher voucher = voucherRepository.getVoucherDetailsByReferenceNo(eCSClearanceDTO.getReceipt_Number());
			if (voucher != null) {
				List<PaymentDetails> paymentDetailsObject = paymentDetailsRepository
						.getPaymentListByVoucher(voucher.getId());
				if (paymentDetailsObject != null) {
					Long paymentDetailsId = eCSClearanceDTO.getPaymentDetailsId();
					for (PaymentDetails paymentDetailsObj : paymentDetailsObject) {
						if (paymentDetailsId != null && paymentDetailsObj.getId() != null
								&& paymentDetailsId.longValue() == paymentDetailsObj.getId().longValue()) {
							viewECSClearanceDTO.setPaymentDetailsId(paymentDetailsObj.getId());
							viewECSClearanceDTO.setEce_to_date(paymentDetailsObj.getEffectiveDate());
							viewECSClearanceDTO.setEffectiveStatus(paymentDetailsObj.getEffectiveStatus());
							if (paymentDetailsObj.getBankReferenceNumber() != null) {
								viewECSClearanceDTO.setBankReferenceNumber(paymentDetailsObj.getBankReferenceNumber());
							}
							viewECSClearanceDTO.setEntityBankBranche(paymentDetailsObj.getEntityBankBranch());
							Long bankBranchId = paymentDetailsObj.getEntityBankBranch() != null
									? (paymentDetailsObj.getEntityBankBranch().getBankBranchMaster() != null
											? paymentDetailsObj.getEntityBankBranch().getBankBranchMaster().getId()
											: null)
									: null;
							if (paymentDetailsObj.getEntityBankBranch() != null) {
								String accountNumber = paymentDetailsObj.getEntityBankBranch().getAccountNumber();
								EntityBankBranch entityBankBranch = entityBankBranchRepository
										.findByBranchIdEntityId(bankBranchId, entityId, accountNumber);
								if (entityBankBranch != null) {
									log.info("==>> BankBranchMasterService Inside getAllBankBranchs<<== Start");
									BankBranchMaster bankBranchMaster = bankBranchMasterRepository
											.getBankByBranchId(entityBankBranch.getBankBranchMaster().getId());
									if (bankBranchMaster != null) {
										BankMaster BankMaster = bankMasterRepository
												.getBankDetails(bankBranchMaster.getBankMaster().getId());
										if (BankMaster != null) {
											viewECSClearanceDTO.setBankName(BankMaster.getBankName());

										}
									}
								}

							}
							if (paymentDetailsObj.getEffectiveDate() != null) {
								viewECSClearanceDTO.setEce_to_date(paymentDetailsObj.getEffectiveDate());
							}
							if (paymentDetailsObj.getEffectiveStatus() != null) {
								viewECSClearanceDTO.setStatus(paymentDetailsObj.getEffectiveStatus());
							}

							if (paymentDetailsObj.getCreatedDate() != null) {
								viewECSClearanceDTO.setDepositeDate(paymentDetailsObj.getCreatedDate());
							}
						}
					}
					baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
					baseDTO.setResponseContent(viewECSClearanceDTO);
				}
			}

			log.info("<-- ecs Details view-->");
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.error("Exception in updatePaymentChequeDetails() ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO splitEcsReceipt(ECSClearanceDTO eCSClearanceDTO) {
		log.info("updateChequeToBank() method starts-------");
		BaseDTO baseDTO = new BaseDTO();
		Voucher voucher = new Voucher();
		PaymentDetails paymentDetailss = new PaymentDetails();
		try {
			Payment payment = new Payment();
			PaymentDetails pmntDtls = new PaymentDetails();
			List<ECSClearanceDTO> eCSClearanceDTOList = eCSClearanceDTO.getSplitEcsReceiptDetailsList();
			if (!CollectionUtils.isEmpty(eCSClearanceDTOList)) {

				for (ECSClearanceDTO scdl : eCSClearanceDTOList) {
					if (scdl.getId() != null) {
						pmntDtls = paymentDetailsRepository.findOne(scdl.getId());
						voucher = pmntDtls.getVoucher();
						payment = pmntDtls.getPayment();
						break;
					}
				}

				//Date depositedDate = null;
				for (ECSClearanceDTO splitChellanDetails : eCSClearanceDTOList) {
					PaymentDetails paymentDetails = new PaymentDetails();
					// AmountTransferDetails amountTransferDetails = new AmountTransferDetails();
					if (splitChellanDetails.getId() != null) {
						paymentDetails = paymentDetailsRepository.findOne(splitChellanDetails.getId());
						paymentDetailss = paymentDetails;
						//depositedDate = paymentDetails.getDepositedDate();
						/*
						 * amountTransferDetails =
						 * amountTransferDetailsRepository.getAmtTrnsDtlsbyAmtIdPmtDtlsId(
						 * chequeToBankResponseDTO.getId(), splitChellanDetails.getId());
						 */

						/* voucher=paymentDetails.getVoucher(); */
					} else {
						// paymentDetails.setCreatedDate(new Date());
						// amountTransferDetails.setCreatedDate(new Date());
						paymentDetails.setCreatedDate(voucher.getCreatedDate());
						// amountTransferDetails.setCreatedDate(voucher.getCreatedDate());
					}
					paymentDetails.setVoucher(voucher);
					paymentDetails.setPayment(payment);
					paymentDetails.setPaymentCategory(pmntDtls.getPaymentCategory());
					paymentDetails.setPaymentMethod(pmntDtls.getPaymentMethod());
					paymentDetails.setPaymentTypeMaster(pmntDtls.getPaymentTypeMaster());
					paymentDetails.setAmount(splitChellanDetails.getAmount());
					paymentDetails.setBankMaster(paymentDetailss.getBankMaster());
					paymentDetails.setBankReferenceNumber(splitChellanDetails.getBankReferenceNumber());
					// paymentDetails.setDepositedDate(depositedDate);
					paymentDetails.setDocumentDate(splitChellanDetails.getRecietp_date());
					paymentDetails.setCreatedBy(userMasterRepository.findOne(loginService.getCurrentUser().getId()));

					if (pmntDtls != null) {
						paymentDetails.setCreatedDate(pmntDtls.getCreatedDate());
					}

					// paymentDetails.setCreatedByName(loginService.getCurrentUser().getUsername());
					paymentDetailsRepository.save(paymentDetails);
					log.info("PaymentDetails updated----------->");
					baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
					/*
					 * EntityBankBranch toBranchId = null; if
					 * (splitChellanDetails.getEntityBankBranch() != null &&
					 * splitChellanDetails.getEntityBankBranch().getId() != null) { toBranchId =
					 * entityBankBranchRepository
					 * .findOne(splitChellanDetails.getEntityBankBranch().getId()); } else {
					 * toBranchId = null; } amountTransfer =
					 * amountTransferRepository.findOne(chequeToBankResponseDTO.getId());
					 * amountTransferDetails.setAmountTransfer(amountTransfer);
					 * amountTransferDetails.setChallanAmount(splitChellanDetails.getNetAmount());
					 * amountTransferDetails.setChallanDate(splitChellanDetails.getChellanDate());
					 * amountTransferDetails.setChallanNumber(splitChellanDetails.getChellanNumber()
					 * ); amountTransferDetails.setToBranch(toBranchId);
					 * amountTransferDetails.setPaymentDetails(paymentDetails);
					 * amountTransferDetails
					 * .setCreatedBy(userMasterRepository.findOne(loginService.getCurrentUser().
					 * getId()));
					 * amountTransferDetails.setCreatedByName(loginService.getCurrentUser().
					 * getUsername()); amountTransferDetailsRepository.save(amountTransferDetails);
					 */

				}

				if (voucher != null && voucher.getId() != null) {
					Date voucherDate = voucher.getCreatedDate() == null ? new Date() : voucher.getCreatedDate();
					jdbcTemplate.update("UPDATE payment_details SET created_date=? WHERE voucher_id=?",
							new Object[] { voucherDate, voucher.getId() });
				}

			}

		} catch (Exception e) {
			log.info("Exception Occured At  editChequeToBank method -------", e);
		}
		return baseDTO;
	}

}
