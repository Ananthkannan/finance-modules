package in.gov.cooptex.finance.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.dto.InvestmentDTO;
import in.gov.cooptex.core.accounts.enums.VoucherTypeDetails;
import in.gov.cooptex.core.accounts.model.InvestmentClosing;
import in.gov.cooptex.core.accounts.model.NewInvestment;
import in.gov.cooptex.core.accounts.model.Payment;
import in.gov.cooptex.core.accounts.model.PaymentDetails;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.VoucherDetails;
import in.gov.cooptex.core.accounts.model.VoucherType;
import in.gov.cooptex.core.accounts.repository.InvestmentCategoryRepository;
import in.gov.cooptex.core.accounts.repository.InvestmentClosingRepository;
import in.gov.cooptex.core.accounts.repository.NewInvestmentRepository;
import in.gov.cooptex.core.accounts.repository.PaymentDetailsRepository;
import in.gov.cooptex.core.accounts.repository.PaymentMethodRepository;
import in.gov.cooptex.core.accounts.repository.PaymentRepository;
import in.gov.cooptex.core.accounts.repository.PaymentTypeMasterRepository;
import in.gov.cooptex.core.accounts.repository.VoucherLogRepository;
import in.gov.cooptex.core.accounts.repository.VoucherNoteRepository;
import in.gov.cooptex.core.accounts.repository.VoucherRepository;
import in.gov.cooptex.core.accounts.repository.VoucherTypeRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.PaymentCategory;
import in.gov.cooptex.core.enums.SequenceName;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.SequenceConfig;
import in.gov.cooptex.core.repository.AppQueryRepository;
import in.gov.cooptex.core.repository.SequenceConfigRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class InvestmentClosingService {

	@Autowired
	InvestmentClosingRepository investmentClosingRepository;

	@Autowired
	ResponseWrapper responseWrapper;

	@Autowired
	InvestmentCategoryRepository investmentCategoryRepository;

	@Autowired
	LoginService loginService;

	@Autowired
	EntityManager entityManager;
	
	@Autowired
	NewInvestmentRepository newInvestmentRepository;
	
	@Autowired
	SequenceConfigRepository sequenceConfigRepository;
	
	@Autowired
	VoucherTypeRepository voucherTypeRepository;
	
	@Autowired
	VoucherRepository voucherRepository;
	
	@Autowired
	UserMasterRepository userMasterRepository;
	
	@Autowired
	PaymentRepository paymentRepository;
	
	@Autowired
	PaymentTypeMasterRepository paymentTypeMasterRepository;
	
	@Autowired
	PaymentMethodRepository paymentMethodRepository;
	
	@Autowired
	PaymentDetailsRepository paymentDetailsRepository;
	
	@Autowired
	AppQueryRepository appQueryRepository;
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	VoucherNoteRepository voucherNoteRepository;
	
	@Autowired
	VoucherLogRepository voucherLogRepository;

	public BaseDTO getById(Long id) {
		log.info("InvestmentClosingService getById method started [" + id + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			InvestmentDTO investmentDTO=new InvestmentDTO();
			if(id!=null) {
				InvestmentClosing investmentClosing = investmentClosingRepository.getOne(id);
				log.info("InvestmentClosing obj------------"+investmentClosing);

				NewInvestment newInvestment=newInvestmentRepository.findOne(investmentClosing.getNewInvestment().getId());
				log.info("NewInvestment obj------------"+newInvestment);
				
				/*VoucherNote voucherNote=voucherNoteRepository.findByVoucherId(investmentClosing.getVoucher().getId());
				VoucherLog voucherLog=voucherLogRepository.findByVoucherId(investmentClosing.getVoucher().getId());
				log.info("VoucherLog obj---------------"+voucherLog);
				log.info("VoucherNote obj---------------"+voucherNote);*/
				
				List<PaymentDetails> paymentDetails=paymentDetailsRepository.getPaymentListByVoucher(investmentClosing.getVoucher().getId());
				log.info("paymentDetails list size-------"+paymentDetails.size());
				if(paymentDetails.size()>0) {
					investmentDTO.setDocumentNumber(paymentDetails.get(0).getBankReferenceNumber());
					investmentDTO.setDocumentDate(paymentDetails.get(0).getDocumentDate());
					investmentDTO.setBankMaster(paymentDetails.get(0).getBankMaster());
				}
				investmentDTO.setPaymentMethod(investmentClosing.getPaymentMethod());
				investmentDTO.setInvestmentClosing(investmentClosing);
				investmentDTO.setNewInvestment(newInvestment);
				//investmentDTO.setFianlApproval(voucherNote.getFinalApproval());
				//investmentDTO.setStage(String.valueOf(voucherLog.getStatus()));
				//investmentDTO.setNote(voucherNote.getNote());
				//investmentDTO.setRemarks(voucherLog.getRemarks());
				//investmentDTO.setUserMaster(voucherNote.getForwardTo());
				
				baseDTO.setResponseContent(investmentDTO);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (RestException restException) {
			log.error("InvestmentClosingService getById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("InvestmentClosingService getById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("InvestmentClosingService getById method completed");
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO deleteById(Long id) {
		log.info("InvestmentClosingService deleteById method started [" + id + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			investmentClosingRepository.delete(id);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("InvestmentClosingService deleteById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (DataIntegrityViolationException exception) {
			log.error("InvestmentClosingService deleteById DataIntegrityViolationException ", exception);
			if (exception.getCause().getCause() instanceof PSQLException) {
				baseDTO.setStatusCode(ErrorDescription.CANNOT_DELETE_REFERENCED_RECORD.getErrorCode());
			} else {
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			}
		} catch (Exception exception) {
			log.error("InvestmentClosingService deleteById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("InvestmentClosingService deleteById method completed");
		return responseWrapper.send(baseDTO);
	}

	@Transactional
	public BaseDTO createInvestmentClosing(InvestmentDTO investmentDTO) {
		log.info("InvestmentClosingService create method Start-----");
		BaseDTO baseDTO = new BaseDTO();
		String referenceNumber = "";
		try {
			if (investmentDTO.getInvestmentClosing().getId() == null) {
				SequenceConfig sequenceConfig = sequenceConfigRepository
						.findBySequenceName(SequenceName.INVESTMENT_CLOSING);
				if (sequenceConfig == null) {
					throw new RestException(ErrorDescription.SEQUENCE_CONFIG_NOT_EXIST);
				}
				referenceNumber = investmentDTO.getEntityMaster().getCode() + sequenceConfig.getSeparator()
						+ sequenceConfig.getPrefix() + AppUtil.getCurrentMonthString() +AppUtil.getCurrentYearString();
				log.info("voucher reference number-----------" + referenceNumber);

				Voucher voucher = new Voucher();
				voucher.setReferenceNumberPrefix(referenceNumber);
				voucher.setReferenceNumber(sequenceConfig.getCurrentValue());
				voucher.setName(VoucherTypeDetails.InvesmentClosing.toString());
				VoucherType voucherType = voucherTypeRepository.findByName(VoucherTypeDetails.Receipt.toString());
				log.info("voucherType Id===============>" + voucherType.getId());
				voucher.setVoucherType(voucherType);
				voucher.setNarration(VoucherTypeDetails.Payment.toString());
				voucher.setFromDate(new Date());
				voucher.setToDate(new Date());
				voucher.setNetAmount(investmentDTO.getInvestmentClosing().getTotalAmountCollected());
				log.info("Voucher table added-------------");

				VoucherDetails voucherDetails = new VoucherDetails();
				voucherDetails.setVoucher(voucher);
				voucherDetails.setAmount(investmentDTO.getInvestmentClosing().getTotalAmountCollected());
				voucher.getVoucherDetailsList().add(voucherDetails);
				log.info("VoucherDetails table added---------------");

				/*VoucherNote voucherNote = new VoucherNote();
				voucherNote.setForwardTo(userMasterRepository.findOne(investmentDTO.getUserMaster().getId()));
				log.info("voucherNote forwardTo user---------------->" + voucherNote.getForwardTo());
				voucherNote.setFinalApproval(investmentDTO.getFianlApproval());
				voucherNote.setNote(investmentDTO.getNote());
				voucherNote.setVoucher(voucher);
				voucher.getVoucherNote().add(voucherNote);
				log.info("voucherNote table added----------------");

				VoucherLog voucherLog = new VoucherLog();
				voucherLog.setVoucher(voucher);
				voucherLog.setStatus(VoucherStatus.SUBMITTED);
				UserMaster userMaster = userMasterRepository.findOne(loginService.getCurrentUser().getId());
				log.info("VoucherLog userMaster----------->" + userMaster.getId());
				voucherLog.setUserMaster(userMaster);
				voucher.getVoucherLogList().add(voucherLog);
				log.info("VoucherLog table added---------------");*/

				voucherRepository.save(voucher);
				log.info("Voucher datas inserted----------------");

				Payment payment = new Payment();
				payment.setEntityMaster(investmentDTO.getEntityMaster());
				payment.setPaymentNumberPrefix(referenceNumber);
				payment.setPaymentNumber(sequenceConfig.getCurrentValue());
				paymentRepository.save(payment);
				log.info("payment saved---------------" + payment.getId());

				PaymentDetails paymentDetails = new PaymentDetails();
				paymentDetails.setPayment(payment);
				paymentDetails.setPaymentCategory(PaymentCategory.PAID_IN);
				if (!investmentDTO.getPaymentMethod().getName().equals("Cash")) {
					paymentDetails.setBankReferenceNumber(investmentDTO.getDocumentNumber().toString());
					paymentDetails.setDocumentDate(investmentDTO.getDocumentDate());
					paymentDetails.setBankMaster(investmentDTO.getBankMaster());
				}
				paymentDetails.setVoucher(voucher);
				paymentDetails.setPaymentTypeMaster(paymentTypeMasterRepository.getCustomerPaymentType());
				paymentDetails
						.setPaymentMethod(paymentMethodRepository.findOne(investmentDTO.getPaymentMethod().getId()));
				paymentDetailsRepository.save(paymentDetails);
				log.info("PaymentDetails saved-------" + paymentDetails.getId());

				sequenceConfig.setCurrentValue(sequenceConfig.getCurrentValue() + 1);
				sequenceConfigRepository.save(sequenceConfig);
				log.info("sequenceConfig value updated-----"+sequenceConfig.getCurrentValue());

				InvestmentClosing investmentClosing = new InvestmentClosing();
				investmentClosing
						.setNewInvestment(newInvestmentRepository.findOne(investmentDTO.getNewInvestment().getId()));
				log.info("new investment---------" + investmentClosing.getNewInvestment());
				investmentClosing.setVoucher(voucher);
				investmentClosing
						.setPaymentMethod(paymentMethodRepository.findOne(investmentDTO.getPaymentMethod().getId()));
				investmentClosing
						.setInvestedAmountCollected(investmentDTO.getInvestmentClosing().getInvestedAmountCollected());
				investmentClosing.setInvestedInterestCollected(
						investmentDTO.getInvestmentClosing().getInvestedInterestCollected());
				investmentClosing
						.setTotalAmountCollected(investmentDTO.getInvestmentClosing().getTotalAmountCollected());
				investmentClosingRepository.save(investmentClosing);
				log.info("InvestmentClosing saved-------" + investmentClosing.getId());

				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}

		} catch (Exception e) {
			log.error("InvestmentClosingService createInvestmentClosing Exception ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("InvestmentClosingService reate method End-----");
		return baseDTO;
	}

	public BaseDTO lazyInvestmentClosing(PaginationDTO paginationDTO) {
		log.info("lazyInvestmentClosing service method starts---------------");
		BaseDTO baseDTO = new BaseDTO();
		List<InvestmentDTO> resultList=null;
		Integer totalRecords = 0;
		try {
			resultList=new ArrayList<InvestmentDTO>();
			Integer start = paginationDTO.getFirst(), pageSize = paginationDTO.getPageSize();
			start = start * pageSize;
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();
			
			ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("INVESTMENT_CLOSING");
			String mainQuery = applicationQuery.getQueryContent();
			log.info("db query----------" + mainQuery);

			if (paginationDTO.getFilters() != null) {
				if (paginationDTO.getFilters().get("investmentType") != null) {
					mainQuery += " where itm.name='" + paginationDTO.getFilters().get("investmentType") + "'";
				}
				if (paginationDTO.getFilters().get("bankName") != null) {
					mainQuery += " where upper(bm.bank_name) like upper('%"
							+ paginationDTO.getFilters().get("bankName") + "%')";
				}
				if (paginationDTO.getFilters().get("branchName") != null) {
					mainQuery += " where upper(bbm.branch_name) like upper('%"
							+ paginationDTO.getFilters().get("branchName") + "%')";
				}
				if (paginationDTO.getFilters().get("investmentId") != null) {
					mainQuery += " where ic.investment_id="
							+ paginationDTO.getFilters().get("investmentId");
				}
			}
			
			String countQuery = mainQuery.replace(
					"select ic.id,ic.investment_id,itm.name,bm.bank_name,bbm.branch_name,ni.cerificate_number",
					"select count(distinct(ic.id)) as count ");
			log.info("count query... " + countQuery);
			countData = jdbcTemplate.queryForList(countQuery);
			log.info("investment closing count size-------------->" + countData.size());

			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					totalRecords = Integer.parseInt(data.get("count").toString().replace(",", ""));
				log.info("total records---------------"+totalRecords);
			}
			
			if (paginationDTO.getSortField() == null)
				mainQuery += " order by ic.id desc limit " + pageSize + " offset " + start + ";";
			
				if (paginationDTO.getSortField() != null && paginationDTO.getSortOrder() != null) {
					log.info("Sort Field:[" + paginationDTO.getSortField() + "] Sort Order:[" + paginationDTO.getSortOrder()
							+ "]");
					if (paginationDTO.getSortField().equals("investmentType")
							&& paginationDTO.getSortOrder().equals("ASCENDING")) {
						mainQuery += " order by itm.name asc  ";
					}
					if (paginationDTO.getSortField().equals("investmentType")
							&& paginationDTO.getSortOrder().equals("DESCENDING")) {
						mainQuery += " order by itm.name desc  ";
					}
					if (paginationDTO.getSortField().equals("bankName")
							&& paginationDTO.getSortOrder().equals("ASCENDING")) {
						mainQuery += " order by bm.bank_name asc ";
					}
					if (paginationDTO.getSortField().equals("bankName")
							&& paginationDTO.getSortOrder().equals("DESCENDING")) {
						mainQuery += " order by bm.bank_name desc ";
					}
					if (paginationDTO.getSortField().equals("branchName")
							&& paginationDTO.getSortOrder().equals("ASCENDING")) {
						mainQuery += " order by bbm.branch_name asc ";
					}
					if (paginationDTO.getSortField().equals("branchName")
							&& paginationDTO.getSortOrder().equals("DESCENDING")) {
						mainQuery += " order by bbm.branch_name desc ";
					}
					if (paginationDTO.getSortField().equals("investmentId")
							&& paginationDTO.getSortOrder().equals("ASCENDING")) {
						mainQuery += " order by ic.investment_id asc  ";
					}
					if (paginationDTO.getSortField().equals("investmentId")
							&& paginationDTO.getSortOrder().equals("DESCENDING")) {
						mainQuery += " order by ic.investment_id desc  ";
					}
					mainQuery += " limit " + pageSize + " offset " + start + ";";
				}

				log.info("filter query----------" + mainQuery);

				listofData = jdbcTemplate.queryForList(mainQuery);
				log.info("investment closing list size-------------->" + listofData.size());
				
				for (Map<String, Object> data : listofData) {
					InvestmentDTO investmentDTO=new InvestmentDTO();
					investmentDTO.setId(Long.valueOf(data.get("id").toString()));
					investmentDTO.setInvestmentType(data.get("name").toString());
					investmentDTO.setCerificateNumber(data.get("cerificate_number").toString());
					investmentDTO.setInvestmentId(Long.valueOf(data.get("investment_id").toString()));
					if(data.get("bank_name")==null) {
						investmentDTO.setBankName(null);
					}else {
						investmentDTO.setBankName(data.get("bank_name").toString());
					}
					if(data.get("branch_name")==null) {
						investmentDTO.setBranchName(null);
					}else {
						investmentDTO.setBranchName(data.get("branch_name").toString());
					}
					resultList.add(investmentDTO);
				}

				log.info("final list size------"+resultList.size());
				baseDTO.setResponseContents(resultList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				baseDTO.setTotalRecords(totalRecords);
		} catch (Exception e) {
			log.error("lazyInvestmentClosing Inside service Exception ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("lazyInvestmentClosing service method Ends---------------");
		return baseDTO;
	}
	
	public BaseDTO loadReferenceNoList(Long categoryId, Long typeId, Long branchId) {
		log.info("loadReferenceNoList service start------");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<NewInvestment> investMentList=newInvestmentRepository.loadReferenceNoList(categoryId,typeId,branchId);
			log.info("investMentList size-------"+investMentList.size());
			baseDTO.setResponseContents(investMentList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		}catch (Exception e) {
			log.info("loadReferenceNoList service Exception------",e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("loadReferenceNoList service end------");
		return baseDTO;
	}

	public BaseDTO referenceNoByInstitution(Long categoryId, Long typeId, Long institutionId) {
		log.info("referenceNoByInstitution service start------");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<NewInvestment> investMentList = newInvestmentRepository.referenceNoByInstitution(categoryId, typeId,
					institutionId);
			log.info("investMentList size-------" + investMentList.size());
			baseDTO.setResponseContents(investMentList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.info("referenceNoByInstitution service Exception------", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("referenceNoByInstitution service end------");
		return baseDTO;
	}

	public BaseDTO checkInvestment(Long investmentId) {
		log.info("checkInvestment service start------");
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("investment id-------" + investmentId);
			InvestmentClosing investmentClosing = investmentClosingRepository.checkInvestmentId(investmentId);
			log.info("investMentList size-------" + investmentClosing);
			if (investmentClosing != null) {
				baseDTO.setStatusCode(ErrorDescription.INVESTMENT_NUMBER_ALREADY_CLOSE.getErrorCode());
			} else {
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}

		} catch (Exception e) {
			log.info("referenceNoByInstitution service Exception------", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("checkInvestment service end------");
		return baseDTO;
	}

}