package in.gov.cooptex.finance.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import commonDataService.AppConfigKey;
import in.gov.cooptex.common.repository.AppConfigRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.AppConfig;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.repository.ProductCategoryRepository;
import in.gov.cooptex.core.repository.RetailProductionPlanMonthWiseRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.finance.excel.report.EmployeeRetirmentExcelReport;
import in.gov.cooptex.finance.pdf.report.EmployeeRetirmentPDFReport;
import in.gov.cooptex.mis.dto.BaseReportDto;
import in.gov.cooptex.mis.dto.EmpRetirmentDto;
import in.gov.cooptex.mis.dto.GeneralLedgerRequestDto;
import in.gov.cooptex.mis.dto.ProductCategoryWiseReportDTO;
import in.gov.cooptex.mis.enums.ReportsNameEnum;
import in.gov.cooptex.mis.repository.ReportsRepository;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class GeneralLedgerReportService {

	@Autowired
	AppConfigRepository appConfigRepository;

	@Autowired
	ReportsRepository reportsRepository;

	@Autowired
	ProductCategoryRepository productCategoryRepository;

	@Autowired
	RetailProductionPlanMonthWiseRepository retailProductionPlanMonthWiseRepository;

	private String downloadFilePath;

	private static final String DOWNLOAD_FILE_URL = AppUtil.getMisServerURL();

	private String reportLogoPath;

	private void loadAppConfigValues() {
		try {

			AppConfig reportLogAppConfig = appConfigRepository.findByAppKey(AppConfigKey.REPORT_LOGO_PATH);

			if (reportLogAppConfig == null) {
				throw new Exception(AppConfigKey.REPORT_LOGO_PATH + " is not available in DB");
			}

			reportLogoPath = reportLogAppConfig.getAppValue();

			AppConfig filePathAppConfig = appConfigRepository.findByAppKey(AppConfigKey.DOWNLOAD_FILEPATH);

			if (filePathAppConfig == null) {
				throw new Exception(AppConfigKey.DOWNLOAD_FILEPATH + " is not available in DB");
			}

			downloadFilePath = filePathAppConfig.getAppValue();

		} catch (Exception ex) {
			log.error("Exception at loadAppConfigValues() ", ex);
		}

	}

	public BaseDTO getAllCategoryList() {
		log.info("ProductCategoryService getAllActiveCategories()============#START");
		BaseDTO baseDTO = new BaseDTO();
		try {

			List<ProductCategory> productCategoryList = productCategoryRepository.getAllActiveCategories();
			baseDTO.setResponseContent(productCategoryList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (Exception exception) {
			log.error("exception Occured ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO getProductCategoryWiseReport(ProductCategoryWiseReportDTO request) {
		log.info("================== getProductCategoryWiseReport() ReportService =====================");
		BaseDTO baseDTO = reportsRepository.getProductCategoryWiseReport(String.valueOf(ReportsNameEnum.PCWPP),
				request);
		return baseDTO;
	}

	public BaseDTO getMonthWiseReport(ProductCategoryWiseReportDTO request) {
		log.info("================== getMonthWiseReport() ReportService =====================");
		BaseDTO baseDTO = reportsRepository.getMonthWiseReport(String.valueOf(ReportsNameEnum.MWPP), request);
		return baseDTO;
	}

	public BaseDTO getEmployeeRetairmentReport(String reportName, EmpRetirmentDto paginationDto) {
		return reportsRepository.getEmployeeRetairmentReport(reportName, paginationDto);
	}

	public BaseDTO getEmployeeRetairmentDownloadReport(EmpRetirmentDto paginationDto) {
		log.info("<-- getEmployeeRetairmentDownloadReport -->");
		loadAppConfigValues();
		BaseDTO content = new BaseDTO();
		BaseDTO pageContent = reportsRepository.getEmployeeRetairmentDownloadReport(paginationDto);
		List<Map<String, Object>> reportlist = pageContent.getListOfData();
		BaseReportDto baseReportInfo = paginationDto.getBaseReportInfo();

		baseReportInfo.setLogoPath(reportLogoPath);

		String path = "";
		if (paginationDto.getDownloadExtensionType() != null
				&& paginationDto.getDownloadExtensionType().equals("PDF")) {
			try {
				log.info("<-- Generating Unit wiseCount Pdf Report -->");
				path = new EmployeeRetirmentPDFReport().generatePdfReport(reportlist, baseReportInfo, downloadFilePath,
						DOWNLOAD_FILE_URL + "/output");
			} catch (Exception e) {
				log.error("Error while Dowoading Pdf - ", e);
				e.printStackTrace();
			}
		} else {
			try {
				log.info("<-- Generating  Unit wiseCount Excel Report -->");
				path = new EmployeeRetirmentExcelReport().generateExcelReport(reportlist, baseReportInfo,
						downloadFilePath, DOWNLOAD_FILE_URL + "/output");
				log.info("Employee Retirment Report URL Path :" + path);
			} catch (Exception e) {
				log.error("Error while Dowoading Pdf - ", e);
				e.printStackTrace();
			}
		}
		content.setDowloadUrlPath(path);
		log.info("<-- Ends Unit wiseCount Download Service -->");
		return content;

	}

	public BaseDTO getGeneralLedgerReport(String reportName, GeneralLedgerRequestDto paginationDto) {
		log.info("<-- getGeneralLedgerReport -->");
		return reportsRepository.getGeneralLedgerReport(reportName, paginationDto);
	}

	public BaseDTO getGeneralLedgerDownloadReport(GeneralLedgerRequestDto paginationDto) {
		log.info("<-- getGeneralLedgerDownloadReport -->");
		BaseDTO pageContent = reportsRepository.getGeneralLedgerDownloadReport(paginationDto);
		return pageContent;

	}
}
