package in.gov.cooptex.finance.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.enums.VoucherStatus;
import in.gov.cooptex.core.accounts.model.GlAccount;
import in.gov.cooptex.core.accounts.model.PurchaseSalesAdvance;
import in.gov.cooptex.core.accounts.model.TaBill;
import in.gov.cooptex.core.accounts.model.TaBillLog;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.VoucherDetails;
import in.gov.cooptex.core.accounts.model.VoucherLog;
import in.gov.cooptex.core.accounts.model.VoucherNote;
import in.gov.cooptex.core.accounts.model.VoucherType;
import in.gov.cooptex.core.accounts.repository.GlAccountRepository;
import in.gov.cooptex.core.accounts.repository.StaffInvoiceAdjustmentRepository;
import in.gov.cooptex.core.accounts.repository.VoucherDetailsRepository;
import in.gov.cooptex.core.accounts.repository.VoucherLogRepository;
import in.gov.cooptex.core.accounts.repository.VoucherNoteRepository;
import in.gov.cooptex.core.accounts.repository.VoucherRepository;
import in.gov.cooptex.core.accounts.repository.VoucherTypeRepository;
import in.gov.cooptex.core.accounts.util.VoucherTypeCons;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.SequenceName;
import in.gov.cooptex.core.finance.repository.TaBillLogRepository;
import in.gov.cooptex.core.finance.repository.TaBillRepository;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EmployeePayDetails;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.PaymentMode;
import in.gov.cooptex.core.model.SequenceConfig;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.AppQueryRepository;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.DesignationRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EmployeePayDetailRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.PaymentModeRepository;
import in.gov.cooptex.core.repository.SequenceConfigRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.ui.GlAccountName;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.LedgerPostingException;
import in.gov.cooptex.exceptions.RestException;

import in.gov.cooptex.finance.TaBillPaymentDTO;
import in.gov.cooptex.finance.advance.repository.PurchaseSalesAdvanceRepository;
import in.gov.cooptex.finance.model.StaffInvoiceAdjustment;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class TaBillPaymentService {

	@Autowired
	TaBillRepository taBillRepository;

	@Autowired
	EmployeeMasterRepository employeeMasterRepository;

	@Autowired
	TaBillLogRepository taBillLogRepository;

	@Autowired
	GlAccountRepository glAccountRepository;

	@Autowired
	EmployeePayDetailRepository employeePayDetailRepository;

	@Autowired
	PaymentModeRepository paymentModeRepository;

	@Autowired
	EmployeeMasterRepository empmasterrepository;

	@Autowired
	UserMasterRepository userMasterRepositor;

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	AppQueryRepository appQueryRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	ResponseWrapper responseWrapper;

	@Autowired
	LoginService loginService;

	@Autowired
	VoucherRepository voucherRepository;

	@Autowired
	StaffInvoiceAdjustmentRepository staffInvoiceAdjustmentRepository;

	@Autowired
	VoucherLogRepository voucherlogRepository;

	@Autowired
	UserMasterRepository userMasterRepository;

	@Autowired
	VoucherNoteRepository voucherNoteRepository;

	@Autowired
	VoucherTypeRepository voucherTypeRepository;

	@Autowired
	VoucherDetailsRepository voucherDetailsRepository;

	@Autowired
	SequenceConfigRepository sequenceConfigRepository;

	@Autowired
	PurchaseSalesAdvanceRepository purchaseSalesAdvanceRepository;

	@Autowired
	DesignationRepository designationRepository;
	
	@Autowired
	ApplicationQueryRepository applicationQueryRepository;
	
	public BaseDTO getemployeelist() {
		log.info("TaBillService:getAllplancode()");
		BaseDTO baseDTO = new BaseDTO();
		List<TaBillPaymentDTO> employeeList = new ArrayList<TaBillPaymentDTO>();
		TaBillPaymentDTO paymentDto = new TaBillPaymentDTO();
		try {
			List<TaBill> tabillList = taBillRepository.getTaBillforPayment();
			for (TaBill taBill : tabillList) {
				paymentDto.setTaBillId(taBill.getId());
				paymentDto.setEmpID(taBill.getEmpMaster().getId());
				paymentDto.setEmpCodeName(
						taBill.getEmpMaster().getEmpCode() + " / " + taBill.getEmpMaster().getFirstName());
				paymentDto.setDesignation(taBill.getEmpMaster().getPersonalInfoEmployment().getDesignation() != null
						? taBill.getEmpMaster().getPersonalInfoEmployment().getDesignation().getName()
						: "");

				if (taBill.getEmpMaster().getSection() != null) {
					paymentDto.setSectionCode(taBill.getEmpMaster().getSection().getCode());
				}

				if (taBill.getTourProgram().getEntityMaster() != null) {
					paymentDto.setEntityCodeName(taBill.getTourProgram().getEntityMaster().getCode().toString() + " / "
							+ taBill.getTourProgram().getEntityMaster().getName());
				}
				// Basic Pay

				GlAccount gelaccount = glAccountRepository.findByName(GlAccountName.BASIC_PAY);
				if (gelaccount != null && taBill.getEmpMaster() != null) {
					EmployeePayDetails employeePayDetail = employeePayDetailRepository
							.getLatestBasicPay(taBill.getEmpMaster().getId(), gelaccount.getId());
					paymentDto.setBasicPay(employeePayDetail.getAmount());
				}

				paymentDto.setClaimType(taBill.getTaBillType());
				paymentDto.setClaimNames(taBill.getClaimName());
				paymentDto.setTotalAmountClaimed(taBill.getClaimTravelAmount() + taBill.getClaimContgAmount());
				paymentDto.setClaimedDate(taBill.getCreatedDate());
				paymentDto.setTotalAmountApproved(taBill.getApprovedTravelAmount() + taBill.getApprovedContgAmount());
				TaBillLog tabilllog = taBillLogRepository.getTaBillLogByID(taBill.getId());
				if (tabilllog != null) {
					paymentDto.setApprovedDate(tabilllog.getCreatedDate());
				}
				paymentDto.setClaimTravell(taBill.getClaimTravelAmount());
				paymentDto.setClaimContegency(taBill.getClaimContgAmount());
				paymentDto.setApprovedtravell(taBill.getApprovedTravelAmount());
				paymentDto.setApprovedContegency(taBill.getApprovedContgAmount());

				PurchaseSalesAdvance purchaseSalesAdvance = purchaseSalesAdvanceRepository
						.findByEmpIdandType(taBill.getEmpMaster().getId(), "Staff Tour Advance");
				if (purchaseSalesAdvance != null) {
					paymentDto.setPurchaseSalesAdvanceId(purchaseSalesAdvance.getId());
					paymentDto.setAdvance(purchaseSalesAdvance.getAmount());
					paymentDto.setAmounttobepaid(paymentDto.getTotalAmountApproved() - paymentDto.getAdvance());
				} else {
					paymentDto.setAdvance(0d);
					paymentDto.setAmounttobepaid(paymentDto.getTotalAmountApproved() - paymentDto.getAdvance());
				}
				employeeList.add(paymentDto);
			}

			baseDTO.setResponseContent(employeeList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {

			log.error("TaBillService getAllplancode RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {

			log.error("TaBillService getAllplancode Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}

		log.info("TaBillService getAllGlAccounts method completed");
		return baseDTO;
	}

	public BaseDTO getpaymentmodelist() {
		log.info("TaBillPaymentService:getpaymentmodelist()");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<PaymentMode> paymentModeList = paymentModeRepository.findAll();
			baseDTO.setResponseContent(paymentModeList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {

			log.error("TaBillPaymentService getpaymentmodelist RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {

			log.error("TaBillService getpaymentmodelist Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}

		log.info("TaBillPaymentService getpaymentmodelist method completed");
		return baseDTO;
	}

	public BaseDTO createtabill(TaBillPaymentDTO taBillpaymentDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("<<<<<<<======tabillPaymentService createtabillPayment method Started=====>>" + taBillpaymentDTO);

			TaBill tabill = taBillRepository.findOne(taBillpaymentDTO.getTaBillId());
			Voucher voucher = new Voucher();
			String refNumberPrefix = "";
			Long sequence_value = null;
			/*
			 * EntityBankBranch bankbranch = entityBankBranchRepository
			 * .findOne(cashCreditLoanLimit.getEntityBankBranch().getId());
			 */
			EntityMaster loginUserEntity = entityMasterRepository
					.findByUserRegion(loginService.getCurrentUser().getId());

			if (taBillpaymentDTO.getPaymentmode() != null) {
				PaymentMode payment = paymentModeRepository.findOne(taBillpaymentDTO.getPaymentmode().getId());
				voucher.setPaymentMode(payment);
			}
			voucher.setName(VoucherTypeCons.TA_BILL_PAYMENT);
			VoucherType voucherType = voucherTypeRepository.findByName(VoucherTypeCons.PAYMENTSMALL);
			voucher.setVoucherType(voucherType);
			voucher.setNetAmount(taBillpaymentDTO.getTotalAmountApproved());
			voucher.setNarration("a");
			SequenceConfig sequenceConfig = sequenceConfigRepository
					.findBySequenceName(SequenceName.valueOf(SequenceName.TA_BILL_PAYMENT.name()));
			if (sequenceConfig == null) {
				throw new RestException(ErrorDescription.SEQUENCE_CONFIG_NOT_EXIST);
			}
			if (loginUserEntity == null) {
				throw new RestException();
			}else {
				voucher.setEntityMaster(loginUserEntity);
			}

			refNumberPrefix = loginUserEntity.getCode() + sequenceConfig.getSeparator() + sequenceConfig.getPrefix()
					+ AppUtil.getCurrentYearString() + AppUtil.getCurrentMonthString()
					+ sequenceConfig.getCurrentValue();
			sequence_value = sequenceConfig.getCurrentValue();

			sequenceConfig.setCurrentValue(sequence_value + 1);

			sequenceConfigRepository.save(sequenceConfig);
			log.info("Voucher Number Prefix  ", refNumberPrefix);
			voucher.setReferenceNumberPrefix(refNumberPrefix);
			voucher.setReferenceNumber(sequenceConfig.getCurrentValue());
			voucher.setFromDate(new Date());
			voucher.setToDate(new Date());

			voucher = voucherRepository.save(voucher);
			VoucherDetails voucherDetails = new VoucherDetails();
			voucherDetails.setAmount(taBillpaymentDTO.getTotalAmountApproved());
			voucherDetails.setVoucher(voucher);
			voucherDetailsRepository.save(voucherDetails);

			VoucherLog voucherlog = new VoucherLog();
			voucherlog.setStatus(VoucherStatus.SUBMITTED);
			UserMaster currentuser = userMasterRepository.findOne(loginService.getCurrentUser().getId());
			voucherlog.setUserMaster(currentuser);
			voucherlog.setVoucher(voucher);
			voucherlogRepository.save(voucherlog);

			VoucherNote voucherNote = new VoucherNote();
			voucherNote.setVoucher(voucher);
			voucherNote.setNote(taBillpaymentDTO.getNote());
			UserMaster user = userMasterRepository.findOne(taBillpaymentDTO.getForwardTo().getId());
			voucherNote.setForwardTo(user);
			voucherNote.setFinalApproval(taBillpaymentDTO.isFinalapproval());
			voucherNoteRepository.save(voucherNote);

			tabill.setVoucher(voucher);
			taBillRepository.save(tabill);

			StaffInvoiceAdjustment staffInvoiceAdjustment = new StaffInvoiceAdjustment();
			staffInvoiceAdjustment.setVoucher(voucher);

			PurchaseSalesAdvance purchaseSalesAdvance = purchaseSalesAdvanceRepository
					.findOne(taBillpaymentDTO.getPurchaseSalesAdvanceId());
			if (purchaseSalesAdvance != null) {
				staffInvoiceAdjustment.setPurchaseSalesAdvance(purchaseSalesAdvance);
				staffInvoiceAdjustment.setAdjustedAmount(purchaseSalesAdvance.getAmount());
				staffInvoiceAdjustment.setAdvanceType(purchaseSalesAdvance.getAdvanceType());
			}
			staffInvoiceAdjustment.setEmpMaster(employeeMasterRepository.findOne(taBillpaymentDTO.getEmpID()));
			staffInvoiceAdjustmentRepository.save(staffInvoiceAdjustment);

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("<<<<<<<======tabillPaymentService createtabill method Ended=====>>" + taBillpaymentDTO);
			return baseDTO;
		} catch (Exception e) {
			log.info("<<<<<<<======tabillPaymentService createtabill method Ended with error=====>>");
			e.printStackTrace();
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			return null;
		}
	}

	public BaseDTO getAll(PaginationDTO paginationDTO) {
		log.info("<<====   TaBillService ---  getAll ====## STARTS");
		BaseDTO baseDTO = null;
		Integer totalRecords = 0;
		List<TaBillPaymentDTO> resultList = null;
		try {
			resultList = new ArrayList<TaBillPaymentDTO>();
			baseDTO = new BaseDTO();
			Integer start = paginationDTO.getFirst(), pageSize = paginationDTO.getPageSize();
			start = start * pageSize;
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();

			ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("TA_BILL_PAYMENT");
			String mainQuery = applicationQuery.getQueryContent();
			log.info("db query----------" + mainQuery);

			// mainQuery = mainQuery.replaceAll(":CHEQUE_TO_BANK",
			// AmountMovementType.CHEQUE_TO_BANK);
			// log.info("after replace query------" + mainQuery);

			if (paginationDTO.getFilters() != null) {
				if (paginationDTO.getFilters().get("status") != null) {
					mainQuery += " and upper(vl.status)=upper('" + paginationDTO.getFilters().get("status")+"')";
				}
				if (paginationDTO.getFilters().get("taBillType") != null) {
					mainQuery += " and upper(t1.ta_bill_type) like upper('%"
							+ paginationDTO.getFilters().get("taBillType") + "%')";
				}
				if (paginationDTO.getFilters().get("tourplancode") != null) {
					mainQuery += " and upper(tp.tour_name) like upper('%"
							+ paginationDTO.getFilters().get("tourplancode") + "%')";
				}
				if (paginationDTO.getFilters().get("claimname") != null) {
					mainQuery += " and upper(t1.claim_name) like upper('%" + paginationDTO.getFilters().get("claimname")
							+ "%')";
				}
				if (paginationDTO.getFilters().get("empCode") != null) {
					mainQuery += " and upper(concat(emp.emp_code,'/',emp.first_name,' ',emp.last_name)) like upper('%" + paginationDTO.getFilters().get("empCode")
							+ "%')";
				}
				if (paginationDTO.getFilters().get("voucher_no") != null) {
					mainQuery += " and upper(concat(v.reference_number_prefix,v.reference_number)) like upper('%"
							+ paginationDTO.getFilters().get("voucher_no") + "%')";
				}
				if (paginationDTO.getFilters().get("voucher_date") != null) {
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					Date fromDate = new Date((long) paginationDTO.getFilters().get("voucher_date"));
					log.info(" fromDate Filter : " + fromDate);
					mainQuery += " and date(v.created_date)::date='" + format.format(fromDate) + "'";
				}
				if (paginationDTO.getFilters().get("totclaimAmount") != null) {
					mainQuery +=" and t1.claim_travel_amount>= '" + paginationDTO.getFilters().get("totclaimAmount") + "'";
				}

			}

			String countQuery = mainQuery.replace(
					"select t1.id, t1.ta_bill_type,tp.tour_name,concat(v.reference_number_prefix,v.reference_number) as vouche_no,date(v.created_date) as voucher_date,v.id as voucher_id ,t1.claim_name,concat(emp.emp_code,'/',emp.first_name,' ',emp.last_name) as emp_code_name,t1.claim_travel_amount,t1.claim_contg_amount,vl.status",
					"select count(distinct(t1.id)) as count ");
			log.info("count query... " + countQuery);
			countData = jdbcTemplate.queryForList(countQuery);

			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					totalRecords = Integer.parseInt(data.get("count").toString().replace(",", ""));
			}

			if (paginationDTO.getSortField() != null && paginationDTO.getSortOrder() != null) {
				log.info("Sort Field:[" + paginationDTO.getSortField() + "] Sort Order:[" + paginationDTO.getSortOrder()
						+ "]");
				if (paginationDTO.getSortField().equals("taBillType")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by t1.ta_bill_type asc  ";
				if (paginationDTO.getSortField().equals("taBillType")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by t1.ta_bill_type desc  ";
				if (paginationDTO.getSortField().equals("tourplancode")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by tp.tour_name asc ";
				if (paginationDTO.getSortField().equals("tourplancode")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by tp.tour_name desc ";
				if (paginationDTO.getSortField().equals("claimname")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by t1.claim_name asc ";
				if (paginationDTO.getSortField().equals("claimname")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by t1.claim_name desc ";
				if (paginationDTO.getSortField().equals("empCode") && paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by emp_code_name ";
				if (paginationDTO.getSortField().equals("empCode") && paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by emp_code_name desc ";

				if (paginationDTO.getSortField().equals("voucher_no")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by vouche_no ";
				if (paginationDTO.getSortField().equals("voucher_no")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by vouche_no desc ";

				if (paginationDTO.getSortField().equals("voucher_date")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by voucher_date ";
				if (paginationDTO.getSortField().equals("voucher_date")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by voucher_date desc ";
				if (paginationDTO.getSortField().equals("totclaimAmount")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by t1.claim_travel_amount asc ";
				if (paginationDTO.getSortField().equals("totclaimAmount")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by t1.claim_travel_amount desc ";
				if (paginationDTO.getSortField().equals("status") && paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by status asc ";
				if (paginationDTO.getSortField().equals("status") && paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by status desc ";
				mainQuery += " limit " + pageSize + " offset " + start + ";";
			} else {

				mainQuery += " order by t1.created_date desc limit " + pageSize + " offset " + start + ";";
			}

			log.info("filter query----------" + mainQuery);

			listofData = jdbcTemplate.queryForList(mainQuery);
			log.info("amount transfer list size-------------->" + listofData.size());

			for (Map<String, Object> data : listofData) {
				TaBillPaymentDTO response = new TaBillPaymentDTO();

				response.setTaBillId(Long.valueOf(data.get("id").toString()));
				response.setVoucherid(Long.valueOf(data.get("voucher_id").toString()));
				response.setTaBillType(data.get("ta_bill_type").toString());
				response.setTourplancode(data.get("tour_name").toString());
				response.setTotclaimAmount(Double.valueOf(data.get("claim_travel_amount").toString()));
				if (data.get("claim_name") != null) {
					response.setClaimname(data.get("claim_name").toString());
				}
				response.setVoucher_no(data.get("vouche_no").toString());
				response.setVoucher_date((Date) data.get("voucher_date"));

				response.setEmpCode(data.get("emp_code_name").toString());
				response.setStatus(data.get("status").toString());

				VoucherNote vouchernote = voucherNoteRepository.findByVoucherId(response.getVoucherid());

				response.setVoucherNote(vouchernote);

				resultList.add(response);
			}

			log.info("final list size---------------" + resultList.size());
			baseDTO.setTotalRecords(totalRecords);
			baseDTO.setResponseContents(resultList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("<<====   ChequeToBankService ---  getAll ====## ENDS");
		} catch (Exception ex) {
			log.error("inside lazy method exception------");
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO getDetailsById(Long id) {
		log.info("<---TaBillService.getDetailsById() starts---->" + id);
		BaseDTO response = new BaseDTO();
		TaBillPaymentDTO paymentDto = new TaBillPaymentDTO();
		try {

			TaBill taBill = taBillRepository.findOne(id);

			paymentDto.setTaBillId(taBill.getId());
			paymentDto.setEmpID(taBill.getEmpMaster().getId());
			paymentDto
					.setEmpCodeName(taBill.getEmpMaster().getEmpCode() + " / " + taBill.getEmpMaster().getFirstName());
			paymentDto.setDesignation(taBill.getEmpMaster().getPersonalInfoEmployment().getDesignation().getName());
			if (taBill.getTourProgram().getEntityMaster() != null) {
				paymentDto.setEntityCodeName(taBill.getTourProgram().getEntityMaster().getCode().toString() + " / "
						+ taBill.getTourProgram().getEntityMaster().getName());
			}
			// Basic Pay
			GlAccount gelaccount = glAccountRepository.findByName(GlAccountName.BASIC_PAY);
			if (gelaccount != null && taBill.getEmpMaster() != null) {
				EmployeePayDetails employeePayDetail = employeePayDetailRepository
						.getLatestBasicPay(taBill.getEmpMaster().getId(), gelaccount.getId());
				paymentDto.setBasicPay(employeePayDetail.getAmount());
			}

			paymentDto.setClaimType(taBill.getTaBillType());
			paymentDto.setClaimNames(taBill.getClaimName());
			paymentDto.setTotalAmountClaimed(taBill.getClaimTravelAmount() + taBill.getClaimContgAmount());
			paymentDto.setClaimedDate(taBill.getCreatedDate());
			paymentDto.setTotalAmountApproved(taBill.getApprovedTravelAmount() + taBill.getApprovedContgAmount());
			TaBillLog tabilllog = taBillLogRepository.getTaBillLogByID(taBill.getId());
			paymentDto.setApprovedDate(tabilllog.getCreatedDate());
			paymentDto.setClaimTravell(taBill.getClaimTravelAmount());
			paymentDto.setClaimContegency(taBill.getClaimContgAmount());
			paymentDto.setApprovedtravell(taBill.getApprovedTravelAmount());
			paymentDto.setApprovedContegency(taBill.getApprovedContgAmount());
		
			paymentDto.setAmounttobepaid((paymentDto.getTotalAmountApproved() !=null ? paymentDto.getTotalAmountApproved() : 0D)- 
					(paymentDto.getAdvance() != null ? paymentDto.getAdvance() : 0D));

			Voucher voucher = voucherRepository.findOne(taBill.getVoucher().getId());
			VoucherNote voucherNote = voucherNoteRepository.findByVoucherId(taBill.getVoucher().getId());
			VoucherLog voucherLog = voucherlogRepository.findByVoucherId(taBill.getVoucher().getId());
			paymentDto.setPaymentmode(voucher.getPaymentMode());
			paymentDto.setVoucherlog(voucherLog);
			paymentDto.setVoucherNote(voucherNote);
			
			List<Map<String, Object>> employeeData = new ArrayList<Map<String, Object>>();
			if(voucher!=null) {
				log.info("<<<:::::::voucher::::not Null::::>>>>"+voucher!=null ? voucher.getId():"Null");
				 ApplicationQuery applicationQueryForlog = applicationQueryRepository.findByQueryName("VOUCHER_LOG_EMPLOYEE_DETAILS");
				 if (applicationQueryForlog == null || applicationQueryForlog.getId() == null) {
					 log.info("Application Query For Log Details not found for query name : " + applicationQueryForlog);
					 response.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
					 return response;
				 }
				 String logquery = applicationQueryForlog.getQueryContent().trim();
				 log.info("<=========VOUCHER_LOG_EMPLOYEE_DETAILS Query Content ======>"+logquery);
				 logquery = logquery.replace(":voucherId", "'"+voucher.getId().toString()+"'");
				 log.info("Query Content For VOUCHER_LOG_EMPLOYEE_DETAILS After replaced plan id View query : " + logquery);
				 employeeData = jdbcTemplate.queryForList(logquery);
				 log.info("<=========VOUCHER_LOG_EMPLOYEE_DETAILS Employee Data======>"+employeeData);
				 response.setTotalListOfData(employeeData);
			}
			
			
			
			response.setResponseContent(paymentDto);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException e) {
			log.error(" Rest Exception Occured ", e);
			response.setStatusCode(e.getStatusCode());
		} catch (Exception e) {
			log.info("Error while retriving getDetailsById----->", e);
			response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<---TaBillService.getDetailsById() end---->");
		return responseWrapper.send(response);
	}

	@Transactional
	public BaseDTO approvetabill(TaBillPaymentDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("approvetabillPayment method start=============>" + requestDTO.getTaBillId());
			TaBill tabill = taBillRepository.findOne(requestDTO.getTaBillId());
			Long f = tabill.getVoucher().getId();
			Voucher voucher = voucherRepository.findOne(tabill.getVoucher().getId());
			log.info("approvetabillPayment method start=============>" + voucher.getId());

			VoucherNote voucherNote = new VoucherNote();
			voucherNote.setVoucher(voucher);
			voucherNote.setFinalApproval(requestDTO.getVoucherNote().getFinalApproval());
			voucherNote.setNote(requestDTO.getVoucherNote().getNote());
			voucherNote.setForwardTo(userMasterRepositor.findOne(requestDTO.getVoucherNote().getForwardTo().getId()));
			voucherNoteRepository.save(voucherNote);
			log.info("approvetabill note inserted------");

			VoucherLog voucherLog = new VoucherLog();
			voucherLog.setVoucher(voucher);
			voucherLog.setStatus(requestDTO.getVoucherlog().getStatus());
			voucherLog.setUserMaster(userMasterRepositor.findOne(loginService.getCurrentUser().getId()));
			voucherLog.setCreatedBy(loginService.getCurrentUser());
			voucherLog.setCreatedByName(loginService.getCurrentUser().getUsername());
			voucherLog.setCreatedDate(new Date());
			voucherLog.setRemarks(requestDTO.getVoucherlog().getRemarks());
			voucherlogRepository.save(voucherLog);
			log.info("approvetabill log inserted------");

			log.info("Approval status----------" + requestDTO.getVoucherlog().getStatus());

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (LedgerPostingException l) {
			log.error("approvetabillPayment method Exception----", l);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("approvetabillPayment method exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO rejecttabill(TaBillPaymentDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("rejecttabill method start=============>" + requestDTO.getTaBillId());
			TaBill tabill = taBillRepository.findOne(requestDTO.getTaBillId());
			Voucher voucher = voucherRepository.findOne(tabill.getVoucher().getId());

			VoucherLog voucherLog = new VoucherLog();
			voucherLog.setVoucher(voucher);
			voucherLog.setStatus(requestDTO.getVoucherlog().getStatus());
			voucherLog.setCreatedBy(loginService.getCurrentUser());
			voucherLog.setUserMaster(userMasterRepositor.findOne(loginService.getCurrentUser().getId()));
			voucherLog.setUserMaster(userMasterRepository.findByUsername(loginService.getCurrentUser().getUsername()));
			voucherLog.setCreatedDate(new Date());
			voucherLog.setRemarks(requestDTO.getVoucherlog().getRemarks());
			voucherlogRepository.save(voucherLog);

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("rejecttabill  log inserted------");
		} catch (Exception e) {
			log.error("rejecttabill method exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO getadvance(Long id) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("getadvance method start=============>" + id);
			PurchaseSalesAdvance purchaseSalesAdvance = purchaseSalesAdvanceRepository.findByEmpIdandType(id,
					"Staff Tour Advance");
			if (purchaseSalesAdvance != null) {
				baseDTO.setResponseContent(purchaseSalesAdvance);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
			log.info("rejecttabill  log inserted------");
		} catch (Exception e) {
			log.error("rejecttabill method exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

}
