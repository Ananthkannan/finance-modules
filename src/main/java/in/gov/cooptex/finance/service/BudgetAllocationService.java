package in.gov.cooptex.finance.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.model.BudgetAllocation;
import in.gov.cooptex.core.accounts.model.BudgetAllocationDetails;
import in.gov.cooptex.core.accounts.model.BudgetConfig;
import in.gov.cooptex.core.accounts.model.BudgetConsolidation;
import in.gov.cooptex.core.accounts.model.BudgetConsolidationDetails;
import in.gov.cooptex.core.accounts.repository.GlAccountRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.finance.repository.BudgetAllocationDetailsRepository;
import in.gov.cooptex.core.finance.repository.BudgetAllocationRepository;
import in.gov.cooptex.core.finance.repository.BudgetConfigRepository;
import in.gov.cooptex.core.finance.repository.BudgetConsolidationDetailsRepository;
import in.gov.cooptex.core.finance.repository.BudgetConsolidationRepository;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.SectionMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.dto.BudgetAllocationDTO;
import in.gov.cooptex.finance.dto.BudgetAllocationExpenseDTO;
import in.gov.cooptex.finance.dto.BudgetRequestDTO;
import in.gov.cooptex.operation.dto.SalesInvoiceItemDTO;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class BudgetAllocationService {

	@Autowired
	BudgetAllocationRepository budgetAllocationRepository;
	
	@Autowired
	BudgetConfigRepository budgetConfigRepository;
	
	@Autowired
	EntityMasterRepository entityMasterRepository;
	
	@Autowired
	GlAccountRepository glAccountRepository;
	
	@Autowired
	EntityManager entityManager;
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	ResponseWrapper responseWrapper;
	
	@Autowired
	ApplicationQueryRepository applicationQueryRepository;
	
	@Autowired
	SectionMasterRepository sectionMasterRepository;
	
	@Autowired
	BudgetConsolidationRepository budgetConsolidationRepository;
	
	@Autowired
	BudgetConsolidationDetailsRepository budgetConsolidationDetailsRepository;

	@Autowired
	BudgetAllocationDetailsRepository budgetAllocationDetailsRepository;
	
	public BaseDTO getAllAllocationListLazy(PaginationDTO paginationDTO) {
		log.info("<====Starts BudgetAllocationService.getAllAllocationListLazy======>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			
			Session session = entityManager.unwrap(Session.class);
			Criteria criteria = session.createCriteria(BudgetAllocation.class,"budgetAllocation");
			
			criteria.createAlias("budgetAllocation.entityMaster","entityMaster");
			criteria.createAlias("budgetAllocation.sectionMaster","sectionMaster",Criteria.LEFT_JOIN);
			criteria.createAlias("budgetAllocation.budgetConfig","budgetConfig");
			
			String filter="";
			log.info("Filters=========>>>"+paginationDTO.getFilters());
			if(paginationDTO.getFilters() != null) {
				
				if(paginationDTO.getFilters().get("budgetConfig.name") != null && !paginationDTO.getFilters().get("budgetConfig.name").toString().trim().isEmpty()) {
					filter =  paginationDTO.getFilters().get("budgetConfig.name").toString().trim();
					Criterion name = Restrictions.like("budgetConfig.name","%"+filter.trim()+"%").ignoreCase();
					Criterion code = Restrictions.like("budgetConfig.code","%"+filter.trim()+"%").ignoreCase();
					criteria.add(Restrictions.or(name,code));
				}
				
				if(paginationDTO.getFilters().get("entityMaster.name") != null && !paginationDTO.getFilters().get("entityMaster.name").toString().trim().isEmpty()) {
					filter =  paginationDTO.getFilters().get("entityMaster.name").toString().trim();
					if(AppUtil.isInteger(filter))
						criteria.add(Restrictions.eq("entityMaster.code",Integer.parseInt(filter)));
					else
						criteria.add(Restrictions.like("entityMaster.name","%"+filter.trim()+"%").ignoreCase());
				}
				
				if(paginationDTO.getFilters().get("sectionMaster.name") != null && !paginationDTO.getFilters().get("sectionMaster.name").toString().trim().isEmpty()) {
					filter =  paginationDTO.getFilters().get("sectionMaster.name").toString().trim();
					Criterion name = Restrictions.like("sectionMaster.name","%"+filter.trim()+"%").ignoreCase();
					Criterion code = Restrictions.like("sectionMaster.code","%"+filter.trim()+"%").ignoreCase();
					criteria.add(Restrictions.or(name,code));
				}
				
				if(paginationDTO.getFilters().get("createdDate") != null) {
					Date date = new Date((long) paginationDTO.getFilters().get("createdDate"));
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = dateFormat.format(date);
					Date minDate = dateFormat.parse(strDate);
					Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
					criteria.add(Restrictions.conjunction().add(Restrictions.ge("budgetAllocation.createdDate", minDate))
							.add(Restrictions.lt("budgetAllocation.createdDate", maxDate)));
				}
				
			}
			criteria.setProjection(Projections.rowCount());
			int total =Integer.parseInt((criteria.uniqueResult()).toString());
			log.info("total records:::::::::::"+total);
			criteria.setProjection(null);
			
			ProjectionList projectionList = Projections.projectionList();
			projectionList.add(Projections.property("budgetAllocation.id"));
			projectionList.add(Projections.property("budgetAllocation.sectionMaster"));
			projectionList.add(Projections.property("budgetAllocation.entityMaster"));
			projectionList.add(Projections.property("budgetAllocation.budgetConfig"));
			projectionList.add(Projections.property("budgetAllocation.createdDate"));
			
			criteria.setProjection(projectionList);
			
			criteria.setFirstResult(paginationDTO.getFirst()*paginationDTO.getPageSize());
			criteria.setMaxResults(paginationDTO.getPageSize());
			
			if(paginationDTO.getSortField()!= null && paginationDTO.getSortOrder() != null) {
				if(paginationDTO.getSortOrder().equals("ASCENDING"))
					criteria.addOrder(Order.asc(paginationDTO.getSortField()));
				else
					criteria.addOrder(Order.desc(paginationDTO.getSortField()));
			}else {
				criteria.addOrder(Order.desc("budgetAllocation.id"));
			}
			
			List<?> list = criteria.list();
			if(list == null || list.size() ==0)
				log.info("BudgetAllocation list is empty or null");
			
			List<BudgetAllocation> budgetAllocationList = new ArrayList<>();
			
			Iterator<?> iterator = list.iterator();
			while(iterator.hasNext()) {
				Object obj[] = (Object [])iterator.next();
				BudgetAllocation b = new BudgetAllocation();
				b.setId((Long)obj[0]);
				b.setSectionMaster((SectionMaster)obj[1]);
				b.setEntityMaster((EntityMaster)obj[2]);
				b.setBudgetConfig((BudgetConfig)obj[3]);
				b.setCreatedDate((Date)obj[4]);
				budgetAllocationList.add(b);
			}
			
			baseDTO.setResponseContent(budgetAllocationList);
			baseDTO.setTotalRecords(total);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		}catch(Exception e) {
			log.error("<=== Exception occured in BudgetAllocationService.getAllAllocationListLazy ======>",e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("<====Ends BudgetAllocationService.getAllAllocationListLazy======>");
		return baseDTO;
	}
	
	
	@Transactional
	public BaseDTO createOrEditBudgetAllocation(BudgetAllocationDTO request) {
		log.info("<====Ends BudgetAllocationService.createBudgetAllocation======>"+request);
		BaseDTO baseDTO = new BaseDTO();
		try {
			
			if(request.getId() == null)
				inputValidation(request);
			
			if(request.getExpenseDtoList() == null || request.getExpenseDtoList().size() == 0) {
				throw  new RestException(ErrorDescription.BUDGET_ALLOCATION_DETAILS_REQUIRED);
			}
			
			BudgetAllocation budgetAllocation = new BudgetAllocation();
			if(request.getId() ==null) {
			
				budgetAllocation.setBudgetConfig(budgetConfigRepository.findOne(request.getBudgetConfig().getId()));
				
				if(request.getBudgetFor() == null || request.getBudgetFor() == "") {
					log.info("budget for null condtion-----");
					budgetAllocation.setEntityMaster(entityMasterRepository.findOne(request.getEntityMasterId()));
				}else {
					log.info("budget for not null condtion-----");
					if(request.getBudgetFor().equals("HeadOffice")) {
						budgetAllocation.setEntityMaster(entityMasterRepository.findOne(request.getEntityMasterId()));
						budgetAllocation.setSectionMaster(sectionMasterRepository.findOne(request.getSectionMaster().getId()));
					}
					else if(request.getBudgetFor().equals("RegionalOffice")) {
						budgetAllocation.setEntityMaster(entityMasterRepository.findOne(request.getEntityMaster().getId()));
					}
					else {
						if(request.getShowRoomEntity()!=null && request.getShowRoomEntity().getId() !=null) {
							budgetAllocation.setEntityMaster(
									entityMasterRepository.findOne(request.getShowRoomEntity().getId()));
						}
					}
				}
				
			}else {
				log.info("Allocation id ::::::::::"+request.getId());
				budgetAllocation = budgetAllocationRepository.findOne(request.getId());
				Validate.objectNotNull(budgetAllocation,ErrorDescription.BUDGET_ALLOCATION_NOT_FOUND);
				
				List<BudgetAllocationDetails> allocationDetails = budgetAllocationDetailsRepository.findByAllocationId(request.getId());
				budgetAllocationDetailsRepository.delete(allocationDetails);
			}
			
			List<BudgetAllocationDetails> detailsList = new ArrayList<>();
			
			for(BudgetAllocationExpenseDTO dto : request.getExpenseDtoList()) {
		
				double sumPercent = dto.getQ1Percent()+dto.getQ2Percent()+dto.getQ3Percent()+dto.getQ4Percent();
				log.info("sum of glName ::::::"+dto.getGlName()+"\t is :::"+sumPercent);
				if(sumPercent > 100 || sumPercent<100) {
					log.info("Percentage should not exceed 100 %");
					throw new RestException(10210);
				}
				
				for(int i=1;i<5;i++) {
					BudgetAllocationDetails detail = new BudgetAllocationDetails();
					detail.setBudgetAllocation(budgetAllocation);
					detail.setGlAccount(glAccountRepository.findOne(dto.getGlId()));
					detail.setAllocationType("QUARTELY");
					if(i==1) {
						detail.setAllocationAmount(dto.getQ1Amount());
						detail.setAllocationFromMonth(dto.getQ1FromMonth());
						detail.setAllocationFromYear(Long.parseLong(dto.getQ1FromYear()));
						detail.setAllocationToMonth(dto.getQ1ToMonth());
						detail.setAllocationToYear(Long.parseLong(dto.getQ1ToYear()));
						detail.setAllocationPercentage(dto.getQ1Percent());
					}else if(i==2) {
						detail.setAllocationAmount(dto.getQ2Amount());
						detail.setAllocationFromMonth(dto.getQ2FromMonth());
						detail.setAllocationFromYear(Long.parseLong(dto.getQ2FromYear()));
						detail.setAllocationToMonth(dto.getQ2ToMonth());
						detail.setAllocationToYear(Long.parseLong(dto.getQ2ToYear()));
						detail.setAllocationPercentage(dto.getQ2Percent());
					}else if(i==3) {
						detail.setAllocationAmount(dto.getQ3Amount());
						detail.setAllocationFromMonth(dto.getQ3FromMonth());
						detail.setAllocationFromYear(Long.parseLong(dto.getQ3FromYear()));
						detail.setAllocationToMonth(dto.getQ3ToMonth());
						detail.setAllocationToYear(Long.parseLong(dto.getQ3ToYear()));
						detail.setAllocationPercentage(dto.getQ3Percent());
					}else if(i==4) {
						detail.setAllocationAmount(dto.getQ4Amount());
						detail.setAllocationFromMonth(dto.getQ4FromMonth());
						detail.setAllocationFromYear(Long.parseLong(dto.getQ4FromYear()));
						detail.setAllocationToMonth(dto.getQ4ToMonth());
						detail.setAllocationToYear(Long.parseLong(dto.getQ4ToYear()));
						detail.setAllocationPercentage(dto.getQ4Percent());
					}
					detailsList.add(detail);
				}
			}
			budgetAllocation.setBudgetAllocationDetailsList(detailsList);
			budgetAllocationRepository.save(budgetAllocation);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		}catch(RestException re) {
			log.error("Exception occured in BudgetAllocationService.createOrEditBudgetAllocation.................",re);
			baseDTO.setStatusCode(re.getStatusCode());
		}catch(ObjectOptimisticLockingFailureException lockEx) {
			log.error("ObjectOptimisticLockingFailureException occured in BudgetAllocationService.createOrEditBudgetAllocation.................",lockEx);
			baseDTO.setStatusCode(ErrorDescription.CANNOT_UPDATE_LOCKED_RECORD.getCode());
		}catch(Exception e) {
			log.error("Exception occured in BudgetAllocationService.createOrEditBudgetAllocation.................",e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("<====Ends BudgetAllocationService.createBudgetAllocation======>");
		return baseDTO;
	}
	
	
	public void inputValidation(BudgetAllocationDTO dto) {
		log.info("<==== Starts BudgetAllocationService.inputValidation ===>"+dto);
		
		Validate.objectNotNull(dto.getBudgetConfig(),ErrorDescription.BUDGET_CONFIG_REQUIRED);
		Validate.objectNotNull(dto.getBudgetConfig().getId(),ErrorDescription.BUDGET_CONFIG_REQUIRED);
		
		if(dto.getBudgetFor()!=null) {
			if(dto.getBudgetFor().equalsIgnoreCase("HeadOffice")) {
				Validate.objectNotNull(dto.getSectionMaster(),ErrorDescription.BUDGET_SECTION_REQUIRED);	
			}else if(dto.getBudgetFor().equalsIgnoreCase("RegionalOffice")){
				Validate.objectNotNull(dto.getEntityMaster(),ErrorDescription.BUDGET_ENTITY_REQUIRED);
			}
		}
		
		log.info("<==== Ends BudgetAllocationService.inputValidation ===>");
		
	}
	
	
	public BaseDTO generateBudgetAllocation(BudgetAllocationDTO request) {
		log.info("<==== Starts BudgetAllocationService.generateBudgetAllocation ===>"+request);
		BaseDTO baseDTO = new BaseDTO();
		Long entityId = null;
		Long sectionId = null;
		try {
			inputValidation(request);
			
			if(request.getEntityMaster() != null && request.getEntityMaster().getId() != null){
				entityId = request.getEntityMaster().getId();
				log.info("generateBudgetAllocation entityId----------->"+entityId);
			}
			
			if(request.getSectionMaster() != null && request.getSectionMaster().getId() != null) {
				sectionId = request.getSectionMaster().getId();
				log.info("generateBudgetAllocation sectionId----------->"+sectionId);
			}
				
			
			BudgetAllocation allocation = budgetAllocationRepository.findByConfigAndEntityAndSection(
					request.getBudgetConfig().getId(),entityId,sectionId);
			if( allocation != null)
				throw new RestException(ErrorDescription.BUDGET_ALLOCATION_EXIST);
			
			List<BudgetAllocationExpenseDTO> allocationExpenseDtoList = executeGenerateBudgetAllocationQuery(request);
			baseDTO.setResponseContent(allocationExpenseDtoList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		}catch(RestException re) {
			log.error("Exception occured in BudgetAllocationService.createOrEditBudgetAllocation.................",re);
			baseDTO.setStatusCode(re.getStatusCode());
		}catch(Exception e) {
			log.error("Exception occured in BudgetAllocationService.createOrEditBudgetAllocation.................",e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("<==== Ends BudgetAllocationService.generateBudgetAllocation ===>");
		return baseDTO;
	}
	
	
	public List<BudgetAllocationExpenseDTO> executeGenerateBudgetAllocationQuery(BudgetAllocationDTO request){
		log.info("Starts BudgetRequestService.executeBudgetExpenseQuery ");
		log.info("ConfigId::::::"+request.getBudgetConfig().getId());
		ApplicationQuery applicationQuery = applicationQueryRepository.findByQueryName("BUDGET_ALLOCATION_GENERATE_QUERY");

		if (applicationQuery == null || applicationQuery.getId() == null) {
			log.info("Application Query not found for query name : " + applicationQuery.getQueryName());
			return null;
		}

		String query = applicationQuery.getQueryContent().trim();

		log.info("Query " + query);
		
		query = applicationQuery.getQueryContent().trim();
		
		String whereQuery="";
		
		if(request.getSectionMaster() != null && request.getSectionMaster().getId() != null)
			whereQuery = whereQuery+" AND bcd.section_id ="+request.getSectionMaster().getId()+" ";
		
		if(request.getEntityMaster() != null && request.getEntityMaster().getId() != null)
			whereQuery = whereQuery+" AND bcd.entity_id="+request.getEntityMaster().getId()+" ";
		
		if(request.getBudgetConfig() != null && request.getBudgetConfig().getId() != null)
			whereQuery = whereQuery+" AND bc.budget_config_id="+request.getBudgetConfig().getId()+" ";
		
		query = query.replace(":CONDITION",whereQuery);
		 
		log.info("after replace Query==>" + query);
		
		List<BudgetAllocationExpenseDTO>  expenseList = jdbcTemplate.query(query, new RowMapper<BudgetAllocationExpenseDTO>() {

			@Override
			public BudgetAllocationExpenseDTO mapRow(ResultSet rs, int no) throws SQLException {
				BudgetAllocationExpenseDTO dto = new BudgetAllocationExpenseDTO();
				
				dto.setGlId(rs.getLong("GL_ID"));

				dto.setGlName(rs.getString("GL_NAME"));
				
				dto.setExpenseCategory(rs.getString("EXPENSE_CATEGORY"));
				
				dto.setEstimationAmt(rs.getDouble("BUDGET_AMOUNT"));
				
				dto.setRemainingAmount(rs.getDouble("BUDGET_AMOUNT"));
				
				dto.setEntityId(rs.getLong("ENTITY_ID"));
				
				String arr[] = rs.getString("CONFIG_FROM").split("-");
				Integer month = Integer.parseInt(arr[0]);
				Integer year = Integer.parseInt(arr[1]);
				for(int i=1;i<=4;i++) {
					
					if(i==1) {
						log.info("Q1 from month-year ::"+month+"-"+year);
						if(month>12) {
							month = month-12;
							dto.setQ1FromMonth(month.toString());
						}else {
							dto.setQ1FromMonth(month.toString());
						}
						log.info("Q1 from month------1--------->"+dto.getQ1FromMonth());
						dto.setQ1FromYear(year.toString());
						month = month+2;
						if(month>12) {
							month = month-12;
							dto.setQ1ToMonth(month+"");
							dto.setQ1ToYear((year+1)+"");
							year+=1;
						}else {
							dto.setQ1ToMonth(month+"");
							dto.setQ1ToYear(year+"");
						}
						month = month+1;
						log.info("Q1 from month-year ::"+month+"-"+year);
					}else if(i==2) {
						log.info("Q2 from month-year ::"+month+"-"+year);
						if(month>12) {
							month = month-12;
							dto.setQ2FromMonth(month.toString());
						}else {
							dto.setQ2FromMonth(month.toString());
						}
						log.info("Q2 from month------2--------->"+dto.getQ2FromMonth());
						dto.setQ2FromYear(year.toString());
						month = month+2;
						if(month>12) {
							month = month-12;
							dto.setQ2ToMonth(month+"");
							dto.setQ2ToYear((year+1)+"");
							year+=1;
						}else {
							dto.setQ2ToMonth(month+"");
							dto.setQ2ToYear(year+"");
						}
						month = month+1;
						log.info("Q2 from month-year ::"+month+"-"+year);
					}else if(i==3) {
						log.info("Q3 from month-year ::"+month+"-"+year);
						if(month>12) {
							month = month-12;
							dto.setQ3FromMonth(month.toString());
						}else {
							dto.setQ3FromMonth(month.toString());
						}
						log.info("Q3 from month------3--------->"+dto.getQ3FromMonth());
						dto.setQ3FromYear(year.toString());
						month = month+2;
						if(month>12) {
							month = month-12;
							dto.setQ3ToMonth(month+"");
							dto.setQ3ToYear((year+1)+"");
							year+=1;
						}else {
							dto.setQ3ToMonth(month+"");
							dto.setQ3ToYear(year+"");
						}
						month = month+1;
						log.info("Q3 from month-year ::"+month+"-"+year);
					}else if(i==4) {
						log.info("Q4 from month-year ::"+month+"-"+year);
						if(month>12) {
							month = month-12;
							dto.setQ4FromMonth(month.toString());
						}else {
							dto.setQ4FromMonth(month.toString());
						}
						log.info("Q4 from month------4--------->"+dto.getQ4FromMonth());
						dto.setQ4FromYear(year.toString());
						month = month+2;
						if(month>12) {
							month = month-12;
							dto.setQ4ToMonth(month+"");
							dto.setQ4ToYear((year+1)+"");
							year+=1;
						}else {
							dto.setQ4ToMonth(month+"");
							dto.setQ4ToYear(year+"");
						}
						month = month+1;
						log.info("Q4 from month-year ::"+month+"-"+year);
					}
				}
				return dto;
			}
		});
		
		log.info("Ends BudgetRequestService.executeBudgetExpenseQuery ");
		
		return expenseList;
	}
	
	
	public BaseDTO getById(Long id) {
		log.info("Starts BudgetRequestService.getById "+id);
		BaseDTO baseDTO = new BaseDTO();
		try {
			Validate.objectNotNull(id,ErrorDescription.BUDGET_ALLOCATION_ID_REQUIRED);
			BudgetAllocation allocation = budgetAllocationRepository.findOne(id);
			Validate.objectNotNull(allocation,ErrorDescription.BUDGET_ALLOCATION_NOT_FOUND);
			
			BudgetAllocationDTO response = new BudgetAllocationDTO();
			response.setId(allocation.getId());
			response.setBudgetConfig(allocation.getBudgetConfig());
			response.setEntityMaster(allocation.getEntityMaster());
			if(allocation.getSectionMaster() != null)
				response.setSectionMaster(allocation.getSectionMaster());		
			
			Map<Long,List<BudgetAllocationDetails>> glGroupExpenses = 
					allocation.getBudgetAllocationDetailsList().stream().collect(Collectors.groupingBy(s->s.getGlAccount().getId()));
			
			List<BudgetAllocationExpenseDTO> expenseList = new ArrayList<>();
			for (Map.Entry<Long,List<BudgetAllocationDetails>> entry : glGroupExpenses.entrySet()) {
				
				
				BudgetAllocationExpenseDTO dto = new BudgetAllocationExpenseDTO();
				dto.setEstimationAmt(entry.getValue().stream().mapToDouble(s->s.getAllocationAmount()).sum());
				dto.setGlId(entry.getValue().get(0).getGlAccount().getId());
				dto.setGlName(entry.getValue().get(0).getGlAccount().getName());
				dto.setExpenseCategory(entry.getValue().get(0).getGlAccount().getExpenseCategory());
				dto.setAllocatedAmount(entry.getValue().stream().mapToDouble(s->s.getAllocationAmount()).sum());
				int i=1;
				for(BudgetAllocationDetails de : entry.getValue()) {
					log.info("i::::::::::::::"+i);
					if(i==1) {
						log.info("i==1::::::::::::::");
						dto.setQ1Amount(de.getAllocationAmount());
						dto.setQ1Percent(de.getAllocationPercentage());
						dto.setQ1FromMonth(de.getAllocationFromMonth());
						dto.setQ1FromYear(de.getAllocationFromYear().toString());
						dto.setQ1ToMonth(de.getAllocationToMonth());
						dto.setQ1ToYear(de.getAllocationToYear().toString());
					}else if(i == 2) {
						log.info("i==2::::::::::::::");
						dto.setQ2Amount(de.getAllocationAmount());
						dto.setQ2Percent(de.getAllocationPercentage());
						dto.setQ2FromMonth(de.getAllocationFromMonth());
						dto.setQ2FromYear(de.getAllocationFromYear().toString());
						dto.setQ2ToMonth(de.getAllocationToMonth());
						dto.setQ2ToYear(de.getAllocationToYear().toString());
					}else if(i == 3) {
						log.info("i==3::::::::::::::");
						dto.setQ3Amount(de.getAllocationAmount());
						dto.setQ3Percent(de.getAllocationPercentage());
						dto.setQ3FromMonth(de.getAllocationFromMonth());
						dto.setQ3FromYear(de.getAllocationFromYear().toString());
						dto.setQ3ToMonth(de.getAllocationToMonth());
						dto.setQ3ToYear(de.getAllocationToYear().toString());
					}else if(i == 4) {
						log.info("i==4::::::::::::::");
						dto.setQ4Amount(de.getAllocationAmount());
						dto.setQ4Percent(de.getAllocationPercentage());
						dto.setQ4FromMonth(de.getAllocationFromMonth());
						dto.setQ4FromYear(de.getAllocationFromYear().toString());
						dto.setQ4ToMonth(de.getAllocationToMonth());
						dto.setQ4ToYear(de.getAllocationToYear().toString());
					}
					i++;
				}
				expenseList.add(dto);
			}
			response.setExpenseDtoList(expenseList);
			baseDTO.setResponseContent(response);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		}catch(RestException re) {
			log.error("Exception occured in BudgetAllocationService.createOrEditBudgetAllocation.................",re);
			baseDTO.setStatusCode(re.getStatusCode());
		}catch(Exception e) {
			log.error("Exception occured in BudgetAllocationService.createOrEditBudgetAllocation.................",e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("Ends BudgetRequestService.getById ");
		
		return baseDTO;
	}	
}


