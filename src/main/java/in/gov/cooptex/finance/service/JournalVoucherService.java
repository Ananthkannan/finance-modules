package in.gov.cooptex.finance.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.service.ApproveRejectCommentsCommonService;
import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.model.CashCreditLoanLimit;
import in.gov.cooptex.core.accounts.model.EntityBankBranch;
import in.gov.cooptex.core.accounts.model.GlAccount;
import in.gov.cooptex.core.accounts.model.JournalVoucher;
import in.gov.cooptex.core.accounts.model.JournalVoucherEntries;
import in.gov.cooptex.core.accounts.model.JournalVoucherLog;
import in.gov.cooptex.core.accounts.model.JournalVoucherNote;
import in.gov.cooptex.core.accounts.model.Payment;
import in.gov.cooptex.core.accounts.model.PaymentDetails;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.VoucherDetails;
import in.gov.cooptex.core.accounts.model.VoucherLog;
import in.gov.cooptex.core.accounts.model.VoucherNote;
import in.gov.cooptex.core.accounts.model.VoucherType;
import in.gov.cooptex.core.accounts.model.WeaverBenefitPayment;
import in.gov.cooptex.core.accounts.repository.GlAccountRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.enums.ApprovalStatus;
import in.gov.cooptex.core.enums.PaymentCategory;
import in.gov.cooptex.core.enums.SequenceName;
import in.gov.cooptex.core.finance.repository.JournalVoucherEntriesRepository;
import in.gov.cooptex.core.finance.repository.JournalVoucherLogRepository;
import in.gov.cooptex.core.finance.repository.JournalVoucherNoteRepository;
import in.gov.cooptex.core.finance.repository.JournalVoucherRepository;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.SequenceConfig;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.EmployeePersonalInfoEmploymentRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.SequenceConfigRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.LedgerPostingException;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.finance.JournalVoucherDTO;
import in.gov.cooptex.finance.dto.WeaversBenefitPaymentDTO;
import in.gov.cooptex.operation.model.SupplierMaster;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class JournalVoucherService {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	GlAccountRepository glAccountRepository;

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	LoginService loginService;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	@Autowired
	ApproveRejectCommentsCommonService approveRejectCommentsCommonService;

	@Autowired
	JournalVoucherRepository journalvoucherRepository;

	@Autowired
	JournalVoucherEntriesRepository journalVoucherEntriesRepository;

	@Autowired
	JournalVoucherLogRepository journalVoucherLogRepository;

	@Autowired
	JournalVoucherNoteRepository journalVoucherNoteRepository;

	@Autowired
	SequenceConfigRepository sequenceConfigRepository;

	@Autowired
	UserMasterRepository userMasterRepositor;

	@Autowired
	ResponseWrapper responseWrapper;

	@Autowired
	EmployeePersonalInfoEmploymentRepository employeePersonalInfoEmploymentRepository;

	public BaseDTO getAllGlAccounts() {
		log.info("JournalVoucherService:getAllGlAccounts()");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<GlAccount> glAccountList = glAccountRepository.findActiveGlAccount();
			baseDTO.setResponseContent(glAccountList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {

			log.error("JournalVoucherService getAllGlAccounts RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {

			log.error("JournalVoucherService getAllGlAccounts Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}

		log.info("JournalVoucherService getAllGlAccounts method completed");
		return baseDTO;
	}

	public BaseDTO createjournalVoucher(JournalVoucherDTO journalVoucherDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {

			log.info("<<<<<<<======JournalVoucherService createjournalVoucher method Started=====>>"
					+ journalVoucherDTO);
			Long voucher_id;

			String refNumberPrefix = "";

			Long sequence_value = null;

			JournalVoucher jvoucher = new JournalVoucher();
			EntityMaster loginUserEntity = entityMasterRepository
					.findByUserRegion(loginService.getCurrentUser().getId());
			if (journalVoucherDTO.getJournalVoucher().getId() != null) {
				jvoucher = journalvoucherRepository.findOne(journalVoucherDTO.getJournalVoucher().getId());
				JournalVoucherEntries jvoucherEntries = new JournalVoucherEntries();
				for (JournalVoucherEntries jventry : journalVoucherDTO.getJournalVoucherEntries()) {
					jvoucherEntries = new JournalVoucherEntries();
					if (jventry.getId() != null) {
						jvoucherEntries = journalVoucherEntriesRepository.findOne(jventry.getId());
					}
					GlAccount glaccount = glAccountRepository.findOne(jventry.getGlAccount().getId());
					jvoucherEntries.setGlAccount(glaccount);
					jvoucherEntries.setJournalVoucher(jvoucher);
					jvoucherEntries.setJvAmount(jventry.getJvAmount());
					jvoucherEntries.setJvAspect(jventry.getJvAspect());
					jvoucherEntries = journalVoucherEntriesRepository.save(jvoucherEntries);
				}

				if (journalVoucherDTO.getRemovejournalVoucherEntries().size() > 0
						&& journalVoucherDTO.getRemovejournalVoucherEntries() != null) {
					for (JournalVoucherEntries rmvjventry : journalVoucherDTO.getRemovejournalVoucherEntries()) {
						journalVoucherEntriesRepository.delete(rmvjventry.getId());
					}
				}

				JournalVoucherLog journalVoucherLog = new JournalVoucherLog();
				journalVoucherLog.setJournalVoucher(jvoucher);
				journalVoucherLog.setStage(ApprovalStage.SUBMITTED);
				journalVoucherLogRepository.save(journalVoucherLog);

				JournalVoucherNote journalVouchernote = new JournalVoucherNote();
				journalVouchernote.setFinalApproval(journalVoucherDTO.getJournalVoucherNote().getFinalApproval());
				journalVouchernote.setNote(journalVoucherDTO.getJournalVoucherNote().getNote());
				journalVouchernote.setJournalVoucher(jvoucher);
				UserMaster frwdto = userMasterRepositor
						.findOne(journalVoucherDTO.getJournalVoucherNote().getUserMaster().getId());
				journalVouchernote.setUserMaster(frwdto);
				journalVoucherNoteRepository.save(journalVouchernote);
			} else {

				List<JournalVoucher> jvList = journalvoucherRepository
						.findByVoucherRefNo(journalVoucherDTO.getJournalVoucher().getName());

				if (jvList != null && jvList.size() > 0) {
					baseDTO.setStatusCode(MastersErrorCode.DUPLICATE_JOURNAL_VOUCHER_PRIFIXNO);
					return baseDTO;
				}
				SequenceConfig sequenceConfig = sequenceConfigRepository
						.findBySequenceName(SequenceName.valueOf(SequenceName.JOURNAL_VOUCHER.name()));
				if (sequenceConfig == null) {
					throw new RestException(ErrorDescription.SEQUENCE_CONFIG_NOT_EXIST);
				}
				refNumberPrefix = loginUserEntity.getCode() + sequenceConfig.getSeparator() + sequenceConfig.getPrefix()
						+ AppUtil.getCurrentYearString() + AppUtil.getCurrentMonthString()
						+ sequenceConfig.getCurrentValue();
				sequence_value = sequenceConfig.getCurrentValue();
				log.info("Voucher Number Prefix  ", refNumberPrefix);
				jvoucher.setJvNumberPrefix(refNumberPrefix);
				jvoucher.setRefNumber(refNumberPrefix);
				jvoucher.setJvNumber(sequenceConfig.getCurrentValue());
				jvoucher.setJvAmount(journalVoucherDTO.getJournalVoucher().getJvAmount());
				jvoucher.setJvDate(journalVoucherDTO.getJournalVoucher().getJvDate());
				jvoucher.setJvNarration(journalVoucherDTO.getJournalVoucher().getJvNarration());
				jvoucher.setEntityMaster(loginUserEntity);
				jvoucher.setName(journalVoucherDTO.getJournalVoucher().getName());
				JournalVoucher jvoucherObj = journalvoucherRepository.save(jvoucher);

				sequenceConfig.setCurrentValue(sequenceConfig.getCurrentValue() + 1);

				sequenceConfigRepository.save(sequenceConfig);

				JournalVoucherEntries jvoucherEntries = new JournalVoucherEntries();
				for (JournalVoucherEntries jventry : journalVoucherDTO.getJournalVoucherEntries()) {
					jvoucherEntries = new JournalVoucherEntries();
					if (jventry.getId() != null) {
						jvoucherEntries = journalVoucherEntriesRepository.findOne(jventry.getId());
					}
					GlAccount glaccount = glAccountRepository.findOne(jventry.getGlAccount().getId());
					jvoucherEntries.setGlAccount(glaccount);
					jvoucherEntries.setJournalVoucher(jvoucherObj);
					jvoucherEntries.setJvAmount(jventry.getJvAmount());
					jvoucherEntries.setJvAspect(jventry.getJvAspect());
					journalVoucherEntriesRepository.save(jvoucherEntries);
				}

				if (journalVoucherDTO.getRemovejournalVoucherEntries().size() > 0
						&& journalVoucherDTO.getRemovejournalVoucherEntries() != null) {
					for (JournalVoucherEntries rmvjventry : journalVoucherDTO.getRemovejournalVoucherEntries()) {
						journalVoucherEntriesRepository.delete(rmvjventry.getId());
					}
				}

				JournalVoucherLog journalVoucherLog = new JournalVoucherLog();
				JournalVoucherNote journalVouchernote = new JournalVoucherNote();

				if (journalVoucherDTO.getSkippApproval().equalsIgnoreCase("NO")) {
					journalVoucherLog.setJournalVoucher(jvoucherObj);
					journalVoucherLog.setStage(ApprovalStage.SUBMITTED);
					journalVoucherLogRepository.save(journalVoucherLog);

					journalVouchernote.setFinalApproval(journalVoucherDTO.getJournalVoucherNote().getFinalApproval());
					journalVouchernote.setNote(journalVoucherDTO.getJournalVoucherNote().getNote());
					journalVouchernote.setJournalVoucher(jvoucherObj);
					UserMaster frwdto = userMasterRepositor
							.findOne(journalVoucherDTO.getJournalVoucherNote().getUserMaster().getId());
					journalVouchernote.setUserMaster(frwdto);
					journalVoucherNoteRepository.save(journalVouchernote);
				} else {

					journalVoucherLog.setJournalVoucher(jvoucherObj);
					journalVoucherLog.setStage(ApprovalStage.FINALAPPROVED);
					journalVoucherLogRepository.save(journalVoucherLog);

					journalVouchernote.setFinalApproval(true);
					journalVouchernote.setNote(journalVoucherDTO.getJournalVoucherNote().getNote());
					journalVouchernote.setJournalVoucher(jvoucherObj);
					UserMaster frwdto = userMasterRepositor.findOne(loginService.getCurrentUser().getId());
					journalVouchernote.setUserMaster(frwdto);
					journalVoucherNoteRepository.save(journalVouchernote);
				}
			}

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("<<<<<<<======JournalVoucherService createjournalVoucher method Ended=====>>" + jvoucher);
			return baseDTO;
		} catch (Exception e) {
			log.info("<<<<<<<======JournalVoucherService createjournalVoucher method Ended with error=====>>");
			e.printStackTrace();
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			return null;
		}

	}

	public BaseDTO getjournalvoucherlist(PaginationDTO paginationDTO) {
		BaseDTO baseDTO = new BaseDTO();
		List<JournalVoucherDTO> responseDtoList = new ArrayList<JournalVoucherDTO>();
		try {
			log.info("<<<< ------- Start JournalVoucherService.getjournalvoucherlist ---------- >>>>>>>");
			Long voucherid;
			log.info(":: JournalVoucher search started ::");
			Session session = entityManager.unwrap(Session.class);
			Criteria criteria = session.createCriteria(JournalVoucher.class, "jv");
			criteria.createAlias("jv.entityMaster", "jventity");

			UserMaster userMaster = loginService.getCurrentUser();
			Long loginUserId = userMaster == null ? null : userMaster.getId();

			Long workLocationId = employeePersonalInfoEmploymentRepository.findEntityId(loginUserId);

			if (workLocationId != null) {
				// criteria.add(Restrictions.eq("jventity.entityMasterRegion.id", null));
				// criteria.add(Restrictions.eq("jventity.entityMasterRegion.id",
				// workLocationId));
				Criterion region = Restrictions.eq("jventity.entityMasterRegion.id", workLocationId);
				Criterion entityId = Restrictions.eq("jventity.id", workLocationId);
				criteria.add(Restrictions.or(region, entityId));
			}

			log.info(":: Criteria search started ::");
			if (paginationDTO.getFilters() != null) {

				String name = (String) paginationDTO.getFilters().get("name");
				if (name != null) {
					criteria.add(Restrictions.like("jv.name", "%" + name.trim() + "%").ignoreCase());
				}
				if (paginationDTO.getFilters().get("prefixnumber") != null) {
					String prefixnumber = paginationDTO.getFilters().get("prefixnumber").toString().trim();
					criteria.add(Restrictions.like("jv.jvNumberPrefix", "%" + prefixnumber.trim() + "%").ignoreCase());
				}
				if (paginationDTO.getFilters().get("effectiveDate") != null) {
					Date date = new Date((long) paginationDTO.getFilters().get("effectiveDate"));
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = dateFormat.format(date);
					Date minDate = dateFormat.parse(strDate);
					Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
					criteria.add(Restrictions.conjunction().add(Restrictions.ge("jv.jvDate", minDate))
							.add(Restrictions.lt("jv.jvDate", maxDate)));
				}
				if (paginationDTO.getFilters().get("createdDate") != null) {
					Date date = new Date((long) paginationDTO.getFilters().get("createdDate"));
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = dateFormat.format(date);
					Date minDate = dateFormat.parse(strDate);
					Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
					criteria.add(Restrictions.conjunction().add(Restrictions.ge("jv.createdDate", minDate))
							.add(Restrictions.lt("jv.createdDate", maxDate)));
				}

				criteria.setProjection(Projections.rowCount());
				Integer totalResult = ((Long) criteria.uniqueResult()).intValue();
				criteria.setProjection(null);

				ProjectionList projectionList = Projections.projectionList();
				projectionList.add(Projections.property("jv.id"));
				projectionList.add(Projections.property("jv.name"));
				projectionList.add(Projections.property("jv.jvNumberPrefix"));
				projectionList.add(Projections.property("jv.jvDate"));
				projectionList.add(Projections.property("jv.createdDate"));

				criteria.setProjection(projectionList);

				if (paginationDTO.getFirst() != null) {
					Integer pageNo = paginationDTO.getFirst();
					Integer pageSize = paginationDTO.getPageSize();
					if (pageNo != null && pageSize != null) {
						criteria.setFirstResult(pageNo * pageSize);
						criteria.setMaxResults(pageSize);
						log.info("PageNo : [" + pageNo + "] pageSize[" + pageSize + "]");
					}

					String sortField = paginationDTO.getSortField();
					String sortOrder = paginationDTO.getSortOrder();
					log.info("sortField outside : [" + sortField + "] sortOrder[" + sortOrder + "]");
					if (paginationDTO.getSortField() != null && paginationDTO.getSortOrder() != null) {
						log.info("sortField : [" + paginationDTO.getSortField() + "] sortOrder["
								+ paginationDTO.getSortOrder() + "]");

						if (paginationDTO.getSortField().equals("name")) {
							sortField = "jv.name";
						} else if (sortField.equals("prefixnumber")) {
							sortField = "jv.jvNumberPrefix";
						} else if (sortField.equals("effectiveDate")) {
							sortField = "jv.jvDate";
						} else if (sortField.equals("createdDate")) {
							sortField = "jv.createdDate";
						}
						if (sortOrder.equals("DESCENDING")) {
							criteria.addOrder(Order.desc(sortField));
						} else {
							criteria.addOrder(Order.asc(sortField));
						}
					} else {
						criteria.addOrder(Order.desc("jv.id"));
					}
				}

				List<?> resultList = criteria.list();

				log.info("criteria list executed and the list size is : " + resultList.size());

				if (resultList == null || resultList.isEmpty() || resultList.size() == 0) {
					log.info("Journal Voucher List is null or empty ");
				}

				Iterator<?> it = resultList.iterator();
				while (it.hasNext()) {
					Object obj[] = (Object[]) it.next();

					JournalVoucherDTO dto = new JournalVoucherDTO();
					if (obj[0] != null) {
						dto.setId((Long) obj[0]);
						log.info("Id::::::::" + dto.getId());
					}
					if (obj[1] != null) {
						dto.setName((String) obj[1]);
					}
					if (obj[2] != null) {
						dto.setPrefixnumber((String) obj[2]);
					}
					if (obj[3] != null) {
						dto.setEffectiveDate((Date) obj[3]);
					}
					if (obj[4] != null) {
						dto.setCreateDate((Date) obj[4]);
					}

					JournalVoucherLog voucherLog = journalVoucherLogRepository.getVoucherLogByJvID(dto.getId());
					JournalVoucherNote voucherNote = journalVoucherNoteRepository.getVoucherNoteByJvID(dto.getId());

					dto.setJournalVoucherlog(voucherLog);
					if (voucherLog != null) {
						dto.setStatus(voucherLog.getStage().toString());
					}
					dto.setJournalVoucherNote(voucherNote);
					log.info(":: List Response ::" + dto);
					responseDtoList.add(dto);
				}
				baseDTO.setResponseContents(responseDtoList);
				baseDTO.setTotalRecords(totalResult);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

			}
		} catch (RestException re) {
			log.warn("<<===  Exception While getjournalvoucherlist ===>>", re);
			baseDTO.setStatusCode(re.getStatusCode());
		} catch (Exception e) {
			log.error("<<===  Exception Occured ===>>", e);
		}
		log.info("<<<< ------- End UserService.getjournalvoucherlist ---------- >>>>>>>");
		return baseDTO;
	}

	public BaseDTO getDetailsById(Long id) {
		log.info("<---journalvoucherService.getDetailsById() starts---->" + id);
		BaseDTO response = new BaseDTO();
		JournalVoucherDTO journalVoucherDTO = new JournalVoucherDTO();
		try {
			JournalVoucher journalVoucher = journalvoucherRepository.findOne(id);
			journalVoucherDTO.setJournalVoucher(journalVoucher);
			JournalVoucherLog voucherLog = journalVoucherLogRepository.getVoucherLogByJvID(journalVoucher.getId());
			JournalVoucherNote vouchernote = journalVoucherNoteRepository.getVoucherNoteByJvID(journalVoucher.getId());

			List<JournalVoucherEntries> jvEntriesList = new ArrayList<JournalVoucherEntries>();
			jvEntriesList = journalVoucherEntriesRepository.findJournalVoucherEntriesListByJV(id);
			journalVoucherDTO.setJournalVoucherEntries(jvEntriesList);
			journalVoucherDTO.setJournalVoucherlog(voucherLog);
			journalVoucherDTO.setJournalVoucherNote(vouchernote);
			response.setResponseContent(journalVoucherDTO);

			List<Map<String, Object>> employeeData = new ArrayList<Map<String, Object>>();
			if (journalVoucher.getId() != null) {
				log.info("<<<:::::::voucher::::not Null::::>>>>" + journalVoucher.getId() != null
						? journalVoucher.getId()
						: "Null");
				ApplicationQuery applicationQueryForlog = applicationQueryRepository
						.findByQueryName("JOURNAL_VOUCHER_LOG_EMPLOYEE_DETAILS");
				if (applicationQueryForlog == null || applicationQueryForlog.getId() == null) {
					log.info("Application Query For Log Details not found for query name : " + applicationQueryForlog);
					response.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
					return response;
				}
				String logquery = applicationQueryForlog.getQueryContent().trim();
				log.info("<=========JOURNAL_VOUCHER_LOG_EMPLOYEE_DETAILS Query Content ======>" + logquery);
				logquery = logquery.replace(":journalVouId", "'" + journalVoucher.getId().toString() + "'");
				log.info("Query Content For JOURNAL_VOUCHER_LOG_EMPLOYEE_DETAILS After replaced plan id View query : "
						+ logquery);
				employeeData = jdbcTemplate.queryForList(logquery);
				log.info("<=========JOURNAL_VOUCHER_LOG_EMPLOYEE_DETAILS Employee Data======>" + employeeData);
				response.setTotalListOfData(employeeData);

				Object[] approveRejectCommentData = approveRejectCommentsCommonService
						.getApproveRejectComments("journal_voucher", "journal_voucher_log", "journal_voucher_id", journalVoucher.getId() );
				response.setCommentAndLogList(approveRejectCommentData);

			}

			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException e) {
			log.error(" Rest Exception Occured ", e);
			response.setStatusCode(e.getStatusCode());
		} catch (Exception e) {
			log.info("Error while retriving getDetailsById----->", e);
			response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<---journalvoucherService.getDetailsById() end---->");
		return responseWrapper.send(response);
	}

	@Transactional
	public BaseDTO approvejv(JournalVoucherDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("approvejv method start=============>" + requestDTO.getId());
			JournalVoucher voucher = journalvoucherRepository.findOne(requestDTO.getId());

			JournalVoucherNote voucherNote = new JournalVoucherNote();
			voucherNote.setJournalVoucher(voucher);
			voucherNote.setFinalApproval(requestDTO.getJournalVoucherNote().getFinalApproval());
			voucherNote.setNote(requestDTO.getJournalVoucherNote().getNote());
			voucherNote.setUserMaster(
					userMasterRepositor.findOne(requestDTO.getJournalVoucherNote().getUserMaster().getId()));
			voucherNote.setCreatedBy(loginService.getCurrentUser());
			voucherNote.setCreatedByName(loginService.getCurrentUser().getUsername());
			voucherNote.setCreatedDate(new Date());
			journalVoucherNoteRepository.save(voucherNote);
			log.info("approvejv note inserted------");

			JournalVoucherLog voucherLog = new JournalVoucherLog();
			voucherLog.setJournalVoucher(voucher);
			voucherLog.setStage(requestDTO.getJournalVoucherlog().getStage());
			voucherLog.setCreatedBy(loginService.getCurrentUser());
			voucherLog.setCreatedByName(loginService.getCurrentUser().getUsername());
			voucherLog.setCreatedDate(new Date());
			voucherLog.setRemarks(requestDTO.getJournalVoucherlog().getRemarks());
			journalVoucherLogRepository.save(voucherLog);
			log.info("approveJv log inserted------");

			log.info("Approval status----------" + requestDTO.getJournalVoucherlog().getStage());

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (LedgerPostingException l) {
			log.error("approveJv method Exception----", l);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("approveJv method exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO rejectjv(JournalVoucherDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("rejectJv method start=============>" + requestDTO.getId());
			JournalVoucher voucher = journalvoucherRepository.findOne(requestDTO.getId());
			JournalVoucherLog voucherLog = new JournalVoucherLog();
			voucherLog.setJournalVoucher(voucher);
			voucherLog.setStage(requestDTO.getJournalVoucherlog().getStage());
			voucherLog.setCreatedBy(loginService.getCurrentUser());
			voucherLog.setCreatedByName(loginService.getCurrentUser().getUsername());
			voucherLog.setCreatedDate(new Date());
			voucherLog.setRemarks(requestDTO.getJournalVoucherlog().getRemarks());
			journalVoucherLogRepository.save(voucherLog);

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("rejectJv  log inserted------");
		} catch (Exception e) {
			log.error("rejectJv method exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO getEntity() {
		log.info("====>> journalvoucherService.getEntity() <<====");
		BaseDTO baseDTO = new BaseDTO();
		try {
			EntityMaster entityMaster = entityMasterRepository.findByUserRegion(loginService.getCurrentUser().getId());
			baseDTO.setResponseContent(entityMaster);
			log.info("..entityMaster.." + entityMaster.getName());
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException re) {
			log.warn("====>> Error while getById <<====", re);
			baseDTO.setStatusCode(re.getStatusCode());
		} catch (Exception e) {
			log.error("====>>  Exception occurred <<====", e);
		}
		log.info("===== journalvoucherService getEntity =====> End ");
		return baseDTO;
	}
}
