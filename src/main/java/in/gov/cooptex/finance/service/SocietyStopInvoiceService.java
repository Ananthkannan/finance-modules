package in.gov.cooptex.finance.service;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.enums.SocietyInvoicePaymentStatus;
import in.gov.cooptex.core.accounts.model.InvestmentTypeMaster;
import in.gov.cooptex.core.accounts.model.NewInvestment;
import in.gov.cooptex.core.accounts.model.PurchaseInvoice;
import in.gov.cooptex.core.accounts.model.PurchaseInvoiceItems;
import in.gov.cooptex.core.accounts.model.StopReleasePayment;
import in.gov.cooptex.core.accounts.model.StopReleasePaymentDetails;
import in.gov.cooptex.core.accounts.model.StopReleasePaymentLog;
import in.gov.cooptex.core.accounts.model.StopReleasePaymentNote;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.VoucherDetails;
import in.gov.cooptex.core.accounts.repository.StopReleasePaymentRepository;
import in.gov.cooptex.core.accounts.repository.VoucherDetailsRepository;
import in.gov.cooptex.core.accounts.repository.VoucherLogRepository;
import in.gov.cooptex.core.accounts.repository.VoucherNoteRepository;
import in.gov.cooptex.core.accounts.repository.VoucherRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.IncrementForwardDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.PurchaseInvoiceDetailsDTO;
import in.gov.cooptex.core.dto.SocietyStopInvoiceDTO;
import in.gov.cooptex.core.dto.StopSocietyDTO;
import in.gov.cooptex.core.dto.StopSocietyInvoiceDTO;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EmpIncrementLog;
import in.gov.cooptex.core.model.EmpIncrementNote;
import in.gov.cooptex.core.model.PurchaseInvoiceAdjustment;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.PurchaseInvoiceAdjustmentRepository;
import in.gov.cooptex.core.repository.PurchaseInvoiceRepository;
import in.gov.cooptex.core.repository.SupplierMasterRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.dto.pos.SocietyReleaseInvoiceDTO;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.InvoiceDetailsRequestDTO;
import in.gov.cooptex.finance.dto.SearchSocietyInvoiceAdjustmentDTO;
import in.gov.cooptex.finance.dto.SocietyPaymentVoucherSearchResponseDTO;
import in.gov.cooptex.finance.dto.VoucherDto;
import in.gov.cooptex.finance.repository.PurchaseInvoiceItemsRepository;
import in.gov.cooptex.finance.repository.SocietyStopInvoiceDetailsRepository;
import in.gov.cooptex.finance.repository.SocietyStopInvoiceRepository;
import in.gov.cooptex.finance.repository.StopReleasePaymentDetailsRepository;
import in.gov.cooptex.finance.repository.StopReleasePaymentLogRepository;
import in.gov.cooptex.finance.repository.StopReleasePaymentNoteRepository;
//import in.gov.cooptex.operation.enums.PurchaseInvoiceStatus;
import in.gov.cooptex.operation.enums.PurchaseInvoiceStatus;
import in.gov.cooptex.operation.model.SupplierMaster;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class SocietyStopInvoiceService {

	@Autowired
	EntityManager em;

	@Autowired
	SocietyStopInvoiceRepository societyStopInvoiceRepository;

	@Autowired
	SocietyStopInvoiceDetailsRepository societyStopInvoiceDetailsRepository;

	@Autowired
	ResponseWrapper responseWrapper;

	@Autowired
	UserMasterRepository userMasterRepository;

	@Autowired
	StopReleasePaymentNoteRepository stopReleasePaymentNoteRepository;

	@Autowired
	StopReleasePaymentLogRepository stopReleasePaymentLogRepository;

	@Autowired
	PurchaseInvoiceRepository purchaseInvoiceRepository;

	@Autowired
	PurchaseInvoiceAdjustmentRepository purchaseInvoiceAdjustmentRepository;

	@Autowired
	VoucherDetailsRepository voucherDetailsRepository;

	@Autowired
	SocietyPaymentVoucherService societyPaymentVoucherService;

	@Autowired
	SupplierMasterRepository supplierMasterRepository;

	@Autowired
	PurchaseInvoiceItemsRepository purchaseInvoiceItemsRepository;

	@Autowired
	VoucherRepository voucherRepository;

	@Autowired
	StopReleasePaymentRepository stopReleasePaymentRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	public BaseDTO loadLazySocietyStopInvoiceOld(PaginationDTO paginationDto) {
		log.info("<==== Starts SocietyStopInvoiceService.loadLazySocietyStopInvoice =====>" + paginationDto);
		BaseDTO response = new BaseDTO();
		try {
			Session session = em.unwrap(Session.class);
			Criteria criteria = session.createCriteria(StopReleasePayment.class, "stopReleasePayment");
			criteria.createAlias("stopReleasePayment.stopReleasePaymentDetailList", "stopReleasePaymentDetails");
			criteria.createAlias("stopReleasePaymentDetails.purchaseInvoice", "purchaseInvoice");
			criteria.createAlias("purchaseInvoice.supplierMaster", "supplierMaster");
			criteria.createAlias("supplierMaster.circleMaster", "circleMaster");
			criteria.add(Restrictions.isNotNull("stopReleasePaymentDetails.dateStopped"));

			if (paginationDto.getFilters() != null) {
				log.info("salesInvoice filters :::" + paginationDto.getFilters());

				if (paginationDto.getFilters().get("societyCode") != null
						&& !paginationDto.getFilters().get("societyCode").toString().trim().isEmpty()) {
					criteria.add(Restrictions
							.like("supplierMaster.name",
									"%" + paginationDto.getFilters().get("societyCode").toString().trim() + "%")
							.ignoreCase());
				}
				if (paginationDto.getFilters().get("regionCode") != null
						&& !paginationDto.getFilters().get("regionCode").toString().trim().isEmpty()) {
					criteria.add(
							Restrictions
									.like("circleMaster.name",
											"%" + paginationDto.getFilters().get("regionCode").toString() + "%")
									.ignoreCase());

				}
				if (paginationDto.getFilters().get("invoiceNumber") != null) {
					/*
					 * criteria.add(Restrictions.like("purchaseInvoice.invoiceNumber",
					 * "%"+paginationDto.getFilters().get("invoiceNumber").toString()+"%").
					 * ignoreCase());
					 */
					String invoicePayment = paginationDto.getFilters().get("invoiceNumber").toString();
					criteria.add(Restrictions.sqlRestriction(
							"CAST(purchasein1_.invoice_number AS TEXT) LIKE '%" + invoicePayment.trim() + "%' "));

				}

				if (paginationDto.getFilters().get("invoiceDate") != null) {
					Date date = new Date((long) paginationDto.getFilters().get("invoiceDate"));
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = dateFormat.format(date);
					Date minDate = dateFormat.parse(strDate);
					Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
					criteria.add(Restrictions.conjunction().add(Restrictions.ge("purchaseInvoice.invoiceDate", minDate))
							.add(Restrictions.lt("purchaseInvoice.invoiceDate", maxDate)));
				}

				if (paginationDto.getFilters().get("stoppedDate") != null) {
					Date date = new Date((long) paginationDto.getFilters().get("stoppedDate"));
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String strStopDate = dateFormat.format(date);
					Date minStopDate = dateFormat.parse(strStopDate);
					Date maxStopDate = new Date(minStopDate.getTime() + TimeUnit.DAYS.toMillis(1));
					criteria.add(Restrictions.conjunction()
							.add(Restrictions.ge("stopReleasePaymentDetails.dateStopped", minStopDate))
							.add(Restrictions.lt("stopReleasePaymentDetails.dateStopped", maxStopDate)));
				}

				// criteria.setProjection(Projections.projectionList().add(Projections.groupProperty("stopReleasePayment.id")));
				criteria.setProjection(Projections.rowCount());
				Long totalResult = (long) ((Long) criteria.uniqueResult()).intValue();
				criteria.setProjection(null);

				ProjectionList projectionList = Projections.projectionList();
				projectionList.add(Projections.property("purchaseInvoice.invoiceNumber"));
				projectionList.add(Projections.property("purchaseInvoice.invoiceDate"));
				projectionList.add(Projections.property("supplierMaster.code"));
				projectionList.add(Projections.property("circleMaster.code"));
				projectionList.add(Projections.property("supplierMaster.name"));
				projectionList.add(Projections.property("circleMaster.name"));
				projectionList.add(Projections.property("supplierMaster.id"));
				projectionList.add(Projections.property("purchaseInvoice.id"));
				projectionList.add(Projections.property("stopReleasePaymentDetails.id"));
				projectionList.add(Projections.property("stopReleasePaymentDetails.dateStopped"));
				projectionList.add(Projections.property("stopReleasePayment.id"));

				criteria.setProjection(projectionList);
				criteria.setProjection(Projections.groupProperty(
						"purchaseInvoice.id ,purchaseInvoice.invoiceNumber,purchaseInvoice.invoiceDate,supplierMaster.code,circleMaster.code"));
				criteria.setFirstResult(paginationDto.getFirst() * paginationDto.getPageSize());
				criteria.setMaxResults(paginationDto.getPageSize());

				/*
				 * if (paginationDto.getSortField() != null && paginationDto.getSortOrder() !=
				 * null) { if (paginationDto.getSortOrder().equals("DESCENDING")) {
				 * criteria.addOrder(Order.desc(paginationDto.getSortField())); } else {
				 * criteria.addOrder(Order.asc(paginationDto.getSortField())); } }
				 */

				if (paginationDto.getFirst() != null) {
					Integer pageNo = paginationDto.getFirst();
					Integer pageSize = paginationDto.getPageSize();
					if (pageNo != null && pageSize != null) {
						criteria.setFirstResult(pageNo * pageSize);
						criteria.setMaxResults(pageSize);
						log.info("PageNo : [" + pageNo + "] pageSize[" + pageSize + "]");
					}

					String sortField = paginationDto.getSortField();
					String sortOrder = paginationDto.getSortOrder();
					log.info("sortField outside : [" + sortField + "] sortOrder[" + sortOrder + "]");
					if (paginationDto.getSortField() != null && paginationDto.getSortOrder() != null) {
						log.info("sortField : [" + paginationDto.getSortField() + "] sortOrder["
								+ paginationDto.getSortOrder() + "]");

						if (paginationDto.getSortField().equals("societyCode")) {
							sortField = "supplierMaster.code";
						} else if (sortField.equals("regionCode")) {
							sortField = "circleMaster.code";
						} else if (sortField.equals("invoiceNumber")) {
							sortField = "purchaseInvoice.invoiceNumber";
						} else if (sortField.equals("invoiceDate")) {
							sortField = "purchaseInvoice.invoiceDate";
						} else if (sortField.equals("stoppedDate")) {
							sortField = "stopReleasePaymentDetails.dateStopped";
						}
						if (sortOrder.equals("DESCENDING")) {
							criteria.addOrder(Order.desc(sortField));
						} else {
							criteria.addOrder(Order.asc(sortField));
						}
					} else {
						criteria.addOrder(Order.desc("stopReleasePaymentDetails.id"));
					}
				}

				List<?> resultList = criteria.list();
				if (resultList == null || resultList.isEmpty() || resultList.size() == 0) {
					log.info(":: stop invoice list is null or empty ::");
				}
				List<SocietyReleaseInvoiceDTO> stopInvoiceList = new ArrayList<>();
				Iterator<?> it = resultList.iterator();

				while (it.hasNext()) {
					Object obj[] = (Object[]) it.next();
					SocietyReleaseInvoiceDTO societyStopInvoiceDTO = new SocietyReleaseInvoiceDTO();
					societyStopInvoiceDTO.setInvoiceNumber((Integer) obj[0]);
					societyStopInvoiceDTO.setInvoiceDate((Date) obj[1]);
					societyStopInvoiceDTO.setSocietyCode((String) obj[2]);
					societyStopInvoiceDTO.setRegionCode((String) obj[3]);
					societyStopInvoiceDTO.setSocietyName((String) obj[4]);
					societyStopInvoiceDTO.setRegionName((String) obj[5]);
					societyStopInvoiceDTO.setSupplierId((Long) obj[6]);
					societyStopInvoiceDTO.setPurchaseInoiceId((Long) obj[7]);
					societyStopInvoiceDTO.setStopReleasePaymentDetailId((Long) obj[8]);
					societyStopInvoiceDTO.setStoppedDate((Date) obj[9]);
					societyStopInvoiceDTO.setStopReleasePaymentId((Long) obj[10]);
					stopInvoiceList.add(societyStopInvoiceDTO);
				}
				response.setResponseContents(stopInvoiceList);
				response.setTotalRecords(totalResult.intValue());
				response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in SocietyStopInvoiceService.loadLazySocietyStopInvoice..", e);
			response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<==== Ends SocietyStopInvoiceService.loadLazySocietyStopInvoice =====>");
		return response;
	}

	public BaseDTO loadLazySocietyStopInvoice(PaginationDTO paginationDto) {
		log.info(" SocietyStopInvoiceService loadLazySocietyStopInvoice  called..." + paginationDto);
		BaseDTO baseDTO = new BaseDTO();
		try {
			Integer total = 0;
			Integer start = paginationDto.getFirst(), pageSize = paginationDto.getPageSize();
			start = start * pageSize;
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();
			// ApplicationQuery applicationQuery =
			// applicationQueryRepository.findByQueryName("NEW_INVESTMENT_LAZY_LIST_QUERY");

			String mainQuery = "select srp.id as stopReleasePaymentId,srp.supplier_id as supplierMasterId, srpl.stage as stage,"
					+ "srpd.purchase_invoice_id as purchaseInvoiceId,srpd.date_stopped as dateStopped,"
					+ "concat(pi.invoice_number_prefix,pi.invoice_number) as invoiceNumber,pi.invoice_date as invoiceDate,sm.code as supplierCode,sm.name as supplierName,"
					+ "cm.code as circleCode,cm.name as circleName " + "from stop_release_payment srp "
					+ "join stop_release_payment_details srpd on srp.id=srpd.stop_release_payment_id "
					+ "join (select max(id) as id from stop_release_payment_details group by stop_release_payment_id) srpd1 on srpd1.id=srpd.id "
					+ "join purchase_invoice pi on pi.id=srpd.purchase_invoice_id "
					+ "join supplier_master sm on sm.id=srp.supplier_id "
					+ "join stop_release_payment_log srpl on srpl.stop_release_payment_id=srp.id "
					+ "join (select max(id) as id from stop_release_payment_log  where action_name in ('STOPPED')  group by stop_release_payment_id) srpl1 on srpl.id=srpl1.id "
					+ "join circle_master cm on cm.id=sm.circle_id where 1=1 ";

			if (paginationDto.getFilters() != null) {

				if (paginationDto.getFilters().get("societyCode") != null) {
					mainQuery += " and upper(sm.name) like upper('%" + paginationDto.getFilters().get("societyCode")
							+ "%') ";
				}

				if (paginationDto.getFilters().get("regionCode") != null) {
					mainQuery += " and upper(cm.name) like upper('%" + paginationDto.getFilters().get("regionCode")
							+ "%') ";
				}
				if (paginationDto.getFilters().get("invoiceDate") != null) {
					Date fromDate = new Date((Long) paginationDto.getFilters().get("invoiceDate"));
					mainQuery += " and pi.invoice_date='" + fromDate + "'";
				}
				if (paginationDto.getFilters().get("dateStopped") != null) {
					Date toDate = new Date((Long) paginationDto.getFilters().get("releasedDate"));
					mainQuery += " and srpd.date_stopped='" + toDate + "'";
				}

				if (paginationDto.getFilters().get("invoiceNumber") != null) {
					Double d = Double.valueOf(paginationDto.getFilters().get("invoiceNumber").toString());
					mainQuery += " and pi.invoice_number >=" + d;
				}

				if (paginationDto.getFilters().get("stage") != null) {
					mainQuery += " and srpl.stage='" + paginationDto.getFilters().get("stage") + "'";
				}

				mainQuery += "group by srp.id,srp.supplier_id,srpd.purchase_invoice_id,srpd.date_stopped,concat(pi.invoice_number_prefix,pi.invoice_number),pi.invoice_date,sm.code,cm.code,cm.name,sm.name,srpl.stage";
			}
			String countQuery = "select count(t) from (" + mainQuery + ")t;";
			log.info("count query... " + countQuery);
			countData = jdbcTemplate.queryForList(countQuery);
			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					total = Integer.parseInt(data.get("count").toString().replace(",", ""));
			}
			log.info("total query... " + total);
			if (paginationDto.getSortField() == null)
				mainQuery += " order by srp.id desc limit " + pageSize + " offset " + start + ";";

			if (paginationDto.getSortField() != null && paginationDto.getSortOrder() != null) {

				log.info("Sort Field:[" + paginationDto.getSortField() + "] Sort Order:[" + paginationDto.getSortOrder()
						+ "]");
				if (paginationDto.getSortField().equals("societyCode")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by sm.name asc ";
				if (paginationDto.getSortField().equals("societyCode")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by sm.name desc ";

				if (paginationDto.getSortField().equals("regionCode")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by cm.name asc ";
				if (paginationDto.getSortField().equals("regionCode")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by cm.name desc ";

				if (paginationDto.getSortField().equals("invoiceDate")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by pi.invoice_date asc  ";
				if (paginationDto.getSortField().equals("invoiceDate")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by pi.invoice_date desc  ";

				if (paginationDto.getSortField().equals("dateStopped")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by srpd.date_stopped asc ";
				if (paginationDto.getSortField().equals("dateStopped")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by srpd.date_stopped desc ";

				if (paginationDto.getSortField().equals("stage") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by srpl.stage asc ";
				if (paginationDto.getSortField().equals("stage") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by srpl.stage desc ";
				mainQuery += " limit " + pageSize + " offset " + start + ";";
			}
			log.info("Main Qury....." + mainQuery);

			List<SocietyReleaseInvoiceDTO> stopInvoiceList = new ArrayList<>();
			listofData = jdbcTemplate.queryForList(mainQuery);
			for (Map<String, Object> data : listofData) {
				SocietyReleaseInvoiceDTO societyStopInvoiceDTO = new SocietyReleaseInvoiceDTO();
				if (data.get("invoicenumber") != null) {
					societyStopInvoiceDTO.setPurchaseInvoiceNumber(data.get("invoicenumber").toString());
				}
				if (data.get("invoicedate") != null) {
					societyStopInvoiceDTO.setInvoiceDate((Date) data.get("invoicedate"));
				}
				if (data.get("suppliermasterid") != null) {
					societyStopInvoiceDTO.setSocietyCode((data.get("suppliercode").toString()));
					societyStopInvoiceDTO.setSocietyName((data.get("suppliername").toString()));
					societyStopInvoiceDTO.setSupplierId(Long.parseLong(data.get("suppliermasterid").toString()));
				}
				if (data.get("purchaseinvoiceid") != null) {
					societyStopInvoiceDTO.setPurchaseInoiceId(Long.parseLong(data.get("purchaseinvoiceid").toString()));
				}

				if (data.get("stopreleasepaymentid") != null) {
					societyStopInvoiceDTO
							.setStopReleasePaymentId(Long.parseLong(data.get("stopreleasepaymentid").toString()));
				}

				if (data.get("datestopped") != null) {
					societyStopInvoiceDTO.setStoppedDate((Date) data.get("datestopped"));
				}

				if (data.get("circlecode") != null) {
					societyStopInvoiceDTO.setRegionCode((data.get("suppliercode").toString()));
					societyStopInvoiceDTO.setRegionName((data.get("circlename").toString()));

				}

				if (data.get("stage") != null) {
					societyStopInvoiceDTO.setStage(data.get("stage").toString());

				}

				stopInvoiceList.add(societyStopInvoiceDTO);
			}
			log.info("Returning loadLazySocietyStopInvoice list...." + stopInvoiceList.size());
			log.info("Total records present in loadLazySocietyStopInvoice..." + total);
			baseDTO.setResponseContents(stopInvoiceList);
			baseDTO.setTotalRecords(total);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (Exception exp) {
			log.error("Exception Cause  : ", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	/**
	 * purpose : to save stop invoice details in
	 * stop_release_payment,stop_release_payment_log, and
	 * stop_release_payment_details table
	 * 
	 * @param societyStopInvoiceDTO
	 * @return BaseDTO
	 */
	// @Transactional(rollbackFor = Exception.class)
	public BaseDTO createPurchaseStopInvoice(SocietyStopInvoiceDTO societyStopInvoiceDTO) {
		log.info("SocietyStopInvoiceService.createPurchaseStopInvoice method started ");
		BaseDTO baseDTO = new BaseDTO();
		List<PurchaseInvoiceItems> purchaseInvoiceItemsList = null;
		PurchaseInvoice purchaseInvoice = null;
		try {
			log.info("createPurchaseStopInvoice societyStopInvoiceDTO==>" + societyStopInvoiceDTO);

			validateStopInvoice(societyStopInvoiceDTO);

			// StopReleasePayment Section
			StopReleasePayment stopReleasePayment = null;
			log.info("createPurchaseStopInvoice getSupplierId==>" + societyStopInvoiceDTO.getSupplierId());
			SupplierMaster supplierMaster = supplierMasterRepository.findOne(societyStopInvoiceDTO.getSupplierId());
			log.info("supplierMaster==>" + supplierMaster);
			if (supplierMaster == null) {
				log.error("Supplier is not found for " + societyStopInvoiceDTO.getSupplierId());
				throw new RestException(ErrorDescription.SOCIETY_IS_EMPTY);
			}
			if (societyStopInvoiceDTO != null && societyStopInvoiceDTO.getStopReleasePaymentId() != null) {
				stopReleasePayment = societyStopInvoiceRepository
						.findOne(societyStopInvoiceDTO.getStopReleasePaymentId());
				stopReleasePayment.setId(societyStopInvoiceDTO.getStopReleasePaymentId());
			} else {
				stopReleasePayment = new StopReleasePayment();

			}
			stopReleasePayment.setSupplier(supplierMaster);
			stopReleasePayment.setFromDate(societyStopInvoiceDTO.getInvoiceFromDate());
			stopReleasePayment.setToDate(societyStopInvoiceDTO.getInvoiceToDate());
			if (societyStopInvoiceDTO.getRemarks() != null) {
				stopReleasePayment.setRemarks(societyStopInvoiceDTO.getRemarks());
			}
			StopReleasePayment stopInvoicePayment = societyStopInvoiceRepository.save(stopReleasePayment);

			// Log Section
			StopReleasePaymentLog stopReleasePaymentLog = null;
			if (societyStopInvoiceDTO != null && societyStopInvoiceDTO.getLogId() != null) {
				log.error("log id==>" + societyStopInvoiceDTO.getLogId());
				stopReleasePaymentLog = stopReleasePaymentLogRepository.findOne(societyStopInvoiceDTO.getLogId());
				stopReleasePaymentLog.setId(societyStopInvoiceDTO.getLogId());
			} else {
				stopReleasePaymentLog = new StopReleasePaymentLog();
			}
			stopReleasePaymentLog.setActionName(PurchaseInvoiceStatus.STOPPED.name());
			stopReleasePaymentLog.setStage(PurchaseInvoiceStatus.SUBMITTED.name());
			stopReleasePaymentLog.setStopReleasePayment(stopInvoicePayment);
			stopReleasePaymentLogRepository.save(stopReleasePaymentLog);

			// Note section
			StopReleasePaymentNote note = null;
			if (societyStopInvoiceDTO != null && societyStopInvoiceDTO.getNoteId() != null) {
				log.error("note id==>" + societyStopInvoiceDTO.getNoteId());
				note = stopReleasePaymentNoteRepository.findOne(societyStopInvoiceDTO.getNoteId());
				note.setId(societyStopInvoiceDTO.getNoteId());
			} else {
				note = new StopReleasePaymentNote();
			}
			note.setStopReleasePayment(stopInvoicePayment);
			note.setForwardTo(userMasterRepository.findOne(societyStopInvoiceDTO.getForwardToUser()));
			if (societyStopInvoiceDTO.getIsFinalApprove() != null) {
				note.setFinalApproval(societyStopInvoiceDTO.getIsFinalApprove());
			}
			note.setNote(societyStopInvoiceDTO.getNote());
			note.setActionName(PurchaseInvoiceStatus.STOPPED.name());
			stopReleasePaymentNoteRepository.save(note);

			for (PurchaseInvoiceDetailsDTO purchaseInvoiceDetailsDTO : societyStopInvoiceDTO
					.getListOfPurchaseInvoices()) {
				log.info("getInvoiceId==>" + purchaseInvoiceDetailsDTO.getInvoiceId());
				purchaseInvoice = new PurchaseInvoice();
				purchaseInvoiceItemsList = purchaseInvoiceItemsRepository
						.findByPurchaseInvoiceId(purchaseInvoiceDetailsDTO.getInvoiceId());
				log.info("invoice item size==>" + purchaseInvoiceItemsList.size());
				if (purchaseInvoiceItemsList.size() == 0) {
					log.info("purchaseInvoiceItemsList is empty for purchase invoice id "
							+ purchaseInvoiceDetailsDTO.getInvoiceId());
					throw new RestException(ErrorDescription.ERROR_EMPTY_LIST);
				}
				log.info("getInvoiceId==>" + purchaseInvoiceDetailsDTO.getInvoiceId());
				purchaseInvoice = purchaseInvoiceRepository.findOne(purchaseInvoiceDetailsDTO.getInvoiceId());
				if (purchaseInvoice == null) {
					log.info("Purchase Invoice Not found for Purchase invoice id:"
							+ purchaseInvoiceDetailsDTO.getInvoiceId());
					throw new RestException(ErrorDescription.PURCHASE_INVOICE_EMPTY);
				}
				for (PurchaseInvoiceItems purchaseInvoiceItems : purchaseInvoiceItemsList) {
					StopReleasePaymentDetails stopReleasePaymentDetails = null;
					if (societyStopInvoiceDTO != null
							&& societyStopInvoiceDTO.getStopReleasePaymentDetailId() != null) {

						log.info("payemntId==>" + societyStopInvoiceDTO.getStopReleasePaymentDetailId());
						stopReleasePaymentDetails = societyStopInvoiceDetailsRepository
								.findOne(societyStopInvoiceDTO.getStopReleasePaymentDetailId());
						stopReleasePaymentDetails.setId(societyStopInvoiceDTO.getStopReleasePaymentDetailId());
					} else {
						stopReleasePaymentDetails = new StopReleasePaymentDetails();
					}
					stopReleasePaymentDetails.setStopReleasePayment(stopInvoicePayment);
					stopReleasePaymentDetails.setDateStopped(new Date());
					stopReleasePaymentDetails.setPurchaseInvoice(purchaseInvoice);
					stopReleasePaymentDetails.setPurchaseInvoiceItemId(purchaseInvoiceItems.getId());
					societyStopInvoiceDetailsRepository.save(stopReleasePaymentDetails);
				}
				updatePurchaseInvoiceStoppedInvoiceStatus(purchaseInvoice.getId());
			}
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("SocietyStopInvoiceService.createPurchaseStopInvoice RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("SocietyStopInvoiceService.createPurchaseStopInvoice Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return responseWrapper.send(baseDTO);
	}

	private void updatePurchaseInvoiceStoppedInvoiceStatus(Long puchase_invoice_id) {
		jdbcTemplate.update("UPDATE purchase_invoice SET status='" + PurchaseInvoiceStatus.STOPPED + "' WHERE id IN("
				+ puchase_invoice_id + ");");
	}

	private void validateStopInvoice(SocietyStopInvoiceDTO societyStopInvoiceDTO) {
		Validate.notNull(societyStopInvoiceDTO, ErrorDescription.PURCHASE_INVOICE_EMPTY);

		Validate.listNotNullOrEmpty(societyStopInvoiceDTO.getListOfPurchaseInvoices(),
				ErrorDescription.ERROR_EMPTY_LIST);

		// Validate.notNull(societyStopInvoiceDTO.getSupplierId(),
		// ErrorDescription.SOCIETY_IS_EMPTY);

		Validate.notNull(societyStopInvoiceDTO.getNote(), ErrorDescription.NOTE_IS_EMPTY);

		Validate.notNull(societyStopInvoiceDTO.getInvoiceFromDate(), ErrorDescription.INVOICE_FROM_DATE);

		Validate.notNull(societyStopInvoiceDTO.getInvoiceToDate(), ErrorDescription.INVOICE_TO_DATE);
	}

	public List<PurchaseInvoiceDetailsDTO> getAllPurchaseInvoiceDetailsNew(List<Integer> listOfPurchaseInvoicesIds) {
		log.info("<--Starts SocietyStopInvoiceService .getAllPurchaseInvoiceDetailsNew-->");
		List<Map<String, Object>> purchaseInvoiceMapList = new ArrayList<Map<String, Object>>();
		String query = null;
		List<PurchaseInvoiceDetailsDTO> purchaseInvoiceDetailsDtoList = new ArrayList<>();
		ApplicationQuery applicationQuery = null;
		try {

			/*
			 * String purchaseInvoicesId = ""; for (Integer invoiceId :
			 * listOfPurchaseInvoicesIds) { purchaseInvoicesId = invoiceId.toString() + ",";
			 * }
			 */
			applicationQuery = applicationQueryRepository.findByQuery("SOCIETY_STOP_INVOICE_DETAILS");
			query = applicationQuery.getQueryContent();

			/*
			 * Long[] idsArray = new Long[listOfPurchaseInvoicesIds.size()]; idsArray =
			 * listOfPurchaseInvoicesIds.toArray(idsArray);
			 */
			String purchaseInvoicesIds = StringUtils.join(listOfPurchaseInvoicesIds, ",");

			if (purchaseInvoicesIds != null && !purchaseInvoicesIds.trim().isEmpty()) {
				log.info("getAllPurchaseInvoiceDetailsNew inside purchaseInvoiceId condition---------");
				query = query.concat(" and pi.id in (" + purchaseInvoicesIds + ")");
			}
			//query = query + " group by sm.id,sm.code,sm.name,pi.id order by sm.code,pi.invoice_number";
			query = query + " order by sm.code,pi.invoice_number";
			log.info("      over all Query PurchaseInvoice query----------" + query);
			purchaseInvoiceMapList = jdbcTemplate.queryForList(query);
			int purchaseInvoiceMapListSize = purchaseInvoiceMapList != null ? purchaseInvoiceMapList.size() : 0;
			log.info("purchaseInvoiceMapList size------" + purchaseInvoiceMapListSize);

			if (!CollectionUtils.isEmpty(purchaseInvoiceMapList)) {
				Map<Object, List<Map<String, Object>>> purchaseInvoiceDetailsBySocietyMap = purchaseInvoiceMapList
						.stream().filter(o -> o.get("supplier_id") != null)
						.collect(Collectors.groupingBy(r -> r.get("supplier_id")));
				if (!CollectionUtils.isEmpty(purchaseInvoiceDetailsBySocietyMap)) {
					for (Map.Entry<Object, List<Map<String, Object>>> entry : purchaseInvoiceDetailsBySocietyMap
							.entrySet()) {

						List<Map<String, Object>> valuesList = entry.getValue();

						if (!CollectionUtils.isEmpty(valuesList)) {
							for (Map<String, Object> dataMap : valuesList) {
								PurchaseInvoiceDetailsDTO purchaseInvoiceDetailsDTO = new PurchaseInvoiceDetailsDTO();
								Long purchaseInvoiceId = dataMap.get("id") != null
										? Long.valueOf(dataMap.get("id").toString())
										: null;
								purchaseInvoiceDetailsDTO
										.setAmountWithountTax(getDouble(dataMap, "amount_without_tax"));
								purchaseInvoiceDetailsDTO.setTax(getDouble(dataMap, "tax_value"));
								purchaseInvoiceDetailsDTO.setTotalAdjustedAmount(purchaseInvoiceAdjustmentRepository
										.getTotalAdjustedAmountByInvoiceId(purchaseInvoiceId));
								purchaseInvoiceDetailsDTO.setTotalAmountPaid(
										voucherDetailsRepository.getTotalAmountByInvoiceId(purchaseInvoiceId));
								purchaseInvoiceDetailsDTO.setSocietyId(getLong(dataMap, "supplier_id"));
								purchaseInvoiceDetailsDTO.setInvoiceId(getLong(dataMap, "id"));
								purchaseInvoiceDetailsDTO
										.setInvoiceNumberPrefix(getString(dataMap, "invoice_number_prefix"));
								purchaseInvoiceDetailsDTO.setInvoiceNumber(dataMap.get("invoice_number") != null
										? Integer.valueOf(dataMap.get("invoice_number").toString())
										: null);
								purchaseInvoiceDetailsDTO.setInvoiceDate(
										dataMap.get("invoice_date") != null ? (Date) (dataMap.get("invoice_date"))
												: null);
								purchaseInvoiceDetailsDTO
										.setTotalPaybleAmount(purchaseInvoiceDetailsDTO.getAmountWithountTax()
												+ purchaseInvoiceDetailsDTO.getTax());
								purchaseInvoiceDetailsDTO
										.setBalanceAmount(purchaseInvoiceDetailsDTO.getTotalPaybleAmount()
												- (purchaseInvoiceDetailsDTO.getTotalAdjustedAmount()
														+ purchaseInvoiceDetailsDTO.getTotalAmountPaid()));

								purchaseInvoiceDetailsDTO
										.setTotalInvoiceAmountToBePaid(purchaseInvoiceDetailsDTO.getBalanceAmount());
								if (purchaseInvoiceDetailsDTO.getTotalInvoiceAmountToBePaid() > 0) {
									purchaseInvoiceDetailsDtoList.add(purchaseInvoiceDetailsDTO);
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			log.info("SocietyStopInvoiceService getAllPurchaseInvoiceDetailsNew exception", e);
		}
		log.info("<--Ends SocietyStopInvoiceService .getAllPurchaseInvoiceDetailsNew-->");
		return purchaseInvoiceDetailsDtoList;
	}

	public List<PurchaseInvoiceDetailsDTO> getAllPurchaseInvoiceDetails(List<PurchaseInvoice> listOfPurchaseInvoices) {
		log.info("Starts SocietyStopInvoiceService.getAllPurchaseInvoiceDetails method");

		List<PurchaseInvoiceDetailsDTO> listOfAdjustmentDTO = new ArrayList<PurchaseInvoiceDetailsDTO>();
		try {
			Double totalAmountPaid = 0.0;
			Double totalAdjustmentAmount = 0.0;
			Double tax = 0.0;
			Double amountWithountTax = 0.0;
			Double totalPaybleAmount = 0.0;
			Double balanceAmount = 0.0;
			Double getTotalAmountPaidAgainstVoucherForInvoiceTemp = 0.0;
			log.info("listOfPurchaseInvoices size==>" + listOfPurchaseInvoices.size());
			if (listOfPurchaseInvoices != null) {
				for (PurchaseInvoice purchaseInvoice : listOfPurchaseInvoices) {

					PurchaseInvoiceDetailsDTO purchaseInvoiceDetailsDTO = new PurchaseInvoiceDetailsDTO();
					purchaseInvoiceDetailsDTO.setVoucherDetailsId(purchaseInvoice.getVoucherDetailId());

					/*
					 * log.info("pur vot id==>" + purchaseInvoice.getVoucherDetailId() + "<=in id=>"
					 * + purchaseInvoice.getId());
					 */
					log.info("VoucherDetail Id==> " + purchaseInvoice.getVoucherDetailId());
					log.info("purchaseInvoice Id==> " + purchaseInvoice.getId());

					List<PurchaseInvoiceAdjustment> purchaseInvoiceAdjustments = purchaseInvoiceAdjustmentRepository
							.getInvoicesByInvoiceId(purchaseInvoice.getId());
					// log.info("Invoice
					// id==>"+purchaseInvoice.getId()+"<=size=>"+purchaseInvoiceAdjustments.size());
					if (purchaseInvoiceAdjustments != null && !purchaseInvoiceAdjustments.isEmpty()
							&& purchaseInvoiceAdjustments.size() > 0) {

						log.info("purchaseInvoiceAdjustments size==> " + purchaseInvoiceAdjustments.size());
						for (PurchaseInvoiceAdjustment PurchaseInvoiceAdjustment : purchaseInvoiceAdjustments) {
							totalAdjustmentAmount += PurchaseInvoiceAdjustment.getTotalAdjustedAmount();
							log.info("Invoice id==>" + purchaseInvoice.getId() + "<=totalAdjustmentAmount=>"
									+ totalAdjustmentAmount);
						}
						purchaseInvoiceDetailsDTO.setTotalAdjustedAmount(totalAdjustmentAmount);
						amountWithountTax = (purchaseInvoice.getMaterialValue() != null)
								? purchaseInvoice.getMaterialValue()
								: 0.0;
						log.info("getAllPurchaseInvoiceDetails :: amountWithountTax==>" + amountWithountTax);
						purchaseInvoiceDetailsDTO.setAmountWithountTax(amountWithountTax);
						// log.info("getCgstValue==>"+purchaseInvoice.getCgstValue()+"<=getSgstValue=>"+purchaseInvoice.getSgstValue());
						// tax = ((purchaseInvoice.getCgstValue() != null) ?
						// purchaseInvoice.getCgstValue() : 0.0) + ((purchaseInvoice.getIgstValue() !=
						// null) ? purchaseInvoice.getIgstValue() : 0.0);
						tax = ((purchaseInvoice.getCgstValue() != null) ? purchaseInvoice.getCgstValue() : 0.0)
								+ ((purchaseInvoice.getSgstValue() != null) ? purchaseInvoice.getSgstValue() : 0.0);
						log.info("getAllPurchaseInvoiceDetails :: tax==>" + tax);
						purchaseInvoiceDetailsDTO.setTax(tax);

						totalPaybleAmount = amountWithountTax + tax;
						log.info("getAllPurchaseInvoiceDetails :: totalPaybleAmount==>" + totalPaybleAmount);
						// log.info("amountWithountTax==>"+amountWithountTax+"<=tax=>"+tax+"<=totalPaybleAmount=>"+totalPaybleAmount);
						purchaseInvoiceDetailsDTO.setTotalPaybleAmount(totalPaybleAmount);
						log.info("getAllPurchaseInvoiceDetails :: purchase invoice id==>" + purchaseInvoice.getId());
						totalAmountPaid = voucherDetailsRepository.getTotalVchAmtPurchaseInvId(purchaseInvoice.getId());
						log.info("getAllPurchaseInvoiceDetails :: totalAmountPaid==>" + totalAmountPaid);
						if (totalAmountPaid == null || totalAmountPaid.isNaN()) {
							purchaseInvoiceDetailsDTO.setTotalAmountPaid(0.0);
						} else {
							purchaseInvoiceDetailsDTO.setTotalAmountPaid(totalAmountPaid);
						}

						balanceAmount = totalPaybleAmount - totalAdjustmentAmount
								- ((totalAmountPaid != null) ? totalAmountPaid : 0.0);
						log.info("getAllPurchaseInvoiceDetails :: balanceAmount==> " + balanceAmount);
						purchaseInvoiceDetailsDTO.setBalanceAmount(balanceAmount);
						log.info("getAllPurchaseInvoiceDetails :: invoice number==> "
								+ purchaseInvoice.getInvoiceNumber());
						purchaseInvoiceDetailsDTO.setInvoiceNumber(purchaseInvoice.getInvoiceNumber());
						log.info("getAllPurchaseInvoiceDetails :: invoice date==> " + purchaseInvoice.getInvoiceDate());
						purchaseInvoiceDetailsDTO.setInvoiceDate(purchaseInvoice.getInvoiceDate());
						log.info("getAllPurchaseInvoiceDetails :: invoice id==>" + purchaseInvoice.getId());
						purchaseInvoiceDetailsDTO.setInvoiceId(purchaseInvoice.getId());
						log.info("getAllPurchaseInvoiceDetails :: getInvoiceNumberPrefix==> "
								+ purchaseInvoice.getInvoiceNumberPrefix());
						purchaseInvoiceDetailsDTO.setInvoiceNumberPrefix(purchaseInvoice.getInvoiceNumberPrefix());
						// to get the voucher amount paid for a particular invoice.
						getTotalAmountPaidAgainstVoucherForInvoiceTemp = voucherDetailsRepository
								.getTotalAmountPaidAgainstVoucherForInvoice(purchaseInvoice.getId());
						purchaseInvoiceDetailsDTO
								.setTotalInvoiceAmountToBePaid((getTotalAmountPaidAgainstVoucherForInvoiceTemp != null)
										? getTotalAmountPaidAgainstVoucherForInvoiceTemp
										: 0.0);
						log.info("getAllPurchaseInvoiceDetails :: supplier master id==> "
								+ purchaseInvoice.getSupplierMaster().getId());
						purchaseInvoiceDetailsDTO.setSocietyId(purchaseInvoice.getSupplierMaster().getId());
						listOfAdjustmentDTO.add(purchaseInvoiceDetailsDTO);
					}
				}
			}
			log.info("Ended SocietyStopInvoiceService.getAllPurchaseInvoiceDetails method");
		} catch (Exception e) {
			log.error("getAllPurchaseInvoiceDetails", e);
		}
		return listOfAdjustmentDTO;

	}

	public List<PurchaseInvoiceDetailsDTO> getAllVoucherDetails(List<PurchaseInvoice> listOfPurchaseInvoices) {
		log.info("Start SocietyStopInvoiceService.invoiceDetails method");

		List<PurchaseInvoiceDetailsDTO> listOfAdjustmentDTO = new ArrayList<PurchaseInvoiceDetailsDTO>();
		Double totalAmountPaid = 0.0;
		Double totalAdjustmentAmount = 0.0;
		Double tax = 0.0;
		Double amountWithountTax = 0.0;
		Double totalPaybleAmount = 0.0;
		Double balanceAmount = 0.0;
		for (PurchaseInvoice purchaseInvoice : listOfPurchaseInvoices) {

			PurchaseInvoiceDetailsDTO purchaseInvoiceDetailsDTO = new PurchaseInvoiceDetailsDTO();
			List<PurchaseInvoiceAdjustment> purchaseInvoiceAdjustments = purchaseInvoiceAdjustmentRepository
					.getInvoicesByInvoiceId(purchaseInvoice.getId());
			if (purchaseInvoiceAdjustments.size() > 0) {
				for (PurchaseInvoiceAdjustment PurchaseInvoiceAdjustment : purchaseInvoiceAdjustments) {
					totalAdjustmentAmount += PurchaseInvoiceAdjustment.getTotalAdjustedAmount();
				}
				purchaseInvoiceDetailsDTO.setTotalAdjustedAmount(totalAdjustmentAmount);
				amountWithountTax = (purchaseInvoice.getMaterialValue() != null) ? purchaseInvoice.getMaterialValue()
						: 0.0;
				purchaseInvoiceDetailsDTO.setAmountWithountTax(amountWithountTax);
				tax = ((purchaseInvoice.getCgstValue() != null) ? purchaseInvoice.getCgstValue() : 0.0)
						+ ((purchaseInvoice.getIgstValue() != null) ? purchaseInvoice.getIgstValue() : 0.0);
				purchaseInvoiceDetailsDTO.setTax(tax);
				totalPaybleAmount = amountWithountTax + tax;
				purchaseInvoiceDetailsDTO.setTotalPaybleAmount(totalPaybleAmount);
				totalAmountPaid = voucherDetailsRepository.getTotalVchAmtPurchaseInvId(purchaseInvoice.getId());
				purchaseInvoiceDetailsDTO.setTotalAmountPaid(totalAmountPaid);
				balanceAmount = totalPaybleAmount - totalAdjustmentAmount
						- ((totalAmountPaid != null) ? totalAmountPaid : 0.0);
				purchaseInvoiceDetailsDTO.setBalanceAmount(balanceAmount);
				purchaseInvoiceDetailsDTO.setInvoiceNumber(purchaseInvoice.getInvoiceNumber());
				purchaseInvoiceDetailsDTO.setInvoiceDate(purchaseInvoice.getInvoiceDate());
				purchaseInvoiceDetailsDTO.setInvoiceId(purchaseInvoice.getId());
				purchaseInvoiceDetailsDTO.setInvoiceNumberPrefix(purchaseInvoice.getInvoiceNumberPrefix());
				purchaseInvoiceDetailsDTO.setSocietyId(purchaseInvoice.getSupplierMaster().getId());
				listOfAdjustmentDTO.add(purchaseInvoiceDetailsDTO);
			}

		}
		log.info("End SocietyStopInvoiceService.invoiceDetails method");
		return listOfAdjustmentDTO;

	}

	public BaseDTO searchPurchaseInvoiceDetailsOld(InvoiceDetailsRequestDTO invoiceDetailsRequestDTO) {
		log.info("<--Starts SocietyStopInvoiceService .searchPurchaseInvoiceDetails-->" + invoiceDetailsRequestDTO);
		BaseDTO baseDTO = new BaseDTO();
		List<PurchaseInvoice> listOfPurchaseInvoicesList = null;
		try {
			// Validate.notNull(invoiceDetailsRequestDTO.getSupplierId(),
			// ErrorDescription.SOCIETY_IS_EMPTY);
			Validate.notNull(invoiceDetailsRequestDTO.getStartDate(), ErrorDescription.INVOICE_FROM_DATE);
			Validate.notNull(invoiceDetailsRequestDTO.getEndDate(), ErrorDescription.INVOICE_TO_DATE);

			// log.info("getSupplierId-->" + invoiceDetailsRequestDTO.getSupplierId());
			log.info("getStartDate-->" + invoiceDetailsRequestDTO.getStartDate());
			log.info("getEndDate-->" + invoiceDetailsRequestDTO.getEndDate());

			listOfPurchaseInvoicesList = societyPaymentVoucherService.getAllPendingInvoicesBasedOnSociety(
					invoiceDetailsRequestDTO.getSupplierId(), invoiceDetailsRequestDTO.getStartDate(),
					invoiceDetailsRequestDTO.getEndDate());
			if (listOfPurchaseInvoicesList != null && !listOfPurchaseInvoicesList.isEmpty()) {
				log.info("searchPurchaseInvoiceDetails :: listOfPurchaseInvoicesList size==> "
						+ listOfPurchaseInvoicesList.size());
				baseDTO.setResponseContents(getAllPurchaseInvoiceDetails(listOfPurchaseInvoicesList));
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			} else {
				log.error("listOfPurchaseInvoicesList is empty or null");
				baseDTO.setStatusCode(ErrorDescription.ERROR_EMPTY_LIST.getErrorCode());
			}
			log.info("<------End SocietyStopInvoiceService .searchPurchaseInvoceDetails--> ");

		} catch (RestException restException) {
			log.error("exception Occured ", restException);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		} catch (Exception exception) {
			log.error("exception Occured ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO searchPendingPurchaseInvoiceDetailsGroupedBySociety(SearchSocietyInvoiceAdjustmentDTO searchDto) {
		log.info("<--Starts SocietyStopInvoiceService .searchPendingPurchaseInvoiceDetailsGroupedBySociety-->"
				+ searchDto);
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<String> statuses = new ArrayList<>();
			statuses.add(PurchaseInvoiceStatus.APPROVED.name());
			statuses.add(PurchaseInvoiceStatus.PARTIALLY_PAID.name());
			List<PurchaseInvoice> purchaseInvoices;
			if (searchDto.getSocietyIds() != null && searchDto.getSocietyIds().size() > 0) {
				purchaseInvoices = purchaseInvoiceRepository
						.findAllByStatusInAndInvoiceDateBetweenAndSupplierMasterIdInOrderByIdAsc(statuses,
								searchDto.getInvoiceFromDate(), searchDto.getInvoiceToDate(),
								searchDto.getSocietyIds());
			} else {
				purchaseInvoices = purchaseInvoiceRepository.findAllByStatusInAndInvoiceDateBetweenOrderByIdAsc(
						statuses, searchDto.getInvoiceFromDate(), searchDto.getInvoiceToDate());
			}
			baseDTO.setResponseContents(process(purchaseInvoices));
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			log.info("<--Ended SocietyStopInvoiceService .searchPendingPurchaseInvoiceDetailsGroupedBySociety-->"
					+ searchDto);

		} catch (RestException restException) {
			log.error("Exception occured " + restException);
			return null;

		} catch (Exception e) {
			log.error("Exception occured " + e);
			return null;

		}
		return baseDTO;

	}

	public BaseDTO deleteStopSociety(Long id) {
		log.info("SocietyStopInvoiceService deleteStopSociety Delete Started and the id is :: " + id);
		BaseDTO baseDTO = new BaseDTO();
		try {
			Validate.notNull(id, ErrorDescription.ROSTER_ID_IS_EMPTY);
			societyStopInvoiceRepository.delete(id);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("SocietyStopInvoiceService.deleteStopSociety() Completed");
		} catch (EmptyResultDataAccessException e) {
			log.error("Exception occurred in SocietyStopInvoiceService.deleteStopSociety() -:", e);
		} catch (Exception exception) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.error("Exception occurred in SocietyStopInvoiceService.deleteStopSociety() -:", exception);

		}
		return baseDTO;
	}

	@Autowired
	VoucherNoteRepository voucherNoteRepository;

	@Autowired
	VoucherLogRepository voucherLogRepository;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	public BaseDTO searchPurchaseInvoiceDetailsBasedOnVoucherId(Long voucherId) {
		log.info("<--Starts SocietyStopInvoiceService .searchPendingPurchaseInvoiceDetailsGroupedBySociety-->"
				+ voucherId);
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("voucherId=>" + voucherId);
			Voucher vocherDetail = voucherRepository.findOne(voucherId);

			List<PurchaseInvoice> purchaseInvoices = new ArrayList<>();

			if (vocherDetail != null) {

				// List<VoucherDetails> voucherDetailsList =
				// voucherDetailsRepository.findAllByVoucherId(voucherId);

				List<VoucherDetails> voucherDetailsList = vocherDetail.getVoucherDetailsList();
				log.info("voucherDetailsList size==>" + voucherDetailsList.size());

				int s = 0;
				for (VoucherDetails voucherdetails : voucherDetailsList) {
					log.info("sssss==>" + s + "<=voucher details id=>" + voucherDetailsList.get(s).getId());
					voucherdetails.getPurchaseInvoice().setVoucherDetailId(voucherDetailsList.get(s).getId());
					purchaseInvoices.add(voucherdetails.getPurchaseInvoice());
					s++;
				}
			}
			baseDTO.setResponseContents(process(purchaseInvoices));
			if (vocherDetail != null) {
				VoucherDto voucherDto = new VoucherDto();
				voucherDto.setVoucherId(vocherDetail.getId());
				voucherDto.setVoucherFromDate(vocherDetail.getFromDate());
				voucherDto.setVoucherToDate(vocherDetail.getToDate());
				voucherDto.setPaymentMode(vocherDetail.getPaymentMode());
				// voucherDto.setVoucherDetailsList(vocherDetail.getVoucherDetailsList());
				// voucherDto.setVoucherNote(vocherDetail.getVoucherNote());
				// voucherDto.setVoucherLogList(vocherDetail.getVoucherLogList());
				voucherDto.setVoucherNoteObj(voucherNoteRepository.findByVoucherId(vocherDetail.getId()));
				voucherDto.setVoucherLogObj(voucherLogRepository.findByVoucherId(vocherDetail.getId()));
				baseDTO.setResponseContent(voucherDto);
			}

			List<Map<String, Object>> employeeData = new ArrayList<Map<String, Object>>();
			if (vocherDetail != null) {
				log.info(
						"<<<:::::::voucher::::not Null::::>>>>" + vocherDetail != null ? vocherDetail.getId() : "Null");
				ApplicationQuery applicationQueryForlog = applicationQueryRepository
						.findByQueryName("VOUCHER_LOG_EMPLOYEE_DETAILS");
				if (applicationQueryForlog == null || applicationQueryForlog.getId() == null) {
					log.info("Application Query For Log Details not found for query name : " + applicationQueryForlog);
					baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
					return baseDTO;
				}
				String logquery = applicationQueryForlog.getQueryContent().trim();
				log.info("<=========VOUCHER_LOG_EMPLOYEE_DETAILS Query Content ======>" + logquery);
				logquery = logquery.replace(":voucherId", "'" + vocherDetail.getId().toString() + "'");
				log.info("Query Content For VOUCHER_LOG_EMPLOYEE_DETAILS After replaced plan id View query : "
						+ logquery);
				employeeData = jdbcTemplate.queryForList(logquery);
				log.info("<=========VOUCHER_LOG_EMPLOYEE_DETAILS Employee Data======>" + employeeData);
				baseDTO.setTotalListOfData(employeeData);
			}

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.error("searchPurchaseInvoiceDetailsBasedOnVoucherId exception==>", e);
		}
		return baseDTO;

	}

	private List<SocietyPaymentVoucherSearchResponseDTO> process(List<PurchaseInvoice> purchaseInvoices) {

		List<SocietyPaymentVoucherSearchResponseDTO> res = new ArrayList<>();
		try {

			Map<Long, SocietyPaymentVoucherSearchResponseDTO> responseDTOMap = new HashMap<>();

			for (PurchaseInvoice invoice : purchaseInvoices) {

				Long societyId = invoice.getSupplierMaster().getId();

				log.info("getVoucherDetailId==>" + invoice.getVoucherDetailId());

				if (responseDTOMap.containsKey(societyId)) {

					SocietyPaymentVoucherSearchResponseDTO dto = responseDTOMap.get(societyId);
					// log.info("if
					// getAmountWithoutTax"+dto.getAmountWithoutTax()+"<==getMaterialValue==>"+invoice.getMaterialValue());
					dto.setAmountWithoutTax(dto.getAmountWithoutTax() + invoice.getMaterialValue());
					dto.setTax((invoice.getCgstValue() != null ? invoice.getCgstValue() : 0)
							+ (invoice.getSgstValue() != null ? invoice.getSgstValue() : 0) + dto.getTax());
					// dto.setTax(invoice.getCgstValue() + invoice.getSgstValue());

					/*
					 * List<PurchaseInvoiceAdjustment> purchaseInvoiceAdjustmentsList =
					 * purchaseInvoiceAdjustmentRepository .findAllByInvoiceId(invoice);
					 */

					List<PurchaseInvoiceAdjustment> purchaseInvoiceAdjustmentsList = purchaseInvoiceAdjustmentRepository
							.getInvoicesByInvoiceId(invoice.getId());

					for (PurchaseInvoiceAdjustment p : purchaseInvoiceAdjustmentsList) {
						log.info(p.getTotalAdjustedAmount() + "<=if TotalAdjustmentAmount=>"
								+ dto.getTotalAdjustmentAmount());
						dto.setTotalAdjustmentAmount(p.getTotalAdjustedAmount() + dto.getTotalAdjustmentAmount());

						/*
						 * totalAdjustmentAmount += p.getTotalAdjustedAmount();
						 * dto.setTotalAdjustmentAmount(totalAdjustmentAmount);
						 * log.info("if Invoice id==>"+invoice.getId()+"<=totalAdjustmentAmount=>"+
						 * totalAdjustmentAmount);
						 */
					}
					List<VoucherDetails> voucherDetailsList = voucherDetailsRepository
							.findAllByPurchaseInvoice(invoice);

					voucherDetailsList.forEach(voucherDetails -> {
						dto.setTotalAmountPaid(voucherDetails.getAmount() + dto.getTotalAmountPaid());
					});

				} else {

					SocietyPaymentVoucherSearchResponseDTO dto = new SocietyPaymentVoucherSearchResponseDTO();
					dto.setSocietyId(invoice.getSupplierMaster().getId());
					dto.setSocietyName(invoice.getSupplierMaster().getName());
					dto.setSocietyCode(invoice.getSupplierMaster().getCode());
					dto.setSocietyDisplayName(dto.getSocietyCode() + "/" + dto.getSocietyName());
					// log.info("else getMaterialValue==>"+invoice.getMaterialValue());
					dto.setAmountWithoutTax(invoice.getMaterialValue());
					dto.setTax((invoice.getCgstValue() != null ? invoice.getCgstValue() : 0)
							+ (invoice.getSgstValue() != null ? invoice.getSgstValue() : 0));

					/*
					 * List<PurchaseInvoiceAdjustment> purchaseInvoiceAdjustmentsList =
					 * purchaseInvoiceAdjustmentRepository .findAllByInvoiceId(invoice);
					 */

					List<PurchaseInvoiceAdjustment> purchaseInvoiceAdjustmentsList = purchaseInvoiceAdjustmentRepository
							.getInvoicesByInvoiceId(invoice.getId());

					for (PurchaseInvoiceAdjustment p : purchaseInvoiceAdjustmentsList) {
						log.info(p.getTotalAdjustedAmount() + "<=else TotalAdjustmentAmount=>"
								+ dto.getTotalAdjustmentAmount());
						dto.setTotalAdjustmentAmount(p.getTotalAdjustedAmount() + dto.getTotalAdjustmentAmount());
					}
					List<VoucherDetails> voucherDetailsList = voucherDetailsRepository
							.findAllByPurchaseInvoice(invoice);

					voucherDetailsList.forEach(voucherDetails -> {
						dto.setTotalAmountPaid(voucherDetails.getAmount() + dto.getTotalAmountPaid());
					});

					responseDTOMap.put(societyId, dto);
				}
			}

			for (Long key : responseDTOMap.keySet()) {

				SocietyPaymentVoucherSearchResponseDTO temp = responseDTOMap.get(key);
				// log.info("getAmountWithoutTax==>"+temp.getAmountWithoutTax());
				// log.info("getTax==>"+temp.getTax());
				temp.setTotalPayableAmount(temp.getAmountWithoutTax() + temp.getTax());
				// log.info("totalPayableAmount==>"+temp.getTotalPayableAmount());
				temp.setBalanceAmount(temp.getTotalPayableAmount() - temp.getTotalAdjustmentAmount());
				temp.setTotalAmountToBePaid(temp.getBalanceAmount());
				List<PurchaseInvoiceDetailsDTO> purchaseInvoiceDetailsDTOList = getAllPurchaseInvoiceDetails(
						purchaseInvoices);
				List<PurchaseInvoiceDetailsDTO> purchaseInvoiceDetailsDTOList1 = new ArrayList<>();
				for (PurchaseInvoiceDetailsDTO purchaseInvoiceDetailsDTO : purchaseInvoiceDetailsDTOList) {
					if (purchaseInvoiceDetailsDTO.getSocietyId().equals(temp.getSocietyId())) {
						// purchaseInvoiceDetailsDTO.setInvoiceId(purchaseInvoiceDetailsDTO.getVoucherDetailId());
						purchaseInvoiceDetailsDTOList1.add(purchaseInvoiceDetailsDTO);
					}
				}
				temp.setPurchaseInvoiceDetailsDTOList(purchaseInvoiceDetailsDTOList1);
				res.add(temp);

			}
		} catch (Exception e) {
			log.error("process ::  Exception ==>", e);
		}
		return res;
	}

	public BaseDTO deleteVoucherById(Long id) {
		log.info(" deleteVoucherById :: id==> " + id);
		BaseDTO baseDTO = new BaseDTO();
		try {
			Validate.notNull(id, ErrorDescription.SOCIETY_PAYMENT_ID_NOT_EMPTY);
			// voucherRepository.delete(id);
			String voucherQuery = "DELETE FROM voucher WHERE id =" + id;
			String voucherDetailsQuery = "DELETE FROM voucher_details WHERE voucher_id =" + id;
			String voucherNoteQuery = "DELETE FROM voucher_note WHERE voucher_id =" + id;
			String voucherLogQuery = "DELETE FROM voucher_log WHERE voucher_id =" + id;

			jdbcTemplate.execute(voucherLogQuery);
			jdbcTemplate.execute(voucherNoteQuery);
			jdbcTemplate.execute(voucherDetailsQuery);
			jdbcTemplate.execute(voucherQuery);
			log.info("Voucher deleted successfully by id" + id);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (Exception e) {
			log.error(" Exception Occured while deleting deleteVoucherById", e);
			baseDTO.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("< === == Delete deleteVoucherById End === == > ");

		return baseDTO;

	}

	/**
	 * purpose : to update society stop log details
	 * 
	 * @param stopSocietyInvoiceDTO
	 * @return
	 */
	public BaseDTO updateSocietyStopLog(StopSocietyInvoiceDTO stopSocietyInvoiceDTO) {
		log.info("SocietyStopInvoiceService :: updateSocietyStopLog ");
		BaseDTO baseDTO = new BaseDTO();
		StopReleasePaymentLog stopLog = null;
		try {
			if (stopSocietyInvoiceDTO != null) {
				// --create new row in emp_increment_log table
				log.info("Stop Release Payment id==>" + stopSocietyInvoiceDTO.getStopReleasePaymentId());
				StopReleasePaymentLog stopReleasePaymentLog = new StopReleasePaymentLog();
				if (stopSocietyInvoiceDTO.getStopReleasePaymentId() != null) {
					stopReleasePaymentLog.setStopReleasePayment(
							stopReleasePaymentRepository.findOne(stopSocietyInvoiceDTO.getStopReleasePaymentId()));
				}
				log.info("status==>" + stopSocietyInvoiceDTO.getStatus());
				stopReleasePaymentLog.setStage(stopSocietyInvoiceDTO.getStatus());
				log.info("forwardRemarks==> " + stopSocietyInvoiceDTO.getForwardRemarks());
				stopReleasePaymentLog.setRemarks(stopSocietyInvoiceDTO.getForwardRemarks());
				stopReleasePaymentLog.setActionName(stopSocietyInvoiceDTO.getActionName());
				stopLog = stopReleasePaymentLogRepository.save(stopReleasePaymentLog);

				// -- update stop_release_payment_note table(Approval or Final-Apprval)
				log.info("forward for==> " + stopSocietyInvoiceDTO.getForwardFor());
				if (stopSocietyInvoiceDTO.getForwardFor().equals(true)) {
					if (stopSocietyInvoiceDTO.getNoteId() != null) {
						log.info("Stop Society Note Id==> " + stopSocietyInvoiceDTO.getNoteId());
						log.info("forward user Id==> " + stopSocietyInvoiceDTO.getForwardUserId());
						StopReleasePaymentNote stopReleasePaymentNote = stopReleasePaymentNoteRepository
								.findOne(stopSocietyInvoiceDTO.getNoteId());
						stopReleasePaymentNote
								.setForwardTo(userMasterRepository.findOne(stopSocietyInvoiceDTO.getForwardUserId()));
						stopReleasePaymentNote.setFinalApproval(stopSocietyInvoiceDTO.getForwardFor());
						stopReleasePaymentNoteRepository.save(stopReleasePaymentNote);
					} else {
						log.error("Note id not found");
					}
				}
			} else {
				log.error("Stop invoice log detail not found");
			}
			if (stopLog != null) {
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			}

		} catch (Exception e) {
			log.error("<--- Error in updateIncrementlog ---> ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return baseDTO;
	}

	// ===============================================================================================================

	public BaseDTO searchPurchaseInvoiceDetails(InvoiceDetailsRequestDTO invoiceDetailsRequestDTO) {
		log.info("<--Starts SocietyStopInvoiceService .getAllPendingPurchaseInvoices-->");
		BaseDTO baseDTO = new BaseDTO();
		List<PurchaseInvoiceDetailsDTO> purchaseInvoiceList = null;
		// List<SocietyPaymentVoucherSearchResponseDTO>
		// societyPaymentVoucherSearchResponseDTOList = null;
		List<PurchaseInvoiceDetailsDTO> societyPaymentVoucherSearchResponseDTOList = null;
		List<Map<String, Object>> purchaseInvoiceMapList = new ArrayList<Map<String, Object>>();
		String query = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Map<Long, SocietyPaymentVoucherSearchResponseDTO> responseDTOMap = new HashMap<>();
		ApplicationQuery applicationQuery = null;
		try {
			societyPaymentVoucherSearchResponseDTOList = new ArrayList<PurchaseInvoiceDetailsDTO>();
			purchaseInvoiceList = new ArrayList<PurchaseInvoiceDetailsDTO>();

			log.info("getAllPendingPurchaseInvoices inside productPlan null condtion---------");

			applicationQuery = applicationQueryRepository.findByQuery("SOCIETY_PAYMENT_DETAILS_SQL");
			query = applicationQuery.getQueryContent();

			if (invoiceDetailsRequestDTO.getStartDate() != null && invoiceDetailsRequestDTO.getEndDate() != null) {
				query = query.replaceAll(":fromDate", dateFormat.format(invoiceDetailsRequestDTO.getStartDate()));
				query = query.replaceAll(":toDate", dateFormat.format(invoiceDetailsRequestDTO.getEndDate()));
			} else {
				query = query.replaceAll("pi.acknowledged_date between  ':fromDate'  and ':toDate'", "1=1");
			}

			query = query.replaceAll(":APPROVED", PurchaseInvoiceStatus.APPROVED.toString());
			query = query.replaceAll(":PARTIALLY_PAID", PurchaseInvoiceStatus.PARTIALLY_PAID.toString());

			if (invoiceDetailsRequestDTO.getSupplierId() != null && invoiceDetailsRequestDTO.getSupplierId() > 0) {
				log.info("getAllPendingPurchaseInvoices inside supplier condtion---------");
				query = query.concat(" and pi.supplier_id in (" + invoiceDetailsRequestDTO.getSupplierId() + ")");
			}
			//query = query + " group by sm.id,sm.code,sm.name,pi.id order by sm.code,pi.invoice_number";
			query = query + " order by sm.code,pi.invoice_number";
			log.info("      over all Query PurchaseInvoice query----------" + query);
			purchaseInvoiceMapList = jdbcTemplate.queryForList(query);
			int purchaseInvoiceMapListSize = purchaseInvoiceMapList != null ? purchaseInvoiceMapList.size() : 0;
			log.info("purchaseInvoiceMapList size------" + purchaseInvoiceMapListSize);
			List<PurchaseInvoiceDetailsDTO> purchaseInvoiceDetailsDtoList = new ArrayList<>();
			if (!CollectionUtils.isEmpty(purchaseInvoiceMapList)) {
				Map<Object, List<Map<String, Object>>> purchaseInvoiceDetailsBySocietyMap = purchaseInvoiceMapList
						.stream().filter(o -> o.get("supplier_id") != null)
						.collect(Collectors.groupingBy(r -> r.get("supplier_id")));
				if (!CollectionUtils.isEmpty(purchaseInvoiceDetailsBySocietyMap)) {
					for (Map.Entry<Object, List<Map<String, Object>>> entry : purchaseInvoiceDetailsBySocietyMap
							.entrySet()) {

						List<Map<String, Object>> valuesList = entry.getValue();

						if (!CollectionUtils.isEmpty(valuesList)) {
							// PurchaseInvoiceDetailsDTO societyPaymentVoucherSearchResponseDTO = new
							// PurchaseInvoiceDetailsDTO();
							Map<String, Object> societyValueMap = valuesList.get(0);

							for (Map<String, Object> dataMap : valuesList) {
								PurchaseInvoiceDetailsDTO purchaseInvoiceDetailsDTO = new PurchaseInvoiceDetailsDTO();
								Long purchaseInvoiceId = dataMap.get("id") != null
										? Long.valueOf(dataMap.get("id").toString())
										: null;
								purchaseInvoiceDetailsDTO
										.setAmountWithountTax(getDouble(dataMap, "amount_without_tax"));
								purchaseInvoiceDetailsDTO.setTax(getDouble(dataMap, "tax_value"));
								purchaseInvoiceDetailsDTO.setTotalAdjustedAmount(purchaseInvoiceAdjustmentRepository
										.getTotalAdjustedAmountByInvoiceId(purchaseInvoiceId));
								purchaseInvoiceDetailsDTO.setTotalAmountPaid(
										voucherDetailsRepository.getTotalAmountByInvoiceId(purchaseInvoiceId));
								purchaseInvoiceDetailsDTO.setSocietyId(getLong(dataMap, "supplier_id"));
								purchaseInvoiceDetailsDTO.setInvoiceId(getLong(dataMap, "id"));
								purchaseInvoiceDetailsDTO
										.setInvoiceNumberPrefix(getString(dataMap, "invoice_number_prefix"));
								purchaseInvoiceDetailsDTO.setInvoiceNumber(dataMap.get("invoice_number") != null
										? Integer.valueOf(dataMap.get("invoice_number").toString())
										: null);
								purchaseInvoiceDetailsDTO.setInvoiceDate(
										dataMap.get("invoice_date") != null ? (Date) (dataMap.get("invoice_date"))
												: null);
								purchaseInvoiceDetailsDTO
										.setTotalPaybleAmount(purchaseInvoiceDetailsDTO.getAmountWithountTax()
												+ purchaseInvoiceDetailsDTO.getTax());
								purchaseInvoiceDetailsDTO
										.setBalanceAmount(purchaseInvoiceDetailsDTO.getTotalPaybleAmount()
												- (purchaseInvoiceDetailsDTO.getTotalAdjustedAmount()
														+ purchaseInvoiceDetailsDTO.getTotalAmountPaid()));

								purchaseInvoiceDetailsDTO
										.setTotalInvoiceAmountToBePaid(purchaseInvoiceDetailsDTO.getBalanceAmount());
								if (purchaseInvoiceDetailsDTO.getTotalInvoiceAmountToBePaid() > 0) {
									purchaseInvoiceDetailsDtoList.add(purchaseInvoiceDetailsDTO);
								}
							}

							/*
							 * societyPaymentVoucherSearchResponseDTO .setSocietyId(getLong(societyValueMap,
							 * "supplier_id"));
							 * societyPaymentVoucherSearchResponseDTO.setSocietyName(getString(
							 * societyValueMap, "name"));
							 * societyPaymentVoucherSearchResponseDTO.setSocietyCode(getString(
							 * societyValueMap, "code")); societyPaymentVoucherSearchResponseDTO
							 * .setSocietyDisplayName(societyPaymentVoucherSearchResponseDTO.getSocietyCode(
							 * ) + "/" + societyPaymentVoucherSearchResponseDTO.getSocietyName()); if
							 * (!CollectionUtils.isEmpty(purchaseInvoiceDetailsList)) {
							 * societyPaymentVoucherSearchResponseDTO
							 * .setPurchaseInvoiceDetailsDTOList(purchaseInvoiceDetailsList);
							 * societyPaymentVoucherSearchResponseDTO.setAmountWithoutTax(
							 * purchaseInvoiceDetailsList .stream().filter(o -> o.getAmountWithountTax() !=
							 * null).collect(Collectors
							 * .summingDouble(PurchaseInvoiceDetailsDTO::getAmountWithountTax)));
							 * societyPaymentVoucherSearchResponseDTO
							 * .setTax(purchaseInvoiceDetailsList.stream().filter(o -> o.getTax() != null)
							 * .collect(Collectors.summingDouble(PurchaseInvoiceDetailsDTO::getTax)));
							 * societyPaymentVoucherSearchResponseDTO.setTotalPayableAmount(
							 * societyPaymentVoucherSearchResponseDTO.getAmountWithoutTax() +
							 * societyPaymentVoucherSearchResponseDTO.getTax());
							 * societyPaymentVoucherSearchResponseDTO
							 * .setTotalAdjustmentAmount(purchaseInvoiceDetailsList.stream() .filter(o ->
							 * o.getTotalAdjustedAmount() != null) .collect(Collectors.summingDouble(
							 * PurchaseInvoiceDetailsDTO::getTotalAdjustedAmount)));
							 * societyPaymentVoucherSearchResponseDTO.setTotalAmountPaid(
							 * purchaseInvoiceDetailsList .stream().filter(o -> o.getTotalAmountPaid() !=
							 * null).collect(Collectors
							 * .summingDouble(PurchaseInvoiceDetailsDTO::getTotalAmountPaid)));
							 * societyPaymentVoucherSearchResponseDTO
							 * .setBalanceAmount(societyPaymentVoucherSearchResponseDTO.
							 * getTotalPayableAmount() -
							 * (societyPaymentVoucherSearchResponseDTO.getTotalAdjustmentAmount() +
							 * societyPaymentVoucherSearchResponseDTO.getTotalAmountPaid()));
							 * societyPaymentVoucherSearchResponseDTO
							 * .setTotalAmountToBePaid(societyPaymentVoucherSearchResponseDTO.
							 * getBalanceAmount()); societyPaymentVoucherSearchResponseDTOList.add(
							 * societyPaymentVoucherSearchResponseDTO); }
							 */
						}
					}
				}
			}
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			baseDTO.setResponseContents(purchaseInvoiceDetailsDtoList);
		} catch (Exception e) {
			log.info("SocietyStopInvoiceService getAllPendingPurchaseInvoices exception", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("<--Ends SocietyStopInvoiceService .getAllPendingPurchaseInvoices-->");
		return baseDTO;
	}

	private static final String DECIMAL_FORMAT = "#0.00";

	private Double getDouble(Map<String, Object> map, String key) {
		try {
			Object obj = map.get(key);
			if (obj != null) {
				String valueStr = String.valueOf(obj);
				if (!org.apache.commons.lang3.StringUtils.isEmpty(valueStr)) {
					DecimalFormat decimalFormat = new DecimalFormat(DECIMAL_FORMAT);
					Double value = decimalFormat.parse(decimalFormat.format(Double.valueOf(valueStr))).doubleValue();
					return value;
				}
			}
		} catch (Exception ex) {
			log.error("Exception at getDouble()", ex);
		}
		return null;
	}

	private String getString(Map<String, Object> map, String key) {
		try {
			Object obj = map.get(key);
			if (obj != null) {
				return String.valueOf(obj);
			}
		} catch (Exception ex) {
			log.error("Exception at getString()", ex);
		}
		return null;
	}

	private Long getLong(Map<String, Object> map, String key) {
		try {
			Object obj = map.get(key);
			if (obj != null) {
				return Long.valueOf(String.valueOf(obj));
			}
		} catch (Exception ex) {
			log.error("Exception at getLong()", ex);
		}
		return null;
	}

	@Autowired
	StopReleasePaymentDetailsRepository stopReleasePaymentDetailsRepository;

	public BaseDTO viewReleasePurchaseInvoiceDetails(SocietyReleaseInvoiceDTO societyReleaseInvoiceDTO) {
		log.info("<--Starts SocietyStopInvoiceService .searchPurchaseInvoiceDetails-->" + societyReleaseInvoiceDTO);
		BaseDTO baseDTO = new BaseDTO();
		Set<Long> set = new HashSet<Long>();
		List<PurchaseInvoiceDetailsDTO> listOfPurchaseInvoiceDTO = null;
		StopSocietyDTO stopSocietyDTO = new StopSocietyDTO();
		try {
			StopReleasePayment stopReleasePayment = new StopReleasePayment();
			Validate.notNull(societyReleaseInvoiceDTO.getPurchaseInoiceId(), ErrorDescription.PURCHASE_INVOICE_EMPTY);

			List<Integer> purchaseInvoiceIdList = societyStopInvoiceDetailsRepository
					.getpurchaseInvoiceIdlistbystopreleasepaymentID(societyReleaseInvoiceDTO.getStopReleasePaymentId());
			if (purchaseInvoiceIdList == null || purchaseInvoiceIdList.isEmpty()) {
				log.info("Purchase Invoice Not found for Purchase invoice id:"
						+ societyReleaseInvoiceDTO.getPurchaseInoiceId());
				throw new RestException(ErrorDescription.PURCHASE_INVOICE_EMPTY);
			}
//			listOfPurchaseInvoiceDTO = getAllPurchaseInvoiceDetails(listOfPurchaseInvoice);
			listOfPurchaseInvoiceDTO = getAllPurchaseInvoiceDetailsNew(purchaseInvoiceIdList);
			if (listOfPurchaseInvoiceDTO.size() > 0) {
				log.info("getStopReleasePaymentDetailId()==>"
						+ societyReleaseInvoiceDTO.getStopReleasePaymentDetailId());
				log.info("getStopReleasePaymentId()==>" + societyReleaseInvoiceDTO.getStopReleasePaymentId());
				/*
				 * StopReleasePaymentDetails stopReleasePaymentDetails1 =
				 * stopReleasePaymentDetailsRepository
				 * .findOne(societyReleaseInvoiceDTO.getStopReleasePaymentDetailId());
				 */
				// if (stopReleasePaymentDetails != null) {
				stopReleasePayment = societyStopInvoiceRepository
						.findOne(societyReleaseInvoiceDTO.getStopReleasePaymentId());
				listOfPurchaseInvoiceDTO.get(0).setStopSocityId(societyReleaseInvoiceDTO.getStopReleasePaymentId());
				listOfPurchaseInvoiceDTO.get(0)
						.setStopSocityDetailId(societyReleaseInvoiceDTO.getStopReleasePaymentDetailId());
				listOfPurchaseInvoiceDTO.get(0).setInvoiceFromDate(stopReleasePayment.getFromDate());
				listOfPurchaseInvoiceDTO.get(0).setInvoiceToDate(stopReleasePayment.getToDate());
				// }

			}
			StopReleasePaymentNote stopReleasePaymentNote = stopReleasePaymentNoteRepository
					.findReleasePaymentNoteByResId(societyReleaseInvoiceDTO.getStopReleasePaymentId());
			List<StopReleasePaymentLog> stopReleasePaymentLogList = stopReleasePaymentLogRepository
					.findReleasePaymentLogByResId(societyReleaseInvoiceDTO.getStopReleasePaymentId());
			StopReleasePaymentLog stopReleasePaymentLog = new StopReleasePaymentLog();
			if (stopReleasePaymentLogList != null && !stopReleasePaymentLogList.isEmpty()) {
				log.info("stopReleasePaymentLogList size==> " + stopReleasePaymentLogList.size());
				stopReleasePaymentLog = stopReleasePaymentLogList.get(stopReleasePaymentLogList.size() - 1);
			} else {
				log.error("StopReleasePaymentLog not found");
			}

			stopSocietyDTO.setStopReleasePaymentLog(stopReleasePaymentLog);
			stopReleasePaymentNote.setStopReleasePayment(null);
			stopSocietyDTO.setStopReleasePaymentNote(stopReleasePaymentNote);

			baseDTO.setResponseContents(listOfPurchaseInvoiceDTO);
			baseDTO.setResponseContent(stopSocietyDTO);

			List<Map<String, Object>> employeeData = new ArrayList<Map<String, Object>>();
			if (societyReleaseInvoiceDTO.getStopReleasePaymentId() != null) {
				log.info("<<<:::::::voucher::::not Null::::>>>>" + societyReleaseInvoiceDTO != null
						? societyReleaseInvoiceDTO.getStopReleasePaymentId()
						: "Null");
				ApplicationQuery applicationQueryForlog = applicationQueryRepository
						.findByQueryName("SOCIETY_STOP_INVOICE_LOG_EMPLOYEE_DETAILS");
				if (applicationQueryForlog == null || applicationQueryForlog.getId() == null) {
					log.info("Application Query For Log Details not found for query name : " + applicationQueryForlog);
					baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
					return baseDTO;
				}
				String logquery = applicationQueryForlog.getQueryContent().trim();
				log.info("<=========SOCIETY_STOP_INVOICE_LOG_EMPLOYEE_DETAILS Query Content ======>" + logquery);
				logquery = logquery.replace(":stopreleasepaymentId",
						"'" + societyReleaseInvoiceDTO.getStopReleasePaymentId().toString() + "'");
				log.info(
						"Query Content For SOCIETY_STOP_INVOICE_LOG_EMPLOYEE_DETAILS After replaced plan id View query : "
								+ logquery);
				employeeData = jdbcTemplate.queryForList(logquery);
				log.info("<=========SOCIETY_STOP_INVOICE_LOG_EMPLOYEE_DETAILS Employee Data======>" + employeeData);
				baseDTO.setTotalListOfData(employeeData);
			}

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("<--Ended SocietyStopInvoiceService .searchPurchaseInvoiceDetails-->");
		} catch (RestException restException) {
			log.error("exception Occured ", restException);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		} catch (Exception exception) {
			log.error("exception Occured ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

}
