package in.gov.cooptex.finance.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.finance.repository.FinancialYearRepository;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.FinancialYear;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.finance.advance.repository.ConcurrentAuditRepository;
import in.gov.cooptex.finance.dto.ConcurrentAuditCbDto;
import in.gov.cooptex.finance.dto.ConcurrentAuditCollectionDTO;
import in.gov.cooptex.finance.dto.ConcurrentAuditResponseDTO;
import in.gov.cooptex.finance.dto.ConcurrentAuditSalesDueDTO;
import in.gov.cooptex.operation.model.ConcurrentAudit;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class ConcurrentAuditService {

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	FinancialYearRepository financialYearRepository;

	@Autowired
	ConcurrentAuditRepository concurrentAuditRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	LoginService loginService;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	@PersistenceContext
	EntityManager entityManager;

	public BaseDTO getConcurrentAuditById(Long id) {
		log.info("========START ConcurrentAuditService.getConcurrentAuditById========");
		log.info("========Id is========" + id);
		BaseDTO baseDto = new BaseDTO();
		ConcurrentAuditResponseDTO concurrentAuditResponseDTO = new ConcurrentAuditResponseDTO();
		try {
			if (id != null) {
				String concurrentAuditCbQuery = "select  cb_as_per_schedule  as " + "schedule" + ", cb_as_per_stock as "
						+ "stock" + ", excess_value as " + "excess" + ", short_value as " + "shortt"
						+ " , rent_advance as " + "rendAdvance" + ", sample_acc as " + "sampleAccount"
						+ " from concurrent_audit_cb where concurrent_audit_id= " + id + "";

				String concurrentAuditCollectionQuery = "select department as " + "DepCode" + ", net_sales as "
						+ "netSales" + ", cheque_no as " + "chequeNo" + ",created_date as " + "date"
						+ ",cheque_amount as " + "amount"
						+ " from concurrent_audit_collection where concurrent_audit_id=" + id + "";
				String concurrentAuditSalesDueQuery = "select * from concurrent_sales_due where concurrent_audit_id="
						+ id + "";

				ConcurrentAudit concurrentAudit = concurrentAuditRepository.findOne(id);

				List<ConcurrentAuditCbDto> concurrentAuditCbDtoList = new ArrayList<>();

				concurrentAuditCbDtoList = jdbcTemplate.query(concurrentAuditCbQuery,
						new BeanPropertyRowMapper<ConcurrentAuditCbDto>(ConcurrentAuditCbDto.class));

				List<ConcurrentAuditCollectionDTO> concurrentAuditCollectionDtoList = new ArrayList<>();

				concurrentAuditCollectionDtoList = jdbcTemplate.query(concurrentAuditCollectionQuery,
						new BeanPropertyRowMapper<ConcurrentAuditCollectionDTO>(ConcurrentAuditCollectionDTO.class));

				List<ConcurrentAuditSalesDueDTO> concurrentAuditSalesDueDtoList = new ArrayList<>();

				concurrentAuditSalesDueDtoList = jdbcTemplate.query(concurrentAuditSalesDueQuery,
						new BeanPropertyRowMapper<ConcurrentAuditSalesDueDTO>(ConcurrentAuditSalesDueDTO.class));

				concurrentAuditResponseDTO.setConcurrentAudit(concurrentAudit);
				concurrentAuditResponseDTO.setConcurrentAuditCbDtoList(concurrentAuditCbDtoList);
				concurrentAuditResponseDTO.setConcurrentAuditCollectionDTO(concurrentAuditCollectionDtoList);
				concurrentAuditResponseDTO.setConcurrentAuditSalesDueDTO(concurrentAuditSalesDueDtoList);

				log.info("========ConcurrentAudit is========" + concurrentAuditResponseDTO.toString());
				baseDto.setResponseContent(concurrentAuditResponseDTO);
				baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			} else {
				throw new Exception("Id Cannot Be Empty");
			}

		} catch (Exception e) {
			log.info("========Exception Occured in ConcurrentAuditService.getConcurrentAuditById========", e);
			log.info("========Exception is========" + e.getMessage());
		}
		log.info("========End ConcurrentAuditService.getConcurrentAuditById========");
		return baseDto;
	}

	public BaseDTO saveOrUpdateConcurrentAudit(ConcurrentAudit concurrentAudit) {
		log.info("============START ConcurrentAuditService.saveOrUpdateConcurrentAudit=======");
		BaseDTO baseDto = new BaseDTO();
		try {
			if (concurrentAudit.getId() == null) {
				if (concurrentAudit.getRegion() != null && concurrentAudit.getRegion().getId() != null) {
					log.info("============Region Id is=======" + concurrentAudit.getRegion().getId());
					EntityMaster region = entityMasterRepository.findOne(concurrentAudit.getRegion().getId());
					concurrentAudit.setRegion(region);
				}
				if (concurrentAudit.getShowroom() != null && concurrentAudit.getShowroom().getId() != null) {
					log.info("============Showroom Id is=======" + concurrentAudit.getRegion().getId());
					EntityMaster showroom = entityMasterRepository.findOne(concurrentAudit.getShowroom().getId());
					concurrentAudit.setShowroom(showroom);
				}
				concurrentAudit.setVersion((long) 0);
				concurrentAudit.setStatus(ApprovalStage.INITIATED);
				ConcurrentAudit concurrentAuditObj = concurrentAuditRepository.save(concurrentAudit);
				log.info("============ConcurrentAuditService Save or Update Succesfully ======="
						+ concurrentAudit.getId());
				baseDto.setResponseContent(concurrentAuditObj);
				baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				ConcurrentAudit concurrentAuditObj = concurrentAuditRepository.findOne(concurrentAudit.getId());
				concurrentAuditObj.setAuditorName(concurrentAudit.getAuditorName());
				concurrentAuditObj.setRegion(concurrentAudit.getRegion());
				concurrentAuditObj.setShowroom(concurrentAudit.getShowroom());
				concurrentAuditObj.setCashAmount(concurrentAudit.getCashAmount());
				concurrentAuditObj.setChequeAmount(concurrentAudit.getChequeAmount());
				concurrentAuditObj.setAuditDate(concurrentAudit.getAuditDate());
				concurrentAuditObj.setPeriodFromDate(concurrentAudit.getPeriodFromDate());
				concurrentAuditObj.setPeriodToDate(concurrentAudit.getPeriodToDate());
				concurrentAuditRepository.save(concurrentAuditObj);
				baseDto.setResponseContent(concurrentAudit);
				baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());

			}
		} catch (Exception e) {
			log.info("============Exception Occured in ConcurrentAuditService.saveOrUpdateConcurrentAudit=======");
			log.info("============Exception is=======" + e.getMessage());

		}
		log.info("============END ConcurrentAuditService.saveOrUpdateConcurrentAudit=======");
		return baseDto;
	}

	public BaseDTO saveClosingBalance(ConcurrentAuditCbDto concurrentAuditCbDto) {
		log.info("============START ConcurrentAuditService.saveClosingBalance=======");
		BaseDTO baseDto = new BaseDTO();
		try {

			UserMaster userMaster = loginService.getCurrentUser();

			Long userId = userMaster != null ? userMaster.getId() : null;

			EntityMaster entityMaster = entityMasterRepository
					.getWorkLocationByUserId(loginService.getCurrentUser().getId());

			String INSERT_CONCURRENT_AUDIT_CLOSINGBALENCE_SQL = "INSERT Into concurrent_audit_cb (entity_id,closing_for,audit_date,cb_as_per_schedule,cb_as_per_stock,excess_value,short_value,rent_advance,rent_advance_description,sample_acc,sample_acc_description,due_credit_sales,"
					+ "due_credit_sales_description,created_by,created_date,concurrent_audit_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

			List<ConcurrentAuditCbDto> list = new ArrayList<>();

			list.addAll(concurrentAuditCbDto.getConcurrentAuditCbDtoList());
			list.addAll(concurrentAuditCbDto.getFurnitureAndFittingList());
			list.addAll(concurrentAuditCbDto.getModernizationAccountList());
			list.addAll(concurrentAuditCbDto.getPlantAndMachineryList());

			Long concurrentAuditId = concurrentAuditCbDto.getConcurrentAudit().getId();

			for (ConcurrentAuditCbDto data : list) {
				if (data != null) {
					jdbcTemplate.update(INSERT_CONCURRENT_AUDIT_CLOSINGBALENCE_SQL, new Object[] { entityMaster.getId(),
							data.getClosingFor(), new java.sql.Date(new Date().getTime()), data.getSchedule(),
							data.getStock(), data.getExcess(), data.getShortt(), concurrentAuditCbDto.getRendAdvance(),
							concurrentAuditCbDto.getReasonForRentAdvance(), concurrentAuditCbDto.getSampleAccount(),
							concurrentAuditCbDto.getReasonForSampleAccount(),
							concurrentAuditCbDto.getDueFromCreditSales(),
							concurrentAuditCbDto.getReasonFordueFromCreditSale(), userId,
							new java.sql.Date(new Date().getTime()), concurrentAuditId });
				}
			}

			String UPDATE_AUDIT_STAGE_SQL = "update concurrent_audit_details set audit_stage='CB_DETAILS' where id=?";

			jdbcTemplate.update(UPDATE_AUDIT_STAGE_SQL, new Object[] { concurrentAuditId });

			baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());

		} catch (Exception e) {
			log.info("============Exception Occured in ConcurrentAuditService.saveClosingBalance=======");
			log.info("============Exception is=======" + e.getMessage());

		}
		log.info("============END ConcurrentAuditService.saveClosingBalance=======");
		return baseDto;
	}

	public BaseDTO getKntAccount(Date dueDate) {
		BaseDTO baseDto = new BaseDTO();
		try {

			ConcurrentAuditCbDto concurrentAuditCbDto = new ConcurrentAuditCbDto();
			concurrentAuditCbDto.setDueDate(dueDate);
			ApplicationQuery kntAccApplicationQuery = applicationQueryRepository
					.findByQueryName("CONCURRENT_AUDIT_KNT_ACC");

			if (kntAccApplicationQuery == null) {
				log.error("Application Query not found - CONCURRENT_AUDIT_KNT_ACC");
				return null;
			}
			if (kntAccApplicationQuery != null) {
				String kntAccountQueries = kntAccApplicationQuery.getQueryContent();
				String dateStr = AppUtil.getFormattedDate(AppUtil.DATE_FORMAT_YYYY_MM_DD,
						concurrentAuditCbDto.getDueDate());
				kntAccountQueries = kntAccountQueries.replace(":searchDate", "'" + dateStr + "'");

				List<ConcurrentAuditCbDto> concurrentAuditCbDtoList = jdbcTemplate.query(kntAccountQueries.toString(),
						new BeanPropertyRowMapper<ConcurrentAuditCbDto>(ConcurrentAuditCbDto.class));
				if (concurrentAuditCbDtoList != null) {
					baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
					baseDto.setResponseContents(concurrentAuditCbDtoList);
				}
			}

		} catch (Exception e) {
			log.info("============Exception is=======", e);
		}
		return baseDto;
	}

	public BaseDTO getFurnitureAndFitting(Date dueDate) {
		BaseDTO baseDto = new BaseDTO();
		try {

			ConcurrentAuditCbDto concurrentAuditCbDto = new ConcurrentAuditCbDto();
			concurrentAuditCbDto.setDueDate(dueDate);
			ApplicationQuery furnitureAndFittingApplicationQuery = applicationQueryRepository
					.findByQueryName("FURNITURE_AND_FITTING_ACCOUNT");

			if (furnitureAndFittingApplicationQuery == null) {
				log.error("Application Query not found - FURNITURE_AND_FITTING_ACCOUNT");
				return null;
			}
			if (furnitureAndFittingApplicationQuery != null) {
				String furnitureAndFittingQueries = furnitureAndFittingApplicationQuery.getQueryContent();
				String dateStr = AppUtil.getFormattedDate(AppUtil.DATE_FORMAT_YYYY_MM_DD,
						concurrentAuditCbDto.getDueDate());
				String assetCategoryCode = "FF";
				furnitureAndFittingQueries = furnitureAndFittingQueries.replace(":searchDate", "'" + dateStr + "'");
				furnitureAndFittingQueries = furnitureAndFittingQueries.replace(":assetCatagoryCode",
						"'" + assetCategoryCode + "'");

				List<ConcurrentAuditCbDto> furnitureAndFittingList = jdbcTemplate.query(
						furnitureAndFittingQueries.toString(),
						new BeanPropertyRowMapper<ConcurrentAuditCbDto>(ConcurrentAuditCbDto.class));
				if (furnitureAndFittingList != null) {
					baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
					baseDto.setResponseContents(furnitureAndFittingList);
				}
			}

		} catch (Exception e) {
			log.info("============Exception is=======", e);
		}
		return baseDto;
	}

	public BaseDTO getPlantAndMachinery(Date dueDate) {
		BaseDTO baseDto = new BaseDTO();
		try {

			ConcurrentAuditCbDto concurrentAuditCbDto = new ConcurrentAuditCbDto();
			concurrentAuditCbDto.setDueDate(dueDate);
			ApplicationQuery plantAndMachineryApplicationQuery = applicationQueryRepository
					.findByQueryName("PLANT_MACHINERY_ACC");

			if (plantAndMachineryApplicationQuery == null) {
				log.error("Application Query not found - PLANT_MACHINERY_ACC");
				return null;
			}
			if (plantAndMachineryApplicationQuery != null) {
				String plantAndMachineryQueries = plantAndMachineryApplicationQuery.getQueryContent();
				String dateStr = AppUtil.getFormattedDate(AppUtil.DATE_FORMAT_YYYY_MM_DD,
						concurrentAuditCbDto.getDueDate());
				String assetCategoryCode = "PM";
				plantAndMachineryQueries = plantAndMachineryQueries.replace(":searchDate", "'" + dateStr + "'");
				plantAndMachineryQueries = plantAndMachineryQueries.replace(":assetCatagoryCode",
						"'" + assetCategoryCode + "'");

				List<ConcurrentAuditCbDto> plantAndMachineryList = jdbcTemplate.query(
						plantAndMachineryQueries.toString(),
						new BeanPropertyRowMapper<ConcurrentAuditCbDto>(ConcurrentAuditCbDto.class));
				if (plantAndMachineryList != null) {
					baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
					baseDto.setResponseContents(plantAndMachineryList);
				}
			}

		} catch (Exception e) {
			log.info("============Exception is=======", e);
		}
		return baseDto;
	}

	public BaseDTO getModernizationAccount(Date dueDate) {
		BaseDTO baseDto = new BaseDTO();
		try {

			ConcurrentAuditCbDto concurrentAuditCbDto = new ConcurrentAuditCbDto();
			concurrentAuditCbDto.setDueDate(dueDate);
			ApplicationQuery modernizationApplicationQuery = applicationQueryRepository
					.findByQueryName("MODERNIZATION_ACC");

			if (modernizationApplicationQuery == null) {
				log.error("Application Query not found - MODERNIZATION_ACC");
				return null;
			}
			if (modernizationApplicationQuery != null) {
				String modernizationQueries = modernizationApplicationQuery.getQueryContent();
				String dateStr = AppUtil.getFormattedDate(AppUtil.DATE_FORMAT_YYYY_MM_DD,
						concurrentAuditCbDto.getDueDate());
				String assetCatagoryCode = "MA";
				modernizationQueries = modernizationQueries.replace(":searchDate", "'" + dateStr + "'");
				modernizationQueries = modernizationQueries.replace(":assetCatagoryCode",
						"'" + assetCatagoryCode + "'");

				List<ConcurrentAuditCbDto> modernizationList = jdbcTemplate.query(modernizationQueries.toString(),
						new BeanPropertyRowMapper<ConcurrentAuditCbDto>(ConcurrentAuditCbDto.class));
				if (modernizationList != null) {
					baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
					baseDto.setResponseContents(modernizationList);
				}
			}

		} catch (Exception e) {
			log.info("============Exception is=======", e);
		}
		return baseDto;
	}

	public BaseDTO getCollectionDetails(Date dueDate) {
		BaseDTO baseDto = new BaseDTO();
		try {

			ConcurrentAuditCollectionDTO concurrentAuditCollectionDto = new ConcurrentAuditCollectionDTO();
			concurrentAuditCollectionDto.setDueDate(dueDate);
			ApplicationQuery collectionApplicationQuery = applicationQueryRepository
					.findByQueryName("CONCURRENT_AUDIT_COLLECTION_DETAILS");

			if (collectionApplicationQuery == null) {
				log.error("Application Query not found - CONCURRENT_AUDIT_COLLECTION_DETAILS");
				return null;
			}
			if (collectionApplicationQuery != null) {
				String collectionQueries = collectionApplicationQuery.getQueryContent();
				String dateStr = AppUtil.getFormattedDate(AppUtil.DATE_FORMAT_YYYY_MM_DD,
						concurrentAuditCollectionDto.getDueDate());
				collectionQueries = collectionQueries.replace(":searchDate", "'" + dateStr + "'");

				List<ConcurrentAuditCollectionDTO> concurrentAuditCollectionList = jdbcTemplate.query(
						collectionQueries.toString(),
						new BeanPropertyRowMapper<ConcurrentAuditCollectionDTO>(ConcurrentAuditCollectionDTO.class));
				if (concurrentAuditCollectionList != null) {
					baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
					baseDto.setResponseContents(concurrentAuditCollectionList);
				}
			}

		} catch (Exception e) {
			log.info("============Exception is=======", e);
		}
		return baseDto;
	}

	public BaseDTO saveConcurrentAuditCollection(ConcurrentAuditCollectionDTO concurrentAuditCollectionDTO) {
		log.info("============START ConcurrentAuditService.saveConcurrentAuditCollection=======");
		BaseDTO baseDto = new BaseDTO();
		try {
			String INSERT_CONCURRENT_AUDIT_COLLECTION_SQL = "INSERT Into concurrent_audit_collection (entity_id,audit_date,customer_name,department,net_sales,cheque_no,cheque_date,cheque_amount,"
					+ "created_by,created_date,concurrent_audit_id) values (?,?,?,?,?,?,?,?,?,?,?)";

			UserMaster userMaster = loginService.getCurrentUser();

			Long userId = userMaster != null ? userMaster.getId() : null;

			EntityMaster entityMaster = entityMasterRepository
					.getWorkLocationByUserId(loginService.getCurrentUser().getId());

			Long concurrentAuditId = concurrentAuditCollectionDTO.getConcurrentAudit().getId();

			if (concurrentAuditCollectionDTO != null) {
				jdbcTemplate.update(INSERT_CONCURRENT_AUDIT_COLLECTION_SQL,
						new Object[] { entityMaster.getId(), new java.sql.Date(new Date().getTime()),
								concurrentAuditCollectionDTO.getNameAndDesignation(),
								concurrentAuditCollectionDTO.getNameAndDesignation(),
								concurrentAuditCollectionDTO.getNetSales(), concurrentAuditCollectionDTO.getChequeNo(),
								concurrentAuditCollectionDTO.getDate(), concurrentAuditCollectionDTO.getAmount(),
								userId, new java.sql.Date(new Date().getTime()), concurrentAuditId });
			}

			String UPDATE_AUDIT_STAGE_SQL = "update concurrent_audit_details set audit_stage='NON_COLLECTION_DETAILS' where id=?";

			jdbcTemplate.update(UPDATE_AUDIT_STAGE_SQL, new Object[] { concurrentAuditId });

			baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (Exception e) {

			log.info("Exception in ConcurrentAuditService.saveConcurrentAuditCollection ", e);

		}

		log.info("============END ConcurrentAuditService.saveConcurrentAuditCollection=======");

		return baseDto;
	}

	public BaseDTO getShowroomWiseYearWiseDue(ConcurrentAuditSalesDueDTO concurrentAuditSalesDueDTO) {
		BaseDTO baseDto = new BaseDTO();
		List<String> financialYearLists = new ArrayList<>();
		ConcurrentAuditSalesDueDTO concurrentAuditSalesDueDto = new ConcurrentAuditSalesDueDTO();
		Map<String, List<ConcurrentAuditSalesDueDTO>> map = new HashMap<>();
		try {

			List<ConcurrentAuditSalesDueDTO> showroomYearwiseDueList = null;

			List<FinancialYear> financialYearList = financialYearRepository.getFinancialYears(
					concurrentAuditSalesDueDTO.getShowroomWiseYearwiseDueFrom(),
					concurrentAuditSalesDueDTO.getShowroomWiseYearwiseDueTo());

			ApplicationQuery showroomYearwiseApplicationQuery = applicationQueryRepository
					.findByQueryName("SHOWROOM_WISE_YEAR_WISE_DUE_DETAILS");

			if (showroomYearwiseApplicationQuery == null) {
				log.error("Application Query not found - SHOWROOM_WISE_YEAR_WISE_DUE_DETAILS");
				return null;
			}
			if (showroomYearwiseApplicationQuery != null) {
				String showroomYearwiseQueries = showroomYearwiseApplicationQuery.getQueryContent();

				if (!CollectionUtils.isEmpty(financialYearList)) {

					for (FinancialYear financialYear : financialYearList) {

						String startYear = financialYear.getStartYear().toString();
						String endYear = financialYear.getEndYear().toString();

						financialYearLists.add(startYear + "-" + endYear);

						String searchFromDate = AppUtil.getFormattedDate(AppUtil.DATE_FORMAT_YYYY_MM_DD,
								financialYear.getStartDate());

						String searchToDate = AppUtil.getFormattedDate(AppUtil.DATE_FORMAT_YYYY_MM_DD,
								financialYear.getEndDate());

						showroomYearwiseQueries = showroomYearwiseQueries.replace(":searchFromDate",
								"'" + searchFromDate + "'");

						showroomYearwiseQueries = showroomYearwiseQueries.replace(":searchToDate",
								"'" + searchToDate + "'");

						showroomYearwiseDueList = jdbcTemplate.query(showroomYearwiseQueries.toString(),
								new BeanPropertyRowMapper<ConcurrentAuditSalesDueDTO>(
										ConcurrentAuditSalesDueDTO.class));
					}
				}

				if (!CollectionUtils.isEmpty(financialYearLists)) {
					for (String finYear : financialYearLists) {
						Map<String, List<ConcurrentAuditSalesDueDTO>> purchaseInvoiceDetailsBySocietyMap = showroomYearwiseDueList
								.stream().filter(o -> o.getYear() != null)
								.collect(Collectors.groupingBy(r -> r.getYear().toString()));
						map.putAll(purchaseInvoiceDetailsBySocietyMap);
					}
				}
				if (showroomYearwiseDueList != null) {
					concurrentAuditSalesDueDto.setConcurrentAuditSalesDueDTOList(showroomYearwiseDueList);
					concurrentAuditSalesDueDto.setFinancialYearLists(financialYearLists);
					concurrentAuditSalesDueDto.setShowroomwisesalesDueDetailsMap(map);
					baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
					baseDto.setResponseContent(concurrentAuditSalesDueDto);
				}
			}

		} catch (Exception e) {
			log.info("============Exception is=======", e);
		}
		return baseDto;
	}

	public BaseDTO saveConcurrentAuditSalesDue(ConcurrentAuditSalesDueDTO concurrentAuditSalesDueDTO) {
		log.info("============START ConcurrentAuditService.saveConcurrentAuditSalesDue=======");
		BaseDTO baseDto = new BaseDTO();
		try {

			String UPDATE_COUNCURRENT_AUDIT_DETAILS_STATUS = "Update concurrent_audit_details set status=?,audit_stage='SALES_DUE_DETAILS' where id=?";

			String INSERT_CONCURRENT_AUDIT_SALES_DUE_SQL = "INSERT Into concurrent_sales_due (entity_id,audit_date,due_for,due_from,due_to,due_year,toatl,organization_type_id,"
					+ "created_by,created_date,modified_by,modified_date,version,concurrent_audit_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

			UserMaster userMaster = loginService.getCurrentUser();

			Long userId = userMaster != null ? userMaster.getId() : null;

			EntityMaster entityMaster = entityMasterRepository
					.getWorkLocationByUserId(loginService.getCurrentUser().getId());

			Long concurrentAuditId = concurrentAuditSalesDueDTO.getConcurrentAudit().getId();

			Integer year = Integer.parseInt(concurrentAuditSalesDueDTO.getYear());

			if (concurrentAuditSalesDueDTO != null) {
				jdbcTemplate.update(INSERT_CONCURRENT_AUDIT_SALES_DUE_SQL,
						new Object[] { entityMaster.getId(), new java.sql.Date(new Date().getTime()),
								concurrentAuditSalesDueDTO.getDueFor(),
								concurrentAuditSalesDueDTO.getShowroomWiseYearwiseDueFrom(),
								concurrentAuditSalesDueDTO.getShowroomWiseYearwiseDueTo(), year,
								concurrentAuditSalesDueDTO.getTotal(), concurrentAuditSalesDueDTO.getNgo(), userId,
								new java.sql.Date(new Date().getTime()), userId,
								new java.sql.Date(new Date().getTime()), null, concurrentAuditId });

			}

			jdbcTemplate.update(UPDATE_COUNCURRENT_AUDIT_DETAILS_STATUS,
					new Object[] { ApprovalStage.SUBMITTED, concurrentAuditId });

			baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (Exception e) {

			log.info("Exception in ConcurrentAuditService.saveConcurrentAuditSalesDue ", e);

		}

		log.info("============END ConcurrentAuditService.saveConcurrentAuditSalesDue=======");

		return baseDto;
	}

	public BaseDTO getCreditSalesDueAsperBankStmt(Date dueDate) {
		BaseDTO baseDto = new BaseDTO();
		try {

			ApplicationQuery creditSalesDueApplicationQuery = applicationQueryRepository
					.findByQueryName("CREDIT_SALES_DUE_AS_PER_BANK_STMT");

			if (creditSalesDueApplicationQuery == null) {
				log.error("Application Query not found - CREDIT_SALES_DUE_AS_PER_BANK_STMT");
				return null;
			}
			if (creditSalesDueApplicationQuery != null) {
				String creditSalesDueQueries = creditSalesDueApplicationQuery.getQueryContent();

				String dateStr = AppUtil.getFormattedDate(AppUtil.DATE_FORMAT_YYYY_MM_DD, dueDate);

				creditSalesDueQueries = creditSalesDueQueries.replace(":searchDate", "'" + dateStr + "'");

				List<ConcurrentAuditSalesDueDTO> crditSalesDueAsPerBankStatementList = jdbcTemplate.query(
						creditSalesDueQueries.toString(),
						new BeanPropertyRowMapper<ConcurrentAuditSalesDueDTO>(ConcurrentAuditSalesDueDTO.class));

				if (crditSalesDueAsPerBankStatementList != null) {

					baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
					baseDto.setResponseContents(crditSalesDueAsPerBankStatementList);
				}
			}

		} catch (Exception e) {
			log.info("============Exception is=======", e);
		}
		return baseDto;
	}

	public BaseDTO getLazyLoadData(PaginationDTO paginationDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {

			log.info("========START ConcurrentAuditService.getLazyLoadData========");
			Session session = entityManager.unwrap(Session.class);
			Criteria criteria = session.createCriteria(ConcurrentAudit.class, "concurrentAudit");
			log.info(":: Criteria search started ::");
			if (paginationDTO.getFilters() != null) {
				String auditOfficerName = (String) paginationDTO.getFilters().get("auditorName");
				if (auditOfficerName != null) {
					criteria.add(Restrictions.or(Restrictions
							.like("concurrentAudit.auditorName", "%" + auditOfficerName.trim() + "%").ignoreCase()));

				}

				if (paginationDTO.getFilters().get("status") != null) {
					String status = (String) (paginationDTO.getFilters().get("status").toString());
					criteria.add(Restrictions.eq("concurrentAudit.status", status));
				}

				if (paginationDTO.getFilters().get("periodFromDate") != null) {
					Date date = new Date((long) paginationDTO.getFilters().get("periodFromDate"));
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = dateFormat.format(date);
					Date minDate = dateFormat.parse(strDate);
					Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
					criteria.add(
							Restrictions.conjunction().add(Restrictions.ge("concurrentAudit.periodFromDate", minDate))
									.add(Restrictions.lt("concurrentAudit.periodFromDate", maxDate)));
				}
				if (paginationDTO.getFilters().get("periodToDate") != null) {
					Date date = new Date((long) paginationDTO.getFilters().get("periodToDate"));
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = dateFormat.format(date);
					Date minDate = dateFormat.parse(strDate);
					Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
					criteria.add(
							Restrictions.conjunction().add(Restrictions.ge("concurrentAudit.periodToDate", minDate))
									.add(Restrictions.lt("concurrentAudit.periodToDate", maxDate)));
				}

				if (paginationDTO.getFilters().get("auditDate") != null) {
					Date date = new Date((long) paginationDTO.getFilters().get("auditDate"));
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = dateFormat.format(date);
					Date minDate = dateFormat.parse(strDate);
					Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
					criteria.add(Restrictions.conjunction().add(Restrictions.ge("concurrentAudit.auditDate", minDate))
							.add(Restrictions.lt("concurrentAudit.auditDate", maxDate)));
				}

				criteria.setProjection(Projections.rowCount());
				Integer totalResult = ((Long) criteria.uniqueResult()).intValue();
				criteria.setProjection(null);

				ProjectionList projectionList = Projections.projectionList();
				projectionList.add(Projections.property("id"));
				projectionList.add(Projections.property("auditorName"));
				projectionList.add(Projections.property("periodFromDate"));
				projectionList.add(Projections.property("periodToDate"));
				projectionList.add(Projections.property("auditDate"));
				projectionList.add(Projections.property("status"));
				projectionList.add(Projections.property("auditStage"));
				criteria.setProjection(projectionList);

				if (paginationDTO.getFirst() != null) {
					Integer pageNo = paginationDTO.getFirst();
					Integer pageSize = paginationDTO.getPageSize();
					if (pageNo != null && pageSize != null) {
						criteria.setFirstResult(pageNo * pageSize);
						criteria.setMaxResults(pageSize);
						log.info("PageNo : [" + pageNo + "] pageSize[" + pageSize + "]");
					}

					String sortField = paginationDTO.getSortField();
					String sortOrder = paginationDTO.getSortOrder();
					log.info("sortField outside : [" + sortField + "] sortOrder[" + sortOrder + "]");
					if (paginationDTO.getSortField() != null && paginationDTO.getSortOrder() != null) {
						log.info("sortField : [" + paginationDTO.getSortField() + "] sortOrder["
								+ paginationDTO.getSortOrder() + "]");

						if (paginationDTO.getSortField().equals("auditorName")) {
							sortField = "concurrentAudit.auditorName";
						} else if (sortField.equals("periodFromDate")) {
							sortField = "concurrentAudit.periodFromDate";
						} else if (sortField.equals("periodToDate")) {
							sortField = "concurrentAudit.periodToDate";
						} else if (sortField.equals("auditDate")) {
							sortField = "concurrentAudit.auditDate";
						} else if (sortField.equals("status")) {
							sortField = "concurrentAudit.status";
						} else if (sortField.equals("id")) {
							sortField = "concurrentAudit.id";
						}
						if (sortOrder.equals("DESCENDING")) {
							criteria.addOrder(Order.desc(sortField));
						} else {
							criteria.addOrder(Order.asc(sortField));
						}
					} else {
						criteria.addOrder(Order.desc("concurrentAudit.modifiedDate"));
					}
				}
				List<?> resultList = criteria.list();
				log.info("criteria list executed and the list size is : " + resultList.size());
				if (resultList == null || resultList.isEmpty() || resultList.size() == 0) {
					log.info("concurrentAudit List is null or empty ");
				}
				List<ConcurrentAudit> concurrentAuditList = new ArrayList<>();
				Iterator<?> it = resultList.iterator();
				while (it.hasNext()) {
					Object ob[] = (Object[]) it.next();
					ConcurrentAudit response = new ConcurrentAudit();
					response.setId((Long) ob[0]);
					log.info("Id::::::::" + response.getId());
					response.setAuditorName((String) ob[1]);
					response.setPeriodFromDate((Date) ob[2]);
					response.setPeriodToDate((Date) ob[3]);
					response.setAuditDate((Date) ob[4]);
					response.setStatus((String) ob[5]);
					response.setAuditStage((String) ob[6]);
					log.info(":: List Response ::" + response);
					concurrentAuditList.add(response);
				}
				baseDTO.setResponseContents(concurrentAuditList);
				baseDTO.setTotalRecords(totalResult);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (Exception exp) {
			log.error("Exception Cause : ", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("========END ConcurrentAuditService.getLazyLoadData========");
		return baseDTO;
	}

}
