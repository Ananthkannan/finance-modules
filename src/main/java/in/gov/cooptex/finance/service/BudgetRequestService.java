package in.gov.cooptex.finance.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import in.gov.cooptex.common.service.ApproveRejectCommentsCommonService;
import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.service.NotificationEmailService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.model.BudgetConfig;
import in.gov.cooptex.core.accounts.model.BudgetRequest;
import in.gov.cooptex.core.accounts.model.BudgetRequestDetails;
import in.gov.cooptex.core.accounts.model.BudgetRequestLog;
import in.gov.cooptex.core.accounts.model.BudgetRequestNote;
import in.gov.cooptex.core.accounts.repository.AccountTransactionDetailsRepository;
import in.gov.cooptex.core.accounts.repository.GlAccountRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.finance.repository.BudgetConfigRepository;
import in.gov.cooptex.core.finance.repository.BudgetRequestDetailsRepository;
import in.gov.cooptex.core.finance.repository.BudgetRequestLogRepository;
import in.gov.cooptex.core.finance.repository.BudgetRequestNoteRepository;
import in.gov.cooptex.core.finance.repository.BudgetRequestRepository;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.SystemNotification;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.SectionMasterRepository;
import in.gov.cooptex.core.repository.SystemNotificationRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.dto.BudgetRequestDTO;
import in.gov.cooptex.finance.dto.BudgetRequestExpenseDTO;
import in.gov.cooptex.finance.dto.BudgetRequestExpenseDetailDTO;
import in.gov.cooptex.finance.enums.CreditSalesDemandStatus;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class BudgetRequestService {

	@Autowired
	BudgetRequestRepository budgetRequestRepository;

	@Autowired
	ApproveRejectCommentsCommonService approveRejectCommentsCommonService;

	@Autowired
	BudgetRequestLogRepository budgetRequestLogRepository;

	@Autowired
	BudgetRequestNoteRepository budgetRequestNoteRepository;

	@Autowired
	SystemNotificationRepository systemNotificationRepository;

	@Autowired
	AccountTransactionDetailsRepository accountTransactionDetailsRepository;

	@Autowired
	ResponseWrapper responseWrapper;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	@Autowired
	UserMasterRepository userMasterRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;
	@Autowired
	LoginService loginService;

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	BudgetConfigRepository budgetConfigRepository;

	@Autowired
	SectionMasterRepository sectionMasterRepository;

	@Autowired
	GlAccountRepository glAccountRepository;

	@Autowired
	EntityManager entityManager;

	@Autowired
	EmployeeMasterRepository employeeMasterRepository;

	@Autowired
	BudgetRequestDetailsRepository budgetRequestDetailsRepository;

	private static final String VIEW_PAGE = "/pages/accounts/budget/viewBudgetRequest.xhtml?faces-redirect=true";

	@Autowired
	NotificationEmailService notificationEmailService;

	public BaseDTO getById(BudgetRequestDTO selectedBudgetRequestDto) {
		log.info("BudgetRequestService getById method started [" + selectedBudgetRequestDto.getId() + "]");
		BaseDTO baseDTO = new BaseDTO();
		List<Map<String, Object>> employeeData = new ArrayList<Map<String, Object>>();
		try {

			Long id = selectedBudgetRequestDto.getId();
			Long notificationId = selectedBudgetRequestDto.getNotificationId();
			Validate.notNull(id, ErrorDescription.BUDGET_REQUEST_ID_EMPTY);
			BudgetRequest budgetRequest = budgetRequestRepository.getOne(id);
			Validate.notNull(budgetRequest, ErrorDescription.BUDGET_NOT_FOUND);

			BudgetRequestDTO budgetRequestDto = new BudgetRequestDTO();

			budgetRequestDto.setId(budgetRequest.getId());
			budgetRequestDto.setBudgetConfig(budgetRequest.getBudgetConfig());
			budgetRequestDto.setEntityMaster(budgetRequest.getEntityMaster());
			budgetRequestDto.setSectionMaster(budgetRequest.getSectionMaster());

			budgetRequestDto.setBudgetConfigId(budgetRequest.getBudgetConfig().getId());
			// budgetRequestDto.setBudgetConfigCode(budgetRequest.getBudgetConfig().getCode());
			// budgetRequestDto.setBudgetConfigName(budgetRequest.getBudgetConfig().getName());
			// budgetRequestDto.setEntityCode(budgetRequest.getEntityMaster().getCode());
			// budgetRequestDto.setEntityName(budgetRequest.getEntityMaster().getName());

			// String from =
			// getMonthAsString(Integer.parseInt(budgetRequest.getBudgetConfig().getFromMonth()))+"-"+budgetRequest.getBudgetConfig().getFromYear();
			// log.info("from month and year :::::"+from);
			// budgetRequestDto.setBudgetFrom(from);
			//
			// String to =
			// getMonthAsString(Integer.parseInt(budgetRequest.getBudgetConfig().getToMonth()))+"-"+budgetRequest.getBudgetConfig().getToYear();
			// log.info("to month and year :::::"+to);
			// budgetRequestDto.setBudgetTo(to);

			if (budgetRequest.getSectionMaster() != null) {
				// budgetRequestDto.setSectionCode(budgetRequest.getSectionMaster().getCode());
				// budgetRequestDto.setSectionName(budgetRequest.getSectionMaster().getName());
				budgetRequestDto.setSectionMaster(budgetRequest.getSectionMaster());
			}

			List<BudgetRequestExpenseDTO> fixedExpenseList = new ArrayList<>();
			List<BudgetRequestExpenseDTO> variableExpenseList = new ArrayList<>();

			String month = String.format("%02d", Integer.parseInt(budgetRequest.getBudgetConfig().getFromMonth()));
			String fromDate = budgetRequest.getBudgetConfig().getFromYear() - 1 + month;
			log.info("FromDate ::" + fromDate);

			month = String.format("%02d", Integer.parseInt(budgetRequest.getBudgetConfig().getToMonth()));
			String toDate = budgetRequest.getBudgetConfig().getToYear() - 1 + month;
			log.info("ToDate ::" + toDate);

			Long sectionId = null;
			if (budgetRequest.getSectionMaster() != null)
				sectionId = budgetRequest.getSectionMaster().getId();

			log.info("entityId::" + budgetRequest.getEntityMaster().getId() + "\t sectionId::" + sectionId
					+ "\t fromDate::" + fromDate + "\t toDate::" + toDate + "\t budgetRequestId::"
					+ budgetRequest.getId());
			List<BudgetRequestExpenseDTO> budgetExpenseDtoList = executeBudgetExpenseQuery(
					budgetRequest.getEntityMaster().getId(), sectionId, fromDate, toDate, budgetRequest.getId());
			if (budgetExpenseDtoList != null)
				log.info("budgetExpenseDtoList size is ::::::::::::" + budgetExpenseDtoList.size());

			for (BudgetRequestExpenseDTO b : budgetExpenseDtoList) {
				if (b.getExpenseCategory().equalsIgnoreCase("VARIABLE"))
					variableExpenseList.add(b);
				if (b.getExpenseCategory().equalsIgnoreCase("FIXED"))
					fixedExpenseList.add(b);
			}
			budgetRequestDto.setFixedExpenseList(fixedExpenseList);
			budgetRequestDto.setVariableExpenseList(variableExpenseList);

			if (id != null) {
				ApplicationQuery applicationQuery = applicationQueryRepository
						.findByQueryName("BUDGET_REQUEST_LOG_EMPLOYEE_DETAILS");
				if (applicationQuery == null || applicationQuery.getId() == null) {
					log.info("Application Query not found for query name : " + applicationQuery);
					baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
					return baseDTO;
				}
				String query = applicationQuery.getQueryContent().trim();
				query = query.replace(":requestId", "'" + id + "'");
				log.info("BUDGET_REQUEST_LOG_EMPLOYEE_DETAILS - Query " + query);
				employeeData = jdbcTemplate.queryForList(query);
				log.info("<=========DEATH_REGISTRATION_LOG_EMPLOYEE_DETAILS Employee Data======>" + employeeData);
				baseDTO.setTotalListOfData(employeeData);

				Object[] approveRejectCommentData = approveRejectCommentsCommonService
						.getApproveRejectComments("budget_request", "budget_request_log", "budget_request_id", id);
				baseDTO.setCommentAndLogList(approveRejectCommentData);

			}

			List<BudgetRequestLog> budgetLogList = budgetRequestLogRepository
					.findByBudgetRequestId(budgetRequest.getId());
			if (budgetLogList != null)
				budgetRequestDto.setBudgetLogList(budgetLogList);

			List<BudgetRequestNote> budgetNoteList = budgetRequestNoteRepository
					.findByBudgetRequestId(budgetRequest.getId());
			if (budgetNoteList != null)
				budgetRequestDto.setBudgetNoteList(budgetNoteList);

			if (notificationId != null) {
				SystemNotification systemNotification = systemNotificationRepository.findOne(notificationId);
				systemNotification.setNotificationRead(true);
				systemNotificationRepository.save(systemNotification);
			}

			baseDTO.setResponseContent(budgetRequestDto);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("BudgetRequestService getById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("BudgetRequestService getById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("BudgetRequestService getById method completed");
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO deleteById(Long id) {
		log.info("BudgetRequestService deleteById method started [" + id + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			Validate.notNull(id, ErrorDescription.BUDGET_REQUEST_ID_EMPTY);
			budgetRequestRepository.delete(id);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("BudgetRequestService deleteById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (DataIntegrityViolationException exception) {
			log.error("BudgetRequestService deleteById DataIntegrityViolationException ", exception);
			if (exception.getCause().getCause() instanceof PSQLException) {
				baseDTO.setStatusCode(ErrorDescription.CANNOT_DELETE_REFERENCED_RECORD.getErrorCode());
			} else {
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			}
		} catch (Exception exception) {
			log.error("BudgetRequestService deleteById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("BudgetRequestService deleteById method completed");
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO getActiveBudgetRequest() {
		log.info("BudgetRequestService:getActiveBudgetRequest()");
		BaseDTO baseDTO = new BaseDTO();
		try {

			List<BudgetRequest> budgetRequestList = budgetRequestRepository.getAllActiveBudgetRequest();
			baseDTO.setResponseContent(budgetRequestList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {

			log.error("BudgetRequestService getActiveBudgetRequestRestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {

			log.error("BudgetRequestService getActiveBudgetRequestException ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}

		log.info("BudgetRequestServicegetActiveBudgetRequestmethod completed");
		return baseDTO;

	}

	public BaseDTO generateExpenseDetails(Long budgetConfigId, Long entityId, Long sectionId) {
		log.info("BudgetRequestService:generateExpenseDetails Started...........");
		log.info("budgetConfidId::" + budgetConfigId + "\t entityId ::" + entityId + "\t sectionId");
		BaseDTO baseDTO = new BaseDTO();
		try {

			Validate.objectNotNull(budgetConfigId, ErrorDescription.BUDGET_CONFIG_REQUIRED);
			BudgetConfig budgetconfig = budgetConfigRepository.findOne(budgetConfigId);
			Validate.objectNotNull(budgetconfig, ErrorDescription.BUDGET_CONFIG_REQUIRED);
			Validate.objectNotNull(entityId, ErrorDescription.BUDGET_ENTITY_REQUIRED);
			EntityMaster entityMaster = entityMasterRepository.findOne(entityId);
			Validate.objectNotNull(entityMaster, ErrorDescription.BUDGET_ENTITY_REQUIRED);

			if (entityMaster.getEntityTypeMaster().getEntityCode().equalsIgnoreCase("HEAD_OFFICE"))
				Validate.objectNotNull(sectionId, ErrorDescription.BUDGET_SECTION_REQUIRED);

			BudgetRequest requestExisting = budgetRequestRepository
					.findByBudgetConfigAndSectionAndEntityAndId(budgetConfigId, sectionId, entityId, null);

			if (requestExisting != null) {
				throw new RestException(ErrorDescription.BUDGET_REQUEST_EXIST);
			}

			String month = String.format("%02d", Integer.parseInt(budgetconfig.getFromMonth()));
			String fromDate = budgetconfig.getFromYear() - 1 + month;
			log.info("FromDate ::" + fromDate);

			month = String.format("%02d", Integer.parseInt(budgetconfig.getToMonth()));
			String toDate = budgetconfig.getToYear() - 1 + month;
			log.info("ToDate ::" + toDate);
			List<BudgetRequestExpenseDTO> budgetExpenseDtoList = executeBudgetExpenseQuery(entityId, sectionId,
					fromDate, toDate, null);
			if (budgetExpenseDtoList != null)
				log.info("budgetExpenseDtoList size is ::::::::::::" + budgetExpenseDtoList.size());

			baseDTO.setResponseContent(budgetExpenseDtoList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("BudgetRequestService generateExpenseDetails RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("BudgetRequestService getActiveBudgetRequestException ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("BudgetRequestService generateExpenseDetails completed");
		return baseDTO;

	}

	public List<BudgetRequestExpenseDTO> executeBudgetExpenseQuery(Long entityId, Long sectionId, String fromDate,
			String toDate, Long budgetRequestId) {
		log.info("Starts BudgetRequestService.executeBudgetExpenseQuery ");
		log.info("entityId::" + entityId + "\t sectionId::" + sectionId + "\t fromDate::" + fromDate + "\t toDate::"
				+ toDate + "\t budgetRequestId::" + budgetRequestId);

		ApplicationQuery applicationQuery = applicationQueryRepository.findByQueryName("BUDGET_REQUEST_EXPENSE");

		if (applicationQuery == null || applicationQuery.getId() == null) {
			log.info("Application Query not found for query name : " + applicationQuery.getQueryName());
			return null;
		}

		String query = applicationQuery.getQueryContent().trim();

		log.info("Query " + query);

		query = applicationQuery.getQueryContent().trim();
		query = query.replace(":ENTITYID", entityId.toString());
		query = query.replace(":TODATE", "'" + toDate + "'");
		query = query.replace(":FROMDATE", "'" + fromDate + "'");

		if (sectionId != null) {
			query = query.replace(":SECTIONID", " AND epie.current_section_id=" + sectionId.toString());
			query = query.replace(":BR_SECTIONID", "AND br.section_id =" + sectionId.toString());
		} else {
			query = query.replace(":SECTIONID", "");
			query = query.replace(":BR_SECTIONID", "");
		}

		if (budgetRequestId != null)
			query = query.replace(":BUDGET_REQUEST_ID", " and gl4.budget_request_id =" + budgetRequestId + " ");
		else
			query = query.replace(":BUDGET_REQUEST_ID", "and 1=0 ");

		log.info("after replace Query==>" + query);

		List<BudgetRequestExpenseDTO> expenseList = jdbcTemplate.query(query, new RowMapper<BudgetRequestExpenseDTO>() {

			@Override
			public BudgetRequestExpenseDTO mapRow(ResultSet rs, int no) throws SQLException {
				BudgetRequestExpenseDTO dto = new BudgetRequestExpenseDTO();

				dto.setActualExpenseAmount(rs.getDouble("ACTUAL_EXPENSE_AMOUNT"));

				dto.setEstimationExpenseAmount(rs.getDouble("ESTIMATION_EXPENSE_AMOUNT"));

				dto.setGlAccountId(rs.getLong("GL_ID"));

				dto.setGlAccountName(rs.getString("GL_NAME"));

				dto.setExpenseCategory(rs.getString("EXPENSE_CATEGORY"));

				if (budgetRequestId != null)
					dto.setBudgetAmount(rs.getDouble("BUDGET_AMOUNT"));
				else
					dto.setBudgetAmount(rs.getDouble("ACTUAL_EXPENSE_AMOUNT"));

				dto.setIncreasePercent(rs.getDouble("INCREASE_PERCENTAGE"));

				dto.setDecreasePercent(rs.getDouble("DECREASE_PERCENTAGE"));

				dto.setBudgetDetailsId(rs.getLong("BUDGET_DETAILS_ID"));

				dto.setGlAccountCode(rs.getString("GL_CODE"));

				List<BudgetRequestExpenseDetailDTO> list = executeBudgetExpenseDetailsQuery(entityId, sectionId,
						fromDate, toDate, dto.getGlAccountId());

				dto.setExpenseDetailsList(list);

				return dto;
			}
		});

		log.info("Ends BudgetRequestService.executeBudgetExpenseQuery ");

		return expenseList;
	}

	public List<BudgetRequestExpenseDetailDTO> executeBudgetExpenseDetailsQuery(Long entityId, Long sectionId,
			String fromDate, String toDate, Long glAccountId) {
		log.info("Starts BudgetRequestService.executeBudgetExpenseDetailsQuery ");
		log.info("entityId::" + entityId + "\t sectionId::" + sectionId + "\t fromDate::" + fromDate + "\t toDate::"
				+ toDate + "\t glAccountId ::" + glAccountId);

		ApplicationQuery applicationQuery = applicationQueryRepository
				.findByQueryName("BUDGET_REQUEST_EXPENSE_DETAILS");

		if (applicationQuery == null || applicationQuery.getId() == null) {
			log.info("Application Query not found for query name : " + applicationQuery.getQueryName());
			return null;
		}

		String query = applicationQuery.getQueryContent().trim();

		log.info("Query " + query);

		query = applicationQuery.getQueryContent().trim();
		query = query.replace(":ENTITYID", entityId.toString());
		query = query.replace(":GL_ID", glAccountId.toString());
		query = query.replace(":TODATE", toDate);
		query = query.replace(":FROMDATE", fromDate);

		if (sectionId != null) {
			query = query.replace(":SECTIONID", " AND epie.current_section_id =" + sectionId.toString());
		} else {
			query = query.replace(":SECTIONID", "");
		}

		log.info("after replace Query==>" + query);

		List<BudgetRequestExpenseDetailDTO> expenseDetailsList = jdbcTemplate.query(query,
				new RowMapper<BudgetRequestExpenseDetailDTO>() {
					@Override
					public BudgetRequestExpenseDetailDTO mapRow(ResultSet rs, int no) throws SQLException {
						BudgetRequestExpenseDetailDTO dto = new BudgetRequestExpenseDetailDTO();

						dto.setMonth(rs.getString("MONTH"));

						dto.setMonthAmount(rs.getDouble("EXPENSE_AMOUNT"));

						dto.setEntityId(rs.getLong("ENTITY_ID"));

						dto.setEntityName(rs.getString("ENTITY_NAME"));

						return dto;
					}
				});

		log.info("Ends BudgetRequestService.executeBudgetExpenseDetailsQuery ");

		return expenseDetailsList;
	}

	@Transactional
	public BaseDTO createBudgetRequest(BudgetRequestDTO request) {
		log.info("BudgetRequestService:createBudgetRequest()" + request);
		BaseDTO response = new BaseDTO();
		try {
			fieldValidation(request);

			BudgetRequest requestExisting = budgetRequestRepository.findByBudgetConfigAndSectionAndEntityAndId(
					request.getBudgetConfigId(), request.getSectionMasterId(), request.getEntityMasterId(),
					request.getId());

			if (requestExisting != null) {
				throw new RestException(ErrorDescription.BUDGET_REQUEST_EXIST);
			}

			if (request.getId() == null) {
				BudgetRequest budgetRequest = convertBudgetRequestDtoToBudgetRequest(request);
				BudgetRequest budgetRequestSaveObj = budgetRequestRepository.save(budgetRequest);

				// save BudgetRequestLog
				BudgetRequestLog log = new BudgetRequestLog();
				log.setBudgetRequest(budgetRequestSaveObj);
				if (request.getStatus().equalsIgnoreCase("SAVE"))
					log.setStage(CreditSalesDemandStatus.INITIATED.toString());
				else
					log.setStage(CreditSalesDemandStatus.SUBMITTED.toString());

				budgetRequestLogRepository.save(log);

				// save BudgetRequest
				BudgetRequestNote note = new BudgetRequestNote();
				note.setBudgetRequest(budgetRequestSaveObj);
				note.setFinalApproval(request.getForwardFor());
				if (request.getForwardToUserId() != null) {
					note.setForwardTo(userMasterRepository.findOne(request.getForwardToUserId()));
					note.setNote(request.getNote());
					budgetRequestNoteRepository.save(note);
				}

				if (budgetRequest != null && budgetRequest.getId() != null) {
					Map<String, Object> additionalData = new HashMap<>();
					// Long empId = request.getEmpMaster() == null ? null :
					// request.getEmpMaster().getId();
					additionalData.put("Url", VIEW_PAGE + "&budgetRequestId=" + budgetRequest.getId() + "&");
					if (note != null && log != null) {
						notificationEmailService.sendMailAndNotificationForForward(note, log, additionalData);
					}
				}
			} else {
				Long requestId = request.getId();
				log.info("Edit called......." + requestId);

				if (requestId != null) {
					BudgetRequest budgetRequest = budgetRequestRepository.findOne(requestId);
					Validate.objectNotNull(budgetRequest, ErrorDescription.BUDGET_NOT_FOUND);

					List<BudgetRequestExpenseDTO> inputList = new ArrayList<>();
					if (!CollectionUtils.isEmpty(request.getFixedExpenseList())) {
						inputList.addAll(request.getFixedExpenseList());
					}
					if (!CollectionUtils.isEmpty(request.getVariableExpenseList())) {
						inputList.addAll(request.getVariableExpenseList());
					}

					budgetRequestDetailsRepository.deleteBudgetRequestDetailsByRequestID(requestId);

					List<BudgetRequestDetails> budgetRequestDetailsList = new ArrayList<>();

					if (!CollectionUtils.isEmpty(inputList)) {
						for (BudgetRequestExpenseDTO detail : inputList) {

							BudgetRequestDetails brd = new BudgetRequestDetails();
							brd.setBudgetRequest(budgetRequest);
							brd.setBudgetAmount(detail.getBudgetAmount());
							brd.setDecreasePercentage(detail.getIncreasePercent());
							brd.setIncreasePercentage(detail.getIncreasePercent());
							brd.setGlAccount(glAccountRepository.findOne(detail.getGlAccountId()));

							budgetRequestDetailsList.add(brd);
						}
					}

					budgetRequestDetailsRepository.save(budgetRequestDetailsList);

					List<BudgetRequestLog> logList = budgetRequestLogRepository.findByBudgetRequestId(requestId);
					Boolean logSave = true;
					if (logList != null && logList.size() > 0 && logList.get(logList.size() - 1).getStage()
							.equalsIgnoreCase(CreditSalesDemandStatus.INITIATED.toString())) {
						logSave = false;
					}

					// save BudgetRequestLog
					BudgetRequestLog log = new BudgetRequestLog();
					if (request.getStatus().equalsIgnoreCase("SAVE") && logSave) {
						log.setBudgetRequest(budgetRequest);
						log.setStage(CreditSalesDemandStatus.INITIATED.toString());
						budgetRequestLogRepository.save(log);
					} else if (request.getStatus().equalsIgnoreCase("SUBMIT")) {
						log.setBudgetRequest(budgetRequest);
						log.setStage(CreditSalesDemandStatus.SUBMITTED.toString());
						budgetRequestLogRepository.save(log);
					}

					/*
					 * //save BudgetRequest BudgetRequestNote note = new BudgetRequestNote();
					 * note.setBudgetRequest(budgetRequest);
					 * note.setFinalApproval(request.getForwardFor());
					 * note.setForwardTo(userMasterRepository.findOne(request.getForwardToUserId()))
					 * ; note.setNote(request.getNote());
					 */
					// save BudgetRequest
					BudgetRequestNote note = null;
					if (request.getNoteId() != null) {
						if (request.getForwardToUserId() != null) {
							note = budgetRequestNoteRepository.findOne(request.getNoteId());
							note.setBudgetRequest(budgetRequest);
							note.setFinalApproval(request.getForwardFor());
							note.setForwardTo(userMasterRepository.findOne(request.getForwardToUserId()));
							note.setNote(request.getNote());
							budgetRequestNoteRepository.save(note);
						}
					} else {
						if (request.getForwardToUserId() != null) {
							BudgetRequestNote noteObj = new BudgetRequestNote();
							noteObj.setBudgetRequest(budgetRequest);
							noteObj.setFinalApproval(request.getForwardFor());
							noteObj.setForwardTo(userMasterRepository.findOne(request.getForwardToUserId()));
							noteObj.setNote(request.getNote());
							note = budgetRequestNoteRepository.save(noteObj);
						}
					}

					/*
					 * Send Notification Mail
					 */

					if (request != null && request.getId() != null) {
						Map<String, Object> additionalData = new HashMap<>();
						// Long empId = request.getEmpMaster() == null ? null :
						// request.getEmpMaster().getId();
						additionalData.put("Url", VIEW_PAGE + "&budgetRequestId=" + request.getId() + "&");
						if (note != null && log != null) {
							notificationEmailService.sendMailAndNotificationForForward(note, log, additionalData);
						}
					}
				}
			}

			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (DataIntegrityViolationException divEX) {
			log.warn("<<===  Error While createBudgetRequest===>>", divEX);
		} catch (ObjectOptimisticLockingFailureException lockEx) {
			log.warn("====>> Error while BudgetRequest <<====", lockEx);
			response.setStatusCode(ErrorDescription.CANNOT_UPDATE_LOCKED_RECORD.getErrorCode());
		} catch (RestException re) {
			log.error("RestException occurred in createBudgetRequest.............", re);
			response.setStatusCode(re.getStatusCode());
		} catch (Exception e) {
			log.error("Exception occurred in createBudgetRequest.............", e);
			response.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("BudgetRequestService:createBudgetRequest()");
		return response;
	}

	public void fieldValidation(BudgetRequestDTO request) {
		log.info("<== Starts  BudgetRequestService.fieldValidation ===>");
		if (request.getId() == null) {
			Validate.objectNotNull(request.getBudgetConfigId(), ErrorDescription.BUDGET_CONFIG_REQUIRED);
			BudgetConfig budgetconfig = budgetConfigRepository.findOne(request.getBudgetConfigId());
			Validate.objectNotNull(budgetconfig, ErrorDescription.BUDGET_CONFIG_REQUIRED);
			Validate.objectNotNull(request.getEntityMasterId(), ErrorDescription.BUDGET_ENTITY_REQUIRED);
			EntityMaster entityMaster = entityMasterRepository.findOne(request.getEntityMasterId());
			Validate.objectNotNull(entityMaster, ErrorDescription.BUDGET_ENTITY_REQUIRED);

			if (entityMaster.getEntityTypeMaster().getEntityCode().equalsIgnoreCase("HEAD_OFFICE"))
				Validate.objectNotNull(request.getSectionMasterId(), ErrorDescription.BUDGET_SECTION_REQUIRED);
		}
		if (!"SAVE".equals(request.getStatus())) {
			Validate.objectNotNull(request.getForwardToUserId(), ErrorDescription.PLEASE_SELECT_FORWARD_TO);
			Validate.objectNotNull(request.getForwardFor(), ErrorDescription.PLEASE_SELECT_FORWARD_FOR);
			Validate.notNullOrEmpty(request.getNote(), ErrorDescription.NOTE_REQUIRED);
		}

		log.info("<== Ends BudgetRequestService.fieldValidation ===>");
	}

	public BudgetRequest convertBudgetRequestDtoToBudgetRequest(BudgetRequestDTO dto) {
		log.info("starts convertBudgetRequestDtoToBudgetRequest");
		BudgetRequest budgetRequest = new BudgetRequest();
		List<BudgetRequestDetails> budgetRequestDetailsList = new ArrayList<>();
		budgetRequest.setActiveStatus(true);
		budgetRequest.setBudgetConfig(budgetConfigRepository.findOne(dto.getBudgetConfigId()));
		budgetRequest.setEntityMaster(entityMasterRepository.findOne(dto.getEntityMasterId()));
		if (dto.getSectionMasterId() != null)
			budgetRequest.setSectionMaster(sectionMasterRepository.findOne(dto.getSectionMasterId()));

		dto.getFixedExpenseList().addAll(dto.getVariableExpenseList());

		for (BudgetRequestExpenseDTO detail : dto.getFixedExpenseList()) {

			BudgetRequestDetails brd = new BudgetRequestDetails();

			brd.setBudgetRequest(budgetRequest);
			brd.setBudgetAmount(detail.getBudgetAmount());
			brd.setDecreasePercentage(detail.getIncreasePercent());
			brd.setIncreasePercentage(detail.getIncreasePercent());
			brd.setGlAccount(glAccountRepository.findOne(detail.getGlAccountId()));

			budgetRequestDetailsList.add(brd);
		}
		budgetRequest.setBudgetRequestDetailsList(budgetRequestDetailsList);
		log.info("Ends convertBudgetRequestDtoToBudgetRequest");
		return budgetRequest;
	}

	@Transactional
	public BaseDTO approveBudgetRequest(BudgetRequestDTO budgetRequestDTO) {
		log.info("<--- starts approveBudgetRequest [" + budgetRequestDTO + "]");
		BaseDTO baseDTO = new BaseDTO();
		List<BudgetRequestDetails> budgetRequestDetailsList = null;
		try {
			BudgetRequest budgetRequest = budgetRequestRepository.findOne(budgetRequestDTO.getId());
			if (budgetRequest != null) {
				budgetRequestDetailsList = new ArrayList<>();
				budgetRequestDTO.getFixedExpenseList().addAll(budgetRequestDTO.getVariableExpenseList());
				for (BudgetRequestExpenseDTO dto : budgetRequestDTO.getFixedExpenseList()) {
					BudgetRequestDetails budgetRequestDetails = budgetRequestDetailsRepository
							.findOne(dto.getBudgetDetailsId());
					budgetRequestDetails.setBudgetRequest(budgetRequest);
					budgetRequestDetails.setBudgetAmount(dto.getBudgetAmount());
					budgetRequestDetails.setIncreasePercentage(dto.getIncreasePercent());
					budgetRequestDetailsList.add(budgetRequestDetails);
				}
				budgetRequestDetailsRepository.save(budgetRequestDetailsList);
				log.info("budgetRequest Details updated-------");

				BudgetRequestNote budgetRequestNote = new BudgetRequestNote();
				budgetRequestNote.setBudgetRequest(budgetRequest);
				budgetRequestNote.setForwardTo(userMasterRepository.findOne(budgetRequestDTO.getForwardToUserId()));
				budgetRequestNote.setFinalApproval(budgetRequestDTO.getForwardFor());
				budgetRequestNote.setNote(budgetRequestDTO.getNote());

				BudgetRequestLog planLog = new BudgetRequestLog();
				planLog.setBudgetRequest(budgetRequest);
				planLog.setStage(CreditSalesDemandStatus.APPROVED.toString());
				planLog.setRemarks(budgetRequestDTO.getRemarks());

				budgetRequest.getBudgetRequestLogList().add(planLog);
				budgetRequest.getBudgetRequestNoteList().add(budgetRequestNote);

				budgetRequestRepository.save(budgetRequest);

				if (ApprovalStage.APPROVED.equals(budgetRequestDTO.getStatus())) {
					String urlPath = VIEW_PAGE + "&budgetRequestId=" + budgetRequestDTO.getId() + "&";
					Long toUserId = budgetRequestDTO.getForwardToUserId();
					notificationEmailService.saveIndividualNotification(loginService.getCurrentUser(), toUserId,
							urlPath, "Budget Request", "Budget Request has been approved");
				} else {
					Map<String, Object> additionalData = new HashMap<>();
					additionalData.put("Url", VIEW_PAGE + "&budgetRequestId=" + budgetRequestDTO.getId() + "&");
					notificationEmailService.sendMailAndNotificationForForward(budgetRequestNote, planLog,
							additionalData);
				}
				log.info("BudgetRequest Approved Successfully. [" + budgetRequestDTO.getId() + "]");
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				log.info("BudgetRequest not Found");
				baseDTO.setStatusCode(ErrorDescription.BUDGET_NOT_FOUND.getCode());
			}

		} catch (Exception exception) {
			log.error("Exception while approve BudgetRequest ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO rejectBudgetRequest(BudgetRequestDTO budgetRequestDTO) {
		log.info("<--- starts BudgetRequestService rejectBudgetRequest[" + budgetRequestDTO + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {

			BudgetRequest budgetRequest = budgetRequestRepository.findOne(budgetRequestDTO.getId());
			if (budgetRequest != null) {
				BudgetRequestLog planLog = new BudgetRequestLog();
				planLog.setBudgetRequest(budgetRequest);
				planLog.setStage(CreditSalesDemandStatus.REJECTED.toString());
				planLog.setRemarks(budgetRequestDTO.getRemarks());
				budgetRequestLogRepository.save(planLog);

				String urlPath = VIEW_PAGE + "&budgetRequestId=" + budgetRequestDTO.getId() + "&";
				Long toUserId = budgetRequestNoteRepository.getCreatedByUserId(budgetRequestDTO.getId());
				notificationEmailService.saveIndividualNotification(loginService.getCurrentUser(), toUserId, urlPath,
						"Budget Request", "Budget Request has been rejected");

				log.info("BudgetRequest Service Rejected Successfully. [" + budgetRequestDTO.getId() + "]");
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());

			} else {
				log.info("BudgetRequest not Found");
				baseDTO.setStatusCode(ErrorDescription.BUDGET_NOT_FOUND.getCode());
			}

		} catch (Exception exception) {
			log.error("Exception rejectBudgetRequest", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(baseDTO);
	}

	@Transactional
	public BaseDTO finalApproveBudgetRequest(BudgetRequestDTO budgetRequestDTO) {
		log.info("<--- starts BudgetRequestService finalApproveBudgetRequest [" + budgetRequestDTO + "]");
		BaseDTO baseDTO = new BaseDTO();
		List<BudgetRequestDetails> budgetRequestDetailsList = null;
		try {
			BudgetRequest budgetRequest = budgetRequestRepository.findOne(budgetRequestDTO.getId());
			if (budgetRequest != null) {
				budgetRequestDetailsList = new ArrayList<>();
				budgetRequestDTO.getFixedExpenseList().addAll(budgetRequestDTO.getVariableExpenseList());
				for (BudgetRequestExpenseDTO dto : budgetRequestDTO.getFixedExpenseList()) {
					BudgetRequestDetails budgetRequestDetails = budgetRequestDetailsRepository
							.findOne(dto.getBudgetDetailsId());
					budgetRequestDetails.setBudgetRequest(budgetRequest);
					budgetRequestDetails.setBudgetAmount(dto.getBudgetAmount());
					budgetRequestDetails.setIncreasePercentage(dto.getIncreasePercent());
					budgetRequestDetailsList.add(budgetRequestDetails);
				}
				budgetRequestDetailsRepository.save(budgetRequestDetailsList);
				log.info("budgetRequest Details updated-------");

				// BudgetRequestNote budgetRequestNote = new BudgetRequestNote();
				// budgetRequestNote.setBudgetRequest(budgetRequest);
				// budgetRequestNote.setForwardTo(userMasterRepository.findOne(budgetRequestDTO.getForwardToUserId()));
				// budgetRequestNote.setFinalApproval(budgetRequestDTO.getForwardFor());
				// budgetRequestNote.setNote(budgetRequestDTO.getNote());

				BudgetRequestLog planLog = new BudgetRequestLog();
				planLog.setBudgetRequest(budgetRequest);
				planLog.setStage(CreditSalesDemandStatus.FINAL_APPROVED.toString());
				planLog.setRemarks(budgetRequestDTO.getRemarks());
				budgetRequestLogRepository.save(planLog);
				log.info("finalApproveBudgetRequest Successfully. [" + budgetRequestDTO.getId() + "]");

				String urlPath = VIEW_PAGE + "&budgetRequestId=" + budgetRequestDTO.getId() + "&";
				Long toUserId = budgetRequest.getCreatedBy().getId();
				notificationEmailService.saveIndividualNotification(loginService.getCurrentUser(), toUserId, urlPath,
						"Budget Request", "Budget Request has been final approved");

				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				log.info("BudgetRequest not Found");
				baseDTO.setStatusCode(ErrorDescription.BUDGET_NOT_FOUND.getCode());
			}

		} catch (Exception exception) {
			log.error("Exception finalApproveBudgetRequest ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO getAllBudgetRequestListLazy(PaginationDTO paginationDto) {
		log.info("<==== Starts BudgetRequestService.getAllBudgetRequestListLazy =====>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			Integer total = 0;
			Integer start = paginationDto.getFirst(), pageSize = paginationDto.getPageSize();
			start = start * pageSize;
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();

			UserMaster userMaster = loginService.getCurrentUser();
			Long loginUserId = userMaster == null ? null : userMaster.getId();

			ApplicationQuery applicationQuery = applicationQueryRepository
					.findByQueryName("BUDGET_REQUEST_LAZY_LIST_QUERY");

			if (applicationQuery == null || applicationQuery.getId() == null) {
				log.info("Application Query not found for query name : " + applicationQuery.getQueryName());
				return null;
			}

			String mainQuery = applicationQuery.getQueryContent().trim();

			mainQuery = StringUtils.replace(mainQuery, ":userId", String.valueOf(loginUserId));

			log.info("Query " + mainQuery);

			log.info("filters ::::::::" + paginationDto.getFilters());

			if (paginationDto.getFilters().get("budgetConfigName") != null
					&& !paginationDto.getFilters().get("budgetConfigName").toString().trim().isEmpty()) {
				String name = paginationDto.getFilters().get("budgetConfigName").toString().trim();
				mainQuery += " and ( upper(bc.name) like upper('%" + name + "%') or upper(bc.code) like upper('%" + name
						+ "%')) ";
			}
			if (paginationDto.getFilters().get("entityName") != null
					&& !paginationDto.getFilters().get("entityName").toString().trim().isEmpty()) {
				String name = paginationDto.getFilters().get("entityName").toString().trim();
				if (AppUtil.isInteger(name))
					mainQuery += " and em.code=" + Integer.parseInt(name) + " ";
				else
					mainQuery += " and upper(em.name) like upper('%" + name + "%') ";
			}

			if (paginationDto.getFilters().get("createdDate") != null) {
				Date createdDate = new Date((Long) paginationDto.getFilters().get("createdDate"));
				mainQuery += " and date(br.created_date)=date('" + createdDate + "')";
			}
			if (paginationDto.getFilters().get("status") != null) {
				String name = paginationDto.getFilters().get("status").toString().trim();
				mainQuery += " and brl.stage='" + name + "'";
			}

			String countQuery = "select count(*) as count from (" + mainQuery + ")t ";

			/*
			 * String countQuery = mainQuery.replace("SELECT\n" +
			 * "   br.id as BUDGET_REQUEST_ID,\n" + "   bc.code as BUDGET_CONFIG_CODE,\n" +
			 * "   bc.name as BUDGET_CONFIG_NAME,\n" + "   em.name as ENTITY_NAME,\n" +
			 * "   em.code as ENTITY_CODE,\n" + "   br.created_date as CREATED_DATE,\n" +
			 * "   brl.stage as STAGE", "select count(*) as count ");
			 */
			log.info("count query... " + countQuery);
			countData = jdbcTemplate.queryForList(countQuery);
			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					total = Integer.parseInt(data.get("count").toString().replace(",", ""));
			}

			if (paginationDto.getSortField() == null)
				mainQuery += " order by br.id desc limit " + pageSize + " offset " + start + ";";

			if (paginationDto.getSortField() != null && paginationDto.getSortOrder() != null) {
				log.info("Sort Field:[" + paginationDto.getSortField() + "] Sort Order:[" + paginationDto.getSortOrder()
						+ "]");
				if (paginationDto.getSortField().equals("budgetConfigName")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by bc.code asc ";
				if (paginationDto.getSortField().equals("budgetConfigName")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by bc.code desc ";

				if (paginationDto.getSortField().equals("entityName")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by em.code asc  ";
				if (paginationDto.getSortField().equals("entityName")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by em.code desc  ";

				if (paginationDto.getSortField().equals("createdDate")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by br.created_date asc ";
				if (paginationDto.getSortField().equals("createdDate")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by br.created_date desc ";

				if (paginationDto.getSortField().equals("status") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by brl.stage asc ";
				if (paginationDto.getSortField().equals("status") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by brl.stage desc ";

				mainQuery += " limit " + pageSize + " offset " + start + ";";
			}
			log.info("Main Qury....." + mainQuery);
			List<BudgetRequestDTO> budgetRequestDtoList = new ArrayList<>();
			listofData = jdbcTemplate.queryForList(mainQuery);
			for (Map<String, Object> data : listofData) {
				BudgetRequestDTO dto = new BudgetRequestDTO();
				if (data.get("BUDGET_REQUEST_ID") != null)
					dto.setId(Long.parseLong(data.get("BUDGET_REQUEST_ID").toString()));

				if (data.get("BUDGET_CONFIG_CODE") != null)
					dto.setBudgetConfigCode((data.get("BUDGET_CONFIG_CODE").toString()));

				if (data.get("BUDGET_CONFIG_NAME") != null)
					dto.setBudgetConfigName((data.get("BUDGET_CONFIG_NAME").toString()));

				if (data.get("ENTITY_NAME") != null)
					dto.setEntityName((data.get("ENTITY_NAME").toString()));

				if (data.get("ENTITY_CODE") != null)
					dto.setEntityCode(Integer.parseInt((data.get("ENTITY_CODE").toString())));

				if (data.get("CREATED_DATE") != null) {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					Date dt = sdf.parse(data.get("CREATED_DATE").toString());
					dto.setCreatedDate(dt);
				}
				if (data.get("STAGE") != null)
					dto.setStatus((data.get("STAGE").toString()));
				budgetRequestDtoList.add(dto);
			}
			log.info("Returning budgetRequestDtoList...." + budgetRequestDtoList.size());
			log.info("Total records present in budgetRequestDtoList..." + total);
			baseDTO.setResponseContent(budgetRequestDtoList);
			baseDTO.setTotalRecords(total);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

			/*
			 * Session session = entityManager.unwrap(Session.class); Criteria criteria =
			 * session.createCriteria(BudgetRequest.class,"budgetRequest");
			 * 
			 * criteria.createAlias("budgetRequest.budgetRequestLogList",
			 * "budgetRequestLog");
			 * criteria.createAlias("budgetRequest.budgetConfig","budgetConfig",
			 * JoinType.LEFT_OUTER_JOIN);
			 * criteria.createAlias("budgetRequest.entityMaster","entityMaster",
			 * JoinType.LEFT_OUTER_JOIN);
			 * 
			 * if(paginationDto.getFilters() != null) {
			 * log.info("budgetRequest filters :::"+paginationDto.getFilters());
			 * 
			 * if(paginationDto.getFilters().get("budgetConfigName") != null &&
			 * !paginationDto.getFilters().get("budgetConfigName").toString().trim().isEmpty
			 * ()){
			 * 
			 * Criterion name =
			 * Restrictions.like("budgetConfig.name","%"+paginationDto.getFilters().get(
			 * "budgetConfigName").toString().trim()+"%").ignoreCase();
			 * 
			 * Criterion code =
			 * Restrictions.like("budgetConfig.code","%"+paginationDto.getFilters().get(
			 * "budgetConfigName").toString().trim()+"%").ignoreCase();
			 * 
			 * criteria.add(Restrictions.or(name,code)); }
			 * 
			 * if(paginationDto.getFilters().get("entityName") != null &&
			 * !paginationDto.getFilters().get("entityName").toString().trim().isEmpty()){
			 * String entity = (String) paginationDto.getFilters().get("entityName");
			 * if(AppUtil.isInteger(entity))
			 * criteria.add(Restrictions.eq("entityMaster.code",Integer.parseInt(entity)));
			 * else
			 * criteria.add(Restrictions.like("entityMaster.name","%"+entity+"%").ignoreCase
			 * ()); }
			 * 
			 * 
			 * if(paginationDto.getFilters().get("budgetConfig.name") != null &&
			 * !paginationDto.getFilters().get("budgetConfig.name").toString().trim().
			 * isEmpty()){
			 * 
			 * Criterion name =
			 * Restrictions.like("budgetConfig.name","%"+paginationDto.getFilters().get(
			 * "budgetConfig.name").toString().trim()+"%").ignoreCase();
			 * 
			 * Criterion code =
			 * Restrictions.like("budgetConfig.code","%"+paginationDto.getFilters().get(
			 * "budgetConfig.name").toString().trim()+"%").ignoreCase();
			 * 
			 * criteria.add(Restrictions.or(name,code)); }
			 * 
			 * if(paginationDto.getFilters().get("entityMaster.Name") != null &&
			 * !paginationDto.getFilters().get("entityMaster.Name").toString().trim().
			 * isEmpty()){ String entity = (String)
			 * paginationDto.getFilters().get("entityMaster.Name");
			 * if(AppUtil.isInteger(entity))
			 * criteria.add(Restrictions.eq("entityMaster.code",Integer.parseInt(entity)));
			 * else
			 * criteria.add(Restrictions.like("entityMaster.name","%"+entity+"%").ignoreCase
			 * ()); }
			 * 
			 * if(paginationDto.getFilters().get("createdDate") != null) { Date date = new
			 * Date((long) paginationDto.getFilters().get("createdDate")); DateFormat
			 * dateFormat = new SimpleDateFormat("dd-MM-yyyy"); String strDate =
			 * dateFormat.format(date); Date minDate = dateFormat.parse(strDate); Date
			 * maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
			 * criteria.add(Restrictions.conjunction().add(Restrictions.ge(
			 * "budgetRequest.createdDate", minDate))
			 * .add(Restrictions.lt("budgetRequest.createdDate", maxDate))); }
			 * 
			 * if(paginationDto.getFilters().get("status") != null){
			 * criteria.add(Restrictions.eq("budgetRequestLog.stage",
			 * paginationDto.getFilters().get("status").toString())); }
			 * 
			 * 
			 * 
			 * DetachedCriteria logCriteria =
			 * DetachedCriteria.forClass(RetailProductionPlanLog.class, "planLogSubQuery");
			 * logCriteria.setProjection(Projections.projectionList().add(Projections.max(
			 * "createdDate"))); logCriteria.add(Restrictions.eqProperty(
			 * "planLogSubQuery.retailProductionPlan.id", "plan.id"));
			 * criteria.add(Subqueries.propertyEq("planLog.createdDate", logCriteria));
			 * 
			 * criteria.setProjection(Projections.rowCount()); Long totalResult = (long)
			 * ((Long) criteria.uniqueResult()).intValue(); criteria.setProjection(null);
			 * 
			 * ProjectionList projectionList = Projections.projectionList();
			 * projectionList.add(Projections.property("budgetRequest.id")); //
			 * projectionList.add(Projections.property("entityMaster.code")); //
			 * projectionList.add(Projections.property("entityMaster.name")); //
			 * projectionList.add(Projections.property("budgetConfig.name")); //
			 * projectionList.add(Projections.property("budgetConfig.code"));
			 * projectionList.add(Projections.property("budgetRequest.budgetConfig"));
			 * projectionList.add(Projections.property("budgetRequest.entityMaster"));
			 * projectionList.add(Projections.property("budgetRequest.createdDate"));
			 * projectionList.add(Projections.property("budgetRequestLog.stage"));
			 * 
			 * // this block of code is written for get the latest STATUS from the
			 * 'budgetRequestLog' DetachedCriteria logCriteria =
			 * DetachedCriteria.forClass(BudgetRequestLog.class,"budgetLog");
			 * logCriteria.setProjection(Projections.projectionList().add(Projections.max(
			 * "createdDate")));
			 * logCriteria.add(Restrictions.eqProperty("budgetLog.budgetRequest.id",
			 * "budgetRequest.id"));
			 * criteria.add(Subqueries.propertyEq("budgetRequestLog.createdDate",
			 * logCriteria));
			 * 
			 * criteria.setProjection(projectionList);
			 * 
			 * criteria.setFirstResult(paginationDto.getFirst() *
			 * paginationDto.getPageSize());
			 * criteria.setMaxResults(paginationDto.getPageSize());
			 * 
			 * if ( paginationDto.getSortField() != null && paginationDto.getSortOrder() !=
			 * null ) { if (paginationDto.getSortOrder().equals("DESCENDING")) {
			 * criteria.addOrder(Order.desc(paginationDto.getSortField())); } else {
			 * criteria.addOrder(Order.asc(paginationDto.getSortField())); } } else {
			 * criteria.addOrder(Order.desc("budgetRequest.id")); }
			 * 
			 * List<?> resultList = criteria.list(); if (resultList == null ||
			 * resultList.isEmpty() || resultList.size() == 0) {
			 * log.info(":: budgetRequest list is null or empty ::"); }
			 * 
			 * List<BudgetRequestDTO> budgetRequestDtoList = new ArrayList<>();
			 * 
			 * Iterator<?> it = resultList.iterator();
			 * 
			 * while (it.hasNext()) { Object obj[] = (Object[]) it.next(); BudgetRequestDTO
			 * dto = new BudgetRequestDTO(); dto.setId((Long) obj[0]);
			 * dto.setBudgetConfig((BudgetConfig) obj[1]);
			 * dto.setEntityMaster((EntityMaster) obj[2]); // dto.setEntityCode((Integer)
			 * obj[1]); // dto.setEntityName((String) obj[2]); //
			 * dto.setBudgetConfigName((String) obj[3]); // dto.setBudgetConfigCode((String)
			 * obj[4]); dto.setCreatedDate((Date) obj[3]); //
			 * dto.setStatus(obj[4].toString()); budgetRequestDtoList.add(dto); }
			 * response.setResponseContents(budgetRequestDtoList);
			 * response.setTotalRecords(totalResult.intValue());
			 * response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());}
			 */
		} catch (Exception e) {
			log.error("Exception occured in BudgetRequestService.getAllBudgetRequestListLazy..", e);
			baseDTO.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<==== Ends BudgetRequestService.getAllBudgetRequestListLazy =====>");
		return responseWrapper.send(baseDTO);
	}

	public String getMonthAsString(Integer key) {
		Map<Integer, String> map = new LinkedHashMap<>();
		map.put(1, "Jan");
		map.put(2, "Feb");
		map.put(3, "Mar");
		map.put(4, "Apr");
		map.put(5, "May");
		map.put(6, "Jun");
		map.put(7, "Jul");
		map.put(8, "Aug");
		map.put(9, "Sep");
		map.put(10, "Oct");
		map.put(11, "Nov");
		map.put(12, "Dec");
		log.info(" key as ::" + map.get(key));
		return map.get(key);
	}

	public BaseDTO findEmployeeDetailsByLoggedInUser(Long userId) {
		log.info(" findEmployeeDetailsByLoggedInUser with user Id " + userId);
		BaseDTO baseDTO = new BaseDTO();
		try {

			EmployeeMaster empDetails = employeeMasterRepository.findEmployeeByUser(userId);
			if (empDetails != null) {
				log.info("Employee Details Exist for the User : " + userId);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				log.info("Employee Details Not Found for the User : " + userId);
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			}
			baseDTO.setResponseContent(empDetails);
		} catch (Exception exp) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Employee findEmployeeDetailsByLoggedInUser failed: :", exp);
		}
		return baseDTO;
	}
}