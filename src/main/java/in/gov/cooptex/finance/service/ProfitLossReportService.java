package in.gov.cooptex.finance.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.repository.AppQueryRepository;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.dto.ProfitLossReportDTO;
import in.gov.cooptex.finance.dto.ReceiptPaymentRequestDTO;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class ProfitLossReportService {

	@PersistenceContext
	EntityManager entityManager;
	
	@Autowired 
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	AppQueryRepository appQueryRepository;

	public BaseDTO getProfitLossReport(ReceiptPaymentRequestDTO receiptPaymentRequestDTO) {
		log.info("ProfitLossReportService getProfitLossReport method started : ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			validationProfitLossReport(receiptPaymentRequestDTO);
			
			log.info("receiptPaymentRequestDTO==>" + receiptPaymentRequestDTO);
			log.info("fromDate==>" + receiptPaymentRequestDTO.getFDate());
			log.info("toDate==>" + receiptPaymentRequestDTO.getTDate());
			
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");  
		    String fDate= formatter.format(receiptPaymentRequestDTO.getFDate());  
		    String tDate= formatter.format(receiptPaymentRequestDTO.getTDate());
		    
		    log.info("fDate==>" + fDate); 
			log.info("tDate==>" + tDate);
		    
		   /* String query = "select ga.name, ga.code, gac.code cagtegorycode, sum( atd.amount ) as amount from account_transaction_details atd "
		    		+ " join gl_account ga on atd.gl_account_id = ga.id join gl_account_group gag on ga.gl_account_group_id = gag.id join "
		    		+ " gl_account_category gac on gag.category_id = gac.id where gac.code in('EXPENSE') and date(atd.created_date) "
		    		+ " between ':fDate' and ':tDate' group by ga.id,gac.code union all select ga.name,ga.code,gac.code as "
		    		+ " cagtegorycode,sum(atd.amount) as income from  account_transaction_details atd  join gl_account ga on atd.gl_account_id="
		    		+ " ga.id join gl_account_group gag on ga.gl_account_group_id=gag.id join gl_account_category gac on gag.category_id="
		    		+ " gac.id where gac.code in ('INCOME') and date(atd.created_date) between ':fDate' and ':tDate' "
		    		+ " group by ga.id,gac.code;";*/
			ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("PROFIT_LOSS_QUERY");
			String query = applicationQuery.getQueryContent();
		    query = query.replace(":fDate", fDate);
			query = query.replace(":tDate", tDate);
		    
		    log.info("query==>" + query);
		    
		    baseDTO.setResponseContent(jdbcTemplate.query(query, new RowMapper<ProfitLossReportDTO>() {

				@Override
				public ProfitLossReportDTO mapRow(ResultSet rs, int no) throws SQLException {
					ProfitLossReportDTO profitLossReportDTO = new ProfitLossReportDTO();
					profitLossReportDTO.setReportParticular(rs.getString("name"));
					profitLossReportDTO.setReportCode(rs.getString("code"));
					profitLossReportDTO.setReportType(rs.getString("cagtegorycode"));
					profitLossReportDTO.setAmount(rs.getDouble("amount"));
					return profitLossReportDTO;
				}
			}));
			log.info("<--- Successfully fetched the records from DB ---> ");
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			
			
		}catch (RestException e) {
			log.info("<--- Exception in getReceiptPaymentReport() ---> ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}catch (Exception e) {
			log.info("<--- Exception in getReceiptPaymentReport() ---> ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return baseDTO;
	}
	
	private void validationProfitLossReport(ReceiptPaymentRequestDTO receiptPaymentRequestDTO) {
		
		Validate.notNull(receiptPaymentRequestDTO.getFDate(), ErrorDescription.INTEND_REQUEST_FROM_DATE_REQUIRED);
		Validate.notNull(receiptPaymentRequestDTO.getTDate(), ErrorDescription.INTEND_REQUEST_TO_DATE_REQUIRED);
		if(receiptPaymentRequestDTO.getFDate().after(receiptPaymentRequestDTO.getTDate())) {
			throw new RestException(ErrorDescription.TODATE_SHOULD_GREATER_THAN_FROMDATE);
		}
	}
}
