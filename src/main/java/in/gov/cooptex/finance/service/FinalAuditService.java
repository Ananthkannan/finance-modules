package in.gov.cooptex.finance.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.model.FinalAuditDetails;
import in.gov.cooptex.core.accounts.model.FinalAuditStocksCheck;
import in.gov.cooptex.core.accounts.model.MarketingInspectionEntity;
import in.gov.cooptex.core.accounts.model.MarketingInspectionInventory;
import in.gov.cooptex.core.accounts.model.MarketingInspectionModernization;
import in.gov.cooptex.core.accounts.model.MarketingInspectionProfitability;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.AddressMaster;
import in.gov.cooptex.core.model.BuildingType;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.repository.AddressMasterRepository;
import in.gov.cooptex.core.repository.BuildingTypeRepository;
import in.gov.cooptex.core.repository.DesignationRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.ProductVarietyMasterRepository;
import in.gov.cooptex.core.util.JdbcUtil;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.FinanceErrorCode;
import in.gov.cooptex.finance.advance.repository.FinalAuditDetailsRepository;
import in.gov.cooptex.finance.advance.repository.FinalAuditStocksCheckRepository;
import in.gov.cooptex.finance.advance.repository.MarketingInspectionEmployeeRepository;
import in.gov.cooptex.finance.advance.repository.MarketingInspectionInventoryRepository;
import in.gov.cooptex.finance.advance.repository.MarketingInspectionModernizationRepository;
import in.gov.cooptex.finance.advance.repository.MarketingInspectionProfitabilityRepository;
import in.gov.cooptex.finance.advance.repository.MarketingInspectionRepository;
import in.gov.cooptex.finance.dto.FinalAuditGoodsDtlsDto;
import in.gov.cooptex.finance.dto.FinalAuditOtherDtlsDto;
import in.gov.cooptex.finance.dto.FinalAuditResponseDto;
import in.gov.cooptex.finance.dto.FinalAuditSalesDtlsDto;
import in.gov.cooptex.finance.dto.FinalAuditStockDtlsDto;
import in.gov.cooptex.finance.dto.MarketingInspectionInventoryDtlsDto;
import in.gov.cooptex.finance.dto.MarketingInspectionProfitabilityDtlsDto;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class FinalAuditService {

	@Autowired
	EmployeeMasterRepository employeeMasterRepository;

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	FinalAuditDetailsRepository finalAuditDetailsRepository;

	@Autowired
	MarketingInspectionRepository marketingInspectionRepository;

	@Autowired
	MarketingInspectionEmployeeRepository marketingInspectionEmployeeRepository;

	@Autowired
	AddressMasterRepository addressMasterRepository;

	@Autowired
	ResponseWrapper responseWrapper;

	@Autowired
	ProductVarietyMasterRepository productVarietyMasterRepository;

	@Autowired
	MarketingInspectionProfitabilityRepository marketingInspectionProfitabilityRepository;

	@Autowired
	MarketingInspectionInventoryRepository marketingInspectionInventoryRepository;

	@Autowired
	MarketingInspectionModernizationRepository marketingInspectionModernizationRepository;

	@Autowired
	BuildingTypeRepository buildingTypeRepository;

	@PersistenceContext
	EntityManager entityManager;

	@Autowired
	JdbcTemplate jdbcTemplate;

	public BaseDTO getEmployeeByAutoComplete(String employeeSearchName) {
		log.info("=========START FinalAuditService.getEmployeeByAutoComplete=====");
		log.info("=========employeeSearchName is=====" + employeeSearchName);
		BaseDTO baseDto = new BaseDTO();
		try {
			if (employeeSearchName != null) {
				List<EmployeeMaster> employeeList = employeeMasterRepository
						.getEmployeeByAutoComplete(employeeSearchName);
				baseDto.setResponseContents(employeeList);
				baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			}
		} catch (Exception e) {
			log.info("=========Exception Occured in FinalAuditService.getEmployeeByAutoComplete=====");
			log.info("=========Exception is=====" + e.getMessage());
		}
		log.info("=========END FinalAuditService.getEmployeeByAutoComplete=====");
		return baseDto;
	}

	@Autowired
	DesignationRepository designationRepository;

	public BaseDTO loadAllInventoryDtlsTable(Long showroomId) {
		BaseDTO baseDto = new BaseDTO();
		try {
			MarketingInspectionInventoryDtlsDto marketingInspectionInventoryDtlsDto = new MarketingInspectionInventoryDtlsDto();
			marketingInspectionInventoryDtlsDto.setInventoryTurnoverList(new ArrayList<>());
			marketingInspectionInventoryDtlsDto.setStockPositionDateInspectionList(new ArrayList<>());
			marketingInspectionInventoryDtlsDto.setFastMovingVarietiesDetailsList(new ArrayList<>());
			marketingInspectionInventoryDtlsDto.setSlowMovingStagnatedVarietiesList(new ArrayList<>());
			/*
			 * marketingInspectionInventoryDtlsDto.getInventoryTurnoverList()
			 * .addAll(commonDataFetchmethod(showroomId,
			 * "MARKETINGAUDIT_INVENTORY_TURN_OVER"));
			 * 
			 * marketingInspectionInventoryDtlsDto.getStockPositionDateInspectionList()
			 * .addAll(commonDataFetchmethod(showroomId,
			 * "MARKETINGAUDIT_STOCK_POSITION_DATE_INSPECTION"));
			 * 
			 * marketingInspectionInventoryDtlsDto.getFastMovingVarietiesDetailsList()
			 * .addAll(commonDataFetchmethod(showroomId,
			 * "MARKETINGAUDIT_FASTMOVING_VARIETIES_DETAILS"));
			 * 
			 * marketingInspectionInventoryDtlsDto.getSlowMovingStagnatedVarietiesList()
			 * .addAll(commonDataFetchmethod(showroomId,
			 * "MARKETINGAUDIT_SLOWMOVING_STAGNATED_VARIETIES"));
			 */

			baseDto.setResponseContent(marketingInspectionInventoryDtlsDto);
			baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());

		} catch (Exception e) {
			log.info("=========Exception Occured in FinalAuditService.loadAllInventoryDtlsTable=====", e);
			log.info("=========Exception is=====" + e.getMessage());
		}

		return baseDto;
	}

	public BaseDTO loadProfitabilityDtlsTable(Long showroomId) {
		BaseDTO baseDto = new BaseDTO();
		try {
			MarketingInspectionProfitabilityDtlsDto marketingInspectionProfitabilityDtlsDto = new MarketingInspectionProfitabilityDtlsDto();
			// marketingInspectionProfitabilityDtlsDto.setProfitabilityLatComplFinYr(new
			// Map<K, V>());
			marketingInspectionProfitabilityDtlsDto.setProfitabilityLatComplFinYrList(new ArrayList<>());

			/*
			 * marketingInspectionProfitabilityDtlsDto.getProfitabilityLatComplFinYrList()
			 * .addAll(commonDataFetchmethod(showroomId,
			 * "PROFITABILITY_LATEST_COMPLETED_FINYEAR"));
			 */

			baseDto.setResponseContent(marketingInspectionProfitabilityDtlsDto);
			baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());

		} catch (Exception e) {
			log.info("=========Exception Occured in FinalAuditService.loadAllInventoryDtlsTable=====", e);
			log.info("=========Exception is=====" + e.getMessage());
		}

		return baseDto;
	}

	public BaseDTO getAllShowroomForRegion(Long regionId) {
		log.info("getAllShowroomForRegion regionId [" + regionId + "]");
		BaseDTO response = new BaseDTO();
		try {
			List<EntityMaster> showroomListForRegion = entityMasterRepository.getAllShowroomForRegion(regionId);
			if (showroomListForRegion != null) {
				log.info("getAllShowroomForRegion :: showroomListForRegion.size==> " + showroomListForRegion.size());
			} else {
				log.error("Showroom not found");
			}
			response.setResponseContent(showroomListForRegion);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());

		} catch (Exception e) {
			log.error("getAllShowroomForRegion ", e);
			response.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(response);
	}

	/**
	 * @return
	 */
	public BaseDTO getAllActiveRegions() {
		log.info("<--- Get all active regions service called --->");
		BaseDTO response = new BaseDTO();
		try {
			List<EntityMaster> regionList = entityMasterRepository.findActiveRegionalOffices();
			List<EntityMaster> responseList = new ArrayList<>();
			for (EntityMaster reg : regionList) {
				/*
				 * reg.getCreatedBy().setRegion(null); if (reg.getModifiedBy() != null)
				 * reg.getModifiedBy().setRegion(null);
				 */
				reg.setEntityTypeMaster(null);
				responseList.add(reg);
			}
			log.info("<---Region list size--->" + responseList.size());
			response.setResponseContents(responseList);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.error("Error while retiving active regions based on state----->", e);
			response.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(response);
	}

	public BaseDTO getProductVarietyNameAutoComplete(String productName) {
		log.info("=======START FinalAuditService.getProductVarietyNameAutoComplete======");
		log.info("=======productName is======" + productName);
		BaseDTO baseDto = new BaseDTO();
		try {
			if (productName != null) {
				List<ProductVarietyMaster> productVarietyMasterList = productVarietyMasterRepository
						.getProductByCodeOrNameAutoComplete(productName);
				baseDto.setResponseContents(productVarietyMasterList);
				baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			}
		} catch (Exception e) {
			log.info("=======Exception Occured in FinalAuditService.getProductVarietyNameAutoComplete======");
			log.info("=======Exception is======" + e.getMessage());
		}
		log.info("=======END FinalAuditService.getProductVarietyNameAutoComplete======");
		return baseDto;
	}

	public BaseDTO saveOrUpdateProfitability(MarketingInspectionProfitability marketingInspectionProfitability) {
		log.info("========START FinalAuditService.saveOrUpdateProfitability========");
		BaseDTO baseDto = new BaseDTO();
		try {
			if (marketingInspectionProfitability.getMarketingInspectionEntity() != null
					&& marketingInspectionProfitability.getMarketingInspectionEntity().getId() != null) {
				MarketingInspectionEntity marketingInspectionEntity = marketingInspectionRepository
						.findOne(marketingInspectionProfitability.getMarketingInspectionEntity().getId());
				marketingInspectionProfitability.setMarketingInspectionEntity(marketingInspectionEntity);
			}

			marketingInspectionProfitability.setVersion((long) 0);
			marketingInspectionProfitability = marketingInspectionProfitabilityRepository
					.save(marketingInspectionProfitability);
			baseDto.setResponseContent(marketingInspectionProfitability);
			baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.info("========Exception Occured in FinalAuditService.saveOrUpdateProfitability========", e);
			log.info("========Exception is========" + e.getMessage());
		}
		log.info("========END FinalAuditService.saveOrUpdateProfitability========");

		return baseDto;
	}

	public BaseDTO saveOrUpdateMarketingInspectionInventory(MarketingInspectionInventory marketingInspectionInventory) {
		log.info("=======START FinalAuditService.saveOrUpdateMarketingInspectionInventory======");
		BaseDTO baseDto = new BaseDTO();
		try {
			if (marketingInspectionInventory.getMarketingInspectionEntity() != null
					&& marketingInspectionInventory.getMarketingInspectionEntity().getId() != null) {
				MarketingInspectionEntity marketingInspectionEntity = marketingInspectionRepository
						.findOne(marketingInspectionInventory.getMarketingInspectionEntity().getId());
				marketingInspectionInventory.setMarketingInspectionEntity(marketingInspectionEntity);
			}
			marketingInspectionInventory.setVersion((long) 0);
			marketingInspectionInventory = marketingInspectionInventoryRepository.save(marketingInspectionInventory);
			baseDto.setResponseContent(marketingInspectionInventory);
			baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.info("=======Exception Occured in FinalAuditService.saveOrUpdateMarketingAuditStockArrangement======");
			log.info("=======Exception is======" + e.getMessage());
		}
		log.info("=======END FinalAuditService.saveOrUpdateMarketingAuditStockArrangement======");
		return baseDto;
	}

	public BaseDTO saveOrUpdateModernization(MarketingInspectionModernization marketingInspectionModernization) {
		log.info("========START FinalAuditService.saveOrUpdateModernization========");
		BaseDTO baseDto = new BaseDTO();
		try {
			if (marketingInspectionModernization.getMarketingInspectionEntity() != null
					&& marketingInspectionModernization.getMarketingInspectionEntity().getId() != null) {
				MarketingInspectionEntity marketingInspectionEntity = marketingInspectionRepository
						.findOne(marketingInspectionModernization.getMarketingInspectionEntity().getId());
				marketingInspectionModernization.setMarketingInspectionEntity(marketingInspectionEntity);
			}

			marketingInspectionModernization.setVersion((long) 0);
			marketingInspectionModernization = marketingInspectionModernizationRepository
					.save(marketingInspectionModernization);
			baseDto.setResponseContent(marketingInspectionModernization);
			baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.info("========Exception Occured in FinalAuditService.saveOrUpdateModernization========");
			log.info("========Exception is========" + e.getMessage());
		}
		log.info("========END FinalAuditService.saveOrUpdateModernization========");

		return baseDto;
	}

	public BaseDTO getAllBuildType() {
		log.info("========START FinalAuditService.getAllBuildType========");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<BuildingType> listBuildingType = buildingTypeRepository.getAll();
			baseDTO.setResponseContents(listBuildingType);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("<<======== ERROR BuildingTypeService---- getAll ::" + e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("========END FinalAuditService.getAllBuildType========");
		return baseDTO;
	}

	public BaseDTO getLazyLoadData(PaginationDTO paginationDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {

			log.info("========START FinalAuditService.getLazyLoadData========");
			Session session = entityManager.unwrap(Session.class);
			Criteria criteria = session.createCriteria(FinalAuditDetails.class, "finalAuditDetails");
			criteria.createAlias("finalAuditDetails.unitId", "unitId");
			log.info(":: Criteria search started ::");
			if (paginationDTO.getFilters() != null) {

				if (paginationDTO.getFilters().get("fromdate") != null) {
					Date date = new Date((long) paginationDTO.getFilters().get("fromdate"));
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = dateFormat.format(date);
					Date minDate = dateFormat.parse(strDate);
					Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
					criteria.add(Restrictions.conjunction()
							.add(Restrictions.ge("finalAuditDetails.periodAuditFrom", minDate))
							.add(Restrictions.lt("finalAuditDetails.periodAuditFrom", maxDate)));
				}

				if (paginationDTO.getFilters().get("todate") != null) {
					Date date = new Date((long) paginationDTO.getFilters().get("todate"));
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = dateFormat.format(date);
					Date minDate = dateFormat.parse(strDate);
					Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
					criteria.add(Restrictions.conjunction().add(Restrictions.ge("finalAuditDetails.todate", minDate))
							.add(Restrictions.lt("finalAuditDetails.todate", maxDate)));
				}

				String entityMasterCode = (String) paginationDTO.getFilters().get("sellingunit");
				if (entityMasterCode != null) {
					if (AppUtil.isInteger(entityMasterCode)) {
						EntityMaster entityMasterCodeValue = entityMasterRepository
								.findByCode(Integer.parseInt(entityMasterCode));
						criteria.add(Restrictions.sqlRestriction("cast(this_.entity_id  as varchar) like '%"
								+ entityMasterCodeValue.getId().toString().trim() + "%'"));
					} else {
						criteria.add(Restrictions.or(
								Restrictions.like("region.name", "%" + entityMasterCode.trim() + "%").ignoreCase()));
					}
				}
				if (paginationDTO.getFilters().get("dateofaudit") != null) {
					Date date = new Date((long) paginationDTO.getFilters().get("dateofaudit"));
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = dateFormat.format(date);
					Date minDate = dateFormat.parse(strDate);
					Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
					criteria.add(
							Restrictions.conjunction().add(Restrictions.ge("finalAuditDetails.dateOfAudit", minDate))
									.add(Restrictions.lt("finalAuditDetails.dateOfAudit", maxDate)));
				}

				criteria.setProjection(Projections.rowCount());
				Integer totalResult = ((Long) criteria.uniqueResult()).intValue();
				criteria.setProjection(null);

				ProjectionList projectionList = Projections.projectionList();
				projectionList.add(Projections.property("id"));
				projectionList.add(Projections.property("periodAuditFrom"));
				projectionList.add(Projections.property("periodAuditTo"));
				projectionList.add(Projections.property("unitId"));
				projectionList.add(Projections.property("dateOfAudit"));

				criteria.setProjection(projectionList);

				if (paginationDTO.getFirst() != null) {
					Integer pageNo = paginationDTO.getFirst();
					Integer pageSize = paginationDTO.getPageSize();
					if (pageNo != null && pageSize != null) {
						criteria.setFirstResult(pageNo * pageSize);
						criteria.setMaxResults(pageSize);
						log.info("PageNo : [" + pageNo + "] pageSize[" + pageSize + "]");
					}

					String sortField = paginationDTO.getSortField();
					String sortOrder = paginationDTO.getSortOrder();
					log.info("sortField outside : [" + sortField + "] sortOrder[" + sortOrder + "]");
					if (paginationDTO.getSortField() != null && paginationDTO.getSortOrder() != null) {
						log.info("sortField : [" + paginationDTO.getSortField() + "] sortOrder["
								+ paginationDTO.getSortOrder() + "]");

						if (paginationDTO.getSortField().equals("inspectedDate")) {
							sortField = "finalAuditDetails.inspectionDate";
						} else if (sortField.equals("lastDateVisit")) {
							sortField = "finalAuditDetails.lastDateVisit";
						} else if (sortField.equals("entityMasterCode")) {
							sortField = "entityMasterCode.code";
							sortField = "entityMasterCode.name";
						} else if (sortField.equals("id")) {
							sortField = "finalAuditDetails.id";
						}
						if (sortOrder.equals("DESCENDING")) {
							criteria.addOrder(Order.desc(sortField));
						} else {
							criteria.addOrder(Order.asc(sortField));
						}
					} else {
						criteria.addOrder(Order.desc("finalAuditDetails.modifiedDate"));
					}
				}
				List<?> resultList = criteria.list();

				if (resultList == null || resultList.isEmpty() || resultList.size() == 0) {
					log.info("finalAuditDetails List is null or empty ");
					baseDTO.setTotalRecords(totalResult);
					baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
					return baseDTO;
				} else {
					log.info("criteria list executed and the list size is : " + resultList.size());
				}
				List<FinalAuditDetails> finalAuditDetailsList = new ArrayList<>();
				Iterator<?> it = resultList.iterator();
				while (it.hasNext()) {
					Object ob[] = (Object[]) it.next();
					FinalAuditDetails response = new FinalAuditDetails();
					/* "id","lastDateVisit","entityMaster","inspectionDate","createdDate" */
					response.setId((Long) ob[0]);
					log.info("Id::::::::" + response.getId());
					// EmployeeMaster employeeMaster = (EmployeeMaster) ob[1];
					EntityMaster entityMaster = (EntityMaster) ob[3];
					response.setPeriodAuditFrom((Date) ob[1]);
					response.setPeriodAuditTo((Date) ob[2]);
					response.setDateOfAudit((Date) ob[4]);
					response.setUnitId(entityMaster);
					log.info(":: List Response ::" + response);
					finalAuditDetailsList.add(response);
				}
				baseDTO.setResponseContents(finalAuditDetailsList);
				baseDTO.setTotalRecords(totalResult);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (Exception exp) {
			log.error("Exception Cause : ", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("========END FinalAuditService.getLazyLoadData========");
		return baseDTO;
	}

	public BaseDTO getFinalAuditById(Long id) {
		log.info("========START FinalAuditService.getFinalAuditById========");
		log.info("========Id is========" + id);
		BaseDTO baseDto = new BaseDTO();
		FinalAuditResponseDto finalAuditResponseDto = new FinalAuditResponseDto();
		List<FinalAuditStocksCheck> finalAuditStocksCheckList = new ArrayList<>();
		try {
			if (id != null) {
				FinalAuditDetails finalAuditDetails = finalAuditDetailsRepository.findOne(id);
				finalAuditStocksCheckList = finalAuditStocksCheckRepository.getByFinalAuditDetailId(id);
				finalAuditResponseDto.setFinalAuditDetails(finalAuditDetails);
				finalAuditResponseDto.setFinalAuditStocksCheckList(finalAuditStocksCheckList);
				/*
				 * if (marketingInspectionEntity.getEntityMaster() != null &&
				 * marketingInspectionEntity.getEntityMaster().getEntityMasterRegion() != null
				 * &&
				 * marketingInspectionEntity.getEntityMaster().getEntityMasterRegion().getId()
				 * != null) { EntityMaster entityMaster = entityMasterRepository
				 * .findOne(marketingInspectionEntity.getEntityMaster().getEntityMasterRegion().
				 * getId()); marketingAuditResponseDto.setEntityMaster(entityMaster); }
				 */

				log.info("========finalAuditResponseDto is========" + finalAuditResponseDto.toString());
				baseDto.setResponseContent(finalAuditResponseDto);
				baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			} else {
				throw new Exception("Id Cannot Be Empty");
			}

		} catch (Exception e) {
			log.info("========Exception Occured in FinalAuditService.getFinalAuditById========");
			log.info("========Exception is========" + e.getMessage());
		}
		log.info("========End FinalAuditService.getFinalAuditById========");
		return baseDto;
	}

	/**
	 * 
	 * @return
	 */
	public BaseDTO getAllActiveShowrooms(String showroomCodeOrName) {
		log.info("FinalAuditService. getAllActiveShowrooms() - START");
		BaseDTO response = new BaseDTO();
		List<EntityMaster> showroomList = new ArrayList<>();
		try {
			showroomList = entityMasterRepository.getAllActiveByAutoComplete(showroomCodeOrName);
			int showroomListSize = showroomList != null ? showroomList.size() : 0;
			log.info("FinalAuditService. getAllActiveShowrooms() - showroomListSize: " + showroomListSize);

			if (!CollectionUtils.isEmpty(showroomList)) {
				List<EntityMaster> responseList = new ArrayList<>();
				for (EntityMaster obj : showroomList) {
					EntityMaster entityMaster = new EntityMaster();
					entityMaster.setId(obj.getId());
					entityMaster.setCode(obj.getCode());
					entityMaster.setName(obj.getName());
					responseList.add(entityMaster);
				}
				response.setResponseContent(responseList);
			}
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.error("Exception at getAllActiveShowrooms()", e);
			response.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("FinalAuditService. getAllActiveShowrooms() - END");
		return response;
	}

	public BaseDTO saveOrUpdateFinalAudit(FinalAuditDetails finalAuditDetails) {
		log.info("=========START FinalAuditService.saveOrUpdateFinalAudit=====");
		BaseDTO baseDto = new BaseDTO();
		try {

			if (finalAuditDetails != null) {

				if (finalAuditDetails.getId() == null) {
					Long showroomId = finalAuditDetails.getUnitId() != null ? finalAuditDetails.getUnitId().getId()
							: null;
					log.info("FinalAuditService.saveOrUpdateFinalAudit() - showroomId: " + showroomId);

					Long existCount = finalAuditDetailsRepository.getExistCountByShowroomIdAndInspectionPeriod(
							showroomId, finalAuditDetails.getDateOfAudit());
					existCount = existCount != null ? existCount.longValue() : 0;
					if (existCount > 0) {
						baseDto.setStatusCode(
								ErrorDescription.getError(FinanceErrorCode.AUDIT_DETAILS_ALREADY_EXIST).getErrorCode());
						return baseDto;
					}

				}

				if (finalAuditDetails.getConcRegionalAddrId() != null
						&& finalAuditDetails.getConcRegionalAddrId().getId() != null) {
					log.info("=========ADDRESS Id is=====" + finalAuditDetails.getConcRegionalAddrId().getId());
					AddressMaster addressMaster = addressMasterRepository
							.findOne(finalAuditDetails.getConcRegionalAddrId().getId());
					finalAuditDetails.setConcRegionalAddrId(addressMaster);
				}
				if (finalAuditDetails.getConcAsstdirHndlmTextAddrId() != null
						&& finalAuditDetails.getConcAsstdirHndlmTextAddrId().getId() != null) {
					log.info("=========ADDRESS Id is=====" + finalAuditDetails.getConcAsstdirHndlmTextAddrId().getId());
					AddressMaster addressMaster = addressMasterRepository
							.findOne(finalAuditDetails.getConcAsstdirHndlmTextAddrId().getId());
					finalAuditDetails.setConcAsstdirHndlmTextAddrId(addressMaster);
				}
				EntityMaster entityMaster = entityMasterRepository.findOne(finalAuditDetails.getUnitId().getId());
				finalAuditDetails.setUnitId(entityMaster);
				finalAuditDetails.setVersion((long) 0);
				finalAuditDetails = finalAuditDetailsRepository.save(finalAuditDetails);

				log.info("=========finalAuditDetails Saved SuccessFully=====");
				baseDto.setResponseContent(finalAuditDetails);
				baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			}

		} catch (Exception e) {
			log.info("=========Exception Occured in FinalAuditService.saveOrUpdateFinalAudit=====", e);
			log.info("=========Exception is=====" + e.getMessage());
		}
		log.info("=========END FinalAuditService.saveOrUpdateFinalAudit=====");
		return baseDto;
	}

	public BaseDTO saveOrUpdateSalesDetails(FinalAuditDetails finalAuditDetails) {
		log.info("=========START FinalAuditService.saveOrUpdateSalesDetails=====");
		BaseDTO baseDto = new BaseDTO();
		FinalAuditDetails finalAuditDetailsObj = new FinalAuditDetails();
		try {
			if (finalAuditDetails != null && finalAuditDetails.getId() != null) {
				finalAuditDetailsObj = finalAuditDetailsRepository.findOne(finalAuditDetails.getId());
				finalAuditDetailsObj.setRemarksAuditorViabilityUnit(finalAuditDetails.getRemarksAuditorViabilityUnit());
				finalAuditDetails = finalAuditDetailsRepository.save(finalAuditDetails);
				log.info("=========finalAuditDetails Saved SuccessFully=====");
				baseDto.setResponseContent(finalAuditDetails);
				baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			}
		} catch (Exception e) {
			log.info("=========Exception Occured in FinalAuditService.saveOrUpdateSalesDetails=====", e);
			log.info("=========Exception is=====" + e.getMessage());
		}
		log.info("=========END FinalAuditService.saveOrUpdateSalesDetails=====");
		return baseDto;
	}

	@Autowired
	FinalAuditStocksCheckRepository finalAuditStocksCheckRepository;

	public BaseDTO saveOrUpdateStockDetails(FinalAuditResponseDto finalAuditResponseDto) {
		log.info("=========START FinalAuditService.saveOrUpdateStockDetails=====");
		BaseDTO baseDto = new BaseDTO();
		try {

			if (finalAuditResponseDto != null) {
				Long auditDetailsId = finalAuditResponseDto.getFinalAuditDetails() != null
						? finalAuditResponseDto.getFinalAuditDetails().getId()
						: null;
				log.info("FinalAuditService.saveOrUpdateStockDetails() - auditDetailsId: " + auditDetailsId);
				if (auditDetailsId != null) {
					finalAuditStocksCheckRepository.deleteByAuditDetailsId(auditDetailsId);
					List<FinalAuditStocksCheck> finalAuditStocksCheckList = finalAuditResponseDto
							.getFinalAuditStocksCheckList();
					if (finalAuditStocksCheckList != null && !finalAuditStocksCheckList.isEmpty()) {
						for (FinalAuditStocksCheck finalAuditStkChk : finalAuditStocksCheckList) {
							if (finalAuditStkChk.getFinalAuditDetails() != null
									&& finalAuditStkChk.getFinalAuditDetails().getId() != null) {
								finalAuditStkChk.setFinalAuditDetails(finalAuditDetailsRepository
										.findOne(finalAuditStkChk.getFinalAuditDetails().getId()));
								finalAuditStkChk.setProductVariety(productVarietyMasterRepository
										.findOne(finalAuditStkChk.getProductVariety().getId()));
								finalAuditStkChk.setVersion((long) 0);
								finalAuditStocksCheckRepository.save(finalAuditStkChk);
							}
						}
						// baseDto.setResponseContent(finalAuditDetails);
					}
					baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
				}
			}
		} catch (Exception e) {
			log.info("=========Exception Occured in FinalAuditService.saveOrUpdateSalesDetails=====", e);
			log.info("=========Exception is=====" + e.getMessage());
		}
		log.info("=========END FinalAuditService.saveOrUpdateSalesDetails=====");
		return baseDto;
	}

	public BaseDTO saveorupdateOtherdetails(FinalAuditDetails finalAuditDetails) {
		log.info("=========START FinalAuditService.saveorupdateOtherdetails=====");
		BaseDTO baseDto = new BaseDTO();
		try {
			if (finalAuditDetails != null && finalAuditDetails.getId() != null) {
				/*
				 * finalAuditDetailsObj =
				 * finalAuditDetailsRepository.findOne(finalAuditDetails.getId());
				 * finalAuditDetailsObj.setRemarksAuditorViabilityUnit(finalAuditDetails.
				 * getRemarksAuditorViabilityUnit()); finalAuditDetails =
				 * finalAuditDetailsRepository.save(finalAuditDetails);
				 * log.info("=========finalAuditDetails Saved SuccessFully=====");
				 */
				baseDto.setResponseContent(finalAuditDetails);
				baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			}
		} catch (Exception e) {
			log.info("=========Exception Occured in FinalAuditService.saveorupdateOtherdetails=====", e);
			log.info("=========Exception is=====" + e.getMessage());
		}
		log.info("=========END FinalAuditService.saveorupdateOtherdetails=====");
		return baseDto;
	}

	public BaseDTO loadAllSalesDetailsDatatable(FinalAuditDetails finalAuditDetails) {
		BaseDTO baseDto = new BaseDTO();
		try {
			if (finalAuditDetails != null) {
				FinalAuditSalesDtlsDto finalAuditSalesDtlsDto = new FinalAuditSalesDtlsDto();
				finalAuditSalesDtlsDto.setSalesAndRemittanceParticulars1List(new ArrayList<>());
				finalAuditSalesDtlsDto.setSalesAndRemittanceParticulars2List(new ArrayList<>());
				finalAuditSalesDtlsDto.setMonthlyCreditSalesRecoveryParticularsList(new ArrayList<>());
				finalAuditSalesDtlsDto.setDetailsPettyCashExpenditureList(new ArrayList<>());
				finalAuditSalesDtlsDto.setCreditSalesOutstandingList(new ArrayList<>());
				finalAuditSalesDtlsDto.setAgeWiseCreditSalesOutstandingList(new ArrayList<>());
				finalAuditSalesDtlsDto.setMonthlyCreditSalesRecoveryParticularsListParticulars(new ArrayList<>());
				finalAuditSalesDtlsDto.setSalesDetailsTotalSales(new ArrayList<>());

				finalAuditSalesDtlsDto.getSalesAndRemittanceParticulars1List()
						.addAll(commonDataFetchmethod(finalAuditDetails.getUnitId().getId(),
								"SALES_AND_REMITTANCE_PARTICULARS_FORM1", finalAuditDetails.getPeriodAuditFrom(),
								finalAuditDetails.getPeriodAuditTo()));

				finalAuditSalesDtlsDto.getSalesAndRemittanceParticulars2List()
						.addAll(commonDataFetchmethod(finalAuditDetails.getUnitId().getId(),
								"SALES_AND_REMITTANCE_PARTICULARS_FORM2", finalAuditDetails.getPeriodAuditFrom(),
								finalAuditDetails.getPeriodAuditTo()));

//				finalAuditSalesDtlsDto.getMonthlyCreditSalesRecoveryParticularsList()
//						.addAll(commonDataFetchmethod(finalAuditDetails.getUnitId().getId(),
//								"MONTHLY_CREDIT_SALES_CUM_RECOVERY_PARTICULARS", finalAuditDetails.getPeriodAuditFrom(),
//								finalAuditDetails.getPeriodAuditTo()));

				List<Map<String, Object>> dataMapList = commonDataFetchmethod(finalAuditDetails.getUnitId().getId(),
						"DETAILS_OF_THE_PETTY_CASH_EXPENDITURE", finalAuditDetails.getPeriodAuditFrom(),
						finalAuditDetails.getPeriodAuditTo());
				if (!CollectionUtils.isEmpty(dataMapList)) {
					Map<String, List<Map<String, Object>>> groupMap = dataMapList.stream()
							.collect(Collectors.groupingBy(a -> a.get("key").toString()));
					Map<String, List<Map<String, Object>>> groupByMonth = dataMapList.stream()
							.collect(Collectors.groupingBy(a -> a.get("month").toString()));
					finalAuditSalesDtlsDto.getPettyCashExpenditureMap().putAll(groupMap);
					finalAuditSalesDtlsDto.getPettyCashMap().putAll(groupByMonth);
				}

				List<Map<String, Object>> mapList = commonDataFetchmethod(finalAuditDetails.getUnitId().getId(),
						"PETTY_CASH_GL_HEAD_CODE", finalAuditDetails.getPeriodAuditFrom(),
						finalAuditDetails.getPeriodAuditTo());
				if (!CollectionUtils.isEmpty(mapList)) {
					List<String> glCodeList = new ArrayList<>();
					for (Map<String, Object> map : mapList) {
						if (map.get("headCode") != null) {
							glCodeList.add(map.get("headCode").toString());
						}
					}
					finalAuditSalesDtlsDto.setHeadCodeList(glCodeList);
				}

				finalAuditSalesDtlsDto.getCreditSalesOutstandingList()
						.addAll(commonDataFetchmethod(finalAuditDetails.getUnitId().getId(), "CREDIT_SALES_OUTSTANDING",
								finalAuditDetails.getPeriodAuditFrom(), finalAuditDetails.getPeriodAuditTo()));

				finalAuditSalesDtlsDto.getAgeWiseCreditSalesOutstandingList()
						.addAll(commonDataFetchmethod(finalAuditDetails.getUnitId().getId(),
								"AGE_WISE_CLASSIFICATION_FOR_CREDIT_SALES_OUTSTANDING",
								finalAuditDetails.getPeriodAuditFrom(), finalAuditDetails.getPeriodAuditTo()));

				finalAuditSalesDtlsDto.getMonthlyCreditSalesRecoveryParticularsListParticulars()
						.addAll(commonDataFetchmethod(finalAuditDetails.getUnitId().getId(),
								"MONTHLY_CREDIT_SALES_PARTICULARS_DETAILS", finalAuditDetails.getPeriodAuditFrom(),
								finalAuditDetails.getPeriodAuditTo()));

				finalAuditSalesDtlsDto.getSalesDetailsTotalSales()
						.addAll(commonDataFetchmethod(finalAuditDetails.getUnitId().getId(),
								"SALES_DETAILS_TOTAL_SALES", finalAuditDetails.getPeriodAuditFrom(),
								finalAuditDetails.getPeriodAuditTo()));

				baseDto.setResponseContent(finalAuditSalesDtlsDto);
				
				baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			}

		} catch (Exception e) {
			log.info("Exception Occured in FinalAuditService.loadAllSalesDetailsDatatable()", e);
		}

		return baseDto;
	}

	public BaseDTO loadAllStockDtlsTable(FinalAuditDetails finalAuditDetails) {
		BaseDTO baseDto = new BaseDTO();
		try {
			if (finalAuditDetails != null) {
				FinalAuditStockDtlsDto finalAuditStockDtlsDto = new FinalAuditStockDtlsDto();

				finalAuditStockDtlsDto.setStockReconciliationStatementAItemsList(new ArrayList<>());
				finalAuditStockDtlsDto.setStockReconciliationStatementAJItemsList(new ArrayList<>());
				finalAuditStockDtlsDto.setStockReconciliationStatementBNJItemsList(new ArrayList<>());
				finalAuditStockDtlsDto.setStockReconciliationStatementCItemsList(new ArrayList<>());
				finalAuditStockDtlsDto.setStockReconciliationStatementDItemsList(new ArrayList<>());
				finalAuditStockDtlsDto.setStockReconciliationStatementOtherItemsList(new ArrayList<>());
				finalAuditStockDtlsDto.setAgeWiseClassificationofClosingStockList(new ArrayList<>());

				finalAuditStockDtlsDto.setStkReconStmntAItemsParticulars(new ArrayList<>());
				finalAuditStockDtlsDto.setStkReconStmntAJItemsParticulars(new ArrayList<>());
				finalAuditStockDtlsDto.setStkReconStmntBNJItemsParticulars(new ArrayList<>());
				finalAuditStockDtlsDto.setStkReconStmntCItemsParticulars(new ArrayList<>());
				finalAuditStockDtlsDto.setStkReconStmntDItemsParticulars(new ArrayList<>());
				finalAuditStockDtlsDto.setStkReconStmntOtherItemsParticulars(new ArrayList<>());

				// finalAuditStockDtlsDto.setDetailsofStocksTestCheckedList(new ArrayList<>());
				finalAuditStockDtlsDto.getStockReconciliationStatementAItemsList()
						.addAll(commonDataFetchmethod(finalAuditDetails.getUnitId().getId(),
								"STOCK_RECONCILIATION_STATEMENT_A_ITEMS", finalAuditDetails.getPeriodAuditFrom(),
								finalAuditDetails.getPeriodAuditTo()));

				finalAuditStockDtlsDto.getStockReconciliationStatementAJItemsList()
						.addAll(commonDataFetchmethod(finalAuditDetails.getUnitId().getId(),
								"STOCK_RECONCILIATION_STATEMENT_AJ_ITEMS", finalAuditDetails.getPeriodAuditFrom(),
								finalAuditDetails.getPeriodAuditTo()));

				finalAuditStockDtlsDto.getStockReconciliationStatementBNJItemsList()
						.addAll(commonDataFetchmethod(finalAuditDetails.getUnitId().getId(),
								"STOCK_RECONCILIATION_STATEMENT_BNJ_ITEMS", finalAuditDetails.getPeriodAuditFrom(),
								finalAuditDetails.getPeriodAuditTo()));

				finalAuditStockDtlsDto.getStockReconciliationStatementCItemsList()
						.addAll(commonDataFetchmethod(finalAuditDetails.getUnitId().getId(),
								"STOCK_RECONCILIATION_STATEMENT_C_ITEMS", finalAuditDetails.getPeriodAuditFrom(),
								finalAuditDetails.getPeriodAuditTo()));

				finalAuditStockDtlsDto.getStockReconciliationStatementDItemsList()
						.addAll(commonDataFetchmethod(finalAuditDetails.getUnitId().getId(),
								"STOCK_RECONCILIATION_STATEMENT_D_ITEMS", finalAuditDetails.getPeriodAuditFrom(),
								finalAuditDetails.getPeriodAuditTo()));

				finalAuditStockDtlsDto.getStockReconciliationStatementOtherItemsList()
						.addAll(commonDataFetchmethod(finalAuditDetails.getUnitId().getId(),
								"STOCK_RECONCILIATION_STATEMENT_OTHER_ITEMS", finalAuditDetails.getPeriodAuditFrom(),
								finalAuditDetails.getPeriodAuditTo()));

				finalAuditStockDtlsDto.getAgeWiseClassificationofClosingStockList()
						.addAll(commonDataFetchmethod(finalAuditDetails.getUnitId().getId(),
								"AGE_WISE_CLASSIFICATION_OF_CLOSING_STOCK", finalAuditDetails.getPeriodAuditFrom(),
								finalAuditDetails.getPeriodAuditTo()));

				finalAuditStockDtlsDto.getStkReconStmntAItemsParticulars()
						.addAll(commonDataFetchmethod(finalAuditDetails.getUnitId().getId(),
								"STOCK_RECONCILIATION_STATEMENT_A_ITEMS_PARTICULARS",
								finalAuditDetails.getPeriodAuditFrom(), finalAuditDetails.getPeriodAuditTo()));

				finalAuditStockDtlsDto.getStkReconStmntAJItemsParticulars()
						.addAll(commonDataFetchmethod(finalAuditDetails.getUnitId().getId(),
								"STOCK_RECONCILIATION_STATEMENT_AJ_ITEMS_PARTICULARS",
								finalAuditDetails.getPeriodAuditFrom(), finalAuditDetails.getPeriodAuditTo()));

				finalAuditStockDtlsDto.getStkReconStmntBNJItemsParticulars()
						.addAll(commonDataFetchmethod(finalAuditDetails.getUnitId().getId(),
								"STOCK_RECONCILIATION_STATEMENT_BNJ_ITEMS_PARTICULARS",
								finalAuditDetails.getPeriodAuditFrom(), finalAuditDetails.getPeriodAuditTo()));

				finalAuditStockDtlsDto.getStkReconStmntCItemsParticulars()
						.addAll(commonDataFetchmethod(finalAuditDetails.getUnitId().getId(),
								"STOCK_RECONCILIATION_STATEMENT_C_ITEMS_PARTICULARS",
								finalAuditDetails.getPeriodAuditFrom(), finalAuditDetails.getPeriodAuditTo()));

				finalAuditStockDtlsDto.getStkReconStmntDItemsParticulars()
						.addAll(commonDataFetchmethod(finalAuditDetails.getUnitId().getId(),
								"STOCK_RECONCILIATION_STATEMENT_D_ITEMS_PARTICULARS",
								finalAuditDetails.getPeriodAuditFrom(), finalAuditDetails.getPeriodAuditTo()));

				finalAuditStockDtlsDto.getStkReconStmntOtherItemsParticulars()
						.addAll(commonDataFetchmethod(finalAuditDetails.getUnitId().getId(),
								"STOCK_RECONCILIATION_STATEMENT_OTHER_ITEMS_PARTICULARS",
								finalAuditDetails.getPeriodAuditFrom(), finalAuditDetails.getPeriodAuditTo()));

				baseDto.setResponseContent(finalAuditStockDtlsDto);
				baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			}

		} catch (Exception e) {
			log.info("=========Exception Occured in FinalAuditService.loadAllSalesDetailsDatatable=====", e);
			log.info("=========Exception is=====" + e.getMessage());
		}

		return baseDto;
	}

	public BaseDTO loadAllGoodDtlsTable(FinalAuditDetails finalAuditDetails) {
		BaseDTO baseDto = new BaseDTO();
		try {
			if (finalAuditDetails != null) {
				FinalAuditGoodsDtlsDto finalAuditGoodsDtlsDto = new FinalAuditGoodsDtlsDto();

				finalAuditGoodsDtlsDto.setDetailsofGoodsReceived1List(new ArrayList<>());
				finalAuditGoodsDtlsDto.setDetailsofGoodsReceived2List(new ArrayList<>());
				// finalAuditStockDtlsDto.setDetailsofStocksTestCheckedList(new ArrayList<>());
				finalAuditGoodsDtlsDto.getDetailsofGoodsReceived1List()
						.addAll(commonDataFetchmethod(finalAuditDetails.getUnitId().getId(),
								"DETAILS_OF_THE_GOODS_RECEIVED_ISSUE_INVOICE_NUM",
								finalAuditDetails.getPeriodAuditFrom(), finalAuditDetails.getPeriodAuditTo()));

				finalAuditGoodsDtlsDto.getDetailsofGoodsReceived2List()
						.addAll(commonDataFetchmethod(finalAuditDetails.getUnitId().getId(),
								"DETAILS_OF_THE_GOODS_RECEIVED_IET_NUM", finalAuditDetails.getPeriodAuditFrom(),
								finalAuditDetails.getPeriodAuditTo()));

				baseDto.setResponseContent(finalAuditGoodsDtlsDto);
				baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			}

		} catch (Exception e) {
			log.info("=========Exception Occured in FinalAuditService.loadAllSalesDetailsDatatable=====", e);
			log.info("=========Exception is=====" + e.getMessage());
		}

		return baseDto;
	}

	public BaseDTO loadAllOtherDtlsTable(FinalAuditDetails finalAuditDetails) {
		BaseDTO baseDto = new BaseDTO();
		try {
			if (finalAuditDetails != null) {
				FinalAuditOtherDtlsDto finalAuditOtherDtlsDto = new FinalAuditOtherDtlsDto();

				finalAuditOtherDtlsDto.setExpenditureStatementfortheUnitList(new ArrayList<>());
				finalAuditOtherDtlsDto.setDetailsOfTheStaffList(new ArrayList<>());

				finalAuditOtherDtlsDto.getExpenditureStatementfortheUnitList()
						.addAll(commonDataFetchmethod(finalAuditDetails.getUnitId().getId(),
								"EXPENDITURE_STATEMENT_FOR_THE_UNIT", finalAuditDetails.getPeriodAuditFrom(),
								finalAuditDetails.getPeriodAuditTo()));

				finalAuditOtherDtlsDto.getDetailsOfTheStaffList()
						.addAll(commonDataFetchmethod(finalAuditDetails.getUnitId().getId(),
								"FINAL_AUDIT_OTHER_DETAILS_OF_THE_STAFF", finalAuditDetails.getPeriodAuditFrom(),
								finalAuditDetails.getPeriodAuditTo()));

				baseDto.setResponseContent(finalAuditOtherDtlsDto);
				baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			}

		} catch (Exception e) {
			log.info("=========Exception Occured in FinalAuditService.loadAllSalesDetailsDatatable=====", e);
			log.info("=========Exception is=====" + e.getMessage());
		}

		return baseDto;
	}

	private List<Map<String, Object>> commonDataFetchmethod(Long entityId, String queryName, Date fromDate, Date toDate)
			throws Exception {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		List<Map<String, Object>> mapList = new ArrayList<>();
		String queryContent = JdbcUtil.getApplicationQuery(jdbcTemplate, queryName);
		if(StringUtils.isNotEmpty(queryContent)) {
			queryContent = queryContent.trim().replace(":entityId", entityId.toString());
			if (fromDate != null) {
				String fromDatestr = dateFormat.format(fromDate);
				queryContent = queryContent.trim().replace(":fromDate", "'" + fromDatestr + "'");
			}
			if (toDate != null) {
				String toDatestr = dateFormat.format(toDate);
				queryContent = queryContent.trim().replace(":toDate", "'" + toDatestr + "'");
			}
			log.info("queryContent - " + queryContent);
			mapList = jdbcTemplate.queryForList(queryContent);
		}
		return mapList;
	}
}
