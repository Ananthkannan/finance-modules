package in.gov.cooptex.finance.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import in.gov.cooptex.core.accounts.dto.CreateBalanceSheetDto;
import in.gov.cooptex.core.accounts.dto.CreateBalanceSheet;
import in.gov.cooptex.core.accounts.dto.PfPaymentDTO;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.AppFeature;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.FileMovementConfig;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class CreateBalanceSheetService {
	@Autowired
	ApplicationQueryRepository applicationQueryRepository;
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
		
	public String getQueryContent(String queryName) {
		log.info("CreateBalanceSheetService getQueryContent method started ]"+"[ queryname ]"+queryName);
		String query="";
		BaseDTO baseDTO = new BaseDTO();
		try {
			ApplicationQuery applicationQuery = applicationQueryRepository.findByQueryName(queryName);
			if (applicationQuery == null || applicationQuery.getId() == null) {
				log.info("Application Query not found for query name : " + applicationQuery);
				baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
			}
			query=applicationQuery.getQueryContent().trim();
			log.info("<================= getQueryContent query ===============>"+query);
		}catch (Exception exception) {
			log.error("CreateBalanceSheetService getQueryContent Exception ", exception);
		}
		return query;
	}
	public BaseDTO getDetailsForCreateBalanceSheetService(CreateBalanceSheetDto createBalanceSheetDto) {
		log.info("PfPaymentService getEntityWisePfPaymentDetails method started");
		BaseDTO baseDTO = new BaseDTO();String query="";String queryName="";
		/*List<Map<String, Object>> objectList = new ArrayList<Map<String, Object>>();*/
		log.info("createBalanceSheetDto "+createBalanceSheetDto.toString());
		List<CreateBalanceSheetDto> createBalanceSheetDtoList = new ArrayList<>();
		log.info("the createBalance sheet data in service"+createBalanceSheetDto.toString());
		String codeName = createBalanceSheetDto.getGlAccountCategory().getName().toUpperCase();
		log.info("codeName"+codeName);
		LocalDate localDate = LocalDate.now();
		log.info("current date is"+localDate);
		Long finId = createBalanceSheetDto.getFinancialYear().getId();
		log.info("the finId is"+finId);
		
		log.info("codition for getting query For Assets"+codeName.equalsIgnoreCase("Assets"));
		log.info("codition for getting query For Liability"+codeName.equalsIgnoreCase("LIABILITY"));
		if(codeName.equalsIgnoreCase("Assets")) {
			queryName="Create_Balance_Sheet_Asset";
		}else if(codeName.equalsIgnoreCase("LIABILITY")) {
			queryName="Create_Balance_Sheet_LIABILITY";
		}else {
			log.info("codeName is not either Assets or liability so we returning null baseDto to Browser ");
			return baseDTO;
		}
		query=getQueryContent(queryName);
		query=query.replace(":CategoryName", "'"+codeName+"'");
		query=query.replace(":CurrentDate", "'"+localDate+"'");
		query=query.replace(":finYearId", "'"+finId+"'");
		List<CreateBalanceSheet> objectList = jdbcTemplate.query(
				query, new BeanPropertyRowMapper<CreateBalanceSheet>(
						CreateBalanceSheet.class));
		log.info("the query is"+query);
		/*objectList = jdbcTemplate.queryForList(query);*/
		log.info("the finId is"+finId);
		log.info("objectList returned from Database is"+objectList.toString());
		log.info("objectList returned from Database is"+objectList.size());
		
		baseDTO.setResponseContents(objectList);
		baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		return baseDTO;
		
	}
	

}
