package in.gov.cooptex.finance.service;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.enums.VoucherStatus;
import in.gov.cooptex.core.accounts.enums.VoucherTypeDetails;
import in.gov.cooptex.core.accounts.model.AccountTransaction;
import in.gov.cooptex.core.accounts.model.AccountTransactionDetails;
import in.gov.cooptex.core.accounts.model.PurchaseInvoice;
import in.gov.cooptex.core.accounts.model.PurchaseInvoiceItems;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.VoucherDetails;
import in.gov.cooptex.core.accounts.model.VoucherLog;
import in.gov.cooptex.core.accounts.model.VoucherNote;
import in.gov.cooptex.core.accounts.model.VoucherType;
import in.gov.cooptex.core.accounts.repository.AccountTransactionRepository;
import in.gov.cooptex.core.accounts.repository.GlAccountRepository;
import in.gov.cooptex.core.accounts.repository.SupplierTypeMasterRepository;
import in.gov.cooptex.core.accounts.repository.VoucherDetailsRepository;
import in.gov.cooptex.core.accounts.repository.VoucherLogRepository;
import in.gov.cooptex.core.accounts.repository.VoucherNoteRepository;
import in.gov.cooptex.core.accounts.repository.VoucherRepository;
import in.gov.cooptex.core.accounts.repository.VoucherTypeRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.enums.SequenceName;
import in.gov.cooptex.core.finance.service.WeaversFinanceService;
import in.gov.cooptex.core.model.ApplicationQuery;

import in.gov.cooptex.core.model.EmployeePersonalInfoEmployment;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.SequenceConfig;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.purchase.model.PurchaseInvoiceItemTax;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.EmployeePersonalInfoEmploymentRepository;
import in.gov.cooptex.core.repository.PaymentModeRepository;
import in.gov.cooptex.core.repository.ProductVarietyMasterRepository;
import in.gov.cooptex.core.repository.ProductVarietyTaxRepository;
import in.gov.cooptex.core.repository.PurchaseInvoiceRepository;
import in.gov.cooptex.core.repository.PurchaseOrderItemsRepository;
import in.gov.cooptex.core.repository.PurchaseOrderRepository;
import in.gov.cooptex.core.repository.SequenceConfigRepository;
import in.gov.cooptex.core.repository.SupplierMasterRepository;
import in.gov.cooptex.core.repository.UomMasterRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.dto.pos.SupplierPaymentDTO;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.dto.SupplierPaymentViewDTO;
import in.gov.cooptex.finance.dto.VoucherDto;
import in.gov.cooptex.operation.model.ProductVarietyTax;
import in.gov.cooptex.operation.model.PurchaseOrder;
import in.gov.cooptex.operation.model.PurchaseOrderItems;
import in.gov.cooptex.operation.model.SupplierMaster;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class SupplierPaymentService {

	@PersistenceContext
	EntityManager entityManager;

	@Autowired
	PurchaseInvoiceRepository purchaseInvoiceRepository;

	@Autowired
	SupplierMasterRepository supplierMasterRepository;

	@Autowired
	ProductVarietyMasterRepository productVarietyMasterRepository;

	@Autowired
	UomMasterRepository uomMasterRepository;

	@Autowired
	ProductVarietyTaxRepository productVarietyTaxRepository;

	@Autowired
	AccountTransactionRepository accountTransactionRepository;

	@Autowired
	GlAccountRepository glAccountRepository;

	@Autowired
	PurchaseOrderRepository purchaseOrderRepository;

	@Autowired
	EmployeePersonalInfoEmploymentRepository employeePersonalInfoEmploymentRepository;

	@Autowired
	LoginService loginService;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	VoucherRepository voucherRepository;

	@Autowired
	PaymentModeRepository paymentModeRepository;

	@Autowired
	VoucherTypeRepository voucherTypeRepository;

	@Autowired
	VoucherLogRepository voucherLogRepository;

	@Autowired
	UserMasterRepository userMasterRepository;

	@Autowired
	VoucherNoteRepository voucherNoteRepository;

	@Autowired
	VoucherDetailsRepository voucherDetailsRepository;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	@Autowired
	SupplierTypeMasterRepository supplierTypeMasterRepository;

	@Autowired
	WeaversFinanceService weaversFinanceService;

	@Autowired
	ResponseWrapper responseWrapper;

	@Autowired
	PurchaseOrderItemsRepository purchaseOrderItemsRepository;

	@Transactional
	public BaseDTO createSupplierPayment(PurchaseInvoice purchaseInvoice) {
		log.info("SupplierPaymentService:createSupplierPayment");
		BaseDTO baseDTO = new BaseDTO();
		List<PurchaseInvoiceItemTax> invoiceItemTaxList = new ArrayList<>();
		try {
			log.info("createSupplierPayment::purchaseInvoice==>" + purchaseInvoice);
			if (purchaseInvoice != null && purchaseInvoice.getPurchaseOrder().getId() != null) {
				log.info("purchase order id==>" + purchaseInvoice.getPurchaseOrder().getId());
				purchaseInvoice
						.setPurchaseOrder(purchaseOrderRepository.findOne(purchaseInvoice.getPurchaseOrder().getId()));
			}
			if (purchaseInvoice != null && purchaseInvoice.getSupplierMaster().getId() != null) {
				purchaseInvoice.setSupplierMaster(
						supplierMasterRepository.findOne(purchaseInvoice.getSupplierMaster().getId()));
			}
			if (purchaseInvoice != null && purchaseInvoice.getPurchaseInvoiceItems() != null) {
				for (PurchaseInvoiceItems items : purchaseInvoice.getPurchaseInvoiceItems()) {
					log.info("product id==>" + items.getProductVarietyMaster() != null
							? items.getProductVarietyMaster().getId()
							: "Empty");
					log.info("uom id==>" + items.getUomMaster().getId());
					if (items != null && items.getProductVarietyMaster() != null
							&& items.getProductVarietyMaster().getId() != null) {
						items.setProductVarietyMaster(
								productVarietyMasterRepository.findOne(items.getProductVarietyMaster().getId()));
					}
					if (items != null && items.getUomMaster() != null && items.getUomMaster().getId() != null) {
						items.setUomMaster(uomMasterRepository.findOne(items.getUomMaster().getId()));
					}
					List<ProductVarietyTax> taxList = productVarietyTaxRepository
							.findTaxProductTaxs(items.getProductVarietyMaster().getId());
					for (ProductVarietyTax tax : taxList) {
						PurchaseInvoiceItemTax invocieItemTax = new PurchaseInvoiceItemTax();
						invocieItemTax.setProductVarietyTax(tax);
						invocieItemTax.setPurchaseInvoiceItems(items);
						log.info("taxvalue==>" + items.getTaxValue());
						invocieItemTax.setTaxValue(items.getTaxValue());
						invoiceItemTaxList.add(invocieItemTax);
					}
					items.setPurchaseInvoiceItemTaxList(invoiceItemTaxList);
				}
			}
			PurchaseInvoice invoice = purchaseInvoiceRepository.save(purchaseInvoice);
			baseDTO = weaversFinanceService.createProcurement(invoice);
			/*
			 * accountTransaction.setPurchaseInvoice(invoice);
			 * accountTransactionRepository.save(accountTransaction);
			 */
			if (baseDTO != null && baseDTO.getStatusCode().equals(ErrorDescription.SUCCESS_RESPONSE.getCode())) {
				log.info("<<:::inside Success Response Condition::::>>" + baseDTO.getStatusCode());
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
				// baseDTO.setResponseContent(invoice);
			} else {
				log.info("<<:::inside FAILURE_RESPONSE Condition ::::>>" + baseDTO != null ? baseDTO.getStatusCode()
						: "baseDto is null");
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
				purchaseInvoiceRepository.delete(invoice);
			}

		} catch (RestException e) {
			log.error("SupplierPaymentService createSupplierPayment Exception", e);
			baseDTO.setStatusCode(ErrorDescription.INTERNAL_ERROR.getCode());
		} catch (Exception e) {
			log.error("SupplierPaymentService createSupplierPayment Exception ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;

	}

	public BaseDTO createAccount(AccountTransaction accountTransaction) {
		log.info("SupplierPaymentService:createSupplierPayment");
		BaseDTO baseDTO = new BaseDTO();
		try {
			if (accountTransaction != null && accountTransaction.getAccountTransactionDetailsList() != null) {
				for (AccountTransactionDetails trAccount : accountTransaction.getAccountTransactionDetailsList()) {
					log.info("accid==>" + trAccount.getGlAccount().getId());
					if (trAccount.getGlAccount().getId() != null) {
						trAccount.setGlAccount(glAccountRepository.findOne(trAccount.getGlAccount().getId()));
					}
				}
			}
			AccountTransaction acc = accountTransactionRepository.save(accountTransaction);
			if (acc != null) {
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
				baseDTO.setResponseContent(acc);
			} else {
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			}
		} catch (RestException e) {
			log.error("SupplierPaymentService createAccount Exception", e);
			baseDTO.setStatusCode(ErrorDescription.INTERNAL_ERROR.getCode());
		} catch (Exception e) {
			log.error("SupplierPaymentService createAccount Exception ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO getInvoiceBillAmount(Long supplierId) {
		log.info("TenderEvaluationService generateTendereval method started ");
		BaseDTO baseDTO = new BaseDTO();

		try {
			/*
			 * String query =
			 * "select * from (select pi.id as invoiceId, pi.invoice_number as invoiceNum, pi.invoice_date as invoiceDate, "
			 * +
			 * " pi.grand_total as invoiceAmount, ps.amount as amountAdvance,(select sum(v.amount) from voucher_details v where "
			 * +
			 * " v.purchase_invoice_id = pi.id )as paidAmount, pi.grand_total-(select sum(v.amount) from voucher_details v where "
			 * +
			 * " v.purchase_invoice_id = pi.id )-ps.amount as balance, pi.payment_duedate as dueDate from purchase_invoice pi, "
			 * +
			 * " purchase_sales_advance ps where ps.purchase_order_id=pi.purchase_order_id and pi.supplier_id= :supId) x where x.balance>0;"
			 * ; query=query.replace(":supId", supplierId.toString());
			 */
			ApplicationQuery applicationQuery = applicationQueryRepository.findByQueryName("INVOICE_VOUCHER_QUERY");
			String query = applicationQuery.getQueryContent().trim();
			query = query.replace(":supId", supplierId.toString());
			log.info("query==>" + query);
			baseDTO.setResponseContent(jdbcTemplate.query(query, new RowMapper<SupplierPaymentDTO>() {

				@Override
				public SupplierPaymentDTO mapRow(ResultSet rs, int no) throws SQLException {
					SupplierPaymentDTO supplierPaymentDTO = new SupplierPaymentDTO();
					supplierPaymentDTO.setInvoiceId(rs.getLong("invoiceId"));
					supplierPaymentDTO.setInvoiceNumber(rs.getInt("invoiceNum"));
					supplierPaymentDTO.setInvoiceDate(rs.getDate("invoiceDate"));
					supplierPaymentDTO.setInvoiceAmount(rs.getDouble("invoiceAmount"));

					supplierPaymentDTO.setAdvanceAmount(rs.getDouble("amountAdvance"));
					supplierPaymentDTO.setAlreadyPaidAmount(rs.getDouble("paidAmount"));
					supplierPaymentDTO.setBalance(rs.getDouble("balance"));
					supplierPaymentDTO.setDueDate(rs.getDate("dueDate"));
					supplierPaymentDTO.setFinalPaidAmount(rs.getDouble("balance"));

					return supplierPaymentDTO;
				}
			}));
			log.info("<--- Successfully fetched the records from DB ---> ");
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.error("<--- Error in getInvoiceBillAmount() ---> ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return baseDTO;
	}

	@Autowired
	SequenceConfigRepository sequenceConfigRepository;

	public BaseDTO saveSupplierPayment(VoucherDto voucherDto) {
		log.info("SupplierPaymentService:saveSupplierPayment" + voucherDto.getTotalPaidAmount());
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("saveSupplierPayment voucherDto==>" + voucherDto);
			validateSupplierPayment(voucherDto);
			Voucher voucher = null;
			SequenceConfig sequenceConfig = null;
			// Formatter fmt = new Formatter();
			// Calendar cal = Calendar.getInstance();
			// fmt = new Formatter();
			// fmt.format("%tb", cal);
			// log.info(fmt);
			// Calendar calendar = Calendar.getInstance();
			// int year = calendar.get(Calendar.YEAR);
			Integer entityCode = 0;
			EmployeePersonalInfoEmployment employment = employeePersonalInfoEmploymentRepository
					.findWorkLocationByUserId(loginService.getCurrentUser().getId());
			sequenceConfig = sequenceConfigRepository
					.findBySequenceName(SequenceName.SUPPLIER_PAYMENT_REFERENCE_NUMBER);
			if (employment != null && employment.getWorkLocation() != null) {
				EntityMaster loginEntity = employment.getWorkLocation();
				if (loginEntity != null && loginEntity.getId() != null) {
					entityCode = loginEntity.getCode();
				}
			}
			if (voucherDto.getVoucherId() != null) {
				voucher = voucherRepository.findOne(voucherDto.getVoucherId());
				voucher.setReferenceNumberPrefix(voucher.getReferenceNumberPrefix());
			} else {
				voucher = new Voucher();
				String referenceNumberPrefix = entityCode + sequenceConfig.getSeparator() + sequenceConfig.getPrefix()
						+ sequenceConfig.getSeparator() + AppUtil.getCurrentMonthString()
						+ AppUtil.getCurrentYearString();

				log.info("referenceNumberPrefix==>" + referenceNumberPrefix);
				voucher.setReferenceNumberPrefix(referenceNumberPrefix);
			}
			VoucherType voucherType = voucherTypeRepository.findByName(VoucherTypeDetails.Payment.toString());
			voucher.setFromDate(voucherDto.getVoucherFromDate());
			voucher.setToDate(voucherDto.getVoucherToDate());
			voucher.setPaymentMode(paymentModeRepository.findById(voucherDto.getPayMode()));
			voucher.setNetAmount(voucherDto.getTotalPaidAmount());
			voucher.setSupplierType(supplierTypeMasterRepository.findOne(voucherDto.getSupplierTypeMaster().getId()));
			voucher.setVoucherType(voucherType);
			voucher.setName(VoucherTypeDetails.SUPPLIER_PAYMENT.toString());
			voucher.setReferenceNumber(sequenceConfig != null ? sequenceConfig.getCurrentValue() : null);
			voucher.setNarration(VoucherTypeDetails.SUPPLIER_PAYMENT.toString());

			// --to voucher details start --
			List<VoucherDetails> voucherDetailList = new ArrayList<>();
			for (SupplierPaymentDTO supplierPaymentDTO : voucherDto.getSupplierVocherDetailsList()) {

				VoucherDetails voucherDetails = null;

				log.info("voucher details id==>" + supplierPaymentDTO != null ? supplierPaymentDTO.getVoucherDetailsId()
						: "0");
				if (supplierPaymentDTO != null && supplierPaymentDTO.getVoucherDetailsId() != null) {
					voucherDetails = voucherDetailsRepository.findOne(supplierPaymentDTO.getVoucherDetailsId());

				} else {
					voucherDetails = new VoucherDetails();
				}
				voucherDetails.setVoucher(voucher);
				log.info(":::::::::supplierPaymentDTO.getInvoiceId():::::::::" + supplierPaymentDTO.getInvoiceId());
//				voucherDetails.setPurchaseInvoice(
//						purchaseInvoiceRepository.findPurchaseInvoiceById(supplierPaymentDTO.getInvoiceId()));
				PurchaseInvoice purchaseInvoice=purchaseInvoiceRepository.findOne(supplierPaymentDTO.getInvoiceId());
				if(purchaseInvoice!=null) {
					voucherDetails.setPurchaseInvoice(purchaseInvoice);
				}
				voucherDetails.setAmount(supplierPaymentDTO.getFinalPaidAmount());
				voucherDetailList.add(voucherDetails);
			}
			voucher.setVoucherDetailsList(voucherDetailList);
			// --to voucher details end --

			// --to voucher log start --
			VoucherLog voucherLog = null;
			List<VoucherLog> logList = new ArrayList<>();
			if (voucherDto.getVoucherLogId() != null) {
				voucherLog = voucherLogRepository.findOne(voucherDto.getVoucherLogId());

			} else {
				voucherLog = new VoucherLog();
			}
			voucherLog.setVoucher(voucher);
			voucherLog.setStatus(VoucherStatus.SUBMITTED);
			voucherLog.setUserMaster(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
			logList.add(voucherLog);
			voucher.setVoucherLogList(logList);
			// --to voucher log end --

			// --to voucher note start --
			VoucherNote voucherNote = null;
			List<VoucherNote> voucherNoteList = new ArrayList<>();
			if (voucherDto.getVoucherNoteId() != null) {
				voucherNote = voucherNoteRepository.findOne(voucherDto.getVoucherNoteId());
			} else {
				voucherNote = new VoucherNote();
			}
			voucherNote.setVoucher(voucher);
			voucherNote.setNote(voucherDto.getSupplierNote());
			voucherNote.setFinalApproval(voucherDto.getForForward());
			voucherNote.setForwardTo(userMasterRepository.findOne(voucherDto.getMasterUser()));
			voucherNoteList.add(voucherNote);
			voucher.setVoucherNote(voucherNoteList);
			// --to voucher note end --

			voucherRepository.save(voucher);
			log.info("::::voucher::" + voucher.getId());
			if (sequenceConfig != null) {
				sequenceConfig.setCurrentValue(sequenceConfig.getCurrentValue() + 1);
				sequenceConfigRepository.save(sequenceConfig);
			}

			// baseDTO.setResponseContent(finalVoucher);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException e) {
			log.error("SupplierPaymentService saveSupplierPayment Exception", e);
			baseDTO.setStatusCode(ErrorDescription.INTERNAL_ERROR.getCode());
		} catch (Exception e) {
			log.error("SupplierPaymentService saveSupplierPayment Exception ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	private void validateSupplierPayment(VoucherDto voucherDto) {
		log.info("SupplierPaymentService:validateSupplierPayment start==>");
		Validate.notNullOrEmpty(voucherDto.getTotalPaidAmount(),
				ErrorDescription.SOCIETY_PAYMENT_TOTAL_AMOUNT_NOT_EMPTY);
		Validate.notNullOrEmpty(voucherDto.getMasterUser(), ErrorDescription.FORWARD_TO_USER_EMPTY);
		Validate.notNullOrEmpty(voucherDto.getVoucherFromDate(), ErrorDescription.RETIREMENT_FROMDATE_NULL);
		Validate.notNullOrEmpty(voucherDto.getVoucherToDate(), ErrorDescription.RETIREMENT_TODATE_NULL);
		Validate.notNullOrEmpty(voucherDto.getSupplierNote(), ErrorDescription.SOCIETY_REQUEST_REG_NOTE_EMPTY);
		Validate.notNullOrEmpty(voucherDto.getPayMode(), ErrorDescription.VOUCHER_PAYMENT_MODE_IS_REQUIRED);
		Validate.notNullOrEmpty(voucherDto.getForForward(), ErrorDescription.FORWARD_FOR_EMPTY);
	}

	public BaseDTO search(Voucher voucher) {
		log.info(" getallDesignTargetlazy  called...");
		BaseDTO baseDTO = new BaseDTO();

		try {
			log.info("voucher==>" + voucher != null ? voucher.getId() : "null");
			Integer total = 0;
			Integer start = voucher.getPaginationDTO().getFirst(), pageSize = voucher.getPaginationDTO().getPageSize();
			start = start * pageSize;

			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();

			/*
			 * String queryName="FINANCE_ADVANCE_PAYMENT_LIST_QUERY"; String
			 * queryNameCount="FINANCE_ADVANCE_PAYMENT_LIST_QUERY_COUNT";
			 * 
			 * ApplicationQuery applicationQuery =
			 * applicationQueryRepository.findByQueryName(queryName);
			 * 
			 * String mainQuery = applicationQuery.getQueryContent().trim();
			 */

			/*
			 * String mainQuery =
			 * "select v.id,v.reference_number,date(v.created_date),v.net_amount,sup.code,sup.name,vn.final_approval from voucher v "
			 * +
			 * " join (select vd.voucher_id,pi.supplier_id from voucher_details vd join purchase_invoice pi on vd.purchase_invoice_id="
			 * +
			 * " pi.id group by vd.voucher_id,pi.supplier_id) x on v.id=x.voucher_id join supplier_master sup on sup.id=x.supplier_id "
			 * +
			 * " join voucher_note vn on vn.voucher_id=v.id where v.narration='SUPPLIER_PAYMENT'"
			 * ;
			 */
			String mainQuery = "select v.id,v.reference_number,v.reference_number_prefix,date(v.created_date),v.net_amount,sup.code,sup.name,vl.status"
					+ " from voucher v join(select vd.voucher_id, pi.supplier_id from voucher_details vd"
					+ " join purchase_invoice pi on vd.purchase_invoice_id = pi.id group by	vd.voucher_id, pi.supplier_id) x on"
					+ " v.id = x.voucher_id  join supplier_master sup on sup.id = x.supplier_id"
					+ " join voucher_log vl on vl.voucher_id = v.id"
					+ " join (select max(id) as id from voucher_log group by voucher_id)vl1 on vl.id=vl1.id"
					+ " where v.narration = 'SUPPLIER_PAYMENT' ";

			// applicationQuery =
			// applicationQueryRepository.findByQueryName(queryNameCount);

			// String queryCount = applicationQuery.getQueryContent().trim();

			if (voucher.getPaginationDTO().getFilters().get("refNumber") != null) {
				mainQuery += " and CAST(v.reference_number as TEXT) LIKE '%"
						+ voucher.getPaginationDTO().getFilters().get("refNumber").toString() + "%' ";
			}
			if (voucher.getPaginationDTO().getFilters().get("voucherDate") != null) {
				Date createdDate = new Date((Long) voucher.getPaginationDTO().getFilters().get("voucherDate"));
				mainQuery += " and v.created_date::date = date '" + createdDate + "'";
			}
			if (voucher.getPaginationDTO().getFilters().get("netAmount") != null) {

				mainQuery += " and CAST(v.net_amount as TEXT) LIKE '%"
						+ voucher.getPaginationDTO().getFilters().get("netAmount").toString() + "%' ";
			}
			if (voucher.getPaginationDTO().getFilters().get("supplierMaster.name") != null) {

				mainQuery += " and CAST(sup.name as TEXT) LIKE '%"
						+ voucher.getPaginationDTO().getFilters().get("supplierMaster.name").toString() + "%' ";
			}
			if (voucher.getPaginationDTO().getFilters().get("approval") != null) {
				mainQuery += " and vl.status='" + voucher.getPaginationDTO().getFilters().get("approval") + "'";
			}

			String countQuery = " select count(*) as count from voucher where narration='SUPPLIER_PAYMENT'";

			countData = jdbcTemplate.queryForList(countQuery);
			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					total = Integer.parseInt(data.get("count").toString().replace(",", ""));
			}

			if (voucher.getPaginationDTO().getSortField() == null)
				mainQuery += " order by id desc limit " + pageSize + " offset " + start + ";";

			if (voucher.getPaginationDTO().getSortField() != null
					&& voucher.getPaginationDTO().getSortOrder() != null) {

				if (voucher.getPaginationDTO().getSortField().equals("refNumber")
						&& voucher.getPaginationDTO().getSortOrder().equals("ASCENDING"))
					mainQuery += " order by v.reference_number asc ";
				if (voucher.getPaginationDTO().getSortField().equals("refNumber")
						&& voucher.getPaginationDTO().getSortOrder().equals("DESCENDING"))
					mainQuery += " order by v.reference_number desc ";

				if (voucher.getPaginationDTO().getSortField().equals("voucherDate")
						&& voucher.getPaginationDTO().getSortOrder().equals("ASCENDING"))
					mainQuery += " order by v.created_date asc  ";
				if (voucher.getPaginationDTO().getSortField().equals("voucherDate")
						&& voucher.getPaginationDTO().getSortOrder().equals("DESCENDING"))
					mainQuery += " order by v.created_date desc  ";

				if (voucher.getPaginationDTO().getSortField().equals("netAmount")
						&& voucher.getPaginationDTO().getSortOrder().equals("ASCENDING"))
					mainQuery += " order by v.net_amount asc  ";
				if (voucher.getPaginationDTO().getSortField().equals("netAmount")
						&& voucher.getPaginationDTO().getSortOrder().equals("DESCENDING"))
					mainQuery += " order by v.net_amount desc  ";

				if (voucher.getPaginationDTO().getSortField().equals("supplierMaster.name")
						&& voucher.getPaginationDTO().getSortOrder().equals("ASCENDING"))
					mainQuery += " order by sup.name asc  ";
				if (voucher.getPaginationDTO().getSortField().equals("supplierMaster.name")
						&& voucher.getPaginationDTO().getSortOrder().equals("DESCENDING"))
					mainQuery += " order by sup.name desc  ";

				if (voucher.getPaginationDTO().getSortField().equals("approval")
						&& voucher.getPaginationDTO().getSortOrder().equals("ASCENDING"))
					mainQuery += " order by vl.status asc  ";
				if (voucher.getPaginationDTO().getSortField().equals("approval")
						&& voucher.getPaginationDTO().getSortOrder().equals("DESCENDING"))
					mainQuery += " order by vl.status desc  ";

				mainQuery += " limit " + pageSize + " offset " + start + ";";
			}

			List<SupplierPaymentViewDTO> supplierPaymentViewDTOList = new ArrayList<>();
			listofData = jdbcTemplate.queryForList(mainQuery);
			for (Map<String, Object> data : listofData) {
				SupplierPaymentViewDTO advancePaymentDTO = new SupplierPaymentViewDTO();
				if (data.get("id") != null) {
					advancePaymentDTO.setVoucherId(Long.parseLong(data.get("id").toString()));
				}
				if (data.get("reference_number") != null && data.get("reference_number_prefix") != null) {
					advancePaymentDTO.setRefNumber(
							data.get("reference_number_prefix").toString() + data.get("reference_number").toString());
				}
				if (data.get("date") != null) {
					advancePaymentDTO.setVoucherDate((Date) data.get("date"));
				}
				if (data.get("net_amount") != null) {
					BigDecimal netAmountDecimal = (BigDecimal) data.get("net_amount");
					Double netAmount = netAmountDecimal.doubleValue();
					advancePaymentDTO.setNetAmount(netAmount);
				}
				if (data.get("code") != null) {
					advancePaymentDTO.setSupplierCodeName(
							data.get("code").toString().concat(" / ").concat(data.get("name").toString()));
				} else {
					advancePaymentDTO.setSupplierCodeName(" - ");
				}
				if (data.get("status") != null) {
					advancePaymentDTO.setStatus(data.get("status").toString());
				}

				supplierPaymentViewDTOList.add(advancePaymentDTO);
			}
			baseDTO.setResponseContent(supplierPaymentViewDTOList);
			baseDTO.setTotalRecords(total);
			log.info("countQuery " + countQuery);
			log.info("mainQuery " + mainQuery);
			log.info("total " + total);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (Exception exp) {
			log.error("Exception Cause  : ", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	@Autowired
	VoucherLogRepository voucherlogRepository;

	public BaseDTO getVoucherById(Long voucherId) {
		log.info(" getVoucherById  called...");
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info(" getVoucherById  voucherId==>" + voucherId);
			SupplierPaymentDTO supplierPaymentDTO = new SupplierPaymentDTO();
			Voucher voucher = voucherRepository.findOne(voucherId);
			List<VoucherDetails> voucherDetailsList = voucherDetailsRepository.findAllByVoucherId(voucherId);
			SupplierMaster supplierMaster = voucherDetailsList.get(0).getPurchaseInvoice().getSupplierMaster();
			SupplierMaster supplierMasterTemp = new SupplierMaster();
			VoucherLog voucherLog = voucherlogRepository.findByVoucherId(voucherId);
			VoucherNote voucherNote = voucherNoteRepository.findByVoucherId(voucherId);
			if (supplierMaster != null) {
				supplierMasterTemp.setId(supplierMaster.getId());
				supplierMasterTemp.setName(supplierMaster.getName());
				supplierMasterTemp.setCode(supplierMaster.getCode());
				supplierMasterTemp.setPrimaryContactName(supplierMaster.getPrimaryContactName());
				supplierMasterTemp.setPrimaryEmail(supplierMaster.getPrimaryEmail());
				supplierMasterTemp.setGstNumber(supplierMaster.getGstNumber());
			}
			supplierPaymentDTO.setVoucher(voucher);
			supplierPaymentDTO.setVoucherLog(voucherLog);
			supplierPaymentDTO.setVoucherNote(voucherNote);
			supplierPaymentDTO.setSupplierMaster(supplierMasterTemp);

			List<Map<String, Object>> employeeData = new ArrayList<Map<String, Object>>();
			if (voucher != null) {
				log.info("<<<:::::::amountTransfer::::not Null::::>>>>" + voucher);
				ApplicationQuery applicationQueryForlog = applicationQueryRepository
						.findByQueryName("VOUCHER_LOG_EMPLOYEE_DETAILS");
				if (applicationQueryForlog == null || applicationQueryForlog.getId() == null) {
					log.info("Application Query For Log Details not found for query name : " + applicationQueryForlog);
					baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
					return baseDTO;
				}
				String logquery = applicationQueryForlog.getQueryContent().trim();
				log.info("<=========VOUCHER_LOG_EMPLOYEE_DETAILS Query Content ======>" + logquery);
				logquery = logquery.replace(":voucherId", "'" + voucher.getId().toString() + "'");
				log.info("Query Content For VOUCHER_LOG_EMPLOYEE_DETAILS After replaced plan id View query : "
						+ logquery);
				employeeData = jdbcTemplate.queryForList(logquery);
				log.info("<=========VOUCHER_LOG_EMPLOYEE_DETAILS Employee Data======>" + employeeData);
				baseDTO.setTotalListOfData(employeeData);
			}

			baseDTO.setResponseContent(supplierPaymentDTO);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException e) {
			log.error("SupplierPaymentService getVoucherById Exception=>", e);
			baseDTO.setStatusCode(ErrorDescription.INTERNAL_ERROR.getCode());
		} catch (Exception exp) {
			log.error("SupplierPaymentService getVoucherById Exception=>", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO getInvoiceBillAmountAsView(Long voucherId) {
		log.info("SupplierPaymentService getInvoiceBillAmountAsView method started ");
		BaseDTO baseDTO = new BaseDTO();

		try {
			log.info("voucherId==>" + voucherId);
			/*
			 * ApplicationQuery applicationQuery =
			 * applicationQueryRepository.findByQueryName("INVOICE_VOUCHER_QUERY"); String
			 * query = applicationQuery.getQueryContent().trim();
			 */
			/*
			 * String query =
			 * "select p.id as invoiceId,p.invoice_number as invoiceNum,p.invoice_date as invoiceDate,p.grand_total "
			 * + " as invoiceAmount,ps.amount as amountAdvance," +
			 * " v.net_amount as paidAmount,(p.grand_total-sum(ps.amount+v.net_amount)) as balance,p.payment_duedate as dueDate from voucher v,voucher_details d,"
			 * +
			 * " purchase_invoice p,purchase_sales_advance ps where v.id=d.voucher_id and p.id=d.purchase_invoice_id and "
			 * +
			 * " ps.purchase_order_id=p.purchase_order_id and v.id= :votId group by p.id,p.invoice_number,p.invoice_date,p.grand_total,"
			 * + " ps.amount,v.net_amount,p.payment_duedate";
			 */
			String query = "select p.id as invoiceId,p.invoice_number as invoiceNum,"
					+ " p.invoice_date as invoiceDate,p.grand_total as invoiceAmount,sum(ps.amount) as amountAdvance,"
					+ " v.net_amount as paidAmount,(p.grand_total-(COALESCE(sum(ps.amount),0)+v.net_amount)) as balance,p.payment_duedate as dueDate "
					+ " from voucher v"
					+ " join voucher_details d on v.id=d.voucher_id join purchase_invoice p on p.id=d.purchase_invoice_id"
					+ " left join purchase_sales_advance ps on ps.purchase_order_id=p.purchase_order_id"
					+ " where  v.id= :votId group by p.id,p.invoice_number,p.invoice_date,p.grand_total,"
					+ " v.net_amount,p.payment_duedate";
			query = query.replace(":votId", voucherId.toString());
			log.info("query==>" + query);
			baseDTO.setResponseContent(jdbcTemplate.query(query, new RowMapper<SupplierPaymentDTO>() {

				@Override
				public SupplierPaymentDTO mapRow(ResultSet rs, int no) throws SQLException {
					SupplierPaymentDTO supplierPaymentDTO = new SupplierPaymentDTO();
					supplierPaymentDTO.setInvoiceId(rs.getLong("invoiceId"));
					supplierPaymentDTO.setInvoiceNumber(rs.getInt("invoiceNum"));
					supplierPaymentDTO.setInvoiceDate(rs.getDate("invoiceDate"));
					supplierPaymentDTO.setInvoiceAmount(rs.getDouble("invoiceAmount"));

					supplierPaymentDTO.setAdvanceAmount(rs.getDouble("amountAdvance"));
					supplierPaymentDTO.setAlreadyPaidAmount(rs.getDouble("paidAmount"));
					supplierPaymentDTO.setBalance(rs.getDouble("balance"));
					supplierPaymentDTO.setDueDate(rs.getDate("dueDate"));
					supplierPaymentDTO.setFinalPaidAmount(rs.getDouble("balance"));

					return supplierPaymentDTO;
				}
			}));
			log.info("<--- Successfully fetched the records from DB ---> ");
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.error("<--- Error in getInvoiceBillAmount() ---> ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return baseDTO;
	}

	public BaseDTO deleteById(Long id) {
		log.info("SupplierPaymentService deleteById method started [" + id + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			// voucherRepository.delete(id);
			jdbcTemplate.execute("delete from voucher_log where voucher_id='" + id + "'");
			jdbcTemplate.execute("delete from voucher_note where voucher_id='" + id + "'");
			jdbcTemplate.execute("delete from voucher_details where voucher_id='" + id + "'");
			jdbcTemplate.execute("delete from voucher where id='" + id + "'");
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("SupplierPaymentService deleteById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (DataIntegrityViolationException exception) {
			log.error("SupplierPaymentService deleteById DataIntegrityViolationException ", exception);
			if (exception.getCause().getCause() instanceof PSQLException) {
				baseDTO.setStatusCode(ErrorDescription.CANNOT_DELETE_REFERENCED_RECORD.getErrorCode());
			} else {
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			}
		} catch (Exception exception) {
			log.error("SupplierPaymentService deleteById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("SupplierPaymentService deleteById method completed");
		return baseDTO;
	}

	@Transactional
	public BaseDTO supplierPaymentApproval(SupplierPaymentDTO supplierPaymentDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("supplierPaymentApproval method start=============>" + supplierPaymentDTO.getVoucher().getId());
			Voucher voucher = voucherRepository.findOne(supplierPaymentDTO.getVoucher().getId());

			if (supplierPaymentDTO.getVoucherLog().getStatus().equals(VoucherStatus.REJECTED)) {
				VoucherLog voucherLog = new VoucherLog();
				voucherLog.setVoucher(voucher);
				voucherLog.setStatus(supplierPaymentDTO.getVoucherLog().getStatus());
				voucherLog.setRemarks(supplierPaymentDTO.getVoucherLog().getRemarks());
				voucherLog.setUserMaster(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
				voucherlogRepository.save(voucherLog);

				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				log.info("supplierPaymentApproval  log inserted------");

			} else {
				VoucherNote voucherNote = new VoucherNote();
				voucherNote.setVoucher(voucher);
				voucherNote.setFinalApproval(supplierPaymentDTO.getVoucherNote().getFinalApproval());
				voucherNote.setNote(supplierPaymentDTO.getVoucherNote().getNote());
				voucherNote.setForwardTo(
						userMasterRepository.findOne(supplierPaymentDTO.getVoucherNote().getForwardTo().getId()));
				voucherNoteRepository.save(voucherNote);
				log.info("supplierPaymentApproval note inserted------");

				VoucherLog voucherLog = new VoucherLog();
				voucherLog.setVoucher(voucher);
				voucherLog.setStatus(supplierPaymentDTO.getVoucherLog().getStatus());
				voucherLog.setRemarks(supplierPaymentDTO.getVoucherLog().getRemarks());
				voucherLog.setUserMaster(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
				voucherlogRepository.save(voucherLog);
				log.info("supplierPaymentApproval log inserted------");

				// log.info("Account posting Start--------"+amountTransfer);
				// log.info("Approval status----------"+requestDTO.getStage());

				// if(requestDTO.getStage().equals(AmountMovementType.FINAL_APPROVED)) {
				// baseDTO=operationService.amountTransferLedgerPosting(amountTransfer,
				// requestDTO.getStage());
				// }
				log.info("Account posting End--------");
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (Exception e) {
			log.error("supplierPaymentApproval method exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO loadSupplierBySupplierTypeId(Long id) {

		log.info("<-Inside SERVICE-Starts loadSupplierBySupplierTypeId-->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<SupplierMaster> suppliermasterList = supplierMasterRepository.getBySupplierTypeMasterByID(id);
			if (suppliermasterList != null) {
				for (SupplierMaster supplierMaster : suppliermasterList) {
					supplierMaster.setCircleMaster(null);
				}
			}
			baseDTO.setResponseContent(suppliermasterList);
			baseDTO.setTotalRecords(suppliermasterList != null ? suppliermasterList.size() : 0);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("<- loadSupplierBySupplierTypeId Data Success-->");
		} catch (Exception exception) {
			log.error("exception Occured loadSupplierBySupplierTypeId : ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO loadPurchaseOrderListBySupplierId(Long id) {
		log.info("SupplierPaymentService loadPurchaseOrderListBySupplierId method started SupplierId--->" + id);
		BaseDTO baseDTO = new BaseDTO();
		try {
			Validate.notNull(id, ErrorDescription.SUPPLIER_ID_EMPTY);
			List<PurchaseOrder> purchaseOrderList = purchaseOrderRepository.loadPurchaseOrderListBySupplierId(id);
			List<PurchaseOrder> purchaseOrderListTemp = new ArrayList<>();
			List<Integer> purchaseInvoiceIdList = null;
			for (PurchaseOrder purchaseOrder : purchaseOrderList) {
				purchaseInvoiceIdList = new ArrayList<>();
				purchaseInvoiceIdList = purchaseInvoiceRepository
						.getpurchaseInvoiceIdlistbypurchaseOrderid(purchaseOrder.getId());
				if (purchaseInvoiceIdList.size() > 0) {
					log.info("Already purchase Order id " + purchaseOrder.getId() + " have a invoice");
				} else {
					purchaseOrderListTemp.add(purchaseOrder);
				}
			}
			log.info("<<<:::::::::Purchase Order List Size::::::::::>>>" + purchaseOrderListTemp.size());
			baseDTO.setResponseContent(purchaseOrderListTemp);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("SupplierPaymentService loadPurchaseOrderListBySupplierId method completed [" + id + "]");
		} catch (RestException exception) {
			baseDTO.setStatusCode(exception.getStatusCode());
			log.error("SupplierPaymentService loadPurchaseOrderListBySupplierId RestException ", exception);
		} catch (Exception exception) {
			log.error("Exception occurred in PurchaseOrderService loadPurchaseOrderListBySupplierId method :",
					exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return responseWrapper.send(baseDTO);

	}

	public BaseDTO getSelectedPurchaseOrderItem(Long id) {
		log.info("<-Inside SERVICE-Starts PurchaseOrderItem-->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<PurchaseOrderItems> purchaseOrderItems = purchaseOrderItemsRepository.getPurchaseOrderList(id);
			baseDTO.setResponseContent(purchaseOrderItems);
			baseDTO.setTotalRecords(purchaseOrderItems.size());
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("<-PurchaseOrderItem GetAll Data Success-->" + purchaseOrderItems.size());
		} catch (Exception exception) {
			log.error("exception Occured PurchaseOrderItem : ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

}
