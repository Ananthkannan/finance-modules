package in.gov.cooptex.finance.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.model.RebateClaim;
import in.gov.cooptex.core.accounts.model.RebateClaimLog;
import in.gov.cooptex.core.accounts.model.RebateClaimNote;
import in.gov.cooptex.core.accounts.repository.RebateClaimRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.SequenceName;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.SequenceConfig;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.ProductCategoryRepository;
import in.gov.cooptex.core.repository.SequenceConfigRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AmountMovementType;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.dto.ClaimStatementDto;
import in.gov.cooptex.finance.dto.ClaimStatementDtoList;
import in.gov.cooptex.finance.repository.RebateClaimLogRepository;
import in.gov.cooptex.finance.repository.RebateClaimNoteRepository;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class RebateClaimService {

	@Autowired
	RebateClaimRepository rebateClaimRepository;

	@Autowired
	RebateClaimNoteRepository rebateClaimNoteRepository;

	@Autowired
	RebateClaimLogRepository rebateClaimLogRepository;

	@Autowired
	ResponseWrapper responseWrapper;

	@Autowired
	RebateClaimDetailsService rebateClaimDetailsService;

	@Autowired
	UserMasterRepository userMasterRepository;

	@PersistenceContext
	EntityManager entityManager;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	SequenceConfigRepository sequenceConfigRepository;

	ApplicationQuery appQuery = null;

	String queryContent = null;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	@Autowired
	ProductCategoryRepository productCategoryRepository;

	public BaseDTO getById(Long id) {
		log.info("RebateClaimService getById method started [" + id + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			// Validate.notNull(id, ErrorDescription.REBATE_CLAIM_ID_EMPTY);
			RebateClaim rebateClaim = rebateClaimRepository.getOne(id);
			baseDTO.setResponseContent(rebateClaim);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("RebateClaimService getById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("RebateClaimService getById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("RebateClaimService getById method completed");
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO deleteById(Long id) {
		log.info("RebateClaimService deleteById method started [" + id + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			// Validate.notNull(id, ErrorDescription.REBATE_CLAIM_ID_EMPTY);
			rebateClaimRepository.delete(id);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("RebateClaimService deleteById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (DataIntegrityViolationException exception) {
			log.error("RebateClaimService deleteById DataIntegrityViolationException ", exception);
			if (exception.getCause().getCause() instanceof PSQLException) {
				baseDTO.setStatusCode(ErrorDescription.CANNOT_DELETE_REFERENCED_RECORD.getErrorCode());
			} else {
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			}
		} catch (Exception exception) {
			log.error("RebateClaimService deleteById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("RebateClaimService deleteById method completed");
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO createRebateClaim(RebateClaim rebateClaim) {
		BaseDTO baseDTO = new BaseDTO();
		rebateClaimRepository.save(rebateClaim);
		return baseDTO;
	}

	public BaseDTO getActiveRebateClaim() {
		log.info("RebateClaimService:getActiveRebateClaim()");
		BaseDTO baseDTO = new BaseDTO();
		try {

			List<RebateClaim> rebateClaimList = rebateClaimRepository.getAllActiveRebateClaim();
			baseDTO.setResponseContent(rebateClaimList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {

			log.error("RebateClaimService getActiveRebateClaimRestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {

			log.error("RebateClaimService getActiveRebateClaimException ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}

		log.info("RebateClaimServicegetActiveRebateClaimmethod completed");
		return baseDTO;

	}

	@SuppressWarnings("unchecked")
	public BaseDTO saveRebateClaimData(ClaimStatementDto claimStatementDto) {

		BaseDTO baseDTO = new BaseDTO();
		RebateClaim rebateClaim = new RebateClaim();
		try {

			Validate.notNull(claimStatementDto, ErrorDescription.CLAIMSTATEMENTDTO_EMPTY);
			Validate.notNull(claimStatementDto.getClaimStatementRequestDto(),
					ErrorDescription.CLAIMSTATEMENT_REQUESTDTO_EMPTY);
			Validate.notNull(claimStatementDto.getClaimStatementRequestDto().getFromDate(),
					ErrorDescription.CLAIMSTATEMENT_FROMDATE_EMPTY);
			Validate.notNull(claimStatementDto.getClaimStatementRequestDto().getToDate(),
					ErrorDescription.CLAIMSTATEMENT_TODATE_EMPTY);
			Date date = claimStatementDto.getClaimStatementRequestDto().getFromDate();
			LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			Long fromYear = (long) localDate.getYear();
			Month fromMonth = localDate.getMonth();
			Date todate = claimStatementDto.getClaimStatementRequestDto().getToDate();
			LocalDate localToDate = todate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			Long toYear = (long) localToDate.getYear();
			Month toMonth = localToDate.getMonth();

			rebateClaim.setActiveStatus(true);
			rebateClaim.setClaimDate(new Date());
			rebateClaim.setFromMonth(fromMonth.toString());
			rebateClaim.setFromYear(fromYear);
			rebateClaim.setToMonth(toMonth.toString());
			rebateClaim.setToYear(toYear);
			rebateClaim.setVersion((long) 0);
			rebateClaim.setClaimAmount(claimStatementDto.getTotalRebateAmount());
			SequenceConfig sequenceConfig = sequenceConfigRepository
					.findBySequenceName(SequenceName.REBATE_CLAIM_STATEMENT_REFNO);
			rebateClaim.setRefNumber(generateClaimStatementCode(sequenceConfig));
			rebateClaim = rebateClaimRepository.save(rebateClaim);
			if (rebateClaim != null && sequenceConfig != null) {
				sequenceConfig.setCurrentValue(sequenceConfig.getCurrentValue() + 1);
				sequenceConfigRepository.save(sequenceConfig);
			}

			saveRebateClaimNoteAndLog(rebateClaim, claimStatementDto);
			List<ClaimStatementDtoList> dataList = new ArrayList<ClaimStatementDtoList>();
			appQuery = applicationQueryRepository.findByQueryName("CLAIM_STATEMENT_DETAILS");
			log.info("appQuery :"+appQuery);
			if (appQuery != null) {
				queryContent = appQuery.getQueryContent();
				log.info("CLAIM_STATEMENT_DETAILS Query  :: " + queryContent);
				if (queryContent != null && !queryContent.isEmpty()) {

					queryContent = queryContent.replace(":startDate",
							"'" + claimStatementDto.getClaimStatementRequestDto().getFromDate().toString() + "'");
					queryContent = queryContent.replace(":endDate",
							"'" + claimStatementDto.getClaimStatementRequestDto().getToDate().toString() + "'");

					String categoryTypeList = StringUtils
							.join(claimStatementDto.getClaimStatementRequestDto().getCategoryCodeList(), ',');
					queryContent = queryContent.replace(":categoryId", "" + categoryTypeList + "");
					String regionList = StringUtils
							.join(claimStatementDto.getClaimStatementRequestDto().getRegionCodeList(), ',');
					queryContent = queryContent.replace(":regionId", "" + regionList + "");
					log.info("queryContent :"+queryContent);
					dataList = jdbcTemplate.query(queryContent, new BeanPropertyRowMapper(ClaimStatementDtoList.class));
					log.info(" Query Data List :: " + dataList);

				}
			}else {
				log.info("appQuery null for CLAIM_STATEMENT_DETAILS:");
			}
			Iterator<ClaimStatementDtoList> claimStatementDtoListItr = dataList.iterator();
			while (claimStatementDtoListItr.hasNext()) {
				ClaimStatementDtoList claimStatementDtoList = claimStatementDtoListItr.next();
				rebateClaimDetailsService.saveRebateClaimDetailsData(claimStatementDto, claimStatementDtoList,
						rebateClaim);
			}

		} catch (RestException restexp) {
			log.error("RebateClaimService RestException", restexp);
			baseDTO.setStatusCode(restexp.getErrorCodeDescription().getErrorCode());
		} catch (DataIntegrityViolationException diExp) {
			log.error("Data Integrity Violation Exception while RebateClaimService group " + diExp);
		} catch (Exception exception) {
			log.error("Error while Creating RebateData ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("RebateClaimService save method ====== End");
		return baseDTO;
	}

	public BaseDTO getRebateDetails(PaginationDTO paginationDto) {

		log.info(" Starat getRebateDetails  called..." + paginationDto);
		BaseDTO baseDTO = new BaseDTO();
		try {
			Integer total = 0;
			Integer start = paginationDto.getFirst(), pageSize = paginationDto.getPageSize();
			start = start * pageSize;
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();
			String mainQuery = "select (rc.id) as id,(rcl.id) as logid,(rc.ref_number) as refNumber,(rc.from_month)"
					+ "as fromMonth,(rc.from_year) as fromYear,(rc.to_month) as toMonth,(rc.to_year)"
					+ "as toYear,(rc.claim_amount) as claimAmount,(rcl.stage) as approvalStatus from "
					+ " rebate_claim rc join rebate_claim_log rcl on rc.id=rcl.rebate_claim_id and "
					+ " rcl.id=(select max(rcl1.id) from rebate_claim_log rcl1 where"
					+ " rcl.rebate_claim_id=rcl1.rebate_claim_id)";

			if (paginationDto.getFilters() != null) {
				if (paginationDto.getFilters().get("stage") != null)
					mainQuery += " and rcl.stage='" + paginationDto.getFilters().get("stage") + "'";

				if (paginationDto.getFilters().get("rebateClaim.fromMonth") != null) {
					String fromMonthYear = (String) paginationDto.getFilters().get("rebateClaim.fromMonth");
					if (AppUtil.isInteger(fromMonthYear))
						mainQuery += " and (cast(rc.from_Year as varchar)) like upper('%" + fromMonthYear + "%') ";
					else
						mainQuery += " and upper(rc.from_Month) like upper('%"
								+ paginationDto.getFilters().get("rebateClaim.fromMonth") + "%') ";

				}

				if (paginationDto.getFilters().get("rebateClaim.toMonth") != null) {
					String toMonthYear = (String) paginationDto.getFilters().get("rebateClaim.toMonth");
					if (AppUtil.isInteger(toMonthYear))
						mainQuery += " and (cast(rc.to_Year as varchar)) like upper('%" + toMonthYear + "%') ";
					else
						mainQuery += " and upper(rc.to_Month) like upper('%"
								+ paginationDto.getFilters().get("rebateClaim.toMonth") + "%') ";

				}

				if (paginationDto.getFilters().get("rebateClaim.refNumber") != null)
					mainQuery += " and upper(rc.ref_Number) like upper('%"
							+ paginationDto.getFilters().get("rebateClaim.refNumber") + "%') ";

				if (paginationDto.getFilters().get("rebateClaim.claimAmount") != null)
					mainQuery += " and (cast(rc.claim_Amount as varchar)) like upper('%"
							+ paginationDto.getFilters().get("rebateClaim.claimAmount") + "%') ";

			}
			String countQuery = mainQuery.replace(
					"select (rc.id) as id,(rcl.id) as logid,(rc.ref_number) as refNumber,(rc.from_month)"
							+ "as fromMonth,(rc.from_year) as fromYear,(rc.to_month) as toMonth,(rc.to_year)"
							+ "as toYear,(rc.claim_amount) as claimAmount,(rcl.stage) as approvalStatus",
					"select count(distinct(rc.id)) as count");
			log.info("count query... " + countQuery);
			countData = jdbcTemplate.queryForList(countQuery);
			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					total = Integer.parseInt(data.get("count").toString().replace(",", ""));
			}
			log.info("total query... " + total);
			if (paginationDto.getSortField() == null)
				mainQuery += " order by id desc limit " + pageSize + " offset " + start + ";";

			if (paginationDto.getSortField() != null && paginationDto.getSortOrder() != null) {
				log.info("Sort Field:[" + paginationDto.getSortField() + "] Sort Order:[" + paginationDto.getSortOrder()
						+ "]");
				if (paginationDto.getSortField().equals("fromMonth")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by fromMonth asc  ";
				if (paginationDto.getSortField().equals("toMonth") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by toMonth desc  ";
				if (paginationDto.getSortField().equals("refNumber")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by refNumber asc ";
				if (paginationDto.getSortField().equals("stage") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by stage asc ";
				if (paginationDto.getSortField().equals("stage") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by stage desc ";
				mainQuery += " limit " + pageSize + " offset " + start + ";";
			}
			log.info("Main Qury....." + mainQuery);
			List<RebateClaimLog> rebateClaimLogList = new ArrayList<>();
			listofData = jdbcTemplate.queryForList(mainQuery);
			for (Map<String, Object> data : listofData) {
				RebateClaimLog rebateClaimLog = new RebateClaimLog();
				if (data.get("logid") != null)
					rebateClaimLog.setId(Long.parseLong(data.get("logid").toString()));
				if (data.get("approvalStatus") != null)
					rebateClaimLog.setStage(data.get("approvalStatus").toString());
				RebateClaim rebateClaim = new RebateClaim();
				if (data.get("id") != null) {
					rebateClaim.setId(Long.parseLong(data.get("id").toString()));
			    }
				rebateClaim.setRefNumber(data.get("refNumber").toString());
				Integer from = (Integer) data.get("fromYear");
				Integer to = (Integer) data.get("toYear");
				Long fromYear = new Long(from);
				Long toYear = new Long(to);
				BigDecimal bg = (BigDecimal) data.get("claimAmount");
				Double claimAmount = bg.doubleValue();
				rebateClaim.setClaimAmount(claimAmount);
				rebateClaim.setFromMonth(data.get("fromMonth").toString());
				rebateClaim.setToMonth(data.get("toMonth").toString());
				rebateClaim.setFromYear(fromYear);
				rebateClaim.setToYear(toYear);
				rebateClaimLog.setRebateClaim(rebateClaim);
				rebateClaimLogList.add(rebateClaimLog);

			}
			log.info("Returning getallRebateClaimlazy list...." + rebateClaimLogList.size());
			log.info("Total records present in getallRebateClaimlazy..." + total);
			baseDTO.setResponseContents(rebateClaimLogList);
			baseDTO.setTotalRecords(total);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (Exception exp) {
			log.error("Exception Cause  : ", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public void saveRebateClaimNoteAndLog(RebateClaim rebateClaim, ClaimStatementDto claimStatementDto) {

		log.info("Start saveRebateClaimNoteAndLog  called..." + rebateClaim);
		BaseDTO baseDto = new BaseDTO();
		try {
			if (claimStatementDto.getUserMaster() != null) {
				UserMaster userMaster = userMasterRepository.findOne(claimStatementDto.getUserMaster().getId());
				RebateClaimLog rebateClaimLog = new RebateClaimLog();
				rebateClaimLog.setRebateClaim(rebateClaim);
				rebateClaimLog.setStage(AmountMovementType.SUBMITTED);
				rebateClaimLogRepository.save(rebateClaimLog);

				RebateClaimNote rebateClaimNote = new RebateClaimNote();
				rebateClaimNote.setForwardTo(userMaster);
				rebateClaimNote.setNote(claimStatementDto.getNote());
				rebateClaimNote.setRebateClaim(rebateClaim);
				rebateClaimNote.setFinalApproval(claimStatementDto.getForwardfor());
				/*if (claimStatementDto.getFianlApproval().equalsIgnoreCase("Approved"))
					rebateClaimNote.setFinalApproval(false);
				else
					rebateClaimNote.setFinalApproval(true);*/
				rebateClaimNoteRepository.save(rebateClaimNote);
			} else {
				RebateClaimLog rebateClaimLog = new RebateClaimLog();
				rebateClaimLog = rebateClaimLogRepository.getRebateClaimData(rebateClaim.getId());
				if (claimStatementDto.getApproveOrReject()) {
					rebateClaimLog.setStage(AmountMovementType.APPROVED);
				} else {
					rebateClaimLog.setStage(AmountMovementType.REJECTED);
				}
				rebateClaimLogRepository.save(rebateClaimLog);
				RebateClaimNote rebateClaimNote = new RebateClaimNote();
				rebateClaimNote = rebateClaimNoteRepository.getRebateClaimData(rebateClaim.getId());
				rebateClaimNote.setNote(claimStatementDto.getNote());
				rebateClaimNote.setFinalApproval(true);
				rebateClaimNoteRepository.save(rebateClaimNote);
			}

			log.info("End saveRebateClaimNoteAndLog  called..." + rebateClaim);
		} catch (Exception exp) {
			log.error("Exception Cause  : ", exp);
			baseDto.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public BaseDTO showViewPage(ClaimStatementDto claimStatementDto) {

		log.info("Start showViewPage  called..." + claimStatementDto);
		BaseDTO baseDto = new BaseDTO();

		try {
			if (claimStatementDto.getRebateClaimDto() != null && claimStatementDto.getUserMaster() != null) {
				Validate.notNull(claimStatementDto.getRebateClaimDto().getId(), ErrorDescription.REBATE_CLAIMID_EMPTY);
				List<ClaimStatementDtoList> dataList = new ArrayList<ClaimStatementDtoList>();
				log.info("Rebate claim Id is--->>>>>" + claimStatementDto.getRebateClaimDto().getId());
				RebateClaimNote rebateClaimNote = rebateClaimNoteRepository.getRebateClaimNote(claimStatementDto.getRebateClaimDto().getId());
				RebateClaimLog rebateClaimLog = rebateClaimLogRepository.getRebateClaimData(claimStatementDto.getRebateClaimDto().getId());

				/*if (rebateClaimNote.getFinalApproval()) {*/
					claimStatementDto.setApproveStatus(rebateClaimNote.getFinalApproval());
				/*}else {
					claimStatementDto.setApproveStatus(false);
				}*/
					claimStatementDto.setApprovalLogStage(rebateClaimLog.getStage());	
				if (rebateClaimNote.getForwardTo().getId().equals(claimStatementDto.getUserMaster().getId())) {
					claimStatementDto.setFinalApprovalStatus(true);
					
				}
				claimStatementDto.setUserMaster(rebateClaimNote.getForwardTo()!=null?rebateClaimNote.getForwardTo():null);
				appQuery = applicationQueryRepository.findByQueryName("CLAIM_STATEMENT_VIEW_DETAILS");
				queryContent = appQuery.getQueryContent();
				queryContent = queryContent.replace(":rebateClaimId",
						"" + claimStatementDto.getRebateClaimDto().getId() + "");
				log.info("Query Content  is--->>>>>" + queryContent);
				dataList = jdbcTemplate.query(queryContent, new BeanPropertyRowMapper(ClaimStatementDtoList.class));

				Double totalGrossAmount = dataList.stream().mapToDouble(ClaimStatementDtoList::getGrossSales).sum();
				Double totalDiscountAmount = dataList.stream().mapToDouble(ClaimStatementDtoList::getDiscountAmount).sum();
				Double totalRebateAmount = dataList.stream().mapToDouble(ClaimStatementDtoList::getRebateAmount).sum();
				Double totalbalanceAmount = dataList.stream().mapToDouble(ClaimStatementDtoList::getBalanceAmount).sum();
				List<String> categoryCodeAndName = new ArrayList<String>();
				Iterator<ClaimStatementDtoList> claimStatmentItr = dataList.iterator();
				while (claimStatmentItr.hasNext()) {
					ClaimStatementDtoList claimStatementDtoList = claimStatmentItr.next();
//					String categoryCodeAndNames = claimStatementDtoList.getCategoryCodeAndName().replaceAll("^\"|\"$", "");
					String categoryCodeAndNames = claimStatementDtoList.getCategoryCodeAndName();
					categoryCodeAndName.add(categoryCodeAndNames);
				}
				claimStatementDto.setUserMaster(rebateClaimNote.getForwardTo());
				claimStatementDto.setNote(rebateClaimNote.getNote());
				claimStatementDto.setClaimStatementDtoList(dataList);
				claimStatementDto.setTotalRebateAmount(totalRebateAmount);
				claimStatementDto.setTotalBalanceAmount(totalbalanceAmount);
				claimStatementDto.setTotalDiscountAmount(totalDiscountAmount);
				claimStatementDto.setTotalGrossAmount(totalGrossAmount);
				claimStatementDto.setRebateClaimNote(rebateClaimNote);
				claimStatementDto.setRebateClaim(rebateClaimNote.getRebateClaim());
				claimStatementDto.setCategoryCodeAndName(categoryCodeAndName);
				baseDto.setResponseContent(claimStatementDto);
				baseDto.setStatusCode(0);
			}

		} catch (Exception exp) {
			log.error("Exception Cause  : ", exp);
			baseDto.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("End showViewPage  called..." + claimStatementDto);
		return baseDto;
	}

	public String generateClaimStatementCode(SequenceConfig sequenceConfig) {
		log.info("<==== Starts RebateClaim Service.generateClaimStatementCode ====>");
		String code = null;
		if (sequenceConfig != null) {
			Long currentValue = sequenceConfig.getCurrentValue();
			code = sequenceConfig.getPrefix();
			LocalDate localDate = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			Month month = localDate.getMonth();
			log.info("currentValue ::" + currentValue);
			code = code + String.format(month + "18-" + "%04d", currentValue + 1);
		}
		log.info("<==== Ends RebateClaim Service.generateClaimStatementCode ====>" + code);
		return code;
	}
	
	public BaseDTO approvedRejectClaimStatement(RebateClaim rebateClaim, ClaimStatementDto claimStatementDto) {
		log.info("<==== Starts RebateClaim Service.approvedRejectClaimStatement ====>");
		BaseDTO baseDTO=new BaseDTO();
		try {

			RebateClaimLog rebateClaimLog = new RebateClaimLog();
			rebateClaimLog = rebateClaimLogRepository.getRebateClaimData(rebateClaim.getId());
			rebateClaimLog.setRemarks(claimStatementDto.getRemark());
			UserMaster userMaster = userMasterRepository.findOne(claimStatementDto.getUserMaster().getId());
			if ("APPROVED".equalsIgnoreCase(claimStatementDto.getFianlApproval())) {
				rebateClaimLog.setStage(AmountMovementType.APPROVED);
			}else if("FINAL_APPROVED".equalsIgnoreCase(claimStatementDto.getFianlApproval())) {
				rebateClaimLog.setStage(AmountMovementType.FINAL_APPROVED);	
			}else {
				rebateClaimLog.setStage(AmountMovementType.REJECTED);
			}
			rebateClaimLogRepository.save(rebateClaimLog);
			RebateClaimNote rebateClaimNote = new RebateClaimNote();
			rebateClaimNote = rebateClaimNoteRepository.getRebateClaimData(rebateClaim.getId());
			rebateClaimNote.setForwardTo(userMaster);
			rebateClaimNote.setNote(claimStatementDto.getNote());
			rebateClaimNote.setFinalApproval(claimStatementDto.getFinalApprovalStatus());
			rebateClaimNoteRepository.save(rebateClaimNote);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		catch(Exception exp) {
			log.error("Exception Cause  : ", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<==== End RebateClaim Service.approvedRejectClaimStatement ====>");
		return baseDTO;
	}

}