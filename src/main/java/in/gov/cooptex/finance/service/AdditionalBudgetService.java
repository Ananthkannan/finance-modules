package in.gov.cooptex.finance.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.service.NotificationEmailService;
import in.gov.cooptex.core.accounts.model.AdditionalBudget;
import in.gov.cooptex.core.accounts.model.AdditionalBudgetDetails;
import in.gov.cooptex.core.accounts.model.AdditionalBudgetLog;
import in.gov.cooptex.core.accounts.model.AdditionalBudgetNote;
import in.gov.cooptex.core.accounts.model.BudgetConfig;
import in.gov.cooptex.core.accounts.model.BudgetRequest;
import in.gov.cooptex.core.accounts.repository.GlAccountRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.finance.repository.AdditionalBudgetDetailsRepository;
import in.gov.cooptex.core.finance.repository.AdditionalBudgetLogRepository;
import in.gov.cooptex.core.finance.repository.AdditionalBudgetNoteRepository;
import in.gov.cooptex.core.finance.repository.AdditionalBudgetRepository;
import in.gov.cooptex.core.finance.repository.BudgetConfigRepository;
import in.gov.cooptex.core.finance.repository.BudgetRequestRepository;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.model.SystemNotification;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.AppQueryRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.SectionMasterRepository;
import in.gov.cooptex.core.repository.SystemNotificationRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.FinanceErrorCode;
import in.gov.cooptex.finance.dto.AdditionalBudgetDTO;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class AdditionalBudgetService {

	@Autowired
	BudgetRequestRepository budgetRequestRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	AppQueryRepository appQueryRepository;

	@Autowired
	BudgetConfigRepository budgetConfigRepository;

	@Autowired
	AdditionalBudgetRepository additionalBudgetRepository;

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	SectionMasterRepository sectionMasterRepository;

	@Autowired
	GlAccountRepository glAccountRepository;

	@Autowired
	UserMasterRepository userMasterRepository;

	@Autowired
	SystemNotificationRepository systemNotificationRepository;

	@Autowired
	AdditionalBudgetLogRepository additionalBudgetLogRepository;

	@Autowired
	AdditionalBudgetNoteRepository additionalBudgetNoteRepository;

	@Autowired
	AdditionalBudgetDetailsRepository additionalBudgetDetailsRepository;

	@Autowired
	NotificationEmailService notificationEmailService;

	@Autowired
	EmployeeMasterRepository employeeMasterRepository;

	@Autowired
	LoginService loginService;

	private static final String ADDITIONAL_BUDGET_VIEW_PAGE = "/pages/accounts/budget/viewAdditionalBudget.xhtml?faces-redirect=true";

	public BaseDTO getBudgetConfig(Long entityId) {
		log.info("----getBudgetConfig() service method starts------");
		BaseDTO baseDTO = new BaseDTO();
		List<BudgetRequest> budgetRequestList = null;
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		try {
			budgetRequestList = new ArrayList<>();
			String mainQuery = "select br.id as id,bc.code as code,bc.name as name from budget_request br join budget_config bc on bc.id = br.budget_config_id "
					+ "where br.entity_id = :entityId";
			log.info("database Query-----" + mainQuery);
			mainQuery = mainQuery.replaceAll(":entityId", entityId.toString());
			log.info("after replace id query--------" + mainQuery);

			listofData = jdbcTemplate.queryForList(mainQuery);
			log.info("list date size-------->" + listofData.size());

			if (listofData.size() > 0) {
				for (Map<String, Object> budget : listofData) {
					BudgetRequest budgetRequest = new BudgetRequest();
					budgetRequest.setId(Long.valueOf(budget.get("id").toString()));
					budgetRequest.setBudgetConfig(new BudgetConfig());
					budgetRequest.getBudgetConfig().setCode(budget.get("code").toString());
					budgetRequest.getBudgetConfig().setName(budget.get("name").toString());
					budgetRequestList.add(budgetRequest);
				}
			}
			baseDTO.setResponseContents(budgetRequestList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("getBudgetConfig exception---", e);
		}
		log.info("----getBudgetConfig() service method ends------");
		return baseDTO;
	}

	public BaseDTO generateBudget(Long requestId, Long entityId, Long sectionId, String budgetCode) {
		log.info("------generateBudget() starts------" + requestId);
		BaseDTO baseDTO = new BaseDTO();
		AdditionalBudgetDTO additionalBudgetDTO = null;
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		try {
			if (requestId != null && entityId != null && budgetCode != null) {
				AdditionalBudget additionalBudget = additionalBudgetRepository.findByRequestId(requestId, entityId,
						sectionId);
				log.info("generateBudget() additionalBudget obj------->" + additionalBudget);
				if (additionalBudget != null) {
					baseDTO.setStatusCode(ErrorDescription.getError((FinanceErrorCode.BUDGET_REQUEST_ALREADY_EXSISTS))
							.getErrorCode());
					return baseDTO;
				}
				additionalBudgetDTO = new AdditionalBudgetDTO();
				additionalBudgetDTO.setFixedBudgetDTOList(new ArrayList<>());
				additionalBudgetDTO.setVariableBudgetDTOList(new ArrayList<>());

				BudgetConfig budgetConfig = budgetConfigRepository.getByCode(budgetCode);
				log.info("budget config------->" + budgetConfig);

				ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("GENERATE_ADDITIONAL_BUDGET");
				if (applicationQuery == null) {
					log.info("---Application query not found----");
					return null;
				}
				String mainQuery = applicationQuery.getQueryContent();
				log.info("database Query-----" + mainQuery);
				mainQuery = mainQuery.replaceAll(":ENTITYID", entityId.toString());
				mainQuery = mainQuery.replaceAll(":SECTIONID", sectionId.toString());
				mainQuery = mainQuery.replaceAll(":BUDGET_REQUEST_ID", requestId.toString());
				mainQuery = mainQuery.replaceAll(":BUDGETCONFIGID", budgetConfig.getId().toString());
				log.info("after replace id query--------" + mainQuery);

				listofData = jdbcTemplate.queryForList(mainQuery);
				log.info("list date size-------->" + listofData.size());

				if (listofData.size() > 0) {
					for (Map<String, Object> budget : listofData) {
						AdditionalBudgetDTO budgetDTO = new AdditionalBudgetDTO();
						budgetDTO.setGlHeadId(Long.valueOf(budget.get("GL_ID").toString()));
						budgetDTO.setHeadOfAccount(
								budget.get("GL_CODE").toString() + "/" + budget.get("GL_NAME").toString());
						budgetDTO.setEstimationExpenseAmount(
								Double.valueOf(budget.get("ESTIMATION_EXPENSE_AMOUNT").toString()));
						budgetDTO
								.setActualExpenseAmount(Double.valueOf(budget.get("ACTUAL_EXPENSE_AMOUNT").toString()));
						budgetDTO.setBudgetAmount(Double.valueOf(budget.get("CURRENT_BUDGET_AMOUNT").toString()));
						budgetDTO.setImprestAmount(Double.valueOf(budget.get("IMPREST_AMOUNT").toString()));
						budgetDTO.setBalancebudget(budgetDTO.getBudgetAmount() - budgetDTO.getImprestAmount());
						if (budget.get("EXPENSE_CATEGORY") != null) {
							if (budget.get("EXPENSE_CATEGORY").equals("FIXED")) {
								additionalBudgetDTO.getFixedBudgetDTOList().add(budgetDTO);
							} else if (budget.get("EXPENSE_CATEGORY").equals("VARIABLE")) {
								additionalBudgetDTO.getVariableBudgetDTOList().add(budgetDTO);
							}
						}
					}
				}
				baseDTO.setResponseContent(additionalBudgetDTO);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			} else {
				log.info("---generateBudget() parameter values null-----");
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			}
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.error("generateBudget exception----", e);
		}
		return baseDTO;
	}

	@Transactional
	public BaseDTO saveAdditionalBudget(AdditionalBudgetDTO additionalBudgetDTO) {
		log.info("----saveAdditionalBudget method starts-----");
		List<AdditionalBudgetDTO> additionalBudgetDTOList = null;
		List<AdditionalBudgetDetails> additionalBudgetDetailsList = null;
		AdditionalBudget additionalBudget = null;
		BaseDTO baseDTO = new BaseDTO();
		try {
			additionalBudgetDTOList = new ArrayList<>();
			additionalBudgetDetailsList = new ArrayList<>();
			if (additionalBudgetDTO.getId() == null) {
				log.info("-----saveAdditionalBudget inside insert called-------");

				additionalBudget = new AdditionalBudget();
				additionalBudget.setAdditionalBudgetFrom(additionalBudgetDTO.getBudgetFromDate());
				additionalBudget.setAdditionalBudgetTo(additionalBudgetDTO.getBudgetToDate());

				BudgetRequest budgetRequest = budgetRequestRepository
						.findOne(additionalBudgetDTO.getBudgetRequest().getId());
				log.info("budgetRequest value-----" + budgetRequest.getId());

				EntityMaster entityMaster = entityMasterRepository.findById(additionalBudgetDTO.getEntityId());
				log.info("entity master value----->" + entityMaster.getId());

				if (additionalBudgetDTO.getSectionId() != null) {
					SectionMaster sectionMaster = sectionMasterRepository.findOne(additionalBudgetDTO.getSectionId());
					log.info("saveAdditionalBudget sectionMaster------" + sectionMaster.getId());
					additionalBudget.setSectionMaster(sectionMaster);
				}
				additionalBudget.setBudgetRequest(budgetRequest);
				additionalBudget.setEntityMaster(entityMaster);

				additionalBudgetDTOList.addAll(additionalBudgetDTO.getFixedBudgetDTOList());
				additionalBudgetDTOList.addAll(additionalBudgetDTO.getVariableBudgetDTOList());

				for (AdditionalBudgetDTO dto : additionalBudgetDTOList) {
					AdditionalBudgetDetails additionalBudgetDetails = new AdditionalBudgetDetails();
					additionalBudgetDetails.setAdditionalBudget(additionalBudget);
					additionalBudgetDetails.setGlAccount(glAccountRepository.findById(dto.getGlHeadId()));
					additionalBudgetDetails.setPreviousBudgetAmount(dto.getEstimationExpenseAmount());
					additionalBudgetDetails.setPreviousExpenseAmount(dto.getActualExpenseAmount());
					additionalBudgetDetails.setCurrentBudgetAmount(dto.getBudgetAmount());
					additionalBudgetDetails.setCurrentExpenseAmount(dto.getImprestAmount());
					additionalBudgetDetails.setAdditionalBudgetAmount(dto.getAdditionalBudget());
					additionalBudgetDetailsList.add(additionalBudgetDetails);
				}
				additionalBudget.setAdditionalBudgetDetailsList(additionalBudgetDetailsList);
				additionalBudgetRepository.save(additionalBudget);
				log.info("---------saveAdditionalBudget additionalbudget inserted-------");
			} else {
				log.info("-----saveAdditionalBudget inside update called-------");
				log.info("additional budget id-------------->" + additionalBudgetDTO.getId());
				additionalBudgetDTOList.addAll(additionalBudgetDTO.getFixedBudgetDTOList());
				additionalBudgetDTOList.addAll(additionalBudgetDTO.getVariableBudgetDTOList());
				additionalBudget = additionalBudgetRepository.findOne(additionalBudgetDTO.getId());
				for (AdditionalBudgetDTO dto : additionalBudgetDTOList) {
					log.info("additional budget details id-------------->" + dto.getDetailsId());
					AdditionalBudgetDetails additionalBudgetDetails = additionalBudgetDetailsRepository
							.findOne(dto.getDetailsId());
					additionalBudgetDetails.setAdditionalBudget(additionalBudget);
					additionalBudgetDetails.setGlAccount(glAccountRepository.findById(dto.getGlHeadId()));
					additionalBudgetDetails.setPreviousBudgetAmount(dto.getEstimationExpenseAmount());
					additionalBudgetDetails.setPreviousExpenseAmount(dto.getActualExpenseAmount());
					additionalBudgetDetails.setCurrentBudgetAmount(dto.getBudgetAmount());
					additionalBudgetDetails.setCurrentExpenseAmount(dto.getImprestAmount());
					additionalBudgetDetails.setAdditionalBudgetAmount(dto.getAdditionalBudget());
					additionalBudgetDetailsList.add(additionalBudgetDetails);
				}
				additionalBudgetDetailsRepository.save(additionalBudgetDetailsList);
				log.info("---------saveAdditionalBudget details updated-------");
			}

			AdditionalBudgetNote additionalBudgetNote = new AdditionalBudgetNote();
			additionalBudgetNote.setAdditionalBudget(additionalBudget);
			additionalBudgetNote.setFinalApproval(additionalBudgetDTO.getFinalApproval());
			additionalBudgetNote
					.setForwardTo(userMasterRepository.findOne(additionalBudgetDTO.getUserMaster().getId()));
			additionalBudgetNote.setNote(additionalBudgetDTO.getNote());
			additionalBudgetNoteRepository.save(additionalBudgetNote);
			log.info("---------saveAdditionalBudget note inserted-------");

			AdditionalBudgetLog additionalBudgetLog = new AdditionalBudgetLog();
			additionalBudgetLog.setAdditionalBudget(additionalBudget);
			additionalBudgetLog.setRemarks(additionalBudgetDTO.getRemarks());
			additionalBudgetLog.setStage("SUBMITTED");
			additionalBudgetLogRepository.save(additionalBudgetLog);

			/*
			 * Send Notification Mail
			 */

			if (additionalBudget != null && additionalBudget.getId() != null) {
				Map<String, Object> additionalData = new HashMap<>();
				Long empId = employeeMasterRepository.getEmployeeIdByUserId(additionalBudget.getCreatedBy().getId());
				additionalData.put("Url", ADDITIONAL_BUDGET_VIEW_PAGE + "&budgetTransferId=" + additionalBudget.getId()
						+ "&empId=" + empId + "&");
				notificationEmailService.sendMailAndNotificationForForward(additionalBudgetNote, additionalBudgetLog,
						additionalData);
			}

			log.info("---------saveAdditionalBudget log inserted-------");
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.error("saveAdditionalBudget exception------", e);
		}
		log.info("----saveAdditionalBudget method ends-----");
		return baseDTO;
	}

	public BaseDTO lazyLoadAdditionalBudget(PaginationDTO paginationDTO) {
		log.info("------------lazyLoadBudgetConsolidation service method starts---------------");
		BaseDTO baseDTO = new BaseDTO();
		List<AdditionalBudgetDTO> additionalbudgetDTOList = null;
		Integer totalRecords = 0;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			additionalbudgetDTOList = new ArrayList<AdditionalBudgetDTO>();
			Integer start = paginationDTO.getFirst(), pageSize = paginationDTO.getPageSize();
			start = start * pageSize;
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();

			ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("ADDITIONAL_BUDGET_LIST");
			if (applicationQuery == null) {
				log.info("--------application query not found-----");
				return null;
			}
			UserMaster userMaster = loginService.getCurrentUser();
			Long loginUserId = userMaster == null ? null : userMaster.getId();
			
			String mainQuery = applicationQuery.getQueryContent();
			mainQuery = StringUtils.replace(mainQuery, ":userId", String.valueOf(loginUserId));
			log.info("db query----------" + mainQuery);
			
			

			if (paginationDTO.getFilters() != null) {
				if (paginationDTO.getFilters().get("budgetName") != null) {
					mainQuery += " and bc.code='" + paginationDTO.getFilters().get("budgetName") + "'";
				}
				if (paginationDTO.getFilters().get("budgetFromDate") != null) {
					Date fromDate = new Date((long) paginationDTO.getFilters().get("budgetFromDate"));
					log.info(" budgetToDate Filter : " + fromDate);
					mainQuery += " and ab.additional_budget_from::date='" + format.format(fromDate) + "'";
				}
				if (paginationDTO.getFilters().get("budgetToDate") != null) {
					Date toDate = new Date((long) paginationDTO.getFilters().get("budgetToDate"));
					log.info(" budgetToDate Filter : " + toDate);
					mainQuery += " and ab.additional_budget_to::date='" + format.format(toDate) + "'";
				}
				if (paginationDTO.getFilters().get("entityCodeName") != null) {
					mainQuery += " and em.code='" + paginationDTO.getFilters().get("entityCodeName") + "'";
				}
				if (paginationDTO.getFilters().get("createdDate") != null) {
					Date fromDate = new Date((long) paginationDTO.getFilters().get("createdDate"));
					log.info(" fromDate Filter : " + fromDate);
					mainQuery += " and ab.created_date::date='" + format.format(fromDate) + "'";
				}
				if (paginationDTO.getFilters().get("stage") != null) {
					mainQuery += " and abl.stage='" + paginationDTO.getFilters().get("stage") + "'";
				}
			}
			
			String countQuery = "select count(*) as count from (" + mainQuery + ")t ";

			/*
			 * String countQuery = mainQuery.replace(
			 * "select ab.id as id,bc.code as budget_code,bc.name as budget_name,ab.additional_budget_from as from_date,\n"
			 * +
			 * "ab.additional_budget_to as to_date,abl.stage as stage,ab.created_date as created_date,\n"
			 * + "em.code as entity_code,em.name as entity_name",
			 * "select count(distinct(ab.id)) as count ");
			 */
			log.info("count query... " + countQuery);
			countData = jdbcTemplate.queryForList(countQuery);

			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					totalRecords = Integer.parseInt(data.get("count").toString().replace(",", ""));
			}

			if (paginationDTO.getSortField() == null)
				mainQuery += " order by ab.id desc limit " + pageSize + " offset " + start + ";";

			if (paginationDTO.getSortField() != null && paginationDTO.getSortOrder() != null) {
				log.info("Sort Field:[" + paginationDTO.getSortField() + "] Sort Order:[" + paginationDTO.getSortOrder()
						+ "]");
				if (paginationDTO.getSortField().equals("budgetName")
						&& paginationDTO.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by bc.code asc  ";
				}
				if (paginationDTO.getSortField().equals("budgetName")
						&& paginationDTO.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by bc.code desc  ";
				}
				if (paginationDTO.getSortField().equals("budgetFromDate")
						&& paginationDTO.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by ab.additional_budget_from asc ";
				}
				if (paginationDTO.getSortField().equals("budgetFromDate")
						&& paginationDTO.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by ab.additional_budget_from desc ";
				}
				if (paginationDTO.getSortField().equals("budgetToDate")
						&& paginationDTO.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by ab.additional_budget_to asc  ";
				}
				if (paginationDTO.getSortField().equals("budgetToDate")
						&& paginationDTO.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by ab.additional_budget_to desc  ";
				}
				if (paginationDTO.getSortField().equals("createdDate")
						&& paginationDTO.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by ab.created_date asc  ";
				}
				if (paginationDTO.getSortField().equals("createdDate")
						&& paginationDTO.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by ab.created_date desc  ";
				}
				if (paginationDTO.getSortField().equals("stage") && paginationDTO.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by abl.stage asc ";
				}
				if (paginationDTO.getSortField().equals("stage") && paginationDTO.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by abl.stage desc ";
				}
				if (paginationDTO.getSortField().equals("entityCodeName")
						&& paginationDTO.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by em.code asc ";
				}
				if (paginationDTO.getSortField().equals("entityCodeName")
						&& paginationDTO.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by em.code desc ";
				}
				mainQuery += " limit " + pageSize + " offset " + start + ";";
			}

			log.info("filter query----------" + mainQuery);

			listofData = jdbcTemplate.queryForList(mainQuery);
			log.info("additionalbudget list size-------------->" + listofData.size());

			for (Map<String, Object> data : listofData) {
				AdditionalBudgetDTO additionalBudgetDTO = new AdditionalBudgetDTO();
				additionalBudgetDTO.setId(Long.valueOf(data.get("id").toString()));
				additionalBudgetDTO.setStage(data.get("stage").toString());
				additionalBudgetDTO
						.setBudgetName(data.get("budget_code").toString() + "/" + data.get("budget_name").toString());
				additionalBudgetDTO.setBudgetFromDate(format.parse(data.get("from_date").toString()));
				additionalBudgetDTO.setBudgetToDate(format.parse(data.get("to_date").toString()));
				additionalBudgetDTO.setCreatedDate(format.parse(data.get("created_date").toString()));
				additionalBudgetDTO.setEntityCodeName(
						data.get("entity_code").toString() + "/" + data.get("entity_name").toString());
				additionalbudgetDTOList.add(additionalBudgetDTO);
			}
			baseDTO.setTotalRecords(totalRecords);
			baseDTO.setResponseContents(additionalbudgetDTOList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("lazyLoadAdditionalBudget exception", e);
		}

		return baseDTO;
	}

	public BaseDTO viewAdditionalBudget(AdditionalBudgetDTO additionalBudgetDTO) {
		log.info("---------starts viewAdditionalBudget service-------------");
		BaseDTO baseDTO = new BaseDTO();
		Long budgetId = additionalBudgetDTO.getId();
		Long notificationId = additionalBudgetDTO.getNotificationId();
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		try {
			if (budgetId != null) {
				additionalBudgetDTO = new AdditionalBudgetDTO();
				additionalBudgetDTO.setFixedBudgetDTOList(new ArrayList<>());
				additionalBudgetDTO.setVariableBudgetDTOList(new ArrayList<>());

				ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("VIEW_ADDITIONAL_BUDGET");
				if (applicationQuery == null) {
					log.info("---Application query not found----");
					return null;
				}
				String mainQuery = applicationQuery.getQueryContent();
				log.info("database Query-----" + mainQuery);
				mainQuery = mainQuery.replaceAll(":budgetId", budgetId.toString());
				log.info("after replace id query--------" + mainQuery);

				listofData = jdbcTemplate.queryForList(mainQuery);
				log.info("list date size-------->" + listofData.size());

				if (listofData.size() > 0) {
					int i = 0;
					additionalBudgetDTO.setId(Long.valueOf(listofData.get(i).get("id").toString()));
					additionalBudgetDTO.setBudgetName(listofData.get(i).get("budget_code").toString() + "/"
							+ listofData.get(i).get("budget_name").toString());
					additionalBudgetDTO.setBudgetFromDate((Date) (listofData.get(i).get("budget_from")));
					additionalBudgetDTO.setBudgetToDate((Date) (listofData.get(i).get("budget_to")));
					additionalBudgetDTO.setEntityCodeName(listofData.get(i).get("entity_code").toString() + "/"
							+ listofData.get(i).get("entity_name").toString());
					if (listofData.get(i).get("section_code") != null
							&& listofData.get(i).get("section_name") != null) {
						additionalBudgetDTO.setSectionCodeName(listofData.get(i).get("section_code").toString() + "/"
								+ listofData.get(i).get("section_name").toString());
					}
					additionalBudgetDTO.setNote(listofData.get(i).get("note").toString());
					additionalBudgetDTO.setFinalApproval((Boolean) listofData.get(i).get("finalapproval"));
					additionalBudgetDTO.setStage(listofData.get(i).get("stage").toString());
					Long forwardToId = Long.valueOf(listofData.get(i).get("forwardto").toString());
					log.info("forwarto id--------" + forwardToId);
					additionalBudgetDTO.setUserMaster(userMasterRepository.findOne(forwardToId));

					for (Map<String, Object> data : listofData) {
						AdditionalBudgetDTO budgetDTO = new AdditionalBudgetDTO();
						budgetDTO.setDetailsId(Long.valueOf(data.get("details_id").toString()));
						budgetDTO.setGlHeadId(Long.valueOf(data.get("gl_id").toString()));
						budgetDTO.setHeadOfAccount(
								data.get("gl_code").toString() + "/" + data.get("gl_name").toString());
						budgetDTO.setEstimationExpenseAmount(
								Double.valueOf(data.get("previous_budget_amount").toString()));
						budgetDTO
								.setActualExpenseAmount(Double.valueOf(data.get("previous_expense_amount").toString()));
						budgetDTO.setBudgetAmount(Double.valueOf(data.get("current_budget_amount").toString()));
						budgetDTO.setImprestAmount(Double.valueOf(data.get("current_expense_amount").toString()));
						budgetDTO.setBalancebudget(budgetDTO.getBudgetAmount() - budgetDTO.getImprestAmount());
						budgetDTO.setAdditionalBudget(Double.valueOf(data.get("additional_budget_amount").toString()));
						if (data.get("expense_category") != null) {
							if (data.get("expense_category").equals("FIXED")) {
								additionalBudgetDTO.getFixedBudgetDTOList().add(budgetDTO);
							} else if (data.get("expense_category").equals("VARIABLE")) {
								additionalBudgetDTO.getVariableBudgetDTOList().add(budgetDTO);
							}
						}
					}
				}

				if (notificationId != null && notificationId > 0) {
					SystemNotification systemNotification = systemNotificationRepository.findOne(notificationId);
					systemNotification.setNotificationRead(true);
					systemNotificationRepository.save(systemNotification);
				}
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
				baseDTO.setResponseContent(additionalBudgetDTO);
			}
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("viewAdditionalBudget service exception-----", e);
		}
		log.info("---------ends viewAdditionalBudget service-------------");
		return baseDTO;
	}

	@Transactional
	public BaseDTO approveOrReject(AdditionalBudgetDTO additionalBudgetDTO) {
		log.info("approveOrReject() starts-------" + additionalBudgetDTO.getId());
		BaseDTO baseDTO = new BaseDTO();
		try {
			if (additionalBudgetDTO.getId() != null) {

				AdditionalBudget additionalBudget = additionalBudgetRepository.findOne(additionalBudgetDTO.getId());
				log.info("additional budget-------" + additionalBudget);

					AdditionalBudgetNote additionalBudgetNote = new AdditionalBudgetNote();
					additionalBudgetNote.setAdditionalBudget(additionalBudget);
					additionalBudgetNote.setFinalApproval(additionalBudgetDTO.getFinalApproval());
					additionalBudgetNote
							.setForwardTo(userMasterRepository.findOne(additionalBudgetDTO.getUserMaster().getId()));
					additionalBudgetNote.setNote(additionalBudgetDTO.getNote());
					additionalBudgetNoteRepository.save(additionalBudgetNote);
					log.info("----approveOrReject() note inserted-----");

					AdditionalBudgetLog additionalBudgetLog = new AdditionalBudgetLog();
					additionalBudgetLog.setAdditionalBudget(additionalBudget);
					additionalBudgetLog.setRemarks(additionalBudgetDTO.getRemarks());
					additionalBudgetLog.setStage(additionalBudgetDTO.getStage());
					additionalBudgetLogRepository.save(additionalBudgetLog);
					log.info("----approveOrReject() log inserted-----");

					if (ApprovalStage.FINALAPPROVED.equals(additionalBudgetDTO.getStage())) {
						String urlPath = ADDITIONAL_BUDGET_VIEW_PAGE + "&budgetTransferId="
								+ additionalBudgetDTO.getId() + "&";
						Long toUserId = additionalBudget.getCreatedBy().getId();
						notificationEmailService.saveIndividualNotification(loginService.getCurrentUser(), toUserId,
								urlPath, "Additional Budget", " Additional Budget  has been final approved");
					} else {
						Map<String, Object> additionalData = new HashMap<>();
						additionalData.put("Url",
								ADDITIONAL_BUDGET_VIEW_PAGE + "&budgetTransferId=" + additionalBudgetDTO.getId() + "&");
						notificationEmailService.sendMailAndNotificationForForward(additionalBudgetNote,
								additionalBudgetLog, additionalData);
					}

				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			}
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("approveOrReject eception--------->", e);
		}
		log.info("----approveOrReject() ends-------");
		return baseDTO;
	}

}
