package in.gov.cooptex.finance.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import in.gov.cooptex.common.repository.AppConfigRepository;
import in.gov.cooptex.common.repository.BankMasterRepository;
import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.enums.VoucherStatus;
import in.gov.cooptex.core.accounts.enums.VoucherTypeDetails;
import in.gov.cooptex.core.accounts.model.ChequeBook;
import in.gov.cooptex.core.accounts.model.EntityBankBranch;
import in.gov.cooptex.core.accounts.model.Payment;
import in.gov.cooptex.core.accounts.model.PaymentDetails;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.VoucherDetails;
import in.gov.cooptex.core.accounts.model.VoucherLog;
import in.gov.cooptex.core.accounts.model.VoucherType;
import in.gov.cooptex.core.accounts.repository.BankBranchMasterRepository;
import in.gov.cooptex.core.accounts.repository.ChequeBookRepository;
import in.gov.cooptex.core.accounts.repository.EntityBankBranchRepository;
import in.gov.cooptex.core.accounts.repository.PaymentDetailsRepository;
import in.gov.cooptex.core.accounts.repository.PaymentMethodRepository;
import in.gov.cooptex.core.accounts.repository.PaymentRepository;
import in.gov.cooptex.core.accounts.repository.PaymentTypeMasterRepository;
import in.gov.cooptex.core.accounts.repository.VoucherDetailsRepository;
import in.gov.cooptex.core.accounts.repository.VoucherLogRepository;
import in.gov.cooptex.core.accounts.repository.VoucherRepository;
import in.gov.cooptex.core.accounts.repository.VoucherTypeRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.enums.PaymentCategory;
import in.gov.cooptex.core.enums.PaymentMode;
import in.gov.cooptex.core.enums.SequenceName;
import in.gov.cooptex.core.finance.service.PaymentFinanceService;
import in.gov.cooptex.core.model.AppConfig;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.BankMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.SequenceConfig;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.PaymentModeRepository;
import in.gov.cooptex.core.repository.SequenceConfigRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.FinanceErrorCode;
import in.gov.cooptex.exceptions.LedgerPostingException;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.finance.dto.BankPaymentDTO;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class CashCompletionService {
	@Autowired
	ResponseWrapper responseWrapper;
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	EntityMasterRepository entityMasterRepository;
	
	@Autowired
	PaymentDetailsRepository paymentDetailsRepository;
	
	@Autowired
	VoucherTypeRepository voucherTypeRepository;
	
	@Autowired
	VoucherRepository voucherRepository;
	
	@Autowired
	UserMasterRepository userMasterRepository;
	
	@Autowired
	EntityManager entityManager;
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	ApplicationQueryRepository applicationQueryRepository;
	
	@Autowired
	BankMasterRepository bankMasterRepository;
	
	@Autowired
	EntityBankBranchRepository entityBankBranchRepository;
	
	@Autowired
	PaymentModeRepository paymentModeRepository;
	
	@Autowired
	PaymentRepository  paymentRepository;
	
	@Autowired
	VoucherLogRepository voucherLogRepository;
	
	@Autowired
	SequenceConfigRepository sequenceConfigRepository;
	
	@Autowired
	PaymentTypeMasterRepository paymentTypeMasterRepository;
	
	@Autowired	
	PaymentMethodRepository paymentMethodRepository;
	
	@Autowired	
	VoucherDetailsRepository voucherDetailsRepository;
	
	@Autowired	
	ChequeBookRepository chequeBookRepository;
	
	@Autowired	
	BankBranchMasterRepository bankBranchMasterRepository;
	
	@Autowired
	AppConfigRepository appConfigRepository;
	
	@Autowired
	PaymentFinanceService paymentFinanceService;
	
//	public BaseDTO loadListofVoucherList() {
//		log.info("<--CashCompletionService() .getListofVoucherTypeList() Started-->");
//		BaseDTO baseDTO = new BaseDTO();
//		try {
//
//			log.info("<--getListofVoucherTypeList() fetch success-->");
//			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
//		} catch (Exception e) {
//			log.error("CashCompletionService() Exception in getListofVoucherTypeList() ",e);
//			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
//		}
//		return responseWrapper.send(baseDTO);
//	}
	
	public BaseDTO getAdvancePaymentLazy(BankPaymentDTO paginationDto) {
		log.info(" getallDesignTargetlazy  called...");
		BaseDTO baseDTO = new BaseDTO();

		try{
		 	Integer total=0;
		 	Integer start = paginationDto.getFirst(),pageSize = paginationDto.getPagesize();
			start=start*pageSize;
			Boolean generateStatus = paginationDto.getGenerateStatus();
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();

			int numberOfVoucherPaid = 0;
			int numberOfVoucher = 0;
			Double voucherPaid = 0.0;
			Double voucherTotal = 0.0;

			String queryName="FINANCE_ACCOUNTS_CASH_COMPLETION_LIST";
			String queryNameCount="FINANCE_ACCOUNTS_CASH_COMPLETION_COUNT_QUERY";
			
			ApplicationQuery applicationQuery = applicationQueryRepository.findByQueryName(queryName);
			String mainQuery = applicationQuery.getQueryContent().trim();
			
			applicationQuery = applicationQueryRepository.findByQueryName(queryNameCount);
			String queryCount = applicationQuery.getQueryContent().trim();
			String statusType1 = VoucherStatus.FINALAPPROVED.toString();
			String statusType2 = VoucherStatus.PAID.toString();
			String statusType3 = VoucherStatus.RECEIVED.toString();
			String paymentMode = PaymentMode.CASH.toString();


			mainQuery += " where vl.status in ('"+statusType1+"','"+statusType2+"','"+statusType3+"') " + 
					" and upper(vm.payment_mode) ='"+paymentMode+"' and (sm.active_status=true or sm.active_status is null)";
			String chequeStatusQuery = null;
			if(paginationDto.getId()!=null) {
				mainQuery += " and t.id='"+paginationDto.getId()+"'";
			}
			if(paginationDto.getVoucherNumber()!=null) {
				mainQuery += " and ( upper(v.reference_number_prefix) like upper('%"+paginationDto.getVoucherNumber()+"%') or ";
				mainQuery += " cast(v.reference_number as varchar) like upper('%"+paginationDto.getVoucherNumber()+"%') )";
			}
			if(paginationDto.getTransactionName()!=null) {
				mainQuery += " and upper(v.name) like upper('%"+paginationDto.getTransactionName()+"%')";
			}
			if(paginationDto.getTransactionType()!=null) {
				mainQuery += " and upper(vt.name) like upper('%"+paginationDto.getTransactionType()+"%')";
			}
			
			if(paginationDto.getVoucherDate()!=null) {
				Date createdDate=paginationDto.getVoucherDate();
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				mainQuery += " and v.created_date::date = date '"+dateFormat.format(createdDate)+"'";	
			}
			if(paginationDto.getNetAmount()!=null) {
				Double value =Double.parseDouble(paginationDto.getNetAmount().toString());
				Integer intValue = value.intValue();
				mainQuery += " and cast(v.net_amount as varchar) like '%"+intValue+"%' ";
			}
			 
			if(paginationDto.getAmontPaidDate()!=null) {
				Date createdDate=paginationDto.getAmontPaidDate();
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				mainQuery += " and pd.created_date::date = date '"+dateFormat.format(createdDate)+"'";	
			}
			if(paginationDto.getStatus()!=null) {
				mainQuery += " and upper(vl.status) like upper('%"+paginationDto.getStatus()+"%')";
			}
			 
			
			String countQuery=mainQuery.replace(queryCount, " select count(*) as count ");

			
			countData=jdbcTemplate.queryForList(countQuery);
			for(Map<String,Object> data:countData){
				if(data.get("count")!=null)
					total=Integer.parseInt(data.get("count").toString().replace(",", ""));
			}
			chequeStatusQuery = mainQuery;
			if(paginationDto.getSortField()==null)
				mainQuery+=" order by id desc limit "+pageSize+" offset "+start+";";
			
			if(paginationDto.getSortField()!=null && paginationDto.getSortOrder()!=null)
			{

				if(paginationDto.getSortField().equals("id") && paginationDto.getSortOrder().equals("ASCENDING"))
						mainQuery+=" order by id asc ";
				if(paginationDto.getSortField().equals("id") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by id desc ";
				
				if(paginationDto.getSortField().equals("voucherNumber") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by voucherNumber asc  ";
				if(paginationDto.getSortField().equals("voucherNumber") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by voucherNumber desc  ";
				
				if(paginationDto.getSortField().equals("transactionName") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by v.name asc  ";
				if(paginationDto.getSortField().equals("transactionName") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by v.name desc  ";
				
				if(paginationDto.getSortField().equals("voucherDate") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by voucherDate asc  ";
				if(paginationDto.getSortField().equals("voucherDate") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by voucherDate desc  ";
				
				if(paginationDto.getSortField().equals("netAmount") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by net_amount asc  ";
				if(paginationDto.getSortField().equals("netAmount") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by net_amount desc  ";
				
				if(paginationDto.getSortField().equals("amontPaidDate") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by pd.created_date asc  ";
				if(paginationDto.getSortField().equals("amontPaidDate") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by pd.created_date desc  ";
				
				if(paginationDto.getSortField().equals("bankNameDeposited") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by bankNameDeposited asc  ";
				if(paginationDto.getSortField().equals("bankNameDeposited") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by bankNameDeposited desc  ";
				
				if(paginationDto.getSortField().equals("status") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by status asc  ";
				if(paginationDto.getSortField().equals("status") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by status desc  ";
			
				mainQuery+=" limit "+pageSize+" offset "+start+";";
			}

			
			log.info("mainQuery :"+mainQuery);
			log.info("Count mainQuery :"+countQuery);
			listofData = jdbcTemplate.queryForList(mainQuery);
			List<BankPaymentDTO> bankPaymentDTOList = new ArrayList<>();
			
			for(Map<String,Object> data:listofData){
				BankPaymentDTO dto = new BankPaymentDTO();

				if(data.get("id")!=null) {
					dto.setId(Long.parseLong(data.get("id").toString()));
				}
				if(data.get("voucherNumber")!=null) {
					dto.setVoucherNumber(data.get("voucherNumber").toString());
				}
				if(data.get("transactionName")!=null) {
					dto.setTransactionName(data.get("transactionName").toString());
				}
				if(data.get("voucherType")!=null) {
					dto.setTransactionType(data.get("voucherType").toString());
				}
				if(data.get("voucherDate")!=null) {
					dto.setVoucherDate((Date)data.get("voucherDate"));
				}else {
					dto.setVoucherDate(null);
				}
				if(data.get("net_amount")!=null) {
					dto.setNetAmount(Double.parseDouble(data.get("net_amount").toString()));
				} 
				if(data.get("amountPaidDate")!=null) {
					dto.setAmontPaidDate((Date)data.get("amountPaidDate"));
				}else {
					dto.setAmontPaidDate(null);
				}
				if(data.get("status")!=null) {
					dto.setStatus(data.get("status").toString());
				}
				if(data.get("supplierCodeName")!=null) {
					dto.setSupplierCodeName(data.get("supplierCodeName").toString());
				}
				if(data.get("supplierID")!=null) {
					dto.setSupplierID(Long.parseLong(data.get("supplierID").toString()));
				}
				if(data.get("payment_mode")!=null) {
					dto.setPaymentModeName(data.get("payment_mode").toString());
				}
				if(data.get("paymentModeID")!=null) {
					dto.setPaymentMode(paymentModeRepository.findOne(Long.parseLong(data.get("paymentModeID").toString()))); 
					dto.setPaymentModeID(Long.parseLong(data.get("paymentModeID").toString()));
				}
				if(data.get("voucherTypeID")!=null) {
					dto.setVoucherType(voucherTypeRepository.findOne(Long.parseLong(data.get("voucherTypeID").toString()))); 
					dto.setVoucherTypeID(Long.parseLong(data.get("voucherTypeID").toString()));
				}
				dto.setReceiptFlag(false);
				bankPaymentDTOList.add(dto);
			} 
			
			BankPaymentDTO bankPaymentDTO=new BankPaymentDTO();
			int paidVoucherCount = 0;
			if(generateStatus == true) {
				listofData = jdbcTemplate.queryForList(chequeStatusQuery); 
				for(Map<String,Object> data:listofData){
					Double value = 0.0;
					if(data.get("net_amount")!=null) {
						value = Double.parseDouble(data.get("net_amount").toString());
					}
					voucherTotal+=value;
					if(data.get("status")!=null) {
						String effectiveStatus = data.get("status").toString();
						numberOfVoucher++;
						if(effectiveStatus.equalsIgnoreCase(statusType2) || effectiveStatus.equalsIgnoreCase(statusType3)) {
							numberOfVoucherPaid++;
							voucherPaid+=value;
							paidVoucherCount++;
						}
					}
				}
				bankPaymentDTO.setNumberOfVoucher(numberOfVoucher);
				bankPaymentDTO.setNumberOfVoucherPaid(numberOfVoucherPaid);
				bankPaymentDTO.setNumberOfVoucherYetPaid(numberOfVoucher - numberOfVoucherPaid);
				bankPaymentDTO.setVoucherTotalAmount(voucherTotal);
				bankPaymentDTO.setVoucherPaidAmount(voucherPaid);
				bankPaymentDTO.setVoucherAmountYetPaid(voucherTotal - voucherPaid);
				bankPaymentDTO.setVoucherTotalAverage(100);
				bankPaymentDTO.setVoucherPaidAverage(Math.round(paidVoucherCount*100/numberOfVoucher));
				bankPaymentDTO.setVoucherYetPaidAverage(Math.round(100 - bankPaymentDTO.getVoucherPaidAverage()));
			}
			bankPaymentDTO.setBankPaymentDTOList(bankPaymentDTOList);
			baseDTO.setResponseContent(bankPaymentDTO);
			baseDTO.setTotalRecords(total);
			log.info("sise of total :"+total);
			log.info("main Query:"+mainQuery);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		}catch(Exception exp){
			log.error("CashCompletionService() ::Exception Cause  lazy Search : " , exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO ;
	}
	
	public BaseDTO loadBankMaterByUserEntityID(Long id) {
		log.info(" loadBankMaterByUserEntityID  called...");
		BaseDTO baseDTO = new BaseDTO();
		try{	

			List<Object[]> objList = bankMasterRepository.loadBankMaterByUserEntityID(id);
			List<BankMaster> bankMasterList = new ArrayList<>();
			objList.forEach(vv -> {
				BankMaster dto = new BankMaster();
				dto.setId(new Long(vv[0].toString()));
				dto.setBankCode(vv[1].toString());
				dto.setBankName(vv[2].toString());
				bankMasterList.add(dto);
			});
			baseDTO.setResponseContent(bankMasterList);
			baseDTO.setTotalRecords(bankMasterList.size());
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		}catch(Exception exp){
			log.error("CashCompletionService() :: Exception Cause loadBankMaterByUserEntityID(): " , exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO ;
	}
	public BaseDTO loadEntityBankBranchAccountDetails(Long id) {
		log.info(" loadEntityBankBranchAccountDetails  called...");
		BaseDTO baseDTO = new BaseDTO();
		try{	
			List<EntityBankBranch> bankMasterList = entityBankBranchRepository.getEntityAccountDetailsByBranchID(id);
			baseDTO.setResponseContent(bankMasterList);
			baseDTO.setTotalRecords(bankMasterList.size());
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		}catch(Exception exp){
			log.error("CashCompletionService() :: Exception Cause loadEntityBankBranchAccountDetails(): " , exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO ;
	}
	
	@Transactional
	public BaseDTO create(BankPaymentDTO dto) {
		log.info(" loadCreateBankPaymentDetails  called...");
		BaseDTO baseDTO = new BaseDTO();
		Payment payment = new Payment();
		try {
			UserMaster userMaster = userMasterRepository.findOne(loginService.getCurrentUser().getId());
			VoucherLog voucherLog = new VoucherLog();
			List<BankPaymentDTO> selectedBankPaymentDTOList = dto.getSelectedBankPaymentDTOList();
			PaymentDetails paymentDetails = new PaymentDetails();

			Long entityID = dto.getEntityMasterID();
			// EntityBankBranch entityBankBranch=
			// entityBankBranchRepository.findOne(dto.getEntityBankBranch().getId());
			SequenceConfig sequenceConfig = sequenceConfigRepository
					.findBySequenceName(SequenceName.valueOf(SequenceName.FINANCE_ACCOUNT_BANK_PAYMENT.name()));
			Long sequenceNumber = sequenceConfig.getCurrentValue();
			String refrenceNumberPrefix = entityID.toString() + sequenceConfig.getSeparator()
					+ sequenceConfig.getPrefix() + AppUtil.getCurrentMonthString() + AppUtil.getCurrentYearString();
			Payment paymentReturn = null;
			VoucherType voucherType = voucherTypeRepository
					.findOne(selectedBankPaymentDTOList.get(0).getVoucherType().getId());
			if (voucherType.getName().equalsIgnoreCase(VoucherTypeDetails.Payment.toString())) {
				log.info("Processed Payment Mode:");
				EntityMaster loginUserEntity = entityMasterRepository.findByUserRegion(userMaster.getId());
				if (loginUserEntity == null) {
					log.info("Login User Info Not Available");
					throw new RestException(ErrorDescription.LOGIN_USER_INFO_NOT_AVAILABLE);
				}
				payment.setEntityMaster(loginUserEntity);
				payment.setPaymentNumberPrefix(refrenceNumberPrefix);
				payment.setPaymentNumber(sequenceNumber);
				payment.setCreatedDate(new Date());
				payment.setCreatedBy(loginService.getCurrentUser());
				payment.setCreatedByName(loginService.getCurrentUser().getUsername());
				paymentReturn = paymentRepository.save(payment);
				log.info("CashCompletionService() :: Payment Save Succesfully " + paymentReturn.getId());
			} else if (voucherType.getName().equalsIgnoreCase(VoucherTypeDetails.Receipt.toString())) {
				log.info("CashCompletionService() :: Processed Receipt Mode:");
			}
			for (BankPaymentDTO bankPaymentDTO : selectedBankPaymentDTOList) {
				Voucher voucher = null;
				if (voucherType.getName().equalsIgnoreCase(VoucherTypeDetails.Payment.toString())) {
					voucher = voucherRepository.findOne(bankPaymentDTO.getId());
					// paymentDetails.setBankReferenceNumber(dto.getChequeReferenceNumber());
					// paymentDetails.setDocumentDate(dto.getChequeReferenceDate());

					paymentDetails.setPayment(paymentReturn);
					paymentDetails.setCreatedDate(new Date());
					paymentDetails.setCreatedBy(userMaster);
					paymentDetails.setCreatedByName(userMaster.getUsername());
					paymentDetails.setPaymentCategory(PaymentCategory.PAID_OUT);
					// List<VoucherDetails> voucherDetails =
					// voucherDetailsRepository.findVoucherDetailsByVoucherId(voucher.getId());
					// VoucherDetails voucherDetails =
					// voucherDetailsRepository.getVoucherDetailsByVoucherID(voucher.getId());
					List<VoucherDetails> voucherDetailsList = voucherDetailsRepository
							.findVoucherDetailsByVoucherId(voucher.getId());
					if (voucher.getSupplierType() != null) {
						paymentDetails.setPaymentTypeMaster(paymentTypeMasterRepository.getSupplierPaymentType()); // Supplier
																													// type
																													// set
					} else if (voucherDetailsList.get(0).getPurchaseInvoice() != null) {
						paymentDetails.setPaymentTypeMaster(paymentTypeMasterRepository.getCustomerPaymentType()); // Customer
																													// type
																													// set
					} else {
						paymentDetails.setPaymentTypeMaster(paymentTypeMasterRepository.getOthersrPaymentType()); // Other
																													// type
																													// set
					}
					if (bankPaymentDTO.getPaymentMode().getPaymentMode()
							.equalsIgnoreCase(PaymentMode.CASH.toString())) {
						paymentDetails.setPaymentMethod(
								paymentMethodRepository.getPaymentMethodByName(PaymentMode.CASH.toString()));
						log.info("Payment method CASH set successfully");
					} else {
						log.error("Payment method CASH set Failed");
					}
					paymentDetails.setVersion(0l);
					paymentDetails.setVoucher(voucher);
					// paymentDetails.setEntityBankBranch(entityBankBranch);
					voucherLog.setModifiedBy(userMaster);
					voucherLog.setModifiedDate(new Date());
					voucherLog.setUserMaster(userMaster);
					voucherLog.setStatus(VoucherStatus.PAID);
					voucherLog.setVoucher(voucher);
					paymentDetailsRepository.save(paymentDetails);
					log.info("CashCompletionService() :: paymentDetails save Successfully");
					baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
				} else if (voucherType.getName().equalsIgnoreCase(VoucherTypeDetails.Receipt.toString())) {
					voucher = voucherRepository.findOne(bankPaymentDTO.getId());
					/*
					 * sequenceConfig = sequenceConfigRepository
					 * .findBySequenceName(SequenceName.valueOf(SequenceName.
					 * FINANCE_ACCOUNT_BANK_RECEIPT.name())); sequenceNumber =
					 * sequenceConfig.getCurrentValue();
					 * sequenceConfig.setCurrentValue(sequenceConfig.getCurrentValue() + 1);
					 * sequenceConfigRepository.save(sequenceConfig);
					 */
					PaymentDetails paymentdetailsObj = paymentDetailsRepository
							.getPaymentDetailsByVoucherId(voucher.getId());
					payment = paymentdetailsObj.getPayment();
					voucherLog.setModifiedBy(userMaster);
					voucherLog.setModifiedDate(new Date());
					voucherLog.setUserMaster(userMaster);
					voucherLog.setVoucher(voucher);
					voucherLog.setStatus(VoucherStatus.RECEIVED);
					log.info("CashCompletionService() :: Voucher Log save Successfully for Receipt Type voucherID :"
							+ bankPaymentDTO.getId());
					baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
				} else {
					log.error("CashCompletionService() :: Invalid Voucher Type Details to save Data :"
							+ bankPaymentDTO.getId());
				}
				log.info("BankPaymentService() :: --> paymentFinanceService()  Called:::>>" + voucher.getName());
				log.info("<<::::Payment Id:::::>>" + payment.getId());
				baseDTO = paymentFinanceService.postingBasedOnVoucherName(voucher, null, payment.getId());
				log.info("<<<:::::::::::::baseDTO:::::::::::::::::::::>>>>>" + baseDTO);
				if (baseDTO.getStatusCode() != ErrorDescription.SUCCESS_RESPONSE.getCode()) {
					log.info("<<<<=========Account transaction Posting Failure Response=======>>>>");
					throw new LedgerPostingException();
				} else {
					sequenceConfig.setCurrentValue(sequenceConfig.getCurrentValue() + 1);
					sequenceConfigRepository.save(sequenceConfig);
					voucherLogRepository.save(voucherLog);
					log.info("CashCompletionService() :: Voucher Log save Successfully for Payment Type voucherID :"
							+ bankPaymentDTO.getId());
					log.info("<<<<=========Account transaction Posting Success Response=======>>>>");
				}
				log.info("BankPaymentService() :: --> paymentFinanceService()  Processed Completed Successfully");
			}

		} catch (LedgerPostingException ledgerexp) {
			log.error("CashCompletionService() ::LedgerPostingException ::: ", ledgerexp);
			if (payment != null && payment.getId() != null) {
				paymentDetailsRepository.deletePaymentDetailBYpaymentID(payment.getId());
				paymentRepository.deletePaymentBYID(payment.getId());
			}
			baseDTO.setStatusCode(ErrorDescription.getError(FinanceErrorCode.ACCOUNT_POSTING_ERROR).getErrorCode());
		} catch (Exception exp) {
			log.error("CashCompletionService() ::Exception Cause loadCreateBankPaymentDetails() : ", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}
	
	public BaseDTO getChequeBookListByBranchID(Long entityID, Long branchID) {
		log.info("<--CashCompletionService() .getChequeBookListByBranchID() Started-->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<ChequeBook> paymentDetails = chequeBookRepository.findChequeBookByBranchID(entityID, branchID);
			baseDTO.setResponseContent(paymentDetails);
			baseDTO.setTotalRecords(paymentDetails.size());
		    log.info("<--getChequeBookListByBranchID() fetch success-->");
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.error("Exception in getChequeBookListByBranchID() ",e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(baseDTO);
	}
	
	//second sub table Lazy list
	public BaseDTO getBankPaymentLazyList(BankPaymentDTO paginationDto) {
		log.info(" getallBankPayment_SecondLazy  called...");
		BaseDTO baseDTO = new BaseDTO();

		try{
		 	Integer total=0;
		 	Integer start = paginationDto.getFirst(),pageSize = paginationDto.getPagesize();
			start=start*pageSize;
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();
			
			String queryName="FINANCE_ACCOUNTS_CASH_COMPLETION_LIST";
			String queryNameCount="FINANCE_ACCOUNTS_CASH_COMPLETION_COUNT_QUERY";
			
			ApplicationQuery applicationQuery = applicationQueryRepository.findByQueryName(queryName);
			String query = applicationQuery.getQueryContent().trim();
			
			applicationQuery = applicationQueryRepository.findByQueryName(queryNameCount);
			String queryCount = applicationQuery.getQueryContent().trim();

			String statusType2 = VoucherStatus.PAID.toString();
			String statusType3 = VoucherStatus.RECEIVED.toString();
			String paymentMode = PaymentMode.CASH.toString();

			query+= " where vl.status in ('"+statusType2+"','"+statusType3+"') " + 
					" and upper(vm.payment_mode) ='"+paymentMode+"' and (sm.active_status=true or sm.active_status is null)";
			

			String mainQuery = query;
			
			if(paginationDto.getId()!=null) {
				mainQuery += " and t.id='"+paginationDto.getId()+"'";
			}
			if(paginationDto.getVoucherNumber()!=null) {
				mainQuery += " and ( upper(v.reference_number_prefix) like upper('%"+paginationDto.getVoucherNumber()+"%') or ";
				mainQuery += " cast(v.reference_number as varchar) like upper('%"+paginationDto.getVoucherNumber()+"%')) ";
			}
			if(paginationDto.getTransactionName()!=null) {
				mainQuery += " and upper(v.name) like upper('%"+paginationDto.getTransactionName()+"%')";
			}
			if(paginationDto.getTransactionType()!=null) {
				mainQuery += " and upper(vt.name) like upper('%"+paginationDto.getTransactionType()+"%')";
			}
			
			if(paginationDto.getVoucherDate()!=null) {
				Date createdDate=paginationDto.getVoucherDate();
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				mainQuery += " and v.created_date::date = date '"+dateFormat.format(createdDate)+"'";	
			}
			if(paginationDto.getNetAmount()!=null) {
				Double value =Double.parseDouble(paginationDto.getNetAmount().toString());
				Integer intValue = value.intValue();
				mainQuery += " and cast(v.net_amount as varchar) like '%"+intValue+"%' ";
			}
			 
			if(paginationDto.getAmontPaidDate()!=null) {
				Date createdDate=paginationDto.getAmontPaidDate();
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				mainQuery += " and pd.created_date::date = date '"+dateFormat.format(createdDate)+"'";	
			}
			if(paginationDto.getStatus()!=null) {
				mainQuery += " and upper(vl.status) like upper('%"+paginationDto.getStatus()+"%')";
			}
			if(paginationDto.getBankName()!=null) {
				mainQuery += " and upper(bankm.bank_name) like upper('%"+paginationDto.getBankName()+"%')";
			}
			 
			String countQuery=mainQuery.replace(queryCount, " select count(*) as count ");
			
			countData=jdbcTemplate.queryForList(countQuery);
			for(Map<String,Object> data:countData){
				if(data.get("count")!=null)
					total=Integer.parseInt(data.get("count").toString().replace(",", ""));
			}
			
			if(paginationDto.getSortField()==null)
				mainQuery+=" order by id desc limit "+pageSize+" offset "+start+";";
			
			if(paginationDto.getSortField()!=null && paginationDto.getSortOrder()!=null)
			{

				if(paginationDto.getSortField().equals("id") && paginationDto.getSortOrder().equals("ASCENDING"))
						mainQuery+=" order by id asc ";
				if(paginationDto.getSortField().equals("id") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by id desc ";
				
				if(paginationDto.getSortField().equals("voucherNumber") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by voucherNumber asc  ";
				if(paginationDto.getSortField().equals("voucherNumber") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by voucherNumber desc  ";
				
				if(paginationDto.getSortField().equals("transactionName") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by v.name asc  ";
				if(paginationDto.getSortField().equals("transactionName") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by v.name desc  ";
				
				if(paginationDto.getSortField().equals("voucherDate") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by voucherDate asc  ";
				if(paginationDto.getSortField().equals("voucherDate") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by voucherDate desc  ";
				
				if(paginationDto.getSortField().equals("netAmount") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by net_amount asc  ";
				if(paginationDto.getSortField().equals("netAmount") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by net_amount desc  ";
				
				if(paginationDto.getSortField().equals("amontPaidDate") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by pd.created_date asc  ";
				if(paginationDto.getSortField().equals("amontPaidDate") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by pd.created_date desc  ";
				
				if(paginationDto.getSortField().equals("bankNameDeposited") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by bankNameDeposited asc  ";
				if(paginationDto.getSortField().equals("bankNameDeposited") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by bankNameDeposited desc  ";
				
				if(paginationDto.getSortField().equals("status") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by status asc  ";
				if(paginationDto.getSortField().equals("status") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by status desc  ";
				
				if(paginationDto.getSortField().equals("bankName") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by bank_name asc  ";
				if(paginationDto.getSortField().equals("bankName") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by bank_name desc  ";
			
				mainQuery+=" limit "+pageSize+" offset "+start+";";
			}

			listofData = jdbcTemplate.queryForList(mainQuery);
			List<BankPaymentDTO> bankPaymentDTOList = new ArrayList<>();
			
			for(Map<String,Object> data:listofData){
				BankPaymentDTO dto = new BankPaymentDTO();

				if(data.get("id")!=null) {
					dto.setId(Long.parseLong(data.get("id").toString()));
				}
				if(data.get("voucherNumber")!=null) {
					dto.setVoucherNumber(data.get("voucherNumber").toString());
				}
				if(data.get("transactionName")!=null) {
					dto.setTransactionName(data.get("transactionName").toString());
				}
				if(data.get("voucherType")!=null) {
					dto.setTransactionType(data.get("voucherType").toString());
				}
				if(data.get("voucherDate")!=null) {
					dto.setVoucherDate((Date)data.get("voucherDate"));
				}else {
					dto.setVoucherDate(null);
				}
				if(data.get("net_amount")!=null) {
					dto.setNetAmount(Double.parseDouble(data.get("net_amount").toString()));
				} 
				if(data.get("amountPaidDate")!=null) {
					dto.setAmontPaidDate((Date)data.get("amountPaidDate"));
				}else {
					dto.setAmontPaidDate(null);
				}
				if(data.get("status")!=null) {
					dto.setStatus(data.get("status").toString());
				}
				if(data.get("supplierCodeName")!=null) {
					dto.setSupplierCodeName(data.get("supplierCodeName").toString());
				}
				if(data.get("supplierID")!=null) {
					dto.setSupplierID(Long.parseLong(data.get("supplierID").toString()));
				}
				
				if(data.get("paymentModeID")!=null) {
					dto.setPaymentMode(paymentModeRepository.findOne(Long.parseLong(data.get("paymentModeID").toString()))); 
					dto.setPaymentModeID(Long.parseLong(data.get("paymentModeID").toString()));
				}
				if(data.get("voucherTypeID")!=null) {
					dto.setVoucherType(voucherTypeRepository.findOne(Long.parseLong(data.get("voucherTypeID").toString()))); 
					dto.setVoucherTypeID(Long.parseLong(data.get("voucherTypeID").toString()));
				}
				if(data.get("bank_name")!=null) {
					dto.setBankMaster(bankMasterRepository.findOne(Long.parseLong(data.get("bankID").toString())));
					dto.setBankName(data.get("bank_name").toString());
				}

				if(data.get("bankBranchMasterID")!=null) {
					dto.setBankBranchMaster(bankBranchMasterRepository.findOne(Long.parseLong(data.get("bankBranchMasterID").toString())));
				}
				if(data.get("entityBankBranchID")!=null) {
					dto.setEntityBankBranch(entityBankBranchRepository.findOne(Long.parseLong(data.get("entityBankBranchID").toString())));
				}
				 
				dto.setReceiptFlag(false);
				bankPaymentDTOList.add(dto);
			} 
			
			BankPaymentDTO bankPaymentDTO=new BankPaymentDTO();

			bankPaymentDTO.setBankPaymentDTOList(bankPaymentDTOList);
			baseDTO.setResponseContent(bankPaymentDTO);
			baseDTO.setTotalRecords(total);
			
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		}catch(Exception exp){
			log.error("CashCompletionService() ::Exception Cause  lazy Search : " , exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO ;
	}
	

	public BaseDTO getBankPaymentView(Long voucherID, Long entityID) {
		log.info(" getallBankPayment_View  called...");
		BaseDTO baseDTO = new BaseDTO();

		try{
			BankPaymentDTO bankPaymentDTO = new BankPaymentDTO();
			PaymentDetails paymentDetails = paymentDetailsRepository.getPaymentDetailsByVoucherId(voucherID);
//			PaymentDetails paymentDetails = paymentDetailsList.get(0);
//			bankPaymentDTO.setChequeReferenceDate(paymentDetails.getDocumentDate());
//			bankPaymentDTO.setChequeReferenceNumber(paymentDetails.getBankReferenceNumber());
						 
			baseDTO.setResponseContent(bankPaymentDTO);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			
		}catch(Exception exp){
			log.error("CashCompletionService() ::Exception Cause  lazy Search : " , exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return responseWrapper.send(baseDTO);
	}
	
	//fetch and validate data for update
	public BaseDTO getBankPaymenteEDIT(Long voucherID, Long entityID) {
		log.info(" getallBankPayment_Edit  called...");
		BaseDTO baseDTO = new BaseDTO();

		try{
			BankPaymentDTO bankPaymentDTO = new BankPaymentDTO();
			PaymentDetails paymentDetails = paymentDetailsRepository.getPaymentDetailsByVoucherMaxID(voucherID,PaymentCategory.PAID_OUT.toString());
//			bankPaymentDTO.setChequeReferenceDate(paymentDetails.getDocumentDate());
//			bankPaymentDTO.setChequeReferenceNumber(paymentDetails.getBankReferenceNumber());
			
			AppConfig appConfig = appConfigRepository.findByKey("FINANCE_BANK_PAYMENT_CHEQUE_UDPATE_DURATION_HOURS");
			bankPaymentDTO.setEditFlag(true);
			bankPaymentDTO.setPaymentDetailsID(paymentDetails.getId());
			bankPaymentDTO.setPaymentID(paymentDetails.getPayment().getId());
			Long hoursAppConfig =Long.parseLong(appConfig.getAppValue());
			Date todatyDate = new Date();
			Date dateEffectiveDate = paymentDetails.getCreatedDate();
			long diff = todatyDate.getTime() - dateEffectiveDate.getTime();
			long diffHours = diff / (60 * 60 * 1000);
			if(hoursAppConfig <= diffHours) {
				bankPaymentDTO.setEditFlag(false);
			}
			 
			baseDTO.setResponseContent(bankPaymentDTO);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			
		}catch(Exception exp){
			log.error("CashCompletionService() ::Exception Cause getBankPaymenteEDIT(): " , exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return responseWrapper.send(baseDTO);
	}
	
	//update 
	public BaseDTO updateBankPayment(BankPaymentDTO dto) {
		log.info("CashCompletionService() :: updateBankPayment received ");
		BaseDTO baseDTO = new BaseDTO();

		try{	
			Payment payment = paymentRepository.findOne(dto.getPaymentID());
			UserMaster userMaster = userMasterRepository.findOne(loginService.getCurrentUser().getId());

			PaymentDetails paymentDetails = paymentDetailsRepository.findOne(dto.getPaymentDetailsID());
//			EntityBankBranch  entityBankBranch= entityBankBranchRepository.findOne(dto.getEntityBankBranch().getId());
			Payment paymentReturn = null;
			VoucherType voucherType = voucherTypeRepository.findOne(dto.getVoucherType().getId());
			if(voucherType.getName().equalsIgnoreCase(VoucherTypeDetails.Payment.toString())) {
				log.info("Processed Payment Mode:");
				EntityMaster loginUserEntity = entityMasterRepository.findByUserRegion(userMaster.getId());
				
				if (loginUserEntity == null) {
					log.info("Login User Info Not Available");
					throw new RestException(ErrorDescription.LOGIN_USER_INFO_NOT_AVAILABLE);
				}
				
				payment.setEntityMaster(loginUserEntity);
				
				payment.setModifiedDate(new Date());
				payment.setModifiedBy(userMaster);
				
				paymentReturn = paymentRepository.save(payment);
				log.info("CashCompletionService() :: Payment update Succesfully "+paymentReturn.getId());
				
			} else if(voucherType.getName().equalsIgnoreCase(VoucherTypeDetails.Receipt.toString())) {
				log.info("CashCompletionService() :: Processed Receipt Mode:");
			}
			

				if(voucherType.getName().equalsIgnoreCase(VoucherTypeDetails.Payment.toString())) {
					Voucher voucher = voucherRepository.findOne(dto.getId());
//					paymentDetails.setBankReferenceNumber(dto.getChequeReferenceNumber());
//					paymentDetails.setDocumentDate(dto.getChequeReferenceDate());
					
					paymentDetails.setPayment(paymentReturn);
					paymentDetails.setModifiedDate(new Date());
					paymentDetails.setModifiedBy(userMaster);
					
					paymentDetails.setPaymentCategory(PaymentCategory.PAID_OUT);
					List<VoucherDetails> voucherDetails = voucherDetailsRepository.findVoucherDetailsByVoucherId(voucher.getId());
					if(voucher.getSupplierType()!=null) {
						paymentDetails.setPaymentTypeMaster(paymentTypeMasterRepository.getSupplierPaymentType()); // Supplier type set
					} else if(voucherDetails.get(0).getPurchaseInvoice()!=null) {
						paymentDetails.setPaymentTypeMaster(paymentTypeMasterRepository.getCustomerPaymentType()); // Customer type set
					}else {
						paymentDetails.setPaymentTypeMaster(paymentTypeMasterRepository.getOthersrPaymentType()); // Other type set
					}
					
					 if(dto.getPaymentMode().getPaymentMode().equalsIgnoreCase(PaymentMode.CASH.toString())
							 ) {
						paymentDetails.setPaymentMethod(paymentMethodRepository.getPaymentMethodByName(PaymentMode.CASH.toString()));
					} 
					log.info("paymentMethod :"+paymentDetails.getPayment());
					 
					paymentDetails.setVersion(0l);
					paymentDetails.setVoucher(voucher);
//					paymentDetails.setEntityBankBranch(entityBankBranch);
					

					 
					paymentDetailsRepository.save(paymentDetails);
					log.info("CashCompletionService() :: VoucherDetails update Successfully");
					
					baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
					
				} else if(voucherType.getName().equalsIgnoreCase(VoucherTypeDetails.Receipt.toString())) {
					
					baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
				}else {
					log.error("CashCompletionService() :: Invalid Voucher Type Details to save Data :"+dto.getId());
				}
				

		}catch(Exception exp){
			log.error("CashCompletionService() ::Exception Cause loadCreateBankPaymentDetails() : " , exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO ;
	}
}

