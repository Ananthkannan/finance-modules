package in.gov.cooptex.finance.service;

import in.gov.cooptex.common.repository.CashMovementRepository;
import in.gov.cooptex.common.service.EntityMasterService;
import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.core.accounts.model.*;
import in.gov.cooptex.core.accounts.repository.EntityBankBranchRepository;
import in.gov.cooptex.core.accounts.repository.PaymentDetailsRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.enums.ApprovalStatus;
import in.gov.cooptex.core.finance.service.OperationService;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.*;
import in.gov.cooptex.core.utilities.AmountMovementType;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.FinanceErrorCode;
import in.gov.cooptex.exceptions.LedgerPostingException;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.BankToBankResponseDTO;
import in.gov.cooptex.finance.BankToBankViewResponseDTO;
import in.gov.cooptex.finance.BanktoBankAddDTO;
import in.gov.cooptex.finance.BanktoBankAddDetailsDTO;
import in.gov.cooptex.finance.CashToBankAddRequestDTO;
import in.gov.cooptex.finance.CashtoBankChallanDTO;
import in.gov.cooptex.finance.CashtoBankResponseDTO;
import in.gov.cooptex.finance.repository.AmountTransferLogRepository;
import in.gov.cooptex.finance.repository.AmountTransferNoteRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Log4j2
public class AmountTransferDetailsService {
    @Autowired
    UserMasterRepository userMasterRepository;
    @Autowired
    EntityMasterService entityMasterService;
    @Autowired
    LoginService loginService;
    @Autowired
    AmountTransferDetailsRepository amountTransferDetailsRepository;
    @Autowired
    AmountTransferRepository amountTransferRepository;

    @Autowired
    EntityMasterRepository entityMasterRepository;

    @Autowired
    CashMovementRepository cashMovementRepository;

    @Autowired
    EmployeeMasterRepository  employeeMasterRepository;

    @Autowired
    EntityBankBranchRepository entityBankBranchRepository;
    @Autowired
    PaymentDetailsRepository paymentDetailsRepository;

    @Autowired
    AmountTransferNoteRepository amountTransferNoteRepository;
    @Autowired
    AmountTransferLogRepository amountTransferLogRepository;

@Transactional
public BaseDTO create(CashToBankAddRequestDTO data)
{
    log.info("cash to bank add method is started"+data);
    BaseDTO baseDTO=new BaseDTO();
    AmountTransferLog response=null;

    AmountTransfer amountTransfer=new AmountTransfer();
    try {
        log.info("execution is started");
        UserMaster user = loginService.getCurrentUser();
        Validate.notNull(data, ErrorDescription.PURCHASE_INVOICE_EMPTY );
        Validate.notNull(data, ErrorDescription.PURCHASE_INVOICE_EMPTY );
        Validate.notNull(data, ErrorDescription.PURCHASE_INVOICE_EMPTY );
        Validate.notNull(data, ErrorDescription.PURCHASE_INVOICE_EMPTY );
        Validate.notNull(data, ErrorDescription.PURCHASE_INVOICE_EMPTY );
        Validate.notNull(data, ErrorDescription.PURCHASE_INVOICE_EMPTY );


        BaseDTO basedto = entityMasterService.getEntityInfoByLoggedInUser(user.getId());
        EntityMaster entity = (EntityMaster) basedto.getResponseContent();


        if (data!=null)
        {
            if(entity!=null) {
                data.setEntityId(entity.getId());
            }
            AmountTransferDetails amountTransferDetails=new AmountTransferDetails();

            EntityMaster em = entityMasterRepository.findById(data.getEntityId());
            CashMovement cm = cashMovementRepository.findOne(data.getCashMovementId());
            EmployeeMaster employeeMaster = employeeMasterRepository.findById(data.getTransferBy());


            if(em==null  || cm==null||employeeMaster==null)
            {
                throw  new RestException();
            }

            amountTransfer.setEntityMaster(em);
            amountTransfer.setCashMovement(cm);
            amountTransfer.setTransferedBy(employeeMaster);
            amountTransfer.setRemarks(data.getRemarks());
            amountTransfer.setMovementType("cashtobank");
            amountTransfer.setTotalAmountTransfered(data.getTotalAmountTransferred());

            log.info("AmountTransfer "+amountTransfer);
            amountTransfer.setOtherExpenseAmount(data.getOtherExpenseAmount());

            amountTransfer = amountTransferRepository.save(amountTransfer);
            log.info("AmountTransfer ++ "+amountTransfer);
        /*    for (AddChallanInfoDTO chinf:data.getChallanaInformation()) {
                EntityBankBranch ebb = entityBankBranchRepository.findOne(chinf.getBranchId());

                amountTransferDetails.setAmountTransfer(amountTransfer);
                amountTransferDetails.setToBranch(ebb);
                amountTransferDetails.setChallanNumber(chinf.getChallanNumber());
                amountTransferDetails.setChallanDate(chinf.getChallanDate());
                amountTransferDetails.setChallanAmount(chinf.getChallanAmount());
                amountTransferDetails.setVersion(data.getVersion());
                log.info("AmountTransferDetails " + amountTransferDetails);
               AmountTransferDetails amountTransferDet = amountTransferDetailsRepository.save(amountTransferDetails);
                log.info("AmountTransferDetails ++ " + amountTransferDet);
            }*/
                UserMaster userMaster = userMasterRepository.findOne(data.getForwardTo());
                if (data.getNote()!=null) {
                    AmountTransferNote note = new AmountTransferNote();
                    note.setAmountTransfer(amountTransfer);
                    note.setNote(data.getNote());
                    note.setFinalApproval(data.getFinalApproval());
                    note.setForwardTo(userMaster);
                    note = amountTransferNoteRepository.save(note);
                }
            AmountTransferLog amountTransferLog=new AmountTransferLog();
            amountTransferLog.setAmountTransfer(amountTransfer);
            amountTransferLog.setStage("submitted");
            if (data.getRemarks()!=null) {
                amountTransferLog.setRemarks(data.getRemarks());
            }
            if(data.getVersion()!=null) {
                amountTransferLog.setVersion(data.getVersion());
            }
            amountTransferLog=amountTransferLogRepository.save(amountTransferLog);
            response=amountTransferLog;
            log.info("log  ++ "+response);

        }
        log.info("cash to bank add method ended "+response);
        baseDTO.setResponseContent(response);
        baseDTO.setTotalRecords(1);
        baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
        return  baseDTO;
    }
    catch (Exception ex)
    {
        log.info("Exception ex"+ex);
        ex.printStackTrace();
        baseDTO.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
        return  baseDTO;
    }
}
    @Transactional
    public BaseDTO edit(List<AmountTransferDetails> data)
    {
        BaseDTO baseDTO=new BaseDTO();
        try {

            for (AmountTransferDetails atd : data) {

                AmountTransferDetails obj = amountTransferDetailsRepository.findOne(atd.getId());
                obj.getAmountTransfer().setTotalAmountTransfered(atd.getAmountTransfer().getTotalAmountTransfered());
                obj.getAmountTransfer().setMovementType(atd.getAmountTransfer().getMovementType());
                obj.getAmountTransfer().setRemarks(atd.getAmountTransfer().getRemarks());
                obj.getAmountTransfer().setOtherExpenseAmount(atd.getAmountTransfer().getOtherExpenseAmount());

                EntityBankBranch entityBankBranch = entityBankBranchRepository.findOne(atd.getToBranch().getId());

                if(entityBankBranch==null){
                    throw new Exception("EntityBankBranch not found for id: "+atd.getToBranch().getId());
                }

                obj.setToBranch(entityBankBranch);
                obj.setChallanAmount(atd.getChallanAmount());
                obj.setChallanDate(atd.getChallanDate());
                obj.setChallanNumber(atd.getChallanNumber());
                amountTransferDetailsRepository.save(obj);
                baseDTO.setResponseContent(atd);
                baseDTO.setTotalRecords(1);
            }
            baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
            return  baseDTO;
        }
        catch (Exception ex)
        {
            log.error("edit in cash to bank exception : "+ex);
            baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
            return  baseDTO;
        }
    }
    
    @Autowired
    OperationService operationService;
    
   @Transactional
    public BaseDTO addBankToBank(BanktoBankAddDTO data)
	{
		BaseDTO baseDTO = new BaseDTO();
		List<AmountTransferLog> result = new ArrayList<>();
		AmountTransfer amountTransfer = new AmountTransfer();
		try {
			log.info("<<<<:::::::::::::execution started is started:::::::::::::::::>>>>");
//			UserMaster user = loginService.getCurrentUser();
//			BaseDTO basedto = entityMasterService.getEntityInfoByLoggedInUser(user.getId());
//			EntityMaster entity = (EntityMaster) basedto.getResponseContent();
//			if (entity != null) {
//				data.setEntityId(entity.getId());
//			}
			if (data != null) {
				log.info("<<<<<::::::data not null::::::::::>>>>>");
				EntityMaster em = entityMasterRepository.findById(data.getEntityId());
				EmployeeMaster employeeMaster = employeeMasterRepository.findById(data.getTransferredBy());
				EntityBankBranch toBranch = entityBankBranchRepository.findOne(data.getToAccountId());

				EntityBankBranch fromBranch = entityBankBranchRepository.findOne(data.getFromAccountId());
				log.info("<<<<<::::::data ID ::::::::::>>>>>" + data.getId());
				if (data.getId() != null && data.getId() > 0) {
					amountTransfer = amountTransferRepository.findOne(data.getId());
				}
				amountTransfer.setEntityMaster(em);
				amountTransfer.setTransferedBy(employeeMaster);

				amountTransfer.setMovementType(AmountMovementType.BANK_TO_BANK);
				amountTransfer.setTotalAmountTransfered(data.getTotalAmountTransfered());
				amountTransfer.setRemarks(data.getRemarks());
				amountTransfer.setVersion(data.getVersion());

				amountTransfer = amountTransferRepository.save(amountTransfer);

				if (data.getChallanaDetails().size() > 0 && data.getChallanaDetails() != null) {
					for (BanktoBankAddDetailsDTO btobadddetails : data.getChallanaDetails()) {

						AmountTransferDetails amountTransferDetails = new AmountTransferDetails();
						amountTransferDetails.setId(btobadddetails.getId());
						amountTransferDetails.setAmountTransfer(amountTransfer);
						amountTransferDetails.setToBranch(toBranch);
						amountTransferDetails.setFromBranch(fromBranch);
						amountTransferDetails.setChallanNumber(btobadddetails.getChallanNumber());
						amountTransferDetails.setChallanDate(btobadddetails.getChallanDate());
						amountTransferDetails.setChallanAmount(btobadddetails.getChallanAmount());
						amountTransferDetails.setBankCharge(btobadddetails.getBankCharge());
						amountTransferDetailsRepository.save(amountTransferDetails);
						// baseDTO.setResponseContent(amountTransferDetails);
					}
				}

				if (data.getChallanaDetailsRemove().size() > 0 && data.getChallanaDetailsRemove() != null) {
					for (BanktoBankAddDetailsDTO remove : data.getChallanaDetailsRemove()) {
						amountTransferDetailsRepository.delete(remove.getId());
					}
				}
				
				
				if (data.getNote() != null && !data.getSikpapproval()) {
					AmountTransferNote note = new AmountTransferNote(); 
					UserMaster userMaster = userMasterRepository.findOne(data.getForwardTo());
					note.setAmountTransfer(amountTransfer);
					note.setNote(data.getNote());
					note.setFinalApproval(data.getForwardFor());
					note.setForwardTo(userMaster);
					note = amountTransferNoteRepository.save(note);
					log.info("note  ++ " + note);
				}
				if(!data.getSikpapproval()) {
					AmountTransferLog amountTransferLog = new AmountTransferLog();
					amountTransferLog.setAmountTransfer(amountTransfer);
					amountTransferLog.setStage(AmountMovementType.SUBMITTED);
					amountTransferLog.setRemarks(data.getRemarks());
					amountTransferLog = amountTransferLogRepository.save(amountTransferLog);
				}else {
				AmountTransferLog amountTransferLog = new AmountTransferLog();
				amountTransferLog.setAmountTransfer(amountTransfer);
				amountTransferLog.setStage(AmountMovementType.FINAL_APPROVED);
				amountTransferLog.setRemarks(data.getRemarks());
				amountTransferLog = amountTransferLogRepository.save(amountTransferLog);
				}
				/*if (amountTransferLog.getStage().equals(AmountMovementType.FINAL_APPROVED)) {
					log.info("::::::::::amountTransfer:::::::::::" + amountTransfer);
					log.info("::::::amountTransferLog.getStage():" + amountTransferLog.getStage());
					baseDTO = operationService.amountTransferLedgerPosting(amountTransfer,
							amountTransferLog.getStage());
					if (baseDTO != null && baseDTO.getStatusCode() == ErrorDescription.SUCCESS_RESPONSE.getCode()) {
						amountTransferLog = amountTransferLogRepository.save(amountTransferLog);
					} else {
						baseDTO.setStatusCode(
								ErrorDescription.getError(FinanceErrorCode.ACCOUNT_POSTING_ERROR).getErrorCode());
						amountTransferDetailsRepository.deletebyamountTransferId(amountTransfer.getId());
						amountTransferRepository.delete(amountTransfer.getId());
					}
				}
				/*
				 * result.add(amountTransferLog); baseDTO.setResponseContents(result);
				 * baseDTO.setTotalRecords(1);
				 */

			} else {
				log.info(
						"<<<<<<<<<:::::::addBankToBank parameter value BanktoBankAddDTO data Null::::::::::::>>>>>>>>>");
			}
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			return baseDTO;
		} catch (NullPointerException e) {
			log.info("<--Null PointerException exception :- -->" + e);
			e.printStackTrace();
		} catch (LedgerPostingException le) {
			log.info("<--LedgerPostingException :- -->" + le);
			if (amountTransfer != null && amountTransfer.getId() != 0) {
				amountTransferDetailsRepository.deletebyamountTransferId(amountTransfer.getId());
				amountTransferRepository.delete(amountTransfer.getId());
			}
			baseDTO.setStatusCode(
					ErrorDescription.getError(FinanceErrorCode.ACCOUNT_POSTING_ERROR).getErrorCode());
			return baseDTO;
			
		} catch (Exception ex) {
			log.info("<--Amont Transfer Details Service/ exception :- -->" + ex);
			ex.printStackTrace();
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			return baseDTO;
		}
		return baseDTO;
	}
    
	public BaseDTO getAllEmployee() {
		log.info("EmployeeService.getAllEmployee() Started ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<EmployeeMaster> employeeList = employeeMasterRepository.getAllEmployee();
			if (employeeList != null && employeeList.size()>0) {
				baseDTO.setResponseContent(employeeList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				log.info("EmployeeService.getAllEmployee() Completed");
			}
		} catch (Exception exception) {
			log.error("Exception occurred in EmployeeService.getAllEmployee() -:", exception);
			baseDTO.setStatusCode(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return baseDTO;
	}
	
	
	public BaseDTO updateAmountTransferDetails(BankToBankViewResponseDTO data)
	{
		BaseDTO responce = new  BaseDTO();
		try {
			
			AmountTransfer amountTransfer = amountTransferRepository.findOne(data.getTransferId());
			amountTransfer.setTotalAmountTransfered(data.getTotalBankTransfer());
			amountTransferRepository.save(amountTransfer);
			Long toEntityBranchId=0L;
			Long fromEntityBranchId=0L;
			 
			

			List<Long> removeId = new ArrayList<>();
			for(CashtoBankChallanDTO ctb:data.getAmountTransferDetails())
			{
				if(ctb.getTransferDetailsId()!=null)
				{
					removeId.add(ctb.getTransferDetailsId());
				}
			}
			
			List<AmountTransferDetails> amountTransferDetails=amountTransferDetailsRepository.getAmountTransferList(data.getTransferId());
			
			fromEntityBranchId=amountTransferDetails.get(0).getFromBranch().getId();
			toEntityBranchId = amountTransferDetails.get(0).getToBranch().getId();
			List<AmountTransferDetails> atdetails=new ArrayList<>();
			removeId.add(0L);
			if(removeId.size()>0)
			{
			  atdetails = amountTransferDetailsRepository.getAmountTransferDetails(removeId,data.getTransferId());
			}
			
			for(AmountTransferDetails obj:atdetails)
			{
				
				amountTransferDetailsRepository.delete(obj.getId());
			}
			
			
			
			EntityBankBranch toBranch = entityBankBranchRepository.findOne(toEntityBranchId);

			EntityBankBranch fromBranch = entityBankBranchRepository.findOne(fromEntityBranchId);			
			
			for(CashtoBankChallanDTO ctb:data.getAmountTransferDetails())
			{
				
				if(ctb.getTransferDetailsId()==null)
				{
					 AmountTransferDetails atd  = new AmountTransferDetails();
					 atd.setAmountTransfer(amountTransfer);
					 atd.setBankCharge(ctb.getBankCharge());
					 atd.setChallanAmount(ctb.getChallanAmount());
					 atd.setChallanDate(ctb.getChallanDate());
					 atd.setChallanNumber(ctb.getChallanNumber());
					 atd.setFromBranch(fromBranch);
					 atd.setToBranch(toBranch);
					 atd.setCreditedDate(ctb.getCreditedDate());
					 amountTransferDetailsRepository.save(atd);
				} else {
					AmountTransferDetails amountTransferDetailsObj = amountTransferDetailsRepository.findOne(ctb.getTransferDetailsId());
					if(amountTransferDetailsObj != null) {
						amountTransferDetailsObj.setCreditedDate(ctb.getCreditedDate());
						amountTransferDetailsRepository.save(amountTransferDetailsObj);
					}
				}
			}
			responce.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			
		} catch (Exception e) {
			log.error("Exception occurred in EmployeeService.updateAmountTransferDetails() -:", e);
			responce.setStatusCode(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		
		return responce;
	}

}
