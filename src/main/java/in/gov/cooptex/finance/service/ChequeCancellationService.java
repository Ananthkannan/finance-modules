/**
 * 
 */
package in.gov.cooptex.finance.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.core.accounts.model.BankBranchMaster;
import in.gov.cooptex.core.accounts.model.ChequeBook;
import in.gov.cooptex.core.accounts.model.EntityBankBranch;
import in.gov.cooptex.core.accounts.repository.BankBranchMasterRepository;
import in.gov.cooptex.core.accounts.repository.ChequeCancellationRepository;
import in.gov.cooptex.core.accounts.repository.EntityBankBranchRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.extern.log4j.Log4j2;

/**
 * @author user
 *
 */

@Log4j2
@Service
public class ChequeCancellationService {

	@Autowired
	ChequeCancellationRepository chequeCancellationRepository;

	@Autowired
	BankBranchMasterRepository bankBranchMasterRepository;

	@Autowired
	EntityBankBranchRepository entityBankBranchRepository;
	@Autowired
	EntityMasterRepository entityMasterRepository;
	@Autowired
	LoginService loginService;

	public BaseDTO bankBranchNameList() {
		log.info("<-Inside SERVICE starts bankBranchNameList method->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			EntityMaster entityMaster = entityMasterRepository
					.getWorkLocationByUserId(loginService.getCurrentUser().getId());
			List<BankBranchMaster> bankBranchList = bankBranchMasterRepository.getbranchByEntityId(entityMaster.getId());
			if (bankBranchList != null) {
				baseDTO.setResponseContents(bankBranchList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				log.info("Ends ChequeCancellationService.bankBranchNameList()");
			}
		} catch (Exception exception) {
			log.error("Exception occurred in ChequeCancellationService.bankBranchNameList():", exception);
			baseDTO.setStatusCode(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO accountNumberList(Long bankBranchMasterId) {

		log.info("<-Inside SERVICE starts accountNumberList method->");
		BaseDTO baseDTO = new BaseDTO();
		try {
        	EntityMaster entityMaster = entityMasterRepository
					.getWorkLocationByUserId(loginService.getCurrentUser().getId());
			List<EntityBankBranch> accountNumberList = entityBankBranchRepository
					.getByBranchMasterIdAndBank(bankBranchMasterId,entityMaster.getId());
			if (accountNumberList != null) {
				baseDTO.setResponseContents(accountNumberList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				log.info("Ends ChequeCancellationService.accountNumberList()");
			}
		} catch (Exception exception) {
			log.error("Exception occurred in ChequeCancellationService.accountNumberList():", exception);
			baseDTO.setStatusCode(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO chequeBookNumberList(Long entityBankBranchId) {

		log.info("<-Inside SERVICE starts chequeBookNumberList method->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<String> chequeBookNumberList = chequeCancellationRepository
					.findByBankBranchMasterId(entityBankBranchId);
			if (chequeBookNumberList != null) {
				baseDTO.setResponseContents(chequeBookNumberList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				log.info("Ends ChequeCancellationService.chequeBookNumberList()");
			}
		} catch (Exception exception) {
			log.error("Exception occurred in ChequeCancellationService.chequeBookNumberList():", exception);
			baseDTO.setStatusCode(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO chequeBookDetails(String chequeBookNumber) {

		log.info("<-Inside SERVICE starts chequeBookDetails method->");
		BaseDTO baseDTO = new BaseDTO();

		try {
			List<ChequeBook> checkBookDetails = new ArrayList<ChequeBook>();
			List<Object[]> chequeDetailsList = chequeCancellationRepository.fetchChequeDetails(chequeBookNumber);
			if (chequeDetailsList != null && chequeDetailsList.size() > 0) {

				Object[] objectArray = null;
				Object[] innerObjectArray = null;
				objectArray = chequeDetailsList.toArray();
				for (int i = 0; i < objectArray.length; i++) {
					ChequeBook chequeBook = new ChequeBook();
					innerObjectArray = (Object[]) objectArray[i];

					chequeBook.setChequeNumber(
							Long.parseLong((innerObjectArray[0] != null ? innerObjectArray[0] : "").toString()));
				
					chequeBook.setAmountPaid(
							Double.parseDouble((innerObjectArray[1] != null ? innerObjectArray[1] : 0).toString()));

					chequeBook.setStatus((innerObjectArray[3] != null ? innerObjectArray[3] : "").toString());

					String paymentDate = (innerObjectArray[2] != null ? innerObjectArray[2] : "").toString();

					String outputDate = null;
					if (paymentDate != null && !paymentDate.equals("")) {
						SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
						SimpleDateFormat outputDateFormat = new SimpleDateFormat("dd-MM-yyyy");

						Date pymntDate = dateFormat.parse(paymentDate);
						outputDate = outputDateFormat.format(pymntDate);
						chequeBook.setPaymentDate(outputDate);
					}
					
					chequeBook.setId(
							Long.parseLong((innerObjectArray[4] != null ? innerObjectArray[4] : "").toString()));
					
					checkBookDetails.add(chequeBook);
				}
			}
			baseDTO.setResponseContents(checkBookDetails);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("Ends ChequeCancellationService.chequeBookDetails()");
		} catch (Exception exception) {
			log.error("Exception occurred in ChequeCancellationService.chequeBookDetails():", exception);
			baseDTO.setStatusCode(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO updateChequeBook(List<ChequeBook> selectedChequeBook) {
		
		log.info("<-Inside SERVICE starts updateChequeBook method->");
		BaseDTO baseDTO = new BaseDTO();
		try {
		if(selectedChequeBook != null && selectedChequeBook.size() > 0) {
			List<ChequeBook> chequeBookList = new ArrayList<ChequeBook>();
			
			for(ChequeBook chequeBook : selectedChequeBook) {
				ChequeBook updateChequeBook = new ChequeBook();
				updateChequeBook = chequeCancellationRepository.findOne(chequeBook.getId());
				updateChequeBook.setStatus("cancelled");
				updateChequeBook.setModifiedDate(new Date());
			//	chequeCancellationRepository.save(updateChequeBook);
				chequeBookList.add(updateChequeBook);
			}
			chequeCancellationRepository.save(chequeBookList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("Ends ChequeCancellationService.updateChequeBook()");
		}
	}catch (Exception exception) {
		log.error("Error occurred in ChequeCancellationService.updateChequeBook():" + exception);
		baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
	}
		return baseDTO;
	}
	

}
