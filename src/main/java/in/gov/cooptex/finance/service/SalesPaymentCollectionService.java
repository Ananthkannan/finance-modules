package in.gov.cooptex.finance.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.dto.SalesPaymentCollectionDTO;
import in.gov.cooptex.core.accounts.dto.SalesPaymentCollectionResponseDTO;
import in.gov.cooptex.core.accounts.enums.SalesInvoiceDetails;
import in.gov.cooptex.core.accounts.enums.VoucherStatus;
import in.gov.cooptex.core.accounts.enums.VoucherTypeDetails;
import in.gov.cooptex.core.accounts.model.GlAccount;
import in.gov.cooptex.core.accounts.model.Payment;
import in.gov.cooptex.core.accounts.model.PaymentDetails;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.VoucherDetails;
import in.gov.cooptex.core.accounts.model.VoucherLog;
import in.gov.cooptex.core.accounts.model.VoucherType;
import in.gov.cooptex.core.accounts.repository.GlAccountRepository;
import in.gov.cooptex.core.accounts.repository.PaymentDetailsRepository;
import in.gov.cooptex.core.accounts.repository.PaymentMethodRepository;
import in.gov.cooptex.core.accounts.repository.PaymentRepository;
import in.gov.cooptex.core.accounts.repository.PaymentTypeMasterRepository;
import in.gov.cooptex.core.accounts.repository.SalesInvoiceAdjustmentRepository;
import in.gov.cooptex.core.accounts.repository.VoucherDetailsRepository;
import in.gov.cooptex.core.accounts.repository.VoucherRepository;
import in.gov.cooptex.core.accounts.repository.VoucherTypeRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.PaymentCategory;
import in.gov.cooptex.core.enums.SequenceName;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.CustomerMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.PaymentMethod;
import in.gov.cooptex.core.model.SequenceConfig;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.CustomerMasterRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.PaymentModeRepository;
import in.gov.cooptex.core.repository.SalesInvoiceRepository;
import in.gov.cooptex.core.repository.SalesOrderRepository;
import in.gov.cooptex.core.repository.SequenceConfigRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.ui.EntityType;
import in.gov.cooptex.core.util.JdbcUtil;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.operation.model.SalesInvoice;
import in.gov.cooptex.operation.model.SalesOrder;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class SalesPaymentCollectionService {

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	LoginService loginService;

	@Autowired
	SequenceConfigRepository sequenceConfigRepository;

	@Autowired
	VoucherRepository voucherRepository;

	@Autowired
	PaymentTypeMasterRepository paymentTypeMasterRepository;

	@Autowired
	PaymentModeRepository paymentModeRepository;

	@Autowired
	UserMasterRepository userMasterRepository;

	@Autowired
	PaymentRepository paymentRepository;

	@Autowired
	VoucherTypeRepository voucherTypeRepository;

	@Autowired
	SalesInvoiceRepository salesInvoiceRepository;

	@Autowired
	PaymentMethodRepository paymentMethodRepository;

	@Autowired
	PaymentDetailsRepository paymentDetailsRepository;

	@Autowired
	VoucherDetailsRepository voucherDetailsRepository;

	@Autowired
	SalesInvoiceAdjustmentRepository salesInvoiceAdjustmentRepository;

	@Autowired
	SalesOrderRepository salesOrderRepository;

	@Autowired
	EntityManager entityManager;

	@Autowired
	ResponseWrapper responseWrapper;

	@Autowired
	CustomerMasterRepository customerMasterRepository;

	@Autowired
	GlAccountRepository glAccountRepository;
	
	private static final String CREDIT_COLLECTION_HO_GL_ACCOUNT_CODE = "225";
	private static final String CREDIT_COLLECTION_RO_GL_ACCOUNT_CODE = "224";
	
	public BaseDTO getSalesInvoiceDetails(SalesPaymentCollectionDTO salesPaymentCollectionDTO) {
		log.info(" SalesPaymentCollectionService getSalesInvoiceDetails salesPaymentCollectionDTO : "
				+ salesPaymentCollectionDTO);
		BaseDTO baseDTO = new BaseDTO();
		try {

			UserMaster loginUser = loginService.getCurrentUser();
			Long loginUserId = loginUser != null ? loginUser.getId() : null;

			EntityMaster entityMaster = entityMasterRepository.getEntityInfoByLoggedInUser(loginUserId);
			Long loginEntityId = entityMaster != null ? entityMaster.getId() : null;

			String queryName = null;
			
			if (salesPaymentCollectionDTO.getAction().equals("ADD")) {
				queryName = SalesInvoiceDetails.SALES_PAYMENT_COLLECTION_INVOICE_DETAILS.toString();
			} else {
				queryName = SalesInvoiceDetails.VIEW_SALES_PAYMENT_COLLECTION_INVOICE_DETAILS.toString();
			}
			
			ApplicationQuery applicationQuery = applicationQueryRepository
					.findByQueryName(queryName);
			if (applicationQuery == null || applicationQuery.getId() == null) {
				log.info("Application Query not found for query name : " + applicationQuery);
				baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
				return baseDTO;
			}
			String queryForSalesInvoiceDetailsList = applicationQuery.getQueryContent().trim();
			if (salesPaymentCollectionDTO.getAction().equals("ADD")) {
				queryForSalesInvoiceDetailsList = queryForSalesInvoiceDetailsList.replace(":id", "cm.id");
				queryForSalesInvoiceDetailsList = queryForSalesInvoiceDetailsList.replace(":inputid",
						salesPaymentCollectionDTO.getCustomerId().toString());
			} else {
				queryForSalesInvoiceDetailsList = queryForSalesInvoiceDetailsList.replace(":voucherId",
						salesPaymentCollectionDTO.getId().toString());
			}

			queryForSalesInvoiceDetailsList = StringUtils.replace(queryForSalesInvoiceDetailsList, ":loginEntityId",
					String.valueOf(loginEntityId));
			log.info("Query Content For queryForSalesInvoiceDetailsList : " + queryForSalesInvoiceDetailsList);

			List<SalesPaymentCollectionResponseDTO> salesInvoiceDetailsList = jdbcTemplate.query(
					queryForSalesInvoiceDetailsList, new BeanPropertyRowMapper<SalesPaymentCollectionResponseDTO>(
							SalesPaymentCollectionResponseDTO.class));
			baseDTO.setResponseContent(salesInvoiceDetailsList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			baseDTO.setStatusCode(restException.getStatusCode());
			log.error("RestException in SalesPaymentCollectionService ", restException);
		} catch (Exception exception) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.error("Exception in SalesPaymentCollectionService ", exception);
		}
		return baseDTO;
	}

	public BaseDTO getCustomerAdvanceCollectionDetails(SalesPaymentCollectionDTO salesPaymentCollectionDTO) {
		log.info(" SalesPaymentCollectionService getCustomerAdvanceCollectionDetails salesPaymentCollectionDTO : "
				+ salesPaymentCollectionDTO);
		BaseDTO baseDTO = new BaseDTO();
		try {
			ApplicationQuery applicationQuery = applicationQueryRepository
					.findByQueryName(SalesInvoiceDetails.SALES_PAYMENT_ADVANCE_COLLECTION_DETAILS.toString());
			if (applicationQuery == null || applicationQuery.getId() == null) {
				log.info("Application Query not found for query name : " + applicationQuery);
				baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
				return baseDTO;
			}
			String queryForAdvanceCollectionDetailsList = applicationQuery.getQueryContent().trim();
			queryForAdvanceCollectionDetailsList = queryForAdvanceCollectionDetailsList.replace(":advancetype",
					"'" + SalesInvoiceDetails.sales_advance.toString() + "'");
			queryForAdvanceCollectionDetailsList = queryForAdvanceCollectionDetailsList.replace(":customerid",
					salesPaymentCollectionDTO.getCustomerId().toString());
			log.info(
					"Query Content For queryForAdvanceCollectionDetailsList : " + queryForAdvanceCollectionDetailsList);

			List<SalesPaymentCollectionResponseDTO> advanceCollectionDetailsList = jdbcTemplate.query(
					queryForAdvanceCollectionDetailsList, new BeanPropertyRowMapper<SalesPaymentCollectionResponseDTO>(
							SalesPaymentCollectionResponseDTO.class));
			baseDTO.setResponseContent(advanceCollectionDetailsList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			baseDTO.setStatusCode(restException.getStatusCode());
			log.error("RestException in SalesPaymentCollectionService ", restException);
		} catch (Exception exception) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.error("Exception in SalesPaymentCollectionService ", exception);
		}
		return baseDTO;
	}

	public BaseDTO getVoucherDetails(SalesPaymentCollectionDTO salesPaymentCollectionDTO) {
		log.info(" SalesPaymentCollectionService getVoucherDetails salesPaymentCollectionDTO : "
				+ salesPaymentCollectionDTO);
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<Map<String, Object>> queryData = new ArrayList<Map<String, Object>>();
			ApplicationQuery applicationQuery = applicationQueryRepository
					.findByQueryName(SalesInvoiceDetails.VIEW_SALES_PAYMENT_COLLECTION.toString());
			if (applicationQuery == null || applicationQuery.getId() == null) {
				log.info("Application Query not found for query name : " + applicationQuery);
				baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
				return baseDTO;
			}
			String query = applicationQuery.getQueryContent().trim();
			// query = query.replace(":vouchertypeid", "'" + 3 + "'");
			query = query.replace(":voucherid", salesPaymentCollectionDTO.getId().toString());
			log.info("Query Content For Sales Payment Collection View query : " + query);
			queryData = jdbcTemplate.queryForList(query);
			if (!queryData.isEmpty()) {
				for (Map<String, Object> queryDataMap : queryData) {
					if (queryDataMap.get("customerId") != null) {
						salesPaymentCollectionDTO
								.setCustomerId(Long.valueOf(queryDataMap.get("customerId").toString()));
					}
					if (queryDataMap.get("customer_name") != null) {
						salesPaymentCollectionDTO.setCustomerName(queryDataMap.get("customer_name").toString());
					}
					if (queryDataMap.get("payment_mode") != null) {
						salesPaymentCollectionDTO.setPaymentMode(queryDataMap.get("payment_mode").toString());
					}
					if (queryDataMap.get("customer_type") != null) {
						salesPaymentCollectionDTO.setCustomerType(queryDataMap.get("customer_type").toString());
					}
					if (!queryDataMap.get("payment_mode").toString().equals("Cash")) {
						if (queryDataMap.get("reference_number") != null) {
							salesPaymentCollectionDTO
									.setReferenceNumber(Long.valueOf(queryDataMap.get("reference_number").toString()));
						}
						if (queryDataMap.get("reference_date") != null) {
							salesPaymentCollectionDTO.setReferenceDate((Date) queryDataMap.get("reference_date"));
						}
						if (queryDataMap.get("bank_name") != null) {
							salesPaymentCollectionDTO.setBankName(queryDataMap.get("bank_name").toString());
						}
					}
					if (queryDataMap.get("reference_amount") != null) {
						salesPaymentCollectionDTO
								.setReferenceAmount(Double.valueOf(queryDataMap.get("reference_amount").toString()));
					}
					if (queryDataMap.get("created_date") != null) {
						salesPaymentCollectionDTO
								.setReceiptDate((Date)(queryDataMap.get("created_date")));
					}
				}
				baseDTO.setResponseContent(salesPaymentCollectionDTO);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (RestException restException) {
			baseDTO.setStatusCode(restException.getStatusCode());
			log.error("RestException in SalesPaymentCollectionService ", restException);
		} catch (Exception exception) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.error("Exception in SalesPaymentCollectionService ", exception);
		}
		return baseDTO;
	}

	@Transactional
	public BaseDTO update(SalesPaymentCollectionDTO salesPaymentCollectionDTO) {
		log.info(" SalesPaymentCollectionService update() - START ");
		BaseDTO baseDTO = new BaseDTO();
		try {

			if (salesPaymentCollectionDTO != null) {
				Long voucherId = salesPaymentCollectionDTO.getId();
				Date receiptDate = salesPaymentCollectionDTO.getReceiptDate();
				log.info(" SalesPaymentCollectionService update() - voucherId: " + voucherId);
				log.info(" SalesPaymentCollectionService update() - receiptDate: " + receiptDate);

				if (voucherId == null) {
					throw new RestException("Voucher details not found.");
				}

				EntityMaster loginUserEntity = entityMasterRepository
						.findByUserRegion(loginService.getCurrentUser().getId());
				if (loginUserEntity == null) {
					throw new RestException("Login user entity details not found.");
				}

				EntityTypeMaster entityTypeMaster = loginUserEntity.getEntityTypeMaster();
				if (entityTypeMaster == null) {
					throw new RestException("Login user entity type details not found.");
				}
				String entityTypeCode = entityTypeMaster.getEntityCode();
				log.info(" SalesPaymentCollectionService update() - entityTypeCode: " + entityTypeCode);

				String HO_GL_ACCOUNT_CODE = JdbcUtil.getAppConfigValue(jdbcTemplate, "CREDIT_COLLECTION_HO_GL_ACCOUNT_CODE");
				String RO_GL_ACCOUNT_CODE = JdbcUtil.getAppConfigValue(jdbcTemplate, "CREDIT_COLLECTION_RO_GL_ACCOUNT_CODE");
				
				if(StringUtils.isEmpty(HO_GL_ACCOUNT_CODE)) {
					HO_GL_ACCOUNT_CODE = CREDIT_COLLECTION_HO_GL_ACCOUNT_CODE;
				}
				
				if(StringUtils.isEmpty(RO_GL_ACCOUNT_CODE)) {
					RO_GL_ACCOUNT_CODE = CREDIT_COLLECTION_RO_GL_ACCOUNT_CODE;
				}
				
				log.info(" SalesPaymentCollectionService update() - HO_GL_ACCOUNT_CODE: " + HO_GL_ACCOUNT_CODE);
				log.info(" SalesPaymentCollectionService update() - RO_GL_ACCOUNT_CODE: " + RO_GL_ACCOUNT_CODE);
				
				Voucher voucherObj = voucherRepository.findOne(voucherId);
				voucherObj.setCreatedDate(receiptDate);
				voucherObj.setFromDate(receiptDate);
				voucherObj.setToDate(receiptDate);
				voucherObj.setVoucherType(voucherTypeRepository.findByName(VoucherTypeDetails.Receipt.toString()));
				
				String paymentMode = salesPaymentCollectionDTO.getPaymentMode();
				log.info(" SalesPaymentCollectionService update() - paymentMode: " + paymentMode);
				if(StringUtils.isNotEmpty(paymentMode)) {
					voucherObj.setPaymentMode(paymentModeRepository.findByCode(paymentMode));
				}

				List<VoucherDetails> voucherDetailsList = voucherDetailsRepository
						.findVoucherDetailsByVoucherId(voucherId);
				if (CollectionUtils.isNotEmpty(voucherDetailsList)) {
					for (VoucherDetails voucherDetailsObj : voucherDetailsList) {
						voucherDetailsObj.setCreatedDate(receiptDate);
						String salesOrderEntityTypeCode = null;
						Long salesInvoiceId = voucherDetailsRepository.getSalesInvoiceId(voucherDetailsObj.getId());
						log.info(" SalesPaymentCollectionService update() - salesInvoiceId: " + salesInvoiceId);
						if(salesInvoiceId != null) {
							SalesInvoice salesInvoice = salesInvoiceRepository.getBySaleaInvoiceId(salesInvoiceId);
							if (salesInvoice != null && salesInvoice.getId() != null) {
								
								if (salesInvoice != null) {
									SalesOrder salesOrder = salesInvoice.getSalesOrder();
									if (salesOrder != null && salesOrder.getId() != null) {
										log.info(" SalesPaymentCollectionService update() - salesOrderId: " + salesOrder.getId());
										EntityMaster entityMaster = salesOrder.getEntityMaster();
										EntityTypeMaster entityTypeMasterObj = entityMaster != null
												? entityMaster.getEntityTypeMaster()
												: null;
										salesOrderEntityTypeCode = entityTypeMasterObj == null ? null
												: entityTypeMasterObj.getEntityCode();
									}
								}
							}
						}
						
						String entityType = salesOrderEntityTypeCode != null ? salesOrderEntityTypeCode : entityTypeCode;
						
						if (EntityType.HEAD_OFFICE.equals(entityType)) {
							GlAccount glaccount = glAccountRepository.findByCode(HO_GL_ACCOUNT_CODE);
							voucherDetailsObj.setGlAccount(glaccount);
						} else {
							GlAccount glaccount = glAccountRepository.findByCode(RO_GL_ACCOUNT_CODE);
							voucherDetailsObj.setGlAccount(glaccount);
						}

					}
					voucherDetailsRepository.save(voucherDetailsList);
				}

				List<PaymentDetails> paymentDetailsList = paymentDetailsRepository.getPaymentListByVoucher(voucherId);
				if (CollectionUtils.isNotEmpty(paymentDetailsList)) {
					for (PaymentDetails paymentDetails : paymentDetailsList) {
						paymentDetails.setCreatedDate(receiptDate);
						paymentDetails.setAmount(voucherObj.getNetAmount());
						Payment payment = paymentDetails.getPayment();
						if (payment != null && payment.getId() != null) {
							payment.setCreatedDate(receiptDate);
							paymentRepository.save(payment);
						}
					}
				}
				paymentDetailsRepository.save(paymentDetailsList);
				voucherRepository.save(voucherObj);

				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (RestException restException) {
			baseDTO.setStatusCode(restException.getStatusCode());
			log.error("RestException in SalesPaymentCollectionService ", restException);
		} catch (Exception exception) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.error("Exception in SalesPaymentCollectionService ", exception);
		}
		return baseDTO;
	}

	@SuppressWarnings("unused")
	public BaseDTO create(SalesPaymentCollectionDTO salesPaymentCollectionDTO) {
		log.info(" SalesPaymentCollectionService create : " + salesPaymentCollectionDTO);
		BaseDTO baseDTO = new BaseDTO();
		try {
			EntityMaster loginUserEntity = entityMasterRepository
					.findByUserRegion(loginService.getCurrentUser().getId());
			Voucher voucher = new Voucher();
			SequenceConfig sequenceConfig = sequenceConfigRepository
					.findBySequenceName(SequenceName.valueOf(SequenceName.SALES_PAYMENT_COLLECTION_NUMBER.name()));
			if (sequenceConfig == null) {
				throw new RestException(ErrorDescription.SEQUENCE_CONFIG_NOT_EXIST);
			}
			String paymentNumberPrefix = loginUserEntity.getCode() + sequenceConfig.getSeparator()
					+ sequenceConfig.getPrefix() + AppUtil.getCurrentYearString() + AppUtil.getCurrentMonthString();
			log.info("<======= Sales Payment Collection Prefix Number ===========>" + paymentNumberPrefix);
			voucher.setReferenceNumberPrefix(paymentNumberPrefix);
			voucher.setReferenceNumber(sequenceConfig.getCurrentValue());
			voucher.setName(VoucherTypeDetails.SalesPayment.toString());
			VoucherType voucherType = voucherTypeRepository.findByName(VoucherTypeDetails.Receipt.toString());
			voucher.setVoucherType(voucherType);
			voucher.setNarration(VoucherTypeDetails.SalesPayment.toString());
			voucher.setFromDate(new Date());
			voucher.setToDate(new Date());

			in.gov.cooptex.core.model.PaymentMode paymentMode = paymentModeRepository
					.findByCode(salesPaymentCollectionDTO.getPaymentMethod().getCode());
			voucher.setPaymentMode(paymentMode);
			Double netAmount = salesPaymentCollectionDTO.getSalesInvoiceDetailsList().stream()
					.mapToDouble(SalesPaymentCollectionResponseDTO::getCollectedAmount).sum();
			voucher.setNetAmount(netAmount);
			log.info("<===== netAmount =======>" + netAmount);
			List<VoucherDetails> voucherDetailsList = new ArrayList<>();
			
			String HO_GL_ACCOUNT_CODE = JdbcUtil.getAppConfigValue(jdbcTemplate, "CREDIT_COLLECTION_HO_GL_ACCOUNT_CODE");
			String RO_GL_ACCOUNT_CODE = JdbcUtil.getAppConfigValue(jdbcTemplate, "CREDIT_COLLECTION_RO_GL_ACCOUNT_CODE");
			
			if(StringUtils.isEmpty(HO_GL_ACCOUNT_CODE)) {
				HO_GL_ACCOUNT_CODE = CREDIT_COLLECTION_HO_GL_ACCOUNT_CODE;
			}
			
			if(StringUtils.isEmpty(RO_GL_ACCOUNT_CODE)) {
				RO_GL_ACCOUNT_CODE = CREDIT_COLLECTION_RO_GL_ACCOUNT_CODE;
			}
			
			log.info(" SalesPaymentCollectionService. create() - HO_GL_ACCOUNT_CODE: " + HO_GL_ACCOUNT_CODE);
			log.info(" SalesPaymentCollectionService. create() - RO_GL_ACCOUNT_CODE: " + RO_GL_ACCOUNT_CODE);
			
			for (SalesPaymentCollectionResponseDTO salesPaymentCollectionResponseDTOObj : salesPaymentCollectionDTO
					.getSalesInvoiceDetailsList()) {
				if (!salesPaymentCollectionResponseDTOObj.getCollectedAmount().equals(Double.valueOf(0))) {
					VoucherDetails voucherDetails = new VoucherDetails();
					voucherDetails.setVoucher(voucher);
					// SalesInvoice salesInvoice = salesInvoiceRepository
					// .getOne(salesPaymentCollectionResponseDTOObj.getInvoiceId());
					SalesInvoice salesInvoice = salesInvoiceRepository
							.getBySaleaInvoiceId(salesPaymentCollectionResponseDTOObj.getInvoiceId());
					voucherDetails.setSalesInvoice(salesInvoice);
					voucherDetails.setAmount(salesPaymentCollectionResponseDTOObj.getCollectedAmount());
					Long entity_id = salesInvoice.getEntityMaster() != null ? salesInvoice.getEntityMaster().getId()
							: null;
					EntityMaster entityMaster = entityMasterRepository.findOne(entity_id);
					voucher.setEntityMaster(entityMaster);
					if (entityMaster.getName().equalsIgnoreCase("HEAD_OFFICE")) {
						GlAccount glaccount = glAccountRepository.findByCode(HO_GL_ACCOUNT_CODE);
						voucherDetails.setGlAccount(glaccount);
					} else {
						GlAccount glaccount = glAccountRepository.findByCode(RO_GL_ACCOUNT_CODE);
						voucherDetails.setGlAccount(glaccount);
					}
					voucherDetailsList.add(voucherDetails);
				}
			}
			voucher.setVoucherDetailsList(voucherDetailsList);
			VoucherLog voucherLog = new VoucherLog();
			voucherLog.setVoucher(voucher);
			voucherLog.setStatus(VoucherStatus.RECEIVED);
			UserMaster userMaster = userMasterRepository.findOne(loginService.getCurrentUser().getId());
			voucherLog.setUserMaster(userMaster);
			voucher.getVoucherLogList().add(voucherLog);
			Payment payment = new Payment();
			if (loginUserEntity == null) {
				log.info("Login User Info Not Available");
				throw new RestException(ErrorDescription.LOGIN_USER_INFO_NOT_AVAILABLE);
			}
			payment.setEntityMaster(loginUserEntity);
			log.info("loginUserEntity ", loginUserEntity);
			payment.setPaymentNumberPrefix(paymentNumberPrefix);
			payment.setPaymentNumber(sequenceConfig.getCurrentValue());
			PaymentDetails paymentDetails = new PaymentDetails();
			paymentDetails.setPayment(payment);
			paymentDetails.setAmount(netAmount);
			paymentDetails.setPaymentCategory(PaymentCategory.PAID_IN);
			if (!salesPaymentCollectionDTO.getPaymentMethod().getName().equalsIgnoreCase("Cash")) {
				paymentDetails.setBankReferenceNumber(salesPaymentCollectionDTO.getReferenceNumber().toString());
				paymentDetails.setDocumentDate(salesPaymentCollectionDTO.getReferenceDate());
				paymentDetails.setBankMaster(salesPaymentCollectionDTO.getBankMaster());
			}
			paymentDetails.setVoucher(voucher);
			paymentDetails.setPaymentTypeMaster(paymentTypeMasterRepository.getCustomerPaymentType());
			paymentDetails.setPaymentMethod(
					paymentMethodRepository.findOne(salesPaymentCollectionDTO.getPaymentMethod().getId()));
			paymentDetails
					.setPaidBy(userMasterRepository.findOne(salesPaymentCollectionDTO.getEmployeeMaster().getUserId()));
			log.info("Saving Payment Entity ", payment);
			// List<SalesInvoiceAdjustment> salesInvoiceAdjustmentList = new ArrayList<>();
			// log.info("<======= Advance Collection Detail List Size
			// ==========>"+salesPaymentCollectionDTO.getAdvanceCollectionDetailsList().size());
			// if (salesPaymentCollectionDTO.getAdvanceCollectionDetailsList() != null) {
			// for (SalesPaymentCollectionResponseDTO salesPaymentCollectionResponseDTO :
			// salesPaymentCollectionDTO
			// .getAdvanceCollectionDetailsList()) {
			// List<SalesInvoiceAdjustment> salesInvoiceAdjustmentLists =
			// salesInvoiceAdjustmentRepository
			// .findBySalesOrderId(salesPaymentCollectionResponseDTO.getSalesOrderId());
			// log.info("<======= Sales Invoice Adjustment List Size ==========>"
			// + salesInvoiceAdjustmentLists.size());
			// if (salesInvoiceAdjustmentLists.size() == 0) {
			// SalesInvoiceAdjustment salesInvoiceAdjustment = new SalesInvoiceAdjustment();
			// salesInvoiceAdjustment.setSalesOrder(
			// salesOrderRepository.findOne(salesPaymentCollectionResponseDTO.getSalesOrderId()));
			// salesInvoiceAdjustment
			// .setAmountAdjusted(salesPaymentCollectionResponseDTO.getCurrentAdjustedAmount());
			// salesInvoiceAdjustment
			// .setBalanceAmountToAdjust(salesPaymentCollectionResponseDTO.getBalanceAdjustedAmount());
			// salesInvoiceAdjustment.setVoucher(voucher);
			// salesInvoiceAdjustmentList.add(salesInvoiceAdjustment);
			// } else {
			// SalesInvoiceAdjustment salesInvAdjObj = salesInvoiceAdjustmentRepository
			// .findOne(salesInvoiceAdjustmentLists.get(0).getId());
			// salesInvAdjObj.setAmountAdjusted(salesInvAdjObj.getAmountAdjusted()
			// + salesPaymentCollectionResponseDTO.getCurrentAdjustedAmount());
			// salesInvAdjObj.setBalanceAmountToAdjust(salesInvAdjObj.getBalanceAmountToAdjust()
			// + salesPaymentCollectionResponseDTO.getBalanceAdjustedAmount());
			// salesInvoiceAdjustmentRepository.save(salesInvAdjObj);
			// log.info("<======= Sales Invoice Adjustment Table Updated ==========>");
			// salesInvoiceAdjustmentList = null;
			// }
			// }
			// }
			// if (salesInvoiceAdjustmentList != null) {
			// salesInvoiceAdjustmentRepository.save(salesInvoiceAdjustmentList);
			// }
			log.info("<======= Sales Invoice Adjustment Table Inserted ==========>");
			sequenceConfig.setCurrentValue(sequenceConfig.getCurrentValue() + 1);
			sequenceConfigRepository.save(sequenceConfig);
			// salesInvoiceAdjustmentRepository.save(salesInvoiceAdjustmentList);
			voucherRepository.save(voucher);
			log.info("<======= Voucher Table Inserted ==========>");
			paymentRepository.save(payment);
			log.info("<======= Payment Table Inserted ==========>");
			paymentDetailsRepository.save(paymentDetails);
			log.info("<======= Payment Details Table Inserted ==========>");
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			baseDTO.setStatusCode(restException.getStatusCode());
			log.error("RestException in SalesPaymentCollectionService ", restException);
		} catch (Exception exception) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.error("Exception in SalesPaymentCollectionService ", exception);
		}
		return baseDTO;
	}

	public BaseDTO search(PaginationDTO request) {
		log.info("Sales Payment Collection search called..." + request);

		BaseDTO baseDTO = new BaseDTO();

		try {

			Integer total = 0;
			Integer start = request.getFirst(), pageSize = request.getPageSize();
			start = start * pageSize;
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();

			UserMaster loginUser = loginService.getCurrentUser();
			Long loginUserId = loginUser != null ? loginUser.getId() : null;

			EntityMaster entityMaster = entityMasterRepository.getEntityInfoByLoggedInUser(loginUserId);
			Long loginEntityId = entityMaster != null ? entityMaster.getId() : null;

			String mainQuery = JdbcUtil.getApplicationQueryContent(jdbcTemplate,
					"SALES_PAYMENT_COLLECTION_LIST_DETAILS_SQL");
			if (StringUtils.isEmpty(mainQuery)) {
				throw new Exception("SALES_PAYMENT_COLLECTION_LIST_DETAILS_SQL - Query not found.");
			}
			mainQuery = StringUtils.replace(mainQuery, ":loginEntityId", String.valueOf(loginEntityId));
			if (request.getFilters() != null) {
				if (request.getFilters().get("voucherPrefixNumber") != null) {
					mainQuery += " and v.reference_number_prefix like '%"
							+ request.getFilters().get("voucherPrefixNumber") + "%'";
					mainQuery += " or cast(v.reference_number as varchar) like '%"
							+ request.getFilters().get("voucherPrefixNumber") + "%'";
				}
				if (request.getFilters().get("voucherDate") != null) {
					Date createdDate = new Date((Long) request.getFilters().get("voucherDate"));
					mainQuery += " and v.created_date::date = date '" + createdDate + "'";
				}
				if (request.getFilters().get("voucherAmount") != null) {
					mainQuery += " and cast(v.net_amount as varchar) like '%"
							+ request.getFilters().get("voucherAmount") + "%'";
				}
				if (request.getFilters().get("customerName") != null) {
					mainQuery += " and c.code like '%" + request.getFilters().get("customerName") + "%'";
					mainQuery += " or upper(c.name) like upper('%" + request.getFilters().get("customerName") + "%')";
				}
				if (request.getFilters().get("paymentMode") != null) {
					mainQuery += " and pm.name='" + request.getFilters().get("paymentMode") + "'";
				}
			}
			mainQuery += " group by v.id,c.id,pm.id";
			// String countQuery = mainQuery.replace(
			// "select v.id as voucherId,date(v.created_date) as
			// voucherDate,concat(v.reference_number_prefix,v.reference_number) as
			// voucherNumber,v.net_amount as voucherAmount,concat(c.code,'/',c.name) as
			// customerCodeorName,pm.name as paymentMode",
			// "select count(distinct(v.id)) as count ");

			String countQuery = "SELECT count(*) AS count FROM (" + mainQuery + ")t";
			log.info("count query... " + countQuery);
			countData = jdbcTemplate.queryForList(countQuery);
			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					total = Integer.parseInt(data.get("count").toString().replace(",", ""));
			}

			if (request.getSortField() == null)
				mainQuery += " order by voucherId desc limit " + pageSize + " offset " + start + ";";

			if (request.getSortField() != null && request.getSortOrder() != null) {
				log.info("Sort Field:[" + request.getSortField() + "] Sort Order:[" + request.getSortOrder() + "]");
				if (request.getSortField().equals("voucherPrefixNumber") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by voucherNumber asc ";
				if (request.getSortField().equals("voucherPrefixNumber") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by voucherNumber desc ";
				if (request.getSortField().equals("voucherDate") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by voucherDate asc  ";
				if (request.getSortField().equals("voucherDate") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by voucherDate desc  ";
				if (request.getSortField().equals("voucherAmount") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by voucherAmount asc ";
				if (request.getSortField().equals("voucherAmount") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by voucherAmount desc ";
				if (request.getSortField().equals("customerName") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by customerCodeorName asc ";
				if (request.getSortField().equals("customerName") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by customerCodeorName desc ";
				if (request.getSortField().equals("paymentMode") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by paymentMode asc ";
				if (request.getSortField().equals("paymentMode") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by paymentMode desc ";
				mainQuery += " limit " + pageSize + " offset " + start + ";";
			}
			log.info("Main Qury....." + mainQuery);
			List<SalesPaymentCollectionDTO> salesPaymentCollectionDTOList = new ArrayList<>();
			listofData = jdbcTemplate.queryForList(mainQuery);
			for (Map<String, Object> data : listofData) {
				SalesPaymentCollectionDTO salesPaymentCollectionDTO = new SalesPaymentCollectionDTO();
				if (data.get("voucherId") != null)
					salesPaymentCollectionDTO.setId(Long.parseLong(data.get("voucherId").toString()));
				if (data.get("voucherNumber") != null)
					salesPaymentCollectionDTO.setVoucherPrefixNumber(data.get("voucherNumber").toString());
				if (data.get("voucherDate") != null)
					salesPaymentCollectionDTO.setVoucherDate((Date) data.get("voucherDate"));
				if (data.get("voucherAmount") != null)
					salesPaymentCollectionDTO.setVoucherAmount(Double.valueOf(data.get("voucherAmount").toString()));
				if (data.get("customerCodeorName") != null)
					salesPaymentCollectionDTO.setCustomerName(data.get("customerCodeorName").toString());
				if (data.get("paymentMode") != null)
					salesPaymentCollectionDTO.setPaymentMode(data.get("paymentMode").toString());

				salesPaymentCollectionDTOList.add(salesPaymentCollectionDTO);
			}
			log.info("Returning salesPaymentCollectionDTO list...." + salesPaymentCollectionDTOList.size());
			log.info("Total records present in salesPaymentCollectionDTO..." + total);
			baseDTO.setResponseContent(salesPaymentCollectionDTOList);
			baseDTO.setTotalRecords(total);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (Exception exp) {
			log.error("Exception Cause  : ", exp);
			baseDTO.setErrorDescription(exp.getMessage());
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO loadPaymentMethod() {
		log.info("<--- Starts SalesPaymentCollectionService .loadPaymentMethod() ---> ");
		BaseDTO baseDTO = new BaseDTO();

		try {

			List<PaymentMethod> paymentMethodList = paymentMethodRepository.getSomePymentMethod();

			baseDTO.setResponseContent(paymentMethodList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (RestException restException) {
			baseDTO.setStatusCode(restException.getStatusCode());
			log.error("RestException in loadPaymentMethod ", restException);
		} catch (Exception exception) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.error("Exception in loadPaymentMethod ", exception);
		}
		log.info("<--- Ends loadPaymentMethod() ---> ");
		return baseDTO;
	}

	public BaseDTO getAllCustomersByNameType(String customerName, Long typeId) {
		log.info("<---Starts CustomerMasterService .getAllCustomersByName--->");
		BaseDTO baseDTO = new BaseDTO();
		int customerListSize = 0;
		try {
			List<CustomerMaster> customerList = customerMasterRepository.findAllCustomerByTypeId(customerName, typeId);
			customerListSize = customerList == null ? 0 : customerList.size();
			log.info("<---getAllCustomersByType() - customerListSize ---> " + customerListSize);
			baseDTO.setResponseContent(customerList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException exception) {

			baseDTO.setStatusCode(exception.getStatusCode());

			log.error("CustomerMasterService.getAllCustomersByName method RestException ", exception);

		} catch (DataIntegrityViolationException exception) {

			log.error("Data Integrity Violation Exception in CustomerMasterService.getAllCustomersByName method",
					exception);

			String exceptionCause = ExceptionUtils.getRootCauseMessage(exception);
			log.error("Exception Cause 1 : " + exceptionCause);

			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());

		} catch (Exception exception) {

			log.error("Error in CustomerMasterService.getAllCustomersByName method ", exception);

			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}

		return baseDTO;
	}
	
	public BaseDTO getAllCustomersByName(String customerName) {
		log.info("<---Starts CustomerMasterService .getAllCustomersByName--->");
		BaseDTO baseDTO = new BaseDTO();
		int customerListSize = 0;
		try {
			List<CustomerMaster> customerList = customerMasterRepository.findCustomerMasterByName(customerName);
			customerListSize = customerList == null ? 0 : customerList.size();
			log.info("<---getAllCustomersByType() - customerListSize ---> " + customerListSize);
			baseDTO.setResponseContent(customerList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException exception) {

			baseDTO.setStatusCode(exception.getStatusCode());

			log.error("CustomerMasterService.getAllCustomersByName method RestException ", exception);

		} catch (DataIntegrityViolationException exception) {

			log.error("Data Integrity Violation Exception in CustomerMasterService.getAllCustomersByName method",
					exception);

			String exceptionCause = ExceptionUtils.getRootCauseMessage(exception);
			log.error("Exception Cause 1 : " + exceptionCause);

			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());

		} catch (Exception exception) {

			log.error("Error in CustomerMasterService.getAllCustomersByName method ", exception);

			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}

		return baseDTO;
	}


}
