package in.gov.cooptex.finance.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.service.NotificationEmailService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.dto.InvestmentInterestCollectionDTO;
import in.gov.cooptex.core.accounts.enums.VoucherStatus;
import in.gov.cooptex.core.accounts.model.InvestmentInterestCollection;
import in.gov.cooptex.core.accounts.model.NewInvestment;
import in.gov.cooptex.core.accounts.model.Payment;
import in.gov.cooptex.core.accounts.model.PaymentDetails;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.VoucherDetails;
import in.gov.cooptex.core.accounts.model.VoucherLog;
import in.gov.cooptex.core.accounts.model.VoucherNote;
import in.gov.cooptex.core.accounts.model.VoucherType;
import in.gov.cooptex.core.accounts.repository.InvestmentInterestCollectionRepository;
import in.gov.cooptex.core.accounts.repository.NewInvestmentRepository;
import in.gov.cooptex.core.accounts.repository.PaymentDetailsRepository;
import in.gov.cooptex.core.accounts.repository.PaymentMethodRepository;
import in.gov.cooptex.core.accounts.repository.PaymentRepository;
import in.gov.cooptex.core.accounts.repository.PaymentTypeMasterRepository;
import in.gov.cooptex.core.accounts.repository.VoucherLogRepository;
import in.gov.cooptex.core.accounts.repository.VoucherNoteRepository;
import in.gov.cooptex.core.accounts.repository.VoucherRepository;
import in.gov.cooptex.core.accounts.repository.VoucherTypeRepository;
import in.gov.cooptex.core.accounts.util.VoucherTypeCons;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.enums.PaymentCategory;
import in.gov.cooptex.core.enums.SequenceName;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.PaymentMethod;
import in.gov.cooptex.core.model.SequenceConfig;
import in.gov.cooptex.core.model.SystemNotification;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.SequenceConfigRepository;
import in.gov.cooptex.core.repository.SystemNotificationRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class InvestmentInterestCollectionService implements VoucherTypeCons {
	private final String VIEW_PAGE = "/pages/finance/viewInvestmentInterestCollection.xhtml?faces-redirect=true";

	@Autowired
	InvestmentInterestCollectionRepository investmentInterestCollectionRepository;

	@Autowired
	ResponseWrapper responseWrapper;

	@Autowired
	ApplicationQueryRepository appQueryRepository;
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	SequenceConfigRepository sequenceConfigRepository;

	@Autowired
	VoucherTypeRepository voucherTypeRepository;

	@Autowired
	UserMasterRepository userMasterRepository;

	@Autowired
	VoucherLogRepository voucherLogRepository;

	@Autowired
	LoginService loginService;

	@Autowired
	PaymentRepository paymentRepository;

	@Autowired
	VoucherRepository voucherRepository;

	@Autowired
	VoucherNoteRepository voucherNoteRepository;

	@Autowired
	PaymentTypeMasterRepository paymentTypeMasterRepository;

	@Autowired
	PaymentDetailsRepository paymentDetailsRepository;

	@Autowired
	PaymentMethodRepository paymentMethodRepository;

	@Autowired
	NewInvestmentRepository newInvestmentRepository;
	
	@Autowired
	ApplicationQueryRepository applicationQueryRepository; 
	
	@Autowired
	NotificationEmailService notificationEmailService;
	
	@Autowired
	SystemNotificationRepository systemNotificationRepository;

	public BaseDTO getById(Long id,Long notificationId) {
		log.info("InvestmentInterestCollectionService getById method started [" + id +"-"+notificationId+ "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			InvestmentInterestCollectionDTO investmentDTO = new InvestmentInterestCollectionDTO();
			if (id != null) {
				InvestmentInterestCollection investmentInterestCollection = investmentInterestCollectionRepository
						.findOne(id);
				log.info("InvestmentInterestCollection obj------------" + investmentInterestCollection);

				NewInvestment newInvestment = newInvestmentRepository
						.findOne(investmentInterestCollection.getNewInvestment().getId());
				log.info("NewInvestment obj------------" + newInvestment);

				VoucherNote voucherNote = voucherNoteRepository
						.findByVoucherId(investmentInterestCollection.getVoucher().getId());
				VoucherLog voucherLog = voucherLogRepository
						.findByVoucherId(investmentInterestCollection.getVoucher().getId());
				log.info("VoucherLog obj---------------" + voucherLog);
				log.info("VoucherNote obj---------------" + voucherNote);

				List<PaymentDetails> paymentDetails = paymentDetailsRepository
						.getPaymentListByVoucher(investmentInterestCollection.getVoucher().getId());
				log.info("paymentDetails list size-------" + paymentDetails.size());
				if (paymentDetails.size() > 0) {
					investmentDTO.setDocumentNumber(paymentDetails.get(0).getBankReferenceNumber());
					investmentDTO.setDocumentDate(paymentDetails.get(0).getDocumentDate());
					investmentDTO.setBankMaster(paymentDetails.get(0).getBankMaster());
				}
				investmentDTO.setPaymentMethod(investmentInterestCollection.getPaymentMethod());
				investmentDTO.setInvestmentInterestCollection(investmentInterestCollection);
				investmentDTO.setNewInvestment(newInvestment);
				investmentDTO.setFianlApproval(voucherNote.getFinalApproval());
				investmentDTO.setStage(String.valueOf(voucherLog.getStatus()));
				investmentDTO.setVoucherNotes(voucherNote);
				investmentDTO.setVoucherNote(voucherNote.getNote());
				investmentDTO.setUserMaster(voucherNote.getForwardTo());
				Voucher voucher = voucherRepository.findOne(investmentInterestCollection.getVoucher().getId());
				List<VoucherNote> voucherNoteList = voucherNoteRepository
						.getVoucherNoteList(investmentInterestCollection.getVoucher().getId());
				List<VoucherLog> voucherLog1 = voucherLogRepository
						.getVoucherLogList(investmentInterestCollection.getVoucher().getId());
				investmentDTO.setVoucherLogList(voucherLog1);
				investmentDTO.setVoucherNoteList(voucherNoteList);
				investmentDTO.setVoucher(voucher);
				
				List<Map<String, Object>> employeeData = new ArrayList<Map<String, Object>>();
				if(voucher!=null) {
					log.info("<<<:::::::voucher::::not Null::::>>>>"+voucher!=null ? voucher.getId():"Null");
					 ApplicationQuery applicationQueryForlog = applicationQueryRepository.findByQueryName("VOUCHER_LOG_EMPLOYEE_DETAILS");
					 if (applicationQueryForlog == null || applicationQueryForlog.getId() == null) {
						 log.info("Application Query For Log Details not found for query name : " + applicationQueryForlog);
						 baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
						 return baseDTO;
					 }
					 String logquery = applicationQueryForlog.getQueryContent().trim();
					 log.info("<=========VOUCHER_LOG_EMPLOYEE_DETAILS Query Content ======>"+logquery);
					 logquery = logquery.replace(":voucherId", "'"+voucher.getId().toString()+"'");
					 log.info("Query Content For VOUCHER_LOG_EMPLOYEE_DETAILS After replaced plan id View query : " + logquery);
					 employeeData = jdbcTemplate.queryForList(logquery);
					 log.info("<=========VOUCHER_LOG_EMPLOYEE_DETAILS Employee Data======>"+employeeData);
					 baseDTO.setTotalListOfData(employeeData);
				}
				
				if (notificationId != null && notificationId > 0) {
					SystemNotification systemNotification = systemNotificationRepository.findOne(notificationId);
					systemNotification.setNotificationRead(true);
					systemNotificationRepository.save(systemNotification);
					
					
				}
				baseDTO.setResponseContent(investmentDTO);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (RestException restException) {
			log.error("InvestmentInterestCollectionService getById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("InvestmentInterestCollectionService getById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("InvestmentInterestCollectionService getById method completed");
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO deleteById(Long id) {
		log.info("<===== Start InvestmentInterestCollectionService.deleteById ======>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			investmentInterestCollectionRepository.delete(id);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("InvestmentInterestCollectionService deleteById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (DataIntegrityViolationException exception) {
			log.error("InvestmentInterestCollectionService deleteById DataIntegrityViolationException ", exception);
			if (exception.getCause().getCause() instanceof PSQLException) {
				baseDTO.setStatusCode(ErrorDescription.CANNOT_DELETE_REFERENCED_RECORD.getErrorCode());
			} else {
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			}
		} catch (Exception exception) {
			log.error("InvestmentInterestCollectionService deleteById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<===== End InvestmentInterestCollectionService.deleteById ======>");
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO searchLazyList(PaginationDTO paginationDTO) {
		log.info("<===== Start InvestmentInterestCollectionService.searchLazyList ======>");
		BaseDTO baseDTO = new BaseDTO();
		Integer totalRecords = 0;
		List<InvestmentInterestCollectionDTO> resultList = null;
		try {
			resultList = new ArrayList<InvestmentInterestCollectionDTO>();
//			baseDTO = new BaseDTO();
			Integer start = paginationDTO.getFirst(), pageSize = paginationDTO.getPageSize();
			start = start * pageSize;
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("INVESTMENT_INTEREST_COLLECTION");
			String mainQuery = applicationQuery.getQueryContent();
			log.info("db query----------" + mainQuery);

			mainQuery = mainQuery.replaceAll(";", "");
			log.info("after replace query------" + mainQuery);

			if (paginationDTO.getFilters() != null) {
				mainQuery += " where:";
				if (paginationDTO.getFilters().get("investmentType") != null) {
					log.info(" investmentType Filter : " + paginationDTO.getFilters().get("investmentType"));
					mainQuery += " and upper(itm.name) like upper('%"
							+ paginationDTO.getFilters().get("investmentType").toString() + "%')";
				}
				if (paginationDTO.getFilters().get("bankName") != null) {
					log.info(" bankName Filter : " + paginationDTO.getFilters().get("bankName"));
					mainQuery += " and upper(bm.bank_name) like upper('%"
							+ paginationDTO.getFilters().get("bankName").toString() + "%')";
				}
				if (paginationDTO.getFilters().get("bankBranch") != null) {
					log.info(" bankBranch Filter : " + paginationDTO.getFilters().get("bankBranch"));
					mainQuery += " and upper(concat(bbm.branch_code,'/',bbm.branch_name)) like upper('%"
							+ paginationDTO.getFilters().get("bankBranch").toString() + "%')";
				}
				if (paginationDTO.getFilters().get("investmentNumber") != null) {
					log.info(" investmentNumber Filter : " + paginationDTO.getFilters().get("investmentNumber"));
					mainQuery += " and cast(ni.cerificate_number as varchar) like ('%"
							+ paginationDTO.getFilters().get("investmentNumber").toString() + "%')";
				}
				if (paginationDTO.getFilters().get("status") != null) {
					log.info(" status Filter : " + paginationDTO.getFilters().get("status"));
					mainQuery += " and upper(vl.status) like upper('%"
							+ paginationDTO.getFilters().get("status").toString() + "%')";
				}
				mainQuery = mainQuery.replaceAll("where: and", "where");
				String where = mainQuery.substring(mainQuery.length() - 6);
				if (where.equals("where:"))
					mainQuery = mainQuery.replaceAll(" where:", "");
			}
			if (paginationDTO.getSortField() != null && paginationDTO.getSortOrder() != null) {
				log.info("Sort Field:[" + paginationDTO.getSortField() + "] Sort Order:[" + paginationDTO.getSortOrder()
						+ "]");
				if (paginationDTO.getSortField().equals("investmentType")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by itm.name asc  ";
				else if (paginationDTO.getSortField().equals("investmentType")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by itm.name desc  ";
				if (paginationDTO.getSortField().equals("bankName") && paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by bm.bank_name_prefix asc ";
				else if (paginationDTO.getSortField().equals("bankName")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by bm.bank_name desc ";
				if (paginationDTO.getSortField().equals("concat") && paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by concat(bbm.branch_code,'/',bbm.branch_name) asc ";
				else if (paginationDTO.getSortField().equals("concat")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by concat(bbm.branch_code,'/',bbm.branch_name) desc ";
				if (paginationDTO.getSortField().equals("investmentNumber")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by ni.cerificate_number asc ";
				else if (paginationDTO.getSortField().equals("investmentNumber")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by ni.cerificate_number desc ";
				if (paginationDTO.getSortField().equals("status") && paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by vl.status asc ";
				else if (paginationDTO.getSortField().equals("status")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by vl.status desc ";
				/* mainQuery += " limit " + pageSize + " offset " + start + ";"; */
			}
			mainQuery += " order by iic.created_date desc";
			listofData = jdbcTemplate.queryForList(mainQuery);
			totalRecords = listofData.size();

			log.info("amount transfer list size-------------->" + listofData.size());

			for (Map<String, Object> data : listofData) {
				InvestmentInterestCollectionDTO response = new InvestmentInterestCollectionDTO();
				response.setId((Long.valueOf(data.get("id").toString())));
				response.setInvestmentType(((data.get("name").toString())));
				response.setBankName(((data.get("bank_name").toString())));
				response.setBankBranch(((data.get("concat").toString())));
				response.setInvestmentNumber((Long.valueOf(data.get("cerificate_number").toString())));
				response.setStatus((String) data.get("status"));
				resultList.add(response);
			}

			log.info("final list size---------------" + resultList.size());
			baseDTO.setTotalRecords(totalRecords!=null?totalRecords:0);
			baseDTO.setResponseContents(resultList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception ex) {
			log.error("inside lazy method exception------", ex);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<===== End InvestmentInterestCollectionService.searchLazyList ======>");
		return baseDTO;
	}

	public BaseDTO createInvestmentInterestCollection(InvestmentInterestCollectionDTO investmentInterestCollectionDTO) {
		log.info("<===== Start InvestmentInterestCollectionService.deleteById ======>");
		BaseDTO baseDTO = new BaseDTO();
		String referenceNumber = "";
		try {
			if (investmentInterestCollectionDTO.getInvestmentInterestCollection().getId() == null) {
				SequenceConfig sequenceConfig = sequenceConfigRepository
						.findBySequenceName(SequenceName.INVESTMENT_INTEREST_COLLECTION);
				if (sequenceConfig == null) {
					throw new RestException(ErrorDescription.SEQUENCE_CONFIG_NOT_EXIST);
				}
				referenceNumber = investmentInterestCollectionDTO.getEntityMaster().getCode()
						+ sequenceConfig.getSeparator() + sequenceConfig.getPrefix() + AppUtil.getCurrentYearString()
						+ AppUtil.getCurrentMonthString();
				log.info("voucher reference number-----------" + referenceNumber);
				UserMaster userMaster = userMasterRepository.findOne(loginService.getCurrentUser().getId());
				Voucher voucher = new Voucher();
				voucher.setReferenceNumberPrefix(referenceNumber);
				voucher.setReferenceNumber(sequenceConfig.getCurrentValue());
				voucher.setName(PAYMENTSMALL);
				voucher.setNetAmount(investmentInterestCollectionDTO.getTotalInterestedCollected() == null ? 0D : 
					investmentInterestCollectionDTO.getTotalInterestedCollected());
				VoucherType voucherType = voucherTypeRepository.findByName(PAYMENTSMALL);
				log.info("voucherType Id===============>" + voucherType.getId());
				voucher.setVoucherType(voucherType);
				voucher.setNarration(PAYMENTSMALL);
				voucher.setFromDate(new Date());
				voucher.setToDate(new Date());
				// voucher.setNetAmount(investmentInterestCollectionDTO.getDocumentAmount());
				voucher.setCreatedDate(new Date());
				voucher.setCreatedBy(userMaster);
				// voucher.setNetAmount(investmentInterestCollectionDTO.getDocumentAmount());
				log.info("Voucher table added-------------");

				VoucherDetails voucherDetails = new VoucherDetails();
				voucherDetails.setVoucher(voucher);
				voucherDetails.setAmount(investmentInterestCollectionDTO.getInterestedCollection());
				voucher.getVoucherDetailsList().add(voucherDetails);
				voucherDetails.setCreatedDate(new Date());
				voucherDetails.setCreatedBy(userMaster);
				log.info("VoucherDetails table added---------------");

				VoucherNote voucherNote = new VoucherNote();
				voucherNote.setForwardTo(
						userMasterRepository.findOne(investmentInterestCollectionDTO.getUserMaster().getId()));
				log.info("voucherNote forwardTo user---------------->" + voucherNote.getForwardTo());
				voucherNote.setFinalApproval(investmentInterestCollectionDTO.getFianlApproval());
				voucherNote.setNote(investmentInterestCollectionDTO.getVoucherNote());
				voucherNote.setVoucher(voucher);
				voucher.getVoucherNote().add(voucherNote);
				voucherNote.setCreatedDate(new Date());
				voucherNote.setCreatedBy(userMaster);
				log.info("voucherNote table added----------------");

				VoucherLog voucherLog = new VoucherLog();
				voucherLog.setVoucher(voucher);
				voucherLog.setStatus(VoucherStatus.SUBMITTED);

				log.info("VoucherLog userMaster----------->" + userMaster.getId());
				voucherLog.setUserMaster(userMaster);
				voucher.getVoucherLogList().add(voucherLog);
				voucherLog.setCreatedDate(new Date());
				voucherLog.setCreatedBy(userMaster);
				log.info("VoucherLog table added---------------");

				voucherRepository.save(voucher);
				log.info("Voucher datas inserted----------------");

				Payment payment = new Payment();
				payment.setEntityMaster(investmentInterestCollectionDTO.getEntityMaster());
				payment.setPaymentNumberPrefix(referenceNumber);
				payment.setPaymentNumber(sequenceConfig.getCurrentValue());
				payment.setCreatedDate(new Date());
				payment.setCreatedBy(userMaster);
				paymentRepository.save(payment);
				log.info("payment saved---------------" + payment.getId());

				PaymentDetails paymentDetails = new PaymentDetails();
				paymentDetails.setPayment(payment);
				paymentDetails.setPaymentCategory(PaymentCategory.PAID_IN);
				if (!investmentInterestCollectionDTO.getPaymentMethod().getName().equals("Cash")) {
					paymentDetails
							.setBankReferenceNumber(investmentInterestCollectionDTO.getDocumentNumber().toString());
					paymentDetails.setDocumentDate(investmentInterestCollectionDTO.getDocumentDate());
					paymentDetails.setBankMaster(investmentInterestCollectionDTO.getBankMaster());
				}
				paymentDetails.setVoucher(voucher);
				paymentDetails.setPaymentTypeMaster(paymentTypeMasterRepository.getCustomerPaymentType());
				paymentDetails.setPaymentMethod(
						paymentMethodRepository.findOne(investmentInterestCollectionDTO.getPaymentMethod().getId()));
				paymentDetails.setCreatedDate(new Date());
				paymentDetails.setCreatedBy(userMaster);
				paymentDetailsRepository.save(paymentDetails);
				log.info("PaymentDetails saved-------" + paymentDetails.getId());

				sequenceConfig.setCurrentValue(sequenceConfig.getCurrentValue() + 1);
				sequenceConfigRepository.save(sequenceConfig);
				log.info("sequenceConfig value updated-----");

				InvestmentInterestCollection investmentInterestCollection = new InvestmentInterestCollection();
				investmentInterestCollection.setNewInvestment(
						newInvestmentRepository.findOne(investmentInterestCollectionDTO.getNewInvestment().getId()));
				log.info("new investment---------" + investmentInterestCollection.getNewInvestment());
				investmentInterestCollection
						.setInterestAmount(investmentInterestCollectionDTO.getInterestedCollection());
				investmentInterestCollection.setPaymentMethod(
						paymentMethodRepository.findOne(investmentInterestCollectionDTO.getPaymentMethod().getId()));
				investmentInterestCollection.setVoucher(voucher);
				investmentInterestCollection.setCreatedDate(new Date());
				investmentInterestCollection.setCreatedBy(userMaster);
				investmentInterestCollection.setInterestPeriodFrom(investmentInterestCollectionDTO.getDepositeDate());
				investmentInterestCollection.setInterestPeriodTo(investmentInterestCollectionDTO.getDepositeDate());
				investmentInterestCollectionRepository.save(investmentInterestCollection);
				log.info("InvestmentInterestCollection saved-------" + investmentInterestCollection.getId());
				
				//For Notification
				Map<String, Object> additionalData = new HashMap<>();
				additionalData.put("Url", VIEW_PAGE + "&id=" + investmentInterestCollection.getId() + "&");
				notificationEmailService.sendMailAndNotificationForForward(voucherNote, voucherLog,
						additionalData);

				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			} else {
				baseDTO = updateInvestmentInterestCollection(investmentInterestCollectionDTO);
			}

		} catch (Exception e) {
			log.error("InvestmentInterestCollectionService createInvestmentInterestCollection Exception ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<===== End InvestmentInterestCollectionService.deleteById ======>");
		return baseDTO;
	}

	public BaseDTO approve(InvestmentInterestCollectionDTO investmentInterestCollectionDTO) {
		log.info("<===== Start StaffMonthlyConveyanceService.staffConveyanceApprove =========>");
		BaseDTO response = new BaseDTO();
		String message="";
		try {
			Validate.objectNotNull(investmentInterestCollectionDTO.getVoucherNotes().getForwardTo(),
					ErrorDescription.NEW_INVESTMENT_FORWARD_TO_NULL);
			if (investmentInterestCollectionDTO.getVoucherLog().getStatus().equals(VoucherStatus.REJECTED)) {
				VoucherLog voucherlog = new VoucherLog();
				voucherlog.setStatus(investmentInterestCollectionDTO.getVoucherLog().getStatus());
				voucherlog.setRemarks(investmentInterestCollectionDTO.getVoucherLog().getRemarks());
				voucherlog.setUserMaster(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
				voucherlog.setVoucher(
						voucherRepository.findOne(investmentInterestCollectionDTO.getInvestmentInterestCollection().getVoucher().getId()));
				voucherLogRepository.save(voucherlog);
				if(voucherlog.getStatus().equals(VoucherStatus.REJECTED)){
					
					List<VoucherLog> toUserId= voucherLogRepository.getforwardtoUserId(investmentInterestCollectionDTO.getVoucher().getId(),loginService.getCurrentUser().getId());
					log.info("logggg"+ toUserId + investmentInterestCollectionDTO.getVoucher().getId()+"--"+ loginService.getCurrentUser().getId());
					for(VoucherLog voucherlg: toUserId) {
						Long approveid= voucherlg.getCreatedBy().getId();
						log.info("iddd" + voucherlg.getCreatedBy().getId());
						String urlPath="/pages/finance/viewInvestmentInterestCollection.xhtml?faces-redirect=true&id="
								+ investmentInterestCollectionDTO.getInvestmentInterestCollection().getId()  + "&stage=" + voucherlog.getStatus() + "&";
						
						
						 if(voucherlog.getStatus().equals(VoucherStatus.REJECTED)){
							 message= "Investment Interest Collection - " + investmentInterestCollectionDTO.getVoucher().getId()+ "  has been Rejected " ;
						}
						notificationEmailService.saveIndividualNotification(loginService.getCurrentUser(),approveid,urlPath,"Investment",message);
					}
					
				}
				
				
				
				
			} else {
				VoucherLog voucherlog = new VoucherLog();
				voucherlog.setStatus(investmentInterestCollectionDTO.getVoucherLog().getStatus());
				voucherlog.setRemarks(investmentInterestCollectionDTO.getVoucherLog().getRemarks());
				voucherlog.setUserMaster(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
				voucherlog.setVoucher(
						voucherRepository.findOne(investmentInterestCollectionDTO.getInvestmentInterestCollection().getVoucher().getId()));
				voucherLogRepository.save(voucherlog);

				VoucherNote voucherNote = new VoucherNote();
				voucherNote.setNote(investmentInterestCollectionDTO.getVoucherNotes().getNote());
				voucherNote.setForwardTo(
						userMasterRepository.findOne(investmentInterestCollectionDTO.getVoucherNotes().getForwardTo().getId()));
				voucherNote.setFinalApproval(investmentInterestCollectionDTO.getVoucherNotes().getFinalApproval());
				voucherNote.setVoucher(
						voucherRepository.findOne(investmentInterestCollectionDTO.getInvestmentInterestCollection().getVoucher().getId()));
				voucherNoteRepository.save(voucherNote);
				
				if(voucherlog.getStatus().equals(VoucherStatus.APPROVED)
						|| voucherlog.getStatus().equals(VoucherStatus.SUBMITTED)
						||voucherlog.getStatus().equals(VoucherStatus.FINALAPPROVED )){
					
					List<VoucherLog> toUserId= voucherLogRepository.getforwardtoUserId(investmentInterestCollectionDTO.getVoucher().getId(),loginService.getCurrentUser().getId());
					log.info("logggg"+ toUserId + investmentInterestCollectionDTO.getVoucher().getId()+"--"+ loginService.getCurrentUser().getId());
					for(VoucherLog voucherlg: toUserId) {
						Long approveid= voucherlg.getCreatedBy().getId();
						log.info("iddd" + voucherlg.getCreatedBy().getId());
						String urlPath="/pages/finance/viewInvestmentInterestCollection.xhtml?faces-redirect=true&id="
								+ investmentInterestCollectionDTO.getInvestmentInterestCollection().getId()  + "&stage=" + voucherlog.getStatus() + "&";
						
						if(voucherlog.getStatus().equals(VoucherStatus.SUBMITTED)){
							 message= "Investment Interest Collection - " + investmentInterestCollectionDTO.getVoucher().getId()+ " has been Approved " ;
						}
						else if(voucherlog.getStatus().equals(VoucherStatus.APPROVED)){
							 message= "Investment Interest Collection - " + investmentInterestCollectionDTO.getVoucher().getId()+ " has been Approved " ;
						}
						else if(voucherlog.getStatus().equals(VoucherStatus.FINALAPPROVED)){
							 message= "Investment Interest Collection - " + investmentInterestCollectionDTO.getVoucher().getId()+ "  Final Approval has been Completed " ;
						}
						
						notificationEmailService.saveIndividualNotification(loginService.getCurrentUser(),approveid,urlPath,"Investment",message);
					}
					
				}
				if(voucherlog.getStatus().equals(VoucherStatus.APPROVED)) {
				Map<String, Object> additionalData = new HashMap<>();
				additionalData.put("Url", VIEW_PAGE + "&id=" +investmentInterestCollectionDTO.getInvestmentInterestCollection().getId() + "&");
				notificationEmailService.sendMailAndNotificationForForward(voucherNote, voucherlog,
						additionalData);
				}
			}

			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		
		
		} catch (RestException restexp) {
			log.error("StaffMonthlyConveyanceController.createStaffConveyancePayment RestException", restexp);
			response.setStatusCode(restexp.getErrorCodeDescription().getErrorCode());
		} catch (DataIntegrityViolationException divEX) {
			log.warn("<<===  Error While StaffMonthlyConveyanceController.staffConveyanceApprove ===>>", divEX);
		} catch (Exception exception) {
			log.error("Error while Creating  StaffMonthlyConveyanceController.staffConveyanceApprove ", exception);
			response.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<===== End StaffMonthlyConveyanceController.staffConveyanceApprove ======>");
		return response;
	}

	public BaseDTO approveOld(InvestmentInterestCollectionDTO investmentInterestCollectionDTO) {
		log.info("<===== Start StaffMonthlyConveyanceService.staffConveyanceApprove ======>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			UserMaster umaster = userMasterRepository.findOne((loginService.getCurrentUser().getId()));
			if (investmentInterestCollectionDTO != null) {
				Voucher voucher = new Voucher();
				if (investmentInterestCollectionDTO.getVoucher() != null)
					voucher = voucherRepository.findOne(investmentInterestCollectionDTO.getVoucher().getId());
				voucher.setPaymentMode(investmentInterestCollectionDTO.getVoucher().getPaymentMode());
				voucherRepository.save(voucher);
				log.info("<===== Voucher updated sucessfully ======>");
				if (investmentInterestCollectionDTO.getAction()) {
					VoucherNote voucherNote = new VoucherNote();
					List<VoucherLog> voucherLog = new ArrayList<>();
					if (investmentInterestCollectionDTO.getVoucherNoteList() != null) {
						voucherNote = voucherNoteRepository.findOne(investmentInterestCollectionDTO.getVoucherNoteList()
								.get(investmentInterestCollectionDTO.getVoucherNoteList().size() - 1).getId());
					}
					/*
					 * if (investmentInterestCollectionDTO.getVoucherLogList() != null) voucherLog =
					 * investmentInterestCollectionDTO.getVoucherLogList();
					 */
					if (investmentInterestCollectionDTO.getStage().equals(VoucherStatus.APPROVED.toString())) {
						VoucherNote vn = new VoucherNote();
						vn.setVoucher(voucher);
						vn.setNote(voucherNote.getNote());
						UserMaster forwardTo = userMasterRepository
								.findOne(investmentInterestCollectionDTO.getUserMaster().getId());
						vn.setForwardTo(forwardTo);
						if (investmentInterestCollectionDTO.getForwardFor()
								.equals(VoucherStatus.FINALAPPROVED.toString()))
							vn.setFinalApproval(true);
						else
							vn.setFinalApproval(false);
						vn.setCreatedDate(new Date());
						vn.setCreatedBy(umaster);
						voucherNoteRepository.save(vn);
						log.info("<===== VoucherNote created sucessfully ======>");
					}
					// if (voucherLog != null)
					if (investmentInterestCollectionDTO.getStage().equals(VoucherStatus.APPROVED.toString()))
						voucherNote.setFinalApproval(true);
					voucherNoteRepository.save(voucherNote);
					log.info("<===== VoucherNote updated sucessfully ======>");
					VoucherLog vl = new VoucherLog();
					vl.setVoucher(voucher);
					vl.setUserMaster(umaster);
					if (investmentInterestCollectionDTO.getStage().equals(VoucherStatus.APPROVED.toString()))
						vl.setStatus(VoucherStatus.FINALAPPROVED);
					else {
						if (investmentInterestCollectionDTO.getForwardFor().equals(VoucherStatus.APPROVED.toString()))
							vl.setStatus(VoucherStatus.SUBMITTED);
						if (investmentInterestCollectionDTO.getForwardFor()
								.equals(VoucherStatus.FINALAPPROVED.toString()))
							vl.setStatus(VoucherStatus.APPROVED);
					}
					vl.setCreatedDate(new Date());
					vl.setCreatedBy(umaster);
					voucherLogRepository.save(vl);
					log.info("<===== VoucherLog updated sucessfully ======>");
				} else {
					VoucherLog vl = new VoucherLog();
					vl.setVoucher(voucher);
					vl.setUserMaster(umaster);
					vl.setStatus(VoucherStatus.REJECTED);
					vl.setCreatedDate(new Date());
					vl.setCreatedBy(umaster);
					voucherLogRepository.save(vl);
					log.info("<===== VoucherLog updated sucessfully ======>");
				}
			}
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restexp) {
			log.error("StaffMonthlyConveyanceController.createStaffConveyancePayment RestException", restexp);
			baseDTO.setStatusCode(restexp.getErrorCodeDescription().getErrorCode());
		} catch (DataIntegrityViolationException divEX) {
			log.warn("<<===  Error While StaffMonthlyConveyanceController.staffConveyanceApprove ===>>", divEX);
		} catch (Exception exception) {
			log.error("Error while Creating  StaffMonthlyConveyanceController.staffConveyanceApprove ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<===== End StaffMonthlyConveyanceController.staffConveyanceApprove ======>");
		return baseDTO;
	}

	public BaseDTO updateInvestmentInterestCollection(InvestmentInterestCollectionDTO investmentInterestCollectionDTO) {
		log.info("<===== Start InvestmentInterestCollectionService.deleteById ======>");
		BaseDTO baseDTO = new BaseDTO();
		String referenceNumber = "";
		try {
			if (investmentInterestCollectionDTO.getInvestmentInterestCollection().getId() != null) {
				UserMaster userMaster = userMasterRepository.findOne(loginService.getCurrentUser().getId());
				/*
				 * InvestmentInterestCollection investmentInterestCollection =
				 * investmentInterestCollectionRepository.findOne(id);
				 */
				Voucher voucher = voucherRepository.findOne(
						investmentInterestCollectionDTO.getInvestmentInterestCollection().getVoucher().getId());
				/*
				 * Payment payment=paymentRepository.findOne(investmentInterestCollectionDTO.
				 * getInvestmentInterestCollection().pa)
				 */
				/*
				 * VoucherDetails voucherDetails = new VoucherDetails();
				 * voucherDetails.setVoucher(voucher);
				 * voucherDetails.setAmount(investmentInterestCollectionDTO.
				 * getInterestedCollection());
				 * voucher.getVoucherDetailsList().add(voucherDetails);
				 * voucherDetails.setCreatedDate(new Date());
				 * voucherDetails.setCreatedBy(userMaster);
				 */
				/* log.info("VoucherDetails table added---------------"); */

				VoucherNote voucherNote = new VoucherNote();
				voucherNote.setForwardTo(
						userMasterRepository.findOne(investmentInterestCollectionDTO.getUserMaster().getId()));
				log.info("voucherNote forwardTo user---------------->" + voucherNote.getForwardTo());
				voucherNote.setFinalApproval(investmentInterestCollectionDTO.getFianlApproval());
				voucherNote.setNote(investmentInterestCollectionDTO.getVoucherNote());
				voucherNote.setVoucher(voucher);
				voucher.getVoucherNote().add(voucherNote);
				voucherNote.setCreatedDate(new Date());
				voucherNote.setCreatedBy(userMaster);
				log.info("voucherNote table added----------------");

				VoucherLog voucherLog = new VoucherLog();
				voucherLog.setVoucher(voucher);
				voucherLog.setStatus(VoucherStatus.SUBMITTED);

				log.info("VoucherLog userMaster----------->" + userMaster.getId());
				voucherLog.setUserMaster(userMaster);
				voucher.getVoucherLogList().add(voucherLog);
				voucherLog.setCreatedDate(new Date());
				voucherLog.setCreatedBy(userMaster);
				log.info("VoucherLog table added---------------");

				voucherRepository.save(voucher);
				log.info("Voucher datas inserted----------------");

				/*
				 * Payment payment = new Payment();
				 * payment.setEntityMaster(investmentInterestCollectionDTO.getEntityMaster());
				 * payment.setPaymentNumberPrefix(referenceNumber); payment.setPaymentNumber(sse
				 * equenceConfig.getCurrentValue()); payment.setCreatedDate(new Date());
				 * payment.setCreatedBy(userMaster); paymentRepository.save(payment);
				 * log.info("payment saved---------------"+payment.getId());
				 * 
				 * PaymentDetails paymentDetails = new PaymentDetails();
				 * paymentDetails.setPayment(payment);
				 * paymentDetails.setPaymentCategory(PaymentCategory.PAID_IN);
				 * if(!investmentInterestCollectionDTO.getPaymentMethod().getName().equals(
				 * "Cash")){
				 * paymentDetails.setBankReferenceNumber(investmentInterestCollectionDTO.
				 * getDocumentNumber().toString());
				 * paymentDetails.setDocumentDate(investmentInterestCollectionDTO.
				 * getDocumentDate());
				 * paymentDetails.setBankMaster(investmentInterestCollectionDTO.getBankMaster())
				 * ; } paymentDetails.setVoucher(voucher);
				 * paymentDetails.setPaymentTypeMaster(paymentTypeMasterRepository.
				 * getCustomerPaymentType());
				 * paymentDetails.setPaymentMethod(paymentMethodRepository.findOne(
				 * investmentInterestCollectionDTO.getPaymentMethod().getId()));
				 * paymentDetails.setCreatedDate(new Date());
				 * paymentDetails.setCreatedBy(userMaster);
				 * paymentDetailsRepository.save(paymentDetails);
				 * log.info("PaymentDetails saved-------"+paymentDetails.getId());
				 */

				/*
				 * sequenceConfig.setCurrentValue(sequenceConfig.getCurrentValue() + 1);
				 * sequenceConfigRepository.save(sequenceConfig);
				 * log.info("sequenceConfig value updated-----");
				 */

				/*
				 * InvestmentInterestCollection investmentInterestCollection=new
				 * InvestmentInterestCollection();
				 * investmentInterestCollection.setNewInvestment(newInvestmentRepository.findOne
				 * (investmentInterestCollectionDTO.getNewInvestment().getId()));
				 * log.info("new investment---------"+investmentInterestCollection.
				 * getNewInvestment()); investmentInterestCollection.setInterestAmount(
				 * investmentInterestCollectionDTO.getInterestedCollection());
				 * investmentInterestCollection.setPaymentMethod(paymentMethodRepository.findOne
				 * (investmentInterestCollectionDTO.getPaymentMethod().getId()));
				 * investmentInterestCollection.setVoucher(voucher);
				 * investmentInterestCollection.setCreatedDate(new Date());
				 * investmentInterestCollection.setCreatedBy(userMaster);
				 * investmentInterestCollection.setInterestPeriodFrom(
				 * investmentInterestCollectionDTO.getDepositeDate());
				 * investmentInterestCollection.setInterestPeriodTo(
				 * investmentInterestCollectionDTO.getDepositeDate());
				 * investmentInterestCollectionRepository.save(investmentInterestCollection);
				 */
				/*
				 * log.info("InvestmentInterestCollection saved-------"
				 * +investmentInterestCollection.getId());
				 */

				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}

		} catch (Exception e) {
			log.error("InvestmentInterestCollectionService createInvestmentInterestCollection Exception ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<===== End InvestmentInterestCollectionService.deleteById ======>");
		return baseDTO;
	}

	public BaseDTO loadPaymentMethod() {
		log.info("<--- Starts PaymentService .loadPaymentMethod() ---> ");
		BaseDTO baseDTO = new BaseDTO();

		try {

			List<PaymentMethod> paymentMethodList = paymentMethodRepository.findAll();
			baseDTO.setResponseContent(paymentMethodList);

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (RestException restException) {
			baseDTO.setStatusCode(restException.getStatusCode());
			log.error("RestException in loadPaymentMethod ", restException);
		} catch (Exception exception) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.error("Exception in loadPaymentMethod ", exception);
		}
		log.info("<--- Ends loadPaymentMethod() ---> ");
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO getsomepaymentmethod() {
		log.info("<--- Starts PaymentService .loadPaymentMethod() ---> ");
		BaseDTO baseDTO = new BaseDTO();

		try {

			List<PaymentMethod> paymentMethodList = paymentMethodRepository.getSomePymentMethod();

			baseDTO.setResponseContent(paymentMethodList);

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (RestException restException) {
			baseDTO.setStatusCode(restException.getStatusCode());
			log.error("RestException in loadPaymentMethod ", restException);
		} catch (Exception exception) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.error("Exception in loadPaymentMethod ", exception);
		}
		log.info("<--- Ends loadPaymentMethod() ---> ");
		return responseWrapper.send(baseDTO);
	}
}
