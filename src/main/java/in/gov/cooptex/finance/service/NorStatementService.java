package in.gov.cooptex.finance.service;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.core.accounts.model.GlAccount;
import in.gov.cooptex.core.accounts.model.JournalVoucher;
import in.gov.cooptex.core.accounts.model.JournalVoucherEntries;
import in.gov.cooptex.core.accounts.repository.GlAccountRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.enums.ReportNames;
import in.gov.cooptex.core.enums.TemplateCode;
import in.gov.cooptex.core.finance.repository.JournalVoucherEntriesRepository;
import in.gov.cooptex.core.finance.repository.JournalVoucherRepository;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.TemplateDetails;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.AppQueryRepository;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.TemplateDetailsRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.util.JdbcUtil;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.VelocityStringUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.finance.dto.NorStatementDTO;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class NorStatementService {

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	TemplateDetailsRepository templateDetailsRepository;

	@Autowired
	AppQueryRepository appQueryRepository;

	@Autowired
	LoginService loginService;

	@Autowired
	private EntityMasterRepository entityMasterRepository;

	@Autowired
	private GlAccountRepository glAccountRepository;

	@Autowired
	private UserMasterRepository userMasterRepository;

	@Autowired
	private JournalVoucherRepository journalVoucherRepository;

	@Autowired
	private JournalVoucherEntriesRepository journalVoucherEntriesRepository;

	// @Async
	public synchronized BaseDTO generateNorStatement(final NorStatementDTO norStatementDTO) {
		log.info("Inside NorStatementService. generateNorStatement() - STARTED");
		BaseDTO baseDTO = new BaseDTO();
		UserMaster currentUser = null;
		Long userId = null;
		String errorMessage = null;
		Integer month = null;
		Integer year = null;
		try {

			month = norStatementDTO.getMonth();
			year = norStatementDTO.getYear();

			Long loginEntityId = norStatementDTO.getLoginEntityId();

			if (month == null) {
				baseDTO.setStatusCode(ErrorDescription.EMP_PROMOTION_MONTH_IS_MANDATORY.getCode());
				return baseDTO;
			}

			if (year == null) {
				baseDTO.setStatusCode(ErrorDescription.EMP_PROMOTION_YEAR_IS_MANDATORY.getCode());
				return baseDTO;
			}

			if (loginEntityId == null) {
				baseDTO.setStatusCode(
						ErrorDescription.getError(OperationErrorCode.EMPLOYEE_WORKlOCATION_EMPTY).getErrorCode());
				return baseDTO;
			}

			currentUser = loginService.getCurrentUser();

			if (currentUser == null || currentUser.getId() == null) {
				baseDTO.setStatusCode(ErrorDescription.ERROR_EMPTY_LOGIN_DETAILS.getCode());
				return baseDTO;
			}

			userId = currentUser.getId();

			List<String> checkNorProcessStatusList = checkNorProcessStatus(loginEntityId, month, year);

			if (checkNorProcessStatusList == null) {

				log.info("NorStatementService. generateNorStatement() - checkNorProcessStatusList IS NULL");
				updateNorProcessStatus(loginEntityId, month, year, userId);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
				baseDTO.setErrorDescription(
						"NOR Statement generation process started. Please check NOR statement after some time.");
			} else {

				log.info("NorStatementService. generateNorStatement() - checkNorProcessStatusList NOT NULL");

				String message = null;
				String status = checkNorProcessStatusList.get(0);
				status = status == null ? "" : status.trim();
				String error = checkNorProcessStatusList.get(1);
				error = error == null ? "" : error.trim();
				String pdfFilePath = checkNorProcessStatusList.get(2);
				pdfFilePath = pdfFilePath == null ? "" : pdfFilePath.trim();

				String createdBy = checkNorProcessStatusList.get(3);
				Long logCreatedBy = createdBy != null ? Long.valueOf(createdBy) : null;
				logCreatedBy = logCreatedBy != null ? logCreatedBy.longValue() : 0L;

				if ("REPORT-COMPLETED".equals(status)) {
					norStatementDTO.setPdfFilePath(pdfFilePath);
				}

				//
				log.info("generateNorStatement()-status: " + status);
				log.info("generateNorStatement()-error: " + error);
				if (StringUtils.isNotEmpty(status)) {
					if ("IN-PROGRESS".equals(status) || "INITIATED".equals(status)) {
						message = "NOR Statement Generation process already started. Please check NOR statement after some time.";

					} else if ("STATEMENT-COMPLETED".equals(status) || "REPORT-COMPLETED".equals(status)) {

						message = "NOR Statement completed for this month and year.";
						baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
						baseDTO.setErrorDescription(
								"NOR Statement Generation process started. Please check after some time.");

						if ("RE-GENERATE".equals(norStatementDTO.getAction())) {
							updateNorProcessStatus(loginEntityId, month, year, userId);
							norStatementDTO.setNorStatementList(new ArrayList<>());
							norStatementDTO.setPdfFilePath(null);
						} else {
							loadNorStatementReport(loginEntityId, month, year, norStatementDTO);
							String regenerateMessage = null;
							if (logCreatedBy.longValue() == userId.longValue()) {
								regenerateMessage = "Are you sure you want to re-generate now?";
							} else {
								regenerateMessage = "NOR Report for this year and month has been generated already by an another user. Do you want to re-generate now. While re-generating existing report will be removed. Please click 'Yes' to regenerate. ";
							}
							norStatementDTO.setMessage(regenerateMessage);
						}
					} else if ("STATEMENT-NOT-COMPLETED".equals(status)) {
						if (StringUtils.isNotEmpty(error)) {
							message = error;
						} else {
							message = "NOR Generation process not completed for this Month: " + month + " and Year: "
									+ year;
						}
					} else if ("REPORT-NOT-COMPLETED".equals(status)) {
						if (StringUtils.isNotEmpty(error)) {
							message = error;
						} else {
							message = "NOR Pdf Generation process not completed for this Month: " + month
									+ " and Year: " + year;
						}
					} else {
						message = status;
					}
				} else {
					message = "Status Unknown. Please contact support.";
				}
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
				baseDTO.setErrorDescription(message);
				baseDTO.setMessage(status);
			}

			baseDTO.setResponseContent(norStatementDTO);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());

		} catch (RestException ex) {
			errorMessage = ex.getMessage();
		} catch (Exception ex) {
			errorMessage = "Unknown error. NOR process cannot be generated. Please contact support. Month: " + month
					+ " and Year: " + year;
			log.error("Exception at initiateQrCodeProcessScheduler()", ex);
		}

		if (StringUtils.isNotEmpty(errorMessage)) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			baseDTO.setErrorDescription(errorMessage);
		}

		log.info("NorStatementService. generateNorStatement() COMPLETED");
		return baseDTO;
	}

	private synchronized void updateNorProcessStatus(Long loginEntityId, Integer month, Integer year, Long userId)
			throws Exception {
		log.info("NorStatementService. updateNorProcessStatus() STARTED");

		String SELECT_NOR_STATUS_SQL = "SELECT COUNT(*) AS count FROM nor_process_log WHERE entity_id=? AND month=? AND year=?";
		Object count = JdbcUtil.getValue(jdbcTemplate, SELECT_NOR_STATUS_SQL,
				new Object[] { loginEntityId, month, year }, "count");

		if (count != null) {

			Integer countInt = Integer.parseInt(count.toString());
			countInt = countInt != null ? countInt.intValue() : 0;

			if (countInt == 0) {

				String INSERT_NOR_PROCESS_STATUS_SQL = "INSERT INTO nor_process_log (entity_id,month,year,nor_process_status,nor_process_error,nor_process_start_time,created_by,created_date,version) VALUES(?,?,?,?,NULL,NOW(),?,NOW(),0)";

				jdbcTemplate.update(INSERT_NOR_PROCESS_STATUS_SQL,
						new Object[] { loginEntityId, month, year, "INITIATED", userId });
			} else {

				String UPDATE_NOR_PROCESS_STATUS_SQL = "UPDATE nor_process_log SET nor_process_status=?, nor_process_error=?, pdf_file_path=NULL, created_by=? WHERE entity_id=? AND month=? AND year=?";

				jdbcTemplate.update(UPDATE_NOR_PROCESS_STATUS_SQL,
						new Object[] { "INITIATED", null, userId, loginEntityId, month, year });
			}
		}
		log.info("NorStatementService.updateNorProcessStatus() COMPLETED");
	}

	private void loadNorStatementReport(Long regionId, Integer month, Integer year, NorStatementDTO norStatementDTO) {

		List<NorStatementDTO> norStatementList = null;

		ApplicationQuery applicationQuery = applicationQueryRepository.findByQueryName("GENERATE_NOR_STATEMENT_QUERY");
		if (applicationQuery == null || applicationQuery.getId() == null) {
			throw new RestException("Application query is not found for query name: GENERATE_NOR_STATEMENT_QUERY");
		}

		String startDateStr = new StringBuilder().append(year).append("-").append(month < 10 ? "0" + month : month)
				.append("-").append("01").toString();
		java.util.Date dte = AppUtil.formatDateStringAsDate(startDateStr, AppUtil.DATE_FORMAT_YYYY_MM_DD);
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(AppUtil.DATE_FORMAT_YYYY_MM_DD);
		String endDateStr = simpleDateFormat.format(AppUtil.getLastDateOfMonth(dte));

		String queryContent = applicationQuery.getQueryContent() == null ? ""
				: applicationQuery.getQueryContent().trim();
		queryContent = StringUtils.replace(queryContent, "${fromDate}", startDateStr);
		queryContent = StringUtils.replace(queryContent, "${toDate}", endDateStr);
		queryContent = StringUtils.replace(queryContent, "${entityId}", String.valueOf(regionId));
		log.info("NorStatementService. generateNorStatement() - Query: " + queryContent);

		List<Map<String, Object>> dataMapList = jdbcTemplate.queryForList(queryContent);
		int dataMapListSize = dataMapList != null ? dataMapList.size() : 0;
		log.info("NorStatementService. generateNorStatement() - dataMapListSize: " + dataMapListSize);

		if (dataMapListSize > 0) {

			norStatementList = new ArrayList<>();

			for (Map<String, Object> dataMap : dataMapList) {
				NorStatementDTO norStatementDTOObj = new NorStatementDTO();
				norStatementDTOObj.setCode(convertObjectToString(dataMap.get("Code")));
				norStatementDTOObj.setHeadCode(convertObjectToString(dataMap.get("Head of Account")));

				BigDecimal debitAmtBigDecimal = (BigDecimal) dataMap.get("Debit");
				Double debitAmount = debitAmtBigDecimal != null ? debitAmtBigDecimal.doubleValue() : 0D;
				debitAmount = debitAmount != null ? debitAmount : 0D;

				BigDecimal creditAmtBigDecimal = (BigDecimal) dataMap.get("Credit");
				Double creditAmount = creditAmtBigDecimal != null ? creditAmtBigDecimal.doubleValue() : 0D;
				creditAmount = creditAmount != null ? creditAmount : 0D;

				norStatementDTOObj.setDebitAmount(debitAmount);
				norStatementDTOObj.setCreditAmount(creditAmount);
				norStatementDTOObj.setDebitAmountStr(AppUtil.formatIndianCommaSeparated(debitAmount));
				norStatementDTOObj.setCreditAmountStr(AppUtil.formatIndianCommaSeparated(creditAmount));
				norStatementList.add(norStatementDTOObj);
			}

			norStatementDTO.setNorStatementList(norStatementList);
			int norStatementListSize = norStatementList != null ? norStatementList.size() : 0;
			if (norStatementListSize > 0) {
				Double totalDebitAmount = norStatementList.stream().filter(o -> o.getDebitAmount() != null)
						.collect(Collectors.summingDouble(NorStatementDTO::getDebitAmount));
				Double totalCreditAmount = norStatementList.stream().filter(o -> o.getCreditAmount() != null)
						.collect(Collectors.summingDouble(NorStatementDTO::getCreditAmount));
				norStatementDTO.setTotalDebitAmount(AppUtil.formatIndianCommaSeparated(totalDebitAmount));
				norStatementDTO.setTotalCreditAmount(AppUtil.formatIndianCommaSeparated(totalCreditAmount));
			}

		}
	}

	/**
	 * 
	 * @param loginEntityId
	 * @param month
	 * @param year
	 * @return
	 */
	private List<String> getNorProcessStatus(Long loginEntityId, Integer month, Integer year) {
		log.info("Inside NorStatementService. getNorProcessStatus() - STARTED");

		List<String> processStatusList = null;

		final String GET_NOR_PROCESS_STATUS_SQL = "SELECT nor_process_status, nor_process_error, pdf_file_path, created_by FROM nor_process_log WHERE entity_id=? AND month=? AND year=?";
		List<Map<String, Object>> statusList = jdbcTemplate.queryForList(GET_NOR_PROCESS_STATUS_SQL,
				new Object[] { loginEntityId, month, year });
		int statusListSize = statusList != null ? statusList.size() : 0;
		if (statusListSize > 0) {
			Map<String, Object> dataMap = statusList.get(0);

			String processStatus = dataMap.get("nor_process_status") != null
					? dataMap.get("nor_process_status").toString()
					: null;
			String processError = dataMap.get("nor_process_error") != null ? dataMap.get("nor_process_error").toString()
					: "NA";
			String pdfFilePath = dataMap.get("pdf_file_path") != null ? dataMap.get("pdf_file_path").toString() : null;
			String createdBy = dataMap.get("created_by") != null ? dataMap.get("created_by").toString() : null;

			if (StringUtils.isNotEmpty(processStatus)) {
				processStatusList = new ArrayList<>();
				processStatusList.add(processStatus);
				processStatusList.add(processError);
				processStatusList.add(pdfFilePath);
				processStatusList.add(createdBy);
			}
		}
		log.info("NorStatementService. getNorProcessStatus() - COMPLETED");
		return processStatusList;
	}

	/**
	 * 
	 * @param loginEntityId
	 * @param month
	 * @param year
	 * @return
	 */
	private synchronized List<String> checkNorProcessStatus(Long loginEntityId, Integer month, Integer year) {
		log.info("Inside NorStatementService. checkNorProcessStatus() - STARTED");
		List<String> processStatusList = null;
		try {

			processStatusList = getNorProcessStatus(loginEntityId, month, year);

		} catch (Exception e) {
			log.error("Exception at checkNorProcessStatus() ", e);
		}
		log.info("NorStatementService. checkNorProcessStatus() COMPLETED");
		return processStatusList;
	}

	/**
	 * 
	 * @param obj
	 * @return
	 */
	private String convertObjectToString(Object obj) {
		String value = "";
		try {
			value = obj != null ? String.valueOf(obj) : "";
		} catch (Exception e) {
			log.error("Exception at NorStatementService.convertObjectToString()", e);
		}
		return value;
	}

	/**
	 * 
	 * @param norStatementDTO
	 * @return
	 */
	public BaseDTO getNorStatementStatus(NorStatementDTO norStatementDTO) {
		BaseDTO baseDTO = new BaseDTO();
		Integer month = null;
		Integer year = null;
		String status = null;
		try {
			month = norStatementDTO.getMonth();
			year = norStatementDTO.getYear();

			Long loginEntityId = norStatementDTO.getLoginEntityId();

			if (month == null) {
				baseDTO.setStatusCode(ErrorDescription.EMP_PROMOTION_MONTH_IS_MANDATORY.getCode());
				return baseDTO;
			}

			if (year == null) {
				baseDTO.setStatusCode(ErrorDescription.EMP_PROMOTION_YEAR_IS_MANDATORY.getCode());
				return baseDTO;
			}

			if (loginEntityId == null) {
				baseDTO.setStatusCode(
						ErrorDescription.getError(OperationErrorCode.EMPLOYEE_WORKlOCATION_EMPTY).getErrorCode());
				return baseDTO;
			}

			List<String> statusList = getNorProcessStatus(loginEntityId, month, year);
			int statusListSize = statusList != null ? statusList.size() : 0;
			if (statusListSize > 0) {
				status = statusList.get(0);
			}
			baseDTO.setMessage(status);
if(norStatementDTO.getAction()!=null)
{
			if (status.equalsIgnoreCase("REPORT-COMPLETED")
					&& (norStatementDTO.getAction().equalsIgnoreCase("GENERATE")||norStatementDTO.getAction().equalsIgnoreCase("RE-GENERATE"))) {
				if (norStatementDTO.getNorStatementList().size() == 0) {
					loadNorStatementReport(loginEntityId, month, year, norStatementDTO);
					baseDTO.setResponseContent(norStatementDTO);
				}
			}
}
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.error("Exception at NorStatementService. getNorStatementStatus()", e);
		}
		return baseDTO;
	}

	public BaseDTO saveNorStatement(NorStatementDTO norStatementDTO) {
		BaseDTO baseDTO = new BaseDTO();
		log.info("NorStatementService saveNorStatement Method Start");
		try {
			JournalVoucher journalVoucher = new JournalVoucher();
			journalVoucher = journalVoucherRepository.getByMonthandYear(norStatementDTO.getLoginEntityId(),
					norStatementDTO.getMonth(), norStatementDTO.getYear());
			log.info(" saveNorStatement ID : " + journalVoucher.getId());
			log.info(" saveNorStatement AMOUNT : " + journalVoucher.getJvAmount());
			if (norStatementDTO != null && journalVoucher == null) {
				String jvReferenceNumber = "";
				Date jvDate = getMonthEndDate(norStatementDTO.getYear(), norStatementDTO.getMonth());
				EntityMaster entityMaster = entityMasterRepository.findId(norStatementDTO.getLoginEntityId());
				String name = "NOR";

				/* journal voucher */

				if (entityMaster != null && entityMaster.getId() != null) {
					journalVoucher.setEntityMaster(entityMaster);
				}
				journalVoucher.setJvDate(jvDate);
				journalVoucher.setName(name);
				Double jvCreditAmount = norStatementDTO.getNorStatementList().stream()
						.filter(o -> o.getCreditAmount() != null)
						.collect(Collectors.summingDouble(NorStatementDTO::getCreditAmount));
				Double jvDebitAmount = norStatementDTO.getNorStatementList().stream()
						.filter(o -> o.getDebitAmount() != null)
						.collect(Collectors.summingDouble(NorStatementDTO::getDebitAmount));
				Double jvAmount = jvCreditAmount + jvDebitAmount;
				journalVoucher.setJvAmount(jvAmount);
				journalVoucher.setCreatedDate(new Date());
				UserMaster userMaster = userMasterRepository.findOne(loginService.getCurrentUser().getId());
				journalVoucher.setCreatedBy(userMaster);
				jvReferenceNumber = generateJvReferenceNumber(entityMaster.getCode());
				if (jvReferenceNumber != null) {
					String refNumber = jvReferenceNumber.split("#")[0];
					journalVoucher.setJvNumberPrefix(refNumber);
					journalVoucher.setRefNumber(refNumber);
					Long jvNumber = Long.valueOf(jvReferenceNumber.split("#")[1]);
					journalVoucher.setJvNumber(jvNumber);
				}
				log.info(" journalVoucher : " + journalVoucher);
				JournalVoucher journalVouchersave = journalVoucherRepository.save(journalVoucher);
				/* Journal Voucher Entries */
				for (NorStatementDTO nordtoobj : norStatementDTO.getNorStatementList()) {
					JournalVoucherEntries journalVoucherEntries = new JournalVoucherEntries();
					UserMaster userMasterObj = userMasterRepository.findOne(loginService.getCurrentUser().getId());
					journalVoucherEntries.setCreatedBy(userMasterObj);
					journalVoucherEntries.setCreatedDate(new Date());
					GlAccount glAccount = glAccountRepository.findByCode(nordtoobj.getCode());
					journalVoucherEntries.setGlAccount(glAccount);
					journalVoucherEntries.setJournalVoucher(journalVouchersave);
					if (nordtoobj.getCreditAmount() > 0 && nordtoobj.getCreditAmount() != null) {
						journalVoucherEntries.setJvAspect("CREDIT");
						journalVoucherEntries.setJvAmount(nordtoobj.getCreditAmount());
						journalVoucherEntriesRepository.save(journalVoucherEntries);
					}

					if (nordtoobj.getDebitAmount() > 0 && nordtoobj.getDebitAmount() != null) {
						journalVoucherEntries.setJvAspect("DEBIT");
						journalVoucherEntries.setJvAmount(nordtoobj.getDebitAmount());
						journalVoucherEntriesRepository.save(journalVoucherEntries);
					}

				}

				// GlAccount
				// glAccount=glAccountRepository.findByCode(norStatementDTO.getCode());
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {

				/* Journal Voucher Update */
				Double jvCreditAmount = norStatementDTO.getNorStatementList().stream()
						.filter(o -> o.getCreditAmount() != null)
						.collect(Collectors.summingDouble(NorStatementDTO::getCreditAmount));
				Double jvDebitAmount = norStatementDTO.getNorStatementList().stream()
						.filter(o -> o.getDebitAmount() != null)
						.collect(Collectors.summingDouble(NorStatementDTO::getDebitAmount));
				Double jvAmount = jvCreditAmount + jvDebitAmount;
				journalVoucher.setJvAmount(jvAmount);
				journalVoucherRepository.save(journalVoucher);

				log.info(" saveNorStatement AMOUNT LATEST : " + journalVoucher.getJvAmount());
				log.info("<==========NorStatement Already Submitted for this Month : " + norStatementDTO.getMonth()
						+ " and Year : " + norStatementDTO.getYear() + " ===========> ");
				/* Journal Voucher Entries */
				List<JournalVoucherEntries> journalVoucherEntriesList = journalVoucherEntriesRepository
						.findJournalVoucherEntriesListByJV(journalVoucher.getId());
				for (NorStatementDTO nordtoobj : norStatementDTO.getNorStatementList()) {

					JournalVoucherEntries jveCreditObj = journalVoucherEntriesList.stream()
							.filter(o -> o.getGlAccount().getCode().equalsIgnoreCase(nordtoobj.getCode())
									&& o.getJvAspect().equalsIgnoreCase("CREDIT"))
							.findAny().orElse(null);

					JournalVoucherEntries jveDebitObj = journalVoucherEntriesList.stream()
							.filter(o -> o.getGlAccount().getCode().equalsIgnoreCase(nordtoobj.getCode())
									&& o.getJvAspect().equalsIgnoreCase("DEBIT"))
							.findAny().orElse(null);

					// CREDIT
					if (jveCreditObj != null) {
						if (nordtoobj.getCreditAmount() > 0 && nordtoobj.getCreditAmount() != null) {
							jveCreditObj.setJvAspect("CREDIT");
							jveCreditObj.setJvAmount(nordtoobj.getCreditAmount());
							journalVoucherEntriesRepository.save(jveCreditObj);
						}
					} else {
						if (nordtoobj.getCreditAmount() > 0 && nordtoobj.getCreditAmount() != null) {
							JournalVoucherEntries journalVoucherEntries = new JournalVoucherEntries();
							UserMaster userMasterObj = userMasterRepository
									.findOne(loginService.getCurrentUser().getId());
							journalVoucherEntries.setCreatedBy(userMasterObj);
							journalVoucherEntries.setCreatedDate(new Date());
							journalVoucherEntries.setGlAccount(jveCreditObj.getGlAccount());
							journalVoucherEntries.setJvAspect("CREDIT");
							journalVoucherEntries.setJvAmount(nordtoobj.getCreditAmount());
							journalVoucherEntries.setJournalVoucher(journalVoucher);
							journalVoucherEntriesRepository.save(journalVoucherEntries);
						}

					}

					// DEBIT
					if (jveDebitObj != null) {
						if (nordtoobj.getDebitAmount() > 0 && nordtoobj.getDebitAmount() != null) {
							jveDebitObj.setJvAspect("DEBIT");
							jveDebitObj.setJvAmount(nordtoobj.getDebitAmount());
							journalVoucherEntriesRepository.save(jveDebitObj);
						}
					} else {
						if (nordtoobj.getDebitAmount() > 0 && nordtoobj.getDebitAmount() != null) {
							JournalVoucherEntries journalVoucherEntries = new JournalVoucherEntries();
							UserMaster userMasterObj = userMasterRepository
									.findOne(loginService.getCurrentUser().getId());
							journalVoucherEntries.setCreatedBy(userMasterObj);
							journalVoucherEntries.setCreatedDate(new Date());
							GlAccount glAccount = glAccountRepository.findByCode(nordtoobj.getCode());
							journalVoucherEntries.setGlAccount(glAccount);
							journalVoucherEntries.setJvAspect("DEBIT");
							journalVoucherEntries.setJvAmount(nordtoobj.getDebitAmount());
							journalVoucherEntries.setJournalVoucher(journalVoucher);
							journalVoucherEntriesRepository.save(journalVoucherEntries);
						}
					}

				}
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			}
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Exception at NorStatementService saveNorStatement Method : ", e);
		}
		log.info("NorStatementService saveNorStatement Method End");
		return baseDTO;
	}

	private Date getMonthEndDate(Integer year, Integer month) throws Exception {
		Date monthEndDate = null;
		log.info(" Month : " + month + " Year : " + year);
		if (year != 0 && month != 0 && month > 0 && month < 13 && year != null && month != null) {
			YearMonth yearMonth = YearMonth.of(year, month);
			LocalDate last = yearMonth.atEndOfMonth();
			System.out.println(last);
			ZoneId defaultZoneId = ZoneId.systemDefault();
			monthEndDate = Date.from(last.atStartOfDay(defaultZoneId).toInstant());
			System.out.println(" MonthEndDate : " + monthEndDate);
		} else {
			throw new Exception("Invalid Date Format!!");
		}
		return monthEndDate;
	}

	private String generateJvReferenceNumber(Integer entityCode) {
		String referenceNumber = null;

		referenceNumber = entityCode + "-" + "NORJV-" + AppUtil.getCurrentMonthNumber()
				+ AppUtil.getCurrentYearString();
		log.info(" Reference : " + referenceNumber);
		String sql = "select coalesce(max(jv.jv_number),0) as count from journal_voucher jv where date_part('Year',jv.created_date)=date_part('Year',current_date) and jv.name='NOR';";
		Integer maxValue = null;
		Map<String, Object> data = jdbcTemplate.queryForMap(sql);
		if (data != null) {
			if (data.get("count") != null) {
				maxValue = Integer.valueOf(data.get("count").toString());
				log.info(" maxValue : " + maxValue);
				maxValue = maxValue + 1;
			}
		}
		referenceNumber = referenceNumber + String.valueOf(maxValue) + "#" + String.valueOf(maxValue);
		log.info(" Main - Reference : " + referenceNumber);
		return referenceNumber;
	}

	public BaseDTO checkWetherAlreadySubmitorNot(NorStatementDTO norStatementDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			JournalVoucher journalVoucher = new JournalVoucher();
			log.info(" Month : " + norStatementDTO.getMonth());
			log.info(" Year : " + norStatementDTO.getYear());
			log.info(" Entity : " + norStatementDTO.getLoginEntityId());
			journalVoucher = journalVoucherRepository.getByMonthandYear(norStatementDTO.getLoginEntityId(),
					norStatementDTO.getMonth(), norStatementDTO.getYear());

			if (journalVoucher != null) {
				baseDTO.setResponseContent(journalVoucher);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
				log.info(" Already Exist : " + journalVoucher.getId());
			} else {
				log.info(" NO_RECORD_FOUND");
				baseDTO.setStatusCode(ErrorDescription.NO_RECORD_FOUND.getCode());
			}
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Exception at NorStatementService saveNorStatement Method : ", e);
		}

		return baseDTO;
	}

}
