package in.gov.cooptex.finance.service;

import in.gov.cooptex.common.service.EntityMasterService;
import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.service.NotificationEmailService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.model.AmountTransfer;
import in.gov.cooptex.core.accounts.model.AmountTransferDetails;
import in.gov.cooptex.core.accounts.model.AmountTransferLog;
import in.gov.cooptex.core.accounts.model.AmountTransferNote;
import in.gov.cooptex.core.accounts.model.EntityBankBranch;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.VoucherDetails;
import in.gov.cooptex.core.accounts.model.VoucherType;
import in.gov.cooptex.core.accounts.repository.EntityBankBranchRepository;
import in.gov.cooptex.core.accounts.repository.VoucherDetailsRepository;
import in.gov.cooptex.core.accounts.repository.VoucherRepository;
import in.gov.cooptex.core.accounts.repository.VoucherTypeRepository;
import in.gov.cooptex.core.accounts.util.VoucherTypeCons;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.SequenceName;
import in.gov.cooptex.core.finance.service.OperationService;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.SequenceConfig;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.AmountTransferDetailsRepository;
import in.gov.cooptex.core.repository.AmountTransferRepository;
import in.gov.cooptex.core.repository.AppQueryRepository;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.SequenceConfigRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AmountMovementType;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.LedgerPostingException;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.finance.*;
import in.gov.cooptex.finance.repository.AmountTransferLogRepository;
import in.gov.cooptex.finance.repository.AmountTransferNoteRepository;
import lombok.extern.log4j.Log4j2;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Log4j2
@Service
public class BankToCashService {

	@Autowired
	private EntityManager em;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private NotificationEmailService notificationEmailService;

	@Autowired
	private AmountTransferRepository amountTransferRepository;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	@Autowired
	private AmountTransferDetailsRepository amountTransferDetailsRepository;

	@Autowired
	private AmountTransferLogRepository amountTransferLogRepository;

	@Autowired
	private AmountTransferNoteRepository amountTransferNoteRepository;

	@Autowired
	private EntityMasterRepository entityMasterRepository;

	@Autowired
	private UserMasterRepository userMasterRepository;

	@Autowired
	EmployeeMasterRepository employeeMasterRepository;

	@Autowired
	EntityBankBranchRepository entityBankBranchRepository;

	@Autowired
	EntityMasterService entityMasterService;

	@Autowired
	LoginService loginService;

	@Autowired
	OperationService operationService;

	@Autowired
	ResponseWrapper responseWrapper;

	private String BANK_TO_CASH = AmountMovementType.BANK_TO_CASH;

	@Autowired
	SequenceConfigRepository sequenceConfigRepository;

	@Autowired
	VoucherTypeRepository voucherTypeRepository;

	@Autowired
	VoucherRepository voucherRepository;

	@Autowired
	VoucherDetailsRepository voucherDetailsRepository;

	@Autowired
	AppQueryRepository appQueryRepository;
	
	
	private static final String ViewUrl="/pages/accounts/viewAmountTransferBankToCash.xhtml?faces-redirect=true";

	public BaseDTO getBankToCashDetails(PaginationDTO request) {
		log.info("<<====   ChequeToBankService ---  getAll ====## STARTS");
		BaseDTO baseDTO = null;
		Integer total = 0;
		List<BankToCashResponseDTO> resultList = null;
		try {
			resultList = new ArrayList<BankToCashResponseDTO>();
			baseDTO = new BaseDTO();
			Integer start = request.getFirst(), pageSize = request.getPageSize();
			start = start * pageSize;

			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();
			ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("BANK_TO_CASH");
			String mainQuery = applicationQuery.getQueryContent();
			log.info("db query----------" + mainQuery);
			String searchcriteria = "";
			if (request.getFilters() != null) {
				if (request.getFilters().get("fromBranch") != null) {
					searchcriteria += " and upper(concat(bm.bank_name)) like upper('%"
							+ request.getFilters().get("fromBranch") + "%')";
				}
				if (request.getFilters().get("status") != null) {
					searchcriteria += " and stage='" + request.getFilters().get("status") + "'";
				}
				if (request.getFilters().get("totalBankTransfer") != null) {

					searchcriteria += " and att.total_amount_transfered >= '"
							+ request.getFilters().get("totalBankTransfer") + "'";
				}
				if (request.getFilters().get("amountTransferBy") != null) {
					searchcriteria += " and upper(concat(emp.first_name,' ',emp.last_name)) like upper('%"
							+ request.getFilters().get("amountTransferBy") + "%')";
				}
				if (request.getFilters().get("createdDate") != null) {
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					Date fromDate = new Date((long) request.getFilters().get("createdDate"));
					log.info(" fromDate Filter : " + fromDate);
					searchcriteria += " and att.created_date::date='" + format.format(fromDate) + "'";
				}

				mainQuery = mainQuery.replace(":searchcriteria", searchcriteria);

			}
//			String countQuery = mainQuery.replace(
//					"SELECT att.id AS id, att.total_amount_transfered AS totalamount,att.created_date AS createddate, atl.stage AS stage,emp.first_name,emp.last_name,bm.bank_name AS frombankName,atn.forward_to as forwardtoId,atn.final_approval",
//					"SELECT count(distinct(att.id)) as count ");
//			countQuery = countQuery.replace(
//					"GROUP BY att.id,att.total_amount_transfered,att.created_date, atl.stage,emp.first_name,emp.last_name,bm.bank_name,atn.forward_to,atn.final_approval",
//					" ");
			String countQuery = "select count(*) from (" + mainQuery + ")t;";

			log.info("count query... " + countQuery);
			countData = jdbcTemplate.queryForList(countQuery);
			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					total = Integer.parseInt(data.get("count").toString().replace(",", ""));
			}
			log.info("total query... " + total + "::pageSize::>>>" + pageSize + "::start::" + start);

			if (request.getSortField() != null && request.getSortOrder() != null) {
				log.info("Sort Field:[" + request.getSortField() + "] Sort Order:[" + request.getSortOrder() + "]");
				if (request.getSortField().equals("fromBranch") && request.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by bm.bank_name asc  ";
				}
				if (request.getSortField().equals("fromBranch") && request.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by bm.bank_name desc  ";
				}
				if (request.getSortField().equals("totalBankTransfer") && request.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by att.total_amount_transfered asc  ";
				}
				if (request.getSortField().equals("totalBankTransfer") && request.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by att.total_amount_transfered desc  ";
				}
				if (request.getSortField().equals("amountTransferBy") && request.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by emp.first_name asc ";
				}
				if (request.getSortField().equals("amountTransferBy") && request.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by emp.first_name desc ";
				}
				if (request.getSortField().equals("createdDate") && request.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by att.created_date asc ";
				}
				if (request.getSortField().equals("createdDate") && request.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by att.created_date desc ";
				}

				if (request.getSortField().equals("status") && request.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by stage asc ";
				}
				if (request.getSortField().equals("status") && request.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by stage desc ";
				}
				mainQuery += " limit " + pageSize + " offset " + start + ";";
			} else {
				mainQuery += " order by id desc limit " + pageSize + " offset " + start + ";";
			}
			log.info("filter query----------" + mainQuery);
			// mainQuery += "order by att.created_date desc";
			listofData = jdbcTemplate.queryForList(mainQuery);
			// totalRecords = listofData.size();
			log.info("amount transfer list size-------------->" + listofData.size());

			for (Map<String, Object> data : listofData) {
				BankToCashResponseDTO response = new BankToCashResponseDTO();
				response.setAmountTransferId(Long.valueOf(data.get("id").toString()));
				response.setTotalBankTransfer(Double.valueOf(data.get("totalamount").toString()));
				response.setAmountTransferBy(
						data.get("first_name").toString() + " " + data.get("last_name").toString());
				response.setStatus(data.get("stage").toString());
				response.setCreatedDate((Date) data.get("createddate"));
				response.setFromBranch(data.get("frombankname").toString());
				if(data.get("forwardtoId")!=null) {
					response.setForwardTO(Long.valueOf(data.get("forwardtoId").toString()));
				}
				if(data.get("final_approval")!=null) {
					response.setFinalApproval(Boolean.valueOf(data.get("final_approval").toString()));
				}
				resultList.add(response);
			}

			log.info("final list size---------------" + resultList.size());
			log.info("Total records present in BankToCash..." + total);
			baseDTO.setTotalRecords(total);
			// baseDTO.setTotalRecords(totalRecords);
			baseDTO.setResponseContents(resultList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("<<====   BanktoCashService ---  getAll ====## ENDS");
		} catch (Exception ex) {
			log.error("inside lazy method exception------");
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO getDetailsById(Long id) {
		log.info("<<====   BankToCashService ---  getDetailsById ====## STARTS");
		BaseDTO baseDTO = new BaseDTO();
		Integer totalRecords = 0;
		Criteria criteria = null;
		try {
			Session session = entityManager.unwrap(Session.class);
			criteria = session.createCriteria(AmountTransferDetails.class, "amountTransferDetails");
			criteria.createAlias("amountTransferDetails.amountTransfer", "amountTransfer");
			// criteria.createAlias("amountTransfer.transferedBy", "transferedBy");
			criteria.createAlias("amountTransferDetails.fromBranch", "fromBranch");
			criteria.createAlias("fromBranch.bankBranchMaster", "bankBranchMaster");
			criteria.createAlias("fromBranch.bankAccountType", "bankAccountType");
			criteria.createAlias("bankBranchMaster.bankMaster", "bankMaster");

			criteria.add(Restrictions.eq("amountTransfer.movementType", AmountMovementType.BANK_TO_CASH.toString()));

			criteria.createAlias("amountTransfer.transferedBy", "empMaster");
			criteria.setProjection(Projections.rowCount());
			totalRecords = ((Long) criteria.uniqueResult()).intValue();
			criteria.setProjection(null);
			ProjectionList projectionList = Projections.projectionList();

			projectionList.add(Projections.property("bankMaster.bankName"));
			// projectionList.add(Projections.property("amountTransferLog.stage"));
			projectionList.add(Projections.property("amountTransfer.id"));
			projectionList.add(Projections.property("amountTransfer.totalAmountTransfered"));
			projectionList.add(Projections.property("empMaster.firstName"));
			projectionList.add(Projections.property("amountTransfer.movementType"));
			projectionList.add(Projections.property("amountTransfer.createdDate"));

			projectionList.add(Projections.property("fromBranch.accountNumber"));
			projectionList.add(Projections.property("bankBranchMaster.branchName"));
			projectionList.add(Projections.property("bankBranchMaster.branchCode"));

			projectionList.add(Projections.property("amountTransferDetails.challanNumber"));
			projectionList.add(Projections.property("amountTransferDetails.challanDate"));
			projectionList.add(Projections.property("amountTransferDetails.challanAmount"));
			projectionList.add(Projections.property("amountTransfer.remarks"));
			projectionList.add(Projections.property("bankAccountType.name"));
			if (id != null) {
				criteria.add(Restrictions.eq("amountTransfer.id", id));
			}

			criteria.setProjection(projectionList);

			List<?> resultList = criteria.list();
			if (resultList == null || resultList.isEmpty() || resultList.size() == 0) {
				log.info("Profile Master List is null or empty ");
				baseDTO.setStatusCode(ErrorDescription.PRODUCT_VARIETY_LIST_EMPTY.getCode());
				return baseDTO;
			}

			log.info("criteria list executed and the list size is  : " + resultList.size());
			List<BankToBankViewResponseDTO> results = new ArrayList<BankToBankViewResponseDTO>();
			List<CashtoBankChallanDTO> challanDTOS = new ArrayList<>();
			Iterator<?> it = resultList.iterator();

			while (it.hasNext()) {
				Object ob[] = (Object[]) it.next();
				// String movement = (String) ob[5];
				// String mv = AmountMovementType.BANK_TO_CASH;
				// if (mv.trim().equals(movement.toLowerCase().trim())) {
				BankInformation frombankInformation = new BankInformation();
				CashtoBankChallanDTO challanDTO = new CashtoBankChallanDTO();
				BankToBankViewResponseDTO response = new BankToBankViewResponseDTO();
				frombankInformation.setBankName((String) ob[0]);
				response.setTotalBankTransfer((Double) ob[2]);
				response.setAmountTransferBy((String) ob[3]);
				// frombankInformation.setAccountType((String)ob[6]);
				frombankInformation.setAccountNumber((String) ob[6]);
				frombankInformation.setBranchName((String) ob[7]);
				frombankInformation.setBranchCode((String) ob[8]);
				challanDTO.setChallanNumber((String) ob[9]);
				challanDTO.setChallanDate((Date) ob[10]);
				challanDTO.setChallanAmount((Double) ob[11]);
				response.setRemarks((String) ob[12]);
				frombankInformation.setAccountType((String) ob[13]);
				response.setSourceBankInformation(frombankInformation);
				challanDTOS.add(challanDTO);
				AmountTransferNote amountTransferNote=amountTransferNoteRepository.findByAmountTransferId(id);
				if(amountTransferNote!=null) {
					if(amountTransferNote.getNote()!=null && amountTransferNote.getNote().trim().length()>0) {
						response.setNote(amountTransferNote.getNote());
					}	
				}
				
				
				results.add(response);

				// }
				for (BankToBankViewResponseDTO btobv : results) {
					btobv.setAmountTransferDetails(challanDTOS);
				}
			}
			AmountTransfer amountTransfer = amountTransferRepository.findOne(id);
			List<Map<String, Object>> employeeData = new ArrayList<Map<String, Object>>();
			if (amountTransfer != null) {
				log.info("<<<:::::::amountTransfer::::not Null::::>>>>" + amountTransfer);
				ApplicationQuery applicationQueryForlog = applicationQueryRepository
						.findByQueryName("AMOUNT_TRANSFER_LOG_EMPLOYEE_DETAILS");
				if (applicationQueryForlog == null || applicationQueryForlog.getId() == null) {
					log.info("Application Query For Log Details not found for query name : " + applicationQueryForlog);
					baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
					return baseDTO;
				}
				String logquery = applicationQueryForlog.getQueryContent().trim();
				log.info("<=========AMOUNT_TRANSFER_LOG_EMPLOYEE_DETAILS Query Content ======>" + logquery);
				logquery = logquery.replace(":amounttransId", "'" + amountTransfer.getId().toString() + "'");
				log.info("Query Content For AMOUNT_TRANSFER_LOG_EMPLOYEE_DETAILS After replaced plan id View query : "
						+ logquery);
				employeeData = jdbcTemplate.queryForList(logquery);
				log.info("<=========AMOUNT_TRANSFER_LOG_EMPLOYEE_DETAILS Employee Data======>" + employeeData);
				baseDTO.setTotalListOfData(employeeData);
			}

			baseDTO.setResponseContents(results);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			baseDTO.setTotalRecords(results.size());
		} catch (Exception ex) {
			log.error("inside getBanktoCashDetailsById method exception------", ex);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("getBanktoCashDetailsById method ends-------");
		return baseDTO;
	}

	@Transactional(rollbackFor = Throwable.class)
	public BaseDTO save(BankToCashSaveRequestDTO requestDTO) {
		log.info("<==== Starts BankToCashService .save =====>");
		BaseDTO response = new BaseDTO();
		try {
			log.info("Request DTO : " + requestDTO);

			EntityMaster entityMaster = entityMasterRepository
					.findOne(requestDTO.getAmountTransfer().getEntityMaster().getId());
			if (entityMaster == null) {
				throw new RestException(
						"EntityMaster not found for id : " + requestDTO.getAmountTransfer().getEntityMaster().getId());
			}
			requestDTO.getAmountTransfer().setEntityMaster(entityMaster);

			AmountTransfer amountTransfer = amountTransferRepository.save(requestDTO.getAmountTransfer());
			if (amountTransfer == null) {
				throw new RestException("Could Not able to save AmountTransfer : " + requestDTO.getAmountTransfer());
			}

			requestDTO.getAmountTransferDetails().setAmountTransfer(amountTransfer);
			AmountTransferDetails amountTransferDetails = amountTransferDetailsRepository
					.save(requestDTO.getAmountTransferDetails());
			if (amountTransferDetails == null) {
				throw new RestException(
						"Could Not able to save AmountTransferDetails : " + requestDTO.getAmountTransfer());
			}

		/*	requestDTO.getAmountTransferLog().setAmountTransfer(amountTransfer);
			AmountTransferLog amountTransferLog = amountTransferLogRepository.save(requestDTO.getAmountTransferLog());
			if (amountTransferLog == null) {
				throw new RestException("Could Not able to save AmountTransferLog : " + requestDTO.getAmountTransfer());
			}*/

			/*UserMaster userMaster = userMasterRepository
					.findOne(requestDTO.getAmountTransferNote().getForwardTo().getId());
			if (entityMaster == null) {
				throw new RestException(
						"EntityMaster not found for id : " + requestDTO.getAmountTransfer().getEntityMaster().getId());
			}*/

		/*	requestDTO.getAmountTransferNote().setForwardTo(userMaster);
			requestDTO.getAmountTransferNote()
					.setNote(requestDTO.getAmountTransferNote().getNote() != null
							? requestDTO.getAmountTransferNote().getNote()
							: " ");
			requestDTO.getAmountTransferNote().setAmountTransfer(amountTransfer);
			AmountTransferNote amountTransferNote = amountTransferNoteRepository
					.save(requestDTO.getAmountTransferNote());
			if (amountTransferNote == null) {
				throw new RestException(
						"Could Not able to save AmountTransferNote : " + requestDTO.getAmountTransfer());
			}*/

			
			if (requestDTO.getAmountTransferNote() != null && !requestDTO.getSikpapproval()) {
				AmountTransferNote note = new AmountTransferNote(); 
				UserMaster userMaster = userMasterRepository
						.findOne(requestDTO.getAmountTransferNote().getForwardTo().getId());
				if (entityMaster == null) {
					throw new RestException(
							"EntityMaster not found for id : " + requestDTO.getAmountTransfer().getEntityMaster().getId());
				}
				requestDTO.getAmountTransferNote().setForwardTo(userMaster);
				requestDTO.getAmountTransferNote()
						.setNote(requestDTO.getAmountTransferNote().getNote() != null
								? requestDTO.getAmountTransferNote().getNote()
								: " ");
				requestDTO.getAmountTransferNote().setAmountTransfer(amountTransfer);
				AmountTransferNote amountTransferNote = amountTransferNoteRepository
						.save(requestDTO.getAmountTransferNote());
				if (amountTransferNote == null) {
					throw new RestException(
							"Could Not able to save AmountTransferNote : " + requestDTO.getAmountTransfer());
				}
				log.info("note  ++ " + note);
			}
			if(!requestDTO.getSikpapproval()) {
				requestDTO.getAmountTransferLog().setAmountTransfer(amountTransfer);
				AmountTransferLog amountTransferLog = amountTransferLogRepository.save(requestDTO.getAmountTransferLog());
				if (amountTransferLog == null) {
					throw new RestException("Could Not able to save AmountTransferLog : " + requestDTO.getAmountTransfer());
				}

			}else {
				requestDTO.getAmountTransferLog().setAmountTransfer(amountTransfer);
				AmountTransferLog amountTransferLog = amountTransferLogRepository.save(requestDTO.getAmountTransferLog());
				if (amountTransferLog == null) {
					throw new RestException("Could Not able to save AmountTransferLog : " + requestDTO.getAmountTransfer());
				}
			}
			
			log.info("<-----BankToCashService saved or updated successfully--->");
			response.setMessage("SUCCESS");
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());

		} catch (ObjectOptimisticLockingFailureException lockEx) {
			log.warn("====>> Error while updating department <<====", lockEx);
			response.setStatusCode(ErrorDescription.CANNOT_UPDATE_LOCKED_RECORD.getErrorCode());
		} catch (RestException re) {
			log.error("RestException occured in BankToCashService .save", re);
			response.setStatusCode(re.getStatusCode());
		} catch (Exception e) {
			log.error("Exception occured in BankToCashService .save", e);
			response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<==== Ends BankToCashService.save =====>");
		return response;
	}

	@Transactional
	public BaseDTO addBankToCash(BanktoCashAddDTO data) {
		log.info("<==== Ends BankToCashService.save Function Entered =====>");
		BaseDTO baseDTO = new BaseDTO();
		List<AmountTransferLog> result = new ArrayList<>();
		// List<AmountTransferDetails> amountDeatilsList=new ArrayList<>();
		// List<AmountTransferDetails> amountTransferDetailsList=new ArrayList<>();
		
		try {
			log.info("<==== Ends BankToCashService Excecution Start Function Entered =====>");
			UserMaster user = loginService.getCurrentUser();
			BaseDTO basedto = entityMasterService.getEntityInfoByLoggedInUser(user.getId());
			EntityMaster entity = (EntityMaster) basedto.getResponseContent();
			if (entity != null) {
				data.setEntityId(entity.getId());
			}
			
			AmountTransfer amountTransfer = new AmountTransfer();
			AmountTransferLog amountTransferLog = new AmountTransferLog();
			AmountTransferNote note = new AmountTransferNote();
			if (data != null) {
				EntityMaster em = entityMasterRepository.findById(data.getEntityId());
				EmployeeMaster employeeMaster = employeeMasterRepository.findOne(data.getTransferredBy());
				EntityBankBranch fromBranch = entityBankBranchRepository.findOne(data.getFromAccountId());

				if (data.getTransfereId() != null) {
					amountTransfer = amountTransferRepository.findOne(data.getTransfereId());
				}

				amountTransfer.setEntityMaster(em);
				amountTransfer.setTransferedBy(employeeMaster);
				amountTransfer.setMovementType(AmountMovementType.BANK_TO_CASH);
				amountTransfer.setTotalAmountTransfered(data.getTotalAmountTransfered());
				amountTransfer.setRemarks(data.getRemarks());
				amountTransfer.setVersion(data.getVersion());
				amountTransfer = amountTransferRepository.save(amountTransfer);

				amountTransferDetailsRepository.deletebyamountTransferId(amountTransfer.getId());

				/*
				 * Voucher voucher = new Voucher(); String refNumberPrefix = ""; Long
				 * sequence_value = null;
				 * 
				 * EntityMaster loginUserEntity = entityMasterRepository
				 * .findByUserRegion(loginService.getCurrentUser().getId());
				 * 
				 * voucher.setName(AmountMovementType.BANK_TO_CASH); VoucherType voucherType =
				 * voucherTypeRepository.findByName(VoucherTypeCons.RECEIPT);
				 * voucher.setVoucherType(voucherType);
				 * voucher.setNetAmount(data.getTotalAmountTransfered());
				 * voucher.setNarration(data.getRemarks()); SequenceConfig sequenceConfig =
				 * sequenceConfigRepository
				 * .findBySequenceName(SequenceName.valueOf(SequenceName.
				 * FINANCE_ACCOUNT_BANK_RECEIPT.name())); if (sequenceConfig == null) { throw
				 * new RestException(ErrorDescription.SEQUENCE_CONFIG_NOT_EXIST); } if
				 * (loginUserEntity != null) { voucher.setEntityMaster(loginUserEntity); }
				 * 
				 * refNumberPrefix = loginUserEntity.getCode() + sequenceConfig.getSeparator() +
				 * sequenceConfig.getPrefix() + AppUtil.getCurrentYearString() +
				 * AppUtil.getCurrentMonthString() + sequenceConfig.getCurrentValue();
				 * sequence_value = sequenceConfig.getCurrentValue();
				 * 
				 * sequenceConfig.setCurrentValue(sequence_value + 1);
				 * 
				 * sequenceConfigRepository.save(sequenceConfig);
				 * log.info("Voucher Number Prefix  ", refNumberPrefix);
				 * voucher.setReferenceNumberPrefix(refNumberPrefix);
				 * voucher.setReferenceNumber(sequenceConfig.getCurrentValue());
				 * voucher.setFromDate(new Date()); voucher.setToDate(new Date()); voucher =
				 * voucherRepository.save(voucher);
				 */

				log.info("ChallanaDetails:" + data.getChallanaDetails().size());
				for (BanktoCashAddDetailsDTO btobadddetails : data.getChallanaDetails()) {
					AmountTransferDetails amountTransferDetails = new AmountTransferDetails();
					/*
					 * if(btobadddetails.getAmountTransferDetailId()!=null) { amountTransferDetails=
					 * amountTransferDetailsRepository.findOne(btobadddetails.
					 * getAmountTransferDetailId()); }
					 */
					amountTransferDetails.setAmountTransfer(amountTransfer);
					amountTransferDetails.setFromBranch(fromBranch);
					amountTransferDetails.setChallanNumber(btobadddetails.getChallanNumber());
					amountTransferDetails.setChallanDate(btobadddetails.getChallanDate());
					amountTransferDetails.setChallanAmount(btobadddetails.getChallanAmount());
					amountTransferDetailsRepository.save(amountTransferDetails);
					/*
					 * VoucherDetails voucherDetails = new VoucherDetails();
					 * voucherDetails.setAmount(btobadddetails.getChallanAmount());
					 * voucherDetails.setVoucher(voucher);
					 * 
					 * 
					 * try { amountTransferDetailsRepository.save(amountTransferDetails);
					 * voucherDetailsRepository.save(voucherDetails); }catch(Exception ex) {
					 * log.info("<--Amont Transfer BankToCashDetails Service/ exception :- -->"+ex);
					 * }
					 */
				}
				log.info("getRemoveChallanaDetails:" + data.getRemoveChallanaDetails() != null
						? data.getRemoveChallanaDetails().size()
						: "null");
				if (data.getRemoveChallanaDetails() != null) {
					log.info("RemoveChallanaDetails list Not Null");
					for (BanktoCashAddDetailsDTO btobadddetails : data.getRemoveChallanaDetails()) {

						AmountTransferDetails amountTransferDetails = new AmountTransferDetails();
						try {
							log.info("DeleteBankToCash remove the challanDetails removedId:"
									+ btobadddetails.getAmountTransferDetailId());
							if(btobadddetails.getAmountTransferDetailId()!=null)
							{
							amountTransferDetailsRepository.delete(btobadddetails.getAmountTransferDetailId());
							}
						} catch (Exception ex) {
							log.info("<--Amont Transfer BankToCashDetails Service/ exception :- -->" + ex);
						}
					}
				} else {
					log.info("RemoveChallanaDetails list null");
				}
				if (data.getNote() != null && !data.getSikpapproval()) {
					UserMaster userMaster = userMasterRepository.findOne(data.getForwardTo());
					// if (data.getNote()!=null) {
					
					note.setAmountTransfer(amountTransfer);
					note.setNote(data.getNote() != null ? data.getNote() : " ");
					note.setFinalApproval(data.getForwardFor());
					note.setForwardTo(userMaster);
					try {
						note = amountTransferNoteRepository.save(note);
						log.info("note  ++ " + note);
					} catch (Exception ex) {
						log.info("<--Amont Transfer BankToCashDetails Note Table Save / exception :- -->" + ex);
					}
					// }

				}	
				
				if(data.getSikpapproval())
				{
					UserMaster userMaster = userMasterRepository.findOne(loginService.getCurrentUser().getId());
					note.setAmountTransfer(amountTransfer);
					note.setNote("Self Approved");
					note.setFinalApproval(data.getForwardFor());
					note.setForwardTo(userMaster);
					note = amountTransferNoteRepository.save(note);
				}
				
				if (!data.getSikpapproval()) {
					
					amountTransferLog.setAmountTransfer(amountTransfer);
					amountTransferLog.setStage(AmountMovementType.SUBMITTED);
					if (data.getRemarks() != null) {
						amountTransferLog.setRemarks(data.getRemarks());
					}
					if (data.getVersion() != null) {
						amountTransferLog.setVersion(data.getVersion());
					}
					try {
						amountTransferLog = amountTransferLogRepository.save(amountTransferLog);
					} catch (Exception ex) {
						log.info("<--Amont Transfer BankToCashDetails Log Table Save / exception :- -->" + ex);
					}

//					result.add(amountTransferLog);
				}else {
					
					amountTransferLog.setAmountTransfer(amountTransfer);
					amountTransferLog.setStage(AmountMovementType.FINAL_APPROVED);
					if (data.getRemarks() != null) {
						amountTransferLog.setRemarks(data.getRemarks());
					}
					if (data.getVersion() != null) {
						amountTransferLog.setVersion(data.getVersion());
					}
					try {
						amountTransferLog = amountTransferLogRepository.save(amountTransferLog);
					} catch (Exception ex) {
						log.info("<--Amont Transfer BankToCashDetails Log Table Save / exception :- -->" + ex);
					}
				}
//				baseDTO.setResponseContents(result);
				baseDTO.setTotalRecords(1);

			}
			
			Map<String, Object> additionalData = new HashMap<>();
			additionalData.put("Url", ViewUrl + "&id=" + amountTransfer.getId() + "&");
		
			notificationEmailService.sendMailAndNotificationForForward(note, amountTransferLog,
					additionalData);
			
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			return baseDTO;
		} catch (Exception ex) {
			log.error("<--Amont BankToCash Transfer Service/ exception :- -->" + ex);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			return baseDTO;
		}
	}

	public BaseDTO getBankToCashEditById(Long id) {
		log.info("<---BankToCashService.getBankToBankEditById() starts---->" + id);
		BaseDTO response = new BaseDTO();

		BankToCashSaveRequestDTO bankToCashDTO = new BankToCashSaveRequestDTO();
		try {

			AmountTransfer amounttransfer = amountTransferRepository.findOne(id);
			bankToCashDTO.setAmountTransfer(amounttransfer);
			bankToCashDTO.setAmountTransferLog(amountTransferLogRepository.findByAmountTransferId(id));
			bankToCashDTO.setAmountTransferNote(amountTransferNoteRepository.findByAmountTransferId(id));
			bankToCashDTO.setAmounttransferdetailsList(amountTransferDetailsRepository.getAmountTransferList(id));
			response.setResponseContent(bankToCashDTO);

			// response.setResponseContents(banktobankDTO);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (RestException e) {
			log.error(" Rest Exception Occured ", e);
			response.setStatusCode(e.getStatusCode());
		} catch (Exception e) {
			log.info("Error while retriving getBankToBankEditById----->", e);
			response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<---BankToCashService.getBankToBankEditById() end---->");
		return responseWrapper.send(response);
	}

	public BaseDTO getAllEmployee() {
		log.info("EmployeeService.getAllEmployee() Started ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<EmployeeMaster> employeeList = employeeMasterRepository.getAllEmployee();
			if (employeeList != null && employeeList.size() > 0) {
				baseDTO.setResponseContent(employeeList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				log.info("EmployeeService.getAllEmployee() Completed");
			}
		} catch (Exception exception) {
			log.error("Exception occurred in EmployeeService.getAllEmployee() -:", exception);
			baseDTO.setStatusCode(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO approveBankToCash(BankToCashResponseDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("approveBankToCash method start=============>" + requestDTO.getAmountTransferId());
			AmountTransfer amountTransfer = amountTransferRepository.findOne(requestDTO.getAmountTransferId());

			AmountTransferNote amountTransferNote = new AmountTransferNote();
			amountTransferNote.setAmountTransfer(amountTransfer);
			amountTransferNote.setFinalApproval(requestDTO.getFinalApproval());
			amountTransferNote.setNote(requestDTO.getNote());
			amountTransferNote.setForwardTo(userMasterRepository.findOne(requestDTO.getForwardTO()));
			amountTransferNote.setCreatedBy(loginService.getCurrentUser());
			amountTransferNote.setCreatedByName(loginService.getCurrentUser().getUsername());
			amountTransferNote.setCreatedDate(new Date());
			amountTransferNoteRepository.save(amountTransferNote);
			log.info("approveBankToCash note inserted------");

			AmountTransferLog amountTransferLog = new AmountTransferLog();
			amountTransferLog.setAmountTransfer(amountTransfer);
			amountTransferLog.setStage(requestDTO.getStatus());
			amountTransferLog.setCreatedBy(loginService.getCurrentUser());
			amountTransferLog.setCreatedByName(loginService.getCurrentUser().getUsername());
			amountTransferLog.setCreatedDate(new Date());
			amountTransferLog.setRemarks(requestDTO.getRemarks());
			amountTransferLogRepository.save(amountTransferLog);
			log.info("approveBankToCash log inserted------");

			log.info("Account posting Start--------" + amountTransfer);
			log.info("Approval status----------" + requestDTO.getStatus());

			if (requestDTO.getStatus().equals(AmountMovementType.FINAL_APPROVED)) {
				baseDTO = operationService.amountTransferLedgerPosting(amountTransfer, requestDTO.getStatus());
			}
			log.info("Account posting End--------");
			
			
			
			//Notification
			
			
			
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (LedgerPostingException l) {
			log.error("approveBankToCash method LedgerPostingException----", l);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("approveBankToCash method exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO rejectBankToCash(BankToCashResponseDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("rejectBankToCash method start=============>" + requestDTO.getAmountTransferId());

			AmountTransfer amountTransfer = amountTransferRepository.findOne(requestDTO.getAmountTransferId());

			AmountTransferLog amountTransferLog = new AmountTransferLog();
			amountTransferLog.setAmountTransfer(amountTransfer);
			amountTransferLog.setStage(requestDTO.getStatus());
			amountTransferLog.setCreatedBy(loginService.getCurrentUser());
			amountTransferLog.setCreatedByName(loginService.getCurrentUser().getUsername());
			amountTransferLog.setCreatedDate(new Date());
			amountTransferLog.setRemarks(requestDTO.getRemarks());
			amountTransferLogRepository.save(amountTransferLog);

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("rejectBankToCash  log inserted------");
		} catch (Exception e) {
			log.error("rejectBankToCash method exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}
}
