package in.gov.cooptex.finance.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.model.ChequeBook;
import in.gov.cooptex.core.accounts.model.EntityBankBranch;
import in.gov.cooptex.core.accounts.repository.EntityBankBranchRepository;
import in.gov.cooptex.core.accounts.repository.PaymentDetailsRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ProfitMarginMaster;
import in.gov.cooptex.core.repository.AmountTransferDetailsRepository;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.finance.ChequeBookDetailsDTO;
import in.gov.cooptex.finance.repository.ChequeBookDetailsRepository;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class ChequeBookDetailsService {
	@Autowired
	EntityMasterRepository entityMasterRepository;
	@Autowired
	LoginService loginService;
	@Autowired
	ChequeBookDetailsRepository chequeBookDetailsRepository;
	@Autowired
	EntityBankBranchRepository entityBankBranchRepository;
	@Autowired
	PaymentDetailsRepository paymentDetailsRepository;
	@Autowired
	AmountTransferDetailsRepository amountTransferDetailsRepository;
	@Autowired
	EntityManager entityManager;
	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	@Autowired
	ResponseWrapper responseWrapper;

	@Autowired
	JdbcTemplate jdbcTemplate;

	public BaseDTO getEntity() {
		log.info("====>> ChequeBookDetailsService.getEntity() <<====");
		BaseDTO baseDTO = new BaseDTO();
		try {
			EntityMaster entityMaster = entityMasterRepository.findByUserRegion(loginService.getCurrentUser().getId());
			baseDTO.setResponseContent(entityMaster);
			log.info("..entityMaster.." + entityMaster.getName());
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException re) {
			log.warn("====>> Error while getById <<====", re);
			baseDTO.setStatusCode(re.getStatusCode());
		} catch (Exception e) {
			log.error("====>>  Exception occurred <<====", e);
		}
		log.info("===== BankBranchMasterService Inside getById() =====> End ");
		return baseDTO;
	}

	public BaseDTO createChequeBookDetails(ChequeBookDetailsDTO chequeBookDetailsDTO) {

		log.info("====>> ChequeBookDetailsService.createChequeBookDetails() <<====");
		BaseDTO baseDTO = new BaseDTO();
		try {
			EntityMaster entityMaster = null;
			EntityBankBranch entityBankBranch = null;
			List<ChequeBook> chequeBookList = new ArrayList<>();
			if (chequeBookDetailsDTO.getChequeNumberFrom() != null
					&& chequeBookDetailsDTO.getChequeNumberTo() != null) {
				Long from = chequeBookDetailsDTO.getChequeNumberFrom();
				Long to = chequeBookDetailsDTO.getChequeNumberTo();
				Long count = to - from;

				log.info("Total Leafs: " + count);

				List<ChequeBook> checkDuplicateChequeBook = chequeBookDetailsRepository.findListOfCheckBookRange(
						chequeBookDetailsDTO.getEntityBankBranch().getId(), chequeBookDetailsDTO.getChequeBookNumber(),
						from, to);
				if (checkDuplicateChequeBook != null && checkDuplicateChequeBook.size() > 0) {
					baseDTO.setStatusCode(MastersErrorCode.RANGE_IS_ALREADY_EXIST);
					return baseDTO;
				}
				/*
				 * For example 501500 - 501599
				 */
				if (chequeBookDetailsDTO.getEntityId() != null) {
					entityMaster = entityMasterRepository.findOne(chequeBookDetailsDTO.getEntityId());
				}
				if (chequeBookDetailsDTO.getEntityBankBranch().getId() != null) {
					entityBankBranch = entityBankBranchRepository
							.findOne(chequeBookDetailsDTO.getEntityBankBranch().getId());
				}
				for (Long i = from ; i <= to; i++) {
					ChequeBook chequeBook = new ChequeBook();
					chequeBook.setChequeNumber(i);
					/*
					 * if (i == 0) {
					 * 
					 * chequeBook.setChequeNumber(from); } else if (i == count) {
					 * 
					 * chequeBook.setChequeNumber(to); } else {
					 * 
					 * chequeBook.setChequeNumber(from + i); }
					 */

					log.info(chequeBookDetailsDTO.getEntityId());
					chequeBook.setEntityMaster(entityMaster);
					log.info("===Id====" + chequeBookDetailsDTO.getId());
					log.info("==AccountNumber===" + chequeBookDetailsDTO.getAccountNumber());
					chequeBook.setEntityBankBranch(entityBankBranch);
					chequeBook.setChequeBookNumber(chequeBookDetailsDTO.getChequeBookNumber());
					chequeBook.setStatus("true");
					chequeBook.setRemarks(chequeBookDetailsDTO.getRemarks());
					chequeBookList.add(chequeBook);
				}
			}

			chequeBookDetailsRepository.save(chequeBookList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restExp) {
			log.error("Exception occered while creating ", restExp);
			baseDTO.setStatusCode(restExp.getErrorCodeDescription().getErrorCode());
		} catch (DataIntegrityViolationException diExp) {
			String exceptionCause = ExceptionUtils.getRootCauseMessage(diExp);
			log.error("Exception Cause  : ", exceptionCause);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("exception", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO getChequeBookDetailsById(Long chequeId) {
		log.info("====>> ChequeBookDetailsService.getChequeBookDetailsById() <<====");
		BaseDTO baseDTO = new BaseDTO();
		try {
			if (chequeId != null) {
				ChequeBook chequeBook = chequeBookDetailsRepository.findOne(chequeId);
				baseDTO.setResponseContent(chequeBook);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (RestException re) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.error("Rest Exception occured while fetching ...", re);
		} catch (Exception e) {
			log.error("Exception occured while fetching  ...", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;

	}

	public BaseDTO getChequeBookDetailsByChequeBookNumber(String chequeBookNumber) {
		log.info("====>> ChequeBookDetailsService.getChequeBookDetailsByChequeBookNumber() <<====");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<ChequeBook> chequeBookList = null;

			if (chequeBookNumber != null) {
				chequeBookList = chequeBookDetailsRepository.findchequeBookNumber(chequeBookNumber);

				ChequeBook chequeBook = chequeBookList.get(0);
				Long chequeNumberFrom = chequeBookDetailsRepository.findchequeNumberFrom(chequeBookNumber);
				Long chequeNumberTo = chequeBookDetailsRepository.findchequeNumberTo(chequeBookNumber);
				ChequeBookDetailsDTO chequeBookDetailsDTO = new ChequeBookDetailsDTO();
				chequeBookDetailsDTO.setEntityId(chequeBook.getEntityMaster().getId());
				chequeBookDetailsDTO.setEntityName(chequeBook.getEntityMaster().getName());
				chequeBookDetailsDTO.setEntityCode(chequeBook.getEntityMaster().getCode());
				chequeBookDetailsDTO.setId(chequeBook.getEntityBankBranch().getBankBranchMaster().getId());
				chequeBookDetailsDTO
						.setBranchCode(chequeBook.getEntityBankBranch().getBankBranchMaster().getBranchCode());
				chequeBookDetailsDTO
						.setBranchName(chequeBook.getEntityBankBranch().getBankBranchMaster().getBranchName());
				chequeBookDetailsDTO.setCreatedDate(chequeBook.getCreatedDate());
				chequeBookDetailsDTO.setCreatedBy(chequeBook.getCreatedBy());
				chequeBookDetailsDTO.setModifiedDate(chequeBook.getModifiedDate());
				chequeBookDetailsDTO.setModifiedBy(chequeBook.getModifiedBy());
				chequeBookDetailsDTO.setEntityBankBranch(chequeBook.getEntityBankBranch());
				// checkBookDetailsDTO.setAccountNumber(Long.valueOf(chequeBook.getEntityBankBranch().getAccountNumber()));
				chequeBookDetailsDTO.setChequeBookNumber(chequeBook.getChequeBookNumber());
				chequeBookDetailsDTO.setRemarks(chequeBook.getRemarks());
				chequeBookDetailsDTO.setChequeNumberFrom(chequeNumberFrom);
				chequeBookDetailsDTO.setChequeNumberTo(chequeNumberTo);
				baseDTO.setResponseContent(chequeBookDetailsDTO);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (RestException re) {
			log.info("==Exception===", re);
			baseDTO.setStatusCode(re.getStatusCode());
		} catch (Exception e) {
			log.error("Exception occured while fetching  ...", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;

	}

	public BaseDTO updateChequeBookDetails(ChequeBookDetailsDTO chequeBookDetailsDTO) {
		log.info("====>> ChequeBookDetailsService.updateChequeBookDetails() <<====");
		BaseDTO baseDTO = new BaseDTO();
		EntityMaster entityMaster = null;
		EntityBankBranch entityBankBranch = null;
		try {
			if (chequeBookDetailsDTO.getChequeBookNumber() != null) {
				chequeBookDetailsRepository.deleteBychequeBookNumber(chequeBookDetailsDTO.getChequeBookNumber());
			}
			List<ChequeBook> chequeBookList = new ArrayList<>();
			if (chequeBookDetailsDTO.getChequeNumberFrom() != null
					&& chequeBookDetailsDTO.getChequeNumberTo() != null) {
				Long from = chequeBookDetailsDTO.getChequeNumberFrom();
				Long to = chequeBookDetailsDTO.getChequeNumberTo();
				Long count = to - from;
				if (chequeBookDetailsDTO.getEntityId() != null) {
					entityMaster = entityMasterRepository.findOne(chequeBookDetailsDTO.getEntityId());
				}
				if (chequeBookDetailsDTO.getEntityBankBranch().getId() != null) {
					entityBankBranch = entityBankBranchRepository
							.findOne(chequeBookDetailsDTO.getEntityBankBranch().getId());

				}
				for (Long i = from; i <= count + 1; i++) {
					ChequeBook chequeBook = new ChequeBook();
					chequeBook.setChequeNumber(i);
					/*
					 * if (i == 0) {
					 * 
					 * chequeBook.setChequeNumber(from); } else if (i == count) {
					 * 
					 * chequeBook.setChequeNumber(to); } else {
					 * 
					 * chequeBook.setChequeNumber(from + i); }
					 */

					log.info(chequeBookDetailsDTO.getEntityId());
					chequeBook.setEntityMaster(entityMaster);
					chequeBook.setEntityBankBranch(entityBankBranch);
					chequeBook.setChequeBookNumber(chequeBookDetailsDTO.getChequeBookNumber());
					chequeBook.setStatus("true");
					chequeBook.setRemarks(chequeBookDetailsDTO.getRemarks());
					chequeBookList.add(chequeBook);
				}
			}
			chequeBookDetailsRepository.save(chequeBookList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (RestException restExp) {
			log.error("Exception occered ", restExp);
			baseDTO.setStatusCode(restExp.getStatusCode());
		} catch (DataIntegrityViolationException diExp) {
			String exceptionCause = ExceptionUtils.getRootCauseMessage(diExp);
			log.error("Exception Cause  : ", exceptionCause);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("<<======== ERROR IN UPDATE", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO getChequeBookLazy(PaginationDTO paginationDto) {
		log.info("====>> ChequeBookDetailsService.getChequeBookLazy() <<====");
		BaseDTO baseDTO = new BaseDTO();

		try {

			Integer total = 0;
			Integer start = paginationDto.getFirst(), pageSize = paginationDto.getPageSize();
			start = start * pageSize;

			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();

			String queryName = "CHEQUE_BOOK_DETAILS_LIST_QUERY";

			ApplicationQuery applicationQuery = applicationQueryRepository.findByQueryName(queryName);

			String query = applicationQuery.getQueryContent().trim();

			String mainQuery = query;
			/*
			 * if(paginationDto.getFilters().get("id")!=null) { mainQuery +=
			 * " and cb.id='"+paginationDto.getFilters().get("id")+"'"; }
			 */
			if (paginationDto.getFilters() != null) {
				if (paginationDto.getFilters().get("entityCodeOrName") != null) {

					if (AppUtil.isInteger(paginationDto.getFilters().get("entityCodeOrName").toString())) {
						mainQuery += " and entity_code like '%" + paginationDto.getFilters().get("entityCodeOrName")
								+ "%'";
					} else {
						mainQuery += " and upper(em.name) like upper('%"
								+ paginationDto.getFilters().get("entityCodeOrName") + "%')";
					}
				}
				if (paginationDto.getFilters().get("branchCodeOrName") != null) {

					if (AppUtil.isInteger(paginationDto.getFilters().get("branchCodeOrName").toString())) {
						mainQuery += " and bbm.branch_code like '%" + paginationDto.getFilters().get("branchCodeOrName")
								+ "%'";
					} else {
						mainQuery += " and upper(bbm.branch_name) like upper('%"
								+ paginationDto.getFilters().get("branchCodeOrName")
								+ "%') ";
								
					}
				}

				if (paginationDto.getFilters().get("accountNumber") != null) {
						mainQuery += " and ebb.account_number like '%" + paginationDto.getFilters().get("accountNumber")
								+ "%'";
					
				}
				if (paginationDto.getFilters().get("chequeBookNumber") != null) {
					mainQuery += " and cb.cheque_book_number like '%"
							+ paginationDto.getFilters().get("chequeBookNumber") + "%'";
				}

				if (paginationDto.getFilters().get("createdDate") != null) {
					Date createdDate = new Date((Long) paginationDto.getFilters().get("createdDate"));
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					mainQuery += " and cb.created_date::date = date '" + dateFormat.format(createdDate) + "'";
				}
				if (paginationDto.getFilters().get("status") != null) {
					mainQuery += " and upper(cb.status) like upper('%" + paginationDto.getFilters().get("status")
							+ "%')";
				}
			}
			mainQuery += " group by cb.entity_id,cb.entity_bank_branch_id,em.code,em.name,bbm.branch_code,bbm.branch_name,ebb.account_number,cb.cheque_book_number,date(cb.created_date),cb.status ";

			String countQuery = "select count(*) from (:mainQuery)t";
			countQuery = countQuery.replace(":mainQuery", mainQuery);
			log.info("count query... " + countQuery);
			countData = jdbcTemplate.queryForList(countQuery);
			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					total = Integer.parseInt(data.get("count").toString().replace(",", ""));
			}

			log.info("total query... " + total + "::pageSize::>>>" + pageSize + "::start::" + start);
			if (paginationDto.getSortField() == null) {
				mainQuery += " order by cheque_book_number desc limit " + pageSize + " offset " + start + ";";
			}
			if (paginationDto.getSortField() != null && paginationDto.getSortOrder() != null) {

				/*
				 * if(paginationDto.getSortField().equals("id") &&
				 * paginationDto.getSortOrder().equals("ASCENDING"))
				 * mainQuery+=" order by id asc "; if(paginationDto.getSortField().equals("id")
				 * && paginationDto.getSortOrder().equals("DESCENDING"))
				 * mainQuery+=" order by id desc ";
				 */

				if (paginationDto.getSortField().equals("entityCodeOrName")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by entity_code asc  ";
				if (paginationDto.getSortField().equals("entityCodeOrName")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by entity_code desc  ";

				if (paginationDto.getSortField().equals("status") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by t.status asc  ";
				if (paginationDto.getSortField().equals("status") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by t.status desc  ";

				if (paginationDto.getSortField().equals("bankBranchCodeOrName")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by bbm.branch_code asc  ";
				if (paginationDto.getSortField().equals("bankBranchCodeOrName")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by bbm.branch_code desc  ";

				if (paginationDto.getSortField().equals("accountNumber")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by ebb.account_number asc  ";
				if (paginationDto.getSortField().equals("accountNumber")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by ebb.account_number desc  ";

				if (paginationDto.getSortField().equals("chequeBookNumber")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by cb.cheque_book_number asc  ";
				if (paginationDto.getSortField().equals("chequeBookNumber")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by cb.cheque_book_number desc  ";

				if (paginationDto.getSortField().equals("createdDate")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by date asc  ";
				if (paginationDto.getSortField().equals("createdDate")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by date desc  ";

				mainQuery += " limit " + pageSize + " offset " + start + ";";
			}
			log.info("filter query----------" + mainQuery);
			List<ChequeBookDetailsDTO> chequeBookDetailsDTOList = new ArrayList<>();
			listofData = jdbcTemplate.queryForList(mainQuery);
			log.info("cheque Book list size-------------->" + listofData.size());
			for (Map<String, Object> data : listofData) {
				ChequeBookDetailsDTO chequeBookDetailsDTO = new ChequeBookDetailsDTO();
				/*
				 * if(data.get("id")!=null)
				 * checkBookDetailsDTO.setCheckBookId(Long.parseLong(data.get("id").toString()))
				 * ;
				 */
				/*
				 * if(data.get("advance_type")!=null)
				 * advancePaymentDTO.setAdvanceType(data.get("advance_type").toString());
				 */

				if (data.get("entity_id") != null) {
					chequeBookDetailsDTO.setEntityCodeOrName(data.get("entity_code").toString().concat(" / ")
							.concat(data.get("entity_name").toString()));
				} else {
					chequeBookDetailsDTO.setEntityCodeOrName(" - ");
				}
				if (data.get("entity_bank_branch_id") != null) {
					chequeBookDetailsDTO.setBranchCodeOrName(data.get("branch_code").toString().concat(" / ")
							.concat(data.get("branch_name").toString()));
				} else {
					chequeBookDetailsDTO.setBranchCodeOrName(" - ");
				}
				if (data.get("account_number") != null) {
					chequeBookDetailsDTO.setAccountNumber(Long.valueOf(data.get("account_number").toString()));
				}
				if (data.get("cheque_book_number") != null) {
					chequeBookDetailsDTO.setChequeBookNumber(data.get("cheque_book_number").toString());
				}
				if (data.get("status") != null)
					chequeBookDetailsDTO.setStatus(data.get("status").toString());
				if (data.get("min(cb.cheque_number)") != null) {
					chequeBookDetailsDTO
							.setChequeNumberFrom(Long.valueOf(data.get("min(cb.cheque_number)").toString()));
				}
				if (data.get("max(cb.cheque_number)") != null) {
					chequeBookDetailsDTO.setChequeNumberTo(Long.valueOf(data.get("max(cb.cheque_number)").toString()));
				}
				if (data.get("date") != null)
					chequeBookDetailsDTO.setCreatedDate((Date) data.get("date"));

				chequeBookDetailsDTOList.add(chequeBookDetailsDTO);
			}
			log.info("final list size---------------" + chequeBookDetailsDTOList.size());
			log.info("Total records present in ChequeBook..." + total);
			baseDTO.setResponseContent(chequeBookDetailsDTOList);
			baseDTO.setTotalRecords(total);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (Exception exp) {
			log.error("Exception Cause getChequeBookLazy  : ", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

}
