package in.gov.cooptex.finance.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import in.gov.cooptex.common.service.EntityMasterService;
import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.service.NotificationEmailService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.model.TourProgramLog;
import in.gov.cooptex.core.accounts.model.TourProgramNote;
import in.gov.cooptex.core.accounts.repository.EntityBankBranchRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.finance.service.OperationService;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.SystemNotification;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.AmountTransferDetailsRepository;
import in.gov.cooptex.core.repository.AmountTransferRepository;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EmployeePersonalInfoEmploymentRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.SystemNotificationRepository;
import in.gov.cooptex.core.repository.TourProgramDetailsRepository;
import in.gov.cooptex.core.repository.TourProgramLogRepository;
import in.gov.cooptex.core.repository.TourProgramNoteRepository;
import in.gov.cooptex.core.repository.TourProgramRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.ui.EntityType;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.TourProgramConstant;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.finance.TourProgramResponseDTO;
import in.gov.cooptex.finance.TourProgramSaveDto;
import in.gov.cooptex.finance.repository.AmountTransferLogRepository;
import in.gov.cooptex.finance.repository.AmountTransferNoteRepository;
import in.gov.cooptex.operation.model.TourProgram;
import in.gov.cooptex.operation.model.TourProgramDetails;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class TourProgramJournalService {

	@Autowired
	private EntityManager em;

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private AmountTransferRepository amountTransferRepository;

	@Autowired
	private TourProgramRepository tourProgramRepository;

	@Autowired
	private TourProgramLogRepository tourProgramLogRepository;

	@Autowired
	NotificationEmailService notificationEmailService;

	@Autowired
	private TourProgramNoteRepository tourProgramNoteRepository;

	@Autowired
	private TourProgramDetailsRepository tourProgramDetailsRepository;

	@Autowired
	private AmountTransferDetailsRepository amountTransferDetailsRepository;

	@Autowired
	private AmountTransferLogRepository amountTransferLogRepository;

	@Autowired
	private AmountTransferNoteRepository amountTransferNoteRepository;

	@Autowired
	private EntityMasterRepository entityMasterRepository;

	@Autowired
	private UserMasterRepository userMasterRepository;

	@Autowired
	EmployeeMasterRepository employeeMasterRepository;

	@Autowired
	EntityBankBranchRepository entityBankBranchRepository;

	@Autowired
	EntityMasterService entityMasterService;

	@Autowired
	LoginService loginService;

	@Autowired
	OperationService operationService;

	@Autowired
	ResponseWrapper responseWrapper;

	@Getter
	@Setter
	EmployeeMaster empMaster;

	@Getter
	@Setter
	EmployeeMasterRepository empRepository;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	@Autowired
	SystemNotificationRepository systemNotificationRepository;

	@Autowired
	EmployeePersonalInfoEmploymentRepository employeePersonalInfoEmploymentRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	private static final String TOUR_PROGRAME_JOURNAL_VIEW_URL = "/pages/accounts/tourProgram/viewTourProgramJournal.xhtml?faces-redirect=true";

	public BaseDTO getTourProgramDetails(PaginationDTO request) {
		BaseDTO baseDTO = new BaseDTO();

		log.info("<<====   TourProgramJournalService ---  getAll ====## STARTS");
		try {
			if (request == null) {
				throw new RestException(ErrorDescription.REQUEST_OBJECT_SHOULD_NOT_EMPTY);
			}
			Integer totalRecords = 0;
			Criteria criteria = null;

			Session session = entityManager.unwrap(Session.class);
			criteria = session.createCriteria(TourProgram.class, "tourProgram");
			criteria.createAlias("tourProgram.entityMaster", "entityMaster");

			UserMaster userMaster = loginService.getCurrentUser();
			Long loginUserId = userMaster == null ? null : userMaster.getId();

			EntityMaster workLocation = entityMasterRepository.getEntityInfoByLoggedInUser(loginUserId);
			EntityTypeMaster entityTypeMaster = workLocation == null ? null : workLocation.getEntityTypeMaster();
			String entityTypeCode = entityTypeMaster == null ? null : entityTypeMaster.getEntityCode();
			Long workLocationId = workLocation != null ? workLocation.getId() : null;
			
			if(!EntityType.HEAD_OFFICE.equals(entityTypeCode)) {
				if (workLocationId != null) {
					criteria.add(Restrictions.eq("entityMaster.id", workLocationId));
				}
			}
			

			if (request.getFilters().get("tourName") != null
					&& !request.getFilters().get("tourName").toString().trim().isEmpty()) {
				log.info("tourName added to criteria:" + request.getFilters().get("tourName"));
				String fbranch = (String) request.getFilters().get("tourName");
				criteria.add(Restrictions.like("tourProgram.tourName", "%" + fbranch.trim() + "%").ignoreCase());
			}

			if (request.getFilters().get("month") != null
					&& !request.getFilters().get("month").toString().trim().isEmpty()) {
				log.info("programMonth added to criteria:" + request.getFilters().get("month"));
				String lname = (String) request.getFilters().get("month");
				// criteria.add(Restrictions.like("tourProgram.programMonth","%"+lname.toUpperCase().trim()+"%").ignoreCase());

				if (AppUtil.isInteger(lname)) {
					log.info("programYear search year is:" + lname);
					criteria.add(Restrictions.eq("tourProgram.programYear", AppUtil.stringToLong(lname, null)));
				} else {
					log.info("programMonth search month is:" + lname);
					criteria.add(Restrictions.like("tourProgram.programMonth", "%" + lname.toUpperCase().trim() + '%')
							.ignoreCase());
				}
			}

			if (request.getFilters().get("entityMaster.name") != null
					&& !request.getFilters().get("entityMaster.name").toString().trim().isEmpty()) {
				log.info("entityMaster.name added to criteria:" + request.getFilters().get("entityMaster.name"));
				String lname = (String) request.getFilters().get("entityMaster.name");
				criteria.add(Restrictions.like("entityMaster.name", "%" + lname.trim() + "%").ignoreCase());
			}

			if (request.getFilters().get("createdDate") != null
					&& !request.getFilters().get("createdDate").toString().trim().isEmpty()) {
				log.info("created date added to criteria:" + request.getFilters().get("createdDate"));
				Date date = new Date((long) request.getFilters().get("createdDate"));
				DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
				String strDate = dateFormat.format(date);
				Date minDate = dateFormat.parse(strDate);
				Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
				criteria.add(Restrictions.conjunction().add(Restrictions.ge("tourProgram.createdDate", minDate))
						.add(Restrictions.lt("tourProgram.createdDate", maxDate)));
			}

			criteria.setProjection(Projections.rowCount());
			totalRecords = ((Long) criteria.uniqueResult()).intValue();
			criteria.setProjection(null);

			Integer pageNo = request.getPageNo();
			Integer pageSize = request.getPageSize();

			if (pageNo != null && pageSize != null) {
				criteria.setFirstResult(pageNo * pageSize);
				criteria.setMaxResults(pageSize);
				log.info("PageNo : [" + pageNo + "] pageSize[" + pageSize + "]");
			}
			ProjectionList projectionList = Projections.projectionList();
			projectionList.add(Projections.property("tourProgram.id"));
			projectionList.add(Projections.property("tourProgram.tourName"));
			projectionList.add(Projections.property("tourProgram.programMonth"));
			projectionList.add(Projections.property("tourProgram.programYear"));
			projectionList.add(Projections.property("entityMaster.name"));
			projectionList.add(Projections.property("tourProgram.createdDate"));
			criteria.setProjection(projectionList);
			criteria.addOrder(Order.desc("tourProgram.id"));

			List<?> resultList = criteria.list();
			if (resultList == null || resultList.isEmpty() || resultList.size() == 0) {
				log.info("Tour Program is null or empty ");
				baseDTO.setStatusCode(ErrorDescription.PRODUCT_VARIETY_LIST_EMPTY.getCode());
				return baseDTO;
			}

			log.info("Tour Program criteria list executed and the list size is  : " + resultList.size());
			List<TourProgramResponseDTO> results = new ArrayList<TourProgramResponseDTO>();
			Iterator<?> it = resultList.iterator();

			ArrayList<Long> transferID = new ArrayList<Long>();
			while (it.hasNext()) {
				Object ob[] = (Object[]) it.next();

				TourProgramResponseDTO response = new TourProgramResponseDTO();
				EntityMaster entityMaster = new EntityMaster();
				entityMaster.setName((String) (ob[4]));
				response.setId((Long) ob[0]);
				response.setTourName((String) ob[1]);
				response.setMonth((String) ob[2]);
				response.setYear((Long) ob[3]);
				response.setCreatedDate((Date) ob[5]);
				response.setEntityMaster(entityMaster);

				TourProgramNote tourProgramNote = tourProgramNoteRepository
						.getTourProgamNoteDetailsById(response.getId());

				log.info("TourProgram Id contians list:" + !transferID.contains(response.getId()));
				TourProgramLog tourProgramLog = tourProgramLogRepository.getTourProgamLogDetailsById(response.getId());
				String stage[] = { TourProgramConstant.TJ_SUBMITTED, TourProgramConstant.TJ_APPROVED,
						TourProgramConstant.TJ_FINAL_APPROVED, TourProgramConstant.TJ_REJECTED };
				String status = tourProgramLog != null ? tourProgramLog.getStage() : "";
				List<String> list = Arrays.asList(stage);
				if (tourProgramLog != null && !transferID.contains(response.getId()) && list.contains(status)) {
					log.info("<<====   TourProgramService tourDetails Id groupBy =====>>");
					String request_status = (String) request.getFilters().get("status");
					if (request_status != null && !request_status.isEmpty()) {
						if (request_status.equalsIgnoreCase(tourProgramLog.getStage())) {
							response.setStatus(request_status);
							response.setRemarks(tourProgramLog.getRemarks());
							if (tourProgramNote != null) {
								response.setForwardTo(tourProgramNote.getForwardTo().getId());
								response.setNote(tourProgramNote.getNote());
								response.setForwardFor(tourProgramNote.getFinalApproval());
							}
							results.add(response);
						}
					} else {
						response.setStatus(tourProgramLog.getStage());
						if (tourProgramNote != null) {
							response.setForwardTo(tourProgramNote.getForwardTo().getId());
							response.setNote(tourProgramNote.getNote());
							response.setForwardFor(tourProgramNote.getFinalApproval());
						}
						results.add(response);
					}
				}
			}
			baseDTO.setTotalRecords(results.size());

			baseDTO.setResponseContents(results);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

			return baseDTO;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex);
			return null;
		}
	}

	@Transactional
	public BaseDTO saveTourProgramDetails(TourProgramSaveDto data) {
		BaseDTO baseDTO = new BaseDTO();
		TourProgramLog result = new TourProgramLog();
		log.info("<==== Starts TourProgramJournalService.save Function Entered =====>");
		TourProgram tourProgram = new TourProgram();

		try {
			UserMaster user = loginService.getCurrentUser();
			BaseDTO basedto = entityMasterService.getEntityInfoByLoggedInUser(user.getId());
			EntityMaster entity = (EntityMaster) basedto.getResponseContent();
			if (entity != null) {
				data.setEntityId(entity.getId());
				log.info("entityId:" + entity.getId());
			}

			if (data != null) {
				log.info("tour Prgoram details list size:" + data.getTourProgramDetailsList().size());

				for (TourProgramDetails tourDeatils : data.getTourProgramDetailsList()) {
					TourProgramDetails tourProgramDetail = new TourProgramDetails();

					if (tourDeatils.getId() != null) {
						tourProgramDetail = tourProgramDetailsRepository.getOne(tourDeatils.getId());

						tourProgram = tourProgramDetail.getTourProgram();

						UserMaster userMaster = userMasterRepository.findOne(data.getForwardTo());
						if (data.getNote() != null) {
							TourProgramNote note = new TourProgramNote();
							log.info("tourProgram :" + tourProgram);
							note.setTourProgram(tourProgram);
							note.setNote(data.getNote());
							note.setFinalApproval(data.getForwardFor());
							note.setForwardTo(userMaster);
							// try {
							note = tourProgramNoteRepository.save(note);
							log.info("TourProgram note saved successfully " + note);
							// } catch (Exception ex) {
							// log.info("<-- TourProgramNote Table Save / exception :- -->" + ex);
							// }
							// }

							TourProgramLog tourProgramLog = new TourProgramLog();
							tourProgramLog.setTourProgram(tourProgram);
							tourProgramLog.setStage(TourProgramConstant.TJ_SUBMITTED);
							if (data.getRemarks() != null) {
								tourProgramLog.setRemarks(data.getRemarks());
							}
							if (data.getVersion() != null) {
								tourProgramLog.setVersion(data.getVersion());
							}
							try {
								tourProgramLog = tourProgramLogRepository.save(tourProgramLog);
								log.info("TourProgram Log saved successfully " + tourProgramLog);
							} catch (Exception ex) {
								log.info("<-- TourProgramLog Table Save / exception :- -->" + ex);
							}

							Map<String, Object> additionalData = new HashMap<>();
							additionalData.put("Url", TOUR_PROGRAME_JOURNAL_VIEW_URL + "&tourProgramJournalId="
									+ tourProgram.getId() + "&");
							notificationEmailService.sendMailAndNotificationForForward(note, tourProgramLog,
									additionalData);

							result = tourProgramLog;
							baseDTO.setResponseContent(result);
							baseDTO.setTotalRecords(1);
							baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
						}
					}
				}

			} else {
				log.info("<-- Empty of TourProgramSaveDto/ exception :- -->");
			}
		} catch (Exception ex) {
			log.info("<-- Exception in TourProgramService/ exception :- -->" + ex);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			return baseDTO;
		}
		return baseDTO;
	}

	public BaseDTO getTourEditById(Long id, Long notificationId) {
		log.info("<--- TourProgram.getTourEditById() starts---->" + id);
		BaseDTO response = new BaseDTO();

		TourProgramSaveDto tourDTO = new TourProgramSaveDto();
		try {

			TourProgram tourProgram = tourProgramRepository.findOne(id);
			tourDTO.setTourProgram(tourProgram);
			tourDTO.setTourProgramDetailsList(tourProgramDetailsRepository.getTourProgramDetailList(id));
			tourDTO.setTourProgramLog(tourProgramLogRepository.getTourProgamLogDetailsById(id));
			tourDTO.setTourProgramNote(tourProgramNoteRepository.getTourProgamNoteDetailsById(id));
			TourProgramNote tourProgramNote = tourProgramNoteRepository.getTourProgamNoteDetailsById(id);
			tourDTO.setForwardFor(tourProgramNote.getFinalApproval());
			tourDTO.setForwardTo(tourProgramNote.getForwardTo().getId());
			tourDTO.setNote(tourProgramNote.getNote());
			response.setResponseContent(tourDTO);

			List<Map<String, Object>> employeeData = new ArrayList<Map<String, Object>>();
			if (tourProgram.getId() != null) {
				log.info("<<<:::::::voucher::::not Null::::>>>>" + tourProgram.getId() != null ? tourProgram.getId()
						: "Null");
				ApplicationQuery applicationQueryForlog = applicationQueryRepository
						.findByQueryName("TOUR_PROGRAM_JOURNAL_LOG_EMPLOYEE_DETAILS");
				if (applicationQueryForlog == null || applicationQueryForlog.getId() == null) {
					log.info("Application Query For Log Details not found for query name : " + applicationQueryForlog);
					response.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
					return response;
				}
				String logquery = applicationQueryForlog.getQueryContent().trim();
				log.info("<=========TOUR_PROGRAM_JOURNAL_LOG_EMPLOYEE_DETAILS Query Content ======>" + logquery);
				logquery = logquery.replace(":tourProgramId", "'" + tourProgram.getId().toString() + "'");
				log.info(
						"Query Content For TOUR_PROGRAM_JOURNAL_LOG_EMPLOYEE_DETAILS After replaced plan id View query : "
								+ logquery);
				employeeData = jdbcTemplate.queryForList(logquery);
				log.info("<=========TOUR_PROGRAM_JOURNAL_LOG_EMPLOYEE_DETAILS Employee Data======>" + employeeData);
				response.setTotalListOfData(employeeData);
			}
			if (notificationId != null && notificationId > 0) {
				SystemNotification systemNotification = systemNotificationRepository.findOne(notificationId);
				systemNotification.setNotificationRead(true);
				systemNotificationRepository.save(systemNotification);
			}
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (RestException e) {
			log.error(" Rest Exception Occured ", e);
			response.setStatusCode(e.getStatusCode());
		} catch (Exception e) {
			log.info("Error while retriving getTourProgramEditById----->", e);
			response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<---TourProgramJournalService.getTourProgramEditById() end---->");
		return responseWrapper.send(response);
	}

	public BaseDTO getAllEmployee() {
		log.info("EmployeeService.getAllEmployee() Started ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<EmployeeMaster> employeeList = employeeMasterRepository.getAllEmployee();
			if (employeeList != null && employeeList.size() > 0) {
				baseDTO.setResponseContent(employeeList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				log.info("TourJournalService.getAllEmployee() Completed");
			}
		} catch (Exception exception) {
			log.error("Exception occurred in EmployeeService.getAllEmployee() -:", exception);
			baseDTO.setStatusCode(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO getAllMonths(Long empId) {
		log.info("TourJournalService.getAllMnthAndYear() Started, loginEmpId:" + empId);
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<TourProgram> tourProgramList = new ArrayList<>();
			UserMaster userMaster = loginService.getCurrentUser();
			if (empId != null) {
				tourProgramList = tourProgramRepository.getAllMonthByEmployee(empId,
						TourProgramConstant.TTP_FINAL_APPROVED);

				if (tourProgramList != null && tourProgramList.size() > 0) {
					baseDTO.setResponseContents(tourProgramList);
					baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
					log.info("TourMonth and year.getAllMonth() Completed");
				}
			} else {
				log.info("TourMonth and year.getAllMonth() empId is empty:" + empId);
			}
		} catch (Exception exception) {
			log.error("Exception occurred in TourJournalService.getAllMonth() -:", exception);
			baseDTO.setStatusCode(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO getTourProgramDetailList(TourProgramSaveDto data) {
		log.info("TourJournalService.getTourProgramDetailList() Started");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<TourProgram> tourProgramList = new ArrayList<>();
			List<TourProgramDetails> tourProgramDetails = new ArrayList<>();
			if (data != null) {
				Long empId = data.getEmpId();
				String month = data.getMonth();
				Long year = new Long(data.getSelectedYear());

				log.info("now searching empId,month and year is" + empId + ' ' + month + ' ' + year);

				tourProgramList = tourProgramRepository.getTourProgramDetailList(empId, month, year);

				if (tourProgramList != null && tourProgramList.size() > 0) {
					for (TourProgram obj : tourProgramList) {
						Long id = obj.getId();
						tourProgramDetails = tourProgramDetailsRepository.getTourProgramDetailList(id);
					}

					baseDTO.setResponseContents(tourProgramDetails);
					baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
					log.info("TourMonth and year.getAllMonth() Completed and size is:" + tourProgramList.size());
				}
			} else {
				log.info("getTourProgramDetailList is empty:");
			}
		} catch (Exception exception) {
			log.error("Exception occurred in TourJournalService.getAllMonth() -:", exception);
			baseDTO.setStatusCode(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO approveTourProgram(TourProgramResponseDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("approveTourProgramJournal method start=============>" + requestDTO.getId());
			TourProgram tourProgram = tourProgramRepository.findOne(requestDTO.getId());

			TourProgramNote tourProgramNote = new TourProgramNote();
			tourProgramNote.setTourProgram(tourProgram);
			tourProgramNote.setFinalApproval(requestDTO.getForwardFor());
			tourProgramNote.setNote(requestDTO.getNote());
			tourProgramNote.setForwardTo(userMasterRepository.findOne(requestDTO.getForwardTo()));
			tourProgramNote.setCreatedBy(loginService.getCurrentUser());
			tourProgramNote.setCreatedByName(loginService.getCurrentUser().getUsername());
			tourProgramNote.setCreatedDate(new Date());
			tourProgramNoteRepository.save(tourProgramNote);
			log.info("approveTourProgramJournal note inserted------");

			TourProgramLog tourProgramLog = new TourProgramLog();
			tourProgramLog.setTourProgram(tourProgram);
			tourProgramLog.setStage(requestDTO.getStatus());
			tourProgramLog.setCreatedBy(loginService.getCurrentUser());
			tourProgramLog.setCreatedByName(loginService.getCurrentUser().getUsername());
			tourProgramLog.setCreatedDate(new Date());
			tourProgramLog.setRemarks(requestDTO.getRemarks());
			tourProgramLogRepository.save(tourProgramLog);
			log.info("approveTourProgram log inserted------");

			log.info("TourProgram posting Start--------" + tourProgram);
			log.info("Approval status----------" + requestDTO.getStatus());

			if (TourProgramConstant.TJ_FINAL_APPROVED.equals(requestDTO.getStatus())) {
				String urlPath = TOUR_PROGRAME_JOURNAL_VIEW_URL + "&tourProgramJournalId=" + tourProgram.getId() + "&";
				Long toUserId = tourProgram.getCreatedBy().getId();
				notificationEmailService.saveIndividualNotification(loginService.getCurrentUser(), toUserId, urlPath,
						"Jurnel Tour", " Jurnel Tour Program  has been final approved");
			} else {
				Map<String, Object> additionalData = new HashMap<>();
				additionalData.put("Url",
						TOUR_PROGRAME_JOURNAL_VIEW_URL + "&tourProgramJournalId=" + requestDTO.getId() + "&");
				notificationEmailService.sendMailAndNotificationForForward(tourProgramNote, tourProgramLog,
						additionalData);
			}

			log.info("Tour posting End--------");
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("approveTourProgramJournal method exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO rejectTourProgram(TourProgramResponseDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("rejectTourProgramJournal method start=============>" + requestDTO.getId());

			TourProgram tourProgram = tourProgramRepository.findOne(requestDTO.getId());

			TourProgramLog tourProgramLog = new TourProgramLog();
			tourProgramLog.setTourProgram(tourProgram);
			tourProgramLog.setStage(requestDTO.getStatus());
			tourProgramLog.setCreatedBy(loginService.getCurrentUser());
			tourProgramLog.setCreatedByName(loginService.getCurrentUser().getUsername());
			tourProgramLog.setCreatedDate(new Date());
			tourProgramLog.setRemarks(requestDTO.getRemarks());
			tourProgramLogRepository.save(tourProgramLog);

			TourProgramNote tourProgramNote = new TourProgramNote();
			tourProgramNote.setTourProgram(tourProgram);
			tourProgramNote.setFinalApproval(requestDTO.getForwardFor());
			tourProgramNote.setNote(requestDTO.getNote());
			tourProgramNote.setForwardTo(userMasterRepository.findOne(requestDTO.getForwardTo()));
			tourProgramNote.setCreatedBy(loginService.getCurrentUser());
			tourProgramNote.setCreatedByName(loginService.getCurrentUser().getUsername());
			tourProgramNote.setCreatedDate(new Date());
			tourProgramNoteRepository.save(tourProgramNote);

			if (TourProgramConstant.TJ_REJECTED.equals(requestDTO.getStatus())) {
				String urlPath = TOUR_PROGRAME_JOURNAL_VIEW_URL + "&tourProgramJournalId=" + tourProgram.getId() + "&";
				Long toUserId = tourProgram.getCreatedBy().getId();
				notificationEmailService.saveIndividualNotification(loginService.getCurrentUser(), toUserId, urlPath,
						"Jurnel Tour", " Jurnel Tour Program  has been Rejected");
			} else {
				Map<String, Object> additionalData = new HashMap<>();
				additionalData.put("Url",
						TOUR_PROGRAME_JOURNAL_VIEW_URL + "&tourProgramJournalId=" + requestDTO.getId() + "&");
				notificationEmailService.sendMailAndNotificationForForward(tourProgramNote, tourProgramLog,
						additionalData);
			}

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("rejectTourProgramJournal  log inserted------");
		} catch (Exception e) {
			log.error("rejectTourProgramJournal method exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}
}
