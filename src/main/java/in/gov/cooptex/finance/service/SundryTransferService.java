package in.gov.cooptex.finance.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import commonDataService.AppConfigKey;
import in.gov.cooptex.common.repository.AppConfigRepository;
import in.gov.cooptex.common.repository.OrganizationMasterRepository;
import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.core.accounts.enums.VoucherTypeDetails;
import in.gov.cooptex.core.accounts.model.CollectionRegister;
import in.gov.cooptex.core.accounts.model.CollectionRegisterDetails;
import in.gov.cooptex.core.accounts.model.CreditSalesRequest;
import in.gov.cooptex.core.accounts.model.GlAccount;
import in.gov.cooptex.core.accounts.model.JournalVoucher;
import in.gov.cooptex.core.accounts.model.JournalVoucherEntries;
import in.gov.cooptex.core.accounts.model.JournalVoucherLog;
import in.gov.cooptex.core.accounts.model.JournalVoucherNote;
import in.gov.cooptex.core.accounts.model.Payment;
import in.gov.cooptex.core.accounts.model.PaymentDetails;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.VoucherDetails;
import in.gov.cooptex.core.accounts.model.VoucherType;
import in.gov.cooptex.core.accounts.repository.GlAccountRepository;
import in.gov.cooptex.core.accounts.repository.PaymentDetailsRepository;
import in.gov.cooptex.core.accounts.repository.PaymentMethodRepository;
import in.gov.cooptex.core.accounts.repository.PaymentRepository;
import in.gov.cooptex.core.accounts.repository.PaymentTypeMasterRepository;
import in.gov.cooptex.core.accounts.repository.VoucherRepository;
import in.gov.cooptex.core.accounts.repository.VoucherTypeRepository;
import in.gov.cooptex.core.accounts.util.VoucherTypeCons;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.enums.PaymentCategory;
import in.gov.cooptex.core.enums.PaymentType;
import in.gov.cooptex.core.finance.repository.JournalVoucherEntriesRepository;
import in.gov.cooptex.core.finance.repository.JournalVoucherLogRepository;
import in.gov.cooptex.core.finance.repository.JournalVoucherNoteRepository;
import in.gov.cooptex.core.finance.repository.JournalVoucherRepository;
import in.gov.cooptex.core.model.AppConfig;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.OrganizationMaster;
import in.gov.cooptex.core.model.PaymentMode;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.AmountTransferDetailsRepository;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.CollectionRegisterDetailsRepository;
import in.gov.cooptex.core.repository.CollectionRegisterRepository;
import in.gov.cooptex.core.repository.CreditSalesRequestRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.PaymentModeRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.util.JdbcUtil;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.SundryTransferKey;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.finance.dto.SundryTransferDTO;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class SundryTransferService {

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	LoginService loginService;

	@Autowired
	AppConfigRepository appConfigRepository;

	@Autowired
	PaymentModeRepository paymentModeRepository;

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	CollectionRegisterRepository collectionRegisterRepository;

	@Autowired
	CollectionRegisterDetailsRepository collectionRegisterDetailsRepository;

	@Autowired
	UserMasterRepository userMasterRepository;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	@Autowired
	VoucherRepository voucherRepository;

	@Autowired
	PaymentRepository paymentRepository;

	@Autowired
	PaymentTypeMasterRepository paymentTypeMasterRepository;

	@Autowired
	PaymentMethodRepository paymentMethodRepository;

	@Autowired
	JournalVoucherRepository journalVoucherRepository;

	@Autowired
	JournalVoucherEntriesRepository journalVoucherEntriesRepository;

	@Autowired
	JournalVoucherLogRepository journalVoucherLogRepository;

	@Autowired
	JournalVoucherNoteRepository journalVoucherNoteRepository;

	@Autowired
	VoucherTypeRepository voucherTypeRepository;

	@Autowired
	GlAccountRepository glAccountRepository;

	@Autowired
	AmountTransferDetailsRepository amountTransferDetailsRepository;

	@Autowired
	PaymentDetailsRepository paymentDetailsRepository;

	@Autowired
	CreditSalesRequestRepository creditSalesRequestRepository;

	@Autowired
	OrganizationMasterRepository organizationMasterRepository;

	private static final String JV_PREFIX = "JV";

	private static final String START_TIME_SUFFIX = " 00:00:00";

	private static final String END_TIME_SUFFIX = " 23:59:59";

	public BaseDTO loadAllSundryData(SundryTransferDTO request) {
		BaseDTO baseDTO = new BaseDTO();
		try {

			Integer total = 0;
			Integer start = request.getFirst(), pageSize = request.getPageSize();
			start = start * pageSize;
			log.info(" FromDate : " + request.getFromDate());
			log.info(" Todate   : " + request.getToDate());
			Long userId = loginService.getCurrentUser().getId();
			log.info(" UserId   : " + userId);

			log.info(" pageSize   : " + pageSize);
			log.info(" Start   : " + start);
			log.info(" First   : " + request.getFirst());
			log.info(" Start   : " + request.getPageSize());

			AppConfig appConfig = appConfigRepository.findByAppKey("SUNDRY_TRANSFER_GLACCOUNT_ID");
			// log.info(" SUNDRY_TRANSFER_GLACCOUNT_ID : " +
			// appConfig.getAppValue().toString());

			if (appConfig == null && appConfig.getAppKey() == null) {
				AppUtil.addError("SUNDRY_TRANSFER_GLACCOUNT_ID Key Not Available in DB.");
				return null;
			}
			if (appConfig.getAppValue() == null && appConfig.getAppValue().equals(" ")) {
				AppUtil.addError("Please update Gl Account Id in App Config.");
				return null;
			}

			String parameters = "";
			if (request.getFromDate() != null && request.getToDate() != null) {
				String fromDateStr = request.getFromDate() + START_TIME_SUFFIX;
				String toDateStr = request.getToDate() + END_TIME_SUFFIX;
				parameters = " and v.created_date between '" + fromDateStr + "' and '" + toDateStr + "' ";
			}
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			// String mainQuery = "select * from (select coalesce(sum(jv.jv_amount),0) as
			// adjustedAmount,em.code||'/'||em.name as entityCodeName,v.entity_id as
			// entityId,em2.code||'/'||em2.name as forEntityCodeName,v.for_entity_id as
			// forEntity,v.reference_number_prefix as receiptNumber,date(v.created_date) as
			// receiptDate,v.narration as description,pm.code as paymentMode,v.net_amount as
			// collectedAmount,vd.gl_account_id as glAccountId,v.payment_mode_id as
			// paymentModeId,vd.voucher_id as voucherId from voucher v join voucher_details
			// vd on v.id=vd.voucher_id join entity_master em on em.id= v.entity_id join
			// (select id from entity_master where region_id in (select id from
			// entity_master where entity_type_id=(select id from entity_type_master where
			// code='REGIONAL_OFFICE') and id in(select work_location_id from
			// emp_personal_info_employment where work_location_id is not null and
			// emp_id=(select id from emp_master where user_id =:userId)))) rm on em.id
			// in(rm.id) left join entity_master em1 on em1.id=v.entity_id and em1.region_id
			// is null join payment_mode pm on pm.id=v.payment_mode_id left join
			// entity_master em2 on em2.id=v.for_entity_id left join journal_voucher jv on
			// jv.voucher_id = v.id where vd.gl_account_id in (:glAccountId) and
			// v.voucher_type_id=4 :parameter group by
			// em.name,em.code,v.reference_number_prefix,v.reference_number,v.net_amount,vd.gl_account_id,v.payment_mode_id,vd.voucher_id,vd.amount,v.created_date,pm.code,v.narration,v.entity_id,em2.code,em2.name,em.name,v.for_entity_id)
			// as t Where 1=1 :condition";

			ApplicationQuery applicationQuery = applicationQueryRepository
					.findByQueryName("GENERATE_SUNDRY_TRANSFER_SQL");
			if (applicationQuery == null) {
				baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getErrorCode());
				return baseDTO;
			}
			String mainQuery = applicationQuery != null ? applicationQuery.getQueryContent() : null;

			String conditions = "";
			if (request.getFilters() != null) {

				if (request.getFilters().get("receiptNumber") != null) {
					conditions += " and t.receiptNumber like '%" + request.getFilters().get("receiptNumber") + "%'";
				}

				if (request.getFilters().get("receiptDate") != null) {
					Date receiptDate = new Date((Long) request.getFilters().get("receiptDate"));
					conditions += "and t.receiptDate::date = date '" + receiptDate + "'";
				}

				if (request.getFilters().get("entityCodeName") != null) {
					conditions += " and t.entityCodeName like '%" + request.getFilters().get("entityCodeName") + "%'";
				}

				if (request.getFilters().get("forEntityCodeName") != null) {
					conditions += " and t.forEntityCodeName like '%" + request.getFilters().get("forEntityCodeName")
							+ "%'";
				}

				if (request.getFilters().get("description") != null) {
					conditions += "and UPPER(t.description) like UPPER('%" + request.getFilters().get("description")
							+ "%')";
				}

				if (request.getFilters().get("modeofPaymentCode") != null) {
					String paymentMode = request.getFilters().get("modeofPaymentCode").toString();
					conditions += " and UPPER(t.paymentMode) like UPPER('%" + paymentMode + "%')";
				}

				if (request.getFilters().get("amountCollected") != null) {
					conditions += " and t.collectedAmount::text like ('%" + request.getFilters().get("amountCollected")
							+ "%')";
				}

				if (request.getFilters().get("adjustedAmount") != null) {
					conditions += " and t.adjustedAmount::text like ('%" + request.getFilters().get("adjustedAmount")
							+ "%')";
				}

				if (request.getFilters().get("documentNumber") != null) {
					conditions += "and t.bankReferenceNumber like '%" + request.getFilters().get("documentNumber")
							+ "%'";
				}

				if (request.getFilters().get("adviseDate") != null) {
					Date documentDate = new Date((Long) request.getFilters().get("adviseDate"));
					conditions += "and t.documentDate::date = date '" + documentDate + "'";
				}
			}
			if (userId != null && userId != 0) {
				mainQuery = mainQuery.replace(":userId", userId.toString());
			} else {
				AppUtil.addError("This user Not Allowed for SundryTransfer!");
				return null;
			}
			mainQuery = mainQuery.replace(":glAccountId", appConfig.getAppValue());
			mainQuery = mainQuery.replace(":parameter", parameters);
			mainQuery = mainQuery.replace(":condition", conditions);

			String countQuery = "SELECT count(*) AS count FROM (" + mainQuery + ")t";
			log.info("count query... " + countQuery);

			// String countQuery = mainQuery.replace("select * ", "select count(*) as count
			// ");
			// countQuery += " limit " + pageSize + " offset " + start + ";";
			// log.info("count query... " + countQuery);
			Map<String, Object> countData = new HashMap<String, Object>();
			countData = jdbcTemplate.queryForMap(countQuery);

			if (countData.get("count") != null) {
				total = Integer.parseInt(countData.get("count").toString().replace(",", ""));
			}

			if (request.getSortField() == null)
				mainQuery += " limit " + pageSize + " offset " + start + ";";

			if (request.getSortField() != null && request.getSortOrder() != null) {
				log.info("Sort Field:[" + request.getSortField() + "] Sort Order:[" + request.getSortOrder() + "]");

				if (request.getSortField().equals("receiptNumber") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += "order by t.receiptNumber asc";
				if (request.getSortField().equals("receiptNumber") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += "order by t.receiptNumber desc";
				if (request.getSortField().equals("receiptDate") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += "order by t.receiptDate asc";
				if (request.getSortField().equals("receiptDate") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += "order by t.receiptDate desc";
				if (request.getSortField().equals("entityCodeName") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += "order by t.entityCodeName asc";
				if (request.getSortField().equals("entityCodeName") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += "order by t.entityCodeName desc";
				if (request.getSortField().equals("forEntityCodeName") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += "order by t.forEntityCodeName asc";
				if (request.getSortField().equals("forEntityCodeName") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += "order by t.forEntityCodeName desc";
				if (request.getSortField().equals("description") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += "order by t.description asc";
				if (request.getSortField().equals("description") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += "order by t.description desc";
				if (request.getSortField().equals("modeofPaymentCode") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += "order by t.paymentMode asc";
				if (request.getSortField().equals("modeofPaymentCode") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += "order by t.paymentMode desc";
				if (request.getSortField().equals("amountCollected") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += "order by t.collectedAmount asc";
				if (request.getSortField().equals("amountCollected") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += "order by t.collectedAmount desc";
				if (request.getSortField().equals("adjustedAmount") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += "order by t.adjustedAmount asc";
				if (request.getSortField().equals("adjustedAmount") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += "order by t.adjustedAmount desc";
				if (request.getSortField().equals("adviseDate") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += "order by t.documentDate asc";
				if (request.getSortField().equals("adviseDate") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += "order by t.documentDate desc";
				if (request.getSortField().equals("documentNumber") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += "order by t.bankReferenceNumber asc";
				if (request.getSortField().equals("documentNumber") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += "order by t.bankReferenceNumber desc";

				mainQuery += " limit " + pageSize + " offset " + start + ";";
			}
			log.info("Main query... " + mainQuery);
			List<SundryTransferDTO> sundryTransferDTOList = new ArrayList<SundryTransferDTO>();
			listofData = jdbcTemplate.queryForList(mainQuery);
			for (Map<String, Object> data : listofData) {
				SundryTransferDTO sundryTransferDTO = new SundryTransferDTO();
				if (data.get("voucherId") != null)
					sundryTransferDTO.setId(Long.parseLong(data.get("voucherId").toString()));
				if (data.get("entityId") != null)
					sundryTransferDTO.setEntityId(Long.parseLong(data.get("entityId").toString()));
				if (data.get("glAccountId") != null)
					sundryTransferDTO.setGlAccountId(Long.parseLong(data.get("glAccountId").toString()));
				if (data.get("receiptNumber") != null)
					sundryTransferDTO.setReceiptNumber(data.get("receiptNumber").toString());
				if (data.get("collectedAmount") != null)
					sundryTransferDTO.setAmountCollected(Double.valueOf(data.get("collectedAmount").toString()));
				if (data.get("orgCodeName") != null)
					sundryTransferDTO.setOrgCodeName(data.get("orgCodeName").toString());
				if (data.get("adjustedAmount") != null)
					sundryTransferDTO.setAdjustedAmount(Double.valueOf(data.get("adjustedAmount").toString()));
				if (data.get("paymentMode") != null)
					sundryTransferDTO.setModeofPaymentCode(data.get("paymentMode").toString());
				if (data.get("description") != null)
					sundryTransferDTO.setDescription(data.get("description").toString());
				if (data.get("entityCodeName") != null)
					sundryTransferDTO.setEntityCodeName(data.get("entityCodeName").toString());
				if (data.get("forEntityCodeName") != null)
					sundryTransferDTO.setForEntityCodeName(data.get("forEntityCodeName").toString());
				if (data.get("receiptDate") != null)
					sundryTransferDTO.setReceiptDate((Date) data.get("receiptDate"));
				if (data.get("documentDate") != null) {
					sundryTransferDTO.setAdviseDate((Date) data.get("documentDate"));
				}
				if (data.get("bankReferenceNumber") != null) {
					sundryTransferDTO.setDocumentNumber((String) data.get("bankReferenceNumber"));
				}
				if (data.get("registerId") != null) {
					sundryTransferDTO.setRegisterId((Long) data.get("registerId"));
				}
				sundryTransferDTOList.add(sundryTransferDTO);
			}

			log.info("Total records present in SundryTransferData..." + total);
			log.info("Total records present in SundryTransferData..." + sundryTransferDTOList.size());
			baseDTO.setResponseContents(sundryTransferDTOList);
			baseDTO.setTotalRecords(total);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.info("Exception at SundryTransferService in loadAllSundryData Method : " + e);
		}
		return baseDTO;
	}

	public BaseDTO adjustment(SundryTransferDTO sundryTransferDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {

			String adjustmentFor = sundryTransferDTO.getAdjustmentFor();
			log.info("SundryTransferService. adjustment() - adjustmentFor: " + adjustmentFor);

			if ("ADJUSTMENT".equals(adjustmentFor)) {
				log.info(" PaymentMode Code : " + sundryTransferDTO.getModeofPaymentCode());
				log.info(" entbity id : " + sundryTransferDTO.getEntityId());
				PaymentMode paymentMode = paymentModeRepository
						.getByCodeWithActiveStatus(sundryTransferDTO.getModeofPaymentCode());
				String paymentCode = paymentMode != null ? paymentMode.getCode() : null;
				log.info(" Step 1 : paymentCode" + paymentCode);
				Long paymentModeId = paymentMode != null ? paymentMode.getId() : null;
				EntityMaster entityMaster = entityMasterRepository
						.getRegionIdByEntityId(sundryTransferDTO.getEntityId());
				Long regionId = entityMaster != null ? entityMaster.getId() : null;
				log.info(" Step 2 : ");
				// if (paymentMode == null) {
				// AppUtil.addError("Payment Mode Not Found.");
				// return null;
				// }

				log.info(" Step 3 : ");
				if (entityMaster != null) {
					log.info(" Step 4 : ");
					log.info(" PaymentMode Id : " + paymentModeId);
					log.info(" PaymentMode Code : " + sundryTransferDTO.getModeofPaymentCode());
					log.info(" Region Id : " + regionId);
					log.info(
							" entityMaster Id : " + entityMaster != null ? entityMaster.getEntityMasterRegion() : null);

					ApplicationQuery applicationQuery = applicationQueryRepository
							.findByQueryName("SUNDRY_TRANSFER_ADJUSTMENT");
					// String Query = "select v.id as VoucherId,pd.id as paymentDetailsId,v.name as
					// transactionName,v.entity_id as entityId,em.code||'/'|| em.name as
					// entityCodeName,v.net_amount as netAmount,v.reference_number_prefix as
					// receiptNumber,vd.gl_account_id as glAccountId,v.created_date as
					// receiptDate,v.payment_mode_id as paymentModeId,pm.code as
					// paymentMode,v.narration as description from voucher v join voucher_details vd
					// on v.id=vd.voucher_id join payment_details pd on v.id=pd.voucher_id join
					// entity_master em on em.id=v.entity_id join (select id from entity_master em
					// where region_id="+entityMaster.getId()+" union select id from entity_master
					// em where id="+entityMaster.getId()+") etm on etm.id=em.id join payment_mode
					// pm on pm.id=v.payment_mode_id where pd.voucher_id=v.id and pd.effective_date
					// is null and pd.effective_status is null and v.payment_mode_id="
					// + paymentMode.getId() + " and v.voucher_type_id=4";
					String Query = applicationQuery.getQueryContent().trim();
					log.info("Final -> Query : " + Query);

					Query = Query.replaceAll(":regionId", String.valueOf(regionId));
					Query = Query.replaceAll(":paymentModeId", String.valueOf(paymentModeId));
					log.info("Final -> Query : " + Query);

					List<SundryTransferDTO> sundryTransferDTOList = new ArrayList<SundryTransferDTO>();
					List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
					log.info("Final -> Query : " + Query);
					listofData = jdbcTemplate.queryForList(Query);
					for (Map<String, Object> data : listofData) {
						SundryTransferDTO sundryTransferDTOObj = new SundryTransferDTO();
						if (data.get("voucherId") != null)
							sundryTransferDTOObj.setId(Long.parseLong(data.get("voucherId").toString()));
						if (data.get("paymentDetailsId") != null)
							sundryTransferDTOObj
									.setPaymentDetailsId(Long.parseLong(data.get("paymentDetailsId").toString()));
						if (data.get("transactionName") != null)
							sundryTransferDTOObj.setTransactionName(data.get("transactionName").toString());
						if (data.get("entityId") != null)
							sundryTransferDTOObj.setEntityId(Long.parseLong(data.get("entityId").toString()));
						// if (data.get("glAccountId") != null)
						// sundryTransferDTOObj.setGlAccountId(Long.parseLong(data.get("glAccountId").toString()));

						if (data.get("referenceNumber") != null)
							sundryTransferDTOObj.setReferenceNumber(data.get("referenceNumber").toString());

						if (data.get("receiptNumber") != null)
							sundryTransferDTOObj.setReceiptNumber(data.get("receiptNumber").toString());
						if (data.get("netAmount") != null)
							sundryTransferDTOObj.setAmountCollected(Double.valueOf(data.get("netAmount").toString()));
						if (data.get("entityCodeName") != null)
							sundryTransferDTOObj.setEntityCodeName(data.get("entityCodeName").toString());
						if (data.get("paymentMode") != null)
							sundryTransferDTOObj.setModeofPaymentCode(data.get("paymentMode").toString());
						if (data.get("description") != null)
							sundryTransferDTOObj.setDescription(data.get("description").toString());
						if (data.get("receiptDate") != null)
							sundryTransferDTOObj.setReceiptDate((Date) data.get("receiptDate"));
						sundryTransferDTOList.add(sundryTransferDTOObj);
					}
					baseDTO.setResponseContents(sundryTransferDTOList);
					baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				} else {
					log.info("============================ step5 ============================");
					baseDTO.setStatusCode(ErrorDescription.ENTITY_MASTER_ID_ISNULL.getCode());
				}
			} else {
				baseDTO = loadRITRecordList(sundryTransferDTO);
			}

		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.info("Exception at SundryTransferService in loadAllSundryData Method : " + e);
		}
		return baseDTO;
	}

	private BaseDTO loadRITRecordList(SundryTransferDTO sundryTransferDTO) throws Exception {
		log.info("SundryTransferService. loadRITRecordList() - START");
		List<SundryTransferDTO> sundryTransferDTOList = null;
		BaseDTO baseDTO = new BaseDTO();
		if (sundryTransferDTO != null) {

			UserMaster loginUser = loginService.getCurrentUser();
			Long loginUserId = loginUser == null ? null : loginUser.getId();
			log.info("SundryTransferService. loadRITRecordList() - loginUserId: " + loginUserId);

			EntityMaster loginEntityMaster = entityMasterRepository.getEntityInfoByLoggedInUser(loginUserId);
			Long loginEntityId = loginEntityMaster == null ? null : loginEntityMaster.getId();
			log.info("SundryTransferService. loadRITRecordList() - loginEntityId: " + loginEntityId);

			String modeofReceipt = null;
			String adjustmentFor = sundryTransferDTO.getAdjustmentFor();
			log.info("SundryTransferService. loadRITRecordList() - adjustmentFor: " + adjustmentFor);

			String modeOfCollection = sundryTransferDTO.getModeofPaymentCode();
			log.info("SundryTransferService. loadRITRecordList() - modeOfCollection: " + modeOfCollection);

			if (SundryTransferKey.CASH_TO_BANK.equals(adjustmentFor)) {
				modeofReceipt = in.gov.cooptex.core.enums.PaymentMode.CASH.toString();
			} else if (SundryTransferKey.CARD_TO_BANK.equals(adjustmentFor)) {
				modeofReceipt = in.gov.cooptex.core.enums.PaymentMode.CARD.toString();
			} else if (SundryTransferKey.CHEQUE_TO_BANK.equals(adjustmentFor)) {
				modeofReceipt = in.gov.cooptex.core.enums.PaymentMode.CHEQUE.toString();
			}

			if (modeOfCollection != null && modeofReceipt != null
					&& !modeofReceipt.toUpperCase().equals(modeOfCollection.toUpperCase())) {
				baseDTO.setResponseContents(sundryTransferDTOList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				return baseDTO;
			}

			Integer monthNumber = sundryTransferDTO.getMonthNumber();
			Integer yearNumber = sundryTransferDTO.getYearNumber();

			log.info("SundryTransferService. loadRITRecordList() - modeofReceipt: " + modeofReceipt);
			log.info("SundryTransferService. loadRITRecordList() - monthNumber: " + monthNumber);
			log.info("SundryTransferService. loadRITRecordList() - yearNumber: " + yearNumber);

			String queryContent = JdbcUtil.getApplicationQueryContent(jdbcTemplate, "RIT_ADJUSTMENT_DATA_SQL");
			if (StringUtils.isEmpty(queryContent)) {
				throw new Exception("Query not found - RIT_ADJUSTMENT_DATA_SQL");
			}

			queryContent = StringUtils.replace(queryContent, ":regionId", String.valueOf(loginEntityId));
			queryContent = StringUtils.replace(queryContent, ":monthNumber", String.valueOf(monthNumber));
			queryContent = StringUtils.replace(queryContent, ":yearNumber", String.valueOf(yearNumber));
			queryContent = StringUtils.replace(queryContent, ":modeofReceipt",
					AppUtil.getValueWithSingleQuote(modeofReceipt));

			log.info("SundryTransferService. loadRITRecordList() - queryContent: " + queryContent);

			sundryTransferDTOList = new ArrayList<SundryTransferDTO>();
			List<Map<String, Object>> mapList = jdbcTemplate.queryForList(queryContent);
			int mapListSize = mapList != null ? mapList.size() : 0;
			log.info("SundryTransferService. loadRITRecordList() - mapListSize: " + mapListSize);

			if (CollectionUtils.isNotEmpty(mapList)) {
				for (Map<String, Object> dataMap : mapList) {
					SundryTransferDTO sundryTransferDTOObj = new SundryTransferDTO();

					if (dataMap.get("entity_id") != null) {
						sundryTransferDTOObj.setEntityId(Long.valueOf(dataMap.get("entity_id").toString()));
					}

					if (dataMap.get("showroom_code") != null && dataMap.get("showroom_name") != null) {
						sundryTransferDTOObj.setEntityCodeName(new StringBuilder().append(dataMap.get("showroom_code"))
								.append(" / ").append(dataMap.get("showroom_name")).toString());
					}

					if (dataMap.get("transaction_name") != null) {
						sundryTransferDTOObj.setTransactionName(dataMap.get("transaction_name").toString());
					}

					if (dataMap.get("rit_sundry_record_id") != null) {
						sundryTransferDTOObj
								.setPaymentDetailsId(Long.valueOf(dataMap.get("rit_sundry_record_id").toString()));
					}

					if (dataMap.get("transaction_date") != null) {
						sundryTransferDTOObj.setTransactionDate((Date) dataMap.get("transaction_date"));
					}

					Double ritClosing = 0D;
					if (dataMap.get("RIT_Closing") != null) {
						ritClosing = Double.valueOf(dataMap.get("RIT_Closing").toString());
						sundryTransferDTOObj.setRitClosing(ritClosing);
					}

					Double alreadyAdjustedAmount = 0D;
					if (dataMap.get("already_adjusted_amount") != null) {
						alreadyAdjustedAmount = Double.valueOf(dataMap.get("already_adjusted_amount").toString());
						sundryTransferDTOObj.setAdjustedAmount(alreadyAdjustedAmount);
					}

					Double balanceAmount = ritClosing - alreadyAdjustedAmount;
					balanceAmount = balanceAmount < 0D ? 0D : balanceAmount;
					sundryTransferDTOObj.setAmountToBeAdjusted(balanceAmount);
					sundryTransferDTOObj.setBalanceAmountToBeAdjusted(0D);

					if (dataMap.get("doc_ref_number") != null) {
						sundryTransferDTOObj.setDocumentNumber(dataMap.get("doc_ref_number").toString());
					}

					if (dataMap.get("mode_of_receipt") != null) {
						sundryTransferDTOObj.setModeofPaymentCode(dataMap.get("mode_of_receipt").toString());
					}

					if (dataMap.get("amount_transfer_details_id") != null) {
						sundryTransferDTOObj.setAmountTransferDetailsId(
								Long.valueOf(dataMap.get("amount_transfer_details_id").toString()));
					}

					sundryTransferDTOList.add(sundryTransferDTOObj);
				}
			}

			baseDTO.setResponseContents(sundryTransferDTOList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} else {
			log.error("Input is empty");
		}
		return baseDTO;
	}

	private static final String UPDATE_PAYMENT_DETAILS_SQL = "UPDATE payment_details SET effective_date=NULL,effective_status=NULL WHERE voucher_id IN "
			+ "(SELECT voucher_id FROM journal_voucher_entries WHERE journal_voucher_id=${journalVoucherId})";

	private static final String DELETE_JOURNAL_VOUCHER_ENTRIES_SQL = "DELETE FROM journal_voucher_entries WHERE journal_voucher_id IN "
			+ "(SELECT id FROM journal_voucher WHERE id=${journalVoucherId} AND name=${name})";

	private static final String DELETE_JOURNAL_VOUCHER_SQL = "DELETE FROM journal_voucher WHERE id=${journalVoucherId} AND name=${name}";

	public BaseDTO deleteAdjustmentDetails(SundryTransferDTO sundryTransferDTO) {
		log.info("SundryTransferService. deleteAdjustmentDetails() - START");
		BaseDTO baseDTO = new BaseDTO();
		try {
			Long journalVoucherId = sundryTransferDTO.getJournalVoucherId();
			log.info("SundryTransferService. deleteAdjustmentDetails() - journalVoucherId: " + journalVoucherId);

			if (journalVoucherId != null) {

				String updateSQL = StringUtils.replace(UPDATE_PAYMENT_DETAILS_SQL, "${journalVoucherId}",
						String.valueOf(journalVoucherId));
				jdbcTemplate.execute(updateSQL);
				log.info("SundryTransferService. deleteAdjustmentDetails() - Payment details updated successfully.");

				String deleteEntriesSQL = StringUtils.replace(DELETE_JOURNAL_VOUCHER_ENTRIES_SQL, "${journalVoucherId}",
						String.valueOf(journalVoucherId));
				deleteEntriesSQL = StringUtils.replace(deleteEntriesSQL, "${name}",
						AppUtil.getValueWithSingleQuote(VoucherTypeDetails.SUNDRY_ADJUSTMENT.toString()));
				jdbcTemplate.execute(deleteEntriesSQL);
				log.info(
						"SundryTransferService. deleteAdjustmentDetails() - Journal voucher entries deleted successfully.");

				String deleteJVSQL = StringUtils.replace(DELETE_JOURNAL_VOUCHER_SQL, "${journalVoucherId}",
						String.valueOf(journalVoucherId));
				deleteJVSQL = StringUtils.replace(deleteJVSQL, "${name}",
						AppUtil.getValueWithSingleQuote(VoucherTypeDetails.SUNDRY_ADJUSTMENT.toString()));
				jdbcTemplate.execute(deleteJVSQL);
				log.info("SundryTransferService. deleteAdjustmentDetails() - Journal voucher updated successfully.");

			}
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.info("Exception at SundryTransferService. deleteAdjustmentDetails()", e);
		}
		log.info("SundryTransferService. deleteAdjustmentDetails() - END");
		return baseDTO;
	}

	/**
	 * 
	 * @param sundryTransferDTO
	 * @return
	 */
	public BaseDTO loadJournalEntriesData(SundryTransferDTO sundryTransferDTO) {
		log.info("SundryTransferService. loadJournalEntriesData() - START");
		BaseDTO baseDTO = new BaseDTO();
		List<SundryTransferDTO> sundryTransferDTOList = null;
		try {

			Long voucherId = sundryTransferDTO.getId();
			log.info("SundryTransferService. loadJournalEntriesData() - voucherId: " + voucherId);

			ApplicationQuery applicationQuery = applicationQueryRepository
					.findByQueryName("GET_JOURNAL_VOUCHER_ENTRIES_SQL");

			if (applicationQuery == null) {
				baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getErrorCode());
				return baseDTO;
			}

			String query = applicationQuery.getQueryContent().trim();
			query = StringUtils.replace(query, ":voucherId", String.valueOf(voucherId));
			log.info("SundryTransferService. loadJournalEntriesData() - query: " + query);

			List<Map<String, Object>> listofData = jdbcTemplate.queryForList(query);
			if (CollectionUtils.isNotEmpty(listofData)) {

				sundryTransferDTOList = new ArrayList<SundryTransferDTO>();
				for (Map<String, Object> data : listofData) {
					SundryTransferDTO sundryTransferDTOObj = new SundryTransferDTO();

					if (data.get("adjusted_voucher_id") != null) {
						sundryTransferDTOObj
								.setAdjustedVoucherId(Long.parseLong(data.get("adjusted_voucher_id").toString()));
					}

					if (data.get("jv_number") != null) {
						sundryTransferDTOObj.setJvNumber(data.get("jv_number").toString());
					}

					if (data.get("referenceNumber") != null) {
						sundryTransferDTOObj.setReferenceNumber(data.get("referenceNumber").toString());
					}
					if (data.get("entityCodeName") != null) {
						sundryTransferDTOObj.setEntityCodeName(data.get("entityCodeName").toString());
					}
					if (data.get("paymentMode") != null) {
						sundryTransferDTOObj.setModeofPaymentCode(data.get("paymentMode").toString());
					}
					if (data.get("jv_date") != null) {
						sundryTransferDTOObj.setJvDate((Date) data.get("jv_date"));
					}

					if (data.get("jv_amount") != null) {
						sundryTransferDTOObj.setAdjustedAmount(Double.valueOf(data.get("jv_amount").toString()));
					}

					if (data.get("gl_code_name") != null) {
						sundryTransferDTOObj.setGlAccountCode(data.get("gl_code_name").toString());
					}

					if (data.get("journal_voucher_id") != null) {
						sundryTransferDTOObj
								.setJournalVoucherId(Long.parseLong(data.get("journal_voucher_id").toString()));
					}

					sundryTransferDTOList.add(sundryTransferDTOObj);
				}
				baseDTO.setResponseContents(sundryTransferDTOList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.info("Exception at SundryTransferService in loadJournalEntriesData Method : ", e);
		}
		return baseDTO;
	}

	/**
	 * 
	 * @param sundryTransferDTO
	 * @return
	 */
	public BaseDTO saveSundryAdjustment(SundryTransferDTO sundryTransferDTO) {
		BaseDTO baseDTO = new BaseDTO();
		log.info("SundryTransferBean. saveSundryAdjustment() - START");
		try {
			JournalVoucher journalVoucher = new JournalVoucher();
			if (sundryTransferDTO.getAdjustmentFor().equalsIgnoreCase("ADJUSTMENT")) {
				Double adjustmentAmount = sundryTransferDTO.getSundryAdjustmentList().stream()
						.filter(o -> o.getAmountCollected() != null)
						.collect(Collectors.summingDouble(SundryTransferDTO::getAmountCollected));
				journalVoucher.setJvAmount(adjustmentAmount);

				// check Validation
				List<JournalVoucher> journalVoucherList = journalVoucherRepository
						.getByVoucherId(sundryTransferDTO.getId());

				Double alreadyCollectedAmount = 0.0;
				if (journalVoucherList.size() > 0) {

					alreadyCollectedAmount = journalVoucherList.stream().filter(o -> o.getJvAmount() != null)
							.collect(Collectors.summingDouble(JournalVoucher::getJvAmount));
				}

				alreadyCollectedAmount = alreadyCollectedAmount + journalVoucher.getJvAmount();

				if (sundryTransferDTO.getTotalAmountCollected() >= alreadyCollectedAmount) {
					log.info(" ######################## Validation Success ======================================= ");
					journalVoucher.setName(VoucherTypeDetails.SUNDRY_ADJUSTMENT.name());
					UserMaster userMaster = userMasterRepository.findOne(loginService.getCurrentUser().getId());
					EntityMaster entityMaster = entityMasterRepository
							.findByUserRegion(loginService.getCurrentUser().getId());
					journalVoucher.setEntityMaster(entityMaster);
					journalVoucher.setCreatedBy(userMaster);
					journalVoucher.setCreatedDate(new Date());
					journalVoucher.setJvDate(sundryTransferDTO.getReceiptDate());

					// JVReference
					String jvReferenceNumber = generateJvReferenceNumber(entityMaster.getCode());
					if (jvReferenceNumber != null) {
						String refNumber = jvReferenceNumber.split("#")[0];
						journalVoucher.setJvNumberPrefix(refNumber);
						journalVoucher.setRefNumber(refNumber);
						Long jvNumber = Long.valueOf(jvReferenceNumber.split("#")[1]);
						journalVoucher.setJvNumber(jvNumber);
					}

					Voucher voucher = voucherRepository.findOne(sundryTransferDTO.getId());
					journalVoucher.setVoucher(voucher);
					log.info(" journalVoucher : " + journalVoucher);
					JournalVoucher journalVouchersaveObj = journalVoucherRepository.save(journalVoucher);
					log.info(" Selected PaymentDetailsId ====sundryTransferDTO===:  "
							+ sundryTransferDTO.getPaymentDetailsId());

					if (sundryTransferDTO.getAdjustmentFor().equalsIgnoreCase("ADJUSTMENT")) {
						SundryTransferDTO sundryAdjustmentDTO = new SundryTransferDTO();
						sundryAdjustmentDTO.setAmountCollected(sundryTransferDTO.getAmountCollected());
						sundryAdjustmentDTO.setGlAccountId(sundryTransferDTO.getGlAccountId());
						sundryAdjustmentDTO.setId(sundryTransferDTO.getId());
						sundryAdjustmentDTO.setPaymentDetailsId(sundryTransferDTO.getPaymentDetailsId());

						for (SundryTransferDTO sundryadjustmentObj : sundryTransferDTO.getSundryAdjustmentList()) {
							JournalVoucherEntries journalVoucherEntries = new JournalVoucherEntries();
							journalVoucherEntries.setCreatedBy(userMaster);
							journalVoucherEntries.setJvAspect(sundryTransferDTO.getPostingType());
							journalVoucherEntries.setCreatedDate(new Date());
							journalVoucherEntries.setJournalVoucher(journalVouchersaveObj);
							journalVoucherEntries.setJvAmount(sundryadjustmentObj.getAmountCollected());
							Voucher voucherObj = voucherRepository.findOne(sundryadjustmentObj.getId());
							journalVoucherEntries.setVoucher(voucherObj);
							if (sundryadjustmentObj.getGlAccountId() != null) {
								GlAccount glAccount = glAccountRepository
										.findById(sundryadjustmentObj.getGlAccountId());
								journalVoucherEntries.setGlAccount(glAccount);
							}
							log.info(" Selected PaymentDetailsId :  " + sundryadjustmentObj.getPaymentDetailsId());
							if (sundryadjustmentObj.getPaymentDetailsId() != null) {
								PaymentDetails paymentDetailsObj = paymentDetailsRepository
										.findOne(sundryadjustmentObj.getPaymentDetailsId());
								// paymentDetailsObj.setEffectiveDate(new Date());
								paymentDetailsObj.setEffectiveDate(sundryTransferDTO.getReceiptDate());
								paymentDetailsObj.setEffectiveStatus("ADJUSTED");
								log.info(" PaymentDetailsId :  " + paymentDetailsObj.getId());
								paymentDetailsRepository.save(paymentDetailsObj);
							}
							journalVoucherEntriesRepository.save(journalVoucherEntries);
						}
					} else {
						log.info("################################ <------- saveSundryAdjustment End ------> ");
						JournalVoucherEntries journalVoucherEntries = new JournalVoucherEntries();
						journalVoucherEntries.setCreatedBy(userMaster);
						journalVoucherEntries.setCreatedDate(new Date());
						journalVoucherEntries.setJournalVoucher(journalVouchersaveObj);
						Long glAccountId = sundryTransferDTO.getSundryAdjustmentList().get(0).getGlAccountId();
						journalVoucherEntries.setJvAmount(sundryTransferDTO.getAmountCollected());
						GlAccount glAccount = glAccountRepository.findById(glAccountId);
						journalVoucherEntries.setGlAccount(glAccount);
						journalVoucherEntries.setJvAspect(sundryTransferDTO.getPostingType());
						journalVoucherEntries.setDtsDate(sundryTransferDTO.getDtsDate());
						journalVoucherEntries.setReferenceNumber(sundryTransferDTO.getReferenceNumber());
						journalVoucherEntriesRepository.save(journalVoucherEntries);
					}
				} else {
					log.info(
							" ######################## Validation Failed Duplicate ======================================= ");
				}

			} else if (sundryTransferDTO.getAdjustmentFor().equalsIgnoreCase("SHOWROOM_CREDIT_SUNDRY_TRANSFER")) {
				/**
				 * CREDIT_SUNDRY_TO_SHOWROOM
				 */
				saveCreditSundryToShowroom(sundryTransferDTO);

			} else if (sundryTransferDTO.getAdjustmentFor().equalsIgnoreCase("REGION_CREDIT_SUNDRY_TRANSFER")) {
				/**
				 * CREDIT_SUNDRY_TO_REGION
				 */
				saveCreditSundryToRegion(sundryTransferDTO);

			} else if (sundryTransferDTO.getAdjustmentFor().equalsIgnoreCase("EXCESS_CREDIT_SUNDRY_TRANSFER")) {
				/**
				 * SUNDRY_TO_EXCESS_COLLECTION
				 */
				saveCreditSundryToExcess(sundryTransferDTO);

			} else if (sundryTransferDTO.getAdjustmentFor().equalsIgnoreCase("HO_COLLECTION_TO_RO_TRANSFER")) {
				/**
				 * HO_COLLECTION_TO_RO_TRANSFER
				 */
				saveHOCollectionToROTransfer(sundryTransferDTO);

			} else {

				UserMaster currentUser = loginService.getCurrentUser();
				Long userId = currentUser == null ? null : currentUser.getId();
				// Double adjustmentAmount =
				// sundryTransferDTO.getSundryAdjustmentList().stream()
				// .filter(o -> o.getRitClosing() != null)
				// .collect(Collectors.summingDouble(SundryTransferDTO::getRitClosing));
				Double adjustmentAmount = sundryTransferDTO.getSundryAdjustmentList().stream()
						.filter(o -> o.getAmountToBeAdjusted() != null)
						.collect(Collectors.summingDouble(SundryTransferDTO::getAmountToBeAdjusted));
				journalVoucher.setJvAmount(adjustmentAmount);

				List<JournalVoucher> journalVoucherList = journalVoucherRepository
						.getByVoucherId(sundryTransferDTO.getId());

				Double alreadyCollectedAmount = 0.0;
				if (journalVoucherList.size() > 0) {
					alreadyCollectedAmount = journalVoucherList.stream().filter(o -> o.getJvAmount() != null)
							.collect(Collectors.summingDouble(JournalVoucher::getJvAmount));
				}

				alreadyCollectedAmount = alreadyCollectedAmount + journalVoucher.getJvAmount();

				if (sundryTransferDTO.getTotalAmountCollected() >= alreadyCollectedAmount) {

					journalVoucher.setName(VoucherTypeDetails.SUNDRY_ADJUSTMENT.name());
					UserMaster userMaster = userMasterRepository.findOne(userId);
					EntityMaster entityMaster = entityMasterRepository.findByUserRegion(userId);
					journalVoucher.setEntityMaster(entityMaster);
					journalVoucher.setCreatedBy(userMaster);
					journalVoucher.setCreatedDate(new Date());
					journalVoucher.setJvDate(sundryTransferDTO.getReceiptDate());

					// JVReference
					String jvReferenceNumber = generateJvReferenceNumber(entityMaster.getCode());
					if (jvReferenceNumber != null) {
						String refNumber = jvReferenceNumber.split("#")[0];
						journalVoucher.setJvNumberPrefix(refNumber);
						journalVoucher.setRefNumber(refNumber);
						Long jvNumber = Long.valueOf(jvReferenceNumber.split("#")[1]);
						journalVoucher.setJvNumber(jvNumber);
					}

					if (sundryTransferDTO.getId() != null) {
						journalVoucher.setVoucher(voucherRepository.findOne(sundryTransferDTO.getId()));
					}

					JournalVoucher journalVouchersaveObj = journalVoucherRepository.save(journalVoucher);

					for (SundryTransferDTO sundryadjustmentObj : sundryTransferDTO.getSundryAdjustmentList()) {
						if (sundryadjustmentObj.getAmountToBeAdjusted() != null) {
							JournalVoucherEntries journalVoucherEntries = new JournalVoucherEntries();
							journalVoucherEntries.setCreatedBy(userMaster);
							journalVoucherEntries.setJvAspect(sundryTransferDTO.getPostingType());
							journalVoucherEntries.setDtsDate(sundryadjustmentObj.getTransactionDate());
							journalVoucherEntries.setCreatedDate(new Date());
							journalVoucherEntries.setJournalVoucher(journalVouchersaveObj);
							journalVoucherEntries.setJvAmount(sundryadjustmentObj.getAmountToBeAdjusted());
							journalVoucherEntries.setReferenceNumber(sundryadjustmentObj.getDocumentNumber());

							if (sundryadjustmentObj.getEntityId() != null) {
								journalVoucherEntries.setEntityMaster(
										entityMasterRepository.findOne(sundryadjustmentObj.getEntityId()));
							}

							if (sundryadjustmentObj.getGlAccountId() != null) {
								GlAccount glAccount = glAccountRepository
										.findById(sundryadjustmentObj.getGlAccountId());
								journalVoucherEntries.setGlAccount(glAccount);
							}
							if (sundryadjustmentObj.getAmountTransferDetailsId() != null) {
								journalVoucherEntries.setAmountTransferDetails(amountTransferDetailsRepository
										.findOne(sundryadjustmentObj.getAmountTransferDetailsId()));
							}
							journalVoucherEntries.setTransactionName(sundryadjustmentObj.getTransactionName());
							journalVoucherEntriesRepository.save(journalVoucherEntries);
						}
					}
				}
			}

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.info("Exception at SundryTransferService in saveSundryAdjustment Method : ", e);
		}
		log.info(" <------- saveSundryAdjustment End ------> ");
		return baseDTO;
	}

	private void saveCreditSundryToExcess(SundryTransferDTO sundryTransferDTOObj) throws Exception {

		Date currentDate = new Date();
		Double adjustmentAmount = sundryTransferDTOObj.getAdjustedAmount();

		CollectionRegister registerObj = null;
		Long registerId = sundryTransferDTOObj.getRegisterId();
		if (registerId != null) {
			registerObj = collectionRegisterRepository.findOne(registerId);
		}
		JournalVoucher journalVoucher = new JournalVoucher();
		journalVoucher.setName(VoucherTypeDetails.SUNDRY_TO_EXCESS_COLLECTION.name());
		UserMaster currentUser = loginService.getCurrentUser();
		UserMaster userMaster = userMasterRepository.findOne(currentUser.getId());
		EntityMaster entityMaster = entityMasterRepository.findByUserRegion(currentUser.getId());
		journalVoucher.setEntityMaster(entityMaster);
		journalVoucher.setCreatedBy(userMaster);
		journalVoucher.setCreatedDate(currentDate);
		journalVoucher.setJvDate(sundryTransferDTOObj.getReceiptDate());
		journalVoucher.setJvAmount(adjustmentAmount);

		// JVReference
		String jvReferenceNumber = generateCreditSundryJvReferenceNumber(entityMaster.getCode(),
				journalVoucher.getName());
		if (jvReferenceNumber != null) {
			String refNumber = jvReferenceNumber.split("#")[0];
			journalVoucher.setJvNumberPrefix(refNumber);
			journalVoucher.setRefNumber(refNumber);
			Long jvNumber = Long.valueOf(jvReferenceNumber.split("#")[1]);
			journalVoucher.setJvNumber(jvNumber);
		}

		OrganizationMaster organizationMaster = null;
		if (registerObj != null && registerObj.getId() != null) {
			journalVoucher.setCollectionRegister(collectionRegisterRepository.findOne(registerObj.getId()));
			organizationMaster = registerObj.getOrganizationMaster();
		}
		Voucher voucher = voucherRepository.findOne(sundryTransferDTOObj.getId());
		journalVoucher.setVoucher(voucher);
		log.info(" journalVoucher : " + (journalVoucher != null ? journalVoucher.getId() : null));
		JournalVoucher journalVouchersaveObj = journalVoucherRepository.save(journalVoucher);

		for (SundryTransferDTO sundryadjustmentObj : sundryTransferDTOObj.getSundryAdjustmentList()) {
			JournalVoucherEntries journalVoucherEntries = new JournalVoucherEntries();
			journalVoucherEntries.setCreatedBy(userMaster);
			journalVoucherEntries.setJvAspect(sundryadjustmentObj.getPostingType());
			journalVoucherEntries.setCreatedDate(currentDate);
			journalVoucherEntries.setJournalVoucher(journalVouchersaveObj);
			journalVoucherEntries.setJvAmount(sundryadjustmentObj.getAdjustedAmount());
			// Voucher voucherObj = voucherRepository.findOne(sundryadjustmentObj.getId());
			// journalVoucherEntries.setVoucher(voucherObj);
			if (sundryadjustmentObj.getGlAccountId() != null) {
				GlAccount glAccount = glAccountRepository.findById(sundryadjustmentObj.getGlAccountId());
				journalVoucherEntries.setGlAccount(glAccount);
			}
			if (organizationMaster != null && organizationMaster.getId() != null) {
				journalVoucherEntries
						.setOrganizationMaster(organizationMasterRepository.findOne(organizationMaster.getId()));
			}
			journalVoucherEntriesRepository.save(journalVoucherEntries);
		}

		//
		JournalVoucherLog journalVoucherLogObj = new JournalVoucherLog();
		journalVoucherLogObj.setJournalVoucher(journalVouchersaveObj);
		journalVoucherLogObj.setStage(ApprovalStage.FINALAPPROVED);
		journalVoucherLogRepository.save(journalVoucherLogObj);

		//
		JournalVoucherNote journalVouchernoteObj = new JournalVoucherNote();
		journalVouchernoteObj.setFinalApproval(true);
		journalVouchernoteObj.setNote("SELF APPROVED");
		journalVouchernoteObj.setJournalVoucher(journalVouchersaveObj);
		UserMaster frwdto = userMasterRepository.findOne(loginService.getCurrentUser().getId());
		journalVouchernoteObj.setUserMaster(frwdto);
		journalVoucherNoteRepository.save(journalVouchernoteObj);

	}

	@Transactional
	private void saveHOCollectionToROTransfer(SundryTransferDTO sundryTransferDTOObj) throws Exception {

		Date currentDate = new Date();
		// Double adjustmentAmount =
		// sundryTransferDTOObj.getSundryAdjustmentList().stream()
		// .filter(o -> o.getAdjustedAmount() != null)
		// .collect(Collectors.summingDouble(SundryTransferDTO::getAdjustedAmount));
		Double adjustmentAmount = sundryTransferDTOObj.getAdjustedAmount();

		// check Validation
		List<JournalVoucher> journalVoucherList = journalVoucherRepository.getByVoucherId(sundryTransferDTOObj.getId());

		Double alreadyCollectedAmount = 0.0;
		if (journalVoucherList.size() > 0) {
			alreadyCollectedAmount = journalVoucherList.stream().filter(o -> o.getJvAmount() != null)
					.collect(Collectors.summingDouble(JournalVoucher::getJvAmount));
		}
		alreadyCollectedAmount = alreadyCollectedAmount + adjustmentAmount;

		String sundryDepositGlCode = JdbcUtil.getAppConfigValue(jdbcTemplate,
				AppConfigKey.SUNDRY_DEPOSIT_GL_ACCOUNT_CODE);
		if (StringUtils.isEmpty(sundryDepositGlCode)) {
			throw new Exception("Sundry Deposit GL code not found.");
		}

		String sundryGlCode = JdbcUtil.getAppConfigValue(jdbcTemplate, AppConfigKey.SUNDRY_GL_ACCOUNT_CODE);
		if (StringUtils.isEmpty(sundryGlCode)) {
			throw new Exception("Sundry GL code not found.");
		}

		if (sundryTransferDTOObj.getTotalAmountCollected() >= alreadyCollectedAmount) {
			log.info(" ######################## Validation Success ======================================= ");

			JournalVoucher journalVoucher = new JournalVoucher();
			journalVoucher.setName(VoucherTypeDetails.HO_COLLECTION_TO_RO_TRANSFER.name());
			UserMaster currentUser = loginService.getCurrentUser();
			UserMaster userMaster = userMasterRepository.findOne(currentUser.getId());
			EntityMaster entityMaster = entityMasterRepository.findByUserRegion(currentUser.getId());
			journalVoucher.setEntityMaster(entityMaster);
			journalVoucher.setCreatedBy(userMaster);
			journalVoucher.setCreatedDate(currentDate);
			journalVoucher.setJvDate(sundryTransferDTOObj.getReceiptDate());
			journalVoucher.setJvAmount(adjustmentAmount);

			// JVReference
			String jvReferenceNumber = generateCreditSundryJvReferenceNumber(entityMaster.getCode(),
					journalVoucher.getName());
			if (jvReferenceNumber != null) {
				String refNumber = jvReferenceNumber.split("#")[0];
				journalVoucher.setJvNumberPrefix(refNumber);
				journalVoucher.setRefNumber(refNumber);
				Long jvNumber = Long.valueOf(jvReferenceNumber.split("#")[1]);
				journalVoucher.setJvNumber(jvNumber);
			}

			Voucher voucher = voucherRepository.findOne(sundryTransferDTOObj.getId());
			journalVoucher.setVoucher(voucher);
			log.info(" journalVoucher : " + (journalVoucher != null ? journalVoucher.getId() : null));
			JournalVoucher journalVouchersaveObj = journalVoucherRepository.save(journalVoucher);

			for (SundryTransferDTO sundryadjustmentObj : sundryTransferDTOObj.getSundryAdjustmentList()) {
				JournalVoucherEntries journalVoucherEntries = new JournalVoucherEntries();
				journalVoucherEntries.setCreatedBy(userMaster);
				journalVoucherEntries.setJvAspect(sundryadjustmentObj.getPostingType());
				journalVoucherEntries.setCreatedDate(currentDate);
				journalVoucherEntries.setJournalVoucher(journalVouchersaveObj);
				journalVoucherEntries.setJvAmount(sundryadjustmentObj.getAdjustedAmount());
				// Voucher voucherObj = voucherRepository.findOne(sundryadjustmentObj.getId());
				// journalVoucherEntries.setVoucher(voucherObj);

				if ("DEBIT".equalsIgnoreCase(sundryadjustmentObj.getPostingType())) {
					GlAccount glAccount = glAccountRepository.findByCode(sundryDepositGlCode);
					journalVoucherEntries.setGlAccount(glAccount);
				} else {
					if (sundryadjustmentObj.getGlAccountId() != null) {
						GlAccount glAccount = glAccountRepository.findById(sundryadjustmentObj.getGlAccountId());
						journalVoucherEntries.setGlAccount(glAccount);
					}
				}

				journalVoucherEntriesRepository.save(journalVoucherEntries);
			}

			UserMaster frwdto = userMasterRepository.findOne(loginService.getCurrentUser().getId());
			//
			JournalVoucherLog journalVoucherLogObj = new JournalVoucherLog();
			journalVoucherLogObj.setJournalVoucher(journalVouchersaveObj);
			journalVoucherLogObj.setStage(ApprovalStage.FINALAPPROVED);
			journalVoucherLogRepository.save(journalVoucherLogObj);

			//
			JournalVoucherNote journalVouchernoteObj = new JournalVoucherNote();
			journalVouchernoteObj.setFinalApproval(true);
			journalVouchernoteObj.setNote("SELF APPROVED");
			journalVouchernoteObj.setJournalVoucher(journalVouchersaveObj);
			journalVouchernoteObj.setUserMaster(frwdto);
			journalVoucherNoteRepository.save(journalVouchernoteObj);

			String paymentModeCode = in.gov.cooptex.core.enums.PaymentMode.ADJUSTMENT.toString();
			String name = VoucherTypeDetails.RO_COLLECTION_HO_TRANSFER.toString();
			Voucher voucherSaveObj = new Voucher();
			EntityMaster voucherEntityMaster = null;
			if (sundryTransferDTOObj.getEntityId() != null) {
				voucherEntityMaster = entityMasterRepository.findOne(sundryTransferDTOObj.getEntityId());
				voucherSaveObj.setEntityMaster(voucherEntityMaster);
			}
			voucherSaveObj.setName(name);
			voucherSaveObj.setNarration(name);
			String voucherNumber = null;
			String referenceNumber = null;
			if (voucherEntityMaster != null) {
				voucherNumber = generateVoucherReferenceNumber(voucherEntityMaster.getCode(), name);
				if (voucherNumber != null) {
					String[] voucherRefList = voucherNumber.split("#");
					referenceNumber = voucherRefList[0];
					voucherSaveObj.setReferenceNumber(Long.parseLong(voucherRefList[1]));
					voucherSaveObj.setReferenceNumberPrefix(new StringBuilder().append(referenceNumber).append("-")
							.append(voucherSaveObj.getReferenceNumber()).toString());
				}
			}
			VoucherType voucherType = voucherTypeRepository.findByName(VoucherTypeCons.JOURNAL);
			PaymentMode paymentMode = paymentModeRepository.findByPaymentMode(paymentModeCode);
			voucherSaveObj.setPaymentMode(paymentMode);
			voucherSaveObj.setVoucherType(voucherType);
			voucherSaveObj.setNetAmount(adjustmentAmount);
			voucherSaveObj.setPaymentFor(PaymentType.OTHERS.toString());
			voucherSaveObj.setFromDate(currentDate);
			voucherSaveObj.setToDate(currentDate);

			List<VoucherDetails> voucherDetList = new ArrayList<>();

			VoucherDetails voucherDet = new VoucherDetails();
			voucherDet.setAmount(adjustmentAmount);
			voucherDet.setVoucher(voucherSaveObj);
			if (StringUtils.isNotEmpty(sundryGlCode)) {
				GlAccount glAccount = glAccountRepository.findByCode(sundryGlCode);
				voucherDet.setGlAccount(glAccount);
			}
			voucherDetList.add(voucherDet);

			voucherSaveObj.setVoucherDetailsList(voucherDetList);
			Voucher voucherUpdateObj = voucherRepository.save(voucherSaveObj);

			// Payment
			Payment pay = new Payment();
			pay.setEntityMaster(voucherEntityMaster);
			pay.setNarration(name);

			if (voucherNumber != null) {
				String[] voucherRefList = voucherNumber.split("#");
				referenceNumber = voucherRefList[0];
				pay.setPaymentNumber(Long.parseLong(voucherRefList[1]));
				pay.setPaymentNumberPrefix(new StringBuilder().append(referenceNumber).append("-")
						.append(voucherSaveObj.getReferenceNumber()).toString());
			}
			List<PaymentDetails> paymentDetails = new ArrayList<>();
			PaymentDetails payDet = new PaymentDetails();
			payDet.setPaymentTypeMaster(paymentTypeMasterRepository.getOthersrPaymentType());
			payDet.setPayment(pay);
			payDet.setVoucher(voucherUpdateObj);
			payDet.setPaymentMethod(paymentMethodRepository.findByCode(paymentModeCode));
			payDet.setPaymentCategory(PaymentCategory.PAID_IN);
			payDet.setAmount(adjustmentAmount);

			paymentDetails.add(payDet);
			pay.setPaymentDetailsList(paymentDetails);
			paymentRepository.save(pay);

			// CREDIT_REGION_TO_SUNDRY

			JournalVoucher journalVoucherObj = new JournalVoucher();
			journalVoucherObj.setName(VoucherTypeDetails.RO_COLLECTION_HO_TRANSFER.name());
			journalVoucherObj.setEntityMaster(voucherEntityMaster);
			journalVoucherObj.setCreatedBy(userMaster);
			journalVoucherObj.setCreatedDate(currentDate);
			journalVoucherObj.setJvDate(sundryTransferDTOObj.getReceiptDate());
			journalVoucherObj.setJvAmount(adjustmentAmount);

			// JVReference
			String jvReferenceNumberObj = generateCreditSundryJvReferenceNumber(voucherEntityMaster.getCode(),
					journalVoucherObj.getName());
			if (jvReferenceNumberObj != null) {
				String refNumber = jvReferenceNumberObj.split("#")[0];
				journalVoucherObj.setJvNumberPrefix(refNumber);
				journalVoucherObj.setRefNumber(refNumber);
				Long jvNumber = Long.valueOf(jvReferenceNumberObj.split("#")[1]);
				journalVoucherObj.setJvNumber(jvNumber);
			}

			Voucher regionToSundryvoucher = voucherRepository.findOne(sundryTransferDTOObj.getId());
			journalVoucherObj.setVoucher(regionToSundryvoucher);
			log.info("HO_COLLECTION_TO_RO_TRANSFER -  journalVoucherObj : "
					+ (journalVoucherObj != null ? journalVoucherObj.getId() : null));
			JournalVoucher journalVoucherUpdateObj = journalVoucherRepository.save(journalVoucherObj);

			for (SundryTransferDTO sundryadjustmentObj : sundryTransferDTOObj.getSundryAdjustmentList()) {
				JournalVoucherEntries journalVoucherEntries = new JournalVoucherEntries();
				journalVoucherEntries.setCreatedBy(userMaster);
				if ("DEBIT".equalsIgnoreCase(sundryadjustmentObj.getPostingType())) {
					journalVoucherEntries.setJvAspect("CREDIT");
				} else if ("CREDIT".equalsIgnoreCase(sundryadjustmentObj.getPostingType())) {
					journalVoucherEntries.setJvAspect("DEBIT");
				}
				if (sundryadjustmentObj.getGlAccountId() != null) {
					GlAccount glAccount = glAccountRepository.findById(sundryadjustmentObj.getGlAccountId());
					journalVoucherEntries.setGlAccount(glAccount);
				}
				journalVoucherEntries.setCreatedDate(currentDate);
				journalVoucherEntries.setJournalVoucher(journalVoucherUpdateObj);
				journalVoucherEntries.setJvAmount(sundryadjustmentObj.getAdjustedAmount());
				// Voucher voucherObj = voucherRepository.findOne(sundryadjustmentObj.getId());
				// journalVoucherEntries.setVoucher(voucherObj);
				journalVoucherEntriesRepository.save(journalVoucherEntries);
			}

			//
			JournalVoucherLog journalVoucherLog = new JournalVoucherLog();
			journalVoucherLog.setJournalVoucher(journalVoucherUpdateObj);
			journalVoucherLog.setStage(ApprovalStage.FINALAPPROVED);
			journalVoucherLogRepository.save(journalVoucherLog);

			//
			JournalVoucherNote journalVouchernote = new JournalVoucherNote();
			journalVouchernote.setFinalApproval(true);
			journalVouchernote.setNote("SELF APPROVED");
			journalVouchernote.setJournalVoucher(journalVoucherUpdateObj);
			journalVouchernote.setUserMaster(frwdto);
			journalVoucherNoteRepository.save(journalVouchernote);
		} else {
			log.info(" ######################## Validation Failed Duplicate ======================================= ");
		}
	}

	private void saveCreditSundryToRegion(SundryTransferDTO sundryTransferDTOObj) throws Exception {

		Date currentDate = new Date();
		Double adjustmentAmount = sundryTransferDTOObj.getAdjustedAmount();

		String sundryGlCode = JdbcUtil.getAppConfigValue(jdbcTemplate, AppConfigKey.SUNDRY_GL_ACCOUNT_CODE);
		if (StringUtils.isEmpty(sundryGlCode)) {
			throw new Exception("Sundry GL code not found.");
		}

		// check Validation

		CollectionRegister registerObj = null;
		Long registerId = sundryTransferDTOObj.getRegisterId();
		if (registerId != null) {
			registerObj = collectionRegisterRepository.findOne(registerId);
		}
		JournalVoucher journalVoucher = new JournalVoucher();
		journalVoucher.setName(VoucherTypeDetails.CREDIT_SUNDRY_TO_REGION.name());
		UserMaster currentUser = loginService.getCurrentUser();
		UserMaster userMaster = userMasterRepository.findOne(currentUser.getId());
		EntityMaster entityMaster = entityMasterRepository.findByUserRegion(currentUser.getId());
		journalVoucher.setEntityMaster(entityMaster);
		journalVoucher.setCreatedBy(userMaster);
		journalVoucher.setCreatedDate(currentDate);
		journalVoucher.setJvDate(sundryTransferDTOObj.getReceiptDate());
		journalVoucher.setJvAmount(adjustmentAmount);

		// JVReference
		String jvReferenceNumber = generateCreditSundryJvReferenceNumber(entityMaster.getCode(),
				journalVoucher.getName());
		if (jvReferenceNumber != null) {
			String refNumber = jvReferenceNumber.split("#")[0];
			journalVoucher.setJvNumberPrefix(refNumber);
			journalVoucher.setRefNumber(refNumber);
			Long jvNumber = Long.valueOf(jvReferenceNumber.split("#")[1]);
			journalVoucher.setJvNumber(jvNumber);
		}
		OrganizationMaster organizationMaster = null;
		if (registerObj != null && registerObj.getId() != null) {
			journalVoucher.setCollectionRegister(collectionRegisterRepository.findOne(registerObj.getId()));
			organizationMaster = registerObj.getOrganizationMaster();
		}
		Voucher voucher = voucherRepository.findOne(sundryTransferDTOObj.getId());
		journalVoucher.setVoucher(voucher);
		log.info(" journalVoucher : " + (journalVoucher != null ? journalVoucher.getId() : null));
		JournalVoucher journalVouchersaveObj = journalVoucherRepository.save(journalVoucher);

		for (SundryTransferDTO sundryadjustmentObj : sundryTransferDTOObj.getSundryAdjustmentList()) {
			JournalVoucherEntries journalVoucherEntries = new JournalVoucherEntries();
			journalVoucherEntries.setCreatedBy(userMaster);
			journalVoucherEntries.setJvAspect(sundryadjustmentObj.getPostingType());
			journalVoucherEntries.setCreatedDate(currentDate);
			journalVoucherEntries.setJournalVoucher(journalVouchersaveObj);
			journalVoucherEntries.setJvAmount(sundryadjustmentObj.getAdjustedAmount());
			// Voucher voucherObj = voucherRepository.findOne(sundryadjustmentObj.getId());
			// journalVoucherEntries.setVoucher(voucherObj);
			if (sundryadjustmentObj.getGlAccountId() != null) {
				GlAccount glAccount = glAccountRepository.findById(sundryadjustmentObj.getGlAccountId());
				journalVoucherEntries.setGlAccount(glAccount);
			}
			if (organizationMaster != null && organizationMaster.getId() != null) {
				journalVoucherEntries
						.setOrganizationMaster(organizationMasterRepository.findOne(organizationMaster.getId()));
			}
			journalVoucherEntriesRepository.save(journalVoucherEntries);
		}

		//
		JournalVoucherLog journalVoucherLog = new JournalVoucherLog();
		journalVoucherLog.setJournalVoucher(journalVouchersaveObj);
		journalVoucherLog.setStage(ApprovalStage.FINALAPPROVED);
		journalVoucherLogRepository.save(journalVoucherLog);

		//
		JournalVoucherNote journalVouchernote = new JournalVoucherNote();
		journalVouchernote.setFinalApproval(true);
		journalVouchernote.setNote("SELF APPROVED");
		journalVouchernote.setJournalVoucher(journalVouchersaveObj);
		UserMaster frwdto = userMasterRepository.findOne(loginService.getCurrentUser().getId());
		journalVouchernote.setUserMaster(frwdto);
		journalVoucherNoteRepository.save(journalVouchernote);

		String paymentModeCode = in.gov.cooptex.core.enums.PaymentMode.ADJUSTMENT.toString();
		String name = VoucherTypeDetails.CREDIT_SUNDRY_TO_REGION.toString();
		Voucher voucherSaveObj = new Voucher();
		EntityMaster voucherEntityMaster = entityMasterRepository.findOne(sundryTransferDTOObj.getEntityId());
		if (sundryTransferDTOObj.getEntityId() != null) {
			voucherSaveObj.setEntityMaster(voucherEntityMaster);
		}
		voucherSaveObj.setName(name);
		voucherSaveObj.setNarration(name);
		String voucherNumber = null;
		String referenceNumber = null;
		if (voucherEntityMaster != null) {
			voucherNumber = generateVoucherReferenceNumber(voucherEntityMaster.getCode(), name);
			if (voucherNumber != null) {
				String[] voucherRefList = voucherNumber.split("#");
				referenceNumber = voucherRefList[0];
				voucherSaveObj.setReferenceNumber(Long.parseLong(voucherRefList[1]));
				voucherSaveObj.setReferenceNumberPrefix(new StringBuilder().append(referenceNumber).append("-")
						.append(voucherSaveObj.getReferenceNumber()).toString());
			}
		}
		VoucherType voucherType = voucherTypeRepository.findByName(VoucherTypeCons.JOURNAL);
		PaymentMode paymentMode = paymentModeRepository.findByPaymentMode(paymentModeCode);
		voucherSaveObj.setPaymentMode(paymentMode);
		voucherSaveObj.setVoucherType(voucherType);
		voucherSaveObj.setNetAmount(adjustmentAmount);
		voucherSaveObj.setPaymentFor(PaymentType.OTHERS.toString());
		voucherSaveObj.setFromDate(currentDate);
		voucherSaveObj.setToDate(currentDate);

		List<VoucherDetails> voucherDetList = new ArrayList<>();
		VoucherDetails voucherDet = new VoucherDetails();
		voucherDet.setAmount(adjustmentAmount);
		voucherDet.setVoucher(voucherSaveObj);
		if (StringUtils.isNotEmpty(sundryGlCode)) {
			GlAccount glAccount = glAccountRepository.findByCode(sundryGlCode);
			voucherDet.setGlAccount(glAccount);
		}
		voucherDetList.add(voucherDet);
		voucherSaveObj.setVoucherDetailsList(voucherDetList);

		Voucher voucherUpdateObj = voucherRepository.save(voucherSaveObj);

		// Payment
		Payment pay = new Payment();
		pay.setEntityMaster(voucherEntityMaster);
		pay.setNarration(name);

		if (voucherNumber != null) {
			String[] voucherRefList = voucherNumber.split("#");
			referenceNumber = voucherRefList[0];
			pay.setPaymentNumber(Long.parseLong(voucherRefList[1]));
			pay.setPaymentNumberPrefix(new StringBuilder().append(referenceNumber).append("-")
					.append(voucherSaveObj.getReferenceNumber()).toString());
		}
		List<PaymentDetails> paymentDetails = new ArrayList<>();
		PaymentDetails payDet = new PaymentDetails();
		payDet.setPaymentTypeMaster(paymentTypeMasterRepository.getOthersrPaymentType());
		payDet.setPayment(pay);
		payDet.setVoucher(voucherUpdateObj);
		payDet.setPaymentMethod(paymentMethodRepository.findByCode(paymentModeCode));
		payDet.setPaymentCategory(PaymentCategory.PAID_IN);
		payDet.setAmount(adjustmentAmount);

		paymentDetails.add(payDet);
		pay.setPaymentDetailsList(paymentDetails);
		paymentRepository.save(pay);

		// CREDIT_REGION_TO_SUNDRY

		JournalVoucher journalVoucherObj = new JournalVoucher();
		journalVoucherObj.setName(VoucherTypeDetails.CREDIT_REGION_TO_SUNDRY.name());
		journalVoucherObj.setEntityMaster(voucherEntityMaster);
		journalVoucherObj.setCreatedBy(userMaster);
		journalVoucherObj.setCreatedDate(currentDate);
		journalVoucherObj.setJvDate(sundryTransferDTOObj.getReceiptDate());
		journalVoucherObj.setJvAmount(adjustmentAmount);

		// JVReference
		String jvReferenceNumberObj = generateCreditSundryJvReferenceNumber(voucherEntityMaster.getCode(),
				journalVoucherObj.getName());
		if (jvReferenceNumberObj != null) {
			String refNumber = jvReferenceNumberObj.split("#")[0];
			journalVoucherObj.setJvNumberPrefix(refNumber);
			journalVoucherObj.setRefNumber(refNumber);
			Long jvNumber = Long.valueOf(jvReferenceNumberObj.split("#")[1]);
			journalVoucherObj.setJvNumber(jvNumber);
		}
		if (registerObj != null && registerObj.getId() != null) {
			journalVoucherObj.setCollectionRegister(collectionRegisterRepository.findOne(registerObj.getId()));
		}
		if (voucherUpdateObj != null && voucherUpdateObj.getId() != null) {
			Voucher regionToSundryvoucher = voucherRepository.findOne(voucherUpdateObj.getId());
			journalVoucherObj.setVoucher(regionToSundryvoucher);
		}
		log.info("CREDIT_REGION_TO_SUNDRY -  journalVoucherObj : "
				+ (journalVoucherObj != null ? journalVoucherObj.getId() : null));
		JournalVoucher journalVoucherUpdateObj = journalVoucherRepository.save(journalVoucherObj);

		for (SundryTransferDTO sundryadjustmentObj : sundryTransferDTOObj.getSundryAdjustmentList()) {
			JournalVoucherEntries journalVoucherEntries = new JournalVoucherEntries();
			journalVoucherEntries.setCreatedBy(userMaster);
			if ("DEBIT".equalsIgnoreCase(sundryadjustmentObj.getPostingType())) {
				journalVoucherEntries.setJvAspect("CREDIT");
			} else if ("CREDIT".equalsIgnoreCase(sundryadjustmentObj.getPostingType())) {
				journalVoucherEntries.setJvAspect("DEBIT");
			}
			journalVoucherEntries.setCreatedDate(currentDate);
			journalVoucherEntries.setJournalVoucher(journalVoucherUpdateObj);
			journalVoucherEntries.setJvAmount(sundryadjustmentObj.getAdjustedAmount());
			// Voucher voucherObj = voucherRepository.findOne(sundryadjustmentObj.getId());
			// journalVoucherEntries.setVoucher(voucherObj);
			if (sundryadjustmentObj.getGlAccountId() != null) {
				GlAccount glAccount = glAccountRepository.findById(sundryadjustmentObj.getGlAccountId());
				journalVoucherEntries.setGlAccount(glAccount);
			}
			journalVoucherEntriesRepository.save(journalVoucherEntries);
		}

		//
		JournalVoucherLog journalVoucherLogObj = new JournalVoucherLog();
		journalVoucherLogObj.setJournalVoucher(journalVoucherUpdateObj);
		journalVoucherLogObj.setStage(ApprovalStage.FINALAPPROVED);
		journalVoucherLogRepository.save(journalVoucherLogObj);

		//
		JournalVoucherNote journalVouchernoteObj = new JournalVoucherNote();
		journalVouchernoteObj.setFinalApproval(true);
		journalVouchernoteObj.setNote("SELF APPROVED");
		journalVouchernoteObj.setJournalVoucher(journalVoucherUpdateObj);
		journalVouchernoteObj.setUserMaster(frwdto);
		journalVoucherNoteRepository.save(journalVouchernoteObj);

	}

	/**
	 * 
	 * @param toEntityCode
	 * @return
	 */
	private synchronized String generateVoucherReferenceNumber(Integer toEntityCode, String name) {
		String referenceNumber = null;
		referenceNumber = JV_PREFIX + toEntityCode + AppUtil.getCurrentMonthString() + AppUtil.getCurrentYearString();
		log.info("referenceNumber: " + referenceNumber);
		String sql = "SELECT COALESCE(max(v.reference_number),0) AS count FROM voucher v WHERE date_part('Year',v.created_date)=date_part('Year',current_date) AND v.name=${name};";
		sql = StringUtils.replace(sql, "${name}", AppUtil.getValueWithSingleQuote(name));
		Integer maxValue = null;
		Map<String, Object> data = jdbcTemplate.queryForMap(sql);
		if (data != null) {
			if (data.get("count") != null) {
				maxValue = Integer.valueOf(data.get("count").toString());
				log.info(" maxValue : " + maxValue);
				maxValue = maxValue + 1;
			}
		}
		referenceNumber = referenceNumber + "#" + String.valueOf(maxValue);
		log.info(" Main - Reference : " + referenceNumber);

		return referenceNumber;
	}

	/**
	 * 
	 * @param sundryTransferDTOObj
	 */
	private void saveCreditSundryToShowroom(SundryTransferDTO sundryTransferDTOObj) throws Exception {

		JournalVoucher journalVoucher = new JournalVoucher();

		Date currentDate = new Date();
		Double adjustmentAmount = sundryTransferDTOObj.getAdjustedAmount();
		journalVoucher.setJvAmount(adjustmentAmount);

		// check Validation

		CollectionRegister registerObj = null;
		Long registerId = sundryTransferDTOObj.getRegisterId();
		if (registerId != null) {
			registerObj = collectionRegisterRepository.findOne(registerId);
		}

		journalVoucher.setName(VoucherTypeDetails.CREDIT_SUNDRY_TO_SHOWROOM.name());
		UserMaster currentUser = loginService.getCurrentUser();
		UserMaster userMaster = userMasterRepository.findOne(currentUser.getId());
		EntityMaster entityMaster = entityMasterRepository.findByUserRegion(currentUser.getId());
		journalVoucher.setEntityMaster(entityMaster);
		journalVoucher.setCreatedBy(userMaster);
		journalVoucher.setCreatedDate(currentDate);
		journalVoucher.setJvDate(sundryTransferDTOObj.getReceiptDate());

		// JVReference
		String jvReferenceNumber = generateCreditSundryJvReferenceNumber(entityMaster.getCode(),
				journalVoucher.getName());
		if (jvReferenceNumber != null) {
			String refNumber = jvReferenceNumber.split("#")[0];
			journalVoucher.setJvNumberPrefix(refNumber);
			journalVoucher.setRefNumber(refNumber);
			Long jvNumber = Long.valueOf(jvReferenceNumber.split("#")[1]);
			journalVoucher.setJvNumber(jvNumber);
		}
		OrganizationMaster organizationMaster = null;
		if (registerObj != null && registerObj.getId() != null) {
			journalVoucher.setCollectionRegister(collectionRegisterRepository.findOne(registerObj.getId()));
			organizationMaster = registerObj.getOrganizationMaster();
		}
		Voucher voucher = voucherRepository.findOne(sundryTransferDTOObj.getId());
		journalVoucher.setVoucher(voucher);
		log.info(" journalVoucher : " + (journalVoucher != null ? journalVoucher.getId() : null));
		JournalVoucher journalVouchersaveObj = journalVoucherRepository.save(journalVoucher);

		for (SundryTransferDTO sundryadjustmentObj : sundryTransferDTOObj.getSundryAdjustmentList()) {
			JournalVoucherEntries journalVoucherEntries = new JournalVoucherEntries();
			journalVoucherEntries.setCreatedBy(userMaster);
			journalVoucherEntries.setJvAspect(sundryadjustmentObj.getPostingType());
			journalVoucherEntries.setCreatedDate(currentDate);
			journalVoucherEntries.setJournalVoucher(journalVouchersaveObj);
			journalVoucherEntries.setJvAmount(sundryadjustmentObj.getAdjustedAmount());
			// Voucher voucherObj = voucherRepository.findOne(sundryadjustmentObj.getId());
			// journalVoucherEntries.setVoucher(voucherObj);
			if (sundryadjustmentObj.getGlAccountId() != null) {
				GlAccount glAccount = glAccountRepository.findById(sundryadjustmentObj.getGlAccountId());
				journalVoucherEntries.setGlAccount(glAccount);
			}
			if (organizationMaster != null && organizationMaster.getId() != null) {
				journalVoucherEntries
						.setOrganizationMaster(organizationMasterRepository.findOne(organizationMaster.getId()));
			}
			journalVoucherEntriesRepository.save(journalVoucherEntries);
		}

		//
		JournalVoucherLog journalVoucherLogObj = new JournalVoucherLog();
		journalVoucherLogObj.setJournalVoucher(journalVouchersaveObj);
		journalVoucherLogObj.setStage(ApprovalStage.FINALAPPROVED);
		journalVoucherLogRepository.save(journalVoucherLogObj);

		//
		JournalVoucherNote journalVouchernoteObj = new JournalVoucherNote();
		journalVouchernoteObj.setFinalApproval(true);
		journalVouchernoteObj.setNote("SELF APPROVED");
		journalVouchernoteObj.setJournalVoucher(journalVouchersaveObj);
		UserMaster frwdto = userMasterRepository.findOne(loginService.getCurrentUser().getId());
		journalVouchernoteObj.setUserMaster(frwdto);
		journalVoucherNoteRepository.save(journalVouchernoteObj);

		// Collection Register

		if (registerObj != null) {
			CollectionRegister collectionRegister = new CollectionRegister();
			collectionRegister.setAmountCollected(adjustmentAmount);

			/* creditSaleRequest */
			if (registerObj.getCreditSalesRequest() != null && registerObj.getCreditSalesRequest().getId() != null) {
				CreditSalesRequest creditSalesRequestObj = creditSalesRequestRepository
						.findOne(registerObj.getCreditSalesRequest().getId());
				collectionRegister.setCreditSalesRequest(creditSalesRequestObj);
			}

			if (registerObj.getOrganizationMaster() != null
					&& registerObj.getOrganizationMaster().getOrgCode() != null) {
				OrganizationMaster organizationMasterObj = new OrganizationMaster();
				organizationMasterObj = organizationMasterRepository
						.findByCode(registerObj.getOrganizationMaster().getOrgCode().trim());
				collectionRegister.setOrganizationMaster(organizationMasterObj);
			}

			collectionRegister.setModeOfPaymentCode(registerObj.getModeOfPaymentCode());
			collectionRegister.setCreatedBy(userMaster);
			collectionRegister.setCreatedDate(currentDate);

			EntityMaster entityMasterObj1 = new EntityMaster();
			if (registerObj.getEntityMaster() != null && registerObj.getEntityMaster().getId() != null) {
				entityMasterObj1 = entityMasterRepository.findById(registerObj.getEntityMaster().getId());
			}

			String collectionNumberSequence = generateCollectionNumber(entityMaster.getCode(), userMaster);

			collectionRegister.setCollectionNumber(collectionNumberSequence);
			collectionRegister.setVersion(0L);
			collectionRegister.setEntityMaster(entityMaster);
			CollectionRegister collectionRegisterSaveObj = collectionRegisterRepository.save(collectionRegister);
			// Collection Register Details

			List<CollectionRegisterDetails> collectionRegisterDetailsList = new ArrayList<>();
			List<SundryTransferDTO> sundryToShowroomList = sundryTransferDTOObj.getSundryToShowroomList();
			if (CollectionUtils.isNotEmpty(sundryToShowroomList)) {
				for (SundryTransferDTO sundryadjustmentObj : sundryToShowroomList) {
					CollectionRegisterDetails collectionRegisterDetailsObj = new CollectionRegisterDetails();
					collectionRegisterDetailsObj.setCollectionRegister(collectionRegisterSaveObj);
					EntityMaster entityMasterObj = entityMasterRepository.findById(sundryadjustmentObj.getEntityId());
					collectionRegisterDetailsObj.setEntityMaster(entityMasterObj);
					collectionRegisterDetailsObj.setSharedAmount(sundryadjustmentObj.getAdjustedAmount());
					collectionRegisterDetailsObj.setCreatedDate(currentDate);
					collectionRegisterDetailsObj.setVersion(0L);
					collectionRegisterDetailsList.add(collectionRegisterDetailsObj);
				}
				collectionRegisterDetailsRepository.save(collectionRegisterDetailsList);
			}
		}

	}

	private synchronized String generateCollectionNumber(Integer regionCode, UserMaster userMaster) throws Exception {
		log.info("SundryTransferService. generateCollectionNumber() - START");
		String sequenceNumber = null;

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");
		SimpleDateFormat monthFormat = new SimpleDateFormat("MM");
		ApplicationQuery applicationQuery = applicationQueryRepository
				.findByQuery("COLLECT_REGISTER_COLLECTION_NUMBER");

		if (applicationQuery == null) {
			throw new Exception("COLLECT_REGISTER_COLLECTION_NUMBER Query Not available in DB");
		}
		String query = applicationQuery.getQueryContent();
		if (userMaster != null) {
			query = query.replace(":entityCode", String.valueOf(regionCode));
		}
		Map<String, Object> maxData = jdbcTemplate.queryForMap(query);
		log.info(" maxData : " + maxData.get("count"));
		Integer maxseq = maxData.get("count") == null ? 0 : Integer.valueOf(maxData.get("count").toString());
		maxseq = maxseq + 1;
		sequenceNumber = "DRN" + regionCode + "-" + dateFormat.format(new Date()) + "" + monthFormat.format(new Date())
				+ "" + maxseq;
		log.info(" sequenceNumber : " + sequenceNumber);

		log.info("SundryTransferService. generateCollectionNumber() - END");
		return sequenceNumber;
	}

	private synchronized String generateCreditSundryJvReferenceNumber(Integer entityCode, String name) {
		String referenceNumber = null;

		referenceNumber = entityCode + "-" + "JV-" + AppUtil.getCurrentMonthString() + AppUtil.getCurrentYearString();
		log.info(" Reference : " + referenceNumber);
		String sql = "select coalesce(max(jv.jv_number),0) as count from journal_voucher jv where date_part('Year',jv.created_date)=date_part('Year',current_date) "
				+ "and entity_id=(select id from entity_master where code=${entityCode}) and jv.name<>'SUNDRY_ADJUSTMENT';";
		sql = StringUtils.replace(sql, "${entityCode}", String.valueOf(entityCode));
		Integer maxValue = null;
		Map<String, Object> data = jdbcTemplate.queryForMap(sql);
		if (data != null) {
			if (data.get("count") != null) {
				maxValue = Integer.valueOf(data.get("count").toString());
				log.info(" maxValue : " + maxValue);
				maxValue = maxValue + 1;
			}
		}
		referenceNumber = referenceNumber + String.valueOf(maxValue) + "#" + String.valueOf(maxValue);
		log.info(" Main - Reference : " + referenceNumber);
		return referenceNumber;
	}

	private synchronized String generateJvReferenceNumber(Integer entityCode) {
		String referenceNumber = null;

		referenceNumber = entityCode + "-" + "JV-" + AppUtil.getCurrentMonthString() + AppUtil.getCurrentYearString();
		log.info(" Reference : " + referenceNumber);
		String sql = "select coalesce(max(jv.jv_number),0) as count from journal_voucher jv where date_part('Year',jv.created_date)=date_part('Year',current_date) and jv.name='SUNDRY_ADJUSTMENT';";
		Integer maxValue = null;
		Map<String, Object> data = jdbcTemplate.queryForMap(sql);
		if (data != null) {
			if (data.get("count") != null) {
				maxValue = Integer.valueOf(data.get("count").toString());
				log.info(" maxValue : " + maxValue);
				maxValue = maxValue + 1;
			}
		}
		referenceNumber = referenceNumber + String.valueOf(maxValue) + "#" + String.valueOf(maxValue);
		log.info(" Main - Reference : " + referenceNumber);
		return referenceNumber;
	}

	public BaseDTO getGlaccountListByAutoComplete(String codename) {
		log.info("<------- SundryTransferService getGlaccountListByAutoComplete()-->");
		log.info("<<====    GlAccount CodeName::: " + codename);
		BaseDTO response = new BaseDTO();
		try {
			List<GlAccount> accList = new ArrayList<>();
			AppConfig appConfig = appConfigRepository.findByAppKey("SUNDRY_ADJUSTMENT_GLACCOUNT_ID");
			if (appConfig != null) {
				List<Long> glAccountIdList = Arrays.asList(appConfig.getAppValue().split(",")).stream()
						.filter(o -> !o.equals("")).map(o -> Long.parseLong(o)).collect(Collectors.toList());
				log.info("<<====    GlAccount glAccountIdList::: " + appConfig.getAppValue().split(","));
				accList = glAccountRepository.loadAutoCopleteGLCodeOrName(codename.toUpperCase(), glAccountIdList);
			}
			if (accList != null && accList.size() > 0) {
				log.info(":::::::::::account List::::::::::::::" + accList.size());
				response.setResponseContents(accList);
				response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				log.info(":::::::::::account List is Null::::::::::::::");
			}

		} catch (Exception e) {
			response.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.info("<<====  exception in getGlaccountListByAutoComplete ===::::  " + e);
		}

		return response;
	}

	public BaseDTO getHeadCodeandNameById(Long id) {
		log.info("<------- SundryTransferService getHeadCodeandNameById()-->");
		BaseDTO response = new BaseDTO();
		try {
			log.info("<------- SundryTransferService Id : -->" + id);

			GlAccount account = glAccountRepository.findOne(id);
			if (account != null) {
				response.setResponseContent(account);
			}
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			response.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.info("<<====  exception in getGlaccountListByAutoComplete ===::::  " + e);
		}

		return response;
	}

}
