package in.gov.cooptex.finance.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.model.AmountTransfer;
import in.gov.cooptex.core.accounts.model.AmountTransferDetails;
import in.gov.cooptex.core.accounts.model.AmountTransferLog;
import in.gov.cooptex.core.accounts.model.AmountTransferNote;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.finance.service.OperationService;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.AmountTransferDetailsRepository;
import in.gov.cooptex.core.repository.AmountTransferRepository;
import in.gov.cooptex.core.repository.AppQueryRepository;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AmountMovementType;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.FinanceErrorCode;
import in.gov.cooptex.exceptions.LedgerPostingException;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.finance.BankInformation;
import in.gov.cooptex.finance.BankToBankDTO;
import in.gov.cooptex.finance.BankToBankResponseDTO;
import in.gov.cooptex.finance.BankToBankViewResponseDTO;
import in.gov.cooptex.finance.CashtoBankChallanDTO;
import in.gov.cooptex.finance.repository.AmountTransferLogRepository;
import in.gov.cooptex.finance.repository.AmountTransferNoteRepository;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class BankToBankService {
	@Autowired
	EntityManager em;

	@PersistenceContext
	EntityManager entityManager;
	
	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	AmountTransferLogRepository amountTransferLogRepository;

	@Autowired
	AmountTransferNoteRepository amountTransferNoteRepository;

	@Autowired
	AmountTransferDetailsRepository amountTransferDetailsRepository;

	@Autowired
	AmountTransferRepository amountTransferRepository;

	@Autowired
	ResponseWrapper responseWrapper;

	@Autowired
	LoginService loginService;

	@Autowired
	private UserMasterRepository userMasterRepository;

	@Autowired
	OperationService operationService;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	AppQueryRepository appQueryRepository;

	public BaseDTO getBankToBankDetails(PaginationDTO paginationDTO) {

		log.info("<<====   ChequeToBankService ---  getAll ====## STARTS");
		BaseDTO baseDTO = new BaseDTO();
		Integer total = 0;
		List<BankToBankResponseDTO> resultList = null;
		try {
			resultList = new ArrayList<BankToBankResponseDTO>();
			Integer start = paginationDTO.getFirst(), pageSize = paginationDTO.getPageSize();
			start = start * pageSize;
			
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();
			ApplicationQuery applicationQuery = appQueryRepository.findByQueryName(AmountMovementType.BANK_TO_BANK);
			String mainQuery = applicationQuery.getQueryContent();
			log.info("db query----------" + mainQuery);

			// mainQuery = mainQuery.replaceAll(":CASH_TO_BANK",
			// AmountMovementType.BANK_TO_BANK);
			String filterQuery = "";
			if (paginationDTO.getFilters() != null) {
				if (paginationDTO.getFilters().get("fromEntity") != null) {
					filterQuery += " and upper(emf.name) like upper('%" + paginationDTO.getFilters().get("fromEntity")
							+ "%')";
				}

				if (paginationDTO.getFilters().get("toEntity") != null) {
					filterQuery += " and upper(emt.name) like upper('%" + paginationDTO.getFilters().get("toEntity")
							+ "%')";
				}
				if (paginationDTO.getFilters().get("status") != null) {
					filterQuery += " and stage='" + paginationDTO.getFilters().get("status") + "'";
				}
				if (paginationDTO.getFilters().get("fromBranch") != null) {
					filterQuery += " and upper(bb.branch_name) like upper('%"
							+ paginationDTO.getFilters().get("fromBranch") + "%')";
				}
				if (paginationDTO.getFilters().get("toBranch") != null) {
					filterQuery += " and upper(bb1.branch_name) like upper('%"
							+ paginationDTO.getFilters().get("toBranch") + "%')";
				}
				if (paginationDTO.getFilters().get("totalBankTransfer") != null) {

					filterQuery += " and att.total_amount_transfered>= '"
							+ paginationDTO.getFilters().get("totalBankTransfer") + "'";
				}

				if (paginationDTO.getFilters().get("amountTransferBy") != null) {
					filterQuery += " and upper(emp.first_name) LIKE upper('%"
							+ paginationDTO.getFilters().get("amountTransferBy") + "%')";
					filterQuery += " or upper(emp.last_name) LIKE upper('%"
							+ paginationDTO.getFilters().get("amountTransferBy") + "%')";

				}

				if (paginationDTO.getFilters().get("createdDate") != null) {
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					Date createdDate = new Date((long) paginationDTO.getFilters().get("createdDate"));
					log.info(" closingDate Filter : " + createdDate);
					filterQuery += " and att.created_date::date='" + format.format(createdDate) + "'";
				}
			}

			UserMaster loginUser = loginService.getCurrentUser();
			Long loginUserId = loginUser == null ? null : loginUser.getId();
			log.info("BankToBankService. getBankToBankDetails() - loginUserId: " + loginUserId);

			EntityMaster loginEntityMaster = entityMasterRepository.getEntityInfoByLoggedInUser(loginUserId);
			Long loginEntityId = loginEntityMaster == null ? null : loginEntityMaster.getId();
			log.info("BankToBankService. getBankToBankDetails() - loginEntityId: " + loginEntityId);
			
			mainQuery = mainQuery.replaceAll(":entityId", String.valueOf(loginEntityId));
			mainQuery = mainQuery.replaceAll(":CURRENTUSERID", String.valueOf(loginUserId));
			
			mainQuery = mainQuery.replaceAll(":searchcriteria", filterQuery);
			log.info("after replace query------" + mainQuery);

			String countQuery = "select count(*) from (" + mainQuery + ")c";
			log.info("count query... " + countQuery);
			countData = jdbcTemplate.queryForList(countQuery);
			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					total = Integer.parseInt(data.get("count").toString().replace(",", ""));
			}
			log.info("total query... " + total + "::pageSize::>>>" + pageSize + "::start::" + start);

			if (paginationDTO.getSortField() != null && paginationDTO.getSortOrder() != null) {
				log.info("Sort Field:[" + paginationDTO.getSortField() + "] Sort Order:[" + paginationDTO.getSortOrder()
						+ "]");
				if (paginationDTO.getSortField().equals("fromBranch")
						&& paginationDTO.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by bb.branch_name asc  ";
				}
				if (paginationDTO.getSortField().equals("fromBranch")
						&& paginationDTO.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by bb.branch_name desc  ";
				}

				if (paginationDTO.getSortField().equals("toBranch")
						&& paginationDTO.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by bb1.branch_name asc  ";
				}
				if (paginationDTO.getSortField().equals("toBranch")
						&& paginationDTO.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by bb1.branch_name desc  ";
				}

				if (paginationDTO.getSortField().equals("totalBankTransfer")
						&& paginationDTO.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by att.total_amount_transfered asc ";
				}
				if (paginationDTO.getSortField().equals("totalBankTransfer")
						&& paginationDTO.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by att.total_amount_transfered desc ";
				}

				if (paginationDTO.getSortField().equals("createdDate")
						&& paginationDTO.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by att.created_date asc ";
				}
				if (paginationDTO.getSortField().equals("createdDate")
						&& paginationDTO.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by att.created_date desc ";
				}

				if (paginationDTO.getSortField().equals("amountTransferBy")
						&& paginationDTO.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by emp.first_name asc ";
				}
				if (paginationDTO.getSortField().equals("amountTransferBy")
						&& paginationDTO.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by emp.first_name desc ";
				}

				if (paginationDTO.getSortField().equals("from_entity")
						&& paginationDTO.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by emf.name asc ";
				}
				if (paginationDTO.getSortField().equals("from_entity")
						&& paginationDTO.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by emf.name desc ";
				}

				if (paginationDTO.getSortField().equals("to_entity")
						&& paginationDTO.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by emt.name asc ";
				}
				if (paginationDTO.getSortField().equals("status") && paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by stage asc ";
				if (paginationDTO.getSortField().equals("status") && paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by stage desc ";
				if (paginationDTO.getSortField().equals("to_entity")
						&& paginationDTO.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by emt.name desc ";
				}

				mainQuery += " limit " + pageSize + " offset " + start + ";";
			} else {
				mainQuery += " order by id desc limit " + pageSize + " offset " + start + ";";
			}

			log.info("filter query----------" + mainQuery);

			// mainQuery += "order by att.created_date desc";
			listofData = jdbcTemplate.queryForList(mainQuery);
			// totalRecords = listofData.size();

			log.info("amount transfer list size-------------->" + listofData.size());

			for (Map<String, Object> data : listofData) {
				/*
				 * BankToBankResponseDTO response = new BankToBankResponseDTO();
				 * response.setAmountTransferDetailsId((Long) ob[0]);
				 * response.setFromBranch((String) ob[1]); response.setAmountTransferId((Long)
				 * ob[2]); response.setTotalBankTransfer((Double) ob[3]);
				 * response.setAmountTransferBy((String) ob[4]);
				 * response.setMovementType((String) ob[6]); response.setCreatedDate((Date)
				 * ob[7]); response.setToBranch((String) ob[8]);
				 */

				BankToBankResponseDTO response = new BankToBankResponseDTO();
				if (data.get("id") != null) {
					response.setAmountTransferId(Long.valueOf(data.get("id").toString()));
				}
				if (data.get("totalamount") != null) {
					response.setTotalBankTransfer(Double.valueOf(data.get("totalamount").toString()));
				}
				if (data.get("first_name") != null && data.get("last_name") != null) {
					response.setAmountTransferBy(
							data.get("first_name").toString() + " " + data.get("last_name").toString());
				} else if (data.get("first_name") != null) {
					response.setAmountTransferBy(data.get("first_name").toString());
				}
				if (data.get("stage") != null) {
					response.setStatus(data.get("stage").toString());
				}
				if (data.get("createddate") != null) {
					response.setCreatedDate((Date) data.get("createddate"));
				}
				if (data.get("tobranchname") != null) {
					response.setToBranch(data.get("tobranchname").toString());
				}
				if (data.get("frbranchname") != null) {
					response.setFromBranch(data.get("frbranchname").toString());
				}
				if (data.get("to_entity") != null) {
					response.setToEntity(data.get("to_entity").toString());
				}
				if (data.get("from_entity") != null) {
					response.setFromEntity(data.get("from_entity").toString());
				}

				AmountTransferNote note = amountTransferNoteRepository
						.findByAmountTransferId(response.getAmountTransferId());
				if (note != null) {
					response.setNote(note.getNote());
					response.setForwardTO(note.getForwardTo().getId());
					response.setFinalApproval(note.getFinalApproval());
				}
				resultList.add(response);
			}

			log.info("final list size---------------" + resultList.size());
			log.info("Total records present in getAllSocietyRegistrationlazy..." + resultList.size());
			// baseDTO.setTotalRecords(resultList.size());
			baseDTO.setTotalRecords(total);
			baseDTO.setResponseContents(resultList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("<<====   ChequeToBankService ---  getAll ====## ENDS");
		} catch (Exception ex) {
			log.error("inside lazy method exception------");
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO getDetailsById(Long id) {
		BaseDTO baseDTO = new BaseDTO();
		Integer totalRecords = 0;
		Criteria criteria = null;

		Session session = entityManager.unwrap(Session.class);
		criteria = session.createCriteria(AmountTransferDetails.class, "amountTransferDetails");
		criteria.createAlias("amountTransferDetails.amountTransfer", "amountTransfer");
		// criteria.createAlias("amountTransfer.transferedBy", "transferedBy");
		criteria.createAlias("amountTransferDetails.fromBranch", "fromBranch");
		criteria.createAlias("fromBranch.bankBranchMaster", "bankBranchMaster");
		criteria.createAlias("fromBranch.bankAccountType", "bankAccountType");
		criteria.createAlias("bankBranchMaster.bankMaster", "bankMaster");

		criteria.createAlias("amountTransfer.transferedBy", "empMaster");
		criteria.createAlias("amountTransferDetails.toBranch", "toBranch");
		criteria.createAlias("toBranch.bankBranchMaster", "tbankBranchMaster");
		criteria.createAlias("toBranch.bankAccountType", "tbankAccountType");
		criteria.createAlias("tbankBranchMaster.bankMaster", "tbankMaster");

		criteria.createAlias("fromBranch.entityMaster", "entityfrom");
		criteria.createAlias("toBranch.entityMaster", "entityto");

		criteria.setProjection(Projections.rowCount());
		totalRecords = ((Long) criteria.uniqueResult()).intValue();
		criteria.setProjection(null);
		ProjectionList projectionList = Projections.projectionList();
		// projectionList.add(Projections.property("amountTransferLog.stage"));
		projectionList.add(Projections.property("amountTransferDetails.id"));
		projectionList.add(Projections.property("bankMaster.bankName"));
		projectionList.add(Projections.property("amountTransfer.id"));
		projectionList.add(Projections.property("amountTransfer.totalAmountTransfered"));
		projectionList.add(Projections.property("empMaster.firstName"));
		projectionList.add(Projections.property("empMaster.id"));
		projectionList.add(Projections.property("amountTransfer.movementType"));
		projectionList.add(Projections.property("amountTransfer.createdDate"));
		projectionList.add(Projections.property("tbankMaster.bankName"));
		projectionList.add(Projections.property("bankAccountType.name"));
		projectionList.add(Projections.property("tbankAccountType.name"));
		projectionList.add(Projections.property("fromBranch.accountNumber"));
		projectionList.add(Projections.property("toBranch.accountNumber"));
		projectionList.add(Projections.property("bankBranchMaster.branchName"));
		projectionList.add(Projections.property("tbankBranchMaster.branchName"));
		projectionList.add(Projections.property("bankBranchMaster.branchCode"));
		projectionList.add(Projections.property("tbankBranchMaster.branchCode"));

		projectionList.add(Projections.property("amountTransferDetails.challanNumber"));
		projectionList.add(Projections.property("amountTransferDetails.challanDate"));
		projectionList.add(Projections.property("amountTransferDetails.challanAmount"));

		// projectionList.add(Projections.property("amountTransfer.totalAmountTransfered"));
		projectionList.add(Projections.property("amountTransfer.remarks"));
		// projectionList.add(Projections.property("transferedBy.firstName"));

		projectionList.add(Projections.property("entityfrom.name"));
		projectionList.add(Projections.property("entityto.name"));
		projectionList.add(Projections.property("amountTransferDetails.bankCharge"));
		projectionList.add(Projections.property("amountTransferDetails.creditedDate"));

		if (id != null) {
			criteria.add(Restrictions.eq("amountTransfer.id", id));
		}

		criteria.setProjection(projectionList);

		List<?> resultList = criteria.list();
		if (resultList == null || resultList.isEmpty() || resultList.size() == 0) {
			log.info("Profile Master List is null or empty ");
			baseDTO.setStatusCode(ErrorDescription.PRODUCT_VARIETY_LIST_EMPTY.getCode());
			return baseDTO;
		}

		log.info("criteria list executed and the list size is  : " + resultList.size());
		List<BankToBankViewResponseDTO> results = new ArrayList<BankToBankViewResponseDTO>();
		List<CashtoBankChallanDTO> challanDTOS = new ArrayList<>();
		Iterator<?> it = resultList.iterator();

		while (it.hasNext()) {
			Object ob[] = (Object[]) it.next();
			String movement = (String) ob[6];
			String mv = AmountMovementType.BANK_TO_BANK;
			if (mv.trim().toLowerCase().equals(movement.toLowerCase().trim())) {
				BankInformation frombankInformation = new BankInformation();
				BankInformation tobankInformation = new BankInformation();
				CashtoBankChallanDTO challanDTO = new CashtoBankChallanDTO();
				BankToBankViewResponseDTO response = new BankToBankViewResponseDTO();
				frombankInformation.setBankName((String) ob[1]);
				tobankInformation.setBankName((String) ob[8]);
				response.setTransferId((Long) ob[2]);
				response.setTotalBankTransfer((Double) ob[3]);
				response.setAmountTransferBy((String) ob[4]);

				frombankInformation.setAccountType((String) ob[9]);
				tobankInformation.setAccountType((String) ob[10]);
				frombankInformation.setAccountNumber((String) ob[11]);
				tobankInformation.setAccountNumber((String) ob[12]);
				frombankInformation.setBranchName((String) ob[13]);
				tobankInformation.setBranchName((String) ob[14]);
				frombankInformation.setBranchCode((String) ob[15]);
				tobankInformation.setBranchCode((String) ob[16]);

				challanDTO.setChallanNumber((String) ob[17]);
				challanDTO.setChallanDate((Date) ob[18]);
				challanDTO.setChallanAmount((Double) ob[19]);
				challanDTO.setTransferDetailsId((Long) ob[0]);
				response.setRemarks((String) ob[20]);
				response.setFromEntity((String) ob[21]);
				response.setToEntity((String) ob[22]);
				challanDTO.setBankCharge((Double) ob[23]);
				challanDTO.setCreditedDate(ob[24] !=null ? (Date) ob[24] : null);

				response.setTototalBankCharge(challanDTO.getBankCharge());
				response.setSourceBankInformation(frombankInformation);
				response.setDestinationBankInformation(tobankInformation);
				challanDTOS.add(challanDTO);
				results.add(response);

			}
			double totalBankCharge = 0;
			for (BankToBankViewResponseDTO btobv : results) {
				btobv.setAmountTransferDetails(challanDTOS);
				if (btobv.getTototalBankCharge() != null) {
					totalBankCharge = totalBankCharge + btobv.getTototalBankCharge();
				}
			}

			baseDTO.setSumTotal(totalBankCharge);
		}

		List<Map<String, Object>> employeeData = new ArrayList<Map<String, Object>>();
		if (id != null && id != 0) {
			log.info("<<<:::::::amountTransfer::::not Null::::>>>>" + id);
			ApplicationQuery applicationQueryForlog = applicationQueryRepository
					.findByQueryName("AMOUNT_TRANSFER_LOG_EMPLOYEE_DETAILS");
			if (applicationQueryForlog == null || applicationQueryForlog.getId() == null) {
				log.info(
						"Application Query For Log Details not found for query name : AMOUNT_TRANSFER_LOG_EMPLOYEE_DETAILS");
				baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
				return baseDTO;
			}
			String logquery = applicationQueryForlog.getQueryContent().trim();
			log.info("<=========AMOUNT_TRANSFER_LOG_EMPLOYEE_DETAILS Query Content ======>" + logquery);
			logquery = logquery.replace(":amounttransId", "'" + id.toString() + "'");
			log.info("Query Content For AMOUNT_TRANSFER_LOG_EMPLOYEE_DETAILS After replaced plan id View query : "
					+ logquery);
			employeeData = jdbcTemplate.queryForList(logquery);
			log.info("<=========AMOUNT_TRANSFER_LOG_EMPLOYEE_DETAILS Employee Data======>" + employeeData);
			baseDTO.setTotalListOfData(employeeData);
		}

		baseDTO.setResponseContents(results);
		baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		baseDTO.setTotalRecords(results.size());
		return baseDTO;

	}

	public BaseDTO getBankToBankEditById(Long id) {
		log.info("<---BankToBankService.getBankToBankEditById() starts---->" + id);
		BaseDTO response = new BaseDTO();
		BankToBankDTO banktobankDTO = new BankToBankDTO();
		try {

			AmountTransfer amounttransfer = amountTransferRepository.findOne(id);
			banktobankDTO.setAmounttransfer(amounttransfer);
			banktobankDTO.setAmountTransferLog(amountTransferLogRepository.findByAmountTransferId(id));
			banktobankDTO.setAmountTransferNote(amountTransferNoteRepository.findByAmountTransferId(id));
			banktobankDTO.setAmounttransferdetailsList(amountTransferDetailsRepository.getAmountTransferList(id));
			response.setResponseContent(banktobankDTO);

			response.setResponseContent(banktobankDTO);

			List<Map<String, Object>> employeeData = new ArrayList<Map<String, Object>>();
			if (amounttransfer != null) {
				log.info("<<<:::::::amountTransfer::::not Null::::>>>>" + amounttransfer);
				ApplicationQuery applicationQueryForlog = applicationQueryRepository
						.findByQueryName("AMOUNT_TRANSFER_LOG_EMPLOYEE_DETAILS");
				if (applicationQueryForlog == null || applicationQueryForlog.getId() == null) {
					log.info(
							"Application Query For Log Details not found for query name : AMOUNT_TRANSFER_LOG_EMPLOYEE_DETAILS");
					response.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
					return response;
				}
				String logquery = applicationQueryForlog.getQueryContent().trim();
				log.info("<=========AMOUNT_TRANSFER_LOG_EMPLOYEE_DETAILS Query Content ======>" + logquery);
				logquery = logquery.replace(":amounttransId", "'" + amounttransfer.getId().toString() + "'");
				log.info("Query Content For AMOUNT_TRANSFER_LOG_EMPLOYEE_DETAILS After replaced plan id View query : "
						+ logquery);
				employeeData = jdbcTemplate.queryForList(logquery);
				log.info("<=========AMOUNT_TRANSFER_LOG_EMPLOYEE_DETAILS Employee Data======>" + employeeData);
				response.setTotalListOfData(employeeData);
			}

			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (RestException e) {
			log.error(" Rest Exception Occured ", e);
			response.setStatusCode(e.getStatusCode());
		} catch (Exception e) {
			log.info("Error while retriving getBankToBankEditById----->", e);
			response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<---BankToBankService.getBankToBankEditById() end---->");
		return responseWrapper.send(response);
	}

	public BaseDTO approveBankToBank(BankToBankResponseDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("approveChequeToBank method start=============>" + requestDTO.getAmountTransferId());
			AmountTransfer amountTransfer = amountTransferRepository.findOne(requestDTO.getAmountTransferId());

			AmountTransferNote amountTransferNote = new AmountTransferNote();
			amountTransferNote.setAmountTransfer(amountTransfer);
			amountTransferNote.setFinalApproval(requestDTO.getFinalApproval());
			amountTransferNote.setNote(requestDTO.getNote());
			amountTransferNote.setForwardTo(userMasterRepository.findOne(requestDTO.getForwardTO()));
			amountTransferNote.setCreatedBy(loginService.getCurrentUser());
			amountTransferNote.setCreatedByName(loginService.getCurrentUser().getUsername());
			amountTransferNote.setCreatedDate(new Date());
			amountTransferNoteRepository.save(amountTransferNote);
			log.info("approveChequeToBank note inserted------");

			AmountTransferLog amountTransferLog = new AmountTransferLog();
			amountTransferLog.setAmountTransfer(amountTransfer);
			amountTransferLog.setStage(requestDTO.getStatus());
			amountTransferLog.setCreatedBy(loginService.getCurrentUser());
			amountTransferLog.setCreatedByName(loginService.getCurrentUser().getUsername());
			amountTransferLog.setCreatedDate(new Date());
			amountTransferLog.setRemarks(requestDTO.getRemarks());
			log.info("approveBankToBank log inserted------");

			log.info("Account posting Start--------" + amountTransfer != null ? amountTransfer.getId() : null);
			log.info("Approval status----------" + requestDTO.getStatus());

			/*
			 * if (requestDTO.getStatus().equals(AmountMovementType.FINAL_APPROVED)) {
			 * baseDTO = operationService.amountTransferLedgerPosting(amountTransfer,
			 * requestDTO.getStatus()); }
			 */
			
			List<CashtoBankChallanDTO> amountTransferDetailsList = requestDTO.getAmountTransferDetailsList();
			if(!CollectionUtils.isEmpty(amountTransferDetailsList)) {
				for(CashtoBankChallanDTO cashtoBankChallanDTO : amountTransferDetailsList) {
					if(cashtoBankChallanDTO != null && cashtoBankChallanDTO.getTransferDetailsId() != null) {
						AmountTransferDetails amountTransferDetails = amountTransferDetailsRepository.
								findOne(cashtoBankChallanDTO.getTransferDetailsId());
						amountTransferDetails.setCreditedDate(cashtoBankChallanDTO.getCreditedDate());
						amountTransferDetailsRepository.save(amountTransferDetails);
					}
				}
			}

			if (amountTransferLog.getStage().equals(AmountMovementType.FINAL_APPROVED)) {
				log.info("::::::amountTransferLog.getStage():" + amountTransferLog.getStage());
				baseDTO = operationService.amountTransferLedgerPosting(amountTransfer, amountTransferLog.getStage());
				if (baseDTO != null && baseDTO.getStatusCode() == ErrorDescription.SUCCESS_RESPONSE.getCode()) {
					amountTransferLog = amountTransferLogRepository.save(amountTransferLog);
					baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				} else {
					baseDTO.setStatusCode(
							ErrorDescription.getError(FinanceErrorCode.ACCOUNT_POSTING_ERROR).getErrorCode());
					amountTransferDetailsRepository.deletebyamountTransferId(amountTransfer.getId());
					amountTransferRepository.delete(amountTransfer.getId());
				}
			} else {
				amountTransferLog = amountTransferLogRepository.save(amountTransferLog);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
			log.info("Account posting End--------");
		} catch (LedgerPostingException l) {
			log.error("approveBankToBank method LedgerPostingException----", l);
			baseDTO.setStatusCode(ErrorDescription.getError(FinanceErrorCode.ACCOUNT_POSTING_ERROR).getErrorCode());
		} catch (Exception e) {
			log.error("approveBankToBank method exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO rejectBankToBank(BankToBankResponseDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("rejectBankToBank method start=============>" + requestDTO.getAmountTransferId());

			AmountTransfer amountTransfer = amountTransferRepository.findOne(requestDTO.getAmountTransferId());

			AmountTransferLog amountTransferLog = new AmountTransferLog();
			amountTransferLog.setAmountTransfer(amountTransfer);
			amountTransferLog.setStage(requestDTO.getStatus());
			amountTransferLog.setCreatedBy(loginService.getCurrentUser());
			amountTransferLog.setCreatedByName(loginService.getCurrentUser().getUsername());
			amountTransferLog.setCreatedDate(new Date());
			amountTransferLog.setRemarks(requestDTO.getRemarks());
			amountTransferLogRepository.save(amountTransferLog);

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("rejectBankToBank  log inserted------");
		} catch (Exception e) {
			log.error("rejectBankToBank method exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}
}
