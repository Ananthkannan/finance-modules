package in.gov.cooptex.finance.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.cooptex.core.accounts.model.PaymentDetails;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.inventory.model.InventoryItems;
import in.gov.cooptex.core.repository.InventoryItemsRepository;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.dto.DamageGoodsDTO;
import in.gov.cooptex.finance.dto.InventoryItemsDamageDTO;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class DamageGoodsService {

	@Autowired
	InventoryItemsRepository inventoryItemsRepository;
	
	public BaseDTO generateRegularToDamageGoods(DamageGoodsDTO damageGoodsDTO) {
		log.info("<=====DamageGoodsService.generateRegularToDamageGoods======>"+damageGoodsDTO);
		BaseDTO response = new BaseDTO();
		List<InventoryItemsDamageDTO> inventoryItemDamageList = new ArrayList<>();
		try {
			Validate.objectNotNull(damageGoodsDTO.getProductCategoryId(),ErrorDescription.ERRO_PRODUCT_CATEGORY_REQ);
			Validate.objectNotNull(damageGoodsDTO.getEntityId(),ErrorDescription.BUDGET_ENTITY_REQUIRED);
			List<InventoryItems> inventoryItemsList = inventoryItemsRepository.findInventoryItemsByEntityIdAndProductId(damageGoodsDTO.getEntityId(),
					damageGoodsDTO.getProductVarietyId(),damageGoodsDTO.getProductGroupId(),damageGoodsDTO.getProductCategoryId(),new Date());
			for(InventoryItems it :inventoryItemsList) {
				InventoryItemsDamageDTO dto = new InventoryItemsDamageDTO();
				
				dto.setId(it.getId());
				dto.setQrCode(it.getQrCode());
				dto.setProductCategory(it.getProductVarietyMaster().getProductGroupMaster().getProductCategory());
				dto.setProductGroup(it.getProductVarietyMaster().getProductGroupMaster());
				dto.setProductVariety(it.getProductVarietyMaster());
				
				if(it.getDamagedQty() == null)
					dto.setStatus("Regular Goods");
				else
					dto.setStatus("Damage Goods");
				
				inventoryItemDamageList.add(dto);
			}
			response.setResponseContent(inventoryItemDamageList);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		}catch(RestException re) {
			log.error("RestException ocurred in DamageGoodsService.generateRegularToDamageGoods........"+re);
			response.setStatusCode(re.getStatusCode());
		}catch(Exception e) {
			log.error("Exception ocurred in DamageGoodsService.generateRegularToDamageGoods........"+e);
			response.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return response;
	}
	
	
	public BaseDTO moveToDamageGoods(List<Long> inventoryIdList) {
			log.info("<=====DamageGoodsService.moveToDamageGoods======>");
		BaseDTO response = new BaseDTO();
		try {
			Validate.listNotNullOrEmpty(inventoryIdList,ErrorDescription.DAMAGE_GOODS_LIST_EMPTY);
			List<InventoryItems> inventoryItemsList = inventoryItemsRepository.findByInventoryItemsId(inventoryIdList);
			for(InventoryItems it :inventoryItemsList) {
				if(it.getOpeningBalance() == null)
					it.setDamagedQty(it.getReceivedQty());
				else
					it.setDamagedQty(it.getOpeningBalance());
			}
			inventoryItemsRepository.save(inventoryItemsList);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		}catch(RestException re) {
			log.error("RestException ocurred in DamageGoodsService.moveToDamageGoods........"+re);
			response.setStatusCode(re.getStatusCode());
		}catch(Exception e) {
			log.error("Exception ocurred in DamageGoodsService.moveToDamageGoods........"+e);
			response.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return response;
	}
	
	
}
