package in.gov.cooptex.finance.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.model.UnitInspectionEntity;
import in.gov.cooptex.core.accounts.model.UnitInspectionMovingProducts;
import in.gov.cooptex.core.accounts.model.UnitInspectionOthers;
import in.gov.cooptex.core.accounts.model.UnitInspectionSales;
import in.gov.cooptex.core.accounts.model.UnitInspectionStockArrangement;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.DNPManagerDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.ShowroomManagerDTO;
import in.gov.cooptex.core.enums.IndexPageQueryName;
import in.gov.cooptex.core.finance.repository.FinancialYearRepository;
import in.gov.cooptex.core.model.AddressMaster;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.BuildingType;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.FinancialYear;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.repository.AddressMasterRepository;
import in.gov.cooptex.core.repository.AppQueryRepository;
import in.gov.cooptex.core.repository.BuildingTypeRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.ProductVarietyMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.FinanceErrorCode;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.advance.repository.UnitInspectionMovingProductsRepository;
import in.gov.cooptex.finance.advance.repository.UnitInspectionOthersRepository;
import in.gov.cooptex.finance.advance.repository.UnitInspectionRepository;
import in.gov.cooptex.finance.advance.repository.UnitInspectionSalesRepository;
import in.gov.cooptex.finance.advance.repository.UnitInspectionStockArrangementRepository;
import in.gov.cooptex.finance.dto.UnitInspectionRequestDto;
import in.gov.cooptex.finance.dto.UnitInspectionResponseDto;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class UnitInspectionService {

	@Autowired
	EmployeeMasterRepository employeeMasterRepository;

	@Autowired
	EntityMasterRepository entityMasterRepository;
	
	@Autowired
	UnitInspectionRepository unitInspectionRepository;
	
	@Autowired
	AddressMasterRepository addressMasterRepository;

	@Autowired
	ResponseWrapper responseWrapper;
	
	@Autowired
	ProductVarietyMasterRepository productVarietyMasterRepository;
	
	@Autowired
	UnitInspectionSalesRepository unitInspectionSalesRepository;
	
	@Autowired
	UnitInspectionMovingProductsRepository unitInspectionMovingProductsRepository;
	
	@Autowired
	UnitInspectionStockArrangementRepository unitInspectionStockArrangementRepository;
	
	@Autowired
	UnitInspectionOthersRepository unitInspectionOthersRepository;
	
	@Autowired
	BuildingTypeRepository buildingTypeRepository;
	
	@PersistenceContext
	EntityManager entityManager;
	
	

	public BaseDTO getEmployeeByAutoComplete(String employeeSearchName) {
		log.info("=========START UnitInspectionService.getEmployeeByAutoComplete=====");
		log.info("=========employeeSearchName is=====" + employeeSearchName);
		BaseDTO baseDto = new BaseDTO();
		try {
			if (employeeSearchName != null) {
				List<EmployeeMaster> employeeList = employeeMasterRepository.getEmployeeByAutoComplete(employeeSearchName);
				baseDto.setResponseContents(employeeList);
				baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			}
		} catch (Exception e) {
			log.info("=========Exception Occured in UnitInspectionService.getEmployeeByAutoComplete=====");
			log.info("=========Exception is=====" + e.getMessage());
		}
		log.info("=========END UnitInspectionService.getEmployeeByAutoComplete=====");
		return baseDto;
	}

	public BaseDTO saveOrUpdateUnitInspectionEntity(UnitInspectionEntity unitInspectionEntity) {
		log.info("#=========START UnitInspectionService.saveOrUpdateUnitInspectionEntity=====");
		BaseDTO baseDto=new BaseDTO();
		try {
			Validate.notNullOrEmpty(unitInspectionEntity.getInspectedBy().getId(),ErrorDescription.getError(MastersErrorCode.INSPECTION_EMP_EMPTY));
			Validate.notNullOrEmpty(unitInspectionEntity.getInspectionDate(),ErrorDescription.getError(MastersErrorCode.INSPECTION_DATE_EMPTY));
			Validate.notNullOrEmpty(unitInspectionEntity.getInspectionStartTime(),ErrorDescription.getError(MastersErrorCode.INSPECTION_START_TIME_EMPTY));
			Validate.notNullOrEmpty(unitInspectionEntity.getEntityMaster().getId(),ErrorDescription.getError(MastersErrorCode.INSPECTION_ENTITY_EMPTY));
			Validate.notNullOrEmpty(unitInspectionEntity.getShowroomManagerAvailable(),ErrorDescription.getError(MastersErrorCode.SHOWROOM_MANAGER_AVAILABLE_EMPTY));
			UnitInspectionEntity unitInspectionEntityObj = unitInspectionRepository.getByEntityIdAndInspectionDate(
					unitInspectionEntity.getEntityMaster().getId(), unitInspectionEntity.getInspectionDate());			
			if (unitInspectionEntityObj != null && unitInspectionEntityObj.getId() != null
					&& (unitInspectionEntity.getId() == null
							|| unitInspectionEntityObj.getId() != unitInspectionEntity.getId())) {
				ErrorDescription.getError(FinanceErrorCode.UNIT_INSPECTION_ALREADY_EXSISTS);
				baseDto.setStatusCode(FinanceErrorCode.UNIT_INSPECTION_ALREADY_EXSISTS);
				return baseDto;
			}
			if(unitInspectionEntity.getManager() !=null && unitInspectionEntity.getManager().getId()!=null)
			{
				log.info("=========MANAGER Id is====="+unitInspectionEntity.getManager().getId());
				EmployeeMaster manager=employeeMasterRepository.findOne(unitInspectionEntity.getManager().getId());
				unitInspectionEntity.setManager(manager);
			}
			if(unitInspectionEntity.getAddressMaster() !=null && unitInspectionEntity.getAddressMaster().getId()!=null)
			{
				log.info("=========ADDRESS Id is====="+unitInspectionEntity.getAddressMaster().getId());
				AddressMaster addressMaster=addressMasterRepository.findOne(unitInspectionEntity.getAddressMaster().getId());
				unitInspectionEntity.setAddressMaster(addressMaster);
			}
			log.info("=========Inspected By Id is====="+unitInspectionEntity.getInspectedBy().getId());
			EmployeeMaster inspectedBy=employeeMasterRepository.findOne(unitInspectionEntity.getInspectedBy().getId());
			log.info("=========Entity Id is====="+unitInspectionEntity.getEntityMaster().getId());
			EntityMaster entityMaster=entityMasterRepository.findOne(unitInspectionEntity.getEntityMaster().getId());
			unitInspectionEntity.setEntityMaster(entityMaster);
			unitInspectionEntity.setInspectedBy(inspectedBy);
			unitInspectionEntity.setTemproraryEmployee(unitInspectionEntity.getTemproraryEmployee()!=null?unitInspectionEntity.getTemproraryEmployee():0);
			unitInspectionEntity.setPermenantEmployee(unitInspectionEntity.getPermenantEmployee()!=null?unitInspectionEntity.getPermenantEmployee():0);
			unitInspectionEntity.setTotalEmployees(unitInspectionEntity.getTemproraryEmployee()+unitInspectionEntity.getPermenantEmployee());
			unitInspectionEntity.setVersion((long) 0);
			unitInspectionEntity=unitInspectionRepository.save(unitInspectionEntity);
			log.info("=========MarketingAuditEntity Saved SuccessFully=====");
			baseDto.setResponseContent(unitInspectionEntity);
			baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.info("=========Exception Occured in UnitInspectionService.saveOrUpdateUnitInspectionEntity=====");
			log.info("=========Exception is=====" + e.getMessage());
		}
		log.info("=========END UnitInspectionService.saveOrUpdateUnitInspectionEntity=====");
		return baseDto;
	}
	
	
	public BaseDTO saveOrUpdateUnitInspectionStockArrangement(UnitInspectionStockArrangement unitInspectionStockArrangement) {
		log.info("=======START UnitInspectionService.saveOrUpdateUnitInspectionStockArrangement======");
		BaseDTO baseDto=new BaseDTO();
		try
		{
			if(unitInspectionStockArrangement.getUnitInspectionEntity()!=null && unitInspectionStockArrangement.getUnitInspectionEntity().getId()!=null)
			{
				UnitInspectionEntity unitInspectionEntity=unitInspectionRepository.findOne(unitInspectionStockArrangement.getUnitInspectionEntity().getId());
				unitInspectionStockArrangement.setUnitInspectionEntity(unitInspectionEntity);
			}
			unitInspectionStockArrangement.setVersion((long) 0);
			unitInspectionStockArrangement=unitInspectionStockArrangementRepository.save(unitInspectionStockArrangement);	
			baseDto.setResponseContent(unitInspectionStockArrangement);
			baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		}catch (Exception e) {
			log.info("=======Exception Occured in UnitInspectionService.saveOrUpdateUnitInspectionStockArrangement======");
			log.info("=======Exception is======"+e.getMessage());
		}
		log.info("=======END UnitInspectionService.saveOrUpdateUnitInspectionStockArrangement======");
		return baseDto;
	}

	public BaseDTO getAllShowroomForRegion(Long regionId) {
		log.info("getAllShowroomForRegion regionId [" + regionId + "]");
		BaseDTO response = new BaseDTO();
		try {
			List<EntityMaster> showroomListForRegion = entityMasterRepository.getAllShowroomForRegion(regionId);
			if(showroomListForRegion != null) {
				log.info("getAllShowroomForRegion :: showroomListForRegion.size==> "+showroomListForRegion.size());
			}else {
				log.error("Showroom not found");
			}
			response.setResponseContent(showroomListForRegion);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());

		} catch (Exception e) {
			log.error("getAllShowroomForRegion ", e);
			response.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(response);
	}

	/**
	 * @return
	 */
	public BaseDTO getAllActiveRegions() {
		log.info("<--- Get all active regions service called --->");
		BaseDTO response = new BaseDTO();
		try {
			List<EntityMaster> regionList = entityMasterRepository.findActiveRegionalOffices();
			List<EntityMaster> responseList = new ArrayList<>();
			for (EntityMaster reg : regionList) {
				/*reg.getCreatedBy().setRegion(null);
				if (reg.getModifiedBy() != null)
					reg.getModifiedBy().setRegion(null);*/
				reg.setEntityTypeMaster(null);
				responseList.add(reg);
			}
			log.info("<---Region list size--->" + responseList.size());
			response.setResponseContents(responseList);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.error("Error while retiving active regions based on state----->", e);
			response.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(response);
	}

	public BaseDTO getProductVarietyNameAutoComplete(String productName) {
		log.info("=======START UnitInspectionService.getProductVarietyNameAutoComplete======");
		log.info("=======productName is======"+productName);
		BaseDTO baseDto=new BaseDTO();
		try
		{
			if (productName != null) {
				List<ProductVarietyMaster> productVarietyMasterList = productVarietyMasterRepository.getProductByCodeOrNameAutoComplete(productName);
				baseDto.setResponseContents(productVarietyMasterList);
				baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			}
		}catch (Exception e) {
			log.info("=======Exception Occured in UnitInspectionService.getProductVarietyNameAutoComplete======");
			log.info("=======Exception is======"+e.getMessage());
		}
		log.info("=======END UnitInspectionService.getProductVarietyNameAutoComplete======");
		return baseDto;
	}

	
	public BaseDTO saveOrUpdateUnitInspectionSales(UnitInspectionRequestDto unitInspectionRequestDto) {
		log.info("=======START UnitInspectionService.saveOrUpdateUnitInspectionSales======");
		BaseDTO baseDto=new BaseDTO();
		try
		{
			UnitInspectionEntity unitInspectionEntity = new UnitInspectionEntity();
			Boolean updateMethod=false;
			UnitInspectionSales unitInspectionSales= unitInspectionRequestDto.getUnitInspectionSales();
			if(unitInspectionSales.getId()!=null)
			{
				updateMethod=true;
			}
			List<UnitInspectionMovingProducts> fastUnitInspectionMovingProductsList=unitInspectionRequestDto.getFastUnitInspectionMovingProducts();
			List<UnitInspectionMovingProducts> slowUnitInspectionMovingProductsList=unitInspectionRequestDto.getSlowUnitInspectionMovingProducts();
			if(unitInspectionSales.getUnitInspectionEntity()!=null && unitInspectionSales.getUnitInspectionEntity().getId()!=null)
			{
				unitInspectionEntity=unitInspectionRepository.findOne(unitInspectionSales.getUnitInspectionEntity().getId());
				unitInspectionSales.setUnitInspectionEntity(unitInspectionEntity);
			}
			unitInspectionSales.setVersion((long) 0);
			unitInspectionSales=unitInspectionSalesRepository.save(unitInspectionSales);
			if(updateMethod)
			{
				List<UnitInspectionMovingProducts> dataList = unitInspectionMovingProductsRepository
						.getByUnitInspectionAndSalesId(unitInspectionSales.getUnitInspectionEntity().getId(),
								unitInspectionSales.getId());
				Iterator<UnitInspectionMovingProducts> dataListItr = dataList.iterator();
				while (dataListItr.hasNext()) {
					UnitInspectionMovingProducts untiInspectionMovingProductsData = dataListItr.next();
					unitInspectionMovingProductsRepository.delete(untiInspectionMovingProductsData);
				}
			}
			Iterator<UnitInspectionMovingProducts> fastItr=fastUnitInspectionMovingProductsList.iterator();
			while(fastItr.hasNext())
			{
				UnitInspectionMovingProducts fastData=fastItr.next();
				fastData.setUnitInspectionSales(unitInspectionSales);
				if(unitInspectionEntity!=null && unitInspectionEntity.getId()!=null)
				{
//					UnitInspectionEntity unitInspectionEntity=unitInspectionRepository.findOne(fastData.getUnitInspectionEntity().getId());
					fastData.setUnitInspectionEntity(unitInspectionEntity);
				}
				if(fastData.getProductVarietyMaster()!=null && fastData.getProductVarietyMaster().getId()!=null)
				{
					ProductVarietyMaster productvarietyMaster=productVarietyMasterRepository.findOne(fastData.getProductVarietyMaster().getId());
					fastData.setProductVarietyMaster(productvarietyMaster);
				}
				fastData.setVersion((long) 0);
				unitInspectionMovingProductsRepository.save(fastData);
			}
			Iterator<UnitInspectionMovingProducts> slowItr=slowUnitInspectionMovingProductsList.iterator();
			while(slowItr.hasNext())
			{
				UnitInspectionMovingProducts slowData=slowItr.next();
				slowData.setUnitInspectionSales(unitInspectionSales);
				if(unitInspectionEntity!=null && unitInspectionEntity.getId()!=null)
				{
//					UnitInspectionEntity unitInspectionEntity = unitInspectionRepository.findOne(slowData.getUnitInspectionEntity().getId());
					 slowData.setUnitInspectionEntity(unitInspectionEntity);
				}
				if(slowData.getProductVarietyMaster()!=null && slowData.getProductVarietyMaster().getId()!=null)
				{
					ProductVarietyMaster productvarietyMaster=productVarietyMasterRepository.findOne(slowData.getProductVarietyMaster().getId());
					slowData.setProductVarietyMaster(productvarietyMaster);
				}
				slowData.setVersion((long) 0);
				unitInspectionMovingProductsRepository.save(slowData);
			}
			baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		}catch (Exception e) {
			log.info("=======Exception Occured in UnitInspectionService.saveOrUpdateUnitInspectionSales======");
			log.info("=======Exception is======"+e.getMessage());
		}
		log.info("=======END UnitInspectionService.saveOrUpdateUnitInspectionSales======");
		return baseDto;
	}

	

	public BaseDTO saveOrUpdateOtherUnitInspection(UnitInspectionOthers unitInspectionOthers) {
		log.info("========START UnitInspectionService.saveOrUpdateOtherUnitInspection========");
		BaseDTO baseDto=new BaseDTO();
		try
		{
			if(unitInspectionOthers.getUnitInspectionEntity()!=null && unitInspectionOthers.getUnitInspectionEntity().getId()!=null)
			{
				UnitInspectionEntity unitInspectionEntity=unitInspectionRepository.findOne(unitInspectionOthers.getUnitInspectionEntity().getId());
				unitInspectionOthers.setUnitInspectionEntity(unitInspectionEntity);
			}
			if(unitInspectionOthers.getBuildingType()!=null&& unitInspectionOthers.getBuildingType().getId()!=null)
			{
				BuildingType buildingType=buildingTypeRepository.findOne(unitInspectionOthers.getBuildingType().getId());
				unitInspectionOthers.setBuildingType(buildingType);
			}
			unitInspectionOthers.setVersion((long) 0);
			unitInspectionOthers=unitInspectionOthersRepository.save(unitInspectionOthers);
			baseDto.setResponseContent(unitInspectionOthers);
			baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		}catch (Exception e) {
			log.info("========Exception Occured in UnitInspectionService.saveOrUpdateOtherMarketingAudit========");
			log.info("========Exception is========"+e.getMessage());
		}
		log.info("========END UnitInspectionService.saveOrUpdateOtherMarketingAudit========");
		
		return baseDto;
	}

	public BaseDTO getAllBuildType() {
		log.info("========START UnitInspectionService.getAllBuildType========");
		BaseDTO baseDTO = new BaseDTO();
		try{
			List<BuildingType> listBuildingType = buildingTypeRepository.getAll();
			baseDTO.setResponseContents(listBuildingType);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		}catch(Exception e){
			log.error("<<======== ERROR BuildingTypeService---- getAll ::"+e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("========END UnitInspectionService.getAllBuildType========");
		return baseDTO;
	}

	public BaseDTO getLazyLoadData(PaginationDTO paginationDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {

			log.info("========START UnitInspectionService.getLazyLoadData========");
			Session session = entityManager.unwrap(Session.class);
			Criteria criteria = session.createCriteria(UnitInspectionEntity.class, "unitInspectionEntity");
			criteria.createAlias("unitInspectionEntity.inspectedBy", "inspectBy");
			criteria.createAlias("unitInspectionEntity.entityMaster", "region");
			log.info(":: Criteria search started ::");
			if (paginationDTO.getFilters() != null) {
				String inspectBy = (String) paginationDTO.getFilters().get("inspectBy");
				if (inspectBy != null) {
					criteria.add(Restrictions.or(
							Restrictions.like("inspectBy.firstName", "%" + inspectBy.trim() + "%").ignoreCase(),
							Restrictions.like("inspectBy.lastName", "%" + inspectBy.trim() + "%").ignoreCase(),
							Restrictions.like("inspectBy.empCode", "%" + inspectBy.trim() + "%").ignoreCase()));
				}
				String region = (String) paginationDTO.getFilters().get("region");
				if (region != null) {
					if (AppUtil.isInteger(region)) {
						EntityMaster regionValue = entityMasterRepository.findByCode(Integer.parseInt(region));
						criteria.add(Restrictions.sqlRestriction("cast(this_.entity_id  as varchar) like '%"
								+ regionValue.getId().toString().trim() + "%'"));
					} else {
						criteria.add(Restrictions
								.or(Restrictions.like("region.name", "%" + region.trim() + "%").ignoreCase()));
					}
				}
				String lName = (String) paginationDTO.getFilters().get("locName");
				if (lName != null) {
					criteria.add(
							Restrictions.like("unitInspectionEntity.locName", "%" + lName.trim() + "%").ignoreCase());
				}

				if (paginationDTO.getFilters().get("status") != null) {
					Boolean status = Boolean.parseBoolean(paginationDTO.getFilters().get("status").toString());
					criteria.add(Restrictions.eq("unitInspectionEntity.status", status));
				}

				if (paginationDTO.getFilters().get("createdDate") != null) {
					Date date = new Date((long) paginationDTO.getFilters().get("createdDate"));
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = dateFormat.format(date);
					Date minDate = dateFormat.parse(strDate);
					Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
					criteria.add(
							Restrictions.conjunction().add(Restrictions.ge("unitInspectionEntity.createdDate", minDate))
									.add(Restrictions.lt("unitInspectionEntity.createdDate", maxDate)));
				}
				if (paginationDTO.getFilters().get("inspectedDate") != null) {
					Date date = new Date((long) paginationDTO.getFilters().get("inspectedDate"));
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = dateFormat.format(date);
					Date minDate = dateFormat.parse(strDate);
					Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
					criteria.add(Restrictions.conjunction()
							.add(Restrictions.ge("unitInspectionEntity.inspectionDate", minDate))
							.add(Restrictions.lt("unitInspectionEntity.inspectionDate", maxDate)));
				}

				criteria.setProjection(Projections.rowCount());
				Integer totalResult = ((Long) criteria.uniqueResult()).intValue();
				criteria.setProjection(null);

				ProjectionList projectionList = Projections.projectionList();
				projectionList.add(Projections.property("id"));
				projectionList.add(Projections.property("inspectedBy"));
				projectionList.add(Projections.property("entityMaster"));
				projectionList.add(Projections.property("inspectionDate"));
				projectionList.add(Projections.property("createdDate"));

				criteria.setProjection(projectionList);

				if (paginationDTO.getFirst() != null) {
					Integer pageNo = paginationDTO.getFirst();
					Integer pageSize = paginationDTO.getPageSize();
					if (pageNo != null && pageSize != null) {
						criteria.setFirstResult(pageNo * pageSize);
						criteria.setMaxResults(pageSize);
						log.info("PageNo : [" + pageNo + "] pageSize[" + pageSize + "]");
					}

					String sortField = paginationDTO.getSortField();
					String sortOrder = paginationDTO.getSortOrder();
					log.info("sortField outside : [" + sortField + "] sortOrder[" + sortOrder + "]");
					if (paginationDTO.getSortField() != null && paginationDTO.getSortOrder() != null) {
						log.info("sortField : [" + paginationDTO.getSortField() + "] sortOrder["
								+ paginationDTO.getSortOrder() + "]");

						if (paginationDTO.getSortField().equals("inspectedDate")) {
							sortField = "unitInspectionEntity.inspectionDate";
						} else if (sortField.equals("createdDate")) {
							sortField = "unitInspectionEntity.createdDate";
						} else if (sortField.equals("inspectBy")) {
							sortField = "inspectBy.firstName";
							sortField = "inspectBy.lastName";
							sortField = "inspectBy.empCode";
						} else if (sortField.equals("region")) {
							sortField = "region.code";
							sortField = "region.name";
						} else if (sortField.equals("id")) {
							sortField = "unitInspectionEntity.id";
						}
						if (sortOrder.equals("DESCENDING")) {
							criteria.addOrder(Order.desc(sortField));
						} else {
							criteria.addOrder(Order.asc(sortField));
						}
					} else {
						criteria.addOrder(Order.desc("unitInspectionEntity.modifiedDate"));
					}
				}
				List<?> resultList = criteria.list();
				log.info("criteria list executed and the list size is : " + resultList.size());
				if (resultList == null || resultList.isEmpty() || resultList.size() == 0) {
					log.info("unitInspectionEntity List is null or empty ");
				}
				List<UnitInspectionEntity> unitInspectionEntityList = new ArrayList<>();
				Iterator<?> it = resultList.iterator();
				while (it.hasNext()) {
					Object ob[] = (Object[]) it.next();
					UnitInspectionEntity response = new UnitInspectionEntity();
					response.setId((Long) ob[0]);
					log.info("Id::::::::" + response.getId());
					EmployeeMaster employeeMaster = (EmployeeMaster) ob[1];
					EntityMaster entityMaster = (EntityMaster) ob[2];
					response.setInspectedBy(employeeMaster);
					response.setEntityMaster(entityMaster);
					response.setInspectionDate((Date) ob[3]);
					response.setCreatedDate((Date) ob[4]);
					log.info(":: List Response ::" + response);
					unitInspectionEntityList.add(response);
				}
				baseDTO.setResponseContents(unitInspectionEntityList);
				baseDTO.setTotalRecords(totalResult);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (Exception exp) {
			log.error("Exception Cause : ", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("========END UnitInspectionService.getLazyLoadData========");
		return baseDTO;
	}

	public BaseDTO getUnitInspectionById(Long id) {
		log.info("========START UnitInspectionService.getUnitInspectionById========");
		log.info("========Id is========"+id);
		BaseDTO baseDto=new BaseDTO();
		UnitInspectionResponseDto unitInspectionResponseDto=new UnitInspectionResponseDto();
		try
		{
			if(id!=null)
			{
				UnitInspectionEntity unitInspectionEntity=unitInspectionRepository.findOne(id);
				UnitInspectionStockArrangement unitInspectionStockArrangement=unitInspectionStockArrangementRepository.findByUnitInspectionId(id);
				UnitInspectionOthers unitInspectionOthers=unitInspectionOthersRepository.findByUnitInspectionId(id);
				UnitInspectionSales unitInspectionSales=unitInspectionSalesRepository.findByUnitInspectionId(id);
				if(unitInspectionSales!=null && unitInspectionSales.getId()!=null)
				{
					List<UnitInspectionMovingProducts> unitInspectionMovingProducts=unitInspectionMovingProductsRepository.getByUnitInspectionAndSalesId(id,unitInspectionSales.getId());
					unitInspectionResponseDto.setUnitInspectionMovingProductsList(unitInspectionMovingProducts);
				}
				unitInspectionResponseDto.setUnitInspectionEntity(unitInspectionEntity);
				unitInspectionResponseDto.setUnitInspectionStockArrangement(unitInspectionStockArrangement);
				unitInspectionResponseDto.setUnitInspectionOthers(unitInspectionOthers);
				unitInspectionResponseDto.setUnitInspectionSales(unitInspectionSales);
				
				if (unitInspectionEntity.getEntityMaster() != null
						&& unitInspectionEntity.getEntityMaster().getEntityMasterRegion() != null
						&& unitInspectionEntity.getEntityMaster().getEntityMasterRegion().getId() != null) {
					EntityMaster entityMaster = entityMasterRepository.findOne(unitInspectionEntity.getEntityMaster().getEntityMasterRegion().getId());
					entityMaster.setBuildingType(null);
					entityMaster.setBankMaster(null);
					entityMaster.setDnpOffice(null);
					entityMaster.setEmployeeMaster(null);
					entityMaster.setEntityCategory(null);
					entityMaster.setEntityContactDetails(null);
					entityMaster.setEntityInchargeType(null);
					entityMaster.setEntityMasterParent(null);
					entityMaster.setWarehhouse(null);
					entityMaster.setWarehouseType(null);
					entityMaster.setMaturityDate(null);
					unitInspectionResponseDto.setEntityMaster(entityMaster);
				}
				log.info("========UnitInspectionEntity is========"+unitInspectionResponseDto.toString());
				baseDto.setResponseContent(unitInspectionResponseDto);
				baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}else
			{
				throw new Exception("Id Cannot Be Empty");
			}
			
		}catch (Exception e) {
			log.info("========Exception Occured in UnitInspectionService.getUnitInspectionById========",e);
			log.info("========Exception is========"+e.getMessage());
		}
		log.info("========End UnitInspectionService.getUnitINspectionById========");
		return baseDto;
	}
//---------------------------------
	
	@Autowired
	AppQueryRepository appQueryRepository;
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public BaseDTO getManagerAndAddrByShowroomId(Long showroomId) {
		log.info("EmployeeService :: getDnpmanagerOffice :: start");
		BaseDTO baseDTO = new BaseDTO();
		try {

			Validate.objectNotNull(showroomId, ErrorDescription.ERROR_ENTITY_TYPE_NOT_FOUND);
			ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("GET_SHOWROOM_MANAGER_NAME");
			if (applicationQuery == null || applicationQuery.getId() == null
					|| applicationQuery.getQueryContent() == null || applicationQuery.getQueryContent().isEmpty()) {
				throw new RestException("GET_SHOWROOM_MANAGER_NAME " + ErrorDescription.QUERY_NOT_FOUND);
			}
			String query = applicationQuery.getQueryContent().trim();
			query = query.replace(":entityId", showroomId.toString());
			baseDTO.setResponseContent(jdbcTemplate.query(query, new RowMapper<ShowroomManagerDTO>() {
				@Override
				public ShowroomManagerDTO mapRow(ResultSet rs, int no) throws SQLException {
					ShowroomManagerDTO employeeMaster = new ShowroomManagerDTO();
					employeeMaster.setEmployeeId(rs.getLong("id"));
					employeeMaster.setEmployeeCode(rs.getString("emp_code"));
					employeeMaster.setFirstName(rs.getString("first_name"));
					employeeMaster.setLastName(rs.getString("last_name"));
					return employeeMaster;
				}
			}));

		} catch (RestException re) {
			log.error(" Rest Exception Occured while getDnpmanagerOffice : ", re);

		} catch (Exception e) {
			log.error(" Exception Occured while getDnpmanagerOffice : ", e);

		}
		return baseDTO;
	}
	
	@Autowired
	FinancialYearRepository financialYearRepository;
	
	public BaseDTO getFastAndSlowMovingProducts(Long showroomId) {
		log.info("UniteInspectionBean :: getFastAndSlowMovingProducts :: start");
		BaseDTO baseDTO = new BaseDTO();
		String showRoomCode = "";
		try {
			Validate.objectNotNull(showroomId, ErrorDescription.ERROR_ENTITY_TYPE_NOT_FOUND);
			ApplicationQuery fastMovingQuantity = appQueryRepository.findByQueryName("INDEX_FAST_MOVING_QUANTITY");
			ApplicationQuery slowMovinfQuantity = appQueryRepository.findByQueryName("INDEX_SLOW_MOVING_QUANTITY");
			if (fastMovingQuantity == null || fastMovingQuantity.getId() == null
					|| fastMovingQuantity.getQueryContent() == null || fastMovingQuantity.getQueryContent().isEmpty()) {
				throw new RestException("INDEX_FAST_MOVING_QUANTITY" + ErrorDescription.QUERY_NOT_FOUND);
			}
			
			if (slowMovinfQuantity == null || slowMovinfQuantity.getId() == null
					|| slowMovinfQuantity.getQueryContent() == null || slowMovinfQuantity.getQueryContent().isEmpty()) {
				throw new RestException("INDEX_SLOW_MOVING_QUANTITY" + ErrorDescription.QUERY_NOT_FOUND);
			}
			List<UnitInspectionMovingProducts> unitInspectionMovingProductsList = new ArrayList<UnitInspectionMovingProducts>();
			
			FinancialYear finyear = financialYearRepository.findByCurrentFinancialYear();
			if(finyear ==null) {
			log.info("finyear is null");	
			}else {
				log.info("finyear start date:"+finyear.getStartYear() +" End Date : "+finyear.getEndDate());	
			}
			
			String[] fromToDateValues = AppUtil.getFromToDateForReport(finyear.getStartDate(),finyear.getEndDate());
			showRoomCode = "em.code=" + "'" + entityMasterRepository.getCodebyEntityID(showroomId) + "'" + "";
			if (fromToDateValues != null && fromToDateValues.length == 2) {
				log.info("<============ From Date ===========> " + fromToDateValues[0]);
				log.info("<============ To Date ===========> " + fromToDateValues[1]);
			} else {
				log.error("fromToDateValues are empty");
				return null;
			}
			
			String fastqueryValue = fastMovingQuantity.getQueryContent().trim();
			String slowqueryValue = slowMovinfQuantity.getQueryContent().trim();
			
			fastqueryValue = fastqueryValue.replace(":fromDate", "'" + fromToDateValues[0] + " 00:00:00'");
			fastqueryValue = fastqueryValue.replace(":toDate", "'" + fromToDateValues[1] + " 23:59:59'");
			fastqueryValue = fastqueryValue.replace(":timeStampTodate", "'" + fromToDateValues[1] + " 00:00:00'");
			fastqueryValue = fastqueryValue.replace(":regionName", showRoomCode);
			
			slowqueryValue = slowqueryValue.replace(":fromDate", "'" + fromToDateValues[0] + " 00:00:00'");
			slowqueryValue = slowqueryValue.replace(":toDate", "'" + fromToDateValues[1] + " 23:59:59'");
			slowqueryValue = slowqueryValue.replace(":timeStampTodate", "'" + fromToDateValues[1] + " 00:00:00'");
			slowqueryValue = slowqueryValue.replace(":regionName", showRoomCode);
			
			unitInspectionMovingProductsList.addAll(jdbcTemplate.query(fastqueryValue, new RowMapper<UnitInspectionMovingProducts>() {
				@Override
				public UnitInspectionMovingProducts mapRow(ResultSet rs, int no) throws SQLException {
					UnitInspectionMovingProducts fastMovingProducts = new UnitInspectionMovingProducts();
					fastMovingProducts.setMovingType("FAST");
					fastMovingProducts.setStockSold(rs.getDouble("sold"));
					String producodename=rs.getString("Product Code / Name");
					String productcode=producodename.split("/")[0].trim();
					fastMovingProducts.setProductVarietyMaster(productVarietyMasterRepository.findByCode(productcode));
					return fastMovingProducts;
				}
			}));
			
			
			unitInspectionMovingProductsList.addAll(jdbcTemplate.query(slowqueryValue, new RowMapper<UnitInspectionMovingProducts>() {
				@Override
				public UnitInspectionMovingProducts mapRow(ResultSet rs, int no) throws SQLException {
					UnitInspectionMovingProducts slowMovingProducts = new UnitInspectionMovingProducts();
					slowMovingProducts.setMovingType("SLOW");
					slowMovingProducts.setStockSold(rs.getDouble("sold"));
					String producodename=rs.getString("Product Code / Name");
					String productcode=producodename.split("/")[0].trim();
					slowMovingProducts.setProductVarietyMaster(productVarietyMasterRepository.findByCode(productcode));
					return slowMovingProducts;
				}
			}));
			
			baseDTO.setResponseContents(unitInspectionMovingProductsList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (RestException re) {
			log.error(" Rest Exception Occured while getDnpmanagerOffice : ", re);

		} catch (Exception e) {
			log.error(" Exception Occured while getDnpmanagerOffice : ", e);

		}
		return baseDTO;
	}
	
}
