package in.gov.cooptex.finance.service;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.dto.AdjustmentDetailsDTO;
import in.gov.cooptex.core.accounts.dto.InvoiceWiseAdjustmentDetailsDTO;
import in.gov.cooptex.core.accounts.dto.RegionCodeDropDownDTO;
import in.gov.cooptex.core.accounts.dto.SocietyCodeDropDownDTO;
import in.gov.cooptex.core.accounts.dto.SocietyInvoiceAdjustmentResponseDTO;
import in.gov.cooptex.core.accounts.dto.SocietyWiseAdjustmentDetailsDTO;
import in.gov.cooptex.core.accounts.enums.VoucherTypeDetails;
import in.gov.cooptex.core.accounts.model.GlAccount;
import in.gov.cooptex.core.accounts.model.Payment;
import in.gov.cooptex.core.accounts.model.PaymentDetails;
import in.gov.cooptex.core.accounts.model.PurchaseInvoice;
import in.gov.cooptex.core.accounts.model.SocietyInvoiceAdjustment;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.VoucherDetails;
import in.gov.cooptex.core.accounts.repository.GlAccountRepository;
import in.gov.cooptex.core.accounts.repository.PaymentMethodRepository;
import in.gov.cooptex.core.accounts.repository.PaymentRepository;
import in.gov.cooptex.core.accounts.repository.PaymentTypeMasterRepository;
import in.gov.cooptex.core.accounts.repository.VoucherDetailsRepository;
import in.gov.cooptex.core.accounts.repository.VoucherRepository;
import in.gov.cooptex.core.accounts.repository.VoucherTypeRepository;
import in.gov.cooptex.core.accounts.util.VoucherTypeCons;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.FlatBillDetailsDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.PaymentCategory;
import in.gov.cooptex.core.enums.SequenceName;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.CircleMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.PurchaseInvoiceAdjustment;
import in.gov.cooptex.core.model.SequenceConfig;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.CircleMasterRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.PaymentModeRepository;
import in.gov.cooptex.core.repository.PurchaseInvoiceAdjustmentRepository;
import in.gov.cooptex.core.repository.PurchaseInvoiceRepository;
import in.gov.cooptex.core.repository.SequenceConfigRepository;
import in.gov.cooptex.core.repository.SupplierMasterRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.ApplicationConstants;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.PurchaseInvoiceAdjustmentListDTO;
import in.gov.cooptex.finance.SocietyDropDownRequestDTO;
import in.gov.cooptex.finance.dto.SearchSocietyInvoiceAdjustmentDTO;
import in.gov.cooptex.finance.repository.PurchaseInvoiceItemsRepository;
import in.gov.cooptex.finance.repository.SocietyInvoiceAdjustmentRepository;
import in.gov.cooptex.operation.model.SupplierMaster;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class SocietyInvoiceAdjustmentService {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private ResponseWrapper responseWrapper;

	@Autowired
	private CircleMasterRepository circleMasterRepository;

	@Autowired
	private SupplierMasterRepository supplierMasterRepository;

	@Autowired
	private PurchaseInvoiceRepository purchaseInvoiceRepository;

	@Autowired
	private VoucherDetailsRepository voucherDetailsRepository; 

	@Autowired
	private PurchaseInvoiceAdjustmentRepository purchaseInvoiceAdjustmentRepository;

	@Autowired
	private SocietyInvoiceAdjustmentRepository societyInvoiceAdjustmentRepository;

	@Autowired
	private PurchaseInvoiceItemsRepository purchaseInvoiceItemsRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired 
	GlAccountRepository glAccountRepository;

	@Autowired
	VoucherRepository voucherRepository;

	@Autowired
	SequenceConfigRepository sequenceConfigRepository;

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	LoginService loginService;

	@Autowired
	VoucherTypeRepository voucherTypeRepository;

	@Autowired
	PaymentModeRepository paymentModeRepository;

	@Autowired
	PaymentMethodRepository paymentMethodRepository;

	@Autowired
	PaymentRepository paymentRepository;

	@Autowired
	PaymentTypeMasterRepository paymentTypeMasterRepository;

	@Autowired
	UserMasterRepository userMasterRepository;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	private static final String DECIMAL_FORMAT = "#0.00";

	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

	private static final String START_TIME_SUFFIX = " 00:00:00";

	private static final String END_TIME_SUFFIX = " 23:59:59";

	public BaseDTO getSocietyAdjustmentList() {
		log.info("<--Starts SocietyInvoiceAdjustmentService .getSocietyAdjustmentList-->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<SocietyInvoiceAdjustment> societyInvoiceAdjustmentList = societyInvoiceAdjustmentRepository.findAll();
			Map<Long, Double> groupedData = getSocietyInvoiceAdjustmentGroupedBySociety(societyInvoiceAdjustmentList,
					null);
			baseDTO.setResponseContents(getSocietyWiseInvoiceAdjustment(groupedData, null));
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (Exception exception) {
			log.error("exception Occured ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<--Ends SocietyInvoiceAdjustmentService .getSocietyAdjustmentList-->");
		return baseDTO;
	}

	/**
	 * 
	 * @param list
	 * @param filterSocietyIds
	 * @return
	 */
	private Map<Long, Double> getSocietyInvoiceAdjustmentGroupedBySociety(List<SocietyInvoiceAdjustment> list,
			List<Long> filterSocietyIds) {
		log.info("SocietyInvoiceAdjustmentService. getSocietyInvoiceAdjustmentGroupedBySociety() - START");

		Map<Long, Double> societyInvoiceAdjustmentDetails = list.stream().filter(it -> it.getSocietyId() != null)
				.collect(Collectors.groupingBy(SocietyInvoiceAdjustment::getSocietyId,
						Collectors.summingDouble(SocietyInvoiceAdjustment::getAdjustmentAmount)));

		int societyInvoiceAdjustmentDetailsSize = societyInvoiceAdjustmentDetails != null
				? societyInvoiceAdjustmentDetails.size()
				: 0;
		log.info(
				"SocietyInvoiceAdjustmentService. getSocietyInvoiceAdjustmentGroupedBySociety() - societyInvoiceAdjustmentDetailsSize: "
						+ societyInvoiceAdjustmentDetailsSize);

		log.info("SocietyInvoiceAdjustmentService. getSocietyInvoiceAdjustmentGroupedBySociety() - END");
		return societyInvoiceAdjustmentDetails;
	}

	private Map<Long, Double> getSocietyInvoiceAdjustmentGroupedBySocietyOld(List<SocietyInvoiceAdjustment> list,
			List<Long> filterSocietyIds) {
		log.info("<--Starts SocietyInvoiceAdjustmentService .getSocietyInvoiceAdjustmentGroupedBySociety-->");
		Map<Long, Double> societyInvoiceAdjustmentDetails = new HashMap<>();
		log.info("::::list Size::::" + list.size());
		for (SocietyInvoiceAdjustment societyInvoiceAdjustment : list) {
			// list.forEach(societyInvoiceAdjustment -> {
			Long societyId = societyInvoiceAdjustment.getSocietyId();
			if (societyInvoiceAdjustmentDetails.containsKey(societyId)) {
				societyInvoiceAdjustmentDetails.put(societyId, societyInvoiceAdjustmentDetails.get(societyId)
						+ societyInvoiceAdjustment.getAdjustmentAmount());
			} else {
				if (filterSocietyIds == null || filterSocietyIds.isEmpty()) {
					societyInvoiceAdjustmentDetails.put(societyId, societyInvoiceAdjustment.getAdjustmentAmount());
				} else {
					if (filterSocietyIds.contains(societyId)) {
						societyInvoiceAdjustmentDetails.put(societyId, societyInvoiceAdjustment.getAdjustmentAmount());
					}
				}
			}
			// });
		}
		log.info("<--Ends SocietyInvoiceAdjustmentService .getSocietyInvoiceAdjustmentGroupedBySociety-->"
				+ societyInvoiceAdjustmentDetails);
		return societyInvoiceAdjustmentDetails;
	}

	/**
	 * 
	 * @param societyInvoiceAdjustmentDetails
	 * @param searchDto
	 * @return
	 */
	@SuppressWarnings("unused")
	private List<SocietyWiseAdjustmentDetailsDTO> getSocietyWiseInvoiceAdjustment(
			Map<Long, Double> societyInvoiceAdjustmentDetails, SearchSocietyInvoiceAdjustmentDTO searchDto) {
		log.info("SocietyInvoiceAdjustmentService .getSocietyWiseInvoiceAdjustment() - START");

		List<SocietyWiseAdjustmentDetailsDTO> response = new ArrayList<>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		List<Map<String, Object>> payableAmountList = new ArrayList<Map<String, Object>>();

		Date invoiceFromDate = searchDto.getInvoiceFromDate();
		Date invoiceToDate = searchDto.getInvoiceToDate();

		String sql = "SELECT COALESCE(SUM(COALESCE(tot.itemamount,0)+COALESCE(tot.taxvalue,0)),0) AS pableamount FROM "
				+ "(SELECT  SUM(pii.item_amount/2) AS itemamount,SUM(piit.tax_value) AS taxvalue FROM purchase_invoice pi "
				+ "LEFT JOIN purchase_invoice_adjustment pia ON pia.invoice_id=pi.id "
				+ "LEFT JOIN voucher v ON v.id=pia.voucher_id "
				+ "JOIN purchase_invoice_items pii ON pii.purchase_invoice_id=pi.id "
				+ "JOIN purchase_invoice_item_tax piit ON piit.purchase_invoice_item_id=pii.id "
				+ "JOIN supplier_master sm ON sm.id=pi.supplier_id "
				+ "WHERE sm.id=:supplierId AND DATE(pi.created_date) BETWEEN :fromDate AND :toDate) AS tot";

		if (searchDto != null) {
			String fromDate = formatter.format(invoiceFromDate);
			sql = StringUtils.replace(sql, ":fromDate", AppUtil.getValueWithSingleQuote(fromDate));
			String toDate = formatter.format(invoiceToDate);
			sql = StringUtils.replace(sql, ":toDate", AppUtil.getValueWithSingleQuote(toDate));
		} else {
			sql = StringUtils.replace(sql, ":fromDate", "v.from_date");
			sql = StringUtils.replace(sql, ":toDate", "v.to_date");
		}

		for (Long societyId : societyInvoiceAdjustmentDetails.keySet()) {

			String societyCode = null;
			String societyName = null;
			String societyDisplayName = null;
			double totalInvoicePayableAmount = 0.0;
			double totalAdjustmentAmount = societyInvoiceAdjustmentDetails.get(societyId); // 2
			double amountAlreadyAdjusted = 0.0;
			double balanceAdjustmentAmount = 0.0;
			double percentageOfAmountToAdjust = 100.0;
			double remainingAdjustmentAmount = 0.0;
			Double voucherDetailsTotalAmount = 0D;

			log.info("SocietyInvoiceAdjustmentService .getSocietyWiseInvoiceAdjustment() - societyId: " + societyId);
			SupplierMaster supplierMaster = supplierMasterRepository.getSupplierMasterBySupplierID(societyId);

			if (supplierMaster != null) {
				societyCode = supplierMaster.getCode();
				societyName = supplierMaster.getName();
				societyDisplayName = societyCode + "/" + societyName;
			} else {
				log.error("Supplier master not found: " + societyId);
			}

			log.info(
					"SocietyInvoiceAdjustmentService .getSocietyWiseInvoiceAdjustment() - societyCode: " + societyCode);
			log.info(
					"SocietyInvoiceAdjustmentService .getSocietyWiseInvoiceAdjustment() - societyName: " + societyName);
			log.info("SocietyInvoiceAdjustmentService .getSocietyWiseInvoiceAdjustment() - societyDisplayName: "
					+ societyDisplayName);

			// Get Total Invoice Payable Amount
			String mainQuery = sql;
			mainQuery = mainQuery.replace(":supplierId", societyId.toString());

			payableAmountList = jdbcTemplate.queryForList(mainQuery);
			if (!CollectionUtils.isEmpty(payableAmountList)) {
				Map<String, Object> dataMap = payableAmountList.get(0);
				if (!dataMap.isEmpty()) {
					totalInvoicePayableAmount = dataMap.get("pableamount") != null
							? Double.valueOf(dataMap.get("pableamount").toString())
							: 0D;
				}
			}

			Double tempAmountAlreadyAdjusted = purchaseInvoiceAdjustmentRepository
					.getTotalAdjustedAmountBySupplierId(societyId);
			tempAmountAlreadyAdjusted = tempAmountAlreadyAdjusted != null ? tempAmountAlreadyAdjusted : 0D;
			log.info("SocietyInvoiceAdjustmentService .getSocietyWiseInvoiceAdjustment() - Already adjusted amount: "
					+ tempAmountAlreadyAdjusted + " for society id: " + societyId);

			voucherDetailsTotalAmount = voucherDetailsRepository.getTotalAmountBySupplierId(societyId);
			voucherDetailsTotalAmount = voucherDetailsTotalAmount != null ? voucherDetailsTotalAmount : 0D;
			log.info("SocietyInvoiceAdjustmentService .getSocietyWiseInvoiceAdjustment() - Voucher details amount: "
					+ voucherDetailsTotalAmount + " for society id: " + societyId);

			log.info("SocietyInvoiceAdjustmentService .getSocietyWiseInvoiceAdjustment() - totalInvoicePayableAmount: "
					+ totalInvoicePayableAmount);
			log.info("SocietyInvoiceAdjustmentService .getSocietyWiseInvoiceAdjustment() - totalAdjustmentAmount: "
					+ totalAdjustmentAmount);
			log.info("SocietyInvoiceAdjustmentService .getSocietyWiseInvoiceAdjustment() - amountAlreadyAdjusted: "
					+ amountAlreadyAdjusted);
			balanceAdjustmentAmount = (totalAdjustmentAmount - amountAlreadyAdjusted);
			log.info("SocietyInvoiceAdjustmentService .getSocietyWiseInvoiceAdjustment() - balanceAdjustmentAmount: "
					+ balanceAdjustmentAmount);
			if (totalInvoicePayableAmount > 0) {
				SocietyWiseAdjustmentDetailsDTO dto = new SocietyWiseAdjustmentDetailsDTO();
				dto.setSocietyId(societyId);
				dto.setSocietyName(societyName);
				dto.setSocietyCode(societyCode);
				dto.setSocietyDisplayName(societyDisplayName);
				dto.setTotalInvoicePayableAmount(totalInvoicePayableAmount);
				dto.setTotalAdjustmentAmount(totalAdjustmentAmount);
				dto.setAmountAlreadyAdjusted(amountAlreadyAdjusted);
				dto.setBalanceAdjustmentAmount(balanceAdjustmentAmount);
				dto.setAmountToBeAdjusted(balanceAdjustmentAmount);
				dto.setRemainingAdjustmentAmount(remainingAdjustmentAmount);
				dto.setPercentageOfAmountToAdjust(percentageOfAmountToAdjust);
				dto.setRemainingInvoiceAmount(dto.getTotalInvoicePayableAmount() - voucherDetailsTotalAmount
						- dto.getAmountAlreadyAdjusted());
				response.add(dto);
			}
		}

		log.info("SocietyInvoiceAdjustmentService .getSocietyWiseInvoiceAdjustment() - END");
		return response;
	}
	// @SuppressWarnings("unused")
	// private List<SocietyWiseAdjustmentDetailsDTO>
	// getSocietyWiseInvoiceAdjustment(
	// Map<Long, Double> societyInvoiceAdjustmentDetails,
	// SearchSocietyInvoiceAdjustmentDTO searchDto) {
	// log.info("SocietyInvoiceAdjustmentService .getSocietyWiseInvoiceAdjustment()
	// - START");
	//
	// List<SocietyWiseAdjustmentDetailsDTO> response = new ArrayList<>();
	// SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	// List<Map<String, Object>> payableAmountList = new ArrayList<Map<String,
	// Object>>();
	//
	// Date invoiceFromDate = searchDto.getInvoiceFromDate();
	// Date invoiceToDate = searchDto.getInvoiceToDate();
	//
	// String sql = "SELECT
	// COALESCE(SUM(COALESCE(tot.itemamount,0)+COALESCE(tot.taxvalue,0)),0) AS
	// pableamount FROM "
	// + "(SELECT SUM(pii.item_amount/2) AS itemamount,SUM(piit.tax_value) AS
	// taxvalue FROM purchase_invoice pi "
	// + "LEFT JOIN purchase_invoice_adjustment pia ON pia.invoice_id=pi.id "
	// + "LEFT JOIN voucher v ON v.id=pia.voucher_id "
	// + "JOIN purchase_invoice_items pii ON pii.purchase_invoice_id=pi.id "
	// + "JOIN purchase_invoice_item_tax piit ON
	// piit.purchase_invoice_item_id=pii.id "
	// + "JOIN supplier_master sm ON sm.id=pi.supplier_id "
	// + "WHERE pi.id=:purchaseInvoiceId and sm.id=:supplierId AND
	// DATE(pi.created_date) BETWEEN :fromDate AND :toDate) AS tot";
	//
	// if (searchDto != null) {
	// String fromDate = formatter.format(invoiceFromDate);
	// sql = StringUtils.replace(sql, ":fromDate",
	// AppUtil.getValueWithSingleQuote(fromDate));
	// String toDate = formatter.format(invoiceToDate);
	// sql = StringUtils.replace(sql, ":toDate",
	// AppUtil.getValueWithSingleQuote(toDate));
	// } else {
	// sql = StringUtils.replace(sql, ":fromDate", "v.from_date");
	// sql = StringUtils.replace(sql, ":toDate", "v.to_date");
	// }
	//
	// for (Long societyId : societyInvoiceAdjustmentDetails.keySet()) {
	//
	// String societyCode = null;
	// String societyName = null;
	// String societyDisplayName = null;
	// double totalInvoicePayableAmount = 0.0;
	// double totalAdjustmentAmount =
	// societyInvoiceAdjustmentDetails.get(societyId); // 2
	// double amountAlreadyAdjusted = 0.0;
	// double balanceAdjustmentAmount = 0.0;
	// double percentageOfAmountToAdjust = 100.0;
	// double remainingAdjustmentAmount = 0.0;
	// Double voucherDetailsTotalAmount = 0D;
	//
	// log.info("SocietyInvoiceAdjustmentService .getSocietyWiseInvoiceAdjustment()
	// - societyId: " + societyId);
	// SupplierMaster supplierMaster =
	// supplierMasterRepository.getSupplierMasterBySupplierID(societyId);
	//
	// if (supplierMaster != null) {
	// societyCode = supplierMaster.getCode();
	// societyName = supplierMaster.getName();
	// societyDisplayName = societyCode + "/" + societyName;
	// } else {
	// log.error("Supplier master not found: " + societyId);
	// }
	//
	// log.info(
	// "SocietyInvoiceAdjustmentService .getSocietyWiseInvoiceAdjustment() -
	// societyCode: " + societyCode);
	// log.info(
	// "SocietyInvoiceAdjustmentService .getSocietyWiseInvoiceAdjustment() -
	// societyName: " + societyName);
	// log.info("SocietyInvoiceAdjustmentService .getSocietyWiseInvoiceAdjustment()
	// - societyDisplayName: "
	// + societyDisplayName);
	//
	// List<PurchaseInvoice> purchaseInvoiceList = new ArrayList<PurchaseInvoice>();
	//
	// if (searchDto != null && invoiceFromDate != null && invoiceToDate != null) {
	// purchaseInvoiceList =
	// purchaseInvoiceRepository.getPurchaseInvoiceIdByDateAndSupplierId(invoiceFromDate,
	// invoiceToDate, societyId);
	// } else {
	// purchaseInvoiceList =
	// purchaseInvoiceRepository.getPurchaseInvoiceIdBySupplierId(societyId);
	// }
	//
	// if (!CollectionUtils.isEmpty(purchaseInvoiceList)) {
	//
	// for (PurchaseInvoice purchaseInvoice : purchaseInvoiceList) {
	//
	// Long purchaseInvoiceId = purchaseInvoice.getId();
	//
	// Double tempAmountAlreadyAdjusted = purchaseInvoiceAdjustmentRepository
	// .getTotalAdjustedAmountByInvoiceId(purchaseInvoiceId);
	// tempAmountAlreadyAdjusted = tempAmountAlreadyAdjusted != null ?
	// tempAmountAlreadyAdjusted : 0D;
	// log.info(
	// "SocietyInvoiceAdjustmentService .getSocietyWiseInvoiceAdjustment() - Already
	// adjusted amount: "
	// + tempAmountAlreadyAdjusted + " for invoice id: " + purchaseInvoiceId);
	//
	// voucherDetailsTotalAmount =
	// voucherDetailsRepository.getTotalAmountByInvoiceId(purchaseInvoiceId);
	// voucherDetailsTotalAmount = voucherDetailsTotalAmount != null ?
	// voucherDetailsTotalAmount : 0D;
	// log.info(
	// "SocietyInvoiceAdjustmentService .getSocietyWiseInvoiceAdjustment() - Voucher
	// details amount: "
	// + voucherDetailsTotalAmount + " for invoice id: " + purchaseInvoiceId);
	//
	// // Get Total Invoice Payable Amount
	// sql = sql.replace(":supplierId", societyId.toString());
	// sql = sql.replace(":purchaseInvoiceId", purchaseInvoiceId.toString());
	//
	// payableAmountList = jdbcTemplate.queryForList(sql);
	// if (!CollectionUtils.isEmpty(payableAmountList)) {
	// Map<String, Object> dataMap = payableAmountList.get(0);
	// if (!dataMap.isEmpty()) {
	// totalInvoicePayableAmount = dataMap.get("pableamount") != null
	// ? Double.valueOf(dataMap.get("pableamount").toString())
	// : 0D;
	// }
	// }
	//
	// amountAlreadyAdjusted += tempAmountAlreadyAdjusted;
	// }
	// } else {
	// log.error("Purchase invoice details not found for society id: " + societyId);
	// }
	//
	// log.info("SocietyInvoiceAdjustmentService .getSocietyWiseInvoiceAdjustment()
	// - totalInvoicePayableAmount: "
	// + totalInvoicePayableAmount);
	// log.info("SocietyInvoiceAdjustmentService .getSocietyWiseInvoiceAdjustment()
	// - totalAdjustmentAmount: "
	// + totalAdjustmentAmount);
	// log.info("SocietyInvoiceAdjustmentService .getSocietyWiseInvoiceAdjustment()
	// - amountAlreadyAdjusted: "
	// + amountAlreadyAdjusted);
	// balanceAdjustmentAmount = (totalAdjustmentAmount - amountAlreadyAdjusted);
	// log.info("SocietyInvoiceAdjustmentService .getSocietyWiseInvoiceAdjustment()
	// - balanceAdjustmentAmount: "
	// + balanceAdjustmentAmount);
	// if (totalInvoicePayableAmount > 0) {
	// SocietyWiseAdjustmentDetailsDTO dto = new SocietyWiseAdjustmentDetailsDTO();
	// dto.setSocietyId(societyId);
	// dto.setSocietyName(societyName);
	// dto.setSocietyCode(societyCode);
	// dto.setSocietyDisplayName(societyDisplayName);
	// dto.setTotalInvoicePayableAmount(totalInvoicePayableAmount);
	// dto.setTotalAdjustmentAmount(totalAdjustmentAmount);
	// dto.setAmountAlreadyAdjusted(amountAlreadyAdjusted);
	// dto.setBalanceAdjustmentAmount(balanceAdjustmentAmount);
	// dto.setAmountToBeAdjusted(balanceAdjustmentAmount);
	// dto.setRemainingAdjustmentAmount(remainingAdjustmentAmount);
	// dto.setPercentageOfAmountToAdjust(percentageOfAmountToAdjust);
	// dto.setRemainingInvoiceAmount(dto.getTotalInvoicePayableAmount() -
	// voucherDetailsTotalAmount
	// - dto.getAmountAlreadyAdjusted());
	// response.add(dto);
	// }
	// }
	//
	// log.info("SocietyInvoiceAdjustmentService .getSocietyWiseInvoiceAdjustment()
	// - END");
	// return response;
	// }

	private List<SocietyWiseAdjustmentDetailsDTO> getSocietyWiseInvoiceAdjustmentOld(
			Map<Long, Double> societyInvoiceAdjustmentDetails, SearchSocietyInvoiceAdjustmentDTO searchDto) {
		log.info("<--Starts SocietyInvoiceAdjustmentService .getSocietyWiseInvoiceAdjustment-->");

		List<SocietyWiseAdjustmentDetailsDTO> response = new ArrayList<>();
		log.info("getSocietyWiseInvoiceAdjustment :: societyInvoiceAdjustmentDetails==> "
				+ societyInvoiceAdjustmentDetails);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		List<Map<String, Object>> payableAmountList = new ArrayList<Map<String, Object>>();
		String sql = "select tot.itemamount+tot.taxvalue as pableamount from  "
				+ "(select  sum(pii.item_amount/2) as itemamount,sum(piit.tax_value) as taxvalue from purchase_invoice pi "
				+ "left join purchase_invoice_adjustment pia on pia.invoice_id=pi.id "
				+ "left join voucher v on v.id=pia.voucher_id "
				+ "join purchase_invoice_items pii on pii.purchase_invoice_id=pi.id "
				+ "join purchase_invoice_item_tax piit on piit.purchase_invoice_item_id=pii.id "
				+ "join supplier_master sm on sm.id=pi.supplier_id "
				+ " and date(pi.created_date) between :fromDate and :toDate) as tot";

		/*
		 * String sql=
		 * "select t.* from(select  sum(pii.item_amount)+sum(piit.tax_value) as pableamount "
		 * + "from purchase_invoice pi " +
		 * "join purchase_invoice_items pii on pii.purchase_invoice_id=pi.id " +
		 * "join (select purchase_invoice_item_id,sum(tax_value) as tax_value from purchase_invoice_item_tax group by purchase_invoice_item_id) piit on piit.purchase_invoice_item_id=pii.id "
		 * + "join supplier_master sm on sm.id=pi.supplier_id  " +
		 * "where pi.id=:purchaseInvoiceId " + "and sm.id=:supplierId " +
		 * "and (pi.invoice_status='FINALAPPROVED' or pi.invoice_status is null) " +
		 * "and date(pi.created_date) between :fromDate and :toDate)t where t.pableamount > 0 "
		 * ;
		 */

		if (searchDto != null) {
			String from = formatter.format(searchDto.getInvoiceFromDate());
			sql = sql.replace(":fromDate", "'" + from + "'");
			String to = formatter.format(searchDto.getInvoiceToDate());
			sql = sql.replace(":toDate", "'" + to + "'");
		} else {
			sql = sql.replace(":fromDate", "v.from_date");
			sql = sql.replace(":toDate", "v.to_date");
		}

		for (Long societyId : societyInvoiceAdjustmentDetails.keySet()) {
			String societyCode = null;
			String societyName = null;
			String societyDisplayName = null;
			double totalInvoicePayableAmount = 0.0;
			double totalAdjustmentAmount = societyInvoiceAdjustmentDetails.get(societyId); // 2
			double amountAlreadyAdjusted = 0.0;
			double balanceAdjustmentAmount = 0.0;
			double percentageOfAmountToAdjust = 100.0;
			double remainingAdjustmentAmount = 0.0;
			double voucherDetailsTotalAmount = 0.0;

			log.info("getSocietyWiseInvoiceAdjustment :: societyId==>" + societyId);

			SupplierMaster supplierMaster = supplierMasterRepository.findOne(societyId);
			if (supplierMaster != null) {
				societyCode = supplierMaster.getCode();
				societyName = supplierMaster.getName();
				societyDisplayName = societyCode + "/" + societyName;

				log.info("getSocietyWiseInvoiceAdjustment :: societyCode ==> " + societyCode);
				log.info("getSocietyWiseInvoiceAdjustment :: societyName ==> " + societyName);
				log.info("getSocietyWiseInvoiceAdjustment :: societyDisplayName ==> " + societyDisplayName);
			} else {
				log.error("Supplier master not found as on society id : " + societyId);
			}

			List<PurchaseInvoice> purchaseInvoiceList = new ArrayList<PurchaseInvoice>();
			if (searchDto != null && searchDto.getInvoiceFromDate() != null && searchDto.getInvoiceToDate() != null) {
				log.info("==If===>" + searchDto.getInvoiceFromDate(), searchDto.getInvoiceToDate(),
						supplierMaster.getId());
				purchaseInvoiceList = purchaseInvoiceRepository.getPurchaseInvoicesBetweenCreatedDatesandsupplierID(
						searchDto.getInvoiceFromDate(), searchDto.getInvoiceToDate(), supplierMaster.getId());
			} else {
				log.info("==Else===>" + supplierMaster);
				purchaseInvoiceList = purchaseInvoiceRepository.findAllBySupplierMasterOrderById(supplierMaster);
			}

			List<Long> purchaseInvoiceIdsbyProductIds = new ArrayList<>();
			if (searchDto != null) {
				if (searchDto.getProductVarietyIds() != null && !searchDto.getProductVarietyIds().isEmpty()) {
					for (Long productId : searchDto.getProductVarietyIds()) {
						log.info(":: productId ::" + productId);
						List<Long> purchaseInvoiceIdsbyProductIdtemp = purchaseInvoiceItemsRepository
								.findByProductVarietyId(productId);

						purchaseInvoiceIdsbyProductIds.addAll(purchaseInvoiceIdsbyProductIdtemp);
						log.info("Size of purchaseInvoiceIdsbyProductIds" + purchaseInvoiceIdsbyProductIds);
					}
					Set<Long> hashset = new LinkedHashSet<Long>(purchaseInvoiceIdsbyProductIds);
					purchaseInvoiceIdsbyProductIds.clear();
					purchaseInvoiceIdsbyProductIds.addAll(hashset);

					List<PurchaseInvoice> purchaseInvoiceListtemp = new ArrayList<>();
					for (PurchaseInvoice purchaseInvoice : purchaseInvoiceList) {
						if (purchaseInvoiceIdsbyProductIds.stream().anyMatch(pids -> pids == purchaseInvoice.getId())) {
							log.info(":::Matched:::" + purchaseInvoiceIdsbyProductIds.stream()
									.anyMatch(pids -> pids == purchaseInvoice.getId()));
							purchaseInvoiceListtemp.add(purchaseInvoice);
						} else {
							log.info(":::Not Matched:::" + purchaseInvoiceIdsbyProductIds.stream()
									.anyMatch(pids -> pids == purchaseInvoice.getId()));
						}
					}
					purchaseInvoiceList.clear();
					purchaseInvoiceList.addAll(purchaseInvoiceListtemp);
				}
			}

			if (purchaseInvoiceList != null && !purchaseInvoiceList.isEmpty()) {
				for (PurchaseInvoice purchaseInvoice : purchaseInvoiceList) {
					double tempAmountAlreadyAdjusted = 0.0;
					// double amountalreadyadjustted=0.0;

					List<PurchaseInvoiceAdjustment> purchaseInvoiceAdjustmentList = purchaseInvoiceAdjustmentRepository
							.getByInvoiceId(purchaseInvoice.getId());

					if (purchaseInvoiceAdjustmentList != null && !purchaseInvoiceAdjustmentList.isEmpty()) {
						for (PurchaseInvoiceAdjustment purchaseInvoiceAdjustment : purchaseInvoiceAdjustmentList) {
							if (purchaseInvoiceAdjustment != null) {
								// amountalreadyadjustted=purchaseInvoiceAdjustment.getTotalAdjustedAmount();

								/*
								 * if (fromDate != null && toDate != null) { Date createdDate =
								 * purchaseInvoiceAdjustment.getCreatedDate(); if (!(createdDate.after(fromDate)
								 * && createdDate.before(toDate))) { continue; } }
								 */
								tempAmountAlreadyAdjusted += purchaseInvoiceAdjustment.getTotalAdjustedAmount();
								log.info("getSocietyWiseInvoiceAdjustment :: tempAmountAlreadyAdjusted ==> "
										+ tempAmountAlreadyAdjusted);
							}
						}
					} else {
						log.error("Purchase invoice adjustment not found as on purchase invoice id : "
								+ purchaseInvoice.getId());
					}

					List<VoucherDetails> voucherDetailsList = voucherDetailsRepository
							.findAllByPurchaseInvoice(purchaseInvoice);

					if (voucherDetailsList != null) {
						for (VoucherDetails voucherDetails : voucherDetailsList) {
							voucherDetailsTotalAmount += voucherDetails.getAmount();
							log.info("getSocietyWiseInvoiceAdjustment :: voucherDetailsTotalAmount ==> "
									+ voucherDetailsTotalAmount);
						}
					} else {
						log.error("Voucher details not found as on purchase invoice id : " + purchaseInvoice.getId());
					}
					/*
					 * if (purchaseInvoice != null && purchaseInvoice.getGrandTotal() != null) {
					 * totalInvoicePayableAmount += purchaseInvoice.getGrandTotal(); } else {
					 * log.error("Purchase invoice grand total not found"); }
					 */

					// get total invoice payable amount
					String mainQuery = sql;
					mainQuery = mainQuery.replace(":supplierId", societyId.toString());
					mainQuery = mainQuery.replace(":purchaseInvoiceId", purchaseInvoice.getId().toString());

					log.info("after value changes query-----------" + mainQuery);

					payableAmountList = jdbcTemplate.queryForList(mainQuery);
					for (Map<String, Object> data : payableAmountList) {
						if (data.get("pableamount") != null) {
							totalInvoicePayableAmount += Double.valueOf(data.get("pableamount").toString());
						}

					}

					amountAlreadyAdjusted += tempAmountAlreadyAdjusted;
					log.info("getSocietyWiseInvoiceAdjustment :: amountAlreadyAdjusted ==> " + amountAlreadyAdjusted);
				}
			} else {
				log.error("Purchase invoice not found");
			}
			log.info("totalInvoicePayableAmount=====>" + totalInvoicePayableAmount);
			log.info("totalAdjustmentAmount==>" + totalAdjustmentAmount);
			log.info("amountAlreadyAdjusted==>" + amountAlreadyAdjusted);
			balanceAdjustmentAmount = (totalAdjustmentAmount - amountAlreadyAdjusted);
			log.info("balanceAdjustmentAmount==>" + balanceAdjustmentAmount);
			if (totalInvoicePayableAmount > 0) {
				SocietyWiseAdjustmentDetailsDTO dto = new SocietyWiseAdjustmentDetailsDTO();
				log.info("getSocietyWiseInvoiceAdjustment :: societyId==> " + societyId);
				log.info("getSocietyWiseInvoiceAdjustment :: societyName==> " + societyName);
				log.info("getSocietyWiseInvoiceAdjustment :: societyCode==> " + societyCode);
				log.info("getSocietyWiseInvoiceAdjustment :: societyDisplayName==> " + societyDisplayName);
				log.info(
						"getSocietyWiseInvoiceAdjustment :: totalInvoicePayableAmount==> " + totalInvoicePayableAmount);
				log.info("getSocietyWiseInvoiceAdjustment :: totalAdjustmentAmount==> " + totalAdjustmentAmount);
				log.info("getSocietyWiseInvoiceAdjustment :: amountAlreadyAdjusted==> " + amountAlreadyAdjusted);
				log.info("getSocietyWiseInvoiceAdjustment :: balanceAdjustmentAmount==> " + balanceAdjustmentAmount);
				log.info(
						"getSocietyWiseInvoiceAdjustment :: remainingAdjustmentAmount==> " + remainingAdjustmentAmount);
				log.info("getSocietyWiseInvoiceAdjustment :: percentageOfAmountToAdjust==> "
						+ percentageOfAmountToAdjust);
				dto.setSocietyId(societyId);
				dto.setSocietyName(societyName);
				dto.setSocietyCode(societyCode);
				dto.setSocietyDisplayName(societyDisplayName);
				dto.setTotalInvoicePayableAmount(totalInvoicePayableAmount);
				dto.setTotalAdjustmentAmount(totalAdjustmentAmount);
				dto.setAmountAlreadyAdjusted(amountAlreadyAdjusted);
				dto.setBalanceAdjustmentAmount(balanceAdjustmentAmount);
				dto.setAmountToBeAdjusted(balanceAdjustmentAmount);
				dto.setRemainingAdjustmentAmount(remainingAdjustmentAmount);
				dto.setPercentageOfAmountToAdjust(percentageOfAmountToAdjust);
				dto.setRemainingInvoiceAmount(dto.getTotalInvoicePayableAmount() - voucherDetailsTotalAmount
						- dto.getAmountAlreadyAdjusted());

				log.info("getSocietyWiseInvoiceAdjustment :: setRemainingInvoiceAmount==> "
						+ dto.getRemainingInvoiceAmount());

				response.add(dto);
			}

		}

		log.info("<<<< ------- Ends UserService .getSocietyWiseInvoiceAdjustment ---------- >>>>>>>");
		return response;
	}

	/**
	 * 
	 * @param searchDto
	 * @return
	 */
	public BaseDTO searchSocietyInvoiceAdjustmentNew(SearchSocietyInvoiceAdjustmentDTO searchDto) {
		log.info("SocietyInvoiceAdjustmentService. searchSocietyInvoiceAdjustment() - START");
		BaseDTO baseDTO = new BaseDTO();
		List<SocietyInvoiceAdjustment> societyInvoiceAdjustmentList = null;
		try {

			List<Long> societyIds = searchDto.getSocietyIds();
			Date invoiceFromDate = searchDto.getInvoiceFromDate();
			Date invoiceToDate = searchDto.getInvoiceToDate();

			log.info("SocietyInvoiceAdjustmentService. searchSocietyInvoiceAdjustment() - societyIds: " + societyIds);
			log.info("SocietyInvoiceAdjustmentService. searchSocietyInvoiceAdjustment() - invoiceFromDate: "
					+ invoiceFromDate);
			log.info("SocietyInvoiceAdjustmentService. searchSocietyInvoiceAdjustment() - invoiceToDate: "
					+ invoiceToDate);

			if (CollectionUtils.isEmpty(societyIds)) {
				societyInvoiceAdjustmentList = societyInvoiceAdjustmentRepository
						.findSocietyAdjustmentListbyTodate(invoiceToDate);
			} else {
				societyInvoiceAdjustmentList = societyInvoiceAdjustmentRepository
						.findSocietyAdjustmentListbyTodateAndSocietyIds(invoiceToDate, societyIds);
			}

			if (CollectionUtils.isEmpty(societyInvoiceAdjustmentList)) {
				throw new RestException("Society invoice adjustment list is empty.");
			}

			int societyInvoiceAdjustmentListSize = societyInvoiceAdjustmentList != null
					? societyInvoiceAdjustmentList.size()
					: 0;
			log.info(
					"SocietyInvoiceAdjustmentService. searchSocietyInvoiceAdjustment() - societyInvoiceAdjustmentListSize: "
							+ societyInvoiceAdjustmentListSize);

			Map<Long, Double> groupedBySocietyData = getSocietyInvoiceAdjustmentGroupedBySociety(
					societyInvoiceAdjustmentList, societyIds);

			int groupedBySocietyDataSize = groupedBySocietyData != null ? groupedBySocietyData.size() : 0;
			log.info("SocietyInvoiceAdjustmentService. searchSocietyInvoiceAdjustment() - groupedBySocietyDataSize: "
					+ groupedBySocietyDataSize);

			List<SocietyWiseAdjustmentDetailsDTO> societyWiseAdjustmentDetailsDTOS = getSocietyWiseInvoiceAdjustment(
					groupedBySocietyData, searchDto);

			// for (SocietyWiseAdjustmentDetailsDTO dto : societyWiseAdjustmentDetailsDTOS)
			// {
			// searchDto.setSocietyId(dto.getSocietyId());
			// dto.setSocietyInvoiceAdjustmentResponseDTO(getSocietyAdjustmentListBySocietyCode(searchDto));
			// }

			log.info("searchSocietyInvoiceAdjustment :: societyWiseAdjustmentDetailsDTOS size==> "
					+ societyWiseAdjustmentDetailsDTOS.size());

			baseDTO.setResponseContents(societyWiseAdjustmentDetailsDTOS);

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

			/*
			 * } else {
			 * baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			 * baseDTO.setErrorDescription("filters not present"); }
			 */
		} catch (RestException re) {
			log.error("RestException Occured at searchSocietyInvoiceAdjustment() ", re);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			baseDTO.setErrorDescription(re.getMessage());
		} catch (Exception exception) {
			log.error("Exception Occured at searchSocietyInvoiceAdjustment() ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	/**
	 * 
	 * @param searchDto
	 * @return
	 */
	public BaseDTO searchSocietyInvoiceAdjustment(SearchSocietyInvoiceAdjustmentDTO searchDto) {
		log.info("SocietyInvoiceAdjustmentService. searchSocietyInvoiceAdjustment() - START");
		BaseDTO baseDTO = new BaseDTO();
		try {

			List<Long> societyIds = searchDto.getSocietyIds();
			Date invoiceFromDate = searchDto.getInvoiceFromDate();
			Date invoiceToDate = searchDto.getInvoiceToDate();

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

			String invoiceFromDateStr = formatter.format(invoiceFromDate);
			String invoiceToDateStr = formatter.format(invoiceToDate);

			log.info("SocietyInvoiceAdjustmentService. searchSocietyInvoiceAdjustment() - societyIds: " + societyIds);
			log.info("SocietyInvoiceAdjustmentService. searchSocietyInvoiceAdjustment() - invoiceFromDate: "
					+ invoiceFromDate);
			log.info("SocietyInvoiceAdjustmentService. searchSocietyInvoiceAdjustment() - invoiceToDate: "
					+ invoiceToDate);

			ApplicationQuery applicationQuery = applicationQueryRepository
					.findByQueryName(ApplicationConstants.SOCIETY_ADJUSTMENT_SOCIETY_WISE_DETAILS_SQL);
			if (applicationQuery == null) {
				baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getErrorCode());
				return baseDTO;
			}

			String queryContent = applicationQuery.getQueryContent().trim();
			queryContent = StringUtils.replace(queryContent, "${fromDate}",
					AppUtil.getValueWithSingleQuote(invoiceFromDateStr + START_TIME_SUFFIX));
			queryContent = StringUtils.replace(queryContent, "${toDate}",
					AppUtil.getValueWithSingleQuote(invoiceToDateStr + END_TIME_SUFFIX));

			if (CollectionUtils.isEmpty(societyIds)) {
				queryContent = StringUtils.replace(queryContent, "${societyParam}", "");
			} else {
				queryContent = StringUtils.replace(queryContent, "${societyParam}", " AND sm.id IN (${societyIds}) ");
				queryContent = StringUtils.replace(queryContent, "${societyIds}",
						AppUtil.commaSeparatedIds(societyIds));
			}

			List<Map<String, Object>> dataMapList = jdbcTemplate.queryForList(queryContent);
			int dataMapListSize = dataMapList != null ? dataMapList.size() : 0;
			log.info("SocietyInvoiceAdjustmentService. searchSocietyInvoiceAdjustment() - dataMapListSize: "
					+ dataMapListSize);

			List<SocietyWiseAdjustmentDetailsDTO> societyAdjustmentList = mapSocietyAdjustmentDetailsDTOList(
					dataMapList);
			int societyAdjustmentListSize = societyAdjustmentList != null ? societyAdjustmentList.size() : 0;
			log.info("SocietyInvoiceAdjustmentService. searchSocietyInvoiceAdjustment() - societyAdjustmentListSize: "
					+ societyAdjustmentListSize);

			baseDTO.setResponseContents(societyAdjustmentList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException re) {
			log.error("RestException Occured at searchSocietyInvoiceAdjustment() ", re);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			baseDTO.setErrorDescription(re.getMessage());
		} catch (Exception exception) {
			log.error("Exception Occured at searchSocietyInvoiceAdjustment() ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	/**
	 * 
	 * @param mapList
	 * @return
	 * @throws Exception
	 */
	private List<SocietyWiseAdjustmentDetailsDTO> mapSocietyAdjustmentDetailsDTOList(List<Map<String, Object>> mapList)
			throws RestException {

		List<SocietyWiseAdjustmentDetailsDTO> societyAdjustmentDetailsDTOList = null;
		double percentageOfAmountToAdjust = 0D;

		if (!CollectionUtils.isEmpty(mapList)) {
			societyAdjustmentDetailsDTOList = new ArrayList<SocietyWiseAdjustmentDetailsDTO>();
			for (Map<String, Object> map : mapList) {

				Double invoicePayableAmount = getDouble(map, "total_invoice_payable_amount");
				invoicePayableAmount = invoicePayableAmount != null ? invoicePayableAmount : 0D;
				if (invoicePayableAmount > 0D) {
					SocietyWiseAdjustmentDetailsDTO dto = new SocietyWiseAdjustmentDetailsDTO();
					dto.setSocietyId(getLong(map, "society_id"));
					dto.setSocietyName(getString(map, "society_name"));
					dto.setSocietyCode(getString(map, "society_code"));
					dto.setSocietyDisplayName(dto.getSocietyCode() + "/" + dto.getSocietyName());
					dto.setTotalInvoicePayableAmount(invoicePayableAmount);
					dto.setTotalAdjustmentAmount(getDouble(map, "total_adjustment_amount"));
					dto.setAmountAlreadyAdjusted(getDouble(map, "amount_already_adjusted"));
					dto.setBalanceAdjustmentAmount(getDouble(map, "balance_adjustment_amount"));
					dto.setAmountToBeAdjusted(getDouble(map, "amount_to_be_adjusted"));
					dto.setRemainingAdjustmentAmount(getDouble(map, "remaining_adjustment_amount"));
					dto.setPercentageOfAmountToAdjust(percentageOfAmountToAdjust);
					dto.setRemainingInvoiceAmount(getDouble(map, "remaining_invoice_amount"));
					if(dto.getRemainingInvoiceAmount() != null && dto.getRemainingInvoiceAmount().doubleValue() > 0D) {
						societyAdjustmentDetailsDTOList.add(dto);
					}
				}
			}

		}

		return societyAdjustmentDetailsDTOList;
	}

	private String getString(Map<String, Object> map, String key) {
		try {
			Object obj = map.get(key);
			if (obj != null) {
				return String.valueOf(obj);
			}
		} catch (Exception ex) {
			log.error("Exception at getString()", ex);
		}
		return null;
	}

	private Long getLong(Map<String, Object> map, String key) {
		try {
			Object obj = map.get(key);
			if (obj != null) {
				return Long.valueOf(String.valueOf(obj));
			}
		} catch (Exception ex) {
			log.error("Exception at getLong()", ex);
		}
		return null;
	}

	private Double getDouble(Map<String, Object> map, String key) {
		try {
			Object obj = map.get(key);
			if (obj != null) {
				String valueStr = String.valueOf(obj);
				if (!StringUtils.isEmpty(valueStr)) {
					DecimalFormat decimalFormat = new DecimalFormat(DECIMAL_FORMAT);
					Double value = decimalFormat.parse(decimalFormat.format(Double.valueOf(valueStr))).doubleValue();
					return value;
				}
			}
		} catch (Exception ex) {
			log.error("Exception at getDouble()", ex);
		}
		return null;
	}

	public BaseDTO searchSocietyInvoiceAdjustmentOld(SearchSocietyInvoiceAdjustmentDTO searchDto) {
		log.info("<--Starts SocietyInvoiceAdjustmentService .searchSocietyInvoiceAdjustment-->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			// if ((searchDto.getSocietyIds() != null)) {
			log.info("SocietyInvoiceAdjustmentService filters :::" + searchDto.toString());

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

			// String todate =(dateFormat.format(searchDto.getInvoiceToDate()));
			// searchDto.setInvoiceToDate();
			/*
			 * Date invoiceFromDate = searchDto.getInvoiceFromDate(); Date invoiceToDate =
			 * searchDto.getInvoiceToDate();
			 */
			List<Long> societyIds = searchDto.getSocietyIds();

			// List<SocietyInvoiceAdjustment> societyInvoiceAdjustmentList =
			// societyInvoiceAdjustmentRepository.findAll();
			List<SocietyInvoiceAdjustment> societyInvoiceAdjustmentList = societyInvoiceAdjustmentRepository
					.findSocietyAdjustmentListbyTodate(searchDto.getInvoiceToDate());

			log.info("societyInvoiceAdjustmentList==>" + societyInvoiceAdjustmentList.size());
			Map<Long, Double> groupedData = getSocietyInvoiceAdjustmentGroupedBySociety(societyInvoiceAdjustmentList,
					societyIds);

			log.info("searchSocietyInvoiceAdjustment :: groupedData==> " + groupedData);
			/*
			 * log.info("searchSocietyInvoiceAdjustment :: invoiceFromDate==> "
			 * +invoiceFromDate);
			 * log.info("searchSocietyInvoiceAdjustment :: invoiceToDate==> "+invoiceToDate)
			 * ;
			 */

			List<SocietyWiseAdjustmentDetailsDTO> societyWiseAdjustmentDetailsDTOS = getSocietyWiseInvoiceAdjustment(
					groupedData, searchDto);

			for (SocietyWiseAdjustmentDetailsDTO dto : societyWiseAdjustmentDetailsDTOS) {
				searchDto.setSocietyId(dto.getSocietyId());
				dto.setSocietyInvoiceAdjustmentResponseDTO(getSocietyAdjustmentListBySocietyCode(searchDto));
			}

			log.info("searchSocietyInvoiceAdjustment :: societyWiseAdjustmentDetailsDTOS size==> "
					+ societyWiseAdjustmentDetailsDTOS.size());

			baseDTO.setResponseContents(societyWiseAdjustmentDetailsDTOS);

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

			/*
			 * } else {
			 * baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			 * baseDTO.setErrorDescription("filters not present"); }
			 */
		} catch (Exception exception) {
			log.error("exception Occured ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public SocietyInvoiceAdjustmentResponseDTO getSocietyAdjustmentListBySocietyCodeOld(
			SearchSocietyInvoiceAdjustmentDTO searchDto) {
		log.info("<--Starts SocietyInvoiceAdjustmentService .getAll-->");
		BaseDTO baseDTO = new BaseDTO();
		SocietyInvoiceAdjustmentResponseDTO responseDTO = new SocietyInvoiceAdjustmentResponseDTO();
		List<Long> supplierIdList = new ArrayList<Long>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String from = formatter.format(searchDto.getInvoiceFromDate());
		String to = formatter.format(searchDto.getInvoiceToDate());
		List<Map<String, Object>> payableAmountList = new ArrayList<Map<String, Object>>();
		String sql = "select tot.itemamount+tot.taxvalue as pableamount from  "
				+ "(select  sum(pii.item_amount/2) as itemamount,sum(piit.tax_value) as taxvalue from purchase_invoice pi "
				+ "join purchase_invoice_items pii on pii.purchase_invoice_id=pi.id "
				+ "join purchase_invoice_item_tax piit on piit.purchase_invoice_item_id=pii.id "
				+ "join supplier_master sm on sm.id=pi.supplier_id "
				+ "where pi.id=:purchaseInvoiceId and sm.id=:supplierId  "
				+ "and date(pi.created_date) between :fromDate and :toDate) as tot";

		try {
			if ((searchDto.getSocietyId() != null)) {
				log.info("SocietyInvoiceAdjustmentService filters :::" + searchDto.toString());

				// DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

				Date invoiceFromDate = searchDto.getInvoiceFromDate();
				Date invoiceToDate = searchDto.getInvoiceToDate();
				Long societyId = searchDto.getSocietyId();
				sql = sql.replace(":supplierId", societyId.toString());
				sql = sql.replace(":fromDate", "'" + from + "'");
				sql = sql.replace(":toDate", "'" + to + "'");

				log.info("getSocietyAdjustmentListBySocietyCode :: invoiceFromDate ==> " + invoiceFromDate);
				log.info("getSocietyAdjustmentListBySocietyCode :: invoiceToDate ==> " + invoiceToDate);
				log.info("getSocietyAdjustmentListBySocietyCode :: societyId ==> " + societyId);

				List<SocietyInvoiceAdjustment> societyInvoiceAdjustmentList = societyInvoiceAdjustmentRepository
						.findBySocietyId(societyId);
				if (societyInvoiceAdjustmentList == null) {
					throw new RestException("SocietyInvoiceAdjustment not Found for the societyId: " + societyId);
				}

				// creating map with societyId and AdjustmentAmount
				HashMap<Long, Double> societyInvoiceAdjustmentDetails = new HashMap<>();
				societyInvoiceAdjustmentDetails.put(societyId, societyInvoiceAdjustmentList.stream()
						.mapToDouble(societyInvoiceAdj -> societyInvoiceAdj.getAdjustmentAmount()).sum());

				SocietyWiseAdjustmentDetailsDTO societyWiseAdjustmentDetailsDTO = null;
				List<SocietyWiseAdjustmentDetailsDTO> societyWiseAdjustmentDetailsDTOS = this
						.getSocietyWiseInvoiceAdjustment(societyInvoiceAdjustmentDetails, searchDto);
				log.info("societyWiseAdjustmentDetailsDTOS==>" + societyWiseAdjustmentDetailsDTOS.size());

				if (societyWiseAdjustmentDetailsDTOS != null && !societyWiseAdjustmentDetailsDTOS.isEmpty()) {
					societyWiseAdjustmentDetailsDTO = societyWiseAdjustmentDetailsDTOS.get(0);
				}

				// creating AdjustmentDetailsDTO
				List<AdjustmentDetailsDTO> adjustmentDetailsDTOLocalList = new ArrayList<>();
				for (SocietyInvoiceAdjustment societyInvoiceAdjustment : societyInvoiceAdjustmentList) {
					AdjustmentDetailsDTO adjustmentDetailsDTO = new AdjustmentDetailsDTO();
					adjustmentDetailsDTO.setId(societyInvoiceAdjustment.getId());
					adjustmentDetailsDTO.setDate(societyInvoiceAdjustment.getCreatedDate());
					adjustmentDetailsDTO.setPercentageOfAmountToAdjust(100.0);
					if (societyInvoiceAdjustment.getSocietyAdjustment() != null
							&& societyInvoiceAdjustment.getSocietyAdjustment().getAdjustmentName() != null) {
						adjustmentDetailsDTO
								.setAdjustmentName(societyInvoiceAdjustment.getSocietyAdjustment().getAdjustmentName());
					}
					if (societyInvoiceAdjustment.getSocietyAdjustment() != null
							&& societyInvoiceAdjustment.getSocietyAdjustment().getAdjustmentCode() != null) {
						adjustmentDetailsDTO
								.setAdjustmentCode(societyInvoiceAdjustment.getSocietyAdjustment().getAdjustmentCode());
					}
					adjustmentDetailsDTO.setCreatedDate(societyInvoiceAdjustment.getCreatedDate());
					adjustmentDetailsDTO.setTotalAdjustmentAmount(societyInvoiceAdjustment.getAdjustmentAmount());
					// adjustmentDetailsDTO.setGlaccount(societyInvoiceAdjustment.getSocietyAdjustment().get);
					/*
					 * if (societyWiseAdjustmentDetailsDTO != null &&
					 * societyWiseAdjustmentDetailsDTO.getBalanceAdjustmentAmount() != null) {
					 */

					List<PurchaseInvoiceAdjustment> purchaseInvoiceAdjustmentList = purchaseInvoiceAdjustmentRepository
							.getPurchaseInvoiceAdjByadjustmentId(societyInvoiceAdjustment.getId());

					if (purchaseInvoiceAdjustmentList != null && purchaseInvoiceAdjustmentList.size() > 0) {
						adjustmentDetailsDTO.setAmountAlreadyAdjusted(purchaseInvoiceAdjustmentList.stream()
								.mapToDouble(pia -> pia.getTotalAdjustedAmount()).sum());
					} else {
						adjustmentDetailsDTO.setAmountAlreadyAdjusted(0D);
					}
					adjustmentDetailsDTO.setBalanceAdjustmentAmount(societyInvoiceAdjustment.getAdjustmentAmount()
							- adjustmentDetailsDTO.getAmountAlreadyAdjusted());
					/* } */
					adjustmentDetailsDTO.setAmountToBeAdjusted(adjustmentDetailsDTO.getBalanceAdjustmentAmount());
					/*
					 * if (adjustmentDetailsDTO != null &&
					 * adjustmentDetailsDTO.getTotalAdjustmentAmount() != null &&
					 * adjustmentDetailsDTO.getBalanceAdjustmentAmount() != null) {
					 */

					/* } */
					adjustmentDetailsDTOLocalList.add(adjustmentDetailsDTO);
				}
				responseDTO.setSocietyWiseAdjustmentDetailsDTO(societyWiseAdjustmentDetailsDTO);
				responseDTO.setAdjustmentDetailsDTOList(adjustmentDetailsDTOLocalList);

				// getting invoice wise adjustment details
				log.info("getSocietyAdjustmentListBySocietyCode :: societyId==> " + societyId);
				SupplierMaster supplierMaster = supplierMasterRepository.getSupplierMasterBySupplierID(societyId);

				/*
				 * List<PurchaseInvoice> purchaseInvoiceList = new ArrayList<>(); if (searchDto
				 * != null && searchDto.getInvoiceFromDate() != null &&
				 * searchDto.getInvoiceToDate() != null) { // purchaseInvoiceList = //
				 * purchaseInvoiceRepository.
				 * getPurchaseInvoicesBetweenCreatedDatesandsupplierID( //
				 * searchDto.getInvoiceFromDate(), searchDto.getInvoiceToDate(), //
				 * supplierMaster.getId());
				 * 
				 * purchaseInvoiceList = purchaseInvoiceRepository
				 * .getPurchaseInvoicesBetweenCreatedDatesandsupplierIDNew(searchDto.
				 * getInvoiceFromDate(), searchDto.getInvoiceToDate(), supplierMaster.getId());
				 * } else { purchaseInvoiceList =
				 * purchaseInvoiceRepository.findAllBySupplierMasterOrderById(supplierMaster); }
				 */

				List<InvoiceWiseAdjustmentDetailsDTO> invoiceWiseAdjustmentDetailsDTOS = new ArrayList<>();

				int checkSno = 1;
				// for (PurchaseInvoice purchaseInvoice : purchaseInvoiceList) {
				// log.info(checkSno + " <=getSocietyAdjustmentListBySocietyCode=> " +
				// purchaseInvoice.getId());
				// checkSno++;
				// InvoiceWiseAdjustmentDetailsDTO detailsDTO = new
				// InvoiceWiseAdjustmentDetailsDTO();
				//
				// double amountAlreadyAdjusted = 0.0;
				//
				// List<PurchaseInvoiceAdjustment> purchaseInvoiceAdjustmentList =
				// purchaseInvoiceAdjustmentRepository
				// .getByInvoiceId(purchaseInvoice.getId());
				// for (PurchaseInvoiceAdjustment purchaseInvoiceAdjustment :
				// purchaseInvoiceAdjustmentList) {
				// if (purchaseInvoiceAdjustment != null) {
				//
				// /*
				// * if (invoiceFromDate != null && invoiceToDate != null) { Date createdDate =
				// * purchaseInvoiceAdjustment.getCreatedDate(); if
				// * (!(createdDate.after(invoiceFromDate) &&
				// createdDate.before(invoiceToDate)))
				// * { continue; } }
				// */
				// amountAlreadyAdjusted += purchaseInvoiceAdjustment.getTotalAdjustedAmount();
				// }
				// }
				//
				// List<VoucherDetails> voucherDetailsList = voucherDetailsRepository
				// .findAllByPurchaseInvoice(purchaseInvoice);
				//
				// // voucherDetailsList.forEach(voucherDetails -> {
				// for (VoucherDetails voucherDetails : voucherDetailsList) {
				// if (voucherDetails != null && voucherDetails.getAmount() != null &&
				// detailsDTO != null
				// && detailsDTO.getAlreadyAmountPaid() != null) {
				// detailsDTO.setAlreadyAmountPaid(
				// voucherDetails.getAmount() + detailsDTO.getAlreadyAmountPaid());
				// } else {
				// log.error("Voucher amount or aleady amount not found ");
				// }
				// // });
				// }
				// log.info("getSocietyAdjustmentListBySocietyCode :: GrandTotal==> "
				// + purchaseInvoice.getGrandTotal());
				//
				// // detailsDTO.setTotalInvoicePayableAmount(purchaseInvoice.getGrandTotal());
				// double totalInvoicePayableAmount = 0.0;
				// String mainQuery = sql;
				// mainQuery = mainQuery.replace(":purchaseInvoiceId",
				// purchaseInvoice.getId().toString());
				//
				// payableAmountList = jdbcTemplate.queryForList(mainQuery);
				// for (Map<String, Object> data : payableAmountList) {
				// if (data.get("pableamount") != null) {
				// totalInvoicePayableAmount +=
				// Double.valueOf(data.get("pableamount").toString());
				// }
				//
				// }
				// detailsDTO.setTotalInvoicePayableAmount(totalInvoicePayableAmount);
				//
				// detailsDTO.setAmountAlreadyAdjusted(amountAlreadyAdjusted);
				// if (detailsDTO != null && detailsDTO.getTotalInvoicePayableAmount() != null
				// && detailsDTO.getAlreadyAmountPaid() != null
				// && detailsDTO.getAmountAlreadyAdjusted() != null) {
				// detailsDTO.setRemainingInvoiceAmount(detailsDTO.getTotalInvoicePayableAmount()
				// - detailsDTO.getAlreadyAmountPaid() - detailsDTO.getAmountAlreadyAdjusted());
				// } else {
				// log.error("TotalInvoicePayableAmount or AlreadyAmountPaid or
				// AmountAlreadyAdjusted not found ");
				// }
				//
				// if (detailsDTO != null && detailsDTO.getTotalInvoicePayableAmount() != null
				// && detailsDTO.getAlreadyAmountPaid() != null
				// && detailsDTO.getAmountAlreadyAdjusted() != null) {
				// double balanceAdjustmentAmount = detailsDTO.getTotalInvoicePayableAmount()
				// - detailsDTO.getAlreadyAmountPaid() - detailsDTO.getAmountAlreadyAdjusted();
				// if (balanceAdjustmentAmount >= 0) {
				// detailsDTO.setBalanceAdjustmentAmount(balanceAdjustmentAmount);
				// }
				// } else {
				// log.error("TotalInvoicePayableAmount or AlreadyAmountPaid or
				// AmountAlreadyAdjusted not found");
				// }
				//
				// detailsDTO.setId(purchaseInvoice.getId());
				// if (purchaseInvoice.getInvoiceIssuedToProductwareHouse() != null) {
				// detailsDTO.setWareHouse(
				// purchaseInvoice.getInvoiceIssuedToProductwareHouse().getCode().toString().concat(
				// " / " + purchaseInvoice.getInvoiceIssuedToProductwareHouse().getName()));
				// }
				// detailsDTO.setInvoiceDate(purchaseInvoice.getInvoiceDate());
				// detailsDTO.setInvoiceNumberPrefix(purchaseInvoice.getInvoiceNumberPrefix());
				// detailsDTO.setInvoiceNumber(purchaseInvoice.getInvoiceNumber());
				//
				// if (detailsDTO != null && detailsDTO.getTotalInvoicePayableAmount() != null
				// && detailsDTO.getAmountAlreadyAdjusted() != null
				// && detailsDTO.getAlreadyAmountPaid() != null) {
				// /*
				// * double balanceAmount = detailsDTO.getTotalInvoicePayableAmount() -
				// * detailsDTO.getAmountAlreadyAdjusted() - detailsDTO.getAlreadyAmountPaid();
				// if
				// * (balanceAmount > 0) {
				// */
				// detailsDTO.setSocietyId(supplierMaster.getId());
				// invoiceWiseAdjustmentDetailsDTOS.add(detailsDTO);
				// // }
				// } else {
				// log.error("TotalInvoicePayableAmount or AmountAlreadyAdjusted or
				// AlreadyAmountPaid not found");
				// }
				//
				// }

				responseDTO.setInvoiceWiseAdjustmentDetailsDTO(invoiceWiseAdjustmentDetailsDTOS);

				baseDTO.setResponseContent(responseDTO);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			} else {
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
				baseDTO.setErrorDescription("filters not present");
			}

		} catch (Exception exception) {
			log.error("exception Occured ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return responseDTO;
	}

	/**
	 * 
	 * @param invoiceFromDateStr
	 * @param invoiceToDateStr
	 * @param societyIds
	 * @return
	 * @throws RestException
	 */
	private List<InvoiceWiseAdjustmentDetailsDTO> getPurchaseInvoiceDetailsBySociety(String invoiceFromDateStr,
			String invoiceToDateStr, String societyIds) throws RestException {

		final String SOCIETY_ADJUSTMENT_PURCHASE_INVOICE_WISE_DETAILS_SQL = ApplicationConstants.SOCIETY_ADJUSTMENT_PURCHASE_INVOICE_WISE_DETAILS_SQL;
		ApplicationQuery applicationQuery = applicationQueryRepository
				.findByQueryName(SOCIETY_ADJUSTMENT_PURCHASE_INVOICE_WISE_DETAILS_SQL);
		if (applicationQuery == null) {
			throw new RestException(
					"Application query is not found: " + SOCIETY_ADJUSTMENT_PURCHASE_INVOICE_WISE_DETAILS_SQL);
		}

		String queryContent = applicationQuery.getQueryContent().trim();

		queryContent = StringUtils.replace(queryContent, "${fromDate}",
				AppUtil.getValueWithSingleQuote(invoiceFromDateStr + START_TIME_SUFFIX));
		queryContent = StringUtils.replace(queryContent, "${toDate}",
				AppUtil.getValueWithSingleQuote(invoiceToDateStr + END_TIME_SUFFIX));
		queryContent = StringUtils.replace(queryContent, "${societyId}", societyIds);

		List<Map<String, Object>> dataMapList = jdbcTemplate.queryForList(queryContent);
		int dataMapListSize = dataMapList != null ? dataMapList.size() : 0;
		log.info("SocietyInvoiceAdjustmentService. getSocietyInvoiceAdjustmentDataByAllSocietyId() - dataMapListSize: "
				+ dataMapListSize);

		List<InvoiceWiseAdjustmentDetailsDTO> purchaseInvoiceList = mapPurchaseInvoiceDetailsDTOList(dataMapList);
		int purchaseInvoiceListSize = purchaseInvoiceList != null ? purchaseInvoiceList.size() : 0;
		log.info(
				"SocietyInvoiceAdjustmentService. getSocietyInvoiceAdjustmentDataBySocietyId() - purchaseInvoiceListSize: "
						+ purchaseInvoiceListSize);

		return purchaseInvoiceList;
	}

	/**
	 * 
	 * @param societyIds
	 * @return
	 * @throws RestException
	 */
	private List<AdjustmentDetailsDTO> getAdjustmentDetailsBySociety(String societyIds) throws RestException {

		final String SOCIETY_ADJUSTMENT_DETAILS_SQL = ApplicationConstants.SOCIETY_ADJUSTMENT_DETAILS_SQL;
		ApplicationQuery applicationQueryObj = applicationQueryRepository
				.findByQueryName(SOCIETY_ADJUSTMENT_DETAILS_SQL);
		if (applicationQueryObj == null) {
			throw new RestException("Application query is not found: " + SOCIETY_ADJUSTMENT_DETAILS_SQL);
		}

		String queryContentObj = applicationQueryObj.getQueryContent().trim();
		queryContentObj = StringUtils.replace(queryContentObj, "${societyId}", String.valueOf(societyIds));

		List<Map<String, Object>> adjustmentDataMapList = jdbcTemplate.queryForList(queryContentObj);
		int adjustmentDataMapListSize = adjustmentDataMapList != null ? adjustmentDataMapList.size() : 0;
		log.info("SocietyInvoiceAdjustmentService. getAdjustmentDetailsBySociety() - adjustmentDataMapListSize: "
				+ adjustmentDataMapListSize);

		List<AdjustmentDetailsDTO> adjustmentList = mapAdjustmentDetailsDTOList(adjustmentDataMapList);
		int adjustmentListSize = adjustmentList != null ? adjustmentList.size() : 0;
		log.info("SocietyInvoiceAdjustmentService. getAdjustmentDetailsBySociety() - adjustmentListSize: "
				+ adjustmentListSize);

		return adjustmentList;
	}

	/**
	 * 
	 * @param searchDto
	 * @return
	 */
	public BaseDTO getSocietyInvoiceAdjustmentDataByAllSocietyId(SearchSocietyInvoiceAdjustmentDTO searchDto) {
		log.info("SocietyInvoiceAdjustmentService. getSocietyInvoiceAdjustmentDataByAllSocietyId() - START");
		BaseDTO baseDTO = new BaseDTO();
		try {
			SocietyInvoiceAdjustmentResponseDTO responseDTO = new SocietyInvoiceAdjustmentResponseDTO();
			List<Long> societyIds = searchDto.getSocietyIds();
			Date invoiceFromDate = searchDto.getInvoiceFromDate();
			Date invoiceToDate = searchDto.getInvoiceToDate();

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

			String invoiceFromDateStr = formatter.format(invoiceFromDate);
			String invoiceToDateStr = formatter.format(invoiceToDate);

			log.info("SocietyInvoiceAdjustmentService. getSocietyInvoiceAdjustmentDataByAllSocietyId() - societyIds: "
					+ societyIds);
			log.info(
					"SocietyInvoiceAdjustmentService. getSocietyInvoiceAdjustmentDataByAllSocietyId() - invoiceFromDate: "
							+ invoiceFromDate);
			log.info(
					"SocietyInvoiceAdjustmentService. getSocietyInvoiceAdjustmentDataByAllSocietyId() - invoiceToDate: "
							+ invoiceToDate);

			String societyIdStr = AppUtil.commaSeparatedIds(societyIds);

			List<InvoiceWiseAdjustmentDetailsDTO> purchaseInvoiceList = getPurchaseInvoiceDetailsBySociety(
					invoiceFromDateStr, invoiceToDateStr, societyIdStr);

			if (!CollectionUtils.isEmpty(purchaseInvoiceList)) {
				Map<Long, List<InvoiceWiseAdjustmentDetailsDTO>> purchaseInvoiceDetailsMap = purchaseInvoiceList
						.stream().filter(it -> it.getSocietyId() != null).collect(Collectors
								.groupingBy(InvoiceWiseAdjustmentDetailsDTO::getSocietyId, Collectors.toList()));
				responseDTO.setPurchaseInvoiceDetailsMap(purchaseInvoiceDetailsMap);
			}

			// Adjustment Details

			List<AdjustmentDetailsDTO> adjustmentList = getAdjustmentDetailsBySociety(societyIdStr);
			if (!CollectionUtils.isEmpty(adjustmentList)) {
				Map<Long, List<AdjustmentDetailsDTO>> adjustnenDetailsMap = adjustmentList.stream()
						.filter(it -> it.getSocietyId() != null)
						.collect(Collectors.groupingBy(AdjustmentDetailsDTO::getSocietyId, Collectors.toList()));
				responseDTO.setAdjustmentDetailsMap(adjustnenDetailsMap);
			}

			baseDTO.setResponseContent(responseDTO);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException re) {
			log.error("RestException Occured at getSocietyInvoiceAdjustmentDataBySocietyId() ", re);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			baseDTO.setErrorDescription(re.getMessage());
		} catch (Exception exception) {
			log.error("Exception Occured at getSocietyInvoiceAdjustmentDataBySocietyId() ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	/**
	 * 
	 * @param searchDto
	 * @return
	 */
	public BaseDTO getSocietyInvoiceAdjustmentDataBySocietyId(SearchSocietyInvoiceAdjustmentDTO searchDto) {
		log.info("SocietyInvoiceAdjustmentService. getSocietyInvoiceAdjustmentDataBySocietyId() - START");
		BaseDTO baseDTO = new BaseDTO();
		try {
			SocietyInvoiceAdjustmentResponseDTO responseDTO = new SocietyInvoiceAdjustmentResponseDTO();
			Long societyId = searchDto.getSocietyId();
			Date invoiceFromDate = searchDto.getInvoiceFromDate();
			Date invoiceToDate = searchDto.getInvoiceToDate();

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

			String invoiceFromDateStr = formatter.format(invoiceFromDate);
			String invoiceToDateStr = formatter.format(invoiceToDate);
			String societyIdStr = String.valueOf(societyId);

			log.info("SocietyInvoiceAdjustmentService. getSocietyInvoiceAdjustmentDataBySocietyId() - societyId: "
					+ societyId);
			log.info("SocietyInvoiceAdjustmentService. getSocietyInvoiceAdjustmentDataBySocietyId() - invoiceFromDate: "
					+ invoiceFromDate);
			log.info("SocietyInvoiceAdjustmentService. getSocietyInvoiceAdjustmentDataBySocietyId() - invoiceToDate: "
					+ invoiceToDate);

			List<InvoiceWiseAdjustmentDetailsDTO> purchaseInvoiceList = getPurchaseInvoiceDetailsBySociety(
					invoiceFromDateStr, invoiceToDateStr, societyIdStr);

			// Adjustment Details
			List<AdjustmentDetailsDTO> adjustmentList = getAdjustmentDetailsBySociety(societyIdStr);

			if (CollectionUtils.isEmpty(adjustmentList)) {
				throw new RestException("SocietyInvoiceAdjustment not found for the societyId: " + societyIdStr);
			}

			responseDTO.setInvoiceWiseAdjustmentDetailsDTO(purchaseInvoiceList);
			responseDTO.setAdjustmentDetailsDTOList(adjustmentList);

			baseDTO.setResponseContent(responseDTO);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException re) {
			log.error("RestException Occured at getSocietyInvoiceAdjustmentDataBySocietyId() ", re);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			baseDTO.setErrorDescription(re.getMessage());
		} catch (Exception exception) {
			log.error("Exception Occured at getSocietyInvoiceAdjustmentDataBySocietyId() ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	private Integer getInt(Map<String, Object> map, String key) {
		log.info("getInt() - Key: " + key);
		try {
			Object obj = map.get(key);
			if (obj != null) {
				return Integer.valueOf(String.valueOf(obj));
			}
		} catch (Exception ex) {
			log.error("Exception at getInt()", ex);
		}
		return null;
	}

	/**
	 * 
	 * @param mapList
	 * @return
	 * @throws RestException
	 */
	private List<InvoiceWiseAdjustmentDetailsDTO> mapPurchaseInvoiceDetailsDTOList(List<Map<String, Object>> mapList)
			throws RestException {

		List<InvoiceWiseAdjustmentDetailsDTO> purchaseInvoiceDetailsDTOList = null;

		if (!CollectionUtils.isEmpty(mapList)) {
			purchaseInvoiceDetailsDTOList = new ArrayList<InvoiceWiseAdjustmentDetailsDTO>();
			for (Map<String, Object> map : mapList) {

				Double amountAlreadyAdjusted = getDouble(map, "amount_already_adjusted");
				amountAlreadyAdjusted = amountAlreadyAdjusted != null ? amountAlreadyAdjusted : 0D;

				Double amountAlreadyPaid = getDouble(map, "already_amount_paid");
				amountAlreadyPaid = amountAlreadyPaid != null ? amountAlreadyPaid : 0D;

				Double totalInvoicePayableAmount = getDouble(map, "total_invoice_payable_amount");
				totalInvoicePayableAmount = totalInvoicePayableAmount != null ? totalInvoicePayableAmount : 0D;

				Double balanceAdjustmentAmount = totalInvoicePayableAmount - amountAlreadyPaid - amountAlreadyAdjusted;
				balanceAdjustmentAmount = balanceAdjustmentAmount != null
						? (balanceAdjustmentAmount <= 0D ? 0D : balanceAdjustmentAmount)
						: 0D;

				InvoiceWiseAdjustmentDetailsDTO invoiceWiseAdjustmentDetailsDTO = new InvoiceWiseAdjustmentDetailsDTO();

				invoiceWiseAdjustmentDetailsDTO.setId(getLong(map, "purchase_invoice_id"));
				invoiceWiseAdjustmentDetailsDTO.setSocietyId(getLong(map, "society_id"));
				invoiceWiseAdjustmentDetailsDTO
						.setWareHouse(getString(map, "entity_code").concat(" / " + getString(map, "entity_name")));
				invoiceWiseAdjustmentDetailsDTO.setInvoiceDate(getDate(map, "invoice_date"));
				invoiceWiseAdjustmentDetailsDTO.setInvoiceNumberPrefix(getString(map, "invoice_number_prefix"));
				invoiceWiseAdjustmentDetailsDTO.setInvoiceNumber(getInt(map, "invoice_number"));
				invoiceWiseAdjustmentDetailsDTO.setAmountAlreadyAdjusted(amountAlreadyAdjusted);
				invoiceWiseAdjustmentDetailsDTO.setAlreadyAmountPaid(amountAlreadyPaid);
				invoiceWiseAdjustmentDetailsDTO.setTotalInvoicePayableAmount(totalInvoicePayableAmount);
				invoiceWiseAdjustmentDetailsDTO.setRemainingInvoiceAmount(balanceAdjustmentAmount);
				invoiceWiseAdjustmentDetailsDTO.setBalanceAdjustmentAmount(balanceAdjustmentAmount);
				purchaseInvoiceDetailsDTOList.add(invoiceWiseAdjustmentDetailsDTO);

			}
		}

		return purchaseInvoiceDetailsDTOList;
	}

	/**
	 * 
	 * @param mapList
	 * @return
	 * @throws RestException
	 */
	private List<AdjustmentDetailsDTO> mapAdjustmentDetailsDTOList(List<Map<String, Object>> mapList)
			throws RestException {

		List<AdjustmentDetailsDTO> adjustmentDetailsDTOList = null;

		if (!CollectionUtils.isEmpty(mapList)) {
			adjustmentDetailsDTOList = new ArrayList<AdjustmentDetailsDTO>();
			for (Map<String, Object> map : mapList) {

				AdjustmentDetailsDTO adjustmentDetailsDTO = new AdjustmentDetailsDTO();
				adjustmentDetailsDTO.setId(getLong(map, "adjustment_id"));
				adjustmentDetailsDTO.setSocietyId(getLong(map, "society_id"));
				adjustmentDetailsDTO.setDate(getDate(map, "created_date"));
				adjustmentDetailsDTO.setPercentageOfAmountToAdjust(0D);
				adjustmentDetailsDTO.setAdjustmentName(getString(map, "adjustment_name"));
				adjustmentDetailsDTO.setAdjustmentCode(getString(map, "adjustment_code"));
				adjustmentDetailsDTO.setCreatedDate(getDate(map, "created_date"));
				Double totalAdjustmentAmount = getDouble(map, "total_adjustment_amount");
				totalAdjustmentAmount = totalAdjustmentAmount != null ? totalAdjustmentAmount : 0D;
				adjustmentDetailsDTO.setTotalAdjustmentAmount(getDouble(map, "total_adjustment_amount"));
				Double amountAlreadyAdjusted = getDouble(map, "amount_already_adjusted");
				amountAlreadyAdjusted = amountAlreadyAdjusted != null ? amountAlreadyAdjusted : 0D;
				adjustmentDetailsDTO.setAmountAlreadyAdjusted(amountAlreadyAdjusted);
				adjustmentDetailsDTO.setBalanceAdjustmentAmount(totalAdjustmentAmount - amountAlreadyAdjusted);
				adjustmentDetailsDTO.setAmountToBeAdjusted(0D);
				adjustmentDetailsDTOList.add(adjustmentDetailsDTO);

			}

		}

		return adjustmentDetailsDTOList;
	}

	private Date getDate(Map<String, Object> map, String key) {
		log.info("getDate() - Key: " + key);
		try {
			Object obj = map.get(key);
			log.info("getDate() - obj1: " + obj);
			if (obj != null && !obj.toString().isEmpty()) {
				return DATE_FORMAT.parse(obj.toString());
			}
		} catch (Exception ex) {
			log.error("Exception at getDate()", ex);
		}
		return null;
	}

	public SocietyInvoiceAdjustmentResponseDTO getSocietyAdjustmentListBySocietyCode(
			SearchSocietyInvoiceAdjustmentDTO searchDto) {
		log.info("<--Starts SocietyInvoiceAdjustmentService .getAll-->");
		BaseDTO baseDTO = new BaseDTO();
		SocietyInvoiceAdjustmentResponseDTO responseDTO = new SocietyInvoiceAdjustmentResponseDTO();
		List<Long> supplierIdList = new ArrayList<Long>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String from = formatter.format(searchDto.getInvoiceFromDate());
		String to = formatter.format(searchDto.getInvoiceToDate());
		List<Map<String, Object>> payableAmountList = new ArrayList<Map<String, Object>>();
		String sql = "select tot.itemamount+tot.taxvalue as pableamount from  "
				+ "(select  sum(pii.item_amount/2) as itemamount,sum(piit.tax_value) as taxvalue from purchase_invoice pi "
				+ "join purchase_invoice_items pii on pii.purchase_invoice_id=pi.id "
				+ "join purchase_invoice_item_tax piit on piit.purchase_invoice_item_id=pii.id "
				+ "join supplier_master sm on sm.id=pi.supplier_id "
				+ "where pi.id=:purchaseInvoiceId and sm.id=:supplierId  "
				+ "and date(pi.created_date) between :fromDate and :toDate) as tot";

		try {
			if ((searchDto.getSocietyId() != null)) {
				log.info("SocietyInvoiceAdjustmentService filters :::" + searchDto.toString());

				// DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

				Date invoiceFromDate = searchDto.getInvoiceFromDate();
				Date invoiceToDate = searchDto.getInvoiceToDate();
				Long societyId = searchDto.getSocietyId();
				sql = sql.replace(":supplierId", societyId.toString());
				sql = sql.replace(":fromDate", "'" + from + "'");
				sql = sql.replace(":toDate", "'" + to + "'");

				log.info("getSocietyAdjustmentListBySocietyCode :: invoiceFromDate ==> " + invoiceFromDate);
				log.info("getSocietyAdjustmentListBySocietyCode :: invoiceToDate ==> " + invoiceToDate);
				log.info("getSocietyAdjustmentListBySocietyCode :: societyId ==> " + societyId);

				List<SocietyInvoiceAdjustment> societyInvoiceAdjustmentList = societyInvoiceAdjustmentRepository
						.findBySocietyId(societyId);
				if (societyInvoiceAdjustmentList == null) {
					throw new RestException("SocietyInvoiceAdjustment not Found for the societyId: " + societyId);
				}

				// creating map with societyId and AdjustmentAmount
				HashMap<Long, Double> societyInvoiceAdjustmentDetails = new HashMap<>();
				societyInvoiceAdjustmentDetails.put(societyId, societyInvoiceAdjustmentList.stream()
						.mapToDouble(societyInvoiceAdj -> societyInvoiceAdj.getAdjustmentAmount()).sum());
				SocietyWiseAdjustmentDetailsDTO societyWiseAdjustmentDetailsDTO = null;
				List<SocietyWiseAdjustmentDetailsDTO> societyWiseAdjustmentDetailsDTOS = this
						.getSocietyWiseInvoiceAdjustment(societyInvoiceAdjustmentDetails, searchDto);
				log.info("societyWiseAdjustmentDetailsDTOS==>" + societyWiseAdjustmentDetailsDTOS.size());
				if (societyWiseAdjustmentDetailsDTOS != null && !societyWiseAdjustmentDetailsDTOS.isEmpty()) {
					societyWiseAdjustmentDetailsDTO = societyWiseAdjustmentDetailsDTOS.get(0);
				}

				// creating AdjustmentDetailsDTO
				List<AdjustmentDetailsDTO> adjustmentDetailsDTOLocalList = new ArrayList<>();
				for (SocietyInvoiceAdjustment societyInvoiceAdjustment : societyInvoiceAdjustmentList) {
					AdjustmentDetailsDTO adjustmentDetailsDTO = new AdjustmentDetailsDTO();
					adjustmentDetailsDTO.setId(societyInvoiceAdjustment.getId());
					adjustmentDetailsDTO.setDate(societyInvoiceAdjustment.getCreatedDate());
					adjustmentDetailsDTO.setPercentageOfAmountToAdjust(100.0);
					if (societyInvoiceAdjustment.getSocietyAdjustment() != null
							&& societyInvoiceAdjustment.getSocietyAdjustment().getAdjustmentName() != null) {
						adjustmentDetailsDTO
								.setAdjustmentName(societyInvoiceAdjustment.getSocietyAdjustment().getAdjustmentName());
					}
					if (societyInvoiceAdjustment.getSocietyAdjustment() != null
							&& societyInvoiceAdjustment.getSocietyAdjustment().getAdjustmentCode() != null) {
						adjustmentDetailsDTO
								.setAdjustmentCode(societyInvoiceAdjustment.getSocietyAdjustment().getAdjustmentCode());
					}
					adjustmentDetailsDTO.setCreatedDate(societyInvoiceAdjustment.getCreatedDate());
					adjustmentDetailsDTO.setTotalAdjustmentAmount(societyInvoiceAdjustment.getAdjustmentAmount());
					// adjustmentDetailsDTO.setGlaccount(societyInvoiceAdjustment.getSocietyAdjustment().get);
					/*
					 * if (societyWiseAdjustmentDetailsDTO != null &&
					 * societyWiseAdjustmentDetailsDTO.getBalanceAdjustmentAmount() != null) {
					 */

					List<PurchaseInvoiceAdjustment> purchaseInvoiceAdjustmentList = purchaseInvoiceAdjustmentRepository
							.getPurchaseInvoiceAdjByadjustmentId(societyInvoiceAdjustment.getId());

					if (purchaseInvoiceAdjustmentList != null && purchaseInvoiceAdjustmentList.size() > 0) {
						adjustmentDetailsDTO.setAmountAlreadyAdjusted(purchaseInvoiceAdjustmentList.stream()
								.mapToDouble(pia -> pia.getTotalAdjustedAmount()).sum());
					} else {
						adjustmentDetailsDTO.setAmountAlreadyAdjusted(0D);
					}
					adjustmentDetailsDTO.setBalanceAdjustmentAmount(societyInvoiceAdjustment.getAdjustmentAmount()
							- adjustmentDetailsDTO.getAmountAlreadyAdjusted());
					/* } */
					adjustmentDetailsDTO.setAmountToBeAdjusted(adjustmentDetailsDTO.getBalanceAdjustmentAmount());
					/*
					 * if (adjustmentDetailsDTO != null &&
					 * adjustmentDetailsDTO.getTotalAdjustmentAmount() != null &&
					 * adjustmentDetailsDTO.getBalanceAdjustmentAmount() != null) {
					 */

					/* } */
					adjustmentDetailsDTOLocalList.add(adjustmentDetailsDTO);
				}
				responseDTO.setSocietyWiseAdjustmentDetailsDTO(societyWiseAdjustmentDetailsDTO);
				responseDTO.setAdjustmentDetailsDTOList(adjustmentDetailsDTOLocalList);

				// getting invoice wise adjustment details
				log.info("getSocietyAdjustmentListBySocietyCode :: societyId==> " + societyId);
				SupplierMaster supplierMaster = supplierMasterRepository.findOne(societyId);

				List<PurchaseInvoice> purchaseInvoiceList = new ArrayList<>();
				if (searchDto != null && searchDto.getInvoiceFromDate() != null
						&& searchDto.getInvoiceToDate() != null) {
					// purchaseInvoiceList =
					// purchaseInvoiceRepository.getPurchaseInvoicesBetweenCreatedDatesandsupplierID(
					// searchDto.getInvoiceFromDate(), searchDto.getInvoiceToDate(),
					// supplierMaster.getId());

					purchaseInvoiceList = purchaseInvoiceRepository
							.getPurchaseInvoicesBetweenCreatedDatesandsupplierIDNew(searchDto.getInvoiceFromDate(),
									searchDto.getInvoiceToDate(), supplierMaster.getId());
				} else {
					purchaseInvoiceList = purchaseInvoiceRepository.findAllBySupplierMasterOrderById(supplierMaster);
				}

				List<Long> purchaseInvoiceIdsbyProductIds = new ArrayList<>();
				if (searchDto.getProductVarietyIds() != null && !searchDto.getProductVarietyIds().isEmpty()) {
					for (Long productId : searchDto.getProductVarietyIds()) {
						List<Long> purchaseInvoiceIdsbyProductIdtemp = purchaseInvoiceItemsRepository
								.findByProductVarietyId(productId);
						purchaseInvoiceIdsbyProductIds.addAll(purchaseInvoiceIdsbyProductIdtemp);
					}
					Set<Long> hashset = new LinkedHashSet<Long>(purchaseInvoiceIdsbyProductIds);
					purchaseInvoiceIdsbyProductIds.clear();
					purchaseInvoiceIdsbyProductIds.addAll(hashset);

					List<PurchaseInvoice> purchaseInvoiceListtemp = new ArrayList<>();
					for (PurchaseInvoice purchaseInvoice : purchaseInvoiceList) {
						if (purchaseInvoiceIdsbyProductIds.stream().anyMatch(pids -> pids == purchaseInvoice.getId())) {
							log.info(":::Matched:::" + purchaseInvoiceIdsbyProductIds.stream()
									.anyMatch(pids -> pids == purchaseInvoice.getId()));
							purchaseInvoiceListtemp.add(purchaseInvoice);
						} else {
							log.info(":::Not Matched:::" + purchaseInvoiceIdsbyProductIds.stream()
									.anyMatch(pids -> pids == purchaseInvoice.getId()));
						}
					}
					purchaseInvoiceList.clear();
					purchaseInvoiceList.addAll(purchaseInvoiceListtemp);
				}

				log.info("getSocietyAdjustmentListBySocietyCode :: purchaseInvoiceList size==> "
						+ purchaseInvoiceList.size());
				List<InvoiceWiseAdjustmentDetailsDTO> invoiceWiseAdjustmentDetailsDTOS = new ArrayList<>();

				int checkSno = 1;
				for (PurchaseInvoice purchaseInvoice : purchaseInvoiceList) {
					log.info(checkSno + " <=getSocietyAdjustmentListBySocietyCode=> " + purchaseInvoice.getId());
					checkSno++;
					InvoiceWiseAdjustmentDetailsDTO detailsDTO = new InvoiceWiseAdjustmentDetailsDTO();

					double amountAlreadyAdjusted = 0.0;

					List<PurchaseInvoiceAdjustment> purchaseInvoiceAdjustmentList = purchaseInvoiceAdjustmentRepository
							.getByInvoiceId(purchaseInvoice.getId());
					for (PurchaseInvoiceAdjustment purchaseInvoiceAdjustment : purchaseInvoiceAdjustmentList) {
						if (purchaseInvoiceAdjustment != null) {

							/*
							 * if (invoiceFromDate != null && invoiceToDate != null) { Date createdDate =
							 * purchaseInvoiceAdjustment.getCreatedDate(); if
							 * (!(createdDate.after(invoiceFromDate) && createdDate.before(invoiceToDate)))
							 * { continue; } }
							 */
							amountAlreadyAdjusted += purchaseInvoiceAdjustment.getTotalAdjustedAmount();
						}
					}

					List<VoucherDetails> voucherDetailsList = voucherDetailsRepository
							.findAllByPurchaseInvoice(purchaseInvoice);

					// voucherDetailsList.forEach(voucherDetails -> {
					for (VoucherDetails voucherDetails : voucherDetailsList) {
						if (voucherDetails != null && voucherDetails.getAmount() != null && detailsDTO != null
								&& detailsDTO.getAlreadyAmountPaid() != null) {
							detailsDTO.setAlreadyAmountPaid(
									voucherDetails.getAmount() + detailsDTO.getAlreadyAmountPaid());
						} else {
							log.error("Voucher amount or aleady amount not found ");
						}
						// });
					}
					log.info("getSocietyAdjustmentListBySocietyCode :: GrandTotal==> "
							+ purchaseInvoice.getGrandTotal());

					// detailsDTO.setTotalInvoicePayableAmount(purchaseInvoice.getGrandTotal());
					double totalInvoicePayableAmount = 0.0;
					String mainQuery = sql;
					mainQuery = mainQuery.replace(":purchaseInvoiceId", purchaseInvoice.getId().toString());

					payableAmountList = jdbcTemplate.queryForList(mainQuery);
					for (Map<String, Object> data : payableAmountList) {
						if (data.get("pableamount") != null) {
							totalInvoicePayableAmount += Double.valueOf(data.get("pableamount").toString());
						}

					}
					detailsDTO.setTotalInvoicePayableAmount(totalInvoicePayableAmount);

					detailsDTO.setAmountAlreadyAdjusted(amountAlreadyAdjusted);
					if (detailsDTO != null && detailsDTO.getTotalInvoicePayableAmount() != null
							&& detailsDTO.getAlreadyAmountPaid() != null
							&& detailsDTO.getAmountAlreadyAdjusted() != null) {
						detailsDTO.setRemainingInvoiceAmount(detailsDTO.getTotalInvoicePayableAmount()
								- detailsDTO.getAlreadyAmountPaid() - detailsDTO.getAmountAlreadyAdjusted());
					} else {
						log.error("TotalInvoicePayableAmount or AlreadyAmountPaid or AmountAlreadyAdjusted not found ");
					}

					if (detailsDTO != null && detailsDTO.getTotalInvoicePayableAmount() != null
							&& detailsDTO.getAlreadyAmountPaid() != null
							&& detailsDTO.getAmountAlreadyAdjusted() != null) {
						double balanceAdjustmentAmount = detailsDTO.getTotalInvoicePayableAmount()
								- detailsDTO.getAlreadyAmountPaid() - detailsDTO.getAmountAlreadyAdjusted();
						if (balanceAdjustmentAmount >= 0) {
							detailsDTO.setBalanceAdjustmentAmount(balanceAdjustmentAmount);
						}
					} else {
						log.error("TotalInvoicePayableAmount or AlreadyAmountPaid or AmountAlreadyAdjusted not found");
					}

					detailsDTO.setId(purchaseInvoice.getId());
					if (purchaseInvoice.getInvoiceIssuedToProductwareHouse() != null) {
						detailsDTO.setWareHouse(
								purchaseInvoice.getInvoiceIssuedToProductwareHouse().getCode().toString().concat(
										" / " + purchaseInvoice.getInvoiceIssuedToProductwareHouse().getName()));
					}
					detailsDTO.setInvoiceDate(purchaseInvoice.getInvoiceDate());
					detailsDTO.setInvoiceNumberPrefix(purchaseInvoice.getInvoiceNumberPrefix());
					detailsDTO.setInvoiceNumber(purchaseInvoice.getInvoiceNumber());

					if (detailsDTO != null && detailsDTO.getTotalInvoicePayableAmount() != null
							&& detailsDTO.getAmountAlreadyAdjusted() != null
							&& detailsDTO.getAlreadyAmountPaid() != null) {
						/*
						 * double balanceAmount = detailsDTO.getTotalInvoicePayableAmount() -
						 * detailsDTO.getAmountAlreadyAdjusted() - detailsDTO.getAlreadyAmountPaid(); if
						 * (balanceAmount > 0) {
						 */
						detailsDTO.setSocietyId(supplierMaster.getId());
						invoiceWiseAdjustmentDetailsDTOS.add(detailsDTO);
						// }
					} else {
						log.error("TotalInvoicePayableAmount or AmountAlreadyAdjusted or AlreadyAmountPaid not found");
					}

				}

				responseDTO.setInvoiceWiseAdjustmentDetailsDTO(invoiceWiseAdjustmentDetailsDTOS);

				baseDTO.setResponseContent(responseDTO);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			} else {
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
				baseDTO.setErrorDescription("filters not present");
			}

		} catch (Exception exception) {
			log.error("exception Occured ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return responseDTO;
	}

	public BaseDTO getSocietyDropdown(SocietyDropDownRequestDTO circleMasters) {
		log.info("<--Starts SocietyInvoiceAdjustmentService .getSocietyDropdown-->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<Object[]> supplierMasterList = supplierMasterRepository
					.findAllByCircleMaster(new HashSet<>(circleMasters.getCircleMasters()));

			List<SocietyCodeDropDownDTO> societyCodeDropDownDTOSList = new ArrayList<>();

			supplierMasterList.forEach(supplierMaster -> {
				SocietyCodeDropDownDTO societyCodeDropDownDTO = new SocietyCodeDropDownDTO();
				societyCodeDropDownDTO.setId(new Long(supplierMaster[0].toString()));
				societyCodeDropDownDTO.setCode((String) supplierMaster[1]);
				societyCodeDropDownDTO.setName((String) supplierMaster[2]);
				societyCodeDropDownDTO
						.setDisplayName(societyCodeDropDownDTO.getCode() + "/" + societyCodeDropDownDTO.getName());

				societyCodeDropDownDTOSList.add(societyCodeDropDownDTO);
			});
			baseDTO.setResponseContents(societyCodeDropDownDTOSList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (Exception exception) {
			log.error("exception Occured ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO getRegionDropdown() {
		log.info("<--Starts SocietyInvoiceAdjustmentService .getRegionDropdown-->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<CircleMaster> circleMasterList = circleMasterRepository.findAllCircle();

			List<RegionCodeDropDownDTO> regionCodeDropDownDTOSList = new ArrayList<>();

			circleMasterList.forEach(circleMaster -> {
				RegionCodeDropDownDTO regionCodeDropDownDTO = new RegionCodeDropDownDTO();
				regionCodeDropDownDTO.setId(circleMaster.getId());
				regionCodeDropDownDTO.setCode(circleMaster.getCode());
				regionCodeDropDownDTO.setName(circleMaster.getName());
				regionCodeDropDownDTO
						.setDisplayName(regionCodeDropDownDTO.getCode() + "/" + regionCodeDropDownDTO.getName());
				regionCodeDropDownDTOSList.add(regionCodeDropDownDTO);
			});
			baseDTO.setResponseContents(regionCodeDropDownDTOSList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (Exception exception) {
			log.error("exception Occured ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO getSocietyInvoiceAdjustmentByInvoiceId(Long invoiceId) {
		log.info("<==== Starts SocietyInvoiceAdjustmentService.getSocietyInvoiceAdjustmentByInvoiceId() =====>");
		BaseDTO response = new BaseDTO();
		try {

			PurchaseInvoice purchaseInvoice = purchaseInvoiceRepository.findOne(invoiceId);
			if (purchaseInvoice != null) {
				List<SocietyInvoiceAdjustment> societyInvoiceAdjustmentList = societyInvoiceAdjustmentRepository
						.findAllBySocietyId(purchaseInvoice.getSupplierMaster().getId());
				Map<Long, Double> invoiceAdjustmentMap = getSocietyInvoiceAdjustmentGroupedBySociety(
						societyInvoiceAdjustmentList, null);
				List<SocietyWiseAdjustmentDetailsDTO> result = getSocietyWiseInvoiceAdjustment(invoiceAdjustmentMap,
						null);
				response.setResponseContents(result);
				response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
				response.setTotalRecords(result.size());
			} else {
				throw new RestException("PurchaseInvoice  not found for invoiceId : " + invoiceId);
			}
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (RestException re) {
			log.error(
					"RestException occured in SocietyInvoiceAdjustmentService.getSocietyInvoiceAdjustmentByInvoiceId ...",
					re);
			response.setStatusCode(re.getStatusCode());
		} catch (Exception e) {
			log.error("Exception occured in SocietyInvoiceAdjustmentService.deleteSocietyInvoiceAdjustmentById ...", e);
			response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<==== Ends SocietyInvoiceAdjustmentService.deleteSocietyInvoiceAdjustmentById =====>");
		return responseWrapper.send(response);

	}

	// @Transactional(rollbackFor = Exception.class)
	public BaseDTO saveOrUpdateold(List<PurchaseInvoiceAdjustment> purchaseInvoiceAdjustmentList) {
		log.info("<==== Starts SocietyInvoiceAdjustmentService.saveOrUpdate =====>");
		BaseDTO response = new BaseDTO();
		PurchaseInvoiceAdjustment savedPurchaseInvoiceAdjustment = null;
		List<PurchaseInvoiceAdjustment> invoiceAdjustmentList = new ArrayList<PurchaseInvoiceAdjustment>();
		boolean voucherFlag = false;
		try {
			// List<PurchaseInvoiceAdjustment> res = new ArrayList<>();
			log.info("saveOrUpdate :: purchaseInvoiceAdjustmentList size==> " + purchaseInvoiceAdjustmentList.size());
			for (PurchaseInvoiceAdjustment adjustment : purchaseInvoiceAdjustmentList) {
				PurchaseInvoice purchaseInvoice = purchaseInvoiceRepository.findOne(adjustment.getInvoiceId().getId());
				if (purchaseInvoice == null) {
					throw new RestException("PurchaseInvoice not found for the given purchaseInvoiceId: "
							+ adjustment.getInvoiceId().getId());
				}
				adjustment.setInvoiceId(purchaseInvoice);

				SocietyInvoiceAdjustment societyInvoiceAdjustment = societyInvoiceAdjustmentRepository
						.getOne(adjustment.getSocietyInvoiceAdjustment().getId());
				adjustment.setSocietyInvoiceAdjustment(societyInvoiceAdjustment);
				if (societyInvoiceAdjustment != null && societyInvoiceAdjustment.getSocietyAdjustment() != null) {
					if (societyInvoiceAdjustment.getSocietyAdjustment().getGlAccount() != null
							&& societyInvoiceAdjustment.getSocietyAdjustment().getGlAccount().getId() != null) {
						GlAccount glAccount = glAccountRepository
								.findById(societyInvoiceAdjustment.getSocietyAdjustment().getGlAccount().getId());
						adjustment.setGlAccount(glAccount);

					}
				}

				if (adjustment.getId() == null) {
					savedPurchaseInvoiceAdjustment = saveSocietyInvoiceAdjustment(adjustment);
					voucherFlag = true;
				} else {
					savedPurchaseInvoiceAdjustment = updateSocietyInvoiceAdjustment(adjustment);
				}
				invoiceAdjustmentList.add(savedPurchaseInvoiceAdjustment);
			}

			Voucher voucher = saveVoucherAndPayment(invoiceAdjustmentList);

			// Update Voucher ID in PurchaseInvoiceAdjustment Table
			if (voucherFlag) {
				for (PurchaseInvoiceAdjustment adjustment : invoiceAdjustmentList) {
					PurchaseInvoiceAdjustment invoiceAdjustment = purchaseInvoiceAdjustmentRepository
							.findById(adjustment.getId());
					invoiceAdjustment.setVoucher(voucher);
					purchaseInvoiceAdjustmentRepository.save(invoiceAdjustment);
				}
			}

			log.info("<-----SocietyInvoiceAdjustment saved or updated successfully--->");
			if (savedPurchaseInvoiceAdjustment != null) {
				response.setMessage("Society adjustment inserted successfully");
				response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				response.setMessage("Society adjustment not inserted");
				response.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			}

		} catch (ObjectOptimisticLockingFailureException lockEx) {
			log.warn("====>> Error while updating department <<====", lockEx);
			response.setStatusCode(ErrorDescription.CANNOT_UPDATE_LOCKED_RECORD.getErrorCode());
		} catch (RestException re) {
			log.error("RestException occured in SocietyInvoiceAdjustmentService.saveOrUpdate", re);
			response.setStatusCode(re.getStatusCode());
		} catch (Exception e) {
			log.error("Exception occured in SocietyInvoiceAdjustmentService.saveOrUpdate", e);
			response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<==== Ends SocietyInvoiceAdjustmentService.saveOrUpdate =====>");
		return response;
	}

	public BaseDTO saveOrUpdate(List<PurchaseInvoiceAdjustment> purchaseInvoiceAdjustmentList) {
		log.info("<==== Starts SocietyInvoiceAdjustmentService.saveOrUpdate =====>");
		BaseDTO response = new BaseDTO();
		PurchaseInvoiceAdjustment savedPurchaseInvoiceAdjustment = null;
		PurchaseInvoiceAdjustment saveAdustmentAmount = null;
		List<PurchaseInvoiceAdjustment> invoiceAdjustmentList = new ArrayList<PurchaseInvoiceAdjustment>();
		
		boolean voucherFlag = false;
		try {
			int index = 0;
			// List<PurchaseInvoiceAdjustment> res = new ArrayList<>();
			log.info("saveOrUpdate :: purchaseInvoiceAdjustmentList size==> " + purchaseInvoiceAdjustmentList.size());
			
			Map<Long, List<PurchaseInvoiceAdjustment>> adjustnenDetailsMap = purchaseInvoiceAdjustmentList.stream()
					.filter(it -> it.getSupplierId() != null)
					.collect(Collectors.groupingBy(it -> it.getSupplierId()));
			
			for(Map.Entry<Long, List<PurchaseInvoiceAdjustment>> entry : adjustnenDetailsMap.entrySet()) {
				List<PurchaseInvoiceAdjustment>  purchaseInvoiceAdjustmentLists = entry.getValue();
				List<AdjustmentDetailsDTO> adustmentList = new ArrayList<AdjustmentDetailsDTO>();
				for (PurchaseInvoiceAdjustment adjustment : purchaseInvoiceAdjustmentLists) {
					PurchaseInvoice purchaseInvoice = purchaseInvoiceRepository.findOne(adjustment.getInvoiceId().getId());
					if (purchaseInvoice == null) {
						throw new RestException("PurchaseInvoice not found for the given purchaseInvoiceId: "
								+ adjustment.getInvoiceId().getId());
					}
					adjustment.setInvoiceId(purchaseInvoice);
					
					if(adustmentList==null || adustmentList.isEmpty()) {
					adustmentList = adjustment.getAdjustmentDetailsDTOList();
					}
					// if (index == 0) {
					for (AdjustmentDetailsDTO objSociety : adustmentList) {
						saveAdustmentAmount = new PurchaseInvoiceAdjustment();
						// if (objSociety.getPercentageOfAmountToAdjust() < 100
						// || objSociety.getPercentageOfAmountToAdjust() == 100) {
						if(objSociety.getAmountToBeAdjusted()!=null && objSociety.getAmountToBeAdjusted()>0 && adjustment.getTotalAdjustedAmount()>0) {
						SocietyInvoiceAdjustment societyInvoiceAdjustment = societyInvoiceAdjustmentRepository
								.getOne(objSociety.getId());
						if (societyInvoiceAdjustment != null && societyInvoiceAdjustment.getSocietyAdjustment() != null) {
							if (societyInvoiceAdjustment.getSocietyAdjustment().getGlAccount() != null
									&& societyInvoiceAdjustment.getSocietyAdjustment().getGlAccount().getId() != null) {
								GlAccount glAccount = glAccountRepository
										.findById(societyInvoiceAdjustment.getSocietyAdjustment().getGlAccount().getId());
								adjustment.setGlAccount(glAccount);
							}
						}
						if(adjustment.getTotalAdjustedAmount()>objSociety.getAmountToBeAdjusted()) {
						saveAdustmentAmount.setTotalAdjustedAmount(objSociety.getAmountToBeAdjusted());
						adjustment.setTotalAdjustedAmount(adjustment.getTotalAdjustedAmount()-objSociety.getAmountToBeAdjusted());
						objSociety.setAmountToBeAdjusted(0D);
						}else {
							saveAdustmentAmount.setTotalAdjustedAmount(adjustment.getTotalAdjustedAmount());
							objSociety.setAmountToBeAdjusted(objSociety.getAmountToBeAdjusted()-adjustment.getTotalAdjustedAmount());
							adjustment.setTotalAdjustedAmount(0D);
						}
						saveAdustmentAmount.setInvoiceId(adjustment.getInvoiceId());
						saveAdustmentAmount.setGlAccount(adjustment.getGlAccount());
						saveAdustmentAmount.setInvoicePayableAmount(adjustment.getInvoicePayableAmount());
						saveAdustmentAmount.setInvoiceBalanceAmountPayable(adjustment.getInvoiceBalanceAmountPayable());

						if (objSociety.getId() != null) {
							saveAdustmentAmount.setSocietyInvoiceAdjustment(
									societyInvoiceAdjustmentRepository.findOne(objSociety.getId()));
						}

						if (adjustment.getId() == null) {
							savedPurchaseInvoiceAdjustment = saveSocietyInvoiceAdjustment(saveAdustmentAmount);
							voucherFlag = true;
						} else {
							savedPurchaseInvoiceAdjustment = updateSocietyInvoiceAdjustment(saveAdustmentAmount);
						}
						invoiceAdjustmentList.add(savedPurchaseInvoiceAdjustment);
						// }
						}
					}
					// }
					// index++;
				}
			}
						
			

			Voucher voucher = saveVoucherAndPayment(invoiceAdjustmentList);

			// Update Voucher ID in PurchaseInvoiceAdjustment Table
			if (voucherFlag) {
				for (PurchaseInvoiceAdjustment adjustment : invoiceAdjustmentList) {
					PurchaseInvoiceAdjustment invoiceAdjustment = purchaseInvoiceAdjustmentRepository
							.findById(adjustment.getId());
					invoiceAdjustment.setVoucher(voucher);
					purchaseInvoiceAdjustmentRepository.save(invoiceAdjustment);
				}
			}

			log.info("<-----SocietyInvoiceAdjustment saved or updated successfully--->");
			if (savedPurchaseInvoiceAdjustment != null) {
				response.setMessage("Society adjustment inserted successfully");
				response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				response.setMessage("Society adjustment not inserted");
				response.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			}

		} catch (ObjectOptimisticLockingFailureException lockEx) {
			log.warn("====>> Error while updating department <<====", lockEx);
			response.setStatusCode(ErrorDescription.CANNOT_UPDATE_LOCKED_RECORD.getErrorCode());
		} catch (RestException re) {
			log.error("RestException occured in SocietyInvoiceAdjustmentService.saveOrUpdate", re);
			response.setStatusCode(re.getStatusCode());
		} catch (Exception e) {
			log.error("Exception occured in SocietyInvoiceAdjustmentService.saveOrUpdate", e);
			response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<==== Ends SocietyInvoiceAdjustmentService.saveOrUpdate =====>");
		return response;
	}

	private PurchaseInvoiceAdjustment saveSocietyInvoiceAdjustment(PurchaseInvoiceAdjustment purchaseInvoiceAdjustment)
			throws Exception {
		log.info("<==== Starts SaveSocietyInvoiceAdjustment method=====>");

		String tranCode = ApplicationConstants.SOCIETY_TRANSACTION_CODE;
		log.info("tranCode=====>" + tranCode);
		purchaseInvoiceAdjustment.setTransactionCode(tranCode);

		PurchaseInvoiceAdjustment purchaseInvoiceAdjustmentObj = purchaseInvoiceAdjustmentRepository
				.save(purchaseInvoiceAdjustment);
		if (purchaseInvoiceAdjustmentObj == null) {
			throw new Exception(
					"PurchaseInvoiceAdjustment failed to save purchaseInvoiceAdjustment: " + purchaseInvoiceAdjustment);
		}

		log.info("<==== Ends SaveSocietyInvoiceAdjustment method=====>");
		return purchaseInvoiceAdjustmentObj;
	}

	private Voucher saveVoucherAndPayment(List<PurchaseInvoiceAdjustment> invoiceAdjustmentList) throws Exception {
		Voucher voucher = null;
		VoucherDetails voucherDetails = null;
		Payment payment = null;
		PaymentDetails paymentDetails = null;

		// if(invoiceAdjustmentList.get(0).getInvoiceId() != null) {
		//
		// }
		// else {
		SequenceConfig sequenceConfig = sequenceConfigRepository.findBySequenceName(SequenceName.SOCIETY_ADJUSTMENT);
		if (sequenceConfig == null) {
			throw new RestException(ErrorDescription.SEQUENCE_CONFIG_NOT_EXIST);
		}
		EntityMaster entityMaster = entityMasterRepository
				.getEntityInfoByLoggedInUser(loginService.getCurrentUser().getId());
		log.info("Entity Master Code---->" + entityMaster.getCode());
		String referenceNumber = entityMaster.getCode() + sequenceConfig.getSeparator() + sequenceConfig.getPrefix()
				+ AppUtil.getCurrentMonthString() + AppUtil.getCurrentYearString();
		voucher = new Voucher();
		voucher.setReferenceNumberPrefix(referenceNumber);
		voucher.setReferenceNumber(sequenceConfig.getCurrentValue());
		voucher.setName(VoucherTypeDetails.SOCIETY_ADJUSTMENT.toString());
		voucher.setVoucherType(voucherTypeRepository.findByName(VoucherTypeCons.ADJUSTMENT.toString()));
		voucher.setNarration(VoucherTypeDetails.SOCIETY_ADJUSTMENT.toString());
		voucher.setFromDate(new Date());
		voucher.setToDate(new Date());
		voucher.setNetAmount(
				invoiceAdjustmentList.stream().mapToDouble(PurchaseInvoiceAdjustment::getTotalAdjustedAmount).sum());
		voucher.setPaymentMode(paymentModeRepository.findByCode("ADJUSTMENT"));

		payment = new Payment();
		payment.setEntityMaster(entityMaster);
		payment.setPaymentNumberPrefix(referenceNumber);
		payment.setPaymentNumber(sequenceConfig.getCurrentValue());

		for (PurchaseInvoiceAdjustment adjustment : invoiceAdjustmentList) {
			voucherDetails = new VoucherDetails();
			voucherDetails.setVoucher(voucher);
//			voucherDetails.setPurchaseInvoice(purchaseInvoiceRepository.findOne(adjustment.getInvoiceId().getId()));
			voucherDetails.setAmount(adjustment.getTotalAdjustedAmount());
			if (adjustment.getGlAccount() != null) {
				voucherDetails.setGlAccount(glAccountRepository.getOne(adjustment.getGlAccount().getId()));
			}

			voucher.getVoucherDetailsList().add(voucherDetails);

			paymentDetails = new PaymentDetails();
			paymentDetails.setPayment(payment);
			paymentDetails.setPaymentCategory(PaymentCategory.PAID_IN);
			paymentDetails.setAmount(adjustment.getTotalAdjustedAmount());
			paymentDetails.setVoucher(voucher);
			paymentDetails.setPaymentMethod(paymentMethodRepository.findByCode("ADJUSTMENT"));
			paymentDetails.setPaymentTypeMaster(paymentTypeMasterRepository.getOthersrPaymentType());
			paymentDetails.setPaidBy(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
			payment.getPaymentDetailsList().add(paymentDetails);
		}
		sequenceConfig.setCurrentValue(sequenceConfig.getCurrentValue() + 1);
		sequenceConfigRepository.save(sequenceConfig);
		// }
		voucherRepository.save(voucher);
		paymentRepository.save(payment);
		return voucher;
	}

	private PurchaseInvoiceAdjustment updateSocietyInvoiceAdjustment(
			PurchaseInvoiceAdjustment purchaseInvoiceAdjustment) throws Exception {
		log.info("<==== Starts updateSocietyInvoiceAdjustment method=====>");
		PurchaseInvoiceAdjustment obj = purchaseInvoiceAdjustmentRepository.findOne(purchaseInvoiceAdjustment.getId());
		if (obj == null) {
			throw new Exception("PurchaseInvoiceAdjustment not found for the given glAccountId: "
					+ purchaseInvoiceAdjustment.getId());
		}
		obj.setModifiedDate(new Date());
		obj.setInvoicePayableAmount(purchaseInvoiceAdjustment.getInvoicePayableAmount());
		obj.setInvoiceBalanceAmountPayable(purchaseInvoiceAdjustment.getInvoiceBalanceAmountPayable());
		obj.setTotalAdjustedAmount(purchaseInvoiceAdjustment.getTotalAdjustedAmount());
		obj = purchaseInvoiceAdjustmentRepository.save(obj);
		log.info("<==== Ends updateSocietyInvoiceAdjustment method=====>");
		return obj;
	}

	public BaseDTO getPurchaseInvoiceList(PaginationDTO paginationDTO) {
		BaseDTO baseDTO = new BaseDTO();
		List<PurchaseInvoiceAdjustmentListDTO> responseDtoList = new ArrayList<>();
		List<Voucher> voucherList = new ArrayList<>();

		try {
			log.info("<<<< ------- Start SocietyInvoiceAdjustmentService.getPurchaseInvoiceList ---------- >>>>>>>");

			log.info(":: PurchaseInvoiceAdjustment search started ::");
			Session session = entityManager.unwrap(Session.class);
			Criteria criteria = session.createCriteria(PurchaseInvoiceAdjustment.class, "invoiceAdjustment");
			criteria.createAlias("invoiceAdjustment.invoiceId", "invoice");
			criteria.createAlias("invoiceAdjustment.societyInvoiceAdjustment", "societyInvoiceAdjustment");
			criteria.createAlias("invoiceAdjustment.voucher", "voucher", JoinType.LEFT_OUTER_JOIN);

			log.info(":: Criteria search started ::");
			if (paginationDTO.getFilters() != null) {

				String transactionCode = (String) paginationDTO.getFilters().get("transactionCode");
				if (transactionCode != null) {
					criteria.add(
							Restrictions.like("invoiceAdjustment.transactionCode", "%" + transactionCode.trim() + "%")
									.ignoreCase());
				}

				String voucherNo = (String) paginationDTO.getFilters().get("referenceNumber");
				if (voucherNo != null) {

					criteria.add(Restrictions.sqlRestriction(
							"CAST(concat(voucher3_.reference_number_prefix,'-',voucher3_.reference_number) AS TEXT) LIKE '%"
									+ voucherNo.trim() + "%' "));
				}
				String invoiceNumber = (String) paginationDTO.getFilters().get("invoiceNumber");
				if (invoiceNumber != null) {

					criteria.add(Restrictions.sqlRestriction(
							"CAST(concat(invoice1_.invoice_number_prefix,invoice1_.invoice_number) AS TEXT) LIKE '%"
									+ invoiceNumber.trim() + "%' "));

				}
				if (paginationDTO.getFilters().get("invoiceId") != null) {
					String invoiceId = paginationDTO.getFilters().get("invoiceId").toString().trim();
					criteria.add(Restrictions
							.sqlRestriction("CAST(invoice1_.id AS TEXT) LIKE '%" + invoiceId.trim() + "%' "));
					// criteria.add(Restrictions.eq("invoice.id", invoiceId));
				}
				if (paginationDTO.getFilters().get("invoicePayableAmount") != null) {
					String invoicePayableAmount = paginationDTO.getFilters().get("invoicePayableAmount").toString();
					/*
					 * criteria.add(Restrictions.eq( "invoiceAdjustment.invoicePayableAmount",
					 * invoicePayableAmount));
					 */
					criteria.add(Restrictions.sqlRestriction("CAST(this_.invoice_payable_amount AS TEXT) LIKE '%"
							+ invoicePayableAmount.trim() + "%' "));
				}
				if (paginationDTO.getFilters().get("totalAdjustedAmount") != null) {
					String totalAdjustedAmount = paginationDTO.getFilters().get("totalAdjustedAmount").toString();
					/*
					 * criteria.add(Restrictions.eq( "invoiceAdjustment.totalAdjustedAmount",
					 * totalAdjustedAmount));
					 */
					criteria.add(Restrictions.sqlRestriction(
							"CAST(this_.total_adjusted_amount AS TEXT) LIKE '%" + totalAdjustedAmount.trim() + "%' "));
				}
				if (paginationDTO.getFilters().get("invoiceBalanceAmountPayable") != null) {
					String invoiceBalanceAmountPayable = paginationDTO.getFilters().get("invoiceBalanceAmountPayable")
							.toString();
					/*
					 * criteria.add(Restrictions.eq(
					 * "invoiceAdjustment.invoiceBalanceAmountPayable",
					 * invoiceBalanceAmountPayable));
					 */
					criteria.add(
							Restrictions.sqlRestriction("CAST(this_.invoice_balance_amount_payable AS TEXT) LIKE '%"
									+ invoiceBalanceAmountPayable.trim() + "%' "));
				}

				if (paginationDTO.getFilters().get("createdDate") != null) {
					Date date = new Date((long) paginationDTO.getFilters().get("createdDate"));
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = dateFormat.format(date);
					Date minDate = dateFormat.parse(strDate);
					Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
					criteria.add(
							Restrictions.conjunction().add(Restrictions.ge("invoiceAdjustment.createdDate", minDate))
									.add(Restrictions.lt("invoiceAdjustment.createdDate", maxDate)));
				}

				criteria.setProjection(Projections.rowCount());
				Integer totalResult = ((Long) criteria.uniqueResult()).intValue();
				criteria.setProjection(null);

				ProjectionList projectionList = Projections.projectionList();
				projectionList.add(Projections.property("id"));
				projectionList.add(Projections.property("transactionCode"));
				projectionList.add(Projections.property("invoicePayableAmount"));
				projectionList.add(Projections.property("totalAdjustedAmount"));
				projectionList.add(Projections.property("invoiceBalanceAmountPayable"));
				projectionList.add(Projections.property("accountHeadPostingType"));
				projectionList.add(Projections.property("societyInvoiceAdjustment.id"));
				projectionList.add(Projections.property("invoice.id"));
				projectionList.add(Projections.property("invoice.invoiceNumberPrefix"));
				projectionList.add(Projections.property("invoice.invoiceNumber"));
				projectionList.add(Projections.property("createdDate"));
				projectionList.add(Projections.property("voucher.id"));
				projectionList.add(Projections.property("voucher.referenceNumber"));
				projectionList.add(Projections.property("voucher.referenceNumberPrefix"));
				criteria.setProjection(projectionList);

				if (paginationDTO.getFirst() != null) {
					Integer pageNo = paginationDTO.getFirst();
					Integer pageSize = paginationDTO.getPageSize();
					if (pageNo != null && pageSize != null) {
						criteria.setFirstResult(pageNo * pageSize);
						criteria.setMaxResults(pageSize);
						log.info("PageNo : [" + pageNo + "] pageSize[" + pageSize + "]");
					}

					String sortField = paginationDTO.getSortField();
					String sortOrder = paginationDTO.getSortOrder();
					log.info("sortField outside : [" + sortField + "] sortOrder[" + sortOrder + "]");
					if (paginationDTO.getSortField() != null && paginationDTO.getSortOrder() != null) {
						log.info("sortField : [" + paginationDTO.getSortField() + "] sortOrder["
								+ paginationDTO.getSortOrder() + "]");

						if (paginationDTO.getSortField().equals("transactionCode")) {
							sortField = "invoiceAdjustment.transactionCode";
						} else if (sortField.equals("invoiceId")) {
							sortField = "invoice.id";
						} else if (sortField.equals("invoicePayableAmount")) {
							sortField = "invoiceAdjustment.invoicePayableAmount";
						} else if (sortField.equals("totalAdjustedAmount")) {
							sortField = "invoiceAdjustment.totalAdjustedAmount";
						} else if (sortField.equals("invoiceBalanceAmountPayable")) {
							sortField = "invoiceAdjustment.invoiceBalanceAmountPayable";
						} else if (sortField.equals("createdDate")) {
							sortField = "invoiceAdjustment.createdDate";
						} else if (sortField.equals("invoiceNumber")) {
							sortField = "invoice.invoiceNumber";
						} else if (sortField.equals("referenceNumber")) {
							sortField = "voucher.referenceNumber";
						}
						if (sortOrder.equals("DESCENDING")) {
							criteria.addOrder(Order.desc(sortField));
						} else {
							criteria.addOrder(Order.asc(sortField));
						}
					} else {
						criteria.addOrder(Order.desc("invoiceAdjustment.id"));
					}
				}

				List<?> resultList = criteria.list();

				log.info("criteria list executed and the list size is : " + resultList.size());

				if (resultList == null || resultList.isEmpty() || resultList.size() == 0) {
					log.info("PurchaseInvoiceAdjustment List is null or empty ");
				}

				Iterator<?> it = resultList.iterator();
				while (it.hasNext()) {
					Object obj[] = (Object[]) it.next();

					PurchaseInvoiceAdjustmentListDTO dto = new PurchaseInvoiceAdjustmentListDTO();

					dto.setId((Long) obj[0]);
					log.info("Id::::::::" + dto.getId());
					dto.setTransactionCode((String) obj[1]);
					dto.setInvoicePayableAmount((Double) obj[2]);
					dto.setTotalAdjustedAmount((Double) obj[3]);
					dto.setInvoiceBalanceAmountPayable((Double) obj[4]);
					dto.setAccountHeadPostingType((String) obj[5]);
					dto.setAdjustmentId((Long) obj[6]);
					dto.setInvoiceId((Long) obj[7]);
					dto.setInvoiceNumberPrefix((String) obj[8]);
					dto.setInvoiceNumber((Integer) obj[9]);
					dto.setCreatedDate((Date) obj[10]);
					dto.setVoucher_id((Long) obj[11]);
					dto.setReferenceNumber((Long) obj[12]);
					dto.setReferenceNumberPrifix((String) obj[13]);

					log.info(":: List Response ::" + dto);
					responseDtoList.add(dto);
				}
				baseDTO.setResponseContents(responseDtoList);
				baseDTO.setTotalRecords(totalResult);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

			}
		} catch (RestException re) {
			log.warn("<<===  Exception While getSocietyAdjustmentList ===>>", re);
			baseDTO.setStatusCode(re.getStatusCode());
		} catch (Exception e) {
			log.error("<<===  Exception Occured ===>>", e);
		}
		log.info("<<<< ------- End UserService.getSocietyAdjustmentList ---------- >>>>>>>");
		return baseDTO;
	}

	public BaseDTO deleteSocietyAdjustment(Long id) {
		log.info("SocietyInvoiceAdjustmentService deleteSocietyAdjustment Delete Started and the id is :: " + id);
		BaseDTO baseDTO = new BaseDTO();
		try {
			Validate.notNull(id, ErrorDescription.ROSTER_ID_IS_EMPTY);
			purchaseInvoiceAdjustmentRepository.delete(id);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("SocietyInvoiceAdjustmentService.deleteSocietyAdjustment() Completed");
		} catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
			baseDTO.setStatusCode(ErrorDescription.EDU_QUL_ID_NOT_EXISTS.getErrorCode());
		} catch (Exception exception) {
			exception.printStackTrace();
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.error("Exception occurred in SocietyInvoiceAdjustmentService.deleteSocietyAdjustment() -:", exception);

		}
		return baseDTO;
	}

	public BaseDTO getByCode(String code) {
		log.info("<<======== SocietyInvoiceAdjustmentService-----  getByCode() " + code);
		BaseDTO baseDTO = new BaseDTO();

		try {
			CircleMaster master = circleMasterRepository.findByCode(code);
			baseDTO.setResponseContent(master);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("<<=========  error in get circle master ======>>");
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}

		return baseDTO;
	}

	public BaseDTO getSocietybycode(String code) {
		log.info("<<======== SocietyInvoiceAdjustmentService-----  getSocietybycode() " + code);
		BaseDTO baseDTO = new BaseDTO();

		try {
			SupplierMaster master = supplierMasterRepository.findByCode(code);
			baseDTO.setResponseContent(master);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("<<=========  error in get circle master ======>>");
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}

		return baseDTO;
	}

	public Boolean isInteger(String input) {
		try {
			Integer.parseInt(input);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public Boolean isLong(String input) {
		try {
			Long.valueOf(input);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
}
