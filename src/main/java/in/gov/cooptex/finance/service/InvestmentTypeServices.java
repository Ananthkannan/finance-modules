package in.gov.cooptex.finance.service;

import java.util.List;

import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.model.InvestmentTypeMaster;
import in.gov.cooptex.core.accounts.repository.InvestmentTypeRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class InvestmentTypeServices {

	@Autowired
	InvestmentTypeRepository investmentTypeRepository;

	@Autowired
	ResponseWrapper responseWrapper;

	public BaseDTO getById(Long id) {
		log.info("InvestmentTypeService  getById method started [" + id + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			// Validate.notNull(id, ErrorDescription.INVESTMENT_CLOSING_ID_EMPTY);
			InvestmentTypeMaster investmentType= investmentTypeRepository.getOne(id);
			baseDTO.setResponseContent(investmentType);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("InvestmentTypeService getById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("InvestmentTypeService getById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("InvestmentTypeService  getById method completed");
		return responseWrapper.send(baseDTO);
	}
	
	public BaseDTO getInvestmentType() {
		log.info("InvestmentTypeService  getInvestmentType method started ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<InvestmentTypeMaster> investmentTypeList= investmentTypeRepository.getInvestmentType();
			baseDTO.setResponseContent(investmentTypeList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("InvestmentTypeService getInvestmentType RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("InvestmentTypeService getInvestmentType Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("InvestmentTypeService  getInvestmentType method completed");
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO deleteById(Long id) {
		log.info("InvestmentTypeService deleteById method started [" + id + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			// Validate.notNull(id, ErrorDescription.INVESTMENT_TYPE_ID_EMPTY);
			investmentTypeRepository.delete(id);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("InvestmentTypeService deleteById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (DataIntegrityViolationException exception) {
			log.error("InvestmenTypeService deleteById DataIntegrityViolationException ", exception);
			if (exception.getCause().getCause() instanceof PSQLException) {
				baseDTO.setStatusCode(ErrorDescription.CANNOT_DELETE_REFERENCED_RECORD.getErrorCode());
			} else {
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			}
		} catch (Exception exception) {
			log.error("InvestmentTypeService deleteById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("InvestmentTypeService deleteById method completed");
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO createInvestmentType(InvestmentTypeMaster investmentType) {
		BaseDTO baseDTO = new BaseDTO();
		investmentTypeRepository.save(investmentType);
		return baseDTO;
	}

}