package in.gov.cooptex.finance.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import in.gov.cooptex.common.repository.CashMovementRepository;
import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.service.NotificationEmailService;
import in.gov.cooptex.core.accounts.model.BudgetAllocationDetails;
import in.gov.cooptex.core.accounts.model.ImprestRequest;
import in.gov.cooptex.core.accounts.model.ImprestRequestDetails;
import in.gov.cooptex.core.accounts.model.ImprestRequestLog;
import in.gov.cooptex.core.accounts.model.ImprestRequestNote;
import in.gov.cooptex.core.accounts.repository.GlAccountRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.finance.repository.BudgetAllocationDetailsRepository;
import in.gov.cooptex.core.finance.repository.BudgetAllocationRepository;
import in.gov.cooptex.core.finance.repository.ImprestRequestDetailsRepository;
import in.gov.cooptex.core.finance.repository.ImprestRequestLogRepository;
import in.gov.cooptex.core.finance.repository.ImprestRequestNoteRepository;
import in.gov.cooptex.core.finance.repository.ImprestRequestRepository;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.SystemNotification;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.AppQueryRepository;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.SystemNotificationRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.finance.dto.ImprestRequestDTO;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class ImprestRequestService {

	@Autowired
	BudgetAllocationRepository budgetAllocationRepository;

	@Autowired
	BudgetAllocationDetailsRepository budgetAllocationDetailsRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	GlAccountRepository glAccountRepository;

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	UserMasterRepository userMasterRepository;

	@Autowired
	ImprestRequestRepository imprestRequestRepository;

	@Autowired
	AppQueryRepository appQueryRepository;

	@Autowired
	ImprestRequestNoteRepository imprestRequestNoteRepository;

	@Autowired
	ImprestRequestLogRepository imprestRequestLogRepository;

	@Autowired
	ImprestRequestDetailsRepository imprestRequestDetailsRepository;
	
	@Autowired
	CashMovementRepository cashMovementRepository;
	
	@Autowired
	ApplicationQueryRepository applicationQueryRepository;
	
	@Autowired
	SystemNotificationRepository systemNotificationRepository;

	@Autowired
	NotificationEmailService notificationEmailService;
	
	@Autowired
	EmployeeMasterRepository employeeMasterRepository;
	
	@Autowired
	LoginService loginService;
	
	private static final String IMPREST_REQUEST_VIEW_PAGE = "/pages/accounts/imprest/viewImprestRequest.xhtml?faces-redirect=true";

	
	public BaseDTO getByBudgetConfig(Long id) {
		log.info("ImprestRequestService getByBudgetConfig Starts=====>" + id);
		BaseDTO baseDTO = new BaseDTO();
		List<BudgetAllocationDetails> allocationDetailsList = null;
		try {
			if (id != null) {
				allocationDetailsList = new ArrayList<>();
				allocationDetailsList = budgetAllocationDetailsRepository.getByBudgetConfig(id);
				baseDTO.setResponseContents(allocationDetailsList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (Exception e) {
			log.error("ImprestRequestService getByBudgetConfig Exception", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	/**
	 * This method is use to get the budget allocation amount and expense amount
	 * based on glAccountId and EntityId and budget period and current date
	 */
	public BaseDTO generateImprestRequest(ImprestRequestDTO imprestRequestDTO) {
		log.info("generateImprestRequest service method starts-----");
		String query = "";
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		BaseDTO baseDTO = null;
		String startDate = "", endDate = "";
		Date fromDate, toDate;
		try {
			imprestRequestDTO.setFixedExpenseList(new ArrayList<>());
			imprestRequestDTO.setVariableExpenseList(new ArrayList<>());
			imprestRequestDTO.setBankBalanceList(new ArrayList<>());
			List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
			if (imprestRequestDTO != null) {
				startDate = imprestRequestDTO.getFromMonth().split("-")[1] + "-"
						+ imprestRequestDTO.getFromMonth().split("-")[0] + "-" + "01";
				endDate = imprestRequestDTO.getToMonth().split("-")[1] + "-"
						+ imprestRequestDTO.getToMonth().split("-")[0] + "-" + "01";
				baseDTO = new BaseDTO();
				fromDate = format.parse(startDate);
				toDate = format.parse(endDate);

				ImprestRequest imprestRequest = imprestRequestRepository.checkByDateAndEntity(fromDate, toDate , imprestRequestDTO.getEntityMaster().getId());
				log.info("ImprestRequest obj ---------" + imprestRequest);
				if (imprestRequest == null) {
					ApplicationQuery applicationQuery = appQueryRepository
							.findByQueryName("IMPREST_REQUEST_GENERATE_QUERY");

					query = applicationQuery.getQueryContent().trim();
					query = query.replace(":fromMonth", imprestRequestDTO.getFromMonth().split("-")[0]);
					query = query.replace(":fromYear", imprestRequestDTO.getFromMonth().split("-")[1]);
					query = query.replace(":toMonth", imprestRequestDTO.getToMonth().split("-")[0]);
					query = query.replace(":toYear", imprestRequestDTO.getToMonth().split("-")[1]);
					query = query.replace(":entityId", imprestRequestDTO.getEntityMaster().getId().toString());
					query = query.replace(":startDate", format.parse(startDate).toString());

					log.info("replace after query----------" + query);

					dataList = jdbcTemplate.queryForList(query);
					log.info("BudgetAllocation list size-----------" + dataList.size());

					if (dataList.size() > 0) {
						for (Map<String, Object> data : dataList) {
							ImprestRequestDTO requestDTO = new ImprestRequestDTO();
							requestDTO.setGlAccountId(Long.valueOf(data.get("id").toString()));
							requestDTO.setAccountHead(data.get("name").toString());
							requestDTO.setBudgetEstimated(
									Double.valueOf(data.get("allocation_amount").toString()));
							requestDTO.setActualExpense(Double.valueOf(data.get("expense_amount").toString()));
							requestDTO.setBalanceExpense(
									requestDTO.getBudgetEstimated() - requestDTO.getActualExpense());
							requestDTO.setImprestAmount(0.0);
							if (data.get("expense_category") != null) {
								if ("FIXED".equals(data.get("expense_category"))) {
									imprestRequestDTO.getFixedExpenseList().add(requestDTO);
								} else if ("VARIABLE".equals(data.get("expense_category"))) {
									imprestRequestDTO.getVariableExpenseList().add(requestDTO);
								}
							}
						}
					}
					log.info("fixed expense list======>" + imprestRequestDTO.getFixedExpenseList().size());
					log.info("variable expense list======>" + imprestRequestDTO.getVariableExpenseList().size());
					imprestRequestDTO.setTotalFixedBudget(imprestRequestDTO.getFixedExpenseList().stream()
							.mapToDouble(ImprestRequestDTO::getBudgetEstimated).sum());
					imprestRequestDTO.setTotalFixedExpense(imprestRequestDTO.getFixedExpenseList().stream()
							.mapToDouble(ImprestRequestDTO::getActualExpense).sum());
					imprestRequestDTO.setTotalFixedBalance(imprestRequestDTO.getFixedExpenseList().stream()
							.mapToDouble(ImprestRequestDTO::getBalanceExpense).sum());
					imprestRequestDTO.setTotalVariableBudget(imprestRequestDTO.getVariableExpenseList().stream()
							.mapToDouble(ImprestRequestDTO::getBudgetEstimated).sum());
					imprestRequestDTO.setTotalVariableExpense(imprestRequestDTO.getVariableExpenseList().stream()
							.mapToDouble(ImprestRequestDTO::getActualExpense).sum());
					imprestRequestDTO.setTotalVariableBalance(imprestRequestDTO.getVariableExpenseList().stream()
							.mapToDouble(ImprestRequestDTO::getBalanceExpense).sum());
					
					//Get Cash Balance
					Double cashBalance = cashMovementRepository.
							findByEntityIdAndDate(imprestRequestDTO.getEntityMaster().getId());
					log.info("imprest request cash balance ---------------->"+cashBalance);
					imprestRequestDTO.setCashBalance(cashBalance !=null ? cashBalance:0.0);
					
					imprestRequestDTO.setBankBalanceList(getBankBalance(imprestRequestDTO.getEntityMaster().getId()));

					baseDTO.setResponseContent(imprestRequestDTO);
					baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				} else {
					baseDTO.setStatusCode(ErrorDescription.IMPREST_REQUEST_EXISTS.getErrorCode());
				}
			}
		} catch (Exception e) {
			log.error("generateImprestRequest method exception-----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	private List<ImprestRequestDTO> getBankBalance(Long entityId) {
		List<ImprestRequestDTO> entityBankList = new ArrayList<>();
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		try {
			ApplicationQuery entityApplicationQuery = appQueryRepository
					.findByQueryName("ENTITY_BANK_BALNCE");
			if(entityApplicationQuery ==null) {
				log.info("======ENTITY_BANK_BALNCE query not found=====");
				return null;
			}
			
			
			String entityQuery = entityApplicationQuery.getQueryContent();
			entityQuery = entityQuery.replaceAll(":entityID", entityId.toString());
			log.info("entity query----------->"+entityQuery);
			dataList = jdbcTemplate.queryForList(entityQuery);
			
			if (dataList.size() > 0) {
				for (Map<String, Object> data : dataList) {
					ImprestRequestDTO imprestRequestDTO = new ImprestRequestDTO();
					imprestRequestDTO.setEntityBranchId(Long.valueOf(data.get("id").toString()));
					if(data.get("closing_balance")!=null) {
						imprestRequestDTO.setClosingBalance(Double.valueOf(data.get("closing_balance").toString()));
					}else {
						imprestRequestDTO.setClosingBalance(0.0);
					}
					imprestRequestDTO.setBranchName(data.get("branch_name").toString());
					imprestRequestDTO.setBankName(data.get("bank_name").toString());
					imprestRequestDTO.setBankBalance(Double.valueOf(data.get("amount").toString()));
					entityBankList.add(imprestRequestDTO);
				}
			}
			
			log.info("entityBankList size---------------->"+entityBankList.size());
			
		}catch (Exception e) {
			log.error("getBankBalance() exception------- ",e);
		}
		return entityBankList;
	}

	@Transactional
	public BaseDTO saveImprestRequest(ImprestRequestDTO imprestRequestDTO) {
		log.info("saveImprestRequest methods Starts--------------");
		BaseDTO baseDTO = new BaseDTO();
		String fromDate = "", toDate = "";
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		ImprestRequest imprestRequest = null;
		try {
			if (imprestRequestDTO != null) {
				if (imprestRequestDTO.getId() == null) {
					fromDate = imprestRequestDTO.getFromMonth().split("-")[1] + "-"
							+ imprestRequestDTO.getFromMonth().split("-")[0] + "-" + "01";
					toDate = imprestRequestDTO.getToMonth().split("-")[1] + "-"
							+ imprestRequestDTO.getToMonth().split("-")[0] + "-" + "01";
					imprestRequest = new ImprestRequest();
					imprestRequest.setEntityMaster(
							entityMasterRepository.findOne(imprestRequestDTO.getEntityMaster().getId()));
					log.info("ImprestRequest entity id------------" + imprestRequest.getEntityMaster().getId());
					imprestRequest.setFromDate(format.parse(fromDate));
					imprestRequest.setToDate(format.parse(toDate));

					if (imprestRequestDTO.getFixedExpenseList().size() > 0) {
						for (ImprestRequestDTO requestDTO : imprestRequestDTO.getFixedExpenseList()) {
							ImprestRequestDetails imprestRequestDetails = new ImprestRequestDetails();
							imprestRequestDetails.setImprestRequest(imprestRequest);
							imprestRequestDetails
									.setGlAccount(glAccountRepository.findById(requestDTO.getGlAccountId()));
							imprestRequestDetails.setImprestAmount(requestDTO.getImprestAmount());
							if(requestDTO.getImprestDetailsRemarks() != null) {
								imprestRequestDetails.setRemark(requestDTO.getImprestDetailsRemarks());
							}
							imprestRequest.getImprestRequestDetailsList().add(imprestRequestDetails);
						}
						log.info("ImprestRequest Details inserted-------");
					}
				} else {
					imprestRequest = imprestRequestRepository.findOne(imprestRequestDTO.getId());
					imprestRequest.setEntityMaster(
							entityMasterRepository.findOne(imprestRequestDTO.getEntityMaster().getId()));

					if(imprestRequestDTO.getFixedExpenseList() != null) {
						if (imprestRequestDTO.getFixedExpenseList().size() > 0) {
							for (ImprestRequestDTO requestDTO : imprestRequestDTO.getFixedExpenseList()) {
								ImprestRequestDetails imprestRequestDetails = imprestRequestDetailsRepository
										.findOne(requestDTO.getDetailsId());
								imprestRequestDetails.setImprestRequest(imprestRequest);
								imprestRequestDetails
										.setGlAccount(glAccountRepository.findById(requestDTO.getGlAccountId()));
								imprestRequestDetails.setImprestAmount(requestDTO.getImprestAmount());
								if(requestDTO.getImprestDetailsRemarks() != null) {
									imprestRequestDetails.setRemark(requestDTO.getImprestDetailsRemarks());
								}
								imprestRequest.getImprestRequestDetailsList().add(imprestRequestDetails);
							}
							log.info("ImprestRequest Details Updated-------");
						}
					}
				}

				ImprestRequestNote imprestRequestNote = new ImprestRequestNote();
				imprestRequestNote.setImprestRequest(imprestRequest);
				imprestRequestNote.setFinalApproval(imprestRequestDTO.getFinalApproval());
				imprestRequestNote
						.setForwardTo(userMasterRepository.findOne(imprestRequestDTO.getUserMaster().getId()));
				log.info("ImprestRequestNote user id-----------" + imprestRequestNote.getForwardTo().getId());
				imprestRequestNote.setNote(imprestRequestDTO.getNote());
				imprestRequest.getImprestRequestNoteList().add(imprestRequestNote);
				log.info("ImprestRequestNote Inserted------");

				ImprestRequestLog imprestRequestLog = new ImprestRequestLog();
				imprestRequestLog.setImprestRequest(imprestRequest);
				imprestRequestLog.setStage("SUBMITTED");
				imprestRequest.getImprestRequestLogList().add(imprestRequestLog);
				log.info("ImprestRequestLog inserted--------");

				imprestRequestRepository.save(imprestRequest);
				/*
				 * Send Notification Mail
				 */

				if (imprestRequest != null && imprestRequest.getId() != null) {
					Map<String, Object> additionalData = new HashMap<>();
					Long empId = employeeMasterRepository.getEmployeeIdByUserId(imprestRequest.getCreatedBy().getId());
					additionalData.put("Url",
							IMPREST_REQUEST_VIEW_PAGE + "&imprestRequestId=" + imprestRequest.getId() + "&empId=" + empId + "&");
					notificationEmailService.sendMailAndNotificationForForward(imprestRequestNote, imprestRequestLog, additionalData);
				}

				
				log.info("ImprestRequest saved Successfully-----------");

				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (Exception e) {
			log.error("saveImprestRequest method exception------", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO lazyLoadImprestRequest(PaginationDTO paginationDTO) {
		log.info("lazyLoadImprestRequest method starts--------");
		BaseDTO baseDTO = null;
		List<ImprestRequestDTO> imprestRequestDTOList = null;
		Integer totalRecords = 0;
		try {
			baseDTO = new BaseDTO();
			imprestRequestDTOList = new ArrayList<>();
			Integer start = paginationDTO.getFirst(), pageSize = paginationDTO.getPageSize();
			start = start * pageSize;
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();
			if (paginationDTO != null) {
				
				UserMaster userMaster = loginService.getCurrentUser();
				Long loginUserId = userMaster == null ? null : userMaster.getId();

				ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("IMPREST_REQUEST_DETAILS");
				String mainQuery = applicationQuery.getQueryContent();
				mainQuery = StringUtils.replace(mainQuery, ":userId", String.valueOf(loginUserId));
				log.info("db query----------" + mainQuery);
				
				

				if (paginationDTO.getFilters() != null) {
					if (paginationDTO.getFilters().get("stage") != null) {
						mainQuery += " and stage='" + paginationDTO.getFilters().get("stage") + "'";
					}
					if (paginationDTO.getFilters().get("imprestAmount") != null) {
						mainQuery += " and ird.imprest_amount= '" + paginationDTO.getFilters().get("imprestAmount")
								+ "'";
					}
					if (paginationDTO.getFilters().get("entityMaster.name") != null) {
						mainQuery += " and ( em.code=" + paginationDTO.getFilters().get("entityMaster.name") + " or "
								+ "upper(em.name) like upper('%" + paginationDTO.getFilters().get("entityMaster.name")
								+ "%') )";
					}
					if (paginationDTO.getFilters().get("fromDate") != null) {
						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
						Date fromDate = new Date((long) paginationDTO.getFilters().get("fromDate"));
						log.info(" fromDate Filter : " + fromDate);
						mainQuery += " and ir.from_date::date='" + format.format(fromDate) + "'";
					}
					if (paginationDTO.getFilters().get("toDate") != null) {
						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
						Date fromDate = new Date((long) paginationDTO.getFilters().get("toDate"));
						log.info(" fromDate Filter : " + fromDate);
						mainQuery += " and ir.to_date::date='" + format.format(fromDate) + "'";
					}
					if (paginationDTO.getFilters().get("createdDate") != null) {
						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
						Date fromDate = new Date((long) paginationDTO.getFilters().get("createdDate"));
						log.info(" fromDate Filter : " + fromDate);
						mainQuery += " and ir.created_date::date='" + format.format(fromDate) + "'";
					}
				}

				ApplicationQuery appQuery = appQueryRepository.findByQueryName("IMPREST_REQUSET_COUNT_QUERY");
				String countQuery = appQuery.getQueryContent();
				log.info("count query... " + countQuery);
				countData = jdbcTemplate.queryForList(countQuery);

				for (Map<String, Object> data : countData) {
					if (data.get("count") != null)
						totalRecords = Integer.parseInt(data.get("count").toString().replace(",", ""));
				}
				if (paginationDTO.getSortField() != null && paginationDTO.getSortOrder() != null) {
					log.info("Sort Field:[" + paginationDTO.getSortField() + "] Sort Order:["
							+ paginationDTO.getSortOrder() + "]");
					if (paginationDTO.getSortField().equals("entityMaster.name")
							&& paginationDTO.getSortOrder().equals("ASCENDING"))
						mainQuery += " order by concat(em.code,' / ',em.name) asc  ";
					if (paginationDTO.getSortField().equals("entityMaster.name")
							&& paginationDTO.getSortOrder().equals("DESCENDING"))
						mainQuery += " order by concat(em.code,' / ',em.name) desc  ";
					if (paginationDTO.getSortField().equals("fromDate")
							&& paginationDTO.getSortOrder().equals("ASCENDING"))
						mainQuery += " order by ir.from_date asc ";
					if (paginationDTO.getSortField().equals("fromDate")
							&& paginationDTO.getSortOrder().equals("DESCENDING"))
						mainQuery += " order by ir.from_date desc ";
					if (paginationDTO.getSortField().equals("toDate")
							&& paginationDTO.getSortOrder().equals("ASCENDING"))
						mainQuery += " order by ir.to_date asc ";
					if (paginationDTO.getSortField().equals("toDate")
							&& paginationDTO.getSortOrder().equals("DESCENDING"))
						mainQuery += " order by ir.to_date desc ";
					if (paginationDTO.getSortField().equals("createdDate")
							&& paginationDTO.getSortOrder().equals("ASCENDING"))
						mainQuery += " order by ir.created_date asc ";
					if (paginationDTO.getSortField().equals("createdDate")
							&& paginationDTO.getSortOrder().equals("DESCENDING"))
						mainQuery += " order by ir.created_date desc ";
					if (paginationDTO.getSortField().equals("stage")
							&& paginationDTO.getSortOrder().equals("ASCENDING"))
						mainQuery += " order by stage asc ";
					if (paginationDTO.getSortField().equals("stage")
							&& paginationDTO.getSortOrder().equals("DESCENDING"))
						mainQuery += " order by stage desc ";
					mainQuery += " limit " + pageSize + " offset " + start + ";";
				}

				if (paginationDTO.getSortField() == null) {
					mainQuery += " order by ir.id desc limit " + pageSize + " offset " + start + ";";
				}
				log.info("filter query----------" + mainQuery);
				

				listofData = jdbcTemplate.queryForList(mainQuery);
				log.info("ImprestRequest list size-------------->" + listofData.size());

				for (Map<String, Object> data : listofData) {
					ImprestRequestDTO imprestRequestDTO = new ImprestRequestDTO();
					imprestRequestDTO.setId(Long.valueOf(data.get("id").toString()));
					imprestRequestDTO.setEntityMaster(new EntityMaster());
					imprestRequestDTO.getEntityMaster().setName(data.get("entityname").toString());
					imprestRequestDTO.setImprestAmount(Double.valueOf(data.get("imprestamount").toString()));
					imprestRequestDTO.setFromDate((Date) data.get("fromdate"));
					imprestRequestDTO.setToDate((Date) data.get("todate"));
					imprestRequestDTO.setCreatedDate((Date) data.get("createddate"));
					imprestRequestDTO.setStage(data.get("stage").toString());
					imprestRequestDTOList.add(imprestRequestDTO);
				}
				log.info("imprestRequestDTOList size--------" + imprestRequestDTOList.size());
				baseDTO.setTotalRecords(totalRecords);
				baseDTO.setResponseContents(imprestRequestDTOList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (Exception e) {
			log.error("lazyLoadImprestRequest method exception-------", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO viewImprestRequest(ImprestRequestDTO imprestRequestDTOObj) {
		Long imprestRequestId = imprestRequestDTOObj.getId();
		Long notificationId = imprestRequestDTOObj.getNotificationId();
		log.info("viewImprestRequest methods Starts--------------" + imprestRequestId);
		String query = "";
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		BaseDTO baseDTO = null;
		Calendar calendar = Calendar.getInstance();
		String fromMonth = "", toMonth = "", fromYear = "", toYear = "";
		try {
			ImprestRequestDTO imprestRequestDTO = new ImprestRequestDTO();
			imprestRequestDTO.setFixedExpenseList(new ArrayList<>());
			imprestRequestDTO.setVariableExpenseList(new ArrayList<>());

			ImprestRequest imprestRequest = imprestRequestRepository.getOne(imprestRequestId);
			log.info("imprestRequset obj----" + imprestRequest);
			if (imprestRequest != null) {
				calendar.setTime(imprestRequest.getFromDate());
				fromMonth = String.valueOf(calendar.get(Calendar.MONTH) + 1);
				fromYear = String.valueOf(calendar.get(Calendar.YEAR));
				log.info("from month--------------" + fromMonth);
				calendar.setTime(imprestRequest.getToDate());
				toMonth = String.valueOf(calendar.get(Calendar.MONTH) + 1);
				toYear = String.valueOf(calendar.get(Calendar.YEAR));

				List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
				if (imprestRequestDTO != null) {
					baseDTO = new BaseDTO();
					ApplicationQuery applicationQuery = appQueryRepository
							.findByQueryName("IMPREST_REQUEST_VIEW_QUERY");

					query = applicationQuery.getQueryContent().trim();
					query = query.replace(":fromMonth", fromMonth);
					query = query.replace(":fromYear", fromYear);
					query = query.replace(":toMonth", toMonth);
					query = query.replace(":toYear", toYear);
					query = query.replace(":entityId", imprestRequest.getEntityMaster().getId().toString());
					query = query.replace(":startDate", format.format(imprestRequest.getFromDate()).toString());
					query = query.replace(":endDate", format.format(imprestRequest.getCreatedDate()).toString());
					query = query.replace(":imprestrequestid ", imprestRequest.getId().toString());

					log.info("replace after query----------" + query);

					dataList = jdbcTemplate.queryForList(query);
					log.info("BudgetAllocation list size-----------" + dataList.size());

					if (dataList.size() > 0) {
						for (Map<String, Object> data : dataList) {
							ImprestRequestDTO requestDTO = new ImprestRequestDTO();
							requestDTO.setDetailsId(data.get("detail_id") != null ? Long.valueOf(data.get("detail_id").toString()) : null);
							requestDTO.setGlAccountId(data.get("id") != null ? Long.valueOf(data.get("id").toString()) : null);
							requestDTO.setAccountHead(data.get("name").toString());
							requestDTO.setBudgetEstimated(
									Double.valueOf(data.get("allocation_amount").toString()));
							requestDTO.setActualExpense(Double.valueOf(data.get("expense_amount").toString()));
							requestDTO.setBalanceExpense(
									requestDTO.getBudgetEstimated() - requestDTO.getActualExpense());
							if(data.get("imprest_amount") != null) {
								requestDTO.setImprestAmount(Double.valueOf(data.get("imprest_amount").toString()));
							}
							if(data.get("remarks") != null) {
								requestDTO.setImprestDetailsRemarks((data.get("remarks").toString()));
							}
							if (data.get("expense_category") != null) {
								if ("FIXED".equals(data.get("expense_category"))) {
									imprestRequestDTO.getFixedExpenseList().add(requestDTO);
								} else if ("VARIABLE".equals(data.get("expense_category"))) {
									imprestRequestDTO.getVariableExpenseList().add(requestDTO);
								}
							}
						}
					}
					
					if(imprestRequestDTO.getFixedExpenseList() != null) {
						log.info("fixed expense list======>" + imprestRequestDTO.getFixedExpenseList().size());
						imprestRequestDTO.setTotalFixedBudget(imprestRequestDTO.getFixedExpenseList().stream()
								.mapToDouble(ImprestRequestDTO::getBudgetEstimated).sum());
						imprestRequestDTO.setTotalFixedExpense(imprestRequestDTO.getFixedExpenseList().stream()
								.mapToDouble(ImprestRequestDTO::getActualExpense).sum());
						imprestRequestDTO.setTotalFixedBalance(imprestRequestDTO.getFixedExpenseList().stream()
								.mapToDouble(ImprestRequestDTO::getBalanceExpense).sum());
						imprestRequestDTO.setTotalFixedImprestAmount(imprestRequestDTO.getFixedExpenseList().stream()
								.mapToDouble(ImprestRequestDTO::getImprestAmount).sum());
					}
					if(imprestRequestDTO.getVariableExpenseList() != null) {
						log.info("variable expense list======>" + imprestRequestDTO.getVariableExpenseList().size());
						imprestRequestDTO.setTotalVariableBudget(imprestRequestDTO.getVariableExpenseList().stream()
								.mapToDouble(ImprestRequestDTO::getBudgetEstimated).sum());
						imprestRequestDTO.setTotalVariableExpense(imprestRequestDTO.getVariableExpenseList().stream()
								.mapToDouble(ImprestRequestDTO::getActualExpense).sum());
						imprestRequestDTO.setTotalVariableBalance(imprestRequestDTO.getVariableExpenseList().stream()
								.mapToDouble(ImprestRequestDTO::getBalanceExpense).sum());
						imprestRequestDTO.setTotalVariableImprestAmount(imprestRequestDTO.getVariableExpenseList().stream()
								.mapToDouble(ImprestRequestDTO::getImprestAmount).sum());
					}
					imprestRequestDTO.setFromDate(imprestRequest.getFromDate());
					imprestRequestDTO.setToDate(imprestRequest.getToDate());
					imprestRequestDTO.setId(imprestRequest.getId());
					imprestRequestDTO.setCreatedUser(imprestRequest.getCreatedBy());

					ImprestRequestNote imprestRequestNote = imprestRequestNoteRepository
							.findByImprestId(imprestRequest.getId());
					ImprestRequestLog imprestRequestLog = imprestRequestLogRepository
							.findByImprestId(imprestRequest.getId());
					log.info("imprestRequestNote obj value-----" + imprestRequestNote);
					log.info("imprestRequestLog obj value-----" + imprestRequestLog);

					if(imprestRequestNote != null) {
						imprestRequestDTO.setUserMaster(imprestRequestNote.getForwardTo());
						imprestRequestDTO.setFinalApproval(imprestRequestNote.getFinalApproval());
						imprestRequestDTO.setNote(imprestRequestNote.getNote());
					}
					if(imprestRequestLog != null) {
						imprestRequestDTO.setStage(imprestRequestLog.getStage());
					}
					
					
					List<Map<String, Object>> employeeData = new ArrayList<Map<String, Object>>();
					if(imprestRequest!=null) {
						log.info("<<<:::::::amountTransfer::::not Null::::>>>>"+imprestRequest.getId());
						 ApplicationQuery applicationQueryForlog = applicationQueryRepository.findByQueryName("IMPREST_LOG_EMPLOYEE_DETAILS");
						 if (applicationQueryForlog == null || applicationQueryForlog.getId() == null) {
							 log.info("Application Query For Log Details not found for query name : " + applicationQueryForlog);
							 baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
							 return baseDTO;
						 }
						 String logquery = applicationQueryForlog.getQueryContent().trim();
						 log.info("<=========IMPREST_LOG_EMPLOYEE_DETAILS Query Content ======>"+logquery);
						 logquery = logquery.replace(":imprestId", "'"+imprestRequest.getId().toString()+"'");
						 log.info("Query Content For IMPREST_LOG_EMPLOYEE_DETAILS After replaced Imprest id View query : " + logquery);
						 employeeData = jdbcTemplate.queryForList(logquery);
						 log.info("<=========IMPREST_LOG_EMPLOYEE_DETAILS Employee Data======>"+employeeData);
						 baseDTO.setTotalListOfData(employeeData);
					}
					
					if (notificationId != null && notificationId >0) {
						SystemNotification systemNotification = systemNotificationRepository.findOne(notificationId);
						systemNotification.setNotificationRead(true);
						systemNotificationRepository.save(systemNotification);
					}
					baseDTO.setResponseContent(imprestRequestDTO);
					baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				}
			}
		} catch (Exception e) {
			log.error("viewImprestRequest method exception-----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO approveImprestRequest(ImprestRequestDTO imprestRequestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("approveImprestRequest method starts-------" + imprestRequestDTO.getId());
			if (imprestRequestDTO.getId() != null) {
				ImprestRequest imprestRequest = imprestRequestRepository.getOne(imprestRequestDTO.getId());
				log.info("imprestRequset obj----" + imprestRequest);

				ImprestRequestNote imprestRequestNote = new ImprestRequestNote();
				imprestRequestNote.setImprestRequest(imprestRequest);
				imprestRequestNote.setFinalApproval(imprestRequestDTO.getFinalApproval());
				imprestRequestNote
						.setForwardTo(userMasterRepository.findOne(imprestRequestDTO.getUserMaster().getId()));
				log.info("ImprestRequestNote user id-----------" + imprestRequestNote.getForwardTo().getId());
				imprestRequestNote.setNote(imprestRequestDTO.getNote());
				imprestRequestNoteRepository.save(imprestRequestNote);
				log.info("ImprestRequestNote Inserted------");

				ImprestRequestLog imprestRequestLog = new ImprestRequestLog();
				imprestRequestLog.setImprestRequest(imprestRequest);
				imprestRequestLog.setStage(imprestRequestDTO.getStage());
				imprestRequestLog.setRemarks(imprestRequestDTO.getRemarks());
				imprestRequestLogRepository.save(imprestRequestLog);
				log.info("ImprestRequestLog inserted--------");
				
				if(ApprovalStage.FINALAPPROVED.equals(imprestRequestDTO.getStage())) {
					String urlPath = IMPREST_REQUEST_VIEW_PAGE + "&imprestRequestId=" + imprestRequestDTO.getId() + "&";
					Long toUserId = imprestRequest.getCreatedBy().getId();
					notificationEmailService.saveIndividualNotification(loginService.getCurrentUser(), toUserId, urlPath,
							"Imprest Request", " Imprest request  has been final approved");
				} else {
					Map<String, Object> additionalData = new HashMap<>();
					additionalData.put("Url", IMPREST_REQUEST_VIEW_PAGE + "&imprestRequestId=" + imprestRequestDTO.getId() + "&");
					notificationEmailService.sendMailAndNotificationForForward(imprestRequestNote, imprestRequestLog,
							additionalData);
				}
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (Exception e) {
			log.error("approveImprestRequest service method exception-----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO rejectImprestRequest(ImprestRequestDTO imprestRequestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("rejectImprestRequest method starts-------" + imprestRequestDTO.getId());
			if (imprestRequestDTO.getId() != null) {
				ImprestRequest imprestRequest = imprestRequestRepository.getOne(imprestRequestDTO.getId());
				log.info("imprestRequset obj----" + imprestRequest);

				ImprestRequestLog imprestRequestLog = new ImprestRequestLog();
				imprestRequestLog.setImprestRequest(imprestRequest);
				imprestRequestLog.setStage(imprestRequestDTO.getStage());
				imprestRequestLog.setRemarks(imprestRequestDTO.getRemarks());
				imprestRequestLogRepository.save(imprestRequestLog);
				log.info("ImprestRequestLog inserted--------");
				
				String urlPath = IMPREST_REQUEST_VIEW_PAGE + "&imprestRequestId=" + imprestRequest.getId() + "&";
				Long toUserId = imprestRequestNoteRepository.getCreatedByUserId(imprestRequest.getId());
				notificationEmailService.saveIndividualNotification(loginService.getCurrentUser(), toUserId, urlPath,
						"Imprest Request", "Imprest Request has been rejected");
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (Exception e) {
			log.error("rejectImprestRequest service method exception-----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

}
