package in.gov.cooptex.finance.service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import in.gov.cooptex.core.accounts.model.SocietyInvoiceAdjustment;
import in.gov.cooptex.core.accounts.model.UnitInspectionEntity;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.CircleMaster;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.SectionMasterRepository;
import in.gov.cooptex.core.repository.SocietyAdjustmentMasterRepository;
import in.gov.cooptex.core.repository.SupplierMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.finance.SoceityInvoiceAdjustmentDTO;
import in.gov.cooptex.finance.model.SocietyAdjustmentMaster;
import in.gov.cooptex.finance.repository.SocietyInvoiceAdjustmentRepository;
import in.gov.cooptex.operation.model.SupplierMaster;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class SoceityInvoiceAdjustmentService {

	@Autowired
	SocietyAdjustmentMasterRepository societyAdjustmentMasterRepository;

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	SectionMasterRepository sectionMasterRepository;

	@Autowired
	SocietyInvoiceAdjustmentRepository societyInvoiceAdjustmentRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	public BaseDTO saveSoceityInvoiceAdjustmentMaster(
			List<SoceityInvoiceAdjustmentDTO> soceityInvoiceAdjustmentDTOList) {
		log.info("<---------Starts SoceityInvoiceAdjustmentService.saveSoceityAdjustmentMaster ---------->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			if (soceityInvoiceAdjustmentDTOList == null || soceityInvoiceAdjustmentDTOList.isEmpty()) {
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
				return baseDTO;
			}
			/*
			 * Double amount;EntityMaster entityMaster;SectionMaster
			 * sectionMaster;CircleMaster circleMaster;soInteger adjustmentMonth;Integer
			 * adjustmentYear; SocietyAdjustmentMaster societyAdjustmentMaster;
			 */

			for (SoceityInvoiceAdjustmentDTO soceityInvoiceAdjustmentDTO : soceityInvoiceAdjustmentDTOList) {
				SocietyInvoiceAdjustment societyInvoiceAdjustment = new SocietyInvoiceAdjustment();
				societyInvoiceAdjustment.setSocietyId(soceityInvoiceAdjustmentDTO.getSupplierMaster().getId());
				societyInvoiceAdjustment.setSocietyAdjustment(societyAdjustmentMasterRepository
						.getOne(soceityInvoiceAdjustmentDTO.getSocietyAdjustmentMaster().getId()));
				societyInvoiceAdjustment.setAdjustmentAmount(soceityInvoiceAdjustmentDTO.getAmount());
				societyInvoiceAdjustment.setAdjustmentYear(soceityInvoiceAdjustmentDTO.getAdjustmentYear());
				societyInvoiceAdjustment.setAdjustmentMonth(soceityInvoiceAdjustmentDTO.getAdjustmentMonth());
				societyInvoiceAdjustment.setReferenceNumber(soceityInvoiceAdjustmentDTO.getReferenceNumber());
				societyInvoiceAdjustment.setReferenceDate(soceityInvoiceAdjustmentDTO.getReferenceDate());
				if (soceityInvoiceAdjustmentDTO.getEntityMaster() != null
						&& soceityInvoiceAdjustmentDTO.getEntityMaster().getId() != null) {
					societyInvoiceAdjustment.setEntityMaster(
							entityMasterRepository.getOne(soceityInvoiceAdjustmentDTO.getEntityMaster().getId()));
				}
				if (soceityInvoiceAdjustmentDTO.getSectionMaster() != null
						&& soceityInvoiceAdjustmentDTO.getSectionMaster().getId() != null) {
					societyInvoiceAdjustment.setSectionMaster(
							sectionMasterRepository.getOne(soceityInvoiceAdjustmentDTO.getSectionMaster().getId()));
				}

				societyInvoiceAdjustmentRepository.save(societyInvoiceAdjustment);
			}
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.info("<---------Starts SoceityInvoiceAdjustmentService.saveSoceityInvoiceAdjustmentMaster ---------->",
					e);
		}
		log.info("<---------Ends SoceityInvoiceAdjustmentService.saveSoceityInvoiceAdjustmentMaster ---------->");
		return baseDTO;

	}

	public BaseDTO getSocietyAdjustmentMasterByCodeOrName(String codeOrName) {
		log.info("====START EntityMasterService.getEntityMasterByNameAndCode====");
		BaseDTO baseDTO = new BaseDTO();
		List<SocietyAdjustmentMaster> societyAdjustmentMasterList = new ArrayList<>();
		try {
			codeOrName = "'%" + codeOrName.toLowerCase() + "%'";
			String query = "select sam.id as adjustmentId,sam.adjustment_code as adjustmentCode,sam.adjustment_name as adjustmentName from society_adjustment_master sam where   (cast(sam.adjustment_code as varchar)) like "
					+ codeOrName + " or lower(sam.adjustment_name) like " + codeOrName + "";
			List<Map<String, Object>> dataList = jdbcTemplate.queryForList(query);
			societyAdjustmentMasterList = getSocietyAdjustmentMasterListFromJdbcTemplateQuery(dataList);
			Integer count = societyAdjustmentMasterList != null && !societyAdjustmentMasterList.isEmpty()
					? societyAdjustmentMasterList.size()
					: 0;
			log.info("=== Count is ====    " + count);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			baseDTO.setResponseContents(societyAdjustmentMasterList);
		} catch (Exception e) {
			log.info("====Exception Occured in SoceityInvoiceAdjustmentService.getEntityMasterByNameAndCode====");
			log.info("====Exception is ====  " + e.getMessage());
		}
		log.info("====END SoceityInvoiceAdjustmentService.getEntityMasterByNameAndCode====");
		return baseDTO;
	}

	public BaseDTO getSectionMasterCodeOrName(String codeOrName) {
		log.info("====START SoceityInvoiceAdjustmentService.getEntityMasterByNameAndCode====");
		BaseDTO baseDTO = new BaseDTO();
		List<SectionMaster> sectionMasterList = new ArrayList<>();
		try {
			codeOrName = "'%" + codeOrName.toLowerCase() + "%'";
			String query = "select sm.id as sectionId,sm.code as sectionCode,sm.name as sectionName from section_master sm where   (cast(sm.code as varchar)) like "
					+ codeOrName + " or lower(sm.name) like " + codeOrName + "";
			List<Map<String, Object>> dataList = jdbcTemplate.queryForList(query);
			sectionMasterList = getSectionMasterListFromJdbcTemplateQuery(dataList);
			Integer count = sectionMasterList != null && !sectionMasterList.isEmpty() ? sectionMasterList.size() : 0;
			log.info("=== Count is ====    " + count);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			baseDTO.setResponseContents(sectionMasterList);
		} catch (Exception e) {
			log.info("====Exception Occured in SoceityInvoiceAdjustmentService.getEntityMasterByNameAndCode====");
			log.info("====Exception is ====  " + e.getMessage());
		}
		log.info("====END SoceityInvoiceAdjustmentService.getEntityMasterByNameAndCode====");
		return baseDTO;
	}

	private List<SectionMaster> getSectionMasterListFromJdbcTemplateQuery(List<Map<String, Object>> dataList) {
		log.info("====START SoceityInvoiceAdjustmentService.getEntityMasterListFromJdbcTemplateQuery====");
		List<SectionMaster> sectionMasterList = new ArrayList<>();
		try {
			log.info("==== dataList size is " + dataList != null && !dataList.isEmpty() ? dataList.size() : 0);
			if (!dataList.isEmpty()) {
				Iterator<Map<String, Object>> itr = dataList.iterator();
				while (itr.hasNext()) {
					SectionMaster sectionMaster = new SectionMaster();
					Map<String, Object> data = itr.next();
					if (data.get("sectionId") != null) {
						sectionMaster.setId(Long.parseLong(data.get("sectionId").toString()));
					}
					if (data.get("sectionCode") != null) {
						sectionMaster.setCode((data.get("sectionCode").toString()));
					}
					if (data.get("sectionName") != null) {
						sectionMaster.setName(data.get("sectionName").toString());
					}
					sectionMasterList.add(sectionMaster);
				}
				log.info("==== entityMasterList size is " + sectionMasterList != null && !sectionMasterList.isEmpty()
						? sectionMasterList.size()
						: 0);
				log.info("====END SoceityInvoiceAdjustmentService.getEntityMasterListFromJdbcTemplateQuery====");
			}
		} catch (Exception e) {
			log.info("====START SoceityInvoiceAdjustmentService.getEntityMasterListFromJdbcTemplateQuery====");
		}
		return sectionMasterList;
	}

	private List<SocietyAdjustmentMaster> getSocietyAdjustmentMasterListFromJdbcTemplateQuery(
			List<Map<String, Object>> dataList) {
		log.info("====START EntityMasterService.getEntityMasterListFromJdbcTemplateQuery====");
		List<SocietyAdjustmentMaster> societyAdjustmentMasterList = new ArrayList<>();
		try {
			log.info("==== dataList size is " + dataList != null && !dataList.isEmpty() ? dataList.size() : 0);
			if (!dataList.isEmpty()) {
				Iterator<Map<String, Object>> itr = dataList.iterator();
				while (itr.hasNext()) {
					SocietyAdjustmentMaster societyAdjustmentMaster = new SocietyAdjustmentMaster();
					Map<String, Object> data = itr.next();
					if (data.get("adjustmentId") != null) {
						societyAdjustmentMaster.setId(Long.parseLong(data.get("adjustmentId").toString()));
					}
					if (data.get("adjustmentCode") != null) {
						societyAdjustmentMaster.setAdjustmentCode((data.get("adjustmentCode").toString()));
					}
					if (data.get("adjustmentName") != null) {
						societyAdjustmentMaster.setAdjustmentName(data.get("adjustmentName").toString());
					}
					societyAdjustmentMasterList.add(societyAdjustmentMaster);
				}
				log.info("==== entityMasterList size is " + societyAdjustmentMasterList != null
						&& !societyAdjustmentMasterList.isEmpty() ? societyAdjustmentMasterList.size() : 0);
				log.info("====END EntityMasterService.getEntityMasterListFromJdbcTemplateQuery====");
			}
		} catch (Exception e) {
			log.info("====START SoceityInvoiceAdjustmentService.getEntityMasterListFromJdbcTemplateQuery====");
		}
		return societyAdjustmentMasterList;
	}

	@PersistenceContext
	EntityManager entityManager;

	@Autowired
	SupplierMasterRepository supplierMasterRepository;

	public BaseDTO getLazyLoadData(PaginationDTO paginationDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {

			log.info("========START SoceityInvoiceAdjustmentService.getLazyLoadData========");
			Session session = entityManager.unwrap(Session.class);
			Criteria criteria = session.createCriteria(SocietyInvoiceAdjustment.class, "societyInvoiceAdjustment");
			criteria.createAlias("societyInvoiceAdjustment.entityMaster", "entityMaster", JoinType.LEFT_OUTER_JOIN);
			criteria.createAlias("societyInvoiceAdjustment.sectionMaster", "sectionMaster", JoinType.LEFT_OUTER_JOIN);
			log.info(":: Criteria search started ::");
			if (paginationDTO.getFilters() != null) {
				String entityCodeName = (String) paginationDTO.getFilters().get("entityCodeName");

				if (entityCodeName != null && !entityCodeName.isEmpty()) {
					criteria.add(Restrictions
							.sqlRestriction("CAST(concat(entitymast1_.code,' / ',entitymast1_.name) AS TEXT) LIKE '%"
									+ entityCodeName.trim() + "%' "));
				}
				String secCodeName = (String) paginationDTO.getFilters().get("secCodeName");
				if (secCodeName != null) {
					criteria.add(Restrictions
							.sqlRestriction("CAST(concat(sectionmas2_.code,' / ',sectionmas2_.name) AS TEXT) LIKE '%"
									+ secCodeName.trim() + "%' "));
				}

				String socCodeName = (String) paginationDTO.getFilters().get("socCodeName");
				if (socCodeName != null) {
					criteria.add(
							Restrictions.sqlRestriction("this_.society_id in (select id from supplier_master sm where"
									+ " CAST(concat(sm.code,' / ',sm.name) AS TEXT) LIKE '%" + socCodeName.trim()
									+ "%' )"));
				}
				if (paginationDTO.getFilters().get("amount") != null) {
					String samount = (String) paginationDTO.getFilters().get("amount");
					/*
					 * Double amount =Double.valueOf(samount);
					 * criteria.add(Restrictions.eq("societyInvoiceAdjustment.adjustmentAmount",
					 * amount));
					 */
					criteria.add(Restrictions
							.sqlRestriction("CAST(this_.adjustment_amount AS TEXT) LIKE '%" + samount.trim() + "%' "));
				}

				if (paginationDTO.getFilters().get("monthyear") != null) {
					String monthyear = (String) paginationDTO.getFilters().get("monthyear");
					criteria.add(Restrictions.sqlRestriction(
							"CAST(concat(this_.adjustment_month,' - ',this_.adjustment_year) AS TEXT) LIKE '%"
									+ monthyear.trim() + "%' "));
				}

				if (paginationDTO.getFilters().get("referenceNumber") != null) {
					String referenceNumber = (String) paginationDTO.getFilters().get("referenceNumber");
					criteria.add(Restrictions.sqlRestriction(
							"CAST(this_.reference_number AS TEXT) LIKE '%" + referenceNumber.trim() + "%' "));
				}

				if (paginationDTO.getFilters().get("referenceDate") != null) {
					Date refDate = new Date((Long) paginationDTO.getFilters().get("referenceDate"));
					criteria.add(Restrictions
							.sqlRestriction("CAST(this_.reference_date AS TEXT)::date = date'" + refDate + "'"));
				}

				criteria.setProjection(Projections.rowCount());
				Integer totalResult = ((Long) criteria.uniqueResult()).intValue();
				criteria.setProjection(null);

				ProjectionList projectionList = Projections.projectionList();
				projectionList.add(Projections.property("id"));
				projectionList.add(Projections.property("societyId"));
				projectionList.add(Projections.property("adjustmentAmount"));
				projectionList.add(Projections.property("adjustmentMonth"));
				projectionList.add(Projections.property("adjustmentYear"));

				projectionList.add(Projections.property("entityMaster.code"));
				projectionList.add(Projections.property("entityMaster.name"));
				projectionList.add(Projections.property("sectionMaster.code"));
				projectionList.add(Projections.property("sectionMaster.name"));
				projectionList.add(Projections.property("societyInvoiceAdjustment.referenceNumber"));
				projectionList.add(Projections.property("societyInvoiceAdjustment.referenceDate"));

				criteria.setProjection(projectionList);

				if (paginationDTO.getFirst() != null) {
					Integer pageNo = paginationDTO.getFirst();
					Integer pageSize = paginationDTO.getPageSize();
					if (pageNo != null && pageSize != null) {
						criteria.setFirstResult(pageNo * pageSize);
						criteria.setMaxResults(pageSize);
						log.info("PageNo : [" + pageNo + "] pageSize[" + pageSize + "]");
					}
					// {secCodeName=dawe, amount=50, socCodeName=eqwed, monthyear=2,
					// entityCodeName=123}
					String sortField = paginationDTO.getSortField();
					String sortOrder = paginationDTO.getSortOrder();
					log.info("sortField outside : [" + sortField + "] sortOrder[" + sortOrder + "]");
					if (paginationDTO.getSortField() != null && paginationDTO.getSortOrder() != null) {
						log.info("sortField : [" + paginationDTO.getSortField() + "] sortOrder["
								+ paginationDTO.getSortOrder() + "]");

						if (paginationDTO.getSortField().equals("secCodeName")) {
							sortField = "societyInvoiceAdjustment.sectionMaster";
						} else if (sortField.equals("amount")) {
							sortField = "societyInvoiceAdjustment.adjustmentAmount";
						} else if (sortField.equals("socCodeName")) {
							sortField = "societyInvoiceAdjustment.societyId";
						} else if (sortField.equals("monthyear")) {
							sortField = "societyInvoiceAdjustment.adjustmentYear";
						} else if (sortField.equals("entityCodeName")) {
							sortField = "societyInvoiceAdjustment.entityMaster";
						} else if (sortField.equals("referenceNumber")) {
							sortField = "societyInvoiceAdjustment.referenceNumber";
						} else if (sortField.equals("referenceDate")) {
							sortField = "societyInvoiceAdjustment.referenceDate";
						}

						if (sortOrder.equals("DESCENDING")) {
							criteria.addOrder(Order.desc(sortField));
						} else {
							criteria.addOrder(Order.asc(sortField));
						}

					} else {
						criteria.addOrder(Order.desc("societyInvoiceAdjustment.modifiedDate"));
					}
				}
				List<?> resultList = criteria.list();
				log.info("criteria list executed and the list size is : " + resultList.size());
				if (resultList == null || resultList.isEmpty() || resultList.size() == 0) {
					log.info("societyInvoiceAdjustment List is null or empty ");
				}
				List<SoceityInvoiceAdjustmentDTO> societyInvoiceAdjustmentDtoList = new ArrayList<>();
				Iterator<?> it = resultList.iterator();
				while (it.hasNext()) {
					Object ob[] = (Object[]) it.next();
					SoceityInvoiceAdjustmentDTO response = new SoceityInvoiceAdjustmentDTO();
					response.setId((Long) ob[0]);
					log.info("Id::::::::" + response.getId());
					if ((Long) ob[1] != null) {
						SupplierMaster supplierMaster = supplierMasterRepository
								.getSupplierMasterBySupplierID((Long) ob[1]);
						response.setSupplierMaster(supplierMaster);
					}
					response.setAmount((Double) ob[2]);
					response.setAdjustmentMonth((Integer) ob[3]);
					response.setAdjustmentYear((Integer) ob[4]);

					if (ob[5] != null && ob[6] != null) {
						EntityMaster entityMaster = new EntityMaster();
						entityMaster.setCode((Integer) ob[5]);
						entityMaster.setName((String) ob[6]);
						response.setEntityMaster(entityMaster);
					}
					if (ob[7] != null && ob[8] != null) {
						SectionMaster sectionMaster = new SectionMaster();
						sectionMaster.setCode((String) ob[7]);
						sectionMaster.setName((String) ob[8]);
						response.setSectionMaster(sectionMaster);
					}
					response.setReferenceNumber(ob[9] != null ? (String) ob[9] : null);
					response.setReferenceDate(ob[10] != null ? (Date) ob[10] : null);
					log.info(":: List Response ::" + response);
					societyInvoiceAdjustmentDtoList.add(response);
				}
				baseDTO.setResponseContents(societyInvoiceAdjustmentDtoList);
				baseDTO.setTotalRecords(totalResult);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (Exception exp) {
			log.error("Exception Cause : ", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("========END UnitInspectionService.getLazyLoadData========");
		return baseDTO;
	}

}
