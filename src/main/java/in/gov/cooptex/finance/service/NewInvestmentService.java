package in.gov.cooptex.finance.service;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.repository.BankMasterRepository;
import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.service.NotificationEmailService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.enums.VoucherStatus;
import in.gov.cooptex.core.accounts.enums.VoucherTypeDetails;
import in.gov.cooptex.core.accounts.model.InvestmentTypeMaster;
import in.gov.cooptex.core.accounts.model.NewInvestment;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.VoucherDetails;
import in.gov.cooptex.core.accounts.model.VoucherLog;
import in.gov.cooptex.core.accounts.model.VoucherNote;
import in.gov.cooptex.core.accounts.repository.BankBranchMasterRepository;
import in.gov.cooptex.core.accounts.repository.InvestmentCategoryRepository;
import in.gov.cooptex.core.accounts.repository.InvestmentInstitutionRepository;
import in.gov.cooptex.core.accounts.repository.InvestmentTypeRepository;
import in.gov.cooptex.core.accounts.repository.NewInvestmentRepository;
import in.gov.cooptex.core.accounts.repository.PaymentMethodRepository;
import in.gov.cooptex.core.accounts.repository.VoucherDetailsRepository;
import in.gov.cooptex.core.accounts.repository.VoucherLogRepository;
import in.gov.cooptex.core.accounts.repository.VoucherNoteRepository;
import in.gov.cooptex.core.accounts.repository.VoucherRepository;
import in.gov.cooptex.core.accounts.repository.VoucherTypeRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.SequenceName;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.PaymentMethod;
import in.gov.cooptex.core.model.SequenceConfig;
import in.gov.cooptex.core.model.SystemNotification;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.SequenceConfigRepository;
import in.gov.cooptex.core.repository.SupplierMasterRepository;
import in.gov.cooptex.core.repository.SystemNotificationRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.dto.NewInvestmentDTO;
import in.gov.cooptex.operation.model.SupplierMaster;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class NewInvestmentService {

	@Autowired
	NewInvestmentRepository newInvestmentRepository;

	@Autowired
	ResponseWrapper responseWrapper;

	@Autowired
	InvestmentCategoryRepository investmentCategoryRepository;

	@Autowired
	InvestmentInstitutionRepository investmentInstitutionRepository;

	@Autowired
	InvestmentTypeRepository investmentTypeRepository;

	@Autowired
	PaymentMethodRepository paymentMethodRepository;

	@Autowired
	BankMasterRepository bankMasterRepository;

	@Autowired
	BankBranchMasterRepository bankBranchMasterRepository;

	@Autowired
	VoucherTypeRepository voucherTypeRepository;

	@Autowired
	UserMasterRepository userMasterRepository;

	@Autowired
	LoginService loginService;

	@Autowired
	VoucherRepository voucherRepository;

	@Autowired
	VoucherLogRepository voucherLogRepository;

	@Autowired
	VoucherNoteRepository voucherNoteRepository;

	@Autowired
	SequenceConfigRepository sequenceConfigRepository;

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	VoucherDetailsRepository voucherDetailsRepository;

	@Autowired
	EntityManager entityManager;

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	ApplicationQueryRepository applicationQueryRepository;
	
	@Autowired
	NotificationEmailService notificationEmailService;

	@Autowired
	SystemNotificationRepository systemNotificationRepository;
	
	
	final String VIEW_PAGE = "/pages/accounts/investment/viewInvestment.xhtml?faces-redirect=true";

	public BaseDTO getById(Long id,Long notificationId) { 
		log.info("NewInvestmentService getById method started [" + id + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			// Validate.notNull(id, ErrorDescription.INVESTMENT_CLOSING_ID_EMPTY);
			NewInvestmentDTO newInvestmentDTO = new NewInvestmentDTO();
			newInvestmentDTO.setNewInvestment(newInvestmentRepository.getOne(id));
			log.info(newInvestmentDTO.getNewInvestment().getId()
					+ "inside getbyid after newinvestmentRepo Getone method voucher ID-->"
					+ newInvestmentDTO.getNewInvestment().getVoucher().getId());
			newInvestmentDTO.setVoucherlog(
					voucherLogRepository.findByVoucherId(newInvestmentDTO.getNewInvestment().getVoucher().getId()));
			newInvestmentDTO.setVoucherNote(
					voucherNoteRepository.findByVoucherId(newInvestmentDTO.getNewInvestment().getVoucher().getId()));
			//For Note
			VoucherNote voucherNote= voucherNoteRepository.getVoucherNoteByID(newInvestmentDTO.getNewInvestment().getVoucher().getId());
			newInvestmentDTO.getVoucherNote().setNote(voucherNote.getNote());
			Voucher voucher= voucherRepository.findOne(newInvestmentDTO.getNewInvestment().getVoucher().getId());
			List<Map<String, Object>> employeeData = new ArrayList<Map<String, Object>>();
			if(voucher!=null) {
				log.info("<<<:::::::voucher::::not Null::::>>>>"+voucher!=null ? voucher.getId():"Null");
				 ApplicationQuery applicationQueryForlog = applicationQueryRepository.findByQueryName("VOUCHER_LOG_EMPLOYEE_DETAILS");
				 if (applicationQueryForlog == null || applicationQueryForlog.getId() == null) {
					 log.info("Application Query For Log Details not found for query name : " + applicationQueryForlog);
					 baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
					 return baseDTO;
				 }
				 String logquery = applicationQueryForlog.getQueryContent().trim();
				 log.info("<=========VOUCHER_LOG_EMPLOYEE_DETAILS Query Content ======>"+logquery);
				 logquery = logquery.replace(":voucherId", "'"+voucher.getId().toString()+"'");
				 log.info("Query Content For VOUCHER_LOG_EMPLOYEE_DETAILS After replaced plan id View query : " + logquery);
				 employeeData = jdbcTemplate.queryForList(logquery);
				 log.info("<=========VOUCHER_LOG_EMPLOYEE_DETAILS Employee Data======>"+employeeData);
				 baseDTO.setTotalListOfData(employeeData);
			}
			if (notificationId != null && notificationId > 0) {
				SystemNotification systemNotification = systemNotificationRepository.findOne(notificationId);
				systemNotification.setNotificationRead(true);
				systemNotificationRepository.save(systemNotification);
				
				
			}
			
			
			baseDTO.setResponseContent(newInvestmentDTO);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("NewInvestmentService getById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("NewInvestmentService getById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("InvestmentClosingService getById method completed");
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO deleteById(Long id) {
		log.info("NewInvestmentService deleteById method started [" + id + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			// Validate.notNull(id, ErrorDescription.INVESTMENT_CLOSING_ID_EMPTY);
			newInvestmentRepository.delete(id);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("NewInvestmentService deleteById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (DataIntegrityViolationException exception) {
			log.error("NewInvestmentService deleteById DataIntegrityViolationException ", exception);
			if (exception.getCause().getCause() instanceof PSQLException) {
				baseDTO.setStatusCode(ErrorDescription.CANNOT_DELETE_REFERENCED_RECORD.getErrorCode());
			} else {
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			}
		} catch (Exception exception) {
			log.error("NewInvestmentService deleteById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("NewInvestmentService deleteById method completed");
		return responseWrapper.send(baseDTO);
	}

	@Autowired
	SupplierMasterRepository supplierMasterRepository;
	
	@Transactional
	public BaseDTO saveorupdate(NewInvestment newInvestment) {
		log.info("NewInvestmentService saveorupdate starts method completed" + newInvestment.getId());
		BaseDTO baseDTO = new BaseDTO();
		String referenceNumber = "";
		try {

			Validate.notNullOrEmpty(newInvestment.getInvestmentCategoryMaster(),
					ErrorDescription.NEW_INVESTMENT_INVESTMENT_CATEGORY_NULL);
			Validate.notNullOrEmpty(newInvestment.getInvestmentTypeMaster(),
					ErrorDescription.NEW_INVESTMENT_INVESTMENT_TYPE_NULL);
			Validate.notNullOrEmpty(newInvestment.getPaymentMethod(), ErrorDescription.NEW_INVESTMENT_PAY_METHOD_NULL);
			Validate.notNullOrEmpty(newInvestment.getInvestmentAmount(),
					ErrorDescription.NEW_INVESTMENT_INVESTMENT_AMOUNT_NULL);
			Validate.notNullOrEmpty(newInvestment.getInvestmentDate(),
					ErrorDescription.NEW_INVESTMENT_INVESTED_DATE_NULL);

			Validate.notNullOrEmpty(newInvestment.getForwardTo(), ErrorDescription.NEW_INVESTMENT_FORWARD_TO_NULL);
			Validate.notNullOrEmpty(newInvestment.getFinalApproval(), ErrorDescription.NEW_INVESTMENT_FORWARD_FOR_NULL);
			Validate.notNullOrEmpty(newInvestment.getNote(), ErrorDescription.NEW_INVESTMENT_NOTE_NULL);

			NewInvestment newInvestmentObj = new NewInvestment();
			if (newInvestment.getId() == null) {
				SequenceConfig sequenceConfig = sequenceConfigRepository
						.findBySequenceName(SequenceName.NEW_INVESTMENT);
				if (sequenceConfig == null) {
					throw new RestException(ErrorDescription.SEQUENCE_CONFIG_NOT_EXIST);
				}
				EntityMaster entityMaster = entityMasterRepository
						.getEntityInfoByLoggedInUser(loginService.getCurrentUser().getId());
				log.info("Entity Master Code---->" + entityMaster.getCode());
				referenceNumber = entityMaster.getCode() + sequenceConfig.getSeparator() + sequenceConfig.getPrefix()
						+ AppUtil.getCurrentMonthString() + AppUtil.getCurrentYearString();
				Voucher voucher = new Voucher();

				voucher.setReferenceNumberPrefix(referenceNumber);
				voucher.setReferenceNumber(sequenceConfig.getCurrentValue());
				voucher.setName(VoucherTypeDetails.NEW_INVESTMENT.toString());
				voucher.setVoucherType(voucherTypeRepository.findByName(VoucherTypeDetails.Payment.toString()));
				voucher.setNarration(VoucherTypeDetails.Payment.toString());
				voucher.setFromDate(new Date());
				voucher.setToDate(new Date());
				voucher.setNetAmount(newInvestment.getInvestmentAmount());	
				voucher.setEntityMaster(entityMaster);

				VoucherDetails voucherDetails = new VoucherDetails();
				voucherDetails.setVoucher(voucher);
				voucherDetails.setAmount(newInvestment.getInvestmentAmount());
				voucher.getVoucherDetailsList().add(voucherDetails);

				VoucherLog voucherLog = new VoucherLog();
				voucherLog.setVoucher(voucher);
				voucherLog.setStatus(VoucherStatus.SUBMITTED);
				voucherLog.setUserMaster(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
				voucher.getVoucherLogList().add(voucherLog);

				VoucherNote voucherNote = new VoucherNote();
				voucherNote.setVoucher(voucher);
				voucherNote.setNote(newInvestment.getNote());
				voucherNote.setFinalApproval(newInvestment.getFinalApproval());
				voucherNote.setForwardTo(userMasterRepository.getOne(newInvestment.getForwardTo().getId()));
				voucher.getVoucherNote().add(voucherNote);

				Voucher voucherObj = voucherRepository.save(voucher);

				if (newInvestment.getBankBranchMaster() != null) {
					newInvestment.setBankBranchMaster(
							bankBranchMasterRepository.getOne(newInvestment.getBankBranchMaster().getId()));
				}
				if (newInvestment.getInvestmentInstitutionMaster() != null) {
					newInvestment.setInvestmentInstitutionMaster(supplierMasterRepository
							.getOne(newInvestment.getInvestmentInstitutionMaster().getId()));
				}
				if (voucherObj != null) {
					newInvestment.setVoucher(voucherObj);
				}
				newInvestment.setInvestmentCategoryMaster(
						investmentCategoryRepository.getOne(newInvestment.getInvestmentCategoryMaster().getId()));
				newInvestment.setInvestmentTypeMaster(
						investmentTypeRepository.getOne(newInvestment.getInvestmentTypeMaster().getId()));
				newInvestment
						.setPaymentMethod(paymentMethodRepository.getOne(newInvestment.getPaymentMethod().getId()));
				newInvestmentObj = newInvestmentRepository.save(newInvestment);

				sequenceConfig.setCurrentValue(sequenceConfig.getCurrentValue() + 1);
				sequenceConfigRepository.save(sequenceConfig);
				//For Notification
				Map<String, Object> additionalData = new HashMap<>();
				additionalData.put("Url", VIEW_PAGE + "&id=" + newInvestment.getId() + "&");
				notificationEmailService.sendMailAndNotificationForForward(voucherNote, voucherLog,
						additionalData);
			} else {
				newInvestmentObj = updatenewInvestment(newInvestment);
			}
			
			
			

			if (newInvestmentObj != null) {
				log.info("<-----NewInvestmentService saved or updated successfully--->");
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				log.info("NEW INVESTMENT OBJ ::::::::::::::" + newInvestmentObj);
			}

		} catch (DataIntegrityViolationException divEX) {
			log.warn("<<===  Error While updating NewInvestmentService ===>>", divEX);
			String exceptionCause = divEX.getCause().getCause().getMessage();
		} catch (ObjectOptimisticLockingFailureException lockEx) {
			log.warn("====>> Error while updating NewInvestmentService <<====", lockEx);
			baseDTO.setStatusCode(ErrorDescription.CANNOT_UPDATE_LOCKED_RECORD.getErrorCode());
		} catch (RestException re) {
			log.error("RestException occured in NewInvestmentService.saveOrUpdate", re);
			baseDTO.setStatusCode(re.getStatusCode());
		} catch (Exception e) {
			log.error("Exception occured in NewInvestmentService.saveOrUpdate", e);
			baseDTO.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<==== Ends NewInvestmentService.saveOrUpdate =====>");
		return responseWrapper.send(baseDTO);
	}

	public NewInvestment updatenewInvestment(NewInvestment newInvestment) {
		log.info("<==== Starts NewInvestmentService method=====>");
		NewInvestment newInvestmentObj = null;
		try {
			newInvestmentObj = newInvestmentRepository.findOne(newInvestment.getId());
			Voucher voucherObj = voucherRepository.findOne(newInvestment.getVoucher().getId());

			voucherObj.setNetAmount(newInvestment.getInvestmentAmount());

			List<VoucherDetails> voucherDetailsList = voucherDetailsRepository.findAllByVoucherId(voucherObj.getId());
			for (VoucherDetails voucherDetails : voucherDetailsList) {
				voucherDetails.setVoucher(voucherObj);
				voucherDetails.setAmount(newInvestment.getInvestmentAmount());
				voucherObj.getVoucherDetailsList().add(voucherDetails);
			}

			VoucherLog voucherLog = new VoucherLog();
			voucherLog.setVoucher(voucherObj);
			voucherLog.setStatus(VoucherStatus.SUBMITTED);
			voucherLog.setUserMaster(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
			voucherObj.getVoucherLogList().add(voucherLog);

			VoucherNote voucherNote = new VoucherNote();
			voucherNote.setVoucher(voucherObj);
			voucherNote.setNote(newInvestment.getNote());
			voucherNote.setFinalApproval(newInvestment.getFinalApproval());
			voucherNote.setForwardTo(userMasterRepository.getOne(newInvestment.getForwardTo().getId()));
			voucherObj.getVoucherNote().add(voucherNote);

			voucherRepository.save(voucherObj);

			if (newInvestment.getBankBranchMaster() != null) {
				newInvestmentObj.setBankBranchMaster(
						bankBranchMasterRepository.getOne(newInvestment.getBankBranchMaster().getId()));
			} else {
				newInvestmentObj.setBankBranchMaster(null);
			}
			if (newInvestment.getInvestmentInstitutionMaster() != null) {
				newInvestmentObj.setInvestmentInstitutionMaster(
						supplierMasterRepository.getOne(newInvestment.getInvestmentInstitutionMaster().getId()));
			} else {
				newInvestmentObj.setInvestmentInstitutionMaster(null);
			}
			if (voucherObj != null) {
				newInvestmentObj.setVoucher(voucherObj);
			}
			newInvestmentObj.setInvestmentCategoryMaster(
					investmentCategoryRepository.getOne(newInvestment.getInvestmentCategoryMaster().getId()));
			newInvestmentObj.setInvestmentTypeMaster(
					investmentTypeRepository.getOne(newInvestment.getInvestmentTypeMaster().getId()));
			newInvestmentObj.setPaymentMethod(paymentMethodRepository.getOne(newInvestment.getPaymentMethod().getId()));
			newInvestmentObj.setInvestmentAmount(newInvestment.getInvestmentAmount());
			newInvestmentObj.setInvestmentDate(newInvestment.getInvestmentDate());
			newInvestmentObj.setShareCertificateFrom(newInvestment.getShareCertificateFrom());
			newInvestmentObj.setShareCertificateTo(newInvestment.getShareCertificateTo());
			newInvestmentObj.setCerificateNumber(newInvestment.getCerificateNumber());
			newInvestmentObj.setCertificateDate(newInvestment.getCertificateDate());
			newInvestmentObj.setCertificateAmount(newInvestment.getCertificateAmount());
			newInvestmentObj.setInvestmentMaturityAmount(newInvestment.getInvestmentMaturityAmount());
			newInvestmentObj.setInvestmentMaturituyDate(newInvestment.getInvestmentMaturituyDate());
			newInvestmentObj.setInterestCollectionType(newInvestment.getInterestCollectionType());
			newInvestmentObj.setRateOfInterest(newInvestment.getRateOfInterest());
			newInvestmentObj.setPreclosureRateOfInterest(newInvestment.getPreclosureRateOfInterest());
			newInvestmentObj = newInvestmentRepository.save(newInvestmentObj);
		} catch (Exception e) {
			log.error("Exception occured in NewInvestmentService.updatenewInvestment", e);
		}

		return newInvestmentObj;
	}

	public BaseDTO getAllNewInvestmentlistlazy(PaginationDTO paginationDto) {
		log.info(" NewInvestmentService getAllNewInvestmentlistlazy  called..." + paginationDto);
		BaseDTO baseDTO = new BaseDTO();
		try {
			Integer total = 0;
			Integer start = paginationDto.getFirst(), pageSize = paginationDto.getPageSize();
			start = start * pageSize;
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();
			ApplicationQuery applicationQuery = applicationQueryRepository.findByQueryName("NEW_INVESTMENT_LAZY_LIST_QUERY");
			
			String mainQuery =applicationQuery.getQueryContent().trim();

			if (paginationDto.getFilters() != null) {

				if (paginationDto.getFilters().get("investmentTypeMaster.name") != null) {
					mainQuery += " and upper(itm.name) like upper('%"
							+ paginationDto.getFilters().get("investmentTypeMaster.name") + "%') ";
				}
				if (paginationDto.getFilters().get("voucher.fromDate") != null) {
					Date fromDate = new Date((Long) paginationDto.getFilters().get("voucher.fromDate"));
					mainQuery += " and v.from_date='" + fromDate + "'";
				}
				if (paginationDto.getFilters().get("voucher.toDate") != null) {
					Date toDate = new Date((Long) paginationDto.getFilters().get("voucher.toDate"));
					mainQuery += " and v.to_date='" + toDate + "'";
				}
				if (paginationDto.getFilters().get("voucher.createdDate") != null) {
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					Date createdDate = new Date((Long) paginationDto.getFilters().get("voucher.createdDate"));
					mainQuery += " and v.created_date::date='" + format.format(createdDate) + "'";
				}
				
				
				if (paginationDto.getFilters().get("investmentAmount") != null) {
					Double d=Double.valueOf(paginationDto.getFilters().get("investmentAmount").toString());
					mainQuery += " and ni.investment_amount >="+d;
				}
				if (paginationDto.getFilters().get("investmentDate") != null) {
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					Date investmentDate = new Date((Long) paginationDto.getFilters().get("investmentDate"));
					mainQuery += " and ni.investment_date::date='" + format.format(investmentDate) + "'";
				}
				
				if (paginationDto.getFilters().get("stage") != null) {
					mainQuery += " and vl.status='" + paginationDto.getFilters().get("stage") + "'";
				}
			}
			String countQuery = mainQuery.replace(
					"SELECT ni.id as id,itm.name as name,v.from_date as fromDate,v.to_date as toDate,v.created_date as createdDate,vl.status as status,ni.investment_amount as investmentamount,ni.investment_date as investmentdate",
					"select count(distinct(ni.id)) as count ");
			log.info("count query... " + countQuery);
			countData = jdbcTemplate.queryForList(countQuery);
			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					total = Integer.parseInt(data.get("count").toString().replace(",", ""));
			}
			log.info("total query... " + total);
			if (paginationDto.getSortField() == null)
				mainQuery += " order by id desc limit " + pageSize + " offset " + start + ";";

			if (paginationDto.getSortField() != null && paginationDto.getSortOrder() != null) {

				log.info("Sort Field:[" + paginationDto.getSortField() + "] Sort Order:[" + paginationDto.getSortOrder()
						+ "]");
				if (paginationDto.getSortField().equals("investmentTypeMaster.name")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by itm.name asc ";
				if (paginationDto.getSortField().equals("investmentTypeMaster.name")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by itm.name desc ";

				if (paginationDto.getSortField().equals("voucher.fromDate")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by v.from_date asc  ";
				if (paginationDto.getSortField().equals("voucher.fromDate")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by v.from_date desc  ";

				if (paginationDto.getSortField().equals("voucher.toDate")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by v.to_date asc ";
				if (paginationDto.getSortField().equals("voucher.toDate")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by v.to_date desc ";

				if (paginationDto.getSortField().equals("voucher.createdDate")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by v.created_date asc ";
				if (paginationDto.getSortField().equals("voucher.createdDate")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by v.created_date desc ";

				if (paginationDto.getSortField().equals("stage") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by vl.status asc ";
				if (paginationDto.getSortField().equals("stage") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by vl.status desc ";
				mainQuery += " limit " + pageSize + " offset " + start + ";";
			}
			log.info("Main Qury....." + mainQuery);
			List<NewInvestment> newInvestmentList = new ArrayList<>();
			listofData = jdbcTemplate.queryForList(mainQuery);
			for (Map<String, Object> data : listofData) {
				NewInvestment newInvestment = new NewInvestment();
				if (data.get("id") != null) {
					newInvestment.setId(Long.parseLong(data.get("id").toString()));
				}
				if (data.get("name") != null) {
					InvestmentTypeMaster investmentTypeMaster = new InvestmentTypeMaster();
					investmentTypeMaster.setName((data.get("name").toString()));
					newInvestment.setInvestmentTypeMaster(investmentTypeMaster);
				}
				if (data.get("status") != null) {
					Voucher voucher = new Voucher();
					voucher.setFromDate((Date) data.get("fromDate"));
					voucher.setToDate((Date) data.get("toDate"));
					voucher.setCreatedDate((Date) data.get("createdDate"));
					newInvestment.setVoucher(voucher);
				}
				if (data.get("status") != null) {
					newInvestment.setStage((data.get("status").toString()));
				}
				
				if (data.get("investmentamount") != null) {
					newInvestment.setInvestmentAmount(Double.parseDouble(data.get("investmentamount").toString()));
				}

				if (data.get("investmentdate") != null) {
					newInvestment.setInvestmentDate((Date) data.get("investmentdate"));
				}
				
				newInvestmentList.add(newInvestment);
			}
			log.info("Returning getAllNewInvestmentlistlazy list...." + newInvestmentList.size());
			log.info("Total records present in getAllNewInvestmentlistlazy..." + total);
			baseDTO.setResponseContent(newInvestmentList);
			baseDTO.setTotalRecords(total);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (Exception exp) {
			log.error("Exception Cause  : ", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	@Transactional
	public BaseDTO newInvestmentForwardStage(NewInvestmentDTO newInvestmentDTO) {
		log.info("inside service newInvestmentForwardStage");
		BaseDTO response = new BaseDTO();
		log.info("newInvestmentService newInvestmentForwardStage method started ["
				+ newInvestmentDTO.getNewInvestment().getVoucher().getId() + "]");
		String message="";
		try {
			log.info("idddd"+ newInvestmentDTO.getNewInvestment().getId()+ "----"+ newInvestmentDTO.getVoucherlog().getStatus());
					
				
				
					
					
			
			if (newInvestmentDTO.getVoucherlog().getStatus().equals(VoucherStatus.REJECTED)) {
				VoucherLog voucherlog = new VoucherLog();
				voucherlog.setStatus(newInvestmentDTO.getVoucherlog().getStatus());
				voucherlog.setRemarks(newInvestmentDTO.getVoucherlog().getRemarks());
				voucherlog.setUserMaster(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
				voucherlog.setVoucher(
						voucherRepository.findOne(newInvestmentDTO.getNewInvestment().getVoucher().getId()));
				voucherLogRepository.save(voucherlog);
				if(voucherlog.getStatus().equals(VoucherStatus.REJECTED)){
					
					List<VoucherLog> toUserId= voucherLogRepository.getforwardtoUserId((newInvestmentDTO.getNewInvestment().getVoucher().getId()),loginService.getCurrentUser().getId());
					log.info("logggg"+ toUserId + (newInvestmentDTO.getNewInvestment().getVoucher().getId()+"--"+ loginService.getCurrentUser().getId()));
					for(VoucherLog voucherlg: toUserId) {
						Long approveid= voucherlg.getCreatedBy().getId();
						log.info("iddd" + voucherlg.getCreatedBy().getId());
						String urlPath="/pages/accounts/investment/viewInvestment.xhtml?faces-redirect=true&id="
								+ newInvestmentDTO.getNewInvestment().getId()  + "&stage=" + voucherlog.getStatus() + "&";
						
						if(voucherlog.getStatus().equals(VoucherStatus.REJECTED)){
							 message= "New Investment - " +newInvestmentDTO.getNewInvestment().getVoucher().getId()+ " has been Rejected " ;
						}
						
						
						notificationEmailService.saveIndividualNotification(loginService.getCurrentUser(),approveid,urlPath,"New Investment",message);
					}
					
				}
				
				
				
				
			} else {
				Validate.objectNotNull(newInvestmentDTO.getVoucherNote().getForwardTo(),
						ErrorDescription.NEW_INVESTMENT_FORWARD_TO_NULL);
				VoucherLog voucherlog = new VoucherLog();
				voucherlog.setStatus(newInvestmentDTO.getVoucherlog().getStatus());
				voucherlog.setRemarks(newInvestmentDTO.getVoucherlog().getRemarks());
				voucherlog.setUserMaster(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
				voucherlog.setVoucher(
						voucherRepository.findOne(newInvestmentDTO.getNewInvestment().getVoucher().getId()));
				voucherLogRepository.save(voucherlog);

				VoucherNote voucherNote = new VoucherNote();
				voucherNote.setNote(newInvestmentDTO.getVoucherNote().getNote());
				voucherNote.setForwardTo(
						userMasterRepository.findOne(newInvestmentDTO.getVoucherNote().getForwardTo().getId()));
				voucherNote.setFinalApproval(newInvestmentDTO.getVoucherNote().getFinalApproval());
				voucherNote.setVoucher(
						voucherRepository.findOne(newInvestmentDTO.getNewInvestment().getVoucher().getId()));
				voucherNoteRepository.save(voucherNote);
				
				if(voucherlog.getStatus().equals(VoucherStatus.APPROVED)
						|| voucherlog.getStatus().equals(VoucherStatus.SUBMITTED)
						||voucherlog.getStatus().equals(VoucherStatus.FINALAPPROVED )){
					
					List<VoucherLog> toUserId= voucherLogRepository.getforwardtoUserId((newInvestmentDTO.getNewInvestment().getVoucher().getId()),loginService.getCurrentUser().getId());
					log.info("logggg"+ toUserId + (newInvestmentDTO.getNewInvestment().getVoucher().getId()+"--"+ loginService.getCurrentUser().getId()));
					for(VoucherLog voucherlg: toUserId) {
						Long approveid= voucherlg.getCreatedBy().getId();
						log.info("iddd" + voucherlg.getCreatedBy().getId());
						String urlPath="/pages/accounts/investment/viewInvestment.xhtml?faces-redirect=true&id="
								+ newInvestmentDTO.getNewInvestment().getId()  + "&stage=" + voucherlog.getStatus() + "&";
						
						if(voucherlog.getStatus().equals(VoucherStatus.SUBMITTED)){
							 message= "New Investment - " +newInvestmentDTO.getNewInvestment().getVoucher().getId()+ " has been Approved " ;
						}
						else if(voucherlog.getStatus().equals(VoucherStatus.APPROVED)){
							 message= "New Investment - " + newInvestmentDTO.getNewInvestment().getVoucher().getId()+ " has been Approved " ;
						}
						else if(voucherlog.getStatus().equals(VoucherStatus.FINALAPPROVED)){
							 message= "New Investment- " + newInvestmentDTO.getNewInvestment().getVoucher().getId()+ "  Final Approval has been Completed " ;
						}
						
						notificationEmailService.saveIndividualNotification(loginService.getCurrentUser(),approveid,urlPath,"New Investment",message);
					}
					
				}
				
				//For Notification
				if(voucherlog.getStatus().equals(VoucherStatus.APPROVED)) {
				Map<String, Object> additionalData = new HashMap<>();
				additionalData.put("Url", VIEW_PAGE + "&id=" +newInvestmentDTO.getNewInvestment().getId() + "&");
				notificationEmailService.sendMailAndNotificationForForward(voucherNote, voucherlog,
						additionalData);
			}
			}

			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());

		} catch (RestException re) {
			log.error("RestException occured in newinvestmentService.newInvestmentForwardStage", re);
			response.setStatusCode(re.getStatusCode());
		} catch (Exception e) {
			log.error("Exception occured...", e);
		}
		log.info("<==== Ends newInvestmentForwardStage method=====>");
		return responseWrapper.send(response);
	}
	
	public BaseDTO loadPaymentMethod() {
		log.info("<--- Starts newinvestmentService .loadPaymentMethod() ---> ");
		BaseDTO baseDTO = new BaseDTO();
		try{
			List<PaymentMethod> paymentMethodList= paymentMethodRepository.getAllOrderByName();
			baseDTO.setResponseContent(paymentMethodList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		}catch (RestException restException) {
			baseDTO.setStatusCode(restException.getStatusCode());
			log.error("RestException in loadPaymentMethod ", restException);
		} catch (Exception exception) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.error("Exception in loadPaymentMethod ", exception);
		}
		log.info("<--- Ends loadPaymentMethod() ---> " );
		return responseWrapper.send(baseDTO);
	} 
	public BaseDTO getSuppilerByType(String supplierTypeCode) {
		log.info("<--- Starts newinvestmentService .getSuppilerByType() ---> ");
		BaseDTO baseDTO = new BaseDTO();
		try{
			List<SupplierMaster> supplierMasterList= supplierMasterRepository.findSocietySupplierTypeCode(supplierTypeCode);
			log.info("supplierMasterList size:"+supplierMasterList!=null?supplierMasterList.size():0);
			baseDTO.setResponseContents(supplierMasterList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		}catch (RestException restException) {
			baseDTO.setStatusCode(restException.getStatusCode());
			log.error("RestException in getSuppilerByType ", restException);
		} catch (Exception exception) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.error("Exception in getSuppilerByType ", exception);
		}
		log.info("<--- Ends getSuppilerByType() ---> " );
		return responseWrapper.send(baseDTO);
	} 
	public BaseDTO getInvestmentType() {
		log.info("newinvestmentService  getInvestmentType method started ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<InvestmentTypeMaster> investmentTypeList= investmentTypeRepository.getInvestmentType();
			baseDTO.setResponseContent(investmentTypeList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("newinvestmentService getInvestmentType RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("newinvestmentService getInvestmentType Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("newinvestmentService  getInvestmentType method completed");
		return responseWrapper.send(baseDTO);
	}
	

}