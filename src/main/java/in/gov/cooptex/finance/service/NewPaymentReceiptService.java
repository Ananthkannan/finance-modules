package in.gov.cooptex.finance.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.repository.BankMasterRepository;
import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.service.NotificationEmailService;
import in.gov.cooptex.core.accounts.enums.VoucherStatus;
import in.gov.cooptex.core.accounts.enums.VoucherTypeDetails;
import in.gov.cooptex.core.accounts.model.AccountTransaction;
import in.gov.cooptex.core.accounts.model.AccountTransactionDetails;
import in.gov.cooptex.core.accounts.model.EntityBankBranch;
import in.gov.cooptex.core.accounts.model.GlAccount;
import in.gov.cooptex.core.accounts.model.Payment;
import in.gov.cooptex.core.accounts.model.PaymentDetails;
import in.gov.cooptex.core.accounts.model.PurchaseInvoice;
import in.gov.cooptex.core.accounts.model.ReceiptDetails;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.VoucherDetails;
import in.gov.cooptex.core.accounts.model.VoucherLog;
import in.gov.cooptex.core.accounts.model.VoucherNote;
import in.gov.cooptex.core.accounts.model.VoucherType;
import in.gov.cooptex.core.accounts.repository.AccountTransactionDetailsRepository;
import in.gov.cooptex.core.accounts.repository.AccountTransactionRepository;
import in.gov.cooptex.core.accounts.repository.EntityBankBranchRepository;
import in.gov.cooptex.core.accounts.repository.GlAccountRepository;
import in.gov.cooptex.core.accounts.repository.PaymentDetailsRepository;
import in.gov.cooptex.core.accounts.repository.PaymentMethodRepository;
import in.gov.cooptex.core.accounts.repository.PaymentRepository;
import in.gov.cooptex.core.accounts.repository.PaymentTypeMasterRepository;
import in.gov.cooptex.core.accounts.repository.VoucherDetailsRepository;
import in.gov.cooptex.core.accounts.repository.VoucherLogRepository;
import in.gov.cooptex.core.accounts.repository.VoucherNoteRepository;
import in.gov.cooptex.core.accounts.repository.VoucherRepository;
import in.gov.cooptex.core.accounts.repository.VoucherTypeRepository;
import in.gov.cooptex.core.accounts.util.VoucherTypeCons;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.PaymentCategory;
import in.gov.cooptex.core.enums.PettyCashPayFor;
import in.gov.cooptex.core.enums.SequenceName;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.BankMaster;
import in.gov.cooptex.core.model.CustomerMaster;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeePersonalInfoEmployment;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.PaymentMethod;
import in.gov.cooptex.core.model.PaymentMode;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.model.SequenceConfig;
import in.gov.cooptex.core.model.SystemNotification;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.CustomerMasterRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EmployeePersonalInfoEmploymentRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.PaymentModeRepository;
import in.gov.cooptex.core.repository.PurchaseInvoiceRepository;
import in.gov.cooptex.core.repository.SectionMasterRepository;
import in.gov.cooptex.core.repository.SequenceConfigRepository;
import in.gov.cooptex.core.repository.SupplierMasterRepository;
import in.gov.cooptex.core.repository.SystemNotificationRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.finance.dto.PettyCashReceiptDTO;
import in.gov.cooptex.operation.model.SupplierMaster;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class NewPaymentReceiptService {

	@Autowired
	SupplierMasterRepository supplierMasterRepository;

	@Autowired
	EmployeePersonalInfoEmploymentRepository employeePersonalInfoEmploymentRepository;

	@Autowired
	NotificationEmailService notificationEmailService;

	@Autowired
	private PurchaseInvoiceRepository purchaseInvoiceRepository;

	@Autowired
	private SectionMasterRepository sectionMasterRepository;

	@Autowired
	CustomerMasterRepository customerMasterRepository;

	@Autowired
	EmployeeMasterRepository employeeMasterRepository;

	@Autowired
	GlAccountRepository glAccountRepository;

	@Autowired
	LoginService loginService;

	@Autowired
	private SystemNotificationRepository systemNotificationRepository;

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	UserMasterRepository userMasterRepository;

	@Autowired
	PaymentModeRepository paymentModeRepository;

	@Autowired
	SequenceConfigRepository sequenceConfigRepository;

	@Autowired
	VoucherTypeRepository voucherTypeRepository;

	@Autowired
	VoucherRepository voucherRepository;

	@Autowired
	VoucherDetailsRepository voucherDetailsRepository;

	@Autowired
	PaymentDetailsRepository paymentDetailsRepository;

	@Autowired
	PaymentRepository paymentRepository;

	@Autowired
	PaymentTypeMasterRepository paymentTypeMasterRepository;

	@Autowired
	PaymentMethodRepository paymentMethodRepository;

	@Autowired
	AccountTransactionDetailsRepository accountTransactionDetailsRepository;

	@Autowired
	AccountTransactionRepository accountTransactionRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	BankMasterRepository bankMasterRepository;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	@Autowired
	EntityBankBranchRepository entityBankBranchRepository;

	@Autowired
	VoucherNoteRepository voucherNoteRepository;

	@Autowired
	VoucherLogRepository voucherLogRepository;

	public BaseDTO getSupplierCodeAutoComplete(Long supplierTypeId, String supplierTypecode) {
		log.info("=========START MarketingAuditService.getEmployeeByAutoComplete=====");
		log.info("=========suppliertypecode is=====" + supplierTypecode);
		BaseDTO baseDto = new BaseDTO();
		try {
			if (supplierTypecode != null) {
				List<SupplierMaster> supplytypelist = supplierMasterRepository
						.getSupplierCodeAutoComplete(supplierTypeId, supplierTypecode);
				baseDto.setResponseContents(supplytypelist);
				baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			}
		} catch (Exception e) {
			log.info("=========Exception Occured in PaymentReceipt.getEmployeeByAutoComplete=====");
			log.info("=========Exception is=====" + e.getMessage());
		}
		log.info("=========END .getsuppliercodeAutoComplete=====");
		return baseDto;
	}

	public BaseDTO getCustomerCodeAutoComplete(Long customerTypeId, String customerTypecode) {

		log.info("=========customertypecode is=====" + customerTypecode);
		BaseDTO baseDto = new BaseDTO();

		try {

			List<CustomerMaster> customerlist = new ArrayList<CustomerMaster>();
			if (customerTypecode != null) {

				customerlist = customerMasterRepository.findAllCustomerByTypeId(customerTypecode, customerTypeId);
				log.info("=========customertypecode Size=====" + customerlist.size());
				baseDto.setResponseContents(customerlist);
				log.info("=========customertypecode is=====ededededededededededed");
				baseDto.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			}
			log.info("=========customertypecode is=====2223332");
		} catch (Exception e) {
			log.error("Exception at getCustomerCodeAutoComplete()", e);
			baseDto.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.info("=========Exception is=====" + e.getMessage());
		}
		log.info("=========END .getCustomercodeAutoComplete=====");
		return baseDto;
	}

	public BaseDTO getActiveEmployeeNameOrderList() {
		log.info("EmployeeService.getActiveEmployeeNameOrderList() Started ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<EmployeeMaster> employeeList = employeeMasterRepository.getActiveEmployeeNameOrderList();
			if (employeeList != null && employeeList.size() > 0) {
				baseDTO.setResponseContent(employeeList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				log.info("EmployeeService.getAllEmployee() Completed");
			}
		} catch (Exception exception) {
			log.error("Exception occurred in EmployeeService.getAllEmployee() -:", exception);
			baseDTO.setStatusCode(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		log.info("EmployeeService.getActiveEmployeeNameOrderList() end ");
		return baseDTO;
	}

	@Transactional
	public BaseDTO createCashReceipt(PettyCashReceiptDTO pettyCashReceiptDTO) {
		log.info("<============== PettyCashReceiptService:createReceipt ================> "
				+ pettyCashReceiptDTO.getTransactionType());
		BaseDTO baseDTO = new BaseDTO();
		List<ReceiptDetails> receiptDetailsList = new ArrayList<ReceiptDetails>();
		List<VoucherDetails> voucherDetailsList = new ArrayList<>();
		List<VoucherNote> voucherNoteList = new ArrayList<>();
		try {
			if (pettyCashReceiptDTO != null) {
				EntityMaster entityMaster = entityMasterRepository
						.findByUserRegion(loginService.getCurrentUser().getId());
				SequenceConfig sequenceConfig;
				VoucherType voucherType;
				Voucher voucher = new Voucher();
				PaymentCategory paymentCategory;
				String numericReferenceNumber;
				if (pettyCashReceiptDTO.getTransactionType().equalsIgnoreCase("RECEIPT")) {
					sequenceConfig = sequenceConfigRepository
							.findBySequenceName(SequenceName.valueOf(SequenceName.PAYMENT_RECEIPT_REF_NUMBER.name()));
					voucher.setName(VoucherTypeDetails.PETTY_RECEIPT.toString());
					voucherType = voucherTypeRepository.findByName(VoucherTypeDetails.Receipt.toString());
					// voucher.setPaymentFor(VoucherTypeDetails.PETTY_RECEIPT.toString());
					paymentCategory = PaymentCategory.PAID_IN;
					// generate ReferenceNumber
					String entityCode = String.valueOf(entityMaster.getCode());
					numericReferenceNumber = getReferenceNumber(VoucherTypeDetails.PETTY_RECEIPT.toString(),
							voucherType.getId(), AppUtil.getCurrentYearString("yy"), entityCode);
				} else {
					sequenceConfig = sequenceConfigRepository
							.findBySequenceName(SequenceName.valueOf(SequenceName.PAYMENT_REF_NUMBER.name()));
					String voucherName = pettyCashReceiptDTO.getVoucherName();
					if (StringUtils.isNotEmpty(voucherName)) {
						voucher.setName(voucherName);
					} else {
						voucher.setName(VoucherTypeDetails.PETTY_CASH_PAYMENT.toString());
					}

					voucherType = voucherTypeRepository.findByName(VoucherTypeDetails.Payment.toString());
					// voucher.setPaymentFor(VoucherTypeDetails.PETTY_CASH_PAYMENT.toString());
					paymentCategory = PaymentCategory.PAID_OUT;
					// generate ReferenceNumber
					// String
					// entityCodyAndYear=entityMaster.getCode()+AppUtil.getCurrentYearString("yy");
					String entityCode = String.valueOf(entityMaster.getCode());
					numericReferenceNumber = getReferenceNumber(voucher.getName(), voucherType.getId(),
							AppUtil.getCurrentYearString("yy"), entityCode);
				}
				if (sequenceConfig == null) {
					throw new RestException(ErrorDescription.SEQUENCE_CONFIG_NOT_EXIST);
				}
				if (AppUtil.checkYearChangeStatus(sequenceConfig.getModifiedDate())) {
					sequenceConfig.setCurrentValue(1L);
				}

				if (pettyCashReceiptDTO.getSectionMaster() != null) {
					SectionMaster sm = sectionMasterRepository
							.getByCode(pettyCashReceiptDTO.getSectionMaster().getCode());
					voucher.setSectionMaster(sm);
				}

				String referenceNumber = sequenceConfig.getPrefix() + entityMaster.getCode()
						+ sequenceConfig.getSeparator() + AppUtil.getCurrentDate() + AppUtil.getCurrentMonthString()
						+ AppUtil.getCurrentYearString() + sequenceConfig.getSeparator() + numericReferenceNumber;

				log.info("<======= referencenumber ========>" + pettyCashReceiptDTO.getVoucherNarration());
				String receiptDetailsRefNumber = referenceNumber + sequenceConfig.getCurrentValue();
				voucher.setReferenceNumberPrefix(referenceNumber);
				voucher.setReferenceNumber(Long.valueOf(numericReferenceNumber));
				// voucher.setName(VoucherTypeDetails.PETTY_RECEIPT.toString());
				// VoucherType voucherType =
				// voucherTypeRepository.findByName(VoucherTypeDetails.Receipt.toString());
				voucher.setVoucherType(voucherType);
				voucher.setNetAmount(pettyCashReceiptDTO.getTotalAmount());
				voucher.setNarration(pettyCashReceiptDTO.getVoucherNarration());
				// voucher.setPaymentFor(VoucherTypeDetails.PETTY_RECEIPT.toString());
				voucher.setFromDate(new Date());
				voucher.setToDate(new Date());
				voucher.setEntityMaster(entityMaster);
				//
				voucher.setTransactionName(pettyCashReceiptDTO.getTransactionName());
				//
				if (pettyCashReceiptDTO.getForEntityMaster() != null) {
					EntityMaster forEntityMaster = entityMasterRepository
							.findById(pettyCashReceiptDTO.getForEntityMaster().getId());
					voucher.setForEntityMaster(forEntityMaster);
				}
				// voucher.setPaymentMode(pettyCashReceiptDTO.getPaymentMode());
				if (pettyCashReceiptDTO.getPaymentMode() != null) {
					voucher.setPaymentMode(
							paymentModeRepository.findByCode(pettyCashReceiptDTO.getPaymentMode().getCode()));
				}
				if (pettyCashReceiptDTO.getSupplierMaster() != null) {
					voucher.setSuplierId(pettyCashReceiptDTO.getSupplierMaster().getId());
				} else if (pettyCashReceiptDTO.getEmployeeMaster() != null) {
					voucher.setEmpId(pettyCashReceiptDTO.getEmployeeMaster().getId());
				} else if (pettyCashReceiptDTO.getCustomerMaster() != null) {
					voucher.setCustomerId(pettyCashReceiptDTO.getCustomerMaster().getId());
				} else if (pettyCashReceiptDTO.getPayName() != null) {
					voucher.setPaymentFor(pettyCashReceiptDTO.getPayName());
				}

				PurchaseInvoice pi = new PurchaseInvoice();
				if (pettyCashReceiptDTO.getPurchaseInvoice() != null) {
					pi = purchaseInvoiceRepository.findOne(pettyCashReceiptDTO.getPurchaseInvoice().getId());
				}
				// 1st table voucher
				for (PettyCashReceiptDTO receiptDetails : pettyCashReceiptDTO.getReceiptDetails()) {
					ReceiptDetails receiptDetailsObj = new ReceiptDetails();
					receiptDetailsObj.setEntityMaster(entityMaster);
					if (pettyCashReceiptDTO.getEmployeeMaster() != null) {
						receiptDetailsObj.setEmpMaster(
								employeeMasterRepository.findById(pettyCashReceiptDTO.getEmployeeMaster().getId()));
						receiptDetailsObj.setPayTo(PettyCashPayFor.EMPLOYEE);
					}
					receiptDetailsObj.setRefNumber(receiptDetailsRefNumber);
					receiptDetailsObj.setExpenseDate(new Date());
					if (receiptDetails.getExpenseAmount() != null) {
						receiptDetailsObj.setExpenseAmount(receiptDetails.getExpenseAmount());
					}

					receiptDetailsObj.setExpenseTransactionName(pettyCashReceiptDTO.getExpenseTransactionName());
					if (pettyCashReceiptDTO.getSupplierMaster() != null) {
						receiptDetailsObj.setPayTo(PettyCashPayFor.SUPPLIER);
						receiptDetailsObj.setSupplierMaster(
								supplierMasterRepository.findOne(pettyCashReceiptDTO.getSupplierMaster().getId()));
					}
					if (pettyCashReceiptDTO.getPayFor() != null) {
						receiptDetailsObj.setPayTo(PettyCashPayFor.OTHER);
						receiptDetailsObj.setPayFor(pettyCashReceiptDTO.getPayName());
					}
					if (pettyCashReceiptDTO.getPaymentMode() != null) {
						receiptDetailsObj.setPaymentMode(pettyCashReceiptDTO.getPaymentMode());
					}
					receiptDetailsObj.setVoucher(voucher);
					receiptDetailsList.add(receiptDetailsObj);
					// Voucher Details
					VoucherDetails voucherDetails = new VoucherDetails();
					voucherDetails.setVoucher(voucher);
					voucherDetails.setPurchaseInvoice(pi);
					voucherDetails.setAmount(receiptDetails.getExpenseAmount());
					if (receiptDetails.getAccount() != null) {
						voucherDetails.setGlAccount(glAccountRepository.findOne(receiptDetails.getAccount().getId()));
					}

					if (receiptDetails.getPostingEmployee() != null) {
						EmployeeMaster empMaster = employeeMasterRepository
								.findOne(receiptDetails.getPostingEmployee().getId());
						voucherDetails.setEmpMaster(empMaster);
					}

					if (receiptDetails.getPostingEntityMaster() != null) {
						EntityMaster em = entityMasterRepository
								.findId(receiptDetails.getPostingEntityMaster().getId());

						voucherDetails.setEntityMaster(em);
					}

					voucherDetailsList.add(voucherDetails);
				}
				voucher.setVoucherDetailsList(voucherDetailsList);

				VoucherLog voucherLog = new VoucherLog();

				VoucherNote voucherNote = new VoucherNote();
				voucherLog.setVoucher(voucher);
				if (pettyCashReceiptDTO.getSkipapproval().equalsIgnoreCase("No")) {

					// Voucher Log
					voucherLog.setStatus(VoucherStatus.SUBMITTED);
					// UserMaster userMaster =
					// userMasterRepository.findOne(loginService.getCurrentUser().getId());
					UserMaster userMaster = userMasterRepository.findOne(pettyCashReceiptDTO.getForwardTo().getId());
					voucherLog.setUserMaster(userMaster);

					// Voucher Note
					voucherNote.setNote(pettyCashReceiptDTO.getNote().toString());
					voucherNote.setFinalApproval(pettyCashReceiptDTO.getFinalApproval());
					if (pettyCashReceiptDTO.getForwardTo() != null) {
						UserMaster forwardTo = userMasterRepository.findOne(pettyCashReceiptDTO.getForwardTo().getId());
						voucherNote.setForwardTo(forwardTo);
					}
					voucherNote.setCreatedBy(userMaster);
					voucherNote.setCreatedByName(userMaster.getUsername());
					voucherNote.setCreatedDate(new Date());
				} else {
					// Voucher Log
					// voucherLog.setVoucher(voucher);
					voucherLog.setStatus(VoucherStatus.FINALAPPROVED);
					UserMaster userMaster = userMasterRepository.findOne(loginService.getCurrentUser().getId());
					voucherLog.setUserMaster(userMaster);
					// voucher.getVoucherLogList().add(voucherLog);
					// Voucher Note
					voucherNote.setNote("Self Approved");
					voucherNote.setFinalApproval(true);
					UserMaster currentUser = loginService.getCurrentUser();
					if (currentUser != null && currentUser.getId() != null) {
						log.info("forward id==> " + currentUser.getId());
						UserMaster userMasterObj = userMasterRepository.findOne(currentUser.getId());
						voucherNote.setForwardTo(userMasterObj);
						voucherNote.setCreatedBy(userMasterObj);
						voucherNote.setCreatedByName(userMasterObj.getUsername());
						voucherNote.setCreatedDate(new Date());
					}

				}
				voucher.getVoucherLogList().add(voucherLog);
				voucherNote.setVoucher(voucher);
				voucherNoteList.add(voucherNote);
				voucher.setVoucherNote(voucherNoteList);
				// voucher.setNarration("");

				// save both 1st and 2nd voucher_details
				Voucher maxVoucher = voucherRepository.save(voucher);
				// log.info("############### Max Voucher Id1 ############
				// "+voucherRepository.save(voucher).getId());
				log.info("############### Max Voucher Id ############ " + maxVoucher.getId());

				// paymentDetailsRepository.save(receiptDetailsList);
				// AppUtil.infoLogStatement("MaxVoucherId : " + savedVoucher.getId());
				// 3 table payments

				Payment payment = new Payment();
				payment.setPaymentNumberPrefix(referenceNumber);
				payment.setPaymentNumber(Long.valueOf(numericReferenceNumber));
				payment.setEntityMaster(entityMaster);
				payment.setNarration(pettyCashReceiptDTO.getVoucherNarration());
				payment.setCreatedBy(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
				payment.setCreatedDate(new Date());
				if (pettyCashReceiptDTO.getForEntityMaster() != null) {
					EntityMaster forEntityMaster = entityMasterRepository
							.findById(pettyCashReceiptDTO.getForEntityMaster().getId());
					payment.setForEntityMaster(forEntityMaster);
				}
				paymentRepository.save(payment);

				// 4th table payment_details
				Double atAmount = 0.0;
				for (PettyCashReceiptDTO dto : pettyCashReceiptDTO.getPettyCashDocumentDTOList()) {
					PaymentDetails paymentDetails = new PaymentDetails();
					paymentDetails.setPayment(payment);
					paymentDetails.setBankReferenceNumber(dto.getDocumentnumber());
					paymentDetails.setDocumentDate(dto.getDocumentDate());
					paymentDetails.setAmount(dto.getExpenseAmount());
					paymentDetails.setVoucher(voucher);
					paymentDetails.setPaymentCategory(paymentCategory);
					atAmount = atAmount + dto.getExpenseAmount();
					if (dto.getEntityBranchId() != null && dto.getEntityBranchId() != 0) {
						EntityBankBranch entityBankBranch = entityBankBranchRepository.getOne(dto.getEntityBranchId());
						paymentDetails.setEntityBankBranch(entityBankBranch);
					}

					log.info("############### Payment Code ############ " + dto.getPaymentMode().getCode());
					// PaymentMethod
					// pmethod=paymentMethodRepository.findByCode(dto.getPaymentMode().getCode());
					PaymentMethod pmethod = paymentMethodRepository.getPaymentByCode(dto.getPaymentMode().getCode());
					log.info("############### Max Voucher sdsdsdsd ###x######## 0" + pmethod.getCode());
					paymentDetails.setPaymentMethod(paymentMethodRepository.findByCode(dto.getPaymentMode().getCode()));
					paymentDetails.setPaymentTypeMaster(paymentTypeMasterRepository.findByName(dto.getReceiveFrom()));
					paymentDetails.setCreatedBy(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
					paymentDetails.setCreatedDate(new Date());
					if (dto.getBankCode() != null) {
						BankMaster bankMaster = bankMasterRepository.getBankDetailsByCode(dto.getBankCode());
						paymentDetails.setBankMaster(bankMaster);

					}

					paymentDetailsRepository.save(paymentDetails);
				}
				AccountTransaction accountTransaction = new AccountTransaction();
				accountTransaction.setPayment(payment);
				accountTransaction.setReferenceNumber(maxVoucher.getId());
				accountTransaction.setName("");
				accountTransaction.setCreatedBy(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
				accountTransaction.setCreatedDate(new Date());
				accountTransactionRepository.save(accountTransaction);
				SectionMaster secMaster = new SectionMaster();
				if (pettyCashReceiptDTO.getSectionMaster() != null) {
					secMaster = sectionMasterRepository.getByCode(pettyCashReceiptDTO.getSectionMaster().getCode());
				}

				int index = 0;

				for (PettyCashReceiptDTO value : pettyCashReceiptDTO.getReceiptDetails()) {
					AccountTransactionDetails accountTransactionDetails = new AccountTransactionDetails();
					accountTransactionDetails.setAccountTransaction(accountTransaction);
					accountTransactionDetails.setSequenceNumber(1L);
					accountTransactionDetails.setAccountAspect(value.getPostingtype());
					accountTransactionDetails.setTransactionDate(new Date());
					accountTransactionDetails.setAmount(value.getExpenseAmount());
					accountTransactionDetails.setVoucherDescription1(value.getDescription());
					accountTransactionDetails.setReceiptNumber(referenceNumber);
					accountTransactionDetails.setPaymentId(payment.getId());

					if (index == 0) {

						String paymentModeCode = pettyCashReceiptDTO.getPaymentMode() != null
								? pettyCashReceiptDTO.getPaymentMode().getCode()
								: null;

						if (StringUtils.isNotEmpty(paymentModeCode)) {
							if ("ADJUSTMENT".equalsIgnoreCase(paymentModeCode)) {
								accountTransactionDetails.setCashAmount(0D);
								accountTransactionDetails.setChequeAmount(0D);
							}else if ("cash".equalsIgnoreCase(paymentModeCode)) {
								accountTransactionDetails.setCashAmount(atAmount);
							} else {
								accountTransactionDetails.setChequeAmount(atAmount);
							}
						} else {
							log.info("Payment mode is empty.");
						}
					}

					if (value.getPostingEmployee() != null) {
						EmployeeMaster empMaster = employeeMasterRepository.findOne(value.getPostingEmployee().getId());
						accountTransactionDetails.setEmpMaster(empMaster);
					}

					if (value.getPostingEntityMaster() != null) {
						EntityMaster em = entityMasterRepository.findById(value.getPostingEntityMaster().getId());
						accountTransactionDetails.setEntityMaster(em);
					}

					accountTransactionDetails.setSectionCode(secMaster.getCode());

					if (value.getAccount() != null) {
						accountTransactionDetails.setGlAccount(glAccountRepository.findOne(value.getAccount().getId()));
					}
					accountTransactionDetails
							.setCreatedBy(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
					accountTransactionDetails.setCreatedDate(new Date());
					accountTransactionDetailsRepository.save(accountTransactionDetails);
					index++;
				}

				sequenceConfig.setCurrentValue(sequenceConfig.getCurrentValue() + 1);

				sequenceConfigRepository.save(sequenceConfig);
				log.info("Start PettycashService.saveCashReceipt");

				String VIEW_PAGE = "/pages/finance/expenseTransaction/viewReceiptTransaction.xhtml?faces-redirect=true;";
				Map<String, Object> additionalData = new HashMap<>();
				additionalData.put("Url", VIEW_PAGE + "&id=" + maxVoucher.getId() + "&");
				notificationEmailService.sendMailAndNotificationForForward(voucherNote, voucherLog, additionalData);

			}
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("PettyCashReceiptService createPettyCashExpense RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("PettyCashReceiptService createPettyCashExpense Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO updatePaymentDetails(PettyCashReceiptDTO pettyCashReceipt) {

		BaseDTO baseDTO = new BaseDTO();
		try {

			Date paymentDate = pettyCashReceipt.getPaymentReceiptDate();
			Long voucherId = 0L;
			Long paymentId = 0L;

			for (PettyCashReceiptDTO dto : pettyCashReceipt.getPettyCashDocumentDTOList()) {

				PaymentDetails pd = paymentDetailsRepository.getOne(dto.getPaymentId());
				voucherId = pd.getVoucher().getId();
				paymentId = pd.getPayment().getId();
				pd.setPayment(paymentRepository.findOne(pd.getPayment().getId()));
				pd.setBankReferenceNumber(dto.getDocumentnumber());
				pd.setDocumentDate(dto.getDocumentDate());
				pd.setAmount(dto.getExpenseAmount());
				pd.setVoucher(voucherRepository.findOne(pd.getVoucher().getId()));
				pd.setPaymentCategory(pd.getPaymentCategory());
				pd.setAmount(dto.getExpenseAmount());
				pd.setBankMaster(null);
				if (dto.getBankCode() != null) {

					if (dto.getBankCode().contains("/")) {
						String bankCode = dto.getBankCode().split("/")[0];
						log.info("<++++++++++++++++ banCode+++++++++++++++> : " + bankCode);
						BankMaster bm = bankMasterRepository.getBankDetailsByCode(bankCode);
						pd.setBankMaster(bm);
					}
				}

				if (dto.getEntityBranchId() != null) {
					EntityBankBranch entityBankBranch = entityBankBranchRepository.getOne(dto.getEntityBranchId());
					pd.setEntityBankBranch(entityBankBranch);
				} else {
					if (pd.getEntityBankBranch() != null) {
						EntityBankBranch entityBankBranch = entityBankBranchRepository
								.getOne(pd.getEntityBankBranch().getId());
						pd.setEntityBankBranch(entityBankBranch);
					}
				}

				pd.setPaymentMethod(paymentMethodRepository.findByCode(pettyCashReceipt.getPaymentMode().getCode()));
				pd.setPaymentTypeMaster(paymentTypeMasterRepository.findByName(pd.getPaymentTypeMaster().getName()));
				pd.setCreatedBy(userMasterRepository.findOne(loginService.getCurrentUser().getId()));

				pd.setCreatedDate(paymentDate);

				if (dto.getBankCode() != null) {
					if (!dto.getBankCode().contains("/")) {
						BankMaster bankMaster = bankMasterRepository.getBankDetailsByCode(dto.getBankCode());
						pd.setBankMaster(bankMaster);
					}

				}

				paymentDetailsRepository.save(pd);

			}

			Voucher voucher = voucherRepository.findOne(voucherId);
			voucher.setCreatedDate(paymentDate);
			voucher.setFromDate(paymentDate);
			// voucher.setToDate(paymentDate);
			if (pettyCashReceipt.getPaymentMode() != null) {
				voucher.setPaymentMode(paymentModeRepository.findByCode(pettyCashReceipt.getPaymentMode().getCode()));
			}
			Voucher updatedVoucher = voucherRepository.save(voucher);

			VoucherLog voucherLog = new VoucherLog();

			VoucherNote voucherNote = new VoucherNote();
			voucherLog.setVoucher(updatedVoucher);
			voucherNote.setVoucher(updatedVoucher);
			/*
			 * if (pettyCashReceipt.getSkipapproval().equalsIgnoreCase("No")) {
			 * 
			 * // Voucher Log voucherLog.setStatus(VoucherStatus.SUBMITTED); // UserMaster
			 * userMaster =
			 * userMasterRepository.findOne(pettyCashReceipt.getForwardTo().getId()); //
			 * voucherLog.setUserMaster(userMaster);
			 * 
			 * // Voucher Note
			 * ///voucherNote.setNote(pettyCashReceipt.getNote().toString());
			 * voucherNote.setFinalApproval(pettyCashReceipt.getFinalApproval()); if
			 * (pettyCashReceipt.getForwardTo() != null) { UserMaster forwardTo =
			 * userMasterRepository.findOne(pettyCashReceipt.getForwardTo().getId());
			 * voucherNote.setForwardTo(forwardTo); } voucherNote.setCreatedBy(userMaster);
			 * voucherNote.setCreatedByName(userMaster.getUsername());
			 * voucherNote.setCreatedDate(new Date()); } else {
			 * voucherLog.setStatus(VoucherStatus.FINALAPPROVED); UserMaster userMaster =
			 * userMasterRepository.findOne(loginService.getCurrentUser().getId());
			 * voucherLog.setUserMaster(userMaster); //
			 * voucher.getVoucherLogList().add(voucherLog); // Voucher Note
			 * voucherNote.setNote("Self Approved"); voucherNote.setFinalApproval(true);
			 * UserMaster currentUser = loginService.getCurrentUser(); if (currentUser !=
			 * null && currentUser.getId() != null) { log.info("forward id==> " +
			 * currentUser.getId()); UserMaster userMasterObj =
			 * userMasterRepository.findOne(currentUser.getId());
			 * voucherNote.setForwardTo(userMasterObj);
			 * voucherNote.setCreatedBy(userMasterObj);
			 * voucherNote.setCreatedByName(userMasterObj.getUsername());
			 * voucherNote.setCreatedDate(new Date()); }
			 * 
			 * }
			 */

			// Voucher Log
			// VoucherLog updatedVoucherLog = voucherLogRepository.save(voucherLog);

			// Voucher Note
			// VoucherNote updatedVoucherNote = voucherNoteRepository.save(voucherNote);

			// voucher details

			List<VoucherDetails> voucherDetails = voucherDetailsRepository.findVoucherDetailsByVoucherId(voucherId);

			for (VoucherDetails vd : voucherDetails) {
				vd.setCreatedDate(paymentDate);
				voucherDetailsRepository.save(vd);
			}

			// payment

			Payment payment = paymentRepository.getOne(paymentId);
			payment.setCreatedDate(paymentDate);
			paymentRepository.save(payment);

			// Account transaction

			AccountTransaction accountTransaction = accountTransactionRepository
					.findAccountTransactionByPaymentId(paymentId);
			if (accountTransaction != null) {
				accountTransaction.setCreatedDate(paymentDate);
				accountTransactionRepository.save(accountTransaction);

				List<AccountTransactionDetails> accountTransactionDetails = accountTransactionDetailsRepository
						.findAccountTransactionDetailsByTransactionId(accountTransaction.getId());

				for (AccountTransactionDetails atd : accountTransactionDetails) {
					atd.setCreatedDate(paymentDate);
					accountTransactionDetailsRepository.save(atd);

				}
			}
			// Notification
			String VIEW_PAGE = "/pages/finance/expenseTransaction/viewReceiptTransaction.xhtml?faces-redirect=true;";
			Map<String, Object> additionalData = new HashMap<>();
			additionalData.put("Url", VIEW_PAGE + "&id=" + updatedVoucher.getId() + "&");
			// notificationEmailService.sendMailAndNotificationForForward(voucherNote,
			// voucherLog, additionalData);

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (Exception e) {

			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.error("PettyCashReceiptService createPettyCashExpense Exception ", e);
		}

		return baseDTO;
	}

	/*
	 * public BaseDTO getCustomerTypeLoading(Long id) {
	 * log.info("<-Inside SERVICE-Starts CustomerTypeMaster-->"); BaseDTO baseDTO =
	 * new BaseDTO(); try {
	 * 
	 * List<CustomerTypeMaster> customertypelist =
	 * customerTypeMasterRepository.getCustomerTypeLoading(id);
	 * baseDTO.setResponseContent(customertypelist);
	 * baseDTO.setTotalRecords(customertypelist.size());
	 * baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
	 * log.info("<-SupplierMaster GetAll Data Success-->"); } catch (Exception
	 * exception) { log.error("exception Occured SupplierMaster : ", exception);
	 * baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode()); }
	 * return baseDTO; }
	 */

	public String getReferenceNumber(String name, Long voucherTypeId, String currentYear, String entityCode) {
		String ReferenceNumber = null;

		try {

			ApplicationQuery appQuery = applicationQueryRepository
					.findByQueryName("PETTY_CASH_RECEIPT_AND_PAYMENT_REFERENCE");
			// String referenceQuery="select * from (SELECT
			// SUBSTRING(st.reference_number_prefix,10,2) as
			// year,SUBSTRING_INDEX(SUBSTRING_INDEX(st.reference_number_prefix,'-',1),'R',-1)
			// as
			// entitycode,SUBSTRING_INDEX(SUBSTRING_INDEX(st.reference_number_prefix,'-',-1),'-',-1)
			// as count,SUBSTRING(SUBSTRING_INDEX(st.reference_number_prefix,'-',1),1,1) as
			// vtype from (select trim(st.reference_number_prefix) as
			// reference_number_prefix from voucher st where st.id in (select max(id) from
			// voucher where voucher_type_id=4 and name='PETTY_RECEIPT')) as st) ar where
			// ar.year=20 and ar.entitycode=10";
			// String referenceQuery = "select entitycodeandyear,count,name,voucher_type_id
			// from (select
			// split_part(substring(reference_number_prefix,2,length(reference_number_prefix)),'-',1)
			// ||substring(split_part(reference_number_prefix,'-',2),6,2) as
			// entitycodeandyear,split_part(reference_number_prefix,'-',3) as
			// count,reference_number_prefix,id,\"name\",voucher_type_id from voucher order
			// by id desc limit 1) as t where t.entitycodeandyear=':entitycodeandyear' and
			// t.voucher_type_id=:voucher_type_id and t.name=':name'";
			if (appQuery != null) {
				String referenceQuery = appQuery.getQueryContent().trim();
				referenceQuery = referenceQuery.replace(":currentYear", currentYear);
				referenceQuery = referenceQuery.replace(":voucherTypeId", voucherTypeId.toString());
				referenceQuery = referenceQuery.replace(":name", name);
				referenceQuery = referenceQuery.replace(":entityCode", entityCode);
				log.info(" Reference Query : " + referenceQuery);
				Map<String, Object> data = jdbcTemplate.queryForMap(referenceQuery);
				log.info(" Reference data : " + data);
				if (data != null) {
					if (data.get("count") != null) {
						log.info(" Count : " + data.get("count"));
						int maxValue = Integer.valueOf(data.get("count").toString());
						log.info(" maxValue : " + maxValue);
						maxValue = maxValue + 1;
						ReferenceNumber = String.valueOf(maxValue);
					} else {
						ReferenceNumber = "1";
					}
					if (data.get("entitycodeandyear") != null) {
						log.info(" entitycodeandyear : " + data.get("entitycodeandyear"));
					}
					if (data.get("name") != null) {
						log.info(" name : " + data.get("name"));
					}
					if (data.get("voucher_type_id") != null) {
						log.info(" voucher_type_id : " + data.get("voucher_type_id"));
					}
				} else {
					ReferenceNumber = "1";
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			log.error("PettyCashReceiptService getReferenceNumber Exception ", e.getMessage());
		}

		return ReferenceNumber;
	}

	// To getAll Receipt Transaction Details
	public BaseDTO loadAllReceiptTransactionDetails() {
		BaseDTO baseDTO = new BaseDTO();
		try {
			EntityMaster entityMaster = entityMasterRepository.findByUserRegion(loginService.getCurrentUser().getId());
			List<PettyCashReceiptDTO> pettyCashReceiptTransactionDetails = new ArrayList<PettyCashReceiptDTO>();
			String Query = "\r\n" + "\r\n"
					+ "select p.id,p.payment_number_prefix,sum(pd.amount) as amount,p.created_date,vt.code,vl.status from payment_details pd\r\n"
					+ "join payment p on p.id=pd.payment_id\r\n"
					+ "join account_transaction act on act.payment_id=p.id\r\n"
					+ "join account_transaction_details atd on atd.account_transaction_id=act.id\r\n"
					+ "join voucher v on v.id=pd.voucher_id and v.entity_id=p.entity_id\r\n"
					+ "join voucher_details vd on vd.voucher_id=v.id\r\n"
					+ "join voucher_note vn on vn.voucher_id =v.id\r\n"
					+ "join voucher_log vl on vl.voucher_id  =v.id\r\n"
					+ "join voucher_type vt on vt.id = v.voucher_type_id where v.entity_id=" + entityMaster.getId()
					+ "\r\n" + " group by p.payment_number_prefix,p.created_date,vt.code,vl.status,p.id;\r\n" + "";
			log.info("< =========== Receipt Query ============== >  sql : " + Query);
			List<Map<String, Object>> dataList = jdbcTemplate.queryForList(Query);

			for (Map<String, Object> data : dataList) {
				PettyCashReceiptDTO pettyCashReceiptDTO = new PettyCashReceiptDTO();

				String referenceNumber = data.get("payment_number_prefix") != null
						? data.get("payment_number_prefix").toString()
						: "-";

				String amount = data.get("amount") != null ? data.get("amount").toString() : "0.00";

				String paymentDate = data.get("created_date") == null ? null : data.get("created_date").toString();

				if (paymentDate != null) {
					paymentDate = AppUtil.changeStringDateFormat("dd-MMM-yyyy", paymentDate);
				}

				String paymentType = data.get("code") != null ? data.get("code").toString() : "-";

				String status = data.get("status") != null ? data.get("status").toString() : "-";

				String id = data.get("id") != null ? data.get("id").toString() : "0";
				pettyCashReceiptDTO.setStatus(status.toUpperCase());
				pettyCashReceiptDTO.setExpenseAmount(Double.valueOf(amount));
				pettyCashReceiptDTO.setExpenseTransactionName(paymentType.toUpperCase());
				pettyCashReceiptDTO.setRemarks(referenceNumber);
				pettyCashReceiptDTO.setCreatedBy(paymentDate);
				pettyCashReceiptDTO.setId(Long.valueOf(id));

				pettyCashReceiptTransactionDetails.add(pettyCashReceiptDTO);
			}
			baseDTO.setResponseContents(pettyCashReceiptTransactionDetails);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (Exception e) {
			// TODO: handle exception
			log.error("PettyCashReceiptService loadReceiptTransactionDetails Exception ", e.getMessage());
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}

		return baseDTO;
	}

	// getAll Receipt Transaction Lazy List
	public BaseDTO loadAllReceiptTransactionDetails(PaginationDTO request) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			Integer total = 0;
			Integer start = request.getFirst(), pageSize = request.getPageSize();
			start = start * pageSize;
			String pageType = request.getPageType();
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();
			// ApplicationQuery applicationQuery =
			// applicationQueryRepository.findByQueryName("PETTY_CASH_EXPENSE_LIST");
			// EntityMaster entityMaster =
			// entityMasterRepository.findByUserRegion(loginService.getCurrentUser().getId());
			EmployeePersonalInfoEmployment employment = employeePersonalInfoEmploymentRepository
					.findWorkLocationByUserId(loginService.getCurrentUser().getId());
			// String mainQuery = applicationQuery.getQueryContent().trim();
			// String mainQuery = "select * from (select v.id as
			// voucherId,p.payment_number_prefix as referenceNumber,sum(pd.amount)
			// receiptAmount,pd.created_date as createdDate,vt.code as voucherType,vl.status
			// as status from payment_details pd\r\n"
			// + "join payment p on p.id=pd.payment_id\r\n"
			// + "join account_transaction act on act.payment_id=p.id\r\n"
			// + "join voucher v on v.id=pd.voucher_id and v.entity_id=p.entity_id \r\n"
			// + "join voucher_log vl on vl.voucher_id =v.id\r\n"
			// + "join (select voucher_id,max(id) as id from voucher_log group by
			// voucher_id) vl1 on vl.id=vl1.id\r\n"
			// + "join voucher_type vt on vt.id = v.voucher_type_id where v.entity_id=" +
			// entityMaster.getId()
			// + "\r\n"
			// + "group by p.payment_number_prefix,pd.created_date,vt.code,vl.status,v.id)
			// as t where 1=1 :condition";
			//
			//
			// new changes for make payment

			// String mainQuery = "\r\n"
			// + "select * from (select v.id as voucherId,p.payment_number_prefix as
			// referenceNumber,sum(v.net_amount) receiptAmount,v.created_date as
			// createdDate,vt.code as voucherType,vl.status as status from payment p\r\n"
			// + "join account_transaction act on act.payment_id=p.id\r\n"
			// + "join voucher v on v.id=act.reference_number and v.entity_id=p.entity_id
			// and v.name in('PETTY_CASH_PAYMENT','PETTY_RECEIPT')\r\n"
			// + "join voucher_log vl on vl.voucher_id =v.id\r\n"
			// + "join (select voucher_id,max(id) as id from voucher_log group by
			// voucher_id) vl1 on vl.id=vl1.id\r\n"
			// + "join voucher_type vt on vt.id = v.voucher_type_id where v.entity_id=" +
			// entityMaster.getId()
			// + " \r\n"
			// + "group by p.payment_number_prefix,v.created_date,vt.code,vl.status,v.id) as
			// t where 1=1 :condition\r\n"
			// + "";

			// String mainQuery="select * from (select v.id as
			// voucherId,p.payment_number_prefix as referenceNumber,sum(v.net_amount)
			// receiptAmount,v.created_date as createdDate,vt.code as voucherType,vl.status
			// as status,vn.forward_to as forwardTo from payment p join account_transaction
			// act on act.payment_id=p.id join voucher v on v.id=act.reference_number and
			// v.entity_id=p.entity_id and v.name in('PETTY_CASH_PAYMENT','PETTY_RECEIPT')
			// join voucher_log vl on vl.voucher_id =v.id join (select voucher_id,max(id) as
			// id from voucher_log group by voucher_id) vl1 on vl.id=vl1.id join
			// voucher_note vn on vn.voucher_id=v.id join (select max(vn1.id) as id from
			// voucher_note vn1 join voucher v1 on v1.id=vn1.voucher_id where
			// (v1.created_by="+createdUserId+" or vn1.forward_to="+createdUserId+") group
			// by vn1.voucher_id) vn1 on vn1.id=vn.id join voucher_type vt on vt.id =
			// v.voucher_type_id where (v.created_by="+createdUserId+" or
			// vn.forward_to="+createdUserId+") group by
			// p.payment_number_prefix,v.created_date,vt.code,vl.status,v.id,vn.forward_to)
			// as t where 1=1 :condition";

			// String mainQuery="select * from (select v.id as
			// voucherId,p.payment_number_prefix as referenceNumber,sum(v.net_amount)
			// receiptAmount,v.created_date as createdDate,vt.code as voucherType,vl.status
			// as status from payment p join account_transaction act on act.payment_id=p.id
			// join voucher v on v.id=act.reference_number and v.entity_id=p.entity_id and
			// v.name in('PETTY_CASH_PAYMENT','PETTY_RECEIPT') join voucher_log vl on
			// vl.voucher_id =v.id join (select voucher_id,max(id) as id from voucher_log
			// group by voucher_id) vl1 on vl.id=vl1.id join voucher_type vt on vt.id =
			// v.voucher_type_id group by
			// p.payment_number_prefix,v.created_date,vt.code,vl.status,v.id) as t where 1=1
			// :condition";

			EntityMaster workLocation = employment.getWorkLocation();
			log.info("workLocation", workLocation.getId());

			String mainQuery = null;
			if (VoucherTypeCons.ADJUSTMENT.equals(pageType)) {
				mainQuery = "select * from (select v.id as voucherId,v.reference_number_prefix as referenceNumber,sum(v.net_amount) receiptAmount,v.created_date as createdDate,vt.code as voucherType,vl.status as status from voucher v join voucher_log vl on vl.voucher_id =v.id join (select voucher_id,max(id) as id from voucher_log group by voucher_id) vl1 on vl.id=vl1.id join voucher_type vt on vt.id = v.voucher_type_id where v.name in('PETTY_CASH_PAYMENT','PETTY_RECEIPT','PETTY_EXPENSE_PAYMENT') and vt.code='Payment' and v.entity_id="
						+ workLocation.getId()
						+ " group by v.reference_number_prefix,v.created_date,vt.code,vl.status,v.id)as t where 1=1 :condition";
			} else {
				mainQuery = "select * from (select v.id as voucherId,v.reference_number_prefix as referenceNumber,sum(v.net_amount) receiptAmount,v.created_date as createdDate,vt.code as voucherType,vl.status as status from voucher v join voucher_log vl on vl.voucher_id =v.id join (select voucher_id,max(id) as id from voucher_log group by voucher_id) vl1 on vl.id=vl1.id join voucher_type vt on vt.id = v.voucher_type_id where v.name in('PETTY_CASH_PAYMENT','PETTY_RECEIPT','PETTY_EXPENSE_PAYMENT') and v.entity_id="
						+ workLocation.getId()
						+ " group by v.reference_number_prefix,v.created_date,vt.code,vl.status,v.id)as t where 1=1 :condition";
			}

			/*
			 * EntityMaster entityMaster =
			 * entityMasterRepository.findByUserRegion(loginService.getCurrentUser().getId()
			 * ); String condition=""; if(entityMaster!=null && entityMaster.getId()!=null)
			 * { condition +=
			 * " and (v.entity_id="+entityMaster.getId()+" or vn.forward_to="+loginService.
			 * getCurrentUser().getId()+")" ; }
			 */

			String condition = "";

			if (request.getFilters() != null) {
				if (request.getFilters().get("sectionCodeName") != null) {
					condition += " and t.referenceNumber like '%" + request.getFilters().get("sectionCodeName") + "%'";
				}
				if (request.getFilters().get("expenseAmount") != null) {
					condition += " and  t.receiptAmount in (" + request.getFilters().get("expenseAmount") + ")";
				}
				if (request.getFilters().get("expenseTypeName") != null) {
					condition += " and t.voucherType like '%" + request.getFilters().get("expenseTypeName") + "%'";
				}
				if (request.getFilters().get("createdDate") != null) {
					Date createdDate = new Date((Long) request.getFilters().get("createdDate"));
					condition += " and t.createdDate::date = date '" + createdDate + "'";
				}
				if (request.getFilters().get("status") != null) {
					condition += " and t.status='" + request.getFilters().get("status") + "'";
				}
			}

			mainQuery = mainQuery.replace(":condition", condition);
			String countQuery = mainQuery.replace("select * ", "select count(*) ");

			log.info("count query... " + countQuery);
			countData = jdbcTemplate.queryForList(countQuery);
			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					total = Integer.parseInt(data.get("count").toString().replace(",", ""));
			}

			log.info("total======>" + total);

			if (request.getSortField() == null)
				mainQuery += " order by voucherId desc limit " + pageSize + " offset " + start + ";";

			if (request.getSortField() != null && request.getSortOrder() != null) {
				log.info("Sort Field:[" + request.getSortField() + "] Sort Order:[" + request.getSortOrder() + "]");
				if (request.getSortField().equals("sectionCodeName") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by t.referenceNumber asc ";
				if (request.getSortField().equals("sectionCodeName") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by t.referenceNumber desc ";
				if (request.getSortField().equals("expenseAmount") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by t.receiptAmount asc  ";
				if (request.getSortField().equals("expenseAmount") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by t.receiptAmount desc  ";
				if (request.getSortField().equals("expenseTypeName") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by t.voucherType asc ";
				if (request.getSortField().equals("expenseTypeName") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by t.voucherType desc ";
				if (request.getSortField().equals("createdDate") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by t.createdDate asc  ";
				if (request.getSortField().equals("createdDate") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by t.createdDate desc  ";
				if (request.getSortField().equals("status") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by t.status asc  ";
				if (request.getSortField().equals("status") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by t.status desc  ";
				mainQuery += " limit " + pageSize + " offset " + start + ";";
			}
			log.info("Main Qury....." + mainQuery);
			List<PettyCashReceiptDTO> pettyCashExpenseDTOList = new ArrayList<>();
			listofData = jdbcTemplate.queryForList(mainQuery);
			for (Map<String, Object> data : listofData) {
				PettyCashReceiptDTO pettyCashReceiptDTO = new PettyCashReceiptDTO();
				Long voucherId = 0L;
				if (data.get("voucherId") != null) {
					voucherId = Long.parseLong(data.get("voucherId").toString());
					pettyCashReceiptDTO.setId(voucherId);
				}
				if (data.get("referenceNumber") != null)
					pettyCashReceiptDTO.setSectionCodeName(data.get("referenceNumber").toString());
				if (data.get("receiptAmount") != null) {
					Double receiptAmount = paymentDetailsRepository.getPaymentAmountByVoucherId(voucherId);
					receiptAmount = receiptAmount == null ? 0.0 : receiptAmount;
					pettyCashReceiptDTO.setExpenseAmount(receiptAmount);
				}
				if (data.get("voucherType") != null)
					pettyCashReceiptDTO.setExpenseTypeName(data.get("voucherType").toString());
				if (data.get("status") != null)
					pettyCashReceiptDTO.setStatus(data.get("status").toString());
				if (data.get("createdDate") != null)
					pettyCashReceiptDTO.setCreatedDate((Date) data.get("createdDate"));
				pettyCashExpenseDTOList.add(pettyCashReceiptDTO);
			}
			log.info("Returning PettyCashExpense list...." + pettyCashExpenseDTOList);
			log.info("Total records present in PettyCashExpense..." + total);
			baseDTO.setResponseContent(pettyCashExpenseDTOList);
			baseDTO.setTotalRecords(total);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception exp) {
			log.error("Exception Cause  : ", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public Long checkBankDetailsAlreadyExistorNot(String referenceNumber, String bankMasterCode,
			String paymentModeCode) {
		long count = 0;
		try {
			long bankMasterId = 0;
			if (bankMasterCode != null) {
				BankMaster bankMaster = bankMasterRepository.getBankDetailsByCode(bankMasterCode);
				bankMasterId = bankMaster.getId();
			}
			PaymentMethod paymentMode = paymentMethodRepository.findByCode(paymentModeCode);
			log.info("<========== Reference No ===========> : " + referenceNumber + " BankCode : " + bankMasterCode
					+ " PaymentMode : " + paymentMode + " BankId : " + bankMasterId);
			count = voucherRepository.getBankDetailsCount(referenceNumber, paymentMode.getId(), bankMasterId);
			log.info("<========== count1 ===========> : " + count);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return count;
	}

	public BaseDTO getDetailsByVoucherId(Long voucherId, Long notificationId) {
		BaseDTO baseDTO = new BaseDTO();
		PettyCashReceiptDTO pettyCashReceiptDTO = new PettyCashReceiptDTO();
		log.info("<========== PettyCashReceiptService : getDetailsByVoucherId Methodm ===========> : " + voucherId);
		try {
			List<Map<String, Object>> queryData = new ArrayList<Map<String, Object>>();
			ApplicationQuery applicationQuery = applicationQueryRepository
					.findByQueryName("PETTY_CASH_RECEIPT_AND_PAYMENT_DETAILS");
			log.info("Query : " + applicationQuery.getQueryContent() + "  : id : " + applicationQuery);
			if (applicationQuery == null || applicationQuery.getId() == null) {
				log.info("Application Query not found for query name : " + applicationQuery);
				baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
				return baseDTO;
			}
			String query = applicationQuery.getQueryContent().trim();
			query = query.replace(":voucherId", voucherId.toString());
			log.info("Query Content For Petty Cash Receipt and Payment View query : " + query);
			queryData = jdbcTemplate.queryForList(query);
			for (Map<String, Object> queryDataMap : queryData) {

				String paymentTypeMasterName = null;

				if (queryDataMap.get("acTranId") != null) {
					Long acTransactionId = Long.valueOf(queryDataMap.get("acTranId").toString());
					pettyCashReceiptDTO.setExpenseId(acTransactionId);
				}

				if (queryDataMap.get("receiptDate") != null) {
					Date paymentReceiptDate = (Date) (queryDataMap.get("receiptDate"));
					pettyCashReceiptDTO.setPaymentReceiptDate(paymentReceiptDate);
				}

				if (queryDataMap.get("voucherId") != null) {
					Long voucherId1 = Long.valueOf(queryDataMap.get("voucherId").toString());
					pettyCashReceiptDTO.setId(voucherId1);
				}

				if (queryDataMap.get("createdBy") != null) {
					Long createdBy = Long.valueOf(queryDataMap.get("createdBy").toString());
					UserMaster um = userMasterRepository.findUserById(createdBy);
					pettyCashReceiptDTO.setUserMaster(um);
				}

				if (queryDataMap.get("status") != null) {
					String status = queryDataMap.get("status").toString();
					pettyCashReceiptDTO.setStatus(status);
				}

				if (queryDataMap.get("forEntityId") != null) {
					Long forEntityId = Long.valueOf(queryDataMap.get("forEntityId").toString());
					EntityMaster forEntityMaster = entityMasterRepository.findById(forEntityId);
					pettyCashReceiptDTO.setForEntityMaster(forEntityMaster);
				}

				if (queryDataMap.get("voucherNote") != null) {
					String voucherNote = queryDataMap.get("voucherNote").toString();
					pettyCashReceiptDTO.setNote(voucherNote);
				}
				if (queryDataMap.get("paymentType") != null) {
					String paymentType = queryDataMap.get("paymentType").toString();
					pettyCashReceiptDTO.setReceiveFrom(paymentType);
				}

				if (queryDataMap.get("sectionId") != null) {
					Long sectionId = Long.valueOf(queryDataMap.get("sectionId").toString());
					SectionMaster sectionMaster = sectionMasterRepository.findOne(sectionId);
					pettyCashReceiptDTO.setSectionMaster(sectionMaster);
				}

				if (queryDataMap.get("purchaseIvoiceId") != null) {
					Long purchaseIvoiceId = Long.valueOf(queryDataMap.get("purchaseIvoiceId").toString());
					PurchaseInvoice pi = purchaseInvoiceRepository.getByPurchaseInvoiceById(purchaseIvoiceId);
					pettyCashReceiptDTO.setPurchaseInvoice(pi);
				}

				if (queryDataMap.get("empId") != null) {
					paymentTypeMasterName = "EMPLOYEE";
					Long empId = Long.valueOf(queryDataMap.get("empId").toString());
					EmployeeMaster employeeMaster = employeeMasterRepository.findById(empId);
					EmployeeMaster employeeeMasterbj = new EmployeeMaster();
					employeeeMasterbj.setId(employeeMaster.getId());
					employeeeMasterbj.setFirstName(employeeMaster.getFirstName());
					employeeeMasterbj.setLastName(employeeMaster.getLastName());
					employeeeMasterbj.setEmpCode(employeeMaster.getEmpCode());
					pettyCashReceiptDTO.setEmployeeMaster(employeeeMasterbj);
				} else if (queryDataMap.get("customerId") != null) {
					paymentTypeMasterName = "CUSTOMER";
					Long customerId = Long.valueOf(queryDataMap.get("customerId").toString());
					CustomerMaster customerMaster = customerMasterRepository.findOne(customerId);
					pettyCashReceiptDTO.setCustomerMaster(customerMaster);
				} else if (queryDataMap.get("supplierId") != null) {
					paymentTypeMasterName = "SUPPLIER";
					Long supplierId = Long.valueOf(queryDataMap.get("supplierId").toString());
					SupplierMaster supplierMaster = supplierMasterRepository.findOne(supplierId);
					pettyCashReceiptDTO.setSupplierMaster(supplierMaster);
				} else if (queryDataMap.get("payFor") != null) {
					paymentTypeMasterName = "OTHERS";
					String payFor = queryDataMap.get("payFor").toString();
					pettyCashReceiptDTO.setPayFor(payFor);
				}
				pettyCashReceiptDTO.setReceiveFrom(paymentTypeMasterName);

				if (queryDataMap.get("paymentCategory") != null) {
					String paymentCategory = queryDataMap.get("paymentCategory").toString();
					pettyCashReceiptDTO.setExpenseTypeName(paymentCategory);
				}
				if (queryDataMap.get("voucherType") != null) {
					String transactionType = queryDataMap.get("voucherType").toString();
					pettyCashReceiptDTO.setTransactionType(transactionType);
				}
				if (queryDataMap.get("narration") != null) {
					String narration = queryDataMap.get("narration").toString();
					pettyCashReceiptDTO.setNarration(narration);
				}

				if (queryDataMap.get("paymentId") != null) {
					Long paymentId = Long.valueOf(queryDataMap.get("paymentId").toString());
					pettyCashReceiptDTO.setPaymentId(paymentId);
				}

				if (queryDataMap.get("finalApproval") != null) {
					String status = queryDataMap.get("finalApproval").toString();
					log.info("<=========== Status ================>  : " + status);
					if (status.equalsIgnoreCase("true")) {
						pettyCashReceiptDTO.setSkipapproval("Yes");
					} else {
						pettyCashReceiptDTO.setSkipapproval("NO");
					}
				}
				if (queryDataMap.get("paymentMode") != null) {
					String paymentMode = queryDataMap.get("paymentMode").toString();
					pettyCashReceiptDTO.setPaymentMode(paymentModeRepository.findByCode(paymentMode));
				}

			}

			// get posting details

			List<PettyCashReceiptDTO> postingDetails = new ArrayList<PettyCashReceiptDTO>();

			// String postingQuery = "select em.id as entityId,emp.id as
			// empId,atd.account_aspect,atd.amount,gl.code||' / '||gl.name as
			// glAccountHead,voucher_description_1 as description from
			// account_transaction_details atd left join account_transaction ats on
			// ats.id=atd.account_transaction_id left join entity_master em on
			// em.id=atd.entity_id left join emp_master emp on emp.id=atd.emp_id left join
			// gl_account gl on gl.id=atd.gl_account_id where
			// (ats.reference_number::integer)="
			// + voucherId;

			/*
			 * String postingQuery =
			 * "select em.id as entityId,emp.id as empId,atd.account_aspect,atd.amount,gl.code||' / '||gl.name as glAccountHead,voucher_description_1 as description from account_transaction_details atd join payment_details pd on pd.payment_id=atd.payment_id left join entity_master em on em.id=atd.entity_id left join emp_master emp on emp.id=atd.emp_id left join gl_account gl on gl.id=atd.gl_account_id where pd.voucher_id="
			 * + voucherId;
			 */

			String postingQuery = "select em.id as entityId,emp.id as empId,atd.account_aspect,atd.amount,gl.code || ' / ' || gl.name as glAccountHead,voucher_description_1 as description from\r\n"
					+ "account_transaction_details atd join (select payment_id,voucher_id from payment_details where voucher_id = "
					+ voucherId + " group by payment_id,voucher_id) pd on\r\n"
					+ "pd.payment_id = atd.payment_id left join entity_master em on em.id = atd.entity_id left join emp_master emp on emp.id = atd.emp_id\r\n"
					+ "left join gl_account gl on gl.id = atd.gl_account_id;";

			List<Map<String, Object>> postingQueryData = new ArrayList<Map<String, Object>>();
			postingQueryData = jdbcTemplate.queryForList(postingQuery);
			for (Map<String, Object> postingQueryDataMap : postingQueryData) {

				PettyCashReceiptDTO postingData = new PettyCashReceiptDTO();

				if (postingQueryDataMap.get("account_aspect") != null) {
					postingData.setPostingtype(postingQueryDataMap.get("account_aspect").toString());
				}

				if (postingQueryDataMap.get("amount") != null) {
					double postingAmount = Double.valueOf(postingQueryDataMap.get("amount").toString());
					postingData.setExpenseAmount(AppUtil.doubleFormat(postingAmount));
				}
				if (postingQueryDataMap.get("glAccountHead") != null) {
					postingData.setAccountCodeOrName(postingQueryDataMap.get("glAccountHead").toString());
				}
				if (postingQueryDataMap.get("description") != null) {
					postingData.setDescription(postingQueryDataMap.get("description").toString());
				}

				if (postingQueryDataMap.get("empId") != null) {
					Long empId = Long.valueOf(postingQueryDataMap.get("empId").toString());
					EmployeeMaster empMaster = employeeMasterRepository.findByemployeeId(empId);
					postingData.setPostingEmployee(empMaster);
				}

				if (postingQueryDataMap.get("entityId") != null) {
					Long entityId = Long.valueOf(postingQueryDataMap.get("entityId").toString());
					EntityMaster entityMaster = entityMasterRepository.findById(entityId);
					postingData.setPostingEntityMaster(entityMaster);
				}

				postingDetails.add(postingData);
			}
			pettyCashReceiptDTO.setReceiptDetails(postingDetails);

			// document Details

			List<PettyCashReceiptDTO> paymentDetails = new ArrayList<PettyCashReceiptDTO>();

			String paymentDetailsQuery = "select pd.bank_reference_number,pd.document_date,pd.bank_id,amount,pm.code,pd.entity_bank_branch_id,pd.id as paymentId,pd.created_date as paymentDate,pd.created_by as createdBy from payment_details pd join payment_method pm on pm.id=pd.payment_method_id where pd.voucher_id="
					+ voucherId;
			log.info("<====== paymentDetailsQuery =========> " + paymentDetailsQuery);
			List<Map<String, Object>> paymentQueryData = new ArrayList<Map<String, Object>>();
			paymentQueryData = jdbcTemplate.queryForList(paymentDetailsQuery);
			for (Map<String, Object> paymentQueryDataMap : paymentQueryData) {
				PettyCashReceiptDTO paymentData = new PettyCashReceiptDTO();

				if (paymentQueryDataMap.get("bank_reference_number") != null) {

					paymentData.setDocumentnumber(paymentQueryDataMap.get("bank_reference_number").toString());
				}

				if (paymentQueryDataMap.get("document_date") != null) {

					paymentData.setDocumentDate((Date) paymentQueryDataMap.get("document_date"));
				}
				if (paymentQueryDataMap.get("bank_id") != null) {
					long bankId = Long.valueOf(paymentQueryDataMap.get("bank_id").toString());
					BankMaster bm = bankMasterRepository.getOne(bankId);
					paymentData.setBankCode(bm.getBankCode() + "/" + bm.getBankName());
				}

				Long createdBy = 0L;
				if (paymentQueryDataMap.get("createdBy") != null) {
					createdBy = Long.valueOf(paymentQueryDataMap.get("createdBy").toString());
				}

				if (paymentQueryDataMap.get("entity_bank_branch_id") != null) {
					Long entityBranchId = Long.valueOf(paymentQueryDataMap.get("entity_bank_branch_id").toString());
					Map<String, Object> data = getBranchDetails(entityBranchId, createdBy);

					if (data.get("accountNumber") != null) {
						String accountNumber = data.get("accountNumber").toString();
						paymentData.setAccountNumber(accountNumber);
					}

					if (data.get("branchCodeName") != null) {
						String branchCodeName = data.get("branchCodeName").toString();
						paymentData.setBankBranch(branchCodeName);
					}
				}

				if (paymentQueryDataMap.get("paymentDate") != null) {

					paymentData.setPaymentReceiptDate((Date) paymentQueryDataMap.get("paymentDate"));
				}

				if (paymentQueryDataMap.get("paymentId") != null) {
					Long paymentId = Long.valueOf(paymentQueryDataMap.get("paymentId").toString());
					// EntityMaster forEntityMaster =entityMasterRepository.findById(forEntityId);
					// pettyCashReceiptDTO.setForEntityMaster(forEntityMaster);
					paymentData.setPaymentId(paymentId);
				}

				if (paymentQueryDataMap.get("code") != null) {
					paymentData.setPostingtype(paymentQueryDataMap.get("code").toString());
				}
				if (paymentQueryDataMap.get("amount") != null) {
					double paymentAmount = Double.valueOf(paymentQueryDataMap.get("amount").toString());
					paymentData.setExpenseAmount(AppUtil.doubleFormat(paymentAmount));
				}
				paymentDetails.add(paymentData);
			}

			// VIEW Note
			List<EmployeeMaster> approvalData = new ArrayList<>();

			VoucherNote voucherNote = voucherNoteRepository.findByVoucherId(voucherId);
			if (voucherNote != null) {
				pettyCashReceiptDTO.setStage(voucherNote.getFinalApproval());
			}

			// Submitted
			VoucherLog submittedVoucherLog = voucherLogRepository.getByVoucherId(voucherId, "SUBMITTED");
			if (submittedVoucherLog != null) {
				Long submitid = submittedVoucherLog.getCreatedBy().getId();
				EmployeeMaster employeeMaster = employeeMasterRepository.findByUserId(submitid);
				employeeMaster.setAchievementList(null);
				employeeMaster.setAdditionalChargeList(null);
				employeeMaster.setContactDetails(null);
				employeeMaster.setDepartment(null);
				employeeMaster.setDisciplinaryActionList(null);
				employeeMaster.setEducationList(null);
				employeeMaster.setEmpExperienceDetailsList(null);
				employeeMaster.setEmployeeDisplayName(null);
				employeeMaster.setEmpSuspensionDetailsList(null);
				employeeMaster.setIncrementDetailsList(null);
				employeeMaster.setEmployeePayscaleInformationList(null);
				employeeMaster.setEmployeeLeaveDeatailsList(null);
				employeeMaster.setEmployeeLoanAndAdvanceDetailsList(null);
				employeeMaster.setEmployeePromotionDetails(null);
				employeeMaster.setEmployeeScannedDocumentList(null);
				employeeMaster.setEmpSuspensionDetailsList(null);
				employeeMaster.setInterchangeDetailsList(null);
				employeeMaster.setJoiningEntity(null);
				employeeMaster.setJoiningEntity(null);
				employeeMaster.setPersonalInfoAccountDetails(null);
				employeeMaster.setPersonaInfoGeneral(null);
				employeeMaster.setSalutation(null);
				employeeMaster.setStatusCode(null);
				employeeMaster.setSection(null);
				employeeMaster.setCreatedDate(submittedVoucherLog.getCreatedDate());
				employeeMaster.setCreatedByName("SUBMITTED");
				EmployeePersonalInfoEmployment employeePersonalInfoEmployment = employeePersonalInfoEmploymentRepository
						.findyByEmployeeId(employeeMaster.getId());
				if (employeePersonalInfoEmployment.getCurrentDesignation() != null) {
					employeeMaster.setDesignation(employeePersonalInfoEmployment.getCurrentDesignation());
				}
				approvalData.add(employeeMaster);
				pettyCashReceiptDTO.setReasonCodeName("SUBMITTED");
			}

			// APPROVED
			VoucherLog approvedVoucherLog = voucherLogRepository.getByVoucherId(voucherId, "APPROVED");
			if (approvedVoucherLog != null) {
				Long submitid = approvedVoucherLog.getCreatedBy().getId();
				EmployeeMaster employeeMaster = employeeMasterRepository.findByUserId(submitid);
				employeeMaster.setAchievementList(null);
				employeeMaster.setAdditionalChargeList(null);
				employeeMaster.setContactDetails(null);
				employeeMaster.setDepartment(null);
				employeeMaster.setDisciplinaryActionList(null);
				employeeMaster.setEducationList(null);
				employeeMaster.setEmpExperienceDetailsList(null);
				employeeMaster.setEmployeeDisplayName(null);
				employeeMaster.setEmpSuspensionDetailsList(null);
				employeeMaster.setIncrementDetailsList(null);
				employeeMaster.setEmployeePayscaleInformationList(null);
				employeeMaster.setEmployeeLeaveDeatailsList(null);
				employeeMaster.setEmployeeLoanAndAdvanceDetailsList(null);
				employeeMaster.setEmployeePromotionDetails(null);
				employeeMaster.setEmployeeScannedDocumentList(null);
				employeeMaster.setEmpSuspensionDetailsList(null);
				employeeMaster.setInterchangeDetailsList(null);
				employeeMaster.setJoiningEntity(null);
				employeeMaster.setJoiningEntity(null);
				employeeMaster.setPersonalInfoAccountDetails(null);
				employeeMaster.setPersonaInfoGeneral(null);
				employeeMaster.setSalutation(null);
				employeeMaster.setStatusCode(null);
				employeeMaster.setSection(null);
				employeeMaster.setCreatedDate(approvedVoucherLog.getCreatedDate());
				employeeMaster.setCreatedByName("APPROVED");
				EmployeePersonalInfoEmployment employeePersonalInfoEmployment = employeePersonalInfoEmploymentRepository
						.findyByEmployeeId(employeeMaster.getId());
				if (employeePersonalInfoEmployment.getCurrentDesignation() != null) {
					employeeMaster.setDesignation(employeePersonalInfoEmployment.getCurrentDesignation());
				}
				approvalData.add(employeeMaster);
				pettyCashReceiptDTO.setReasonCodeName("APPROVED");
			}

			// FINAL APPROVED
			VoucherLog finalApprovedVoucherLog = voucherLogRepository.getByVoucherId(voucherId, "FINALAPPROVED");
			EmployeeMaster finalApprovedEmp = getEmployeeData(finalApprovedVoucherLog, "FINALAPPROVED");
			if (finalApprovedEmp != null && finalApprovedEmp.getId() != null) {
				approvalData.add(finalApprovedEmp);
				pettyCashReceiptDTO.setReasonCodeName("FINALAPPROVED");
			}

			// Reject
			VoucherLog rejectedVoucherLog = voucherLogRepository.getByVoucherId(voucherId, "REJECTED");
			EmployeeMaster rejectedEmp = getEmployeeData(rejectedVoucherLog, "REJECTED");
			if (rejectedEmp != null && rejectedEmp.getId() != null) {
				approvalData.add(rejectedEmp);
				pettyCashReceiptDTO.setReasonCodeName("REJECTED");
			}
			// Notification

			if (notificationId != null && notificationId > 0) {
				SystemNotification systemNotification = systemNotificationRepository.findOne(notificationId);
				systemNotification.setNotificationRead(true);
				systemNotificationRepository.save(systemNotification);
			}

			pettyCashReceiptDTO.setPettyCashDocumentDTOList(paymentDetails);

			baseDTO.setResponseContent(pettyCashReceiptDTO);
			baseDTO.setResponseContents(approvalData);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.error("Exception in PettyCashReceiptService getDetailsByVoucherId ", e.getMessage());
		}
		return baseDTO;
	}

	public Map<String, Object> getBranchDetails(Long entityBranchId, Long createdBy) {
		log.info("PettyCashReceiptService. getBranchDetails() - START");
		Map<String, Object> data = new HashMap<String, Object>();
		try {
			UserMaster loginUser = loginService.getCurrentUser();
			Long loginUserId = loginUser == null ? null : loginUser.getId();
			log.info("PettyCashReceiptService. getBranchDetails() - loginUserId: " + loginUserId);

			EntityMaster entityMaster = entityMasterRepository.findByUserRegion(loginUserId);
			Long loginEntityId = entityMaster == null ? null : entityMaster.getId();
			// EntityMaster entityMaster =
			// entityMasterRepository.findByUserRegion(createdBy);

			String sql = "select ebb.account_number as accountNumber,bbm.branch_code||'/'||bbm.branch_name as branchCodeName from entity_bank_branch ebb  join bank_branch_master bbm on bbm.id=ebb.branch_id where ebb.id="
					+ entityBranchId;
			List<Map<String, Object>> dataMapList = jdbcTemplate.queryForList(sql);
			int dataMapListSize = dataMapList == null ? 0 : dataMapList.size();
			log.info("PettyCashReceiptService. getBranchDetails() - dataMapListSize: " + dataMapListSize);

			if (!CollectionUtils.isEmpty(dataMapList)) {
				data = dataMapList.get(0);
			}
		} catch (Exception e) {
			log.error("Exception in PettyCashReceiptService getBranchDetails ", e);
		}
		log.info("PettyCashReceiptService. getBranchDetails() - END");
		return data;
	}

	private EmployeeMaster getEmployeeData(VoucherLog vl, String status) {
		EmployeeMaster employeeMaster = new EmployeeMaster();
		try {

			if (vl != null) {
				Long submitid = vl.getCreatedBy().getId();
				employeeMaster = employeeMasterRepository.findByUserId(submitid);
				employeeMaster.setAchievementList(null);
				employeeMaster.setAdditionalChargeList(null);
				employeeMaster.setContactDetails(null);
				employeeMaster.setDepartment(null);
				employeeMaster.setDisciplinaryActionList(null);
				employeeMaster.setEducationList(null);
				employeeMaster.setEmpExperienceDetailsList(null);
				employeeMaster.setEmployeeDisplayName(null);
				employeeMaster.setEmpSuspensionDetailsList(null);
				employeeMaster.setIncrementDetailsList(null);
				employeeMaster.setEmployeePayscaleInformationList(null);
				employeeMaster.setEmployeeLeaveDeatailsList(null);
				employeeMaster.setEmployeeLoanAndAdvanceDetailsList(null);
				employeeMaster.setEmployeePromotionDetails(null);
				employeeMaster.setEmployeeScannedDocumentList(null);
				employeeMaster.setEmpSuspensionDetailsList(null);
				employeeMaster.setInterchangeDetailsList(null);
				employeeMaster.setJoiningEntity(null);
				employeeMaster.setJoiningEntity(null);
				employeeMaster.setPersonalInfoAccountDetails(null);
				employeeMaster.setPersonaInfoGeneral(null);
				employeeMaster.setSalutation(null);
				employeeMaster.setStatusCode(null);
				employeeMaster.setSection(null);
				employeeMaster.setCreatedDate(vl.getCreatedDate());
				employeeMaster.setCreatedByName(status.toUpperCase());
				EmployeePersonalInfoEmployment employeePersonalInfoEmployment = employeePersonalInfoEmploymentRepository
						.findyByEmployeeId(employeeMaster.getId());
				if (employeePersonalInfoEmployment.getCurrentDesignation() != null) {
					employeeMaster.setDesignation(employeePersonalInfoEmployment.getCurrentDesignation());
				}
			}

		} catch (Exception e) {
			log.error("Exception in PettyCashReceiptService getEmployeeData ", e.getMessage());
		}

		return employeeMaster;
	}

	public BaseDTO loadforentityMasterList(String searchParam) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info(" SearchParam : " + searchParam);
			List<EntityMaster> showRoomList = new ArrayList<EntityMaster>();
			showRoomList = entityMasterRepository.getShowRoomListByAutoComplete(searchParam);
			baseDTO.setResponseContents(showRoomList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Exception in PettyCashReceiptService loadforentityMasterList ", e);
		}
		return baseDTO;
	}

	public BaseDTO submitStatus(PettyCashReceiptDTO pcDTO) {
		BaseDTO responce = new BaseDTO();
		log.info(" <==================== SubmitStatus Method calling =======================> ");
		try {

			VoucherLog voucherLog = new VoucherLog();
			voucherLog.setCreatedDate(new Date());
			voucherLog.setCreatedBy(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
			voucherLog.setUserMaster(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
			Voucher voucher = voucherRepository.findOne(pcDTO.getId());
			voucherLog.setVoucher(voucher);
			if (!pcDTO.getActionApprovalStatus().equalsIgnoreCase("REJECTED")) {
				if (pcDTO.getActionApprovalStatus().equalsIgnoreCase("Approved")) {
					voucherLog.setStatus(VoucherStatus.APPROVED);
				} else {
					voucherLog.setStatus(VoucherStatus.FINALAPPROVED);
				}
			} else {
				voucherLog.setStatus(VoucherStatus.REJECTED);
			}
			VoucherLog vl = voucherLogRepository.save(voucherLog);

			VoucherNote voucherNote = new VoucherNote();
			UserMaster forwardTo = userMasterRepository.findOne(pcDTO.getForwardTo().getId());
			voucherNote.setForwardTo(forwardTo);
			voucherNote.setCreatedBy(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
			voucherNote.setCreatedDate(new Date());
			voucherNote.setVoucher(voucher);
			voucherNote.setNote(pcDTO.getNote());
			if (pcDTO.getStatus().equalsIgnoreCase("Final Approval")) {
				voucherNote.setFinalApproval(true);
			} else {
				voucherNote.setFinalApproval(false);
			}
			VoucherNote vn = voucherNoteRepository.save(voucherNote);

			String VIEW_PAGE = "/pages/finance/expenseTransaction/viewReceiptTransaction.xhtml?faces-redirect=true;";

			Map<String, Object> additionalData = new HashMap<>();
			additionalData.put("Url", VIEW_PAGE + "&id=" + vn.getVoucher().getId() + "&");
			notificationEmailService.sendMailAndNotificationForForward(vn, vl, additionalData);

		} catch (Exception e) {
			// TODO: handle exception
			responce.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Exception in PettyCashReceiptService submitStatus ", e);
		}

		responce.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		log.info(" <==================== SubmitStatus Method end =======================> ");

		return responce;
	}

	public BaseDTO cancelInvoiceByVoucherId(Long voucherId) {
		BaseDTO response = new BaseDTO();

		try {

			String cancelInvoiceDate = "2018-03-31";

			Date date = new SimpleDateFormat("yyyy-MM-dd").parse(cancelInvoiceDate);

			// Voucher
			Voucher voucher = voucherRepository.findOne(voucherId);
			voucher.setCreatedDate(date);
			voucher.setFromDate(date);
			voucherRepository.save(voucher);

			// VoucherDetails
			List<VoucherDetails> voucherDetails = voucherDetailsRepository.findVoucherDetailsByVoucherId(voucherId);
			for (VoucherDetails vd : voucherDetails) {
				vd.setCreatedDate(date);
				voucherDetailsRepository.save(vd);
			}

			// voucherLog
			VoucherLog voucherLog = voucherLogRepository.findByVoucherId(voucherId);
			voucherLog.setStatus(VoucherStatus.CANCEL);
			voucherLogRepository.save(voucherLog);

			// PaymentDetails
			List<PaymentDetails> paymentDetailsList = paymentDetailsRepository.getDetailByVoucherId(voucherId);
			Long paymentId = paymentDetailsList.get(0).getPayment().getId();
			for (PaymentDetails pd : paymentDetailsList) {

				pd.setCreatedDate(date);
				if (pd.getDocumentDate() != null) {
					pd.setDocumentDate(date);
				}
				paymentDetailsRepository.save(pd);
			}

			// payment
			Payment payment = paymentRepository.findOne(paymentId);
			payment.setCreatedDate(date);
			paymentRepository.save(payment);

			// accountTransaction
			AccountTransaction accountTransaction = accountTransactionRepository
					.findAccountTransactionByPaymentId(paymentId);
			accountTransaction.setCreatedDate(date);
			accountTransactionRepository.save(accountTransaction);

			// AccountTransactionDetails
			List<AccountTransactionDetails> accountTransactionDetails = accountTransactionDetailsRepository
					.findAccountTransactionDetailsByTransactionId(accountTransaction.getId());
			for (AccountTransactionDetails atd : accountTransactionDetails) {
				atd.setCreatedDate(date);
				if (atd.getTransactionDate() != null) {
					atd.setTransactionDate(date);
				}
				accountTransactionDetailsRepository.save(atd);
			}
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("Exception in paymentservice cancelInvoiceByVoucherId Method : ", e);
		}

		return response;
	}

	public BaseDTO getPaymentAmount(Long voucherId) {
		BaseDTO response = new BaseDTO();
		try {

			Double paymentAmount = paymentDetailsRepository.getPaymentAmountByVoucherId(voucherId);

			if (paymentAmount != null && paymentAmount > 0) {
				response.setSumTotal(paymentAmount);
			} else {
				response.setSumTotal(0.0);
			}
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			// TODO: handle exception
			response.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Exception in paymentservice getPaymentAmountByVoucherId Method : ", e);
		}

		return response;
	}

	public BaseDTO makePayment(PettyCashReceiptDTO pettyCashReceiptDTO) {

		BaseDTO response = new BaseDTO();
		try {

			Payment payment = paymentRepository.findOne(pettyCashReceiptDTO.getPaymentId());
			Voucher voucher = voucherRepository.getOne(pettyCashReceiptDTO.getId());
			PaymentMode pm = paymentModeRepository.findByCode(pettyCashReceiptDTO.getPaymentMode().getCode());
			voucher.setPaymentMode(pm);
			voucherRepository.save(voucher);

			for (PettyCashReceiptDTO pcrd : pettyCashReceiptDTO.getPettyCashDocumentDTOList()) {
				PaymentDetails paymentDetails = new PaymentDetails();
				paymentDetails.setAmount(pcrd.getExpenseAmount());
				paymentDetails.setDocumentDate(pcrd.getDocumentDate());
				paymentDetails.setBankReferenceNumber(pcrd.getDocumentnumber());

				if (pcrd.getBankCode() != null) {
					BankMaster bm = bankMasterRepository.getBankDetailsByCode(pcrd.getBankCode());
					paymentDetails.setBankMaster(bm);
				}

				if (pcrd.getEntityBranchId() != null && pcrd.getEntityBranchId() != 0) {
					EntityBankBranch entityBankBranch = entityBankBranchRepository.getOne(pcrd.getEntityBranchId());
					paymentDetails.setEntityBankBranch(entityBankBranch);
				}

				paymentDetails.setPaymentMethod(
						paymentMethodRepository.findByCode(pettyCashReceiptDTO.getPaymentMode().getCode()));
				paymentDetails.setPaymentTypeMaster(
						paymentTypeMasterRepository.findByName(pettyCashReceiptDTO.getReceiveFrom()));
				paymentDetails.setCreatedBy(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
				paymentDetails.setCreatedDate(new Date());
				PaymentCategory paymentCategory = PaymentCategory.PAID_OUT;
				paymentDetails.setPaymentCategory(paymentCategory);

				if (pettyCashReceiptDTO.getId() != null) {
					// voucher

					paymentDetails.setVoucher(voucher);

					// payment

					paymentDetails.setPayment(payment);
					paymentDetailsRepository.save(paymentDetails);
				}
			}
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			response.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Exception in paymentservice makePayment Method : ", e);
		}

		return response;
	}

	/**
	 * 
	 * @param headCode
	 * @return
	 */
	public BaseDTO getGlaccountByCode(String headCode) {
		log.info("PaymentReceiptService. getGlaccountByCode() - START " + headCode);
		BaseDTO baseDTO = new BaseDTO();
		GlAccount glAccountObj = new GlAccount();
		try {
			GlAccount glAccount = glAccountRepository.findByCode(headCode);

			if (glAccount != null) {
				log.info("glAccountId" + glAccount.getId());

				glAccountObj.setId(glAccount.getId());
				glAccountObj.setName(glAccount.getName());
				glAccountObj.setCode(glAccount.getCode());
				glAccountObj.setLName(glAccount.getLName());
				glAccountObj.setActiveStatus(glAccount.getActiveStatus());
				glAccountObj.setCreatedDate(glAccount.getCreatedDate());
				glAccountObj.setGlAccountGroup(glAccount.getGlAccountGroup());
			}

			baseDTO.setResponseContent(glAccountObj);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.error("Exception at getGlaccountByCode()", e);
		}
		log.info("PaymentReceiptService. getGlaccountByCode() - ENDS ");
		return baseDTO;
	}

	/**
	 * 
	 * @param headCode
	 * @return
	 */
	public BaseDTO getSundryAdvanceOBAmount(Long loginEntityId) {
		log.info("PaymentReceiptService. getSundryAdvanceOBAmount() - START " + loginEntityId);
		BaseDTO baseDTO = new BaseDTO();
		Double sundryAdvanceOBAmt = 0D;

		try {

			ApplicationQuery applicationQuery = applicationQueryRepository
					.findByQueryName("SUNDRY_ADVANCE_OB_AMOUNT_SQL");
			if (applicationQuery == null) {
				baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getErrorCode());
				return baseDTO;
			}

			int CurrentYear = Calendar.getInstance().get(Calendar.YEAR);
			int CurrentMonth = (Calendar.getInstance().get(Calendar.MONTH) + 1);
			String financiyalYearFromDate = "";
			String financiyalYearTodate = "";
			if (CurrentMonth < 4) {
				financiyalYearFromDate = (CurrentYear - 1) + "-04-01";
				financiyalYearTodate = (CurrentYear) + "-31-03";
			} else {
				financiyalYearFromDate = (CurrentYear) + "-04-01";
				financiyalYearTodate = (CurrentYear + 1) + "-03-31";
			}

			log.info("PaymentReceiptService. getSundryAdvanceOBAmount() - financiyalYearFromDate: "
					+ financiyalYearFromDate);
			log.info("PaymentReceiptService. getSundryAdvanceOBAmount() - financiyalYearTodate:  "
					+ financiyalYearTodate);

			String SQL = applicationQuery.getQueryContent();
			SQL = StringUtils.replace(SQL, ":loginEntityId", String.valueOf(loginEntityId));
			SQL = StringUtils.replace(SQL, ":fromDate", AppUtil.getValueWithSingleQuote(financiyalYearFromDate));
			SQL = StringUtils.replace(SQL, ":toDate", AppUtil.getValueWithSingleQuote(financiyalYearTodate));
			log.info("PaymentReceiptService. getSundryAdvanceOBAmount() - SQL: " + SQL);

			List<Map<String, Object>> dataMapList = jdbcTemplate.queryForList(SQL);

			if (!CollectionUtils.isEmpty(dataMapList)) {
				Map<String, Object> dataMap = dataMapList.get(0);
				if (!dataMap.isEmpty()) {
					sundryAdvanceOBAmt = dataMap.get("sundry_advance_ob_amt") != null
							? Double.parseDouble(dataMap.get("sundry_advance_ob_amt").toString())
							: 0D;
				} else {
					sundryAdvanceOBAmt = 0D;
				}
			} else {
				sundryAdvanceOBAmt = 0D;
			}

			log.info("PaymentReceiptService. getSundryAdvanceOBAmount() - sundryAdvanceOBAmt: " + sundryAdvanceOBAmt);
			baseDTO.setResponseContent(sundryAdvanceOBAmt);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.error("Exception at getSundryAdvanceOBAmount()", e);
		}
		log.info("PaymentReceiptService. getSundryAdvanceOBAmount() - ENDS ");
		return baseDTO;
	}
}
