package in.gov.cooptex.finance.service;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.repository.OrganizationMasterRepository;
import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.model.CreditSalesDemand;
import in.gov.cooptex.core.accounts.model.CreditSalesDemandDetails;
import in.gov.cooptex.core.accounts.model.CreditSalesDemandLog;
import in.gov.cooptex.core.accounts.model.CreditSalesDemandNote;
import in.gov.cooptex.core.accounts.model.CreditSalesRequest;
import in.gov.cooptex.core.accounts.repository.CreditSalesDemandLogRepository;
import in.gov.cooptex.core.accounts.repository.CreditSalesDemandNoteRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.CreditSalesDemandScheduleDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.OrganizationMaster;
import in.gov.cooptex.core.pos.repository.CreditSalesAccountInvoiceRepo;
import in.gov.cooptex.core.pos.repository.CreditSalesDemandRepository;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.CreditSalesRequestRepository;
import in.gov.cooptex.core.repository.CustomerMasterRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.util.JdbcUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.dto.CreditSalesDemandDetailsDto;
import in.gov.cooptex.finance.dto.CreditSalesDemandDto;
import in.gov.cooptex.finance.dto.CreditSalesDemandRequestDto;
import in.gov.cooptex.finance.dto.InstallmentDto;
import in.gov.cooptex.finance.enums.CreditSalesDemandStatus;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class CreditSalesDemandService {

	@Autowired
	CreditSalesDemandRepository creditSalesDemandRepository;

	@Autowired
	OrganizationMasterRepository organizationMasterRepository;

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	CreditSalesRequestRepository creditSalesRequestRepository;

	@Autowired
	EntityManager entityManager;

	@Autowired
	ResponseWrapper responseWrapper;

	@Autowired
	CreditSalesAccountInvoiceRepo CreditSalesAccountInvoiceRepo;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	CustomerMasterRepository customerMasterRepository;

	@Autowired
	LoginService loginService;

	@Autowired
	EmployeeMasterRepository employeeMasterRepository;

	@Autowired
	CreditSalesDemandLogRepository creditSalesDemandLogRepository;

	@Autowired
	CreditSalesDemandNoteRepository creditSalesDemandNoteRepository;

	@Autowired
	UserMasterRepository userMasterRepository;

	public BaseDTO getById(Long id) {
		log.info("CreditSalesDemandService getById method started [" + id + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			CreditSalesDemand demand = creditSalesDemandRepository.findOne(id);
			Validate.notNull(demand, ErrorDescription.DEMAND_NOT_FOUND);
			baseDTO.setResponseContent(demand);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("CreditSalesDemandService getById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("CreditSalesDemandService getById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("CreditSalesDemandService getById method completed");
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO getByDemandId(Long id) {
		log.info("CreditSalesDemandService getByDemandId method started [" + id + "]");
		BaseDTO baseDTO = new BaseDTO();
		CreditSalesDemandDto dto = new CreditSalesDemandDto();
		try {
			CreditSalesDemand demand = creditSalesDemandRepository.findOne(id);
			Validate.notNull(demand, ErrorDescription.DEMAND_NOT_FOUND);

			dto.setId(demand.getId());
			dto.setCreatedBy(demand.getCreatedBy().getUsername());
			dto.setCreatedById(demand.getCreatedBy().getId());
			dto.setModifiedBy(demand.getModifiedBy().getUsername());

			dto.setCreatedDate(demand.getCreatedDate());
			dto.setModifiedDate(demand.getModifiedDate());

			dto.setCreditSalesRequest(demand.getCreditSalesRequest());
			dto.setOrganizationMaster(demand.getOrganizationMaster());

			List<CreditSalesDemandDetailsDto> demandList = executeDemandDetailsViewQuery(id);
			dto.setDetailsList(demandList);

			List<CreditSalesDemandNote> noteList = creditSalesDemandNoteRepository
					.findAllByCreditSalesDemandId(demand.getId());
			dto.setCreditSalesDemandNoteList(noteList);

			CreditSalesDemandLog creditSalesDemandLog = creditSalesDemandLogRepository
					.findByCreditSalesDemandId(demand.getId()).get(0);
			dto.setCreditSalesDemandLog(creditSalesDemandLog);

			List<CreditSalesDemandLog> logList = creditSalesDemandLogRepository
					.findAllByCreditSalesDemandId(demand.getId());
			dto.setCreditSalesDemandLogList(logList);

			CreditSalesDemandNote note = creditSalesDemandNoteRepository.findByCreditSalesDemandId(demand.getId())
					.get(0);
			dto.setCreditSalesDemandNote(note);

			List<Map<String, Object>> employeeData = new ArrayList<Map<String, Object>>();
			if (id != null) {
				log.info("<<<:::::::demand::ID::not Null::::>>>>" + id);
				ApplicationQuery applicationQueryForlog = applicationQueryRepository
						.findByQueryName("CREDIT_SALES_DEMAND_LOG_EMPLOYEE_DETAILS");
				if (applicationQueryForlog == null || applicationQueryForlog.getId() == null) {
					log.info("Application Query For Log Details not found for query name : " + applicationQueryForlog);
					baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
					return baseDTO;
				}
				String logquery = applicationQueryForlog.getQueryContent().trim();
				log.info("<=========CREDIT_SALES_DEMAND_LOG_EMPLOYEE_DETAILS Query Content ======>" + logquery);
				logquery = logquery.replace(":creditSalesDemandId", "'" + id.toString() + "'");
				log.info("<=========CREDIT_SALES_DEMAND_LOG_EMPLOYEE_DETAILS Query Content afterReplace ======>"
						+ logquery);
				log.info(
						"Query Content For CREDIT_SALES_DEMAND_LOG_EMPLOYEE_DETAILS After replaced plan id View query : "
								+ logquery);
				employeeData = jdbcTemplate.queryForList(logquery);
				log.info("<=========CREDIT_SALES_DEMAND_LOG_EMPLOYEE_DETAILS Employee Data======>" + employeeData);
				baseDTO.setTotalListOfData(employeeData);
			}

			baseDTO.setResponseContent(dto);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("CreditSalesDemandService getByDemandId RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("CreditSalesDemandService getByDemandId Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("CreditSalesDemandService getByDemandId method completed");
		return baseDTO;
	}

	public BaseDTO deleteById(Long id) {
		log.info("CreditSalesDemandService deleteById method started [" + id + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			CreditSalesDemand demand = creditSalesDemandRepository.findOne(id);
			Validate.notNull(demand, ErrorDescription.DEMAND_NOT_FOUND);
			creditSalesDemandRepository.delete(id);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("CreditSalesDemandService deleteById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (DataIntegrityViolationException exception) {
			log.error("CreditSalesDemandService deleteById DataIntegrityViolationException ", exception);
			if (exception.getCause().getCause() instanceof PSQLException) {
				baseDTO.setStatusCode(ErrorDescription.CANNOT_DELETE_REFERENCED_RECORD.getErrorCode());
			} else {
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			}
		} catch (Exception exception) {
			log.error("CreditSalesDemandService deleteById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("CreditSalesDemandService deleteById method completed");
		return responseWrapper.send(baseDTO);
	}

	@Transactional
	public BaseDTO createCreditSalesDemand(CreditSalesDemandRequestDto request) {
		log.info("CreditSalesDemandService:createcreditSalesDemand()" + request);
		BaseDTO response = new BaseDTO();
		try {
			fieldValidation(request.getCreditSalesDemandDto());
			CreditSalesDemand creditSalesDemand = convertCreditSalesDemandDtoToCreditSalesDemand(
					request.getCreditSalesDemandDto());
			CreditSalesDemand creditSalesDemandSave = creditSalesDemandRepository.save(creditSalesDemand);

			// save CreditSalesDemandLog
			CreditSalesDemandLog log = new CreditSalesDemandLog();
			log.setCreditSalesDemand(creditSalesDemandSave);
			log.setStage(CreditSalesDemandStatus.SUBMITTED);
			creditSalesDemandLogRepository.save(log);

			// save CreditSalesDemandNote
			CreditSalesDemandNote note = new CreditSalesDemandNote();
			note.setCreditSalesDemand(creditSalesDemandSave);
			note.setFinalApproval(request.getCreditSalesDemandNote().getFinalApproval());
			note.setForwardTo(userMasterRepository.findOne(request.getCreditSalesDemandNote().getForwardTo().getId()));
			note.setNote(request.getCreditSalesDemandNote().getNote());
			creditSalesDemandNoteRepository.save(note);

			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (DataIntegrityViolationException divEX) {
			log.warn("<<===  Error While createcreditSalesDemand ===>>", divEX);
		} catch (ObjectOptimisticLockingFailureException lockEx) {
			log.warn("====>> Error while createcreditSalesDemand <<====", lockEx);
			response.setStatusCode(ErrorDescription.CANNOT_UPDATE_LOCKED_RECORD.getErrorCode());
		} catch (RestException re) {
			log.error("RestException occurred in createcreditSalesDemand.............", re);
			response.setStatusCode(re.getStatusCode());
		} catch (Exception e) {
			log.error("Exception occurred in createcreditSalesDemand.............", e);
			response.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("CreditSalesDemandService:createcreditSalesDemand()");
		return response;
	}

	public BaseDTO getActivecreditSalesDemand() {
		log.info("CreditSalesDemandService:getActivecreditSalesDemand()");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<CreditSalesDemand> creditSalesDemandList = creditSalesDemandRepository.findAll();
			baseDTO.setResponseContent(creditSalesDemandList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("CreditSalesDemandService getActivecreditSalesDemandRestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("CreditSalesDemandService getActivecreditSalesDemandException ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("CreditSalesDemandServicegetActivecreditSalesDemandmethod completed");
		return baseDTO;

	}

	/**
	 * @author Prabhu
	 * @param paginationDto
	 * @return
	 */

	/*
	 * public BaseDTO getAllCreditSalesDemandListLazy(PaginationDTO paginationDto) {
	 * log.
	 * info("<==== Starts CreditSalesDemandService.getAllCreditSalesDemandListLazy =====>"
	 * ); BaseDTO response = new BaseDTO(); try { Session session =
	 * entityManager.unwrap(Session.class); Criteria criteria =
	 * session.createCriteria(CreditSalesDemand.class,"creditSalesDemand");
	 * 
	 * criteria.createAlias("creditSalesDemand.creditSalesDemandLogList",
	 * "creditSalesDemandLog");
	 * criteria.createAlias("creditSalesDemand.creditSalesRequest",
	 * "creditSalesRequest");
	 * criteria.createAlias("creditSalesDemand.organizationMaster",
	 * "organizationMaster");
	 * 
	 * if(paginationDto.getFilters() != null) {
	 * log.info("credit sales demand filters :::"+paginationDto.getFilters());
	 * 
	 * if(paginationDto.getFilters().get("creditSalesPeriod") != null &&
	 * !paginationDto.getFilters().get("creditSalesPeriod").toString().trim().
	 * isEmpty()){
	 * criteria.add(Restrictions.like("creditSalesRequest.name","%"+paginationDto.
	 * getFilters().get("creditSalesPeriod").toString().trim()+"%").ignoreCase()); }
	 * 
	 * if(paginationDto.getFilters().get("organizationName") != null &&
	 * !paginationDto.getFilters().get("organizationName").toString().trim().isEmpty
	 * ()){
	 * 
	 * Criterion orgCode =
	 * Restrictions.like("organizationMaster.orgCode","%"+paginationDto.getFilters()
	 * .get("organizationName").toString().trim()+"%").ignoreCase();
	 * 
	 * Criterion orgName =
	 * Restrictions.like("organizationMaster.orgName","%"+paginationDto.getFilters()
	 * .get("organizationName").toString().trim()+"%").ignoreCase();
	 * 
	 * criteria.add(Restrictions.or(orgCode,orgName)); }
	 * 
	 * if(paginationDto.getFilters().get("createdDate") != null) { Date date = new
	 * Date((long) paginationDto.getFilters().get("createdDate")); DateFormat
	 * dateFormat = new SimpleDateFormat("dd-MM-yyyy"); String strDate =
	 * dateFormat.format(date); Date minDate = dateFormat.parse(strDate); Date
	 * maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
	 * criteria.add(Restrictions.conjunction().add(Restrictions.ge(
	 * "creditSalesDemand.createdDate", minDate))
	 * .add(Restrictions.lt("creditSalesDemand.createdDate", maxDate))); }
	 * 
	 * if(paginationDto.getFilters().get("stage") != null){
	 * criteria.add(Restrictions.eq("creditSalesDemandLog.stage",
	 * CreditSalesDemandStatus.valueOf(paginationDto.getFilters().get("stage").
	 * toString()))); }
	 * 
	 * 
	 * // this block of code is written for get the latest STATUS from the
	 * 'demandLog' DetachedCriteria logCriteria =
	 * DetachedCriteria.forClass(CreditSalesDemandLog.class,"demandLog");
	 * logCriteria.setProjection(Projections.projectionList().add(Projections.max(
	 * "createdDate")));
	 * logCriteria.add(Restrictions.eqProperty("demandLog.creditSalesDemand.id",
	 * "creditSalesDemand.id"));
	 * criteria.add(Subqueries.propertyEq("creditSalesDemandLog.createdDate",
	 * logCriteria));
	 * 
	 * 
	 * criteria.setProjection(Projections.rowCount()); Long totalResult = (long)
	 * ((Long) criteria.uniqueResult()).intValue(); criteria.setProjection(null);
	 * 
	 * ProjectionList projectionList = Projections.projectionList();
	 * projectionList.add(Projections.property("creditSalesDemand.id"));
	 * projectionList.add(Projections.property("creditSalesRequest.name"));
	 * projectionList.add(Projections.property("organizationMaster.orgCode"));
	 * projectionList.add(Projections.property("organizationMaster.orgName"));
	 * projectionList.add(Projections.property("creditSalesDemand.createdDate"));
	 * projectionList.add(Projections.property("creditSalesDemandLog.stage"));
	 * criteria.setProjection(projectionList);
	 * 
	 * criteria.setFirstResult(paginationDto.getFirst() *
	 * paginationDto.getPageSize());
	 * criteria.setMaxResults(paginationDto.getPageSize());
	 * 
	 * if ( paginationDto.getSortField() != null && paginationDto.getSortOrder() !=
	 * null ) { if (paginationDto.getSortOrder().equals("DESCENDING")) {
	 * criteria.addOrder(Order.desc(paginationDto.getSortField())); } else {
	 * criteria.addOrder(Order.asc(paginationDto.getSortField())); } } else {
	 * criteria.addOrder(Order.desc("creditSalesDemand.id")); }
	 * 
	 * List<?> resultList = criteria.list(); if (resultList == null ||
	 * resultList.isEmpty() || resultList.size() == 0) {
	 * log.info(":: creditSalesDemand list is null or empty ::"); }
	 * 
	 * List<CreditSalesDemandDto> creditSalesDemandDtoList = new ArrayList<>();
	 * 
	 * Iterator<?> it = resultList.iterator();
	 * 
	 * while (it.hasNext()) { Object obj[] = (Object[]) it.next();
	 * CreditSalesDemandDto creditSalesDemandDto = new CreditSalesDemandDto();
	 * creditSalesDemandDto.setId((Long) obj[0]);
	 * creditSalesDemandDto.setCreditSalesPeriod((String) obj[1]);
	 * creditSalesDemandDto.setOrganizationCode((String) obj[2]);
	 * creditSalesDemandDto.setOrganizationName((String) obj[2]);
	 * creditSalesDemandDto.setCreatedDate((Date) obj[4]);
	 * creditSalesDemandDto.setStage(obj[5].toString());
	 * creditSalesDemandDtoList.add(creditSalesDemandDto); }
	 * response.setResponseContents(creditSalesDemandDtoList);
	 * response.setTotalRecords(totalResult.intValue());
	 * response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode()); }
	 * }catch(Exception e) { log.
	 * error("Exception occured in CreditSalesDemandService.getAllCreditSalesDemandListLazy.."
	 * ,e); response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
	 * } log.
	 * info("<==== Ends CreditSalesDemandService.getAllCreditSalesDemandListLazy =====>"
	 * ); return responseWrapper.send(response); }
	 */

	public void fieldValidation(CreditSalesDemandDto request) {
		log.info("<== Starts  CreditSalesDemandService.fieldValidation ===>");
		Validate.objectNotNull(request.getCreditSalesRequest(), ErrorDescription.CREDIT_SALES_PERIOD_REQUIRED);
		Validate.objectNotNull(request.getCreditSalesRequest().getId(), ErrorDescription.CREDIT_SALES_PERIOD_REQUIRED);

		Validate.objectNotNull(request.getOrganizationMaster(), ErrorDescription.ORGANIZATION_REQUIRED);
		Validate.objectNotNull(request.getOrganizationMaster().getId(), ErrorDescription.ORGANIZATION_REQUIRED);

		Validate.objectNotNull(request.getDetailsList(), ErrorDescription.DEMAND_DETAILS_REQUIRED);
		log.info("<== Ends CreditSalesDemandService.fieldValidation ===>");
	}

	public BaseDTO getAllCreditSalesRequest() {
		log.info("<==== Starts CreditSalesDemandService.getAllCreditSalesRequest =====>");
		BaseDTO response = new BaseDTO();
		try {
			List<CreditSalesRequest> creditSalesRequestList = creditSalesRequestRepository
					.listAllActiveCreditSalesRequest();
			response.setResponseContent(creditSalesRequestList);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (RestException re) {
			log.error("RestException occured in CreditSalesDemandService.getAllCreditSalesRequest ...", re);
			response.setStatusCode(re.getStatusCode());
		} catch (Exception e) {
			log.error("Exception occured in CreditSalesDemandService.getAllCreditSalesRequest ...", e);
			response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<==== Ends CreditSalesDemandService.getAllCreditSalesRequest =====>");
		return response;

	}

	public BaseDTO getAllCreditSalesRequestByRegion(Long regionId) {
		log.info("<==== Starts CreditSalesDemandService.getAllCreditSalesRequest =====>");
		BaseDTO response = new BaseDTO();
		try {
			List<CreditSalesRequest> creditSalesRequestList = creditSalesRequestRepository
					.listAllActiveCreditSalesRequestbyRegion(regionId);
			response.setResponseContent(creditSalesRequestList);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (RestException re) {
			log.error("RestException occured in CreditSalesDemandService.getAllCreditSalesRequest ...", re);
			response.setStatusCode(re.getStatusCode());
		} catch (Exception e) {
			log.error("Exception occured in CreditSalesDemandService.getAllCreditSalesRequest ...", e);
			response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<==== Ends CreditSalesDemandService.getAllCreditSalesRequest =====>");
		return response;

	}

	public BaseDTO getAllOraganizationMastersByCreditSalesRequestDate(CreditSalesRequest request) {
		log.info("<==== Starts CreditSalesDemandService.getAllOraganizationMastersByCreditSalesRequestDate =====>"
				+ request.getFromDate() + "\t toDate::" + request.getToDate());
		BaseDTO response = new BaseDTO();
		try {
			List<OrganizationMaster> organizationMasters = CreditSalesAccountInvoiceRepo
					.findByCreatedDate(request.getFromDate(), request.getToDate());
			log.info("<<<::::::organization Master List::::::>>>" + organizationMasters.size());
			response.setResponseContent(organizationMasters);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (RestException re) {
			log.error(
					"RestException occured in CreditSalesDemandService.getAllOraganizationMastersByCreditSalesRequestDate ...",
					re);
			response.setStatusCode(re.getStatusCode());
		} catch (Exception e) {
			log.error(
					"Exception occured in CreditSalesDemandService.getAllOraganizationMastersByCreditSalesRequestDate ...",
					e);
			response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<==== Ends CreditSalesDemandService.getAllOraganizationMastersByCreditSalesRequestDate =====>");
		return response;
	}

	public BaseDTO getDemandDetailsList(CreditSalesDemandDto request) {
		log.info("<---Starts CreditSalesDemandService .getDemandDetailsList() ---> ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			Validate.objectNotNull(request.getCreditSalesRequest(), ErrorDescription.CREDIT_SALES_PERIOD_REQUIRED);
			Validate.objectNotNull(request.getCreditSalesRequest().getId(),
					ErrorDescription.CREDIT_SALES_PERIOD_REQUIRED);

//			Validate.objectNotNull(request.getOrganizationMaster(), ErrorDescription.ORGANIZATION_REQUIRED);
//			Validate.objectNotNull(request.getOrganizationMaster().getId(), ErrorDescription.ORGANIZATION_REQUIRED);

			CreditSalesDemand existCreditSalesDemand = creditSalesDemandRepository
					.findByCreditSalesRequestAndOrganization(request.getCreditSalesRequest().getId(),
							request.getOrganizationMaster().getId());
			if (existCreditSalesDemand != null) {
				log.info("Request already exist");
				List<CreditSalesDemandLog> demandLogList = creditSalesDemandLogRepository
						.findByCreditSalesDemandId(existCreditSalesDemand.getId());
				if (demandLogList != null && demandLogList.size() > 0
						&& !demandLogList.get(0).getStage().equals(CreditSalesDemandStatus.REJECTED)) {
					log.info("already exist request status ::" + demandLogList.get(0).getStage());
					throw new RestException(ErrorDescription.CREDIT_SALES_DEMAND_EXIST);
				}
			}

			List<CreditSalesDemandDetailsDto> demandDetailsList = executeDemandDetailsQuery(
					request.getCreditSalesRequest().getFromDate(), request.getCreditSalesRequest().getToDate(),
					request.getOrganizationMaster().getId());
			if (demandDetailsList != null) {
				log.info("demandDetailsList size is :::::::" + demandDetailsList.size());
				Long max = demandDetailsList.stream()
						.collect(Collectors
								.maxBy(Comparator.comparingLong(CreditSalesDemandDetailsDto::getNoOfInstallment)))
						.get().getNoOfInstallment();
				for (CreditSalesDemandDetailsDto dto : demandDetailsList) {
					HashMap<Long, Double> mapList = new HashMap<Long, Double>();
					for (long i = 1; i <= max; i++) {
						InstallmentDto installment = new InstallmentDto();
						if (dto.getNoOfInstallment() >= i) {
							installment.setInstallmentValue(dto.getMonthlyInstallmentAmount());
						} else {
							installment.setInstallmentValue(0.0);
						}
						mapList.put(i, installment.getInstallmentValue());
					}
					dto.setInstallmentMap(mapList);
				}
			}
			baseDTO.setResponseContent(demandDetailsList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (RestException re) {
			log.error("RestException occured in CreditSalesDemandService.getDemandDetailsList ...", re);
			baseDTO.setStatusCode(re.getStatusCode());
		} catch (Exception e) {
			log.info("<--- Exception in CreditSalesDemandService .getDemandDetailsList() ---> ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return baseDTO;
	}

	public List<CreditSalesDemandDetailsDto> executeDemandDetailsQuery(Date fromDate, Date toDate,
			Long organizationId) {
		log.info("Starts CreditSalesDemandService.executeDemandDetailsQuery ");
		log.info("fromDate ::" + fromDate + "\t toDate:::" + toDate + "\t organizationId::" + organizationId);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		ApplicationQuery applicationQuery = applicationQueryRepository.findByQueryName("DEMAND_DETAILS_QUERY");

		if (applicationQuery == null || applicationQuery.getId() == null) {
			log.info("Application Query not found for query name : " + applicationQuery.getQueryName());
			return null;
		}

		String query = applicationQuery.getQueryContent().trim();

		log.info("Query " + query);

		query = applicationQuery.getQueryContent().trim();
		query = query.replace(":organizationId", organizationId.toString());
		query = query.replace(":toDate", "'" + sdf.format(toDate) + "'");
		query = query.replace(":fromDate", "'" + sdf.format(fromDate) + "'");

		log.info("after replace Query==>" + query);

		List<CreditSalesDemandDetailsDto> demandDetailsList = jdbcTemplate.query(query,
				new RowMapper<CreditSalesDemandDetailsDto>() {

					@Override
					public CreditSalesDemandDetailsDto mapRow(ResultSet rs, int no) throws SQLException {
						CreditSalesDemandDetailsDto dto = new CreditSalesDemandDetailsDto();
						dto.setCustomerCode(rs.getString("customerCode"));
						dto.setCustomerName(rs.getString("customerName"));
						dto.setCustomerId(rs.getLong("customerId"));

						dto.setDesignationName(rs.getString("designation"));
						dto.setEmpCode(rs.getString("empCode"));
						dto.setSanctionOrderNumber(rs.getDouble("sanctionOrderNumber"));
						dto.setTotalPurchaseAmount(rs.getDouble("totalPurchaseAmount"));

						dto.setMonthlyInstallmentAmount(rs.getDouble("monthlyInstallment"));
						dto.setNoOfInstallment(rs.getLong("noOfInstallment"));

						return dto;
					}
				});

		log.info("Ends CreditSalesDemandService.executeDemandDetailsQuery ");

		return demandDetailsList;
	}

	public List<CreditSalesDemandDetailsDto> executeDemandDetailsViewQuery(Long demandId) {
		log.info("Starts CreditSalesDemandService.executeDemandDetailsViewQuery ");
		log.info("demandId::" + demandId);

		ApplicationQuery applicationQuery = applicationQueryRepository.findByQueryName("DEMAND_DETAILS_VIEW_QUERY");

		if (applicationQuery == null || applicationQuery.getId() == null) {
			log.info("Application Query not found for query name : " + applicationQuery.getQueryName());
			return null;
		}

		String query = applicationQuery.getQueryContent().trim();

		log.info("Query " + query);

		query = applicationQuery.getQueryContent().trim();
		query = query.replace(":DEMANDID", demandId.toString());

		log.info("after replace Query==>" + query);

		List<CreditSalesDemandDetailsDto> demandDetailsList = jdbcTemplate.query(query,
				new RowMapper<CreditSalesDemandDetailsDto>() {

					@Override
					public CreditSalesDemandDetailsDto mapRow(ResultSet rs, int no) throws SQLException {
						CreditSalesDemandDetailsDto dto = new CreditSalesDemandDetailsDto();
						dto.setCustomerCode(rs.getString("customerCode"));
						dto.setCustomerName(rs.getString("customerName"));
						dto.setCustomerId(rs.getLong("customerId"));

						dto.setDesignationName(rs.getString("designation"));
//				dto.setEmpCode(rs.getString("empCode"));
						dto.setSanctionOrderNumber(rs.getDouble("sanctionOrderNumber"));
						dto.setTotalPurchaseAmount(rs.getDouble("totalPurchaseAmount"));

						dto.setMonthlyInstallmentAmount(rs.getDouble("monthlyInstallment"));
						dto.setNoOfInstallment(rs.getLong("noOfInstallment"));

						Array array = rs.getArray("installmentDetails");

						String[] installments = (String[]) array.getArray();
						List<InstallmentDto> installmentList = new ArrayList<>();
						HashMap<Long, Double> mapList = new HashMap<Long, Double>();
						long count = 0;
						for (String i : installments) {
							count++;
							InstallmentDto install = new InstallmentDto();
							log.info("installment details ::::::" + i);
							String in[] = i.split("-");
							install.setMonth(in[0]);
							install.setId(count);
							install.setYear(Integer.parseInt(in[1]));
							install.setInstallmentValue(Double.parseDouble(in[2]));
							mapList.put(count, Double.parseDouble(in[2]));
							installmentList.add(install);
						}
						dto.setInstallmentMap(mapList);
						dto.setInstallmentList(installmentList);
						return dto;
					}
				});

		log.info("Ends CreditSalesDemandService.executeDemandDetailsViewQuery ");

		return demandDetailsList;
	}

	public CreditSalesDemand convertCreditSalesDemandDtoToCreditSalesDemand(CreditSalesDemandDto dto) {
		log.info("starts convertCreditSalesDemandDtoToCreditSalesDemand");
		CreditSalesDemand creditSalesDemand = new CreditSalesDemand();
		List<CreditSalesDemandDetails> demandDetailsList = new ArrayList<>();
		creditSalesDemand
				.setCreditSalesRequest(creditSalesRequestRepository.findOne(dto.getCreditSalesRequest().getId()));
		creditSalesDemand
				.setOrganizationMaster(organizationMasterRepository.findOne(dto.getOrganizationMaster().getId()));
		EmployeeMaster emp = employeeMasterRepository.findByUserId(loginService.getCurrentUser().getId());
		creditSalesDemand.setEntityMaster(
				entityMasterRepository.findOne(emp.getPersonalInfoEmployment().getWorkLocation().getId()));
		Long max = dto.getDetailsList().stream()
				.collect(Collectors.maxBy(Comparator.comparingLong(CreditSalesDemandDetailsDto::getNoOfInstallment)))
				.get().getNoOfInstallment();
		for (CreditSalesDemandDetailsDto details : dto.getDetailsList()) {

			Date date = new Date();
			LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			int month = localDate.getMonthValue(); // 1 to 12
			int currentMonth = localDate.getMonthValue(); // 1 to 12
			long year = localDate.getYear();
			log.info("Month ::" + month + "\t year :::" + year);
			for (int i = 1; i <= max; i++) {
				CreditSalesDemandDetails csdd = new CreditSalesDemandDetails();
				csdd.setTotalPurchaseAmount(details.getTotalPurchaseAmount());
				csdd.setCustomerMaster(customerMasterRepository.findOne(details.getCustomerId()));
				csdd.setCreditSalesDemand(creditSalesDemand);
				csdd.setSanctionOrderNumber(details.getSanctionOrderNumber());
				csdd.setMonthlyInstallmentAmount(details.getMonthlyInstallmentAmount());
				csdd.setNumberOfInstallment((double) details.getNoOfInstallment());
				csdd.setTotalPurchaseAmount(details.getTotalPurchaseAmount());
				csdd.setDemandAmount(details.getMonthlyInstallmentAmount());

				month = month + 1;

//				Calendar calendar = Calendar.getInstance();
//				calendar.add(Calendar.MONTH,(int)i);
//				DateFormat sdf = new SimpleDateFormat("MM");
//				log.info("Month ::"+sdf.format(calendar.getTime()));
//				log.info("Year ::"+calendar.getInstance().get(Calendar.YEAR));
				log.info(" month :::" + month);
				csdd.setDemandMonth(String.valueOf(month)); // 01
				if (month >= 12) {
					month = 0;
					year = year + 1;
				}
				log.info("Month count::" + month + "\t year :::" + year);
				csdd.setDemandYear(year);
				demandDetailsList.add(csdd);
			}
		}
		creditSalesDemand.setCredistSalesDemandDetList(demandDetailsList);
		log.info("Ends convertCreditSalesDemandDtoToCreditSalesDemand");
		return creditSalesDemand;
	}

	public Boolean checkExist(CreditSalesDemandDetails details, List<CreditSalesDemandDetailsDto> list) {
		log.info("Starts  checkExist-------");
		for (CreditSalesDemandDetailsDto dto : list) {
			if (dto.getCustomerId().equals(details.getCustomerMaster().getId()))
				return true;

		}
		log.info("Ends checkExist------");
		return false;
	}

	/*
	 * @Transactional public BaseDTO approveRejectDemand(CreditSalesDemandRequestDto
	 * request) {
	 * log.info("<==== Starts CreditSalesDemandService.approveRejectDemand =====>");
	 * BaseDTO response = new BaseDTO(); try { CreditSalesDemand demand =
	 * creditSalesDemandRepository.findOne(request.getCreditSalesDemand().getId());
	 * Validate.notNull(demand, ErrorDescription.DEMAND_NOT_FOUND);
	 * 
	 * CreditSalesDemandNote note =
	 * creditSalesDemandNoteRepository.findByCreditSalesDemandId(demand.getId());
	 * if(note.getFinalApproval()) {
	 * 
	 * }else { CreditSalesDemandNote demandNote = new CreditSalesDemandNote();
	 * demandNote.setCreditSalesDemand(demand); demandNote.setNote(note.getNote());
	 * demandNote.setFinalApproval(request.getCreditSalesDemandNote().
	 * getFinalApproval());
	 * demandNote.setForwardTo(userMasterRepository.findOne(request.
	 * getCreditSalesDemandNote().getForwardTo().getId()));
	 * creditSalesDemandNoteRepository.save(demandNote);
	 * 
	 * CreditSalesDemandLog demandLog = new CreditSalesDemandLog();
	 * demandLog.setCreditSalesDemand(demand);
	 * demandLog.setStage(request.getCreditSalesDemandLog().getStage());
	 * demandLog.setRemarks(request.getCreditSalesDemandLog().getRemarks());
	 * creditSalesDemandLogRepository.save(demandLog); }
	 * response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
	 * }catch(RestException re) { log.
	 * error("RestException occured in CreditSalesDemandService.approveRejectDemand ..."
	 * ,re); response.setStatusCode(re.getStatusCode()); }catch(Exception e) { log.
	 * error("Exception occured in CreditSalesDemandService.approveRejectDemand ..."
	 * ,e); response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
	 * } log.info("<==== Ends CreditSalesDemandService.approveRejectDemand =====>");
	 * return response; }
	 */

	public BaseDTO approveCreditSalesDemand(CreditSalesDemand request) {
		log.info("<--- starts CreditSalesDemand approve [" + request + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			CreditSalesDemand creditSalesDemand = creditSalesDemandRepository.findOne(request.getId());
			if (creditSalesDemand != null) {

				CreditSalesDemandNote demandNote = new CreditSalesDemandNote();
				demandNote.setCreditSalesDemand(creditSalesDemand);
				demandNote.setForwardTo(userMasterRepository.findOne(request.getForwardToId()));
				demandNote.setFinalApproval(request.getIsFinalApprove());
				demandNote.setNote(request.getNote());

				CreditSalesDemandLog planLog = new CreditSalesDemandLog();
				planLog.setCreditSalesDemand(creditSalesDemand);
				planLog.setStage(CreditSalesDemandStatus.APPROVED);
				planLog.setRemarks(request.getRemarks());

				creditSalesDemand.getCreditSalesDemandLogList().add(planLog);
				creditSalesDemand.getCreditSalesDemandNoteList().add(demandNote);

				creditSalesDemandRepository.save(creditSalesDemand);
				log.info("CreditSalesDemand Approved Successfully. [" + request.getId() + "]");
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				log.info("CreditSalesDemand not Found");
				baseDTO.setStatusCode(ErrorDescription.DEMAND_NOT_FOUND.getCode());
			}

		} catch (Exception exception) {
			log.error("Exception while approve CreditSalesDemand ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO rejectCreditSalesDemand(CreditSalesDemand request) {
		log.info("<--- starts CreditSalesDemand rejectCreditSalesDemand [" + request + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			CreditSalesDemand creditSalesDemand = creditSalesDemandRepository.findOne(request.getId());
			if (creditSalesDemand != null) {
				CreditSalesDemandLog planLog = new CreditSalesDemandLog();
				planLog.setCreditSalesDemand(creditSalesDemand);
				planLog.setStage(CreditSalesDemandStatus.REJECTED);
				planLog.setRemarks(request.getRemarks());
				creditSalesDemandLogRepository.save(planLog);

				log.info("CreditSalesDemand Service Rejected Successfully. [" + request.getId() + "]");
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());

			} else {
				log.info("CreditSalesDemand not Found");
				baseDTO.setStatusCode(ErrorDescription.DEMAND_NOT_FOUND.getCode());
			}

		} catch (Exception exception) {
			log.error("Exception rejectCreditSalesDemand ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO finalApproveCreditSalesDemand(CreditSalesDemand request) {
		log.info("<--- starts CreditSalesDemandService finalApproveCreditSalesDemand [" + request + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			CreditSalesDemand creditSalesDemand = creditSalesDemandRepository.findOne(request.getId());
			if (creditSalesDemand != null) {
				CreditSalesDemandLog planLog = new CreditSalesDemandLog();
				planLog.setCreditSalesDemand(creditSalesDemand);
				planLog.setStage(CreditSalesDemandStatus.FINAL_APPROVED);
				planLog.setRemarks(request.getRemarks());
				creditSalesDemandLogRepository.save(planLog);
				log.info("finalApproveCreditSalesDemand Successfully. [" + request.getId() + "]");
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				log.info("CreditSalesDemand not Found");
				baseDTO.setStatusCode(ErrorDescription.DEMAND_NOT_FOUND.getCode());
			}

		} catch (Exception exception) {
			log.error("Exception finalApproveCreditSalesDemand ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO getAllCreditsalesDemandListLazy(PaginationDTO paginationDto) {
		log.info("<==== Starts CreditSalesDemandService.getAllCreditsalesDemandListLazy =====>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			Integer total = 0;
			Integer start = paginationDto.getFirst(), pageSize = paginationDto.getPageSize();
			start = start * pageSize;
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();

			ApplicationQuery applicationQuery = applicationQueryRepository
					.findByQueryName("CREDIT_SALES_DEMAND_LAZY_LIST_QUERY");

			if (applicationQuery == null || applicationQuery.getId() == null) {
				log.info("Application Query not found for query name : " + applicationQuery.getQueryName());
				return null;
			}

			String mainQuery = applicationQuery.getQueryContent().trim();

			log.info("Query " + mainQuery);

			mainQuery = applicationQuery.getQueryContent().trim();

			log.info("filters ::::::::" + paginationDto.getFilters());

			if (paginationDto.getFilters().get("creditSalesPeriod") != null
					&& !paginationDto.getFilters().get("creditSalesPeriod").toString().trim().isEmpty()) {
				String name = paginationDto.getFilters().get("creditSalesPeriod").toString().trim();
				mainQuery += " and ( upper(csr.name) like upper('%" + name + "%')) ";
			}
			if (paginationDto.getFilters().get("organizationName") != null
					&& !paginationDto.getFilters().get("organizationName").toString().trim().isEmpty()) {
				String name = paginationDto.getFilters().get("organizationName").toString().trim();
				mainQuery += " and ( upper(om.org_name) like upper('%" + name
						+ "%') or upper(om.org_code) like upper('%" + name + "%'))";
			}

			if (paginationDto.getFilters().get("createdDate") != null) {
				Date createdDate = new Date((Long) paginationDto.getFilters().get("createdDate"));
				mainQuery += " and date(csd.created_date)=date('" + createdDate + "')";
			}
			if (paginationDto.getFilters().get("stage") != null) {
				String name = paginationDto.getFilters().get("stage").toString().trim();
				mainQuery += " and csdl.stage='" + name + "'";
			}

			String countQuery = mainQuery.replace(
					"SELECT\n" + "    csd.id as DEMAND_ID,\n" + "    csr.name as SALES_PERIOD,\n"
							+ "    om.org_code as ORG_CODE,\n" + "    om.org_name as ORG_NAME,\n"
							+ "    csd.created_date as CREATED_DATE,\n" + "    csdl.stage as STAGE",
					"select count(*) as count ");
			log.info("count query... " + countQuery);
			countData = jdbcTemplate.queryForList(countQuery);
			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					total = Integer.parseInt(data.get("count").toString().replace(",", ""));
			}

			if (paginationDto.getSortField() == null)
				mainQuery += " order by csd.id desc limit " + pageSize + " offset " + start + ";";

			if (paginationDto.getSortField() != null && paginationDto.getSortOrder() != null) {
				log.info("Sort Field:[" + paginationDto.getSortField() + "] Sort Order:[" + paginationDto.getSortOrder()
						+ "]");
				if (paginationDto.getSortField().equals("creditSalesPeriod")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by csr.name asc ";
				if (paginationDto.getSortField().equals("creditSalesPeriod")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by csr.name desc ";

				if (paginationDto.getSortField().equals("organizationName")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by om.org_code asc  ";
				if (paginationDto.getSortField().equals("organizationName")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by om.org_code desc  ";

				if (paginationDto.getSortField().equals("createdDate")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by csd.created_date asc ";
				if (paginationDto.getSortField().equals("createdDate")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by csd.created_date desc ";

				if (paginationDto.getSortField().equals("status") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by csdl.stage asc ";
				if (paginationDto.getSortField().equals("status") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by csdl.stage desc ";

				mainQuery += " limit " + pageSize + " offset " + start + ";";
			}
			log.info("Main Qury....." + mainQuery);
			List<CreditSalesDemandDto> creditSalesDemandRequestList = new ArrayList<>();
			listofData = jdbcTemplate.queryForList(mainQuery);
			for (Map<String, Object> data : listofData) {
				CreditSalesDemandDto dto = new CreditSalesDemandDto();
				if (data.get("DEMAND_ID") != null)
					dto.setId(Long.parseLong(data.get("DEMAND_ID").toString()));

				if (data.get("SALES_PERIOD") != null)
					dto.setCreditSalesPeriod((data.get("SALES_PERIOD").toString()));

				if (data.get("ORG_CODE") != null)
					dto.setOrganizationCode((data.get("ORG_CODE").toString()));

				if (data.get("ORG_NAME") != null)
					dto.setOrganizationName((data.get("ORG_NAME").toString()));

				if (data.get("CREATED_DATE") != null) {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					Date dt = sdf.parse(data.get("CREATED_DATE").toString());
					dto.setCreatedDate(dt);
				}

				if (data.get("STAGE") != null)
					dto.setStage((data.get("STAGE").toString()));
				creditSalesDemandRequestList.add(dto);
			}
			log.info("Returning creditSalesDemandRequestList...." + creditSalesDemandRequestList.size());
			log.info("Total records present in creditSalesDemandRequestList..." + total);
			baseDTO.setResponseContent(creditSalesDemandRequestList);
			baseDTO.setTotalRecords(total);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("Exception occured in CreditSalesDemandService.getAllCreditsalesDemandListLazy..", e);
			baseDTO.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<==== Ends CreditSalesDemandService.getAllCreditsalesDemandListLazy =====>");
		return responseWrapper.send(baseDTO);
	}

	private static final String creditSalesDemandScheduleSQL = "INSERT INTO credit_sales_demand_schedule(credit_sales_request_id,region_id,demand_start_year,demand_start_month,created_date,schedule_status) "
			+ "VALUES(${creditSalesRequestId},${regionId},${demandStartYear},${demandStartMonth},NOW(),'INITIATED');";

	private static final String creditSalesDemandScheduleCheckSQL = "SELECT demand_file_path,schedule_status,demand_file_status FROM credit_sales_demand_schedule WHERE credit_sales_request_id=${creditSalesRequestId} AND region_id=${regionId} ";

	private static final String creditSalesDemandList = "select csd.id, csd.credit_sales_request_id, csd.organization_id, csd.region_id, sum(csdd.monthly_installment_amount) as amount, concat(om.org_code, '/', om.org_name) as orgCodeName, count(distinct(csdd.customer_id)) as customercount, csd.pdf_file_path from credit_sales_demand csd left join credit_sales_demand_details csdd on csdd.credit_sales_demand_id = csd.id left join organization_master om on om.id = csd.organization_id where csd.region_id =:regionID and csd.credit_sales_request_id =:requestID group by csd.id, om.org_name, om.org_code";

	private static final String creditSalesDemandDetailsListQuery = " select distinct concat(em.code, '/', em.name) as showroomCodeName, cm.name as employeeName, csdd.employee_number, csdd.sanction_order_number, csdd.total_purchase_amount, csdd.number_of_installment, csdd.cheque_received, csdd.bill_numbers_date from credit_sales_demand_details csdd join customer_master cm on cm.id = csdd.customer_id join entity_master em on em.id = csdd.registered_entity_id where csdd.credit_sales_demand_id =:demandID";

	/**
	 * @param creditSalesDemandScheduleDTO
	 * @return
	 */
	public synchronized BaseDTO initiateCreditDemandSchedule(
			CreditSalesDemandScheduleDTO creditSalesDemandScheduleDTO) {
		log.info("<--Starts CreditSalesDemandController.initiateCreditDemandSchedule-->");
		BaseDTO baseDTO = new BaseDTO();
		String errorMessage = null;
		try {

			validateCreditDemandScheduleInput(creditSalesDemandScheduleDTO);

			String checkSql = creditSalesDemandScheduleCheckSQL;
			checkSql = checkSql.replace("${creditSalesRequestId}",
					String.valueOf(creditSalesDemandScheduleDTO.getCreditSalesRequestId()));
			checkSql = checkSql.replace("${regionId}", String.valueOf(creditSalesDemandScheduleDTO.getRegionId()));
			log.info("Query for Datatable=====>>" + checkSql);

			if (creditSalesDemandScheduleDTO.getDemandSartMonth() != null) {
				checkSql = checkSql + " AND demand_start_month='" + creditSalesDemandScheduleDTO.getDemandSartMonth()
						+ "'";
			}
			if (creditSalesDemandScheduleDTO.getDemandSartYear() != null) {
				checkSql = checkSql + " AND demand_start_year='" + creditSalesDemandScheduleDTO.getDemandSartYear()
						+ "'";
			}
			log.info("Query for Datatable=====>>" + checkSql);
			// Object valueObj = JdbcUtil.getValue(jdbcTemplate, checkSql, null,
			// "schedule_status");

			List<Map<String, Object>> mapList = jdbcTemplate.queryForList(checkSql);
			//
			Object demandFileStatusObj = null;
			Object scheduleStatusObj = null;
			Object filePathObj = null;
			//
			if (mapList != null && !mapList.isEmpty()) {
				Map<String, Object> map = mapList.get(0);
				demandFileStatusObj = map.get("demand_file_status");
				scheduleStatusObj = map.get("schedule_status");
				filePathObj = map.get("demand_file_path");
			}

			String scheduleStatus = scheduleStatusObj == null ? "" : scheduleStatusObj.toString();
			String demandFileStatus = demandFileStatusObj == null ? "" : demandFileStatusObj.toString();
			String filePath = filePathObj == null ? "" : filePathObj.toString();

			String message = null;

			if (!scheduleStatus.isEmpty()) {
				if ("INITIATED".equals(scheduleStatus)) {
					message = "Process initiated already.";
				} else if ("IN_PROGRESS".equals(scheduleStatus)) {
					message = "Process is in progress.";
				} else if ("COMPLETED".equals(scheduleStatus)) {
					message = "Process completed.";
					if ("INITIATED".equals(demandFileStatus)) {
						message = "Process completed. File generation is initiated.";
					} else if ("IN_PROGRESS".equals(demandFileStatus)) {
						message = "Process completed. File generation is in progress.";
					} else if ("COMPLETED".equals(demandFileStatus)) {
						message = "Process completed.";
						if (!filePath.isEmpty()) {
							message = "Process completed and demand file generated.";
							baseDTO.setGeneralContent(filePath.toString());
						} else {
							message = "Process completed but demand file not generated.";
						}
					} else {
						message = "Demand file not generated. Please contact support.";
					}
				} else {
					message = "Demand process not completed. Please contact support.";
				}
				baseDTO.setStatusCode(0);
				baseDTO.setMessage(message);
				return baseDTO;
			} else {
				String sql = creditSalesDemandScheduleSQL;
				sql = sql.replace("${creditSalesRequestId}",
						String.valueOf(creditSalesDemandScheduleDTO.getCreditSalesRequestId()));
				sql = sql.replace("${regionId}", String.valueOf(creditSalesDemandScheduleDTO.getRegionId()));
				sql = sql.replace("${demandStartYear}",
						String.valueOf(creditSalesDemandScheduleDTO.getDemandSartYear()));
				sql = sql.replace("${demandStartMonth}",
						String.valueOf(creditSalesDemandScheduleDTO.getDemandSartMonth()));

				int cnt = jdbcTemplate.update(sql);
				log.info(cnt + " CreditSalesDemandSchedule Row Inserted");
				baseDTO.setMessage("Process initiated");
			}
			baseDTO.setStatusCode(0);
		} catch (RestException ex) {
			errorMessage = ex.getMessage();
			log.error("RestException at initiateCreditDemandSchedule(1)" + errorMessage);
		} catch (Exception ex) {
			errorMessage = "Unknown error. Unable to initiate credit demand schedule. Please contact support.";
			log.error("Exception at initiateCreditDemandSchedule(2)", ex);
		}

		if (StringUtils.isNotEmpty(errorMessage)) {
			baseDTO = new BaseDTO();
			baseDTO.setStatusCode(1);
			baseDTO.setErrorDescription(errorMessage);
		}
		return baseDTO;
	}

	/**
	 * @param creditSalesDemandScheduleDTO
	 * @throws Exception
	 */
	private void validateCreditDemandScheduleInput(CreditSalesDemandScheduleDTO creditSalesDemandScheduleDTO)
			throws Exception {

		if (creditSalesDemandScheduleDTO == null) {
			throw new RestException("Invalid credit demand schedule input");
		}

		if (creditSalesDemandScheduleDTO.getCreditSalesRequestId() == null) {
			throw new RestException("Credit sales request is empty");
		}

		if (creditSalesDemandScheduleDTO.getRegionId() == null) {
			throw new RestException("Credit sales region is empty");
		}

		if (creditSalesDemandScheduleDTO.getDemandSartYear() == null) {
			throw new RestException("Demand start year is empty");
		}

		if (creditSalesDemandScheduleDTO.getDemandSartMonth() == null) {
			throw new RestException("Demand start month is empty");
		}
	}

	public BaseDTO getDemandList(CreditSalesDemandDto request) {
		log.info("<---Starts CreditSalesDemandService .getDemandDetailsList() ---> ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			Validate.objectNotNull(request.getCreditSalesRequestId(), ErrorDescription.CREDIT_SALES_PERIOD_REQUIRED);

//			Validate.objectNotNull(request.getOrganizationMaster(), ErrorDescription.ORGANIZATION_REQUIRED);
//			Validate.objectNotNull(request.getOrganizationMaster().getId(), ErrorDescription.ORGANIZATION_REQUIRED);

//			List<CreditSalesDemand> existCreditSalesDemandList = creditSalesDemandRepository
//					.findByCreditSalesRequestAndRegion(request.getCreditSalesRequestId(), request.getRegionId());

			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<CreditSalesDemandDto> creditSalesDemandRequestList = new ArrayList<>();

			String sql = creditSalesDemandList;
			sql = sql.replace(":requestID", String.valueOf(request.getCreditSalesRequestId()));
			sql = sql.replace(":regionID", String.valueOf(request.getRegionId()));

			listofData = jdbcTemplate.queryForList(sql);
			for (Map<String, Object> data : listofData) {
				CreditSalesDemandDto dto = new CreditSalesDemandDto();
				if (data.get("id") != null)
					dto.setId(Long.parseLong(data.get("id").toString()));

				if (data.get("credit_sales_request_id") != null)
					dto.setCreditSalesRequestId(Long.parseLong((data.get("credit_sales_request_id").toString())));

				if (data.get("orgCodeName") != null)
					dto.setOrganizationCodeName((data.get("orgCodeName").toString()));

				if (data.get("organization_id") != null)
					dto.setOrganizationId(Long.parseLong(data.get("organization_id").toString()));

				if (data.get("region_id") != null)
					dto.setRegionId(Long.parseLong((data.get("region_id").toString())));

				if (data.get("amount") != null)
					dto.setTotalAmount(Double.parseDouble((data.get("amount").toString())));

				if (data.get("customercount") != null)
					dto.setCustomerCount(Double.parseDouble(data.get("customercount").toString()));

				if (data.get("pdf_file_path") != null)
					dto.setFilePath((data.get("pdf_file_path").toString()));

				creditSalesDemandRequestList.add(dto);
			}
			if (creditSalesDemandRequestList != null && creditSalesDemandRequestList.size() > 0) {
				baseDTO.setResponseContent(creditSalesDemandRequestList);
//				baseDTO.setResponseContents(existCreditSalesDemandList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
				baseDTO.setMessage("Credit Sales is empty");
			}
		} catch (RestException re) {
			log.error("RestException occured in CreditSalesDemandService.getDemandDetailsList ...", re);
			baseDTO.setStatusCode(re.getStatusCode());
		} catch (Exception e) {
			log.info("<--- Exception in CreditSalesDemandService .getDemandDetailsList() ---> ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return baseDTO;
	}

	public BaseDTO getDemandDetailsListNew(CreditSalesDemandDto request) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("<---Starts CreditSalesDemandService .getDemandDetailsList() ---> ");
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<CreditSalesDemandDetailsDto> creditSalesDemandDetailsList = new ArrayList<>();

			String sql = creditSalesDemandDetailsListQuery;
			sql = sql.replace(":demandID", String.valueOf(request.getId()));

			listofData = jdbcTemplate.queryForList(sql);
			for (Map<String, Object> data : listofData) {
				CreditSalesDemandDetailsDto dto = new CreditSalesDemandDetailsDto();
				if (data.get("id") != null)
					dto.setId(Long.parseLong(data.get("id").toString()));

				if (data.get("credit_sales_demand_id") != null)
					dto.setCreditSalesDemandId(Long.parseLong(data.get("credit_sales_demand_id").toString()));

				if (data.get("showroomCodeName") != null)
					dto.setShowroomCodeName((data.get("showroomCodeName").toString()));

				if (data.get("employeeName") != null)
					dto.setEmployeeName(data.get("employeeName").toString());

				if (data.get("employee_number") != null)
					dto.setEmployeeCode(data.get("employee_number").toString());

				if (data.get("sanction_order_number") != null)
					dto.setSanctionOrderNumber(Double.parseDouble((data.get("sanction_order_number").toString())));

				if (data.get("total_purchase_amount") != null)
					dto.setTotalPurchaseAmount(Double.parseDouble(data.get("total_purchase_amount").toString()));

				if (data.get("number_of_installment") != null)
					dto.setNoOfInstallment(Long.parseLong(data.get("number_of_installment").toString()));

				if (data.get("bill_numbers_date") != null)
					dto.setBillNumberAndDate(data.get("bill_numbers_date").toString());

				if (data.get("cheque_received") != null)
					dto.setChequeReceived(data.get("cheque_received").toString());

				creditSalesDemandDetailsList.add(dto);
			}

			if (creditSalesDemandDetailsList != null && creditSalesDemandDetailsList.size() > 0) {
				baseDTO.setResponseContents(creditSalesDemandDetailsList);
//			baseDTO.setResponseContents(existCreditSalesDemandList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
				baseDTO.setMessage("Credit Sales is empty");
			}
		} catch (RestException re) {
			log.error("RestException occured in CreditSalesDemandService.getDemandDetailsList ...", re);
			baseDTO.setStatusCode(re.getStatusCode());
		} catch (Exception e) {
			log.info("<--- Exception in CreditSalesDemandService .getDemandDetailsList() ---> ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return baseDTO;
	}

}