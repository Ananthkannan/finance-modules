package in.gov.cooptex.finance.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.cooptex.core.accounts.model.GlAccount;
import in.gov.cooptex.core.accounts.repository.GlAccountRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.enums.ApprovalStatus;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeePayRollDetails;
import in.gov.cooptex.core.model.EmployeePersonalInfoEmployment;
import in.gov.cooptex.core.model.EmployeeTransferDetails;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EmployeePayrollDetailsRepository;
import in.gov.cooptex.core.repository.EmployeePersonalInfoEmploymentRepository;
import in.gov.cooptex.core.repository.EmployeeRetirementRepository;
import in.gov.cooptex.core.repository.EmployeeTransferDetailsRepository;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.dto.EmpSettlementDTO;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class EmpFinalSettlementService {

	@Autowired
	EmployeeRetirementRepository employeeRetirementRepository;

	@Autowired
	EmployeeMasterRepository employeeMasterRepository;

	@Autowired
	EmployeePersonalInfoEmploymentRepository employeePersonalInfoEmploymentRepository;

	@Autowired
	GlAccountRepository glAccountRepository;

	@Autowired
	EmployeePayrollDetailsRepository employeePayrollDetailsRepository;
	
	@Autowired
	EmployeeTransferDetailsRepository employeeTransferDetailsRepository;

	public BaseDTO getAllEmployeeFromRetirement() {
		log.info("Starts EmpFinalSettlementService.getAllEmployeeFromRetirement ");
		BaseDTO response = new BaseDTO();
		try {
			List<EmployeeMaster> empList = employeeRetirementRepository.getAllEmployee(ApprovalStatus.APPROVED);
			if(empList != null)
				log.info("EmpList size is::::::::::"+empList.size());
			response.setResponseContent(empList);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		}catch(RestException re) {
			log.error("Exception occured in EmpFinalSettlementService.getAllEmployeeFromRetirement.................",re);
			response.setStatusCode(re.getStatusCode());
		}catch(Exception e) {
			log.error("Exception occured in EmpFinalSettlementService.getAllEmployeeFromRetirement.................",e);
			response.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("Ends EmpFinalSettlementService.getAllEmployeeFromRetirement ");
		return response;
	}



	public BaseDTO getEmployeeDetails(Long id) {
		log.info("Ends EmpFinalSettlementService.getEmployeeDetails "+id);
		BaseDTO response = new BaseDTO();
		try {
			EmpSettlementDTO dto = new EmpSettlementDTO();
			//			Validate.objectNotNull(id, ErrorDescription.Employeerequired);
			EmployeePersonalInfoEmployment empPersonalInfo = employeePersonalInfoEmploymentRepository.findyByEmployeeId(id);

			Validate.objectNotNull(empPersonalInfo, ErrorDescription.EMP_NOT_FOUND);

			dto.setDesignation(empPersonalInfo.getDesignation().getName());
			dto.setDateOfBirth(empPersonalInfo.getEmployeeMaster().getDob());
			dto.setDateOfAppointment(empPersonalInfo.getDateOfAppointment());
			dto.setDateOfRetirement(empPersonalInfo.getRetirementDate());
			//				dto.setPlaceOfRetirement(empPersonalInfo.getret);
			List<EmployeePayRollDetails> payDetails = employeePayrollDetailsRepository.findPaymentDetailsByEmpId(id);

			for(EmployeePayRollDetails p : payDetails) {
				if(p.getHead().getName().equalsIgnoreCase("Basic Pay"))
					dto.setBasicPay(p.getAmount());
				if(p.getHead().getName().equalsIgnoreCase("CCA"))
					dto.setCca(p.getAmount());
				if(p.getHead().getName().equalsIgnoreCase("DA"))
					dto.setDa(p.getAmount());
				if(p.getHead().getName().equalsIgnoreCase("HRA"))
					dto.setHra(p.getAmount());
			}
			dto.setTotal(dto.getBasicPay()+dto.getCca()+dto.getDa()+dto.getHra());
			
			List<EmployeeTransferDetails> transferDetails = employeeTransferDetailsRepository.findByIdAndStatus(id);
			if(transferDetails != null)
				dto.setTransferDetails(transferDetails);
			response.setResponseContent(dto);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());

		}catch(RestException re) {
			log.error("Exception occured in EmpFinalSettlementService.getEmployeeDetails.................",re);
			response.setStatusCode(re.getStatusCode());
		}catch(Exception e) {
			log.error("Exception occured in EmpFinalSettlementService.getEmployeeDetails.................",e);
			response.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("Ends EmpFinalSettlementService.getEmployeeDetails ");
		return response;
	}
}
