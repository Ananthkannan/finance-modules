package in.gov.cooptex.finance.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;

import commonDataService.AppConfigKey;
import in.gov.cooptex.common.repository.BankMasterRepository;
import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.util.HeaderFooterPageEvent;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.dto.RegionCodeDropDownDTO;
import in.gov.cooptex.core.accounts.dto.SocietyCodeDropDownDTO;
import in.gov.cooptex.core.accounts.enums.SocietyInvoicePaymentStatus;
import in.gov.cooptex.core.accounts.enums.VoucherStatus;
import in.gov.cooptex.core.accounts.enums.VoucherTypeDetails;
import in.gov.cooptex.core.accounts.model.AccountTransaction;
import in.gov.cooptex.core.accounts.model.AccountTransactionConfig;
import in.gov.cooptex.core.accounts.model.AccountTransactionDetails;
import in.gov.cooptex.core.accounts.model.BankBranchMaster;
import in.gov.cooptex.core.accounts.model.Payment;
import in.gov.cooptex.core.accounts.model.PaymentDetails;
import in.gov.cooptex.core.accounts.model.PurchaseInvoice;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.VoucherDetails;
import in.gov.cooptex.core.accounts.model.VoucherLog;
import in.gov.cooptex.core.accounts.model.VoucherNote;
import in.gov.cooptex.core.accounts.model.VoucherType;
import in.gov.cooptex.core.accounts.repository.AccountTransactionConfigRepository;
import in.gov.cooptex.core.accounts.repository.AccountTransactionDetailsRepository;
import in.gov.cooptex.core.accounts.repository.AccountTransactionRepository;
import in.gov.cooptex.core.accounts.repository.BankBranchMasterRepository;
import in.gov.cooptex.core.accounts.repository.GlAccountRepository;
import in.gov.cooptex.core.accounts.repository.PaymentDetailsRepository;
import in.gov.cooptex.core.accounts.repository.PaymentMethodRepository;
import in.gov.cooptex.core.accounts.repository.PaymentRepository;
import in.gov.cooptex.core.accounts.repository.PaymentTypeMasterRepository;
import in.gov.cooptex.core.accounts.repository.VoucherDetailsRepository;
import in.gov.cooptex.core.accounts.repository.VoucherLogRepository;
import in.gov.cooptex.core.accounts.repository.VoucherNoteRepository;
import in.gov.cooptex.core.accounts.repository.VoucherRepository;
import in.gov.cooptex.core.accounts.repository.VoucherTypeRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.PurchaseInvoiceDetailsDTO;
import in.gov.cooptex.core.enums.PaymentCategory;
import in.gov.cooptex.core.enums.SequenceName;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.BankMaster;
import in.gov.cooptex.core.model.CircleMaster;
import in.gov.cooptex.core.model.DistrictMaster;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.PurchaseInvoiceAdjustment;
import in.gov.cooptex.core.model.SequenceConfig;
import in.gov.cooptex.core.model.SocietyInvoicePayment;
import in.gov.cooptex.core.model.SocietyInvoicePaymentDetails;
import in.gov.cooptex.core.model.SocietyInvoicePaymentLog;
import in.gov.cooptex.core.model.SocietyInvoicePaymentNote;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.CacheRepository;
import in.gov.cooptex.core.repository.CircleMasterRepository;
import in.gov.cooptex.core.repository.ContractExportPlanRepository;
import in.gov.cooptex.core.repository.DistrictMasterRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.GovtSchemePlanRepository;
import in.gov.cooptex.core.repository.PaymentModeRepository;
import in.gov.cooptex.core.repository.PurchaseInvoiceAdjustmentRepository;
import in.gov.cooptex.core.repository.PurchaseInvoiceRepository;
import in.gov.cooptex.core.repository.RetailProductionPlanRepository;
import in.gov.cooptex.core.repository.SequenceConfigRepository;
import in.gov.cooptex.core.repository.SocietyInvoicePaymentDetailsRepository;
import in.gov.cooptex.core.repository.SocietyInvoicePaymentLogRepository;
import in.gov.cooptex.core.repository.SocietyInvoicePaymentNoteRepository;
import in.gov.cooptex.core.repository.SocietyInvoicePaymentRepository;
import in.gov.cooptex.core.repository.SupplierMasterRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.util.JdbcUtil;
import in.gov.cooptex.core.utilities.AmountToWordConverter;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.LedgerPostingException;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.SocietyPaymentVoucherListDTO;
import in.gov.cooptex.finance.dto.SearchSocietyInvoiceAdjustmentDTO;
import in.gov.cooptex.finance.dto.SocietyInvoicePaymentDropDownDTO;
import in.gov.cooptex.finance.dto.SocietyInvoicePaymentDto;
import in.gov.cooptex.finance.dto.SocietyPaymentVoucherAddDTO;
import in.gov.cooptex.finance.dto.SocietyPaymentVoucherDetailsAddDTO;
import in.gov.cooptex.finance.dto.SocietyPaymentVoucherSearchResponseDTO;
import in.gov.cooptex.finance.dto.VoucherDto;
import in.gov.cooptex.operation.enums.PurchaseInvoiceStatus;
import in.gov.cooptex.operation.model.GovtSchemePlan;
import in.gov.cooptex.operation.model.GovtSchemeType;
import in.gov.cooptex.operation.model.ProductionPlanFor;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.repository.AdditionalProductionPlanRepository;
import in.gov.cooptex.operation.repository.GovtSchemeTypeRepository;
import in.gov.cooptex.operation.repository.ProductionPlanForRepository;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class SocietyPaymentVoucherService {

	@Autowired
	PurchaseInvoiceRepository purchaseInvoiceRepository;

	@Autowired
	VoucherDetailsRepository voucherDetailsRepository;

	@Autowired
	CacheRepository cacheRepository;

	@Autowired
	ResponseWrapper responseWrapper;

	@PersistenceContext
	EntityManager entityManager;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	VoucherRepository voucherRepository;

	@Autowired
	VoucherLogRepository voucherLogRepository;

	@Autowired
	PaymentModeRepository paymentModeRepository;

	@Autowired
	VoucherTypeRepository voucherTypeRepository;

	@Autowired
	UserMasterRepository userMasterRepository;

	@Autowired
	VoucherNoteRepository voucherNoteRepository;

	@Autowired
	ProductionPlanForRepository productionPlanForRepository;

	@Autowired
	GovtSchemePlanRepository govtSchemePlanRepository;

	@Autowired
	ContractExportPlanRepository contractExportPlanRepository;

	@Autowired
	AdditionalProductionPlanRepository additionalProductionPlanRepository;

	@Autowired
	RetailProductionPlanRepository retailProductionPlanRepository;

	@Autowired
	PurchaseInvoiceAdjustmentRepository purchaseInvoiceAdjustmentRepository;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	@Autowired
	GovtSchemeTypeRepository govtSchemeTypeRepository;

	@Autowired
	SupplierMasterRepository supplierMasterRepository;

	@Autowired
	CircleMasterRepository circleMasterRepository;

	@Autowired
	DistrictMasterRepository districtMasterRepository;

	@Autowired
	SocietyInvoicePaymentRepository societyInvoiceRepository;

	@Autowired
	SocietyInvoicePaymentDetailsRepository societyInvoicePaymentDetailsRepository;

	@Autowired
	SocietyInvoicePaymentLogRepository societyInvoicePaymentLogRepository;

	@Autowired
	SocietyInvoicePaymentNoteRepository societyInvoicePaymentNoteRepository;

	@Autowired
	BankBranchMasterRepository bankBranchMasterRepository;
	@Autowired
	BankMasterRepository bankMasterRepository;

	@Autowired
	PaymentRepository paymentRepository;

	@Autowired
	PaymentMethodRepository paymentMethodRepository;

	@Autowired
	PaymentDetailsRepository paymentDetailsRepository;

	@Autowired
	AccountTransactionRepository accountTransactionRepository;

	@Autowired
	AccountTransactionDetailsRepository accountTransactionDetailsRepository;

	@Autowired
	AccountTransactionConfigRepository accountTransactionConfigRepository;

	@Autowired
	GlAccountRepository glAccountRepository;

	@Autowired
	PaymentTypeMasterRepository paymentTypeMasterRepository;

	@Autowired
	EmployeeMasterRepository employeeMasterRepository;

	/**
	 *
	 * @param paginationDto
	 * @return BaseDTO
	 */
	public BaseDTO getAllPaymentVouchersLazyOld(PaginationDTO paginationDto) {
		log.info("<--Starts SocietyPaymentVoucherService .getAllPaymentVouchersLazy-->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			Integer total = 0;
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

			log.info("pagination values calculation... ");
			Integer start = paginationDto.getFirst(), pageSize = paginationDto.getPageSize();
			start = start * pageSize;
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();

			log.info("query creation... ");
			String mainQuery = "select t.status as status,t.created_date as createdDateVoucherLog , "
					+ "v.id as id, v.net_amount as netAmount , v.reference_number_prefix || v.reference_number as voucherNumber, "
					+ "(select pm.payment_mode from payment_mode pm where pm.id in (select payment_mode_id from voucher where id = v.id)) as paymentMode,"
					+ "v.created_date as createdDate,t.id AS voucherLogId " + "from voucher v, "
					+ "	(select * from voucher_log v where v.id in ( "
					+ "		select max(vl.id) from voucher_log vl group by vl.voucher_id) "
					+ "		) as t where v.id=t.voucher_id and v.name='SOCIETY_INVOICE_PAYMENT' ";

			log.info("filters to query started... ");
			if (paginationDto.getFilters() != null) {
				if (paginationDto.getFilters().get("id") != null)
					mainQuery += " and v.id='" + paginationDto.getFilters().get("id") + "'";
				// if(paginationDto.getFilters().get("voucherLogId")!=null)
				// mainQuery += " and
				// t.id='"+paginationDto.getFilters().get("voucherLogId")+"'";
				if (paginationDto.getFilters().get("voucherNumber") != null)
					mainQuery += " and upper(v.reference_number_prefix || v.reference_number) like upper('%"
							+ paginationDto.getFilters().get("voucherNumber") + "%') ";
				if (paginationDto.getFilters().get("netAmount") != null)
					mainQuery += " and v.net_amount='" + paginationDto.getFilters().get("netAmount") + "'";
				if (paginationDto.getFilters().get("status") != null)
					mainQuery += " and t.status='" + paginationDto.getFilters().get("status") + "'";
				if (paginationDto.getFilters().get("paymentMode") != null)
					mainQuery += " and upper((select pm.payment_mode from payment_mode pm where pm.id in (select payment_mode_id from voucher where id = v.id))) like upper('%"
							+ paginationDto.getFilters().get("paymentMode") + "%')";

				if (paginationDto.getFilters().get("createdDate") != null) {

					/*
					 * Date date = new Date((long)
					 * itemConsumptionDTO.getFilters().get("createdDate")); DateFormat dateFormat =
					 * new SimpleDateFormat("dd-MM-yyyy"); String strDate = dateFormat.format(date);
					 * Date minDate = dateFormat.parse(strDate); Date maxDate = new
					 * Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
					 * criteria.add(Restrictions.conjunction().add(Restrictions.ge(
					 * "itemConsumption.createdDate", minDate))
					 * .add(Restrictions.lt("itemConsumption.createdDate", maxDate)));
					 */

					/*
					 * String dateString = paginationDto.getFilters().get("createdDate").toString();
					 * Date createdDate=simpleDateFormat.parse(dateString); mainQuery +=
					 * " and date(v.created_date)=date('"+createdDate+"')";
					 */

					Date date = new Date((long) paginationDto.getFilters().get("createdDate"));
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = dateFormat.format(date);
					Date minDate = dateFormat.parse(strDate);
					// Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
					mainQuery += " and date(v.created_date) = '" + minDate + "'";
				}
			}

			log.info("query creation to fetch total records count... ");
			String countQuery = mainQuery.replace("select t.status as status,t.created_date as createdDateVoucherLog , "
					+ "v.id as id, v.net_amount as netAmount , v.reference_number_prefix || v.reference_number as voucherNumber, "
					+ "(select pm.payment_mode from payment_mode pm where pm.id in (select payment_mode_id from voucher where id = v.id)) as paymentMode,"
					+ "v.created_date as createdDate,t.id AS voucherLogId ", "select count(distinct(v.id)) as count ");
			log.info("count query... " + countQuery);
			countData = jdbcTemplate.queryForList(countQuery);
			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					total = Integer.parseInt(data.get("count").toString().replace(",", ""));
			}
			log.info("total no of records fetched... " + total);

			log.info("sorting logic invoked... ");
			if (paginationDto.getSortField() == null)
				mainQuery += " order by v.id desc limit " + pageSize + " offset " + start + ";";

			if (paginationDto.getSortField() != null && paginationDto.getSortOrder() != null) {
				log.info("Sort Field:[" + paginationDto.getSortField() + "] Sort Order:[" + paginationDto.getSortOrder()
						+ "]");

				if (paginationDto.getSortField().equals("id") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by v.id asc ";
				if (paginationDto.getSortField().equals("id") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by v.id desc ";

				if (paginationDto.getFilters().get("voucherNumber") != null)
					mainQuery += " and upper(v.reference_number_prefix || v.reference_number) like upper('%"
							+ paginationDto.getFilters().get("voucherNumber") + "%') ";

				if (paginationDto.getSortField().equals("voucherNumber")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by upper(v.reference_number_prefix || v.reference_number) asc  ";
				if (paginationDto.getSortField().equals("voucherNumber")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by upper(v.reference_number_prefix || v.reference_number) desc  ";

				// if(paginationDto.getSortField().equals("voucherLogId") &&
				// paginationDto.getSortOrder().equals("ASCENDING"))
				// mainQuery+=" order by t.id asc ";
				// if(paginationDto.getSortField().equals("voucherLogId") &&
				// paginationDto.getSortOrder().equals("DESCENDING"))
				// mainQuery+=" order by t.id desc ";

				if (paginationDto.getSortField().equals("netAmount")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by v.net_amount asc ";
				if (paginationDto.getSortField().equals("netAmount")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by v.net_amount desc ";

				if (paginationDto.getSortField().equals("createdDate")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by v.created_date asc ";
				if (paginationDto.getSortField().equals("createdDate")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by v.created_date desc ";

				if (paginationDto.getSortField().equals("status") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by t.status asc ";
				if (paginationDto.getSortField().equals("status") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by t.status desc ";

				if (paginationDto.getSortField().equals("paymentMode")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by paymentMode asc ";
				if (paginationDto.getSortField().equals("paymentMode")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by paymentMode desc ";

				mainQuery += " limit " + pageSize + " offset " + start + ";";
			}

			log.info("final main query after application filters and sorting....." + mainQuery);
			List<VoucherLog> voucherLogList = new ArrayList<>();
			listofData = jdbcTemplate.queryForList(mainQuery);
			List<SocietyPaymentVoucherListDTO> societyPaymentVoucherListDTOList = new ArrayList<>();
			for (Map<String, Object> data : listofData) {
				SocietyPaymentVoucherListDTO societyPaymentVoucherListDTO = new SocietyPaymentVoucherListDTO();
				VoucherLog voucherLog = new VoucherLog();
				// Voucher voucher = new Voucher();
				// PaymentMode paymentMode = new PaymentMode();

				if (data.get("voucherLogId") != null) {
					voucherLog.setId(Long.parseLong(data.get("voucherLogId").toString()));
					// societyPaymentVoucherListDTO.getVoucherId();
				}
				if (data.get("id") != null) {
					// voucher.setId(Long.valueOf(data.get("id").toString()));
					societyPaymentVoucherListDTO.setId(Long.valueOf(data.get("id").toString()));
				}
				if (data.get("voucherNumber") != null) {
					// voucher.setId(Long.valueOf(data.get("id").toString()));
					societyPaymentVoucherListDTO.setVoucherNumber(String.valueOf(data.get("voucherNumber").toString()));
				}
				if (data.get("netAmount") != null) {
					// voucher.setNetAmount(Double.valueOf(data.get("netAmount").toString()));
					societyPaymentVoucherListDTO.setNetAmount(Double.valueOf(data.get("netAmount").toString()));
				}
				if (data.get("paymentMode") != null) {
					// paymentMode.setPaymentMode(data.get("paymentMode").toString());
					societyPaymentVoucherListDTO.setPaymentMode(data.get("paymentMode").toString());

				}
				if (data.get("status") != null) {
					// voucherLog.setStatus(VoucherStatus.valueOf((String) data.get("status")));
					societyPaymentVoucherListDTO.setStatus((String) data.get("status"));
				}

				if (data.get("createdDate") != null) {
					// voucher.setCreatedDate((Date) data.get("createdDate"));
					String createdDateVoucher = data.get("createdDate").toString();
					// voucher.setCreatedDate(simpleDateFormat.parse(createdDateVoucher));
					societyPaymentVoucherListDTO.setCreatedDate(simpleDateFormat.parse(createdDateVoucher));
				}

				if (data.get("createdDateVoucherLog") != null) {
					voucherLog.setCreatedDate((Date) data.get("createdDateVoucherLog"));
					String createdDateVoucherLog = data.get("createdDateVoucherLog").toString();
					voucherLog.setCreatedDate(simpleDateFormat.parse(createdDateVoucherLog));

					// voucherLog.setCreatedDate(simpleDateFormat.format((Date)data.get("createdDateVoucherLog")));
				}

				societyPaymentVoucherListDTOList.add(societyPaymentVoucherListDTO);
				// voucher.setPaymentMode(paymentMode);
				// voucherLog.setVoucher(voucher);
				// voucherLogList.add(voucherLog);

			}
			log.info("Returning getAllPaymentVouchersLazy list...." + societyPaymentVoucherListDTOList.size());
			log.info("Total records present in getAllPaymentVouchersLazy..." + total);
			baseDTO.setResponseContents(societyPaymentVoucherListDTOList);
			baseDTO.setTotalRecords(total);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (RestException re) {
			log.warn("RestException :: SocietyPaymentVoucherService .getAllPaymentVouchersLazy ", re);
			baseDTO.setStatusCode(re.getStatusCode());
		} catch (Exception exp) {
			log.error("Exception :: SocietyPaymentVoucherService .getAllPaymentVouchersLazy ", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<--Ends SocietyPaymentVoucherService .getAllPaymentVouchersLazy-->");
		return baseDTO;
	}

	public BaseDTO getAllPaymentVouchersLazy(PaginationDTO paginationDto) {
		log.info("<--Starts SocietyPaymentVoucherService .getAllPaymentVouchersLazy-->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			Integer total = 0;
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

			log.info("pagination values calculation... ");
			Integer start = paginationDto.getFirst(), pageSize = paginationDto.getPageSize();
			start = start * pageSize;
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();

			log.info("query creation... ");
			String mainQuery = "select s.invoice_to_date as invoicetodate,s.invoice_from_date as invfromdate ,pm.payment_mode as paymentmode, "
					+ "vl.stage as stages, s.created_date as createddate, s.id as id,concat(v.reference_number_prefix,'-',v.reference_number) as voucherNo "
					+ "from society_invoice_payment s join payment_mode pm on s.payment_mode_id=pm.id "
					+ "left join voucher v on s.voucher_id=v.id "
					+ "join society_invoice_payment_log vl on vl.society_invoice_payment_id=s.id "
					+ "join (select society_invoice_payment_id,max(id) as id " + "from society_invoice_payment_log "
					+ "group by society_invoice_payment_id) vl1 on vl1.id=vl.id ";

			log.info("filters to query started... ");
			if (paginationDto.getFilters() != null) {
				if (paginationDto.getFilters().get("invoiceFromDate") != null) {
					Date date = new Date((long) paginationDto.getFilters().get("invoiceFromDate"));
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = dateFormat.format(date);
					Date invoiceFromDate = dateFormat.parse(strDate);
					mainQuery += " and date(s.invoice_from_date) = '" + invoiceFromDate + "'";

				}

				if (paginationDto.getFilters().get("invoiceToDate") != null) {
					Date date = new Date((long) paginationDto.getFilters().get("invoiceToDate"));
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = dateFormat.format(date);
					Date invoiceToDate = dateFormat.parse(strDate);
					mainQuery += " and date(s.invoice_to_date) = '" + invoiceToDate + "'";
				}

				if (paginationDto.getFilters().get("voucherNumber") != null)
					mainQuery += " and concat(v.reference_number_prefix,'-',v.reference_number) like '%"
							+ paginationDto.getFilters().get("voucherNumber") + "%'";

				if (paginationDto.getFilters().get("status") != null)
					mainQuery += " and vl.stage='" + paginationDto.getFilters().get("status") + "'";

				if (paginationDto.getFilters().get("paymentmode") != null)
					mainQuery += " and upper((select pm.payment_mode from payment_mode pm where pm.id in (select payment_mode_id from society_invoice_payment where id = s.id))) like upper('%"
							+ paginationDto.getFilters().get("paymentmode") + "%')";

				if (paginationDto.getFilters().get("createdDate") != null) {
					Date date = new Date((long) paginationDto.getFilters().get("createdDate"));
					DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					String strDate = dateFormat.format(date);
					Date minDate = dateFormat.parse(strDate);
					// Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
					mainQuery += " and date(s.created_date) = '" + minDate + "'";
				}
			}

			log.info("query creation to fetch total records count... ");
			String countQuery = "select count(t1.*) from (select from society_invoice_payment s "
					+ "join payment_mode pm on s.payment_mode_id=pm.id "
					+ "join society_invoice_payment_log vl on vl.society_invoice_payment_id=s.id "
					+ "join (select society_invoice_payment_id,max(id) as id "
					+ "from society_invoice_payment_log group by society_invoice_payment_id) vl1 on vl1.id=vl.id) as t1 ";

			log.info("count query... " + countQuery);
			countData = jdbcTemplate.queryForList(countQuery);
			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					total = Integer.parseInt(data.get("count").toString().replace(",", ""));
			}
			log.info("total no of records fetched... " + total);

			log.info("sorting logic invoked... ");

			if (paginationDto.getSortField() == null)
				mainQuery += " order by s.id desc limit " + pageSize + " offset " + start + ";";

			if (paginationDto.getSortField() != null && paginationDto.getSortOrder() != null) {
				log.info("Sort Field:[" + paginationDto.getSortField() + "] Sort Order:[" + paginationDto.getSortOrder()
						+ "]");

				if (paginationDto.getSortField().equals("id") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by s.id asc ";
				if (paginationDto.getSortField().equals("id") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by s.id desc ";

				if (paginationDto.getSortField().equals("invfromdate")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by s.invoice_from_date asc ";
				if (paginationDto.getSortField().equals("invfromdate")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by s.invoice_from_date desc ";

				if (paginationDto.getSortField().equals("invoicetodate")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by s.invoice_to_date asc ";
				if (paginationDto.getSortField().equals("invoicetodate")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by s.invoice_to_date desc ";

				if (paginationDto.getSortField().equals("stage") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by t.stage asc ";
				if (paginationDto.getSortField().equals("stage") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by t.stage desc ";

				if (paginationDto.getSortField().equals("createdDate")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by s.created_date asc ";
				if (paginationDto.getSortField().equals("createdDate")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by s.created_date desc ";

				if (paginationDto.getSortField().equals("paymentMode")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by paymentMode asc ";
				if (paginationDto.getSortField().equals("paymentMode")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by paymentMode desc ";

				if (paginationDto.getSortField().equals("voucherNo")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by voucherNo asc ";
				if (paginationDto.getSortField().equals("voucherNo")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by voucherNo desc ";

				mainQuery += " limit " + pageSize + " offset " + start + ";";
			}

			log.info("final main query after application filters and sorting....." + mainQuery);
			listofData = jdbcTemplate.queryForList(mainQuery);
			List<SocietyPaymentVoucherListDTO> societyPaymentVoucherListDTOList = new ArrayList<>();
			for (Map<String, Object> data : listofData) {
				SocietyPaymentVoucherListDTO societyPaymentVoucherListDTO = new SocietyPaymentVoucherListDTO();

				if (data.get("id") != null) {
					societyPaymentVoucherListDTO.setId(Long.valueOf(data.get("id").toString()));
				}
				if (data.get("invfromdate") != null) {
					societyPaymentVoucherListDTO.setInvoiceFromDate((Date) (data.get("invfromdate")));
				}
				if (data.get("invoicetodate") != null) {
					societyPaymentVoucherListDTO.setInvoiceToDate((Date) (data.get("invoicetodate")));
				}
				if (data.get("paymentmode") != null) {
					societyPaymentVoucherListDTO.setPaymentMode(data.get("paymentmode").toString());

				}
				if (data.get("stages") != null) {
					societyPaymentVoucherListDTO.setStatus((String) data.get("stages"));
				}

				if (data.get("createddate") != null) {
					String createdDateVoucher = data.get("createddate").toString();
					societyPaymentVoucherListDTO.setCreatedDate(simpleDateFormat.parse(createdDateVoucher));
				}
				if (data.get("voucherNo") != null && !data.get("voucherNo").equals("-")) {
					societyPaymentVoucherListDTO.setVoucherNumber((String) data.get("voucherNo"));
				}

				societyPaymentVoucherListDTOList.add(societyPaymentVoucherListDTO);
			}
			log.info("Returning getAllPaymentVouchersLazy list...." + societyPaymentVoucherListDTOList.size());
			log.info("Total records present in getAllPaymentVouchersLazy..." + total);
			baseDTO.setResponseContents(societyPaymentVoucherListDTOList);
			baseDTO.setTotalRecords(total);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (RestException re) {
			log.warn("RestException :: SocietyPaymentVoucherService .getAllPaymentVouchersLazy ", re);
			baseDTO.setStatusCode(re.getStatusCode());
		} catch (Exception exp) {
			log.error("Exception :: SocietyPaymentVoucherService .getAllPaymentVouchersLazy ", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<--Ends SocietyPaymentVoucherService .getAllPaymentVouchersLazy-->");
		return baseDTO;
	}

	public List<PurchaseInvoice> getAllPendingInvoicesBasedOnSociety(Long supplierId, Date startDate, Date endDate) {
		log.info("<--Starts SocietyPaymentVoucherService .getAllPendingInvoicesBasedOnSociety-->");

		List<PurchaseInvoice> purchaseInvoicesList = null;
		try {

			List<String> statuses = new ArrayList<>();
			statuses.add("APPROVED");
			statuses.add("PARTIALLY_PAID");

			purchaseInvoicesList = purchaseInvoiceRepository
					.findAllBySupplierMasterIdAndInvoiceDateBetweenAndStatusInOrderByIdAsc(supplierId, startDate,
							endDate, statuses);
			if (purchaseInvoicesList != null && !purchaseInvoicesList.isEmpty()) {
				log.info("getAllPendingInvoicesBasedOnSociety :: purchaseInvoicesList size==> "
						+ purchaseInvoicesList.size());
			} else {
				log.error("purchaseInvoicesList is null ");
			}
			log.info("<--Ends SocietyPaymentVoucherService .getAllPendingInvoicesBasedOnSociety-->");
		} catch (Exception e) {
			log.error("getAllPendingInvoicesBasedOnSociety :: exception==> ", e);
		}

		return purchaseInvoicesList;

	}

	// @Transactional(rollbackFor = Exception.class)
	// public BaseDTO saveSocietyPaymentVoucher(SocietyPaymentVoucherAddDTO
	// societyPaymentVoucherAddDTO) {
	// log.info("<--Starts SocietyPaymentVoucherService
	// .saveSocietyPaymentVoucher-->");
	// BaseDTO response = new BaseDTO();
	// try {
	// log.info("==>> Inside saveSocietyPaymentVoucher <<== Start ");
	//
	// Voucher voucher = new Voucher();
	//
	// voucher.setFromDate(societyPaymentVoucherAddDTO.getInvoiceFromDate());
	// voucher.setToDate(societyPaymentVoucherAddDTO.getInvoiceToDate());
	// voucher.setPaymentMode(paymentModeRepository.findById(societyPaymentVoucherAddDTO.getPaymentModeId()));
	// voucher.setNetAmount(societyPaymentVoucherAddDTO.getVoucherNetAmount());
	// voucher.setVoucherType(voucherTypeRepository.findOne(Long.valueOf(1)));
	// voucher.setName("Payment");
	// Calendar calendar = Calendar.getInstance();
	//
	// Formatter fmt = new Formatter();
	// Calendar cal = Calendar.getInstance();
	// fmt = new Formatter();
	// fmt.format("%tb", cal);
	// System.out.println(fmt);
	//
	//
	// int year = calendar.get(Calendar.YEAR);
	//// int month = calendar.get(Calendar.MONTH);
	// String referenceNumberPrefix = "HO-P"+fmt+year;
	// voucher.setReferenceNumberPrefix(referenceNumberPrefix);
	// voucher.setReferenceNumber(voucherRepository.findMaxReferenceNumberForPaymentVoucher()
	// + 1);
	// voucher.setNarration(societyPaymentVoucherAddDTO.getNarration());
	// voucherRepository.save(voucher);
	//
	// for (SocietyPaymentVoucherDetailsAddDTO societyPaymentVoucherDetailsAddDTO
	// :societyPaymentVoucherAddDTO.getSocietyPaymentVoucherDetailsAddDTOList()) {
	//
	// VoucherDetails voucherDetails = new VoucherDetails();
	//
	// voucherDetails.setVoucher(voucher);
	// voucherDetails.setPurchaseInvoice(purchaseInvoiceRepository.findOne(societyPaymentVoucherDetailsAddDTO.getPurchaseInvoiceId()));
	// voucherDetails.setAmount(societyPaymentVoucherDetailsAddDTO.getPurchaseInvoiceVoucherAmount());
	// voucherDetailsRepository.save(voucherDetails);
	// }
	//
	// VoucherLog voucherLog = new VoucherLog();
	// voucherLog.setVoucher(voucher);
	// voucherLog.setStatus(VoucherStatus.SUBMITTED);
	// voucherLog.setUserMaster(userMasterRepository.findOne(societyPaymentVoucherAddDTO.getUserId()));
	// voucherLogRepository.save(voucherLog);
	//
	// VoucherNote voucherNote = new VoucherNote();
	// voucherNote.setVoucher(voucher);
	// voucherNote.setNote(societyPaymentVoucherAddDTO.getVoucherNoteComment());
	// voucherNote.setFinalApproval(societyPaymentVoucherAddDTO.getFinalApproval());
	// voucherNote.setForwardTo(userMasterRepository.findOne(societyPaymentVoucherAddDTO.getForwardToUserId()));
	// voucherNoteRepository.save(voucherNote);
	//
	//
	// } catch (Exception e) {
	// log.error("Exception While performing saveSocietyPaymentVoucher ===>>", e);
	// }
	// log.info("==>> BankBranchMasterService Inside saveSocietyPaymentVoucher <<==
	// End ");
	// response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
	// log.info("<--Ends SocietyPaymentVoucherService
	// .saveSocietyPaymentVoucher-->");
	// return response;
	// }

	// Dev in progress

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	SequenceConfigRepository sequenceConfigRepository;

	@Autowired
	LoginService loginService;

	@Transactional(rollbackFor = Exception.class)
	public BaseDTO saveSocietyPaymentVoucherOld(SocietyPaymentVoucherAddDTO societyPaymentVoucherAddDTO) {
		log.info("<--Starts SocietyPaymentVoucherService .saveSocietyPaymentVoucher-->");
		BaseDTO response = new BaseDTO();
		try {
			log.info("==>>  Inside saveSocietyPaymentVoucher <<== Start ");

			Validate.notNullOrEmpty(societyPaymentVoucherAddDTO.getVoucherNetAmount(),
					ErrorDescription.SOCIETY_PAYMENT_TOTAL_AMOUNT_NOT_EMPTY);
			Validate.notNullOrEmpty(societyPaymentVoucherAddDTO.getForwardToUserId(),
					ErrorDescription.FORWARD_TO_USER_EMPTY);
			/*
			 * Validate.notNullOrEmpty(societyPaymentVoucherAddDTO.getInvoiceFromDate(),
			 * ErrorDescription.RETIREMENT_FROMDATE_NULL);
			 * Validate.notNullOrEmpty(societyPaymentVoucherAddDTO.getInvoiceToDate(),
			 * ErrorDescription.RETIREMENT_TODATE_NULL);
			 */
			// Validate.notNullOrEmpty(societyPaymentVoucherAddDTO.getNarration(),ErrorDescription.VOUCHER_NARRATION_EMPTY);
			Validate.notNullOrEmpty(societyPaymentVoucherAddDTO.getVoucherNoteComment(),
					ErrorDescription.SOCIETY_REQUEST_REG_NOTE_EMPTY);
			Validate.notNullOrEmpty(societyPaymentVoucherAddDTO.getPaymentModeId(),
					ErrorDescription.VOUCHER_PAYMENT_MODE_IS_REQUIRED);
			Validate.notNullOrEmpty(societyPaymentVoucherAddDTO.getFinalApproval(), ErrorDescription.FORWARD_FOR_EMPTY);

			Voucher voucher = null;
			Formatter fmt = new Formatter();
			Calendar cal = Calendar.getInstance();
			fmt = new Formatter();
			fmt.format("%tb", cal);
			log.info(fmt);
			Calendar calendar = Calendar.getInstance();

			int year = calendar.get(Calendar.YEAR);
			log.info("voucher id==>" + societyPaymentVoucherAddDTO.getVoucherId());
			if (societyPaymentVoucherAddDTO.getVoucherId() != null) {
				voucher = voucherRepository.findOne(societyPaymentVoucherAddDTO.getVoucherId());
				voucher.setReferenceNumberPrefix(voucher.getReferenceNumberPrefix());
			} else {
				voucher = new Voucher();
				// String referenceNumberPrefix = "HO-P" + fmt + year;
				// voucher.setReferenceNumberPrefix(referenceNumberPrefix);
			}
			voucher.setFromDate(new Date());
			voucher.setToDate(new Date());
			voucher.setPaymentMode(paymentModeRepository.findById(societyPaymentVoucherAddDTO.getPaymentModeId()));
			voucher.setNetAmount(societyPaymentVoucherAddDTO.getVoucherNetAmount());
			VoucherType voucherType = voucherTypeRepository.findByName(VoucherTypeDetails.Payment.toString());
			voucher.setVoucherType(voucherType);
			// voucher.setName("Payment");
			voucher.setName(VoucherTypeDetails.SOCIETY_INVOICE_PAYMENT.toString());
			voucher.setReferenceNumber(voucherRepository.findMaxReferenceNumberForPaymentVoucher() + 1);
			voucher.setNarration(societyPaymentVoucherAddDTO.getNarration());

			//////////////////////
			EntityMaster loginUserEntity = entityMasterRepository
					.findByUserRegion(loginService.getCurrentUser().getId());
			SequenceConfig sequenceConfig = sequenceConfigRepository
					.findBySequenceName(SequenceName.valueOf(SequenceName.SOCIETY_INVOICE_PAYMENT.name()));
			if (sequenceConfig == null) {
				throw new RestException(ErrorDescription.SEQUENCE_CONFIG_NOT_EXIST);
			}
			String paymentNumberPrefix = loginUserEntity.getCode() + "-" + sequenceConfig.getPrefix()
					+ AppUtil.getCurrentYearString() + AppUtil.getCurrentMonthString();
			voucher.setReferenceNumberPrefix(paymentNumberPrefix);
			voucher.setReferenceNumber(sequenceConfig.getCurrentValue());

			//////////////////////////////////////////
			Voucher finalVoucher = voucherRepository.save(voucher);

			log.info("finalVoucher==>" + finalVoucher);

			for (SocietyPaymentVoucherDetailsAddDTO societyPaymentVoucherDetailsAddDTO : societyPaymentVoucherAddDTO
					.getSocietyPaymentVoucherDetailsAddDTOList()) {

				if (societyPaymentVoucherDetailsAddDTO.getPurchaseInvoiceVoucherAmount() != null
						&& societyPaymentVoucherDetailsAddDTO.getPurchaseInvoiceVoucherAmount() > 0) {
					VoucherDetails voucherDetails = null;
					log.info("voucher details id==>" + societyPaymentVoucherDetailsAddDTO.getVocherDetailsId());
					if (societyPaymentVoucherDetailsAddDTO != null
							&& societyPaymentVoucherDetailsAddDTO.getVocherDetailsId() != null) {
						voucherDetails = voucherDetailsRepository
								.findOne(societyPaymentVoucherDetailsAddDTO.getVocherDetailsId());
					} else {
						voucherDetails = new VoucherDetails();
					}
					voucherDetails.setVoucher(finalVoucher);
					voucherDetails.setPurchaseInvoice(purchaseInvoiceRepository
							.getOne(societyPaymentVoucherDetailsAddDTO.getPurchaseInvoiceId()));
					voucherDetails.setAmount(societyPaymentVoucherDetailsAddDTO.getPurchaseInvoiceVoucherAmount());
					voucherDetailsRepository.save(voucherDetails);
				}
			}

			VoucherLog voucherLog = new VoucherLog();
			voucherLog.setVoucher(finalVoucher);
			voucherLog.setStatus(VoucherStatus.SUBMITTED);
			voucherLog.setUserMaster(userMasterRepository.findOne(societyPaymentVoucherAddDTO.getUserId()));
			voucherLogRepository.save(voucherLog);

			VoucherNote voucherNote = new VoucherNote();
			voucherNote.setVoucher(finalVoucher);
			voucherNote.setNote(societyPaymentVoucherAddDTO.getVoucherNoteComment());
			voucherNote.setFinalApproval(societyPaymentVoucherAddDTO.getFinalApproval());
			voucherNote.setForwardTo(userMasterRepository.findOne(societyPaymentVoucherAddDTO.getForwardToUserId()));
			voucherNoteRepository.save(voucherNote);

			sequenceConfig.setCurrentValue(sequenceConfig.getCurrentValue() + 1);
			log.info("Sequence Config Value is" + sequenceConfig);
			sequenceConfigRepository.save(sequenceConfig);

			response.setResponseContent(finalVoucher);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (DataIntegrityViolationException divEX) {
			log.warn(
					"<<===DataIntegrityViolationException :: SocietyPaymentVoucherService .saveSocietyPaymentVoucher() ===>>",
					divEX);

		} catch (ObjectOptimisticLockingFailureException lockEx) {
			log.warn(
					"====>> ObjectOptimisticLockingFailureException ::  SocietyPaymentVoucherService .saveSocietyPaymentVoucher()<<====",
					lockEx);
			response.setStatusCode(ErrorDescription.CANNOT_UPDATE_LOCKED_RECORD.getErrorCode());
			return response;
		} catch (RestException re) {
			log.error("RestException :: SocietyPaymentVoucherService .saveSocietyPaymentVoucher()", re);
			response.setStatusCode(re.getStatusCode());
			return response;
		} catch (Exception e) {
			log.error("Exception :: SocietyPaymentVoucherService .saveSocietyPaymentVoucher()", e);
			response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return response;
		}
		response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		log.info("<--Ends SocietyPaymentVoucherService .saveSocietyPaymentVoucher-->");
		return response;
	}

	// @Transactional(rollbackFor = Exception.class)
	public BaseDTO saveSocietyPaymentVoucher(SocietyPaymentVoucherAddDTO societyPaymentVoucherAddDTO) {
		log.info("<--Starts SocietyPaymentVoucherService .saveSocietyPaymentVoucher-->");
		BaseDTO response = new BaseDTO();
		try {
			log.info("==>>  Inside saveSocietyPaymentVoucher <<== Start ");
			SocietyInvoicePayment paymentobj = new SocietyInvoicePayment();
			Validate.notNullOrEmpty(societyPaymentVoucherAddDTO.getVoucherNetAmount(),
					ErrorDescription.SOCIETY_PAYMENT_TOTAL_AMOUNT_NOT_EMPTY);
			Validate.notNullOrEmpty(societyPaymentVoucherAddDTO.getForwardToUserId(),
					ErrorDescription.FORWARD_TO_USER_EMPTY);
			Validate.notNullOrEmpty(societyPaymentVoucherAddDTO.getVoucherNoteComment(),
					ErrorDescription.SOCIETY_REQUEST_REG_NOTE_EMPTY);
			Validate.notNullOrEmpty(societyPaymentVoucherAddDTO.getPaymentModeId(),
					ErrorDescription.VOUCHER_PAYMENT_MODE_IS_REQUIRED);
			Validate.notNullOrEmpty(societyPaymentVoucherAddDTO.getFinalApproval(), ErrorDescription.FORWARD_FOR_EMPTY);

			paymentobj.setInvoiceFromDate(societyPaymentVoucherAddDTO.getInvoiceFromDate());
			paymentobj.setInvoiceToDate(societyPaymentVoucherAddDTO.getInvoiceToDate());
			paymentobj.setPaymentmodeid(societyPaymentVoucherAddDTO.getPaymentModeId());
			//////////////////////////////////////////
			SocietyInvoicePayment invoicePayment = societyInvoiceRepository.save(paymentobj);

			for (SocietyPaymentVoucherDetailsAddDTO societyPaymentVoucherDetailsAddDTO : societyPaymentVoucherAddDTO
					.getSocietyPaymentVoucherDetailsAddDTOList()) {
				SocietyInvoicePaymentDetails societyInvoiceDetails = null;
				if (societyPaymentVoucherDetailsAddDTO.getPurchaseInvoiceVoucherAmount() != null
						&& societyPaymentVoucherDetailsAddDTO.getPurchaseInvoiceVoucherAmount() > 0) {

					log.info("voucher details id==>" + societyPaymentVoucherDetailsAddDTO.getVocherDetailsId());
					if (societyPaymentVoucherDetailsAddDTO != null
							&& societyPaymentVoucherDetailsAddDTO.getVocherDetailsId() != null) {
						societyInvoiceDetails = societyInvoicePaymentDetailsRepository
								.findOne(societyPaymentVoucherDetailsAddDTO.getVocherDetailsId());
					} else {
						societyInvoiceDetails = new SocietyInvoicePaymentDetails();
					}

					societyInvoiceDetails
							.setAdjustedamount(societyPaymentVoucherDetailsAddDTO.getTotalAdjustedAmount());
					societyInvoiceDetails
							.setAmountwithouttax(societyPaymentVoucherDetailsAddDTO.getAmountWithountTax());
					societyInvoiceDetails.setPayableamount(societyPaymentVoucherDetailsAddDTO.getTotalPaybleAmount());
					societyInvoiceDetails.setBalanceamount(societyPaymentVoucherDetailsAddDTO.getBalanceAmount());
					societyInvoiceDetails.setTaxamount(societyPaymentVoucherDetailsAddDTO.getTax());
					societyInvoiceDetails.setPaidamount(societyPaymentVoucherDetailsAddDTO.getTotalAmountPaid());
					societyInvoiceDetails
							.setPaymentamount(societyPaymentVoucherDetailsAddDTO.getTotalInvoiceAmountToBePaid());
					societyInvoiceDetails.setSocietyInvoicePayment(invoicePayment);
					societyInvoiceDetails.setPaymentpercetnage(societyPaymentVoucherDetailsAddDTO.getPercentage());
					societyInvoiceDetails.setPurchaseInvoice(purchaseInvoiceRepository
							.getOne(societyPaymentVoucherDetailsAddDTO.getPurchaseInvoiceId()));
					PurchaseInvoice puchase = purchaseInvoiceRepository
							.getOne(societyInvoiceDetails.getPurchaseInvoice().getId());
					if (puchase != null && puchase.getSupplierMaster().getId() != null) {

						SupplierMaster supplierMaster = supplierMasterRepository
								.getOne(puchase.getSupplierMaster().getId());
						if (supplierMaster != null) {
							societyInvoiceDetails.setSupplierMaster(supplierMaster);
							societyInvoiceDetails.setDistrictcode(supplierMaster.getDistriccode());
							societyInvoiceDetails.setLoomtype(supplierMaster.getLoomType());
						}
						if (supplierMaster.getBranchMaster() != null
								&& supplierMaster.getBranchMaster().getId() != null) {
							BankBranchMaster bankbranchmaster = bankBranchMasterRepository
									.getOne(supplierMaster.getBranchMaster().getId());
							if (bankbranchmaster != null) {
								societyInvoiceDetails.setBranchcode(bankbranchmaster.getBranchCode());
								societyInvoiceDetails.setBranchname(bankbranchmaster.getBranchName());
							}
							if (bankbranchmaster.getBankMaster() != null
									&& bankbranchmaster.getBankMaster().getId() != null) {
								BankMaster bankmaster = bankMasterRepository
										.getOne(bankbranchmaster.getBankMaster().getId());
								if (bankmaster != null) {
									societyInvoiceDetails.setBankname(bankmaster.getBankName());
								}
							}
						}

						if (supplierMaster.getCircleMaster() != null
								&& supplierMaster.getCircleMaster().getId() != null) {
							CircleMaster circlemaster = circleMasterRepository
									.findOne(societyInvoiceDetails.getSupplierMaster().getCircleMaster().getId());
							if (circlemaster != null) {
								societyInvoiceDetails.setCircleid(circlemaster.getId());
							}
						}

					}
					societyInvoicePaymentDetailsRepository.save(societyInvoiceDetails);
					updatePurchaseInvoice(puchase.getId());
					updatePurchaseInvoiceCurrentSocietyInvoiceStatus(puchase.getId());
				}
			}

			SocietyInvoicePaymentLog societyInvPaymentlog = new SocietyInvoicePaymentLog();
			societyInvPaymentlog.setSocietyInvoicePayment(invoicePayment);
			societyInvPaymentlog.setStage(SocietyInvoicePaymentStatus.SUBMITTED);
			societyInvoicePaymentLogRepository.save(societyInvPaymentlog);

			SocietyInvoicePaymentNote societyInvPaymentNote = new SocietyInvoicePaymentNote();
			societyInvPaymentNote.setSocietyInvoicePayment(invoicePayment);
			societyInvPaymentNote.setNote(societyPaymentVoucherAddDTO.getVoucherNoteComment());
			societyInvPaymentNote.setFinalApproval(societyPaymentVoucherAddDTO.getFinalApproval());
			societyInvPaymentNote
					.setForwardto(userMasterRepository.findOne(societyPaymentVoucherAddDTO.getForwardToUserId()));
			societyInvoicePaymentNoteRepository.save(societyInvPaymentNote);

			response.setResponseContent(invoicePayment);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (DataIntegrityViolationException divEX) {
			log.warn(
					"<<===DataIntegrityViolationException :: SocietyPaymentVoucherService .saveSocietyPaymentVoucher() ===>>",
					divEX);

		} catch (ObjectOptimisticLockingFailureException lockEx) {
			log.warn(
					"====>> ObjectOptimisticLockingFailureException ::  SocietyPaymentVoucherService .saveSocietyPaymentVoucher()<<====",
					lockEx);
			response.setStatusCode(ErrorDescription.CANNOT_UPDATE_LOCKED_RECORD.getErrorCode());
			return response;
		} catch (RestException re) {
			log.error("RestException :: SocietyPaymentVoucherService .saveSocietyPaymentVoucher()", re);
			response.setStatusCode(re.getStatusCode());
			return response;
		} catch (Exception e) {
			log.error("Exception :: SocietyPaymentVoucherService .saveSocietyPaymentVoucher()", e);
			response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return response;
		}
		response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		log.info("<--Ends SocietyPaymentVoucherService .saveSocietyPaymentVoucher-->");
		return response;
	}

	private void updatePurchaseInvoiceCurrentSocietyInvoiceStatus(Long puchase_invoice_id) {
		jdbcTemplate.update("UPDATE purchase_invoice SET invoice_status='" + SocietyInvoicePaymentStatus.SUBMITTED
				+ "' WHERE id IN(" + puchase_invoice_id + ");");

	}

	private void updatePurchaseInvoice(Long puchase_invoice_id) {
		jdbcTemplate.update("UPDATE purchase_invoice SET status='" + SocietyInvoicePaymentStatus.PAYMENTPENDING
				+ "' WHERE id IN(" + puchase_invoice_id + ");");

	}

	public BaseDTO loadProductionPlanForList() {
		BaseDTO baseDTO = new BaseDTO();
		List<ProductionPlanFor> productionPlanForList = null;
		try {
			productionPlanForList = new ArrayList<>();
			productionPlanForList = productionPlanForRepository.findAll();
			log.info("SocietyPaymentVoucherService loadProductionPlanForList -------" + productionPlanForList.size());
			baseDTO.setResponseContents(productionPlanForList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("SocietyPaymentVoucherService loadProductionPlanForList exce----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO loadGovtSchemePlan() {
		log.info("SocietyPaymentVoucherService loadProductionPlanForList starts-------");
		BaseDTO baseDTO = new BaseDTO();
		List<GovtSchemePlan> govtSchemePlanList = null;
		try {
			govtSchemePlanList = new ArrayList<>();
			govtSchemePlanList = govtSchemePlanRepository.getAllGovtSchemePlan();
			log.info("SocietyPaymentVoucherService loadProductionPlanForList -------" + govtSchemePlanList.size());
			baseDTO.setResponseContents(govtSchemePlanList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("SocietyPaymentVoucherService loadGovtSchemePlan exce----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO getProductionPlanByPlanType(String planCode) {
		log.info("SocietyPaymentVoucherService getProductionPlanByPlanType starts-------");
		BaseDTO baseDTO = new BaseDTO();
		List<RegionCodeDropDownDTO> productionPlanList = new ArrayList<RegionCodeDropDownDTO>();
		List<Object[]> planList = new ArrayList<Object[]>();
		try {
			if (planCode != null) {
				log.info("inside if getProductionPlanByPlanType--------" + planCode);
				switch (planCode) {
				case "CONTRACT":
					planList = contractExportPlanRepository.getPlanNameandCode("Contract");
					break;
				case "EXPORT":
					planList = contractExportPlanRepository.getPlanNameandCode("Export");
					break;

				case "ADD_PROD_PLAN":
					planList = additionalProductionPlanRepository.getPlanNameandCode();
					break;

				case "RETAIL":
					planList = retailProductionPlanRepository.getPlanNameandCode();
					break;

				default:
					log.info("productionPlanFor code is not found");
					break;
				}
				log.info("planName list size-------" + planList.size());

				if (planList.size() > 0) {
					planList.forEach(plan -> {
						RegionCodeDropDownDTO dto = new RegionCodeDropDownDTO();
						dto.setId(new Long(plan[0].toString()));
						dto.setName((String) plan[1]);
						dto.setCode((String) plan[2]);
						productionPlanList.add(dto);
					});
				}

				log.info("getProductionPlanByPlanType productionPlanList---" + productionPlanList.size());
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				baseDTO.setResponseContents(productionPlanList);
			}
		} catch (Exception e) {
			log.error("SocietyPaymentVoucherService getProductionPlanByPlanType exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("SocietyPaymentVoucherService getProductionPlanByPlanType ends-------");
		return baseDTO;
	}

	public BaseDTO getProductionPlanBySchemeType(String schemeCode) {
		log.info("SocietyPaymentVoucherService getProductionPlanBySchemeType starts-------");
		BaseDTO baseDTO = new BaseDTO();
		List<RegionCodeDropDownDTO> productionPlanList = new ArrayList<RegionCodeDropDownDTO>();
		List<Object[]> planList = new ArrayList<Object[]>();
		try {
			if (schemeCode != null) {
				planList = govtSchemePlanRepository.getPlanName(schemeCode);
				log.info("planName list size-------" + planList.size());

				if (planList.size() > 0) {
					planList.forEach(plan -> {
						RegionCodeDropDownDTO dto = new RegionCodeDropDownDTO();
						dto.setId(new Long(plan[0].toString()));
						dto.setName((String) plan[1]);
						productionPlanList.add(dto);
					});
				}

				log.info("getProductionPlanByPlanType productionPlanList---" + productionPlanList.size());
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				baseDTO.setResponseContents(productionPlanList);
			}
		} catch (Exception e) {
			log.info("getProductionPlanBySchemeType exception---", e);
		}
		log.info("SocietyPaymentVoucherService getProductionPlanBySchemeType ends-------");
		return baseDTO;
	}

	public BaseDTO getAllPendingPurchaseInvoices(SocietyPaymentVoucherSearchResponseDTO responseDTO) {
		log.info("<--Starts SocietyStopInvoiceService .getAllPendingPurchaseInvoices-->");
		BaseDTO baseDTO = new BaseDTO();
		List<PurchaseInvoiceDetailsDTO> purchaseInvoiceList = null;
		List<SocietyPaymentVoucherSearchResponseDTO> societyPaymentVoucherSearchResponseDTOList = null;
		List<Map<String, Object>> purchaseInvoiceMapList = new ArrayList<Map<String, Object>>();
		String query = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Map<Long, SocietyPaymentVoucherSearchResponseDTO> responseDTOMap = new HashMap<>();
		ApplicationQuery applicationQuery = null;
		try {
			societyPaymentVoucherSearchResponseDTOList = new ArrayList<SocietyPaymentVoucherSearchResponseDTO>();
			purchaseInvoiceList = new ArrayList<PurchaseInvoiceDetailsDTO>();

			if (responseDTO.getProductionPlanFor() != null && responseDTO.getProductionPlanFor().getId() != null
					&& responseDTO.getRegionCodeDropDownDTO() != null
					&& responseDTO.getRegionCodeDropDownDTO().getId() != null) {
				log.info("getAllPendingPurchaseInvoices inside Planfor not null and plan not null condtion---------");

				applicationQuery = applicationQueryRepository.findByQuery("SOCIETY_PAYMENT_VOUCHER_PLANFOR");
				query = applicationQuery.getQueryContent();

				if (responseDTO.getProductionPlanFor().getCode().equals("CONTRACT")
						|| responseDTO.getProductionPlanFor().getCode().equals("EXPORT")) {
					query = query.concat(
							" and po.con_export_scheme_plan_id=" + responseDTO.getRegionCodeDropDownDTO().getId());
				} else if (responseDTO.getProductionPlanFor().getCode().equals("ADD_PROD_PLAN")
						&& responseDTO.getRegionCodeDropDownDTO().getId() != null) {
					query = query.concat(
							" and po.additional_scheme_plan_id=" + responseDTO.getRegionCodeDropDownDTO().getId());
				} else if (responseDTO.getProductionPlanFor().getCode().equals("GOVT_SCHEME")) {
					query = query
							.concat(" and po.govt_scheme_plan_id=" + responseDTO.getRegionCodeDropDownDTO().getId());
				} else if (responseDTO.getProductionPlanFor().getCode().equals("RETAIL")) {
					query = query.concat("and po.govt_scheme_plan_id is null and po.additional_scheme_plan_id is null "
							+ " and po.con_export_scheme_plan_id is null");
				}
			} else if (responseDTO.getProductionPlanFor() != null && responseDTO.getProductionPlanFor().getId() != null
					&& responseDTO.getRegionCodeDropDownDTO() == null) {
				log.info("getAllPendingPurchaseInvoices inside productPlan not null and plan null condtion---------");

				applicationQuery = applicationQueryRepository.findByQuery("SOCIETY_PAYMENT_VOUCHER_PLANFOR");
				query = applicationQuery.getQueryContent();

				if (responseDTO.getProductionPlanFor().getCode().equals("CONTRACT")) {
					query = query.concat(" and cep.plan_type='Contract'");
				} else if (responseDTO.getProductionPlanFor().getCode().equals("EXPORT")) {
					query = query.concat(" and cep.plan_type='Export'");
				}
			} else {
				log.info("getAllPendingPurchaseInvoices inside productPlan null condtion---------");

				applicationQuery = applicationQueryRepository.findByQuery("SOCIETY_PAYMENT_DETAILS_SQL");
				query = applicationQuery.getQueryContent();
			}

			if (responseDTO.getInvoiceFromDate() != null && responseDTO.getInvoiceToDate() != null) {
				query = query.replaceAll(":fromDate", dateFormat.format(responseDTO.getInvoiceFromDate()));
				query = query.replaceAll(":toDate", dateFormat.format(responseDTO.getInvoiceToDate()));
			} else {
				query = query.replaceAll("pi.acknowledged_date between ':fromDate' and ':toDate'", "1=1");
				query = query.replaceAll("pi.acknowledged_date between  ':fromDate'  and ' :toDate'", "1=1");
			}

			query = query.replaceAll(":APPROVED", PurchaseInvoiceStatus.APPROVED.toString());
			query = query.replaceAll(":PARTIALLY_PAID", PurchaseInvoiceStatus.PARTIALLY_PAID.toString());

			/*
			 * if (responseDTO.getCircleMaster() != null &&
			 * responseDTO.getCircleMaster().getId() != null) {
			 * log.info("getAllPendingPurchaseInvoices inside circle condtion---------");
			 * query = query.concat(" and cm.id=" + responseDTO.getCircleMaster().getId());
			 * } else if (responseDTO.getDistrictMaster() != null &&
			 * responseDTO.getDistrictMaster().getId() != null) { query = query.concat(
			 * " and cm.id in (select distinct sm1.circle_id from supplier_master sm1 join address_master am on sm1.address_id=am.id where am.district_id="
			 * + responseDTO.getDistrictMaster().getId() +
			 * " and sm1.circle_id is not null)"); }
			 */

			/*
			 * if (responseDTO.getSocietyCodeDropDownDTO() != null &&
			 * responseDTO.getSocietyCodeDropDownDTO().getId() != null) {
			 * log.info("getAllPendingPurchaseInvoices inside supplier condtion---------");
			 * query = query.concat(" and pi.supplier_id=" +
			 * responseDTO.getSocietyCodeDropDownDTO().getId()); }
			 */
			/*
			 * if (responseDTO.getProductCategory() != null &&
			 * responseDTO.getProductCategory().getId() != null) { query =
			 * query.concat(" and  pc.id=" + responseDTO.getProductCategory().getId()); }
			 * 
			 * if (responseDTO.getProductGroupMaster() != null &&
			 * responseDTO.getProductGroupMaster().getId() != null) { query =
			 * query.concat(" and  pgm.id=" + responseDTO.getProductGroupMaster().getId());
			 * }
			 * 
			 * if (responseDTO.getProductVarietyMaster() != null &&
			 * responseDTO.getProductVarietyMaster().getId() != null) { log.
			 * info("getAllPendingPurchaseInvoices inside productvarity condtion---------");
			 * query = query.concat(" and pvm.id  =" +
			 * responseDTO.getProductVarietyMaster().getId()); }
			 * 
			 * if (responseDTO.getInvoiceFromAmount() != null &&
			 * responseDTO.getInvoiceToAmount() != null) {
			 * log.info("getAllPendingPurchaseInvoices inside amount condtion---------");
			 * query =
			 * query.concat(" and pi.material_value between :fromAmount and :toAmount");
			 * query = query.replaceAll(":fromAmount",
			 * responseDTO.getInvoiceFromAmount().toString()); query =
			 * query.replaceAll(":toAmount", responseDTO.getInvoiceToAmount().toString()); }
			 */

			if (responseDTO.getSelectedCircleMasterIds() != null
					&& !responseDTO.getSelectedCircleMasterIds().isEmpty()) {
				log.info("getAllPendingPurchaseInvoices inside circle condtion---------");
				query = query.concat(
						" and cm.id in (" + AppUtil.commaSeparatedIds(responseDTO.getSelectedCircleMasterIds()) + ")");
			} else if (responseDTO.getSelectedDistrictMasterIds() != null
					&& !responseDTO.getSelectedDistrictMasterIds().isEmpty()) {
				query = query.concat(
						" and dm.id in ("
								+ AppUtil.commaSeparatedIds(responseDTO.getSelectedDistrictMasterIds())
								+ ")");
			}

			if (responseDTO.getSelectedLoomTypeList() != null && !responseDTO.getSelectedLoomTypeList().isEmpty()) {
				log.info("getAllPendingPurchaseInvoices inside Loom Type condtion---------");
				query = query.concat(" and sm.loom_type in ("
						+ AppUtil.commaSeparatedStringList(responseDTO.getSelectedLoomTypeList()) + ")");
			}
			if (responseDTO.getSelectedOrderTypeList() != null && !responseDTO.getSelectedOrderTypeList().isEmpty()) {
				log.info("getAllPendingPurchaseInvoices inside Order Type condtion---------");
				query = query.concat(" and pi.order_type in ("
						+ AppUtil.commaSeparatedStringList(responseDTO.getSelectedOrderTypeList()) + ")");
			}

			if (responseDTO.getSelectedSocietyIds() != null && !responseDTO.getSelectedSocietyIds().isEmpty()) {
				log.info("getAllPendingPurchaseInvoices inside SelectedSocietyIds condtion---------");
				query = query.concat(" and pi.supplier_id in ("
						+ AppUtil.commaSeparatedIds(responseDTO.getSelectedSocietyIds()) + ")");
			}
			// query = query + " group by sm.code,sm.name,pi.id order by sm.code";
			//query = query + " group by sm.id,sm.code,sm.name,pi.id order by sm.code,pi.invoice_number";
			query = query + " order by sm.code,pi.invoice_number";
			log.info("      over all Query PurchaseInvoice query----------" + query);
			purchaseInvoiceMapList = jdbcTemplate.queryForList(query);
			int purchaseInvoiceMapListSize = purchaseInvoiceMapList != null ? purchaseInvoiceMapList.size() : 0;
			log.info("purchaseInvoiceMapList size------" + purchaseInvoiceMapListSize);

			if (!CollectionUtils.isEmpty(purchaseInvoiceMapList)) {
				Map<Object, List<Map<String, Object>>> purchaseInvoiceDetailsBySocietyMap = purchaseInvoiceMapList
						.stream().filter(o -> o.get("supplier_id") != null)
						.collect(Collectors.groupingBy(r -> r.get("supplier_id")));
				if (!CollectionUtils.isEmpty(purchaseInvoiceDetailsBySocietyMap)) {
					for (Map.Entry<Object, List<Map<String, Object>>> entry : purchaseInvoiceDetailsBySocietyMap
							.entrySet()) {

						List<Map<String, Object>> valuesList = entry.getValue();
						List<PurchaseInvoiceDetailsDTO> purchaseInvoiceDetailsList = null;
						if (!CollectionUtils.isEmpty(valuesList)) {
							SocietyPaymentVoucherSearchResponseDTO societyPaymentVoucherSearchResponseDTO = new SocietyPaymentVoucherSearchResponseDTO();
							Map<String, Object> societyValueMap = valuesList.get(0);
							purchaseInvoiceDetailsList = new ArrayList<>();
							for (Map<String, Object> dataMap : valuesList) {
								PurchaseInvoiceDetailsDTO purchaseInvoiceDetailsDTO = new PurchaseInvoiceDetailsDTO();
								Long purchaseInvoiceId = dataMap.get("id") != null
										? Long.valueOf(dataMap.get("id").toString())
										: null;
								purchaseInvoiceDetailsDTO
										.setAmountWithountTax(getDouble(dataMap, "amount_without_tax"));
								purchaseInvoiceDetailsDTO.setTax(getDouble(dataMap, "tax_value"));
								purchaseInvoiceDetailsDTO.setTotalAdjustedAmount(purchaseInvoiceAdjustmentRepository
										.getTotalAdjustedAmountByInvoiceId(purchaseInvoiceId));
								purchaseInvoiceDetailsDTO.setTotalAmountPaid(
										voucherDetailsRepository.getTotalAmountByInvoiceId(purchaseInvoiceId));
								purchaseInvoiceDetailsDTO.setSocietyId(getLong(dataMap, "supplier_id"));
								purchaseInvoiceDetailsDTO.setInvoiceId(getLong(dataMap, "id"));
								purchaseInvoiceDetailsDTO
										.setInvoiceNumberPrefix(getString(dataMap, "invoice_number_prefix"));
								purchaseInvoiceDetailsDTO.setInvoiceNumber(dataMap.get("invoice_number") != null
										? Integer.valueOf(dataMap.get("invoice_number").toString())
										: null);
								purchaseInvoiceDetailsDTO.setInvoiceDate(
										dataMap.get("invoice_date") != null ? (Date) (dataMap.get("invoice_date"))
												: null);
								purchaseInvoiceDetailsDTO
										.setTotalPaybleAmount(purchaseInvoiceDetailsDTO.getAmountWithountTax()
												+ purchaseInvoiceDetailsDTO.getTax());
								purchaseInvoiceDetailsDTO
										.setBalanceAmount(purchaseInvoiceDetailsDTO.getTotalPaybleAmount()
												- (purchaseInvoiceDetailsDTO.getTotalAdjustedAmount()
														+ purchaseInvoiceDetailsDTO.getTotalAmountPaid()));

								purchaseInvoiceDetailsDTO
										.setTotalInvoiceAmountToBePaid(purchaseInvoiceDetailsDTO.getBalanceAmount());
								if (purchaseInvoiceDetailsDTO.getTotalInvoiceAmountToBePaid() > 0) {
									purchaseInvoiceDetailsList.add(purchaseInvoiceDetailsDTO);
								}
							}

							societyPaymentVoucherSearchResponseDTO
									.setSocietyId(getLong(societyValueMap, "supplier_id"));
							societyPaymentVoucherSearchResponseDTO.setSocietyName(getString(societyValueMap, "name"));
							societyPaymentVoucherSearchResponseDTO.setSocietyCode(getString(societyValueMap, "code"));
							societyPaymentVoucherSearchResponseDTO
									.setSocietyDisplayName(societyPaymentVoucherSearchResponseDTO.getSocietyCode() + "/"
											+ societyPaymentVoucherSearchResponseDTO.getSocietyName());
							if (!CollectionUtils.isEmpty(purchaseInvoiceDetailsList)) {
								societyPaymentVoucherSearchResponseDTO
										.setPurchaseInvoiceDetailsDTOList(purchaseInvoiceDetailsList);
								societyPaymentVoucherSearchResponseDTO.setAmountWithoutTax(purchaseInvoiceDetailsList
										.stream().filter(o -> o.getAmountWithountTax() != null).collect(Collectors
												.summingDouble(PurchaseInvoiceDetailsDTO::getAmountWithountTax)));
								societyPaymentVoucherSearchResponseDTO
										.setTax(purchaseInvoiceDetailsList.stream().filter(o -> o.getTax() != null)
												.collect(Collectors.summingDouble(PurchaseInvoiceDetailsDTO::getTax)));
								societyPaymentVoucherSearchResponseDTO.setTotalPayableAmount(
										societyPaymentVoucherSearchResponseDTO.getAmountWithoutTax()
												+ societyPaymentVoucherSearchResponseDTO.getTax());
								societyPaymentVoucherSearchResponseDTO
										.setTotalAdjustmentAmount(purchaseInvoiceDetailsList.stream()
												.filter(o -> o.getTotalAdjustedAmount() != null)
												.collect(Collectors.summingDouble(
														PurchaseInvoiceDetailsDTO::getTotalAdjustedAmount)));
								societyPaymentVoucherSearchResponseDTO.setTotalAmountPaid(purchaseInvoiceDetailsList
										.stream().filter(o -> o.getTotalAmountPaid() != null).collect(Collectors
												.summingDouble(PurchaseInvoiceDetailsDTO::getTotalAmountPaid)));
								societyPaymentVoucherSearchResponseDTO
										.setBalanceAmount(societyPaymentVoucherSearchResponseDTO.getTotalPayableAmount()
												- (societyPaymentVoucherSearchResponseDTO.getTotalAdjustmentAmount()
														+ societyPaymentVoucherSearchResponseDTO.getTotalAmountPaid()));
								societyPaymentVoucherSearchResponseDTO.setTotalAmountToBePaid(
										societyPaymentVoucherSearchResponseDTO.getBalanceAmount());
								societyPaymentVoucherSearchResponseDTOList.add(societyPaymentVoucherSearchResponseDTO);
							}
						}
					}
				}
			}

			// if (purchaseInvoiceMapList.size() > 0) {
			// for (Map<String, Object> map : purchaseInvoiceMapList) {
			//
			// Long supplierId = Long.valueOf(map.get("supplier_id").toString());
			//
			// log.info("purchaseInvoice id-----------> " + map.get("id"));
			//
			// SocietyPaymentVoucherSearchResponseDTO dto = null;
			// Double invoiceDetailsTotalAmount = 0.0;
			// if (responseDTOMap.containsKey(supplierId)) {
			// dto = responseDTOMap.get(supplierId);
			//
			// if (map.get("amount_without_tax") != null) {
			// log.info("material_value==> " + map.get("amount_without_tax").toString());
			// dto.setAmountWithoutTax(dto.getAmountWithoutTax()
			// + new BigDecimal(map.get("amount_without_tax").toString()).doubleValue());
			// log.info("Amount without tax==> " + dto.getAmountWithoutTax());
			// }
			// if (map.get("tax_value") != null) {
			// dto.setTax(new BigDecimal(map.get("tax_value").toString()).doubleValue() +
			// dto.getTax());
			// }
			//
			// List<BigDecimal> adjustmentAmount = purchaseInvoiceAdjustmentRepository
			// .getAdjustmentAmountByInvoiceId(Long.valueOf(map.get("id").toString()));
			// log.info("adjustmentAmount --------------" + adjustmentAmount.size());
			// dto.setTotalAdjustmentAmount(0.0);
			// if (adjustmentAmount.size() > 0) {
			// for (BigDecimal d : adjustmentAmount) {
			// dto.setTotalAdjustmentAmount(
			// d.add(new BigDecimal(dto.getTotalAdjustmentAmount())).doubleValue());
			// log.info(d + "<= TotalAdjustmentAmount =>" + dto.getTotalAdjustmentAmount());
			// }
			// }
			//
			// log.info("purchaseInvoice id-----------> " + map.get("id"));
			// List<VoucherDetails> voucherDetailsList = voucherDetailsRepository
			// .findVoucherDetailsByPurchaseInvoiceId(Long.valueOf(map.get("id").toString()));
			//
			// log.info("voucherDetailsList list------" + voucherDetailsList.size());
			//
			// if (voucherDetailsList.size() > 0) {
			// for (VoucherDetails voucherDetails : voucherDetailsList) {
			// log.info("Voucher amount==> " + voucherDetails.getAmount());
			// log.info("Already Total amount paid==> " + dto.getTotalAmountPaid());
			// log.info("Already Total amount paid invoice details==>" +
			// invoiceDetailsTotalAmount);
			// dto.setTotalAmountPaid(voucherDetails.getAmount() +
			// dto.getTotalAmountPaid());
			// invoiceDetailsTotalAmount = voucherDetails.getAmount() +
			// invoiceDetailsTotalAmount;
			// }
			// log.info("Total Paid amount ------" + dto.getTotalAmountPaid());
			// log.info("Total Paid amount --invoiceDetailsTotalAmount----" +
			// invoiceDetailsTotalAmount);
			// }
			//
			// } else {
			// dto = new SocietyPaymentVoucherSearchResponseDTO();
			// if (map.get("supplier_id") != null) {
			// log.info("supplier id==> " + map.get("supplier_id").toString());
			// dto.setSocietyId(Long.valueOf(map.get("supplier_id").toString()));
			//
			// log.info("Society name==> " + map.get("name").toString());
			// dto.setSocietyName(map.get("name").toString());
			//
			// log.info("Society code==> " + map.get("code").toString());
			// dto.setSocietyCode(map.get("code").toString());
			//
			// dto.setSocietyDisplayName(dto.getSocietyCode() + "/" + dto.getSocietyName());
			// log.info("Society displayname==> " + dto.getSocietyDisplayName());
			// }
			// if (map.get("amount_without_tax") != null) {
			// log.info("Material value==> " + map.get("amount_without_tax").toString());
			// dto.setAmountWithoutTax(
			// new BigDecimal(map.get("amount_without_tax").toString()).doubleValue());
			// log.info("AmountWithoutTax==> " + dto.getAmountWithoutTax());
			// }
			// if (map.get("tax_value") != null) {
			// log.info("tax_value==> " + map.get("tax_value").toString());
			// dto.setTax(new BigDecimal(map.get("tax_value").toString()).doubleValue());
			// }
			//
			// log.info("purchaseInvoice id-----------> " + map.get("id"));
			// List<BigDecimal> adjustmentAmount = purchaseInvoiceAdjustmentRepository
			// .getAdjustmentAmountByInvoiceId(Long.valueOf(map.get("id").toString()));
			// log.info("adjustmentAmount --------------" + adjustmentAmount.size());
			//
			// if (adjustmentAmount.size() > 0) {
			// for (BigDecimal d : adjustmentAmount) {
			// dto.setTotalAdjustmentAmount(
			// d.add(new BigDecimal(dto.getTotalAdjustmentAmount())).doubleValue());
			// log.info(d + "<= TotalAdjustmentAmount =>" + dto.getTotalAdjustmentAmount());
			// }
			// }
			//
			// log.info("purchaseInvoice id-----------> " + map.get("id"));
			// List<VoucherDetails> voucherDetailsList = voucherDetailsRepository
			// .findVoucherDetailsByPurchaseInvoiceId(Long.valueOf(map.get("id").toString()));
			//
			// log.info("voucherDetailsList list------" + voucherDetailsList.size());
			//
			// if (voucherDetailsList.size() > 0) {
			// for (VoucherDetails voucherDetails : voucherDetailsList) {
			// dto.setTotalAmountPaid(voucherDetails.getAmount() +
			// dto.getTotalAmountPaid());
			// invoiceDetailsTotalAmount = voucherDetails.getAmount() +
			// invoiceDetailsTotalAmount;
			// }
			// log.info("Total Paid amount --invoiceDetailsTotalAmount----" +
			// invoiceDetailsTotalAmount);
			// log.info("Total Paid amount ------" + dto.getTotalAmountPaid());
			// }
			//
			// Long purchaseInvoiceId = map.get("id") != null ?
			// Long.valueOf(map.get("id").toString()) : null;
			// if (purchaseInvoiceId != null) {
			// dto.setTotalAmountPaid(
			// voucherDetailsRepository.getTotalAmountByInvoiceId(purchaseInvoiceId));
			// }
			//
			// responseDTOMap.put(supplierId, dto);
			// }
			//
			// PurchaseInvoiceDetailsDTO invoiceDetailsDTO = new
			// PurchaseInvoiceDetailsDTO();
			// if (map.get("supplier_id") != null) {
			// invoiceDetailsDTO.setSocietyId(Long.valueOf(map.get("supplier_id").toString()));
			// }
			// invoiceDetailsDTO.setInvoiceId(Long.valueOf(map.get("id").toString()));
			// if (map.get("invoice_number_prefix") != null) {
			// invoiceDetailsDTO.setInvoiceNumberPrefix(map.get("invoice_number_prefix").toString());
			// }
			// if (map.get("invoice_number") != null) {
			// invoiceDetailsDTO.setInvoiceNumber(Integer.valueOf(map.get("invoice_number").toString()));
			// }
			// if (map.get("invoice_date") != null) {
			// invoiceDetailsDTO.setInvoiceDate((Date) map.get("invoice_date"));
			// }
			// if (map.get("amount_without_tax") != null) {
			// invoiceDetailsDTO.setAmountWithountTax(
			// new BigDecimal(map.get("amount_without_tax").toString()).doubleValue());
			// }
			// if (map.get("tax_value") != null) {
			// invoiceDetailsDTO.setTax(new
			// BigDecimal(map.get("tax_value").toString()).doubleValue());
			// }
			// invoiceDetailsDTO.setTotalAdjustedAmount(dto.getTotalAdjustmentAmount());
			// invoiceDetailsDTO.setTotalAmountPaid(invoiceDetailsTotalAmount);
			// invoiceDetailsDTO.setTotalPaybleAmount(
			// (invoiceDetailsDTO.getAmountWithountTax() != null ?
			// invoiceDetailsDTO.getAmountWithountTax()
			// : 0.0) + (invoiceDetailsDTO.getTax() != null ? invoiceDetailsDTO.getTax() :
			// 0.0));
			// invoiceDetailsDTO.setBalanceAmount(
			// (invoiceDetailsDTO.getTotalPaybleAmount() != null ?
			// invoiceDetailsDTO.getTotalPaybleAmount()
			// : 0.0)
			// - ((invoiceDetailsDTO.getTotalAdjustedAmount() != null
			// ? invoiceDetailsDTO.getTotalAdjustedAmount()
			// : 0.0)
			// + (invoiceDetailsDTO.getTotalAmountPaid() != null
			// ? invoiceDetailsDTO.getTotalAmountPaid()
			// : 0.0)));
			// // invoiceDetailsDTO.setTotalInvoiceAmountToBePaid(
			// // invoiceDetailsDTO.getTotalAmountPaid() -
			// // invoiceDetailsDTO.getBalanceAmount());
			// invoiceDetailsDTO.setTotalInvoiceAmountToBePaid(invoiceDetailsDTO.getBalanceAmount());
			// if (invoiceDetailsDTO.getTotalInvoiceAmountToBePaid() > 0) {
			// purchaseInvoiceList.add(invoiceDetailsDTO);
			// }
			//
			// }
			// log.info("responseDTOMap size----------" + responseDTOMap.size());
			// log.info("purchaseInvoiceList size-----" + purchaseInvoiceList.size());
			// if (responseDTOMap.size() > 0) {
			// for (Long key : responseDTOMap.keySet()) {
			//
			// SocietyPaymentVoucherSearchResponseDTO dto = responseDTOMap.get(key);
			//
			// List<PurchaseInvoiceDetailsDTO> purchaseInvoiceDetailsDTOList =
			// purchaseInvoiceList;
			//
			// List<PurchaseInvoiceDetailsDTO> purchaseInvoiceDetailsDTOList1 = new
			// ArrayList<>();
			//
			// for (PurchaseInvoiceDetailsDTO purchaseInvoiceDetailsDTO :
			// purchaseInvoiceDetailsDTOList) {
			// if (purchaseInvoiceDetailsDTO.getSocietyId() != null && dto.getSocietyId() !=
			// null
			// && purchaseInvoiceDetailsDTO.getSocietyId().longValue() == dto.getSocietyId()
			// .longValue()) {
			// purchaseInvoiceDetailsDTOList1.add(purchaseInvoiceDetailsDTO);
			// }
			// }
			//
			// if (!CollectionUtils.isEmpty(purchaseInvoiceDetailsDTOList1)) {
			// dto.setTotalAdjustmentAmount(purchaseInvoiceDetailsDTOList1.stream()
			// .filter(o -> o.getTotalAdjustedAmount() != null).collect(Collectors
			// .summingDouble(PurchaseInvoiceDetailsDTO::getTotalAdjustedAmount)));
			// }
			//
			// dto.setTotalPayableAmount(
			// (dto.getAmountWithoutTax() + dto.getTax()) - dto.getTotalAdjustmentAmount());
			// dto.setBalanceAmount(dto.getTotalPayableAmount() - dto.getTotalAmountPaid());
			// dto.setTotalAmountToBePaid(dto.getTotalAmountPaid() -
			// dto.getBalanceAmount());
			//
			// log.info("purchaseInvoiceDetailsDTOList before stream size--------"
			// + purchaseInvoiceDetailsDTOList1.size());
			//
			// List<PurchaseInvoiceDetailsDTO> detailsDTOList =
			// purchaseInvoiceDetailsDTOList1.stream()
			// .collect(Collectors.groupingBy(po -> po.getInvoiceId())).entrySet().stream()
			// .map(e -> e.getValue().stream()
			// .reduce((f1, f2) -> new PurchaseInvoiceDetailsDTO(f1.getInvoiceId(),
			// f1.getAmountWithountTax() + f2.getAmountWithountTax(),
			// (f1.getTax() != null ? f1.getTax() : 0)
			// + (f2.getTax() != null ? f2.getTax() : 0),
			// f1.getTotalPaybleAmount() + f2.getTotalPaybleAmount(),
			// f1.getTotalAdjustedAmount() + f2.getTotalAdjustedAmount(),
			// f1.getTotalAmountPaid() + f2.getTotalAmountPaid(),
			// f1.getBalanceAmount() + f2.getBalanceAmount(),
			// f1.getTotalInvoiceAmountToBePaid() + f2.getTotalInvoiceAmountToBePaid(),
			// f1.getInvoiceDate(), f1.getInvoiceNumber(),
			// f1.getInvoiceNumberPrefix())))
			// .map(f -> f.get()).collect(Collectors.toList());
			//
			// log.info("purchaseInvoiceDetailsDTOList after add lo0p size--------" +
			// detailsDTOList.size());
			// dto.setPurchaseInvoiceDetailsDTOList(detailsDTOList);
			// dto.setIndividualInvoicePercentage(100D);
			// societyPaymentVoucherSearchResponseDTOList.add(dto);
			//
			// }
			// }
			// log.info("societyPaymentVoucherSearchResponseDTOList size=========>"
			// + societyPaymentVoucherSearchResponseDTOList.size());
			// }
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			baseDTO.setResponseContents(societyPaymentVoucherSearchResponseDTOList);
		} catch (Exception e) {
			log.info("SocietyStopInvoiceService getAllPendingPurchaseInvoices exception", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("<--Ends SocietyStopInvoiceService .getAllPendingPurchaseInvoices-->");
		return baseDTO;
	}

	private static final String DECIMAL_FORMAT = "#0.00";

	private Double getDouble(Map<String, Object> map, String key) {
		try {
			Object obj = map.get(key);
			if (obj != null) {
				String valueStr = String.valueOf(obj);
				if (!org.apache.commons.lang3.StringUtils.isEmpty(valueStr)) {
					DecimalFormat decimalFormat = new DecimalFormat(DECIMAL_FORMAT);
					Double value = decimalFormat.parse(decimalFormat.format(Double.valueOf(valueStr))).doubleValue();
					return value;
				}
			}
		} catch (Exception ex) {
			log.error("Exception at getDouble()", ex);
		}
		return null;
	}

	private String getString(Map<String, Object> map, String key) {
		try {
			Object obj = map.get(key);
			if (obj != null) {
				return String.valueOf(obj);
			}
		} catch (Exception ex) {
			log.error("Exception at getString()", ex);
		}
		return null;
	}

	private Long getLong(Map<String, Object> map, String key) {
		try {
			Object obj = map.get(key);
			if (obj != null) {
				return Long.valueOf(String.valueOf(obj));
			}
		} catch (Exception ex) {
			log.error("Exception at getLong()", ex);
		}
		return null;
	}

	public BaseDTO govtSchemeTypeList() {
		log.info("<--FDSPlanService .govtSchemeTypeList() Started-->");
		BaseDTO baseDTO = new BaseDTO();
		List<GovtSchemeType> govtSchemeTypeList = new ArrayList<>();
		try {
			govtSchemeTypeList = govtSchemeTypeRepository.getAllGovtSchemeType();
			if (govtSchemeTypeList != null && govtSchemeTypeList.size() > 0) {
				log.info("govtSchemeTypeList size() " + govtSchemeTypeList.size());
				baseDTO.setResponseContents(govtSchemeTypeList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				log.info("govtSchemeTypeList is empty ");
				baseDTO.setStatusCode(ErrorDescription.ERROR_EMPTY_LIST.getCode());
			}

		} catch (Exception e) {
			log.error("Exception in govtSchemeTypeList() ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return baseDTO;
	}

	public BaseDTO getAllLoomType(Long circleId) {
		log.info("<--SocietyPaymentVoucher .getAllLoomType() Started-->");
		BaseDTO baseDTO = new BaseDTO();
		List<Object> object = new ArrayList<>();
		SupplierMaster loomType = new SupplierMaster();
		List<SupplierMaster> loomTypeLists = new ArrayList<>();
		try {
			object = supplierMasterRepository.findLoomByCircleId(circleId);

			if (object != null && object.size() > 0) {
				log.info("<--SocietyPaymentVoucher object Size == ->" + object.size());
				for (int i = 0; i < object.size(); i++) {
					if (object.get(i) != null) {
						loomType = new SupplierMaster();
						log.info("<-  ===  -->" + object.get(i).toString());
						loomType.setLoomType(object.get(i).toString());
						loomTypeLists.add(loomType);
					}
				}
				log.info("getAllLoomType size() " + loomTypeLists.size());
				baseDTO.setResponseContents(loomTypeLists);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				log.info("getAllLoomType is empty ");
				baseDTO.setStatusCode(ErrorDescription.ERROR_EMPTY_LIST.getCode());
			}

		} catch (Exception e) {
			log.error("Exception in getAllLoomType() ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return baseDTO;
	}

	public BaseDTO getAllSocietyDetails(String loomtype) {
		log.info("<--Starts SocietyInvoiceAdjustmentService .getSocietyDropdown-->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<Object[]> supplierMasterList = supplierMasterRepository.findAllByLoomType(loomtype);

			List<SocietyCodeDropDownDTO> societyCodeDropDownDTOSList = new ArrayList<>();
			if (supplierMasterList != null && supplierMasterList.size() > 0) {
				supplierMasterList.forEach(supplierMaster -> {
					SocietyCodeDropDownDTO societyCodeDropDownDTO = new SocietyCodeDropDownDTO();
					societyCodeDropDownDTO.setId(new Long(supplierMaster[0].toString()));
					societyCodeDropDownDTO.setCode((String) supplierMaster[1]);
					societyCodeDropDownDTO.setName((String) supplierMaster[2]);
					societyCodeDropDownDTO
							.setDisplayName(societyCodeDropDownDTO.getCode() + "/" + societyCodeDropDownDTO.getName());

					societyCodeDropDownDTOSList.add(societyCodeDropDownDTO);
				});
				baseDTO.setResponseContents(societyCodeDropDownDTOSList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}

		} catch (Exception exception) {
			log.error("exception Occured ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO getCircleMasterByDistrictId(Long districId) {
		log.info("<--Starts SocietyInvoiceAdjustmentService .getCircleMasterByDistrictId-->");
		log.info("<--district id=" + districId);
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<Long> circleIdList = new ArrayList<Long>();
			List<CircleMaster> circleMasterList = new ArrayList<CircleMaster>();
			if (districId != null) {
				circleIdList = supplierMasterRepository.getAllCircleMasterId(districId);
			}
			if (circleIdList != null) {
				circleMasterList = circleMasterRepository.findAllCircleMasterByIdList(circleIdList);
			}
			baseDTO.setResponseContents(circleMasterList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception ex) {
			log.error("exception Occured ", ex);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<--End SocietyInvoiceAdjustmentService .getCircleMasterByDistrictId-->");
		return baseDTO;
	}

	public BaseDTO getDistrictBySupplier() {
		log.info("<--Starts SocietyInvoiceAdjustmentService .getDistrictBySupplier-->");

		BaseDTO baseDTO = new BaseDTO();
		try {
			List<Long> districId = new ArrayList<Long>();
			List<DistrictMaster> districtMasterList = new ArrayList<DistrictMaster>();
			districId = supplierMasterRepository.getDistricIdBySupplier();

			if (districId != null) {
				districtMasterList = districtMasterRepository.getDistricById(districId);
			}
			baseDTO.setResponseContents(districtMasterList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception ex) {
			log.error("exception Occured ", ex);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<--End SocietyInvoiceAdjustmentService .getDistrictBySupplier-->");
		return baseDTO;
	}

	public BaseDTO rejectSocietyPayment(VoucherDto voucherDto) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("approvefrCost method start=============>" + voucherDto.getVoucherId());
			Voucher voucher = voucherRepository.findOne(voucherDto.getVoucherId());

			VoucherLog voucherLog = new VoucherLog();
			voucherLog.setVoucher(voucher);
			voucherLog.setStatus(VoucherStatus.REJECTED);
			voucherLog.setUserMaster(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
			voucherLog.setCreatedBy(loginService.getCurrentUser());
			voucherLog.setCreatedByName(loginService.getCurrentUser().getUsername());
			voucherLog.setCreatedDate(new Date());
			voucherLog.setRemarks(voucherDto.getRemarks());
			voucherLogRepository.save(voucherLog);

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("rejectSocietyPayment  log inserted------");
		} catch (Exception e) {
			log.error("rejectSocietyPayment method exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	private String getReferenceNumber(String name, Long voucherTypeId, String currentYear, String entityCode) {
		String ReferenceNumber = null;
		try {
			ApplicationQuery appQuery = applicationQueryRepository
					.findByQueryName("PETTY_CASH_RECEIPT_AND_PAYMENT_REFERENCE");
			if (appQuery != null) {
				String referenceQuery = appQuery.getQueryContent().trim();
				referenceQuery = referenceQuery.replace(":currentYear", currentYear);
				referenceQuery = referenceQuery.replace(":voucherTypeId", voucherTypeId.toString());
				referenceQuery = referenceQuery.replace(":name", name);
				referenceQuery = referenceQuery.replace(":entityCode", entityCode);
				log.info(" Reference Query : " + referenceQuery);
				Map<String, Object> data = jdbcTemplate.queryForMap(referenceQuery);
				log.info(" Reference data : " + data);
				if (data != null) {
					if (data.get("count") != null) {
						log.info(" Count : " + data.get("count"));
						int maxValue = Integer.valueOf(data.get("count").toString());
						log.info(" maxValue : " + maxValue);
						maxValue = maxValue + 1;
						ReferenceNumber = String.valueOf(maxValue);
					} else {
						ReferenceNumber = "1";
					}
					if (data.get("entitycodeandyear") != null) {
						log.info(" entitycodeandyear : " + data.get("entitycodeandyear"));
					}
					if (data.get("name") != null) {
						log.info(" name : " + data.get("name"));
					}
					if (data.get("voucher_type_id") != null) {
						log.info(" voucher_type_id : " + data.get("voucher_type_id"));
					}
				} else {
					ReferenceNumber = "1";
				}
			}
		} catch (Exception e) {
			log.error("PettyCashReceiptService getReferenceNumber Exception ", e.getMessage());
		}

		return ReferenceNumber;
	}

	@Transactional
	public BaseDTO approveSocietyPayment(SocietyInvoicePaymentDto societyInvoicePaymentDto) {
		BaseDTO baseDTO = new BaseDTO();
		/*
		 * try { log.info("approveSocietyPayment method start=============>" +
		 * societyInvoicePaymentDto.getVoucherId()); Voucher voucher =
		 * voucherRepository.findOne(societyInvoicePaymentDto.getVoucherId());
		 * 
		 * VoucherNote voucherNote = new VoucherNote(); voucherNote.setVoucher(voucher);
		 * voucherNote.setFinalApproval(societyInvoicePaymentDto.getFinalApproval());
		 * voucherNote.setNote(societyInvoicePaymentDto.getVoucherNoteObj().getNote());
		 * voucherNote.setForwardTo(userMasterRepository.findOne(
		 * societyInvoicePaymentDto.getForwardTo()));
		 * voucherNoteRepository.save(voucherNote);
		 * log.info("approveSocietyPayment note inserted------");
		 * 
		 * VoucherLog voucherLog = new VoucherLog(); voucherLog.setVoucher(voucher);
		 * 
		 * if
		 * (societyInvoicePaymentDto.getStatus().equalsIgnoreCase(VoucherStatus.APPROVED
		 * .toString())) { voucherLog.setStatus(VoucherStatus.APPROVED); } else if
		 * (societyInvoicePaymentDto.getStatus().equalsIgnoreCase(VoucherStatus.
		 * FINALAPPROVED.toString())) {
		 * voucherLog.setStatus(VoucherStatus.FINALAPPROVED); }
		 * voucherLog.setUserMaster(userMasterRepository.findOne(loginService.
		 * getCurrentUser().getId()));
		 * voucherLog.setCreatedBy(loginService.getCurrentUser());
		 * voucherLog.setCreatedByName(loginService.getCurrentUser().getUsername());
		 * voucherLog.setCreatedDate(new Date());
		 * voucherLog.setRemarks(societyInvoicePaymentDto.getRemarks());
		 * voucherLogRepository.save(voucherLog);
		 * log.info("approveSocietyPayment log inserted------");
		 * 
		 * log.info("Approval status----------" + societyInvoicePaymentDto.getStatus());
		 * 
		 * baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode()); }
		 * catch (LedgerPostingException l) {
		 * log.error("approveSocietyPayment method Exception----", l);
		 * baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode()); }
		 * catch (Exception e) { log.error("approveSocietyPayment method exception----",
		 * e); baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		 * }
		 */

		try {
			log.info("approveSocietyPayment method start=============>"
					+ societyInvoicePaymentDto.getSocietyInvoicePaymentId());
			SocietyInvoicePayment societyInvoicePayment = societyInvoiceRepository
					.findOne(societyInvoicePaymentDto.getSocietyInvoicePaymentId());
			if (societyInvoicePaymentDto.getStatus()
					.equalsIgnoreCase(SocietyInvoicePaymentStatus.APPROVED.toString())) {

				SocietyInvoicePaymentNote societyInvoicePaymentNote = new SocietyInvoicePaymentNote();
				societyInvoicePaymentNote.setSocietyInvoicePayment(societyInvoicePayment);
				societyInvoicePaymentNote.setFinalApproval(societyInvoicePaymentDto.getFinalApproval());
				societyInvoicePaymentNote.setNote(societyInvoicePaymentDto.getSocietyInvoicePaymentNoteObj().getNote());
				societyInvoicePaymentNote
						.setForwardto(userMasterRepository.findOne(societyInvoicePaymentDto.getForwardTo()));
				societyInvoicePaymentNoteRepository.save(societyInvoicePaymentNote);
				log.info("approveSocietyPayment note inserted------");
				SocietyInvoicePaymentLog societyInvoicePaymentLog = new SocietyInvoicePaymentLog();
				societyInvoicePaymentLog.setSocietyInvoicePayment(societyInvoicePayment);
				societyInvoicePaymentLog.setStage(SocietyInvoicePaymentStatus.APPROVED);
				// societyInvoicePaymentLog.set(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
				societyInvoicePaymentLog.setCreatedBy(loginService.getCurrentUser());
				societyInvoicePaymentLog.setCreatedByName(loginService.getCurrentUser().getUsername());
				societyInvoicePaymentLog.setCreatedDate(new Date());
				societyInvoicePaymentLog.setRemarks(societyInvoicePaymentDto.getRemarks());
				societyInvoicePaymentLogRepository.save(societyInvoicePaymentLog);
				for (SocietyPaymentVoucherSearchResponseDTO paymentList : societyInvoicePaymentDto
						.getViewSocietyPaymentVoucherSearchResponseDTOList()) {
					List<SocietyInvoicePaymentDetails> societyInvPayDetails = societyInvoicePaymentDetailsRepository
							.getPurchaseInvoiceBySocietyId(paymentList.getSocietyId(),
									societyInvoicePaymentDto.getSocietyInvoicePaymentId());
					for (SocietyInvoicePaymentDetails objctDetails : societyInvPayDetails) {
						updatePurchaseInvoiceStatus(objctDetails.getPurchaseInvoice().getId(),
								societyInvoicePaymentLog.getStage());
					}

				}
			}

			else if (societyInvoicePaymentDto.getStatus().equalsIgnoreCase(VoucherStatus.FINALAPPROVED.toString())) {
				SocietyInvoicePaymentNote societyInvoicePaymentNote = new SocietyInvoicePaymentNote();
				societyInvoicePaymentNote.setSocietyInvoicePayment(societyInvoicePayment);
				societyInvoicePaymentNote.setFinalApproval(societyInvoicePaymentDto.getFinalApproval());
				societyInvoicePaymentNote.setNote(societyInvoicePaymentDto.getSocietyInvoicePaymentNoteObj().getNote());
				societyInvoicePaymentNote
						.setForwardto(userMasterRepository.findOne(societyInvoicePaymentDto.getForwardTo()));
				societyInvoicePaymentNoteRepository.save(societyInvoicePaymentNote);
				log.info("approveSocietyPayment note inserted------");
				SocietyInvoicePaymentLog societyInvoicePaymentLog = new SocietyInvoicePaymentLog();
				societyInvoicePaymentLog.setSocietyInvoicePayment(societyInvoicePayment);
				societyInvoicePaymentLog.setStage(SocietyInvoicePaymentStatus.FINALAPPROVED);
				// societyInvoicePaymentLog.set(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
				societyInvoicePaymentLog.setCreatedBy(loginService.getCurrentUser());
				societyInvoicePaymentLog.setCreatedByName(loginService.getCurrentUser().getUsername());
				societyInvoicePaymentLog.setCreatedDate(new Date());
				societyInvoicePaymentLog.setRemarks(societyInvoicePaymentDto.getRemarks());
				societyInvoicePaymentLogRepository.save(societyInvoicePaymentLog);
				Voucher voucher = new Voucher();
				Formatter fmt = new Formatter();
				Calendar cal = Calendar.getInstance();
				fmt = new Formatter();
				fmt.format("%tb", cal);
				log.info(fmt);
				Calendar calendar = Calendar.getInstance();
				double totalvoucherNetamount = 0.00;

				if (societyInvoicePaymentDto.getViewSocietyPaymentVoucherSearchResponseDTOList() != null) {
					int year = calendar.get(Calendar.YEAR);
					log.info("voucher id==>" + societyInvoicePaymentDto.getSocietyInvoicePaymentId());
					/*
					 * if (societyInvoicePaymentDto.getSocietyInvoicePaymentId() != null) { voucher
					 * =
					 * voucherRepository.findOne(societyInvoicePaymentDto.getSocietyInvoicePaymentId
					 * ()); voucher.setReferenceNumberPrefix(voucher.getReferenceNumberPrefix()); }
					 * else { voucher = new Voucher(); // String referenceNumberPrefix = "HO-P" +
					 * fmt + year; // voucher.setReferenceNumberPrefix(referenceNumberPrefix); }
					 */

					VoucherType voucherType = voucherTypeRepository.findByName(VoucherTypeDetails.Payment.toString());
					voucher.setFromDate(new Date());
					voucher.setToDate(new Date());
					voucher.setVoucherType(voucherType);
					// voucher.setName("Payment");
					voucher.setName(VoucherTypeDetails.PETTY_CASH_PAYMENT.toString());

					//////////////////////
					EntityMaster loginUserEntity = entityMasterRepository
							.findByUserRegion(loginService.getCurrentUser().getId());
					voucher.setEntityMaster(loginUserEntity);
					// SequenceConfig sequenceConfig = sequenceConfigRepository
					// .findBySequenceName(SequenceName.valueOf(SequenceName.SOCIETY_INVOICE_PAYMENT.name()));
					SequenceConfig sequenceConfig = sequenceConfigRepository
							.findBySequenceName(SequenceName.valueOf(SequenceName.PAYMENT_REF_NUMBER.name()));
					if (sequenceConfig == null) {
						throw new RestException(ErrorDescription.SEQUENCE_CONFIG_NOT_EXIST);
					}
					// String paymentNumberPrefix = loginUserEntity.getCode() + "-" +
					// sequenceConfig.getPrefix()
					// + AppUtil.getCurrentYearString() + AppUtil.getCurrentMonthString();

					String loginEntityCodeStr = loginUserEntity.getCode() != null
							? String.valueOf(loginUserEntity.getCode())
							: null;
					String numericReferenceNumber = getReferenceNumber(voucher.getName(), voucherType.getId(),
							AppUtil.getCurrentYearString("yy"), loginEntityCodeStr);

					String paymentNumberPrefix = sequenceConfig.getPrefix() + loginUserEntity.getCode()
							+ sequenceConfig.getSeparator() + AppUtil.getCurrentDate() + AppUtil.getCurrentMonthString()
							+ AppUtil.getCurrentYearString() + sequenceConfig.getSeparator() + numericReferenceNumber;

					voucher.setReferenceNumberPrefix(paymentNumberPrefix);
					voucher.setReferenceNumber(Long.valueOf(numericReferenceNumber));
					voucher.setNetAmount(societyInvoicePaymentDto.getNetAmountVoucher());
					voucher.setNarration("payment");
					voucher.setPaymentFor(VoucherTypeDetails.SOCIETY_INVOICE_PAYMENT.toString());
					Voucher finalVoucher = voucherRepository.save(voucher);
					societyInvoicePayment.setVoucher_id(finalVoucher.getId());
					societyInvoiceRepository.save(societyInvoicePayment);

					//////////////////////////////////////////
					for (SocietyPaymentVoucherSearchResponseDTO paymentList : societyInvoicePaymentDto
							.getViewSocietyPaymentVoucherSearchResponseDTOList()) {
						List<SocietyInvoicePaymentDetails> societyInvPayDetails = societyInvoicePaymentDetailsRepository
								.getPurchaseInvoiceBySocietyId(paymentList.getSocietyId(),
										societyInvoicePaymentDto.getSocietyInvoicePaymentId());
						for (SocietyInvoicePaymentDetails objctDetails : societyInvPayDetails) {

							//
							objctDetails.setVoucher(finalVoucher);
							societyInvoicePaymentDetailsRepository.save(objctDetails);

							//
							VoucherDetails voucherDetailsObj = new VoucherDetails();
							voucherDetailsObj.setVoucher(finalVoucher);
							voucherDetailsObj.setPurchaseInvoice(objctDetails.getPurchaseInvoice());
							voucherDetailsObj.setAmount(objctDetails.getPaymentamount());
							voucherDetailsRepository.save(voucherDetailsObj);
							updateParchaseInvoiceStatus(objctDetails.getPurchaseInvoice().getId());
							updatePurchaseInvoiceStatus(objctDetails.getPurchaseInvoice().getId(),
									societyInvoicePaymentLog.getStage());
						}

					}

					VoucherNote voucherNote = new VoucherNote();
					voucherNote.setVoucher(voucher);
					voucherNote.setFinalApproval(societyInvoicePaymentDto.getFinalApproval());
					voucherNote.setNote(societyInvoicePaymentDto.getSocietyInvoicePaymentNoteObj().getNote());
					voucherNote.setForwardTo(userMasterRepository.findOne(societyInvoicePaymentDto.getForwardTo()));
					voucherNoteRepository.save(voucherNote);
					log.info("approveSocietyPayment note inserted------");

					VoucherLog voucherLog = new VoucherLog();
					voucherLog.setVoucher(voucher);

					if (societyInvoicePaymentDto.getStatus().equalsIgnoreCase(VoucherStatus.APPROVED.toString())) {
						voucherLog.setStatus(VoucherStatus.APPROVED);
					} else if (societyInvoicePaymentDto.getStatus()
							.equalsIgnoreCase(VoucherStatus.FINALAPPROVED.toString())) {
						voucherLog.setStatus(VoucherStatus.FINALAPPROVED);
					}
					voucherLog.setUserMaster(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
					voucherLog.setCreatedBy(loginService.getCurrentUser());
					voucherLog.setCreatedByName(loginService.getCurrentUser().getUsername());
					voucherLog.setCreatedDate(new Date());
					voucherLog.setRemarks(societyInvoicePaymentDto.getRemarks());
					voucherLogRepository.save(voucherLog);
					log.info("approveSocietyPayment log inserted------");

					log.info("Approval status----------" + societyInvoicePaymentDto.getStatus());

					Payment payment = new Payment();
					payment.setEntityMaster(loginUserEntity);
					log.info("loginUserEntity ", loginUserEntity);
					payment.setPaymentNumberPrefix(paymentNumberPrefix);
					payment.setPaymentNumber(sequenceConfig.getCurrentValue());

					PaymentDetails paymentDetails = new PaymentDetails();
					paymentDetails.setPayment(payment);
					paymentDetails.setPaymentCategory(PaymentCategory.PAID_IN);
					paymentDetails.setVoucher(finalVoucher);
					paymentDetails.setPaymentTypeMaster(paymentTypeMasterRepository.getSupplierPaymentType());
					paymentDetails.setPaidBy(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
					paymentDetails.setPaymentMethod(
							paymentMethodRepository.findOne(societyInvoicePayment.getPaymentmodeid()));

					paymentRepository.save(payment);
					paymentDetailsRepository.save(paymentDetails);

					AccountTransaction accountTransaction = new AccountTransaction();
					accountTransaction.setPayment(payment);
					accountTransaction.setReferenceNumber(finalVoucher != null ? finalVoucher.getId() : null);
					accountTransaction.setName("");
					accountTransaction
							.setCreatedBy(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
					accountTransaction.setCreatedDate(new Date());
					accountTransactionRepository.save(accountTransaction);

					AccountTransactionDetails accountTransactionDetails = new AccountTransactionDetails();
					accountTransactionDetails.setAccountTransaction(accountTransaction);
					accountTransactionDetails.setSequenceNumber(1L);
					accountTransactionDetails.setTransactionDate(new Date());
					accountTransactionDetails.setAmount(societyInvoicePaymentDto.getNetAmountVoucher());
					accountTransactionDetails.setChequeAmount(societyInvoicePaymentDto.getNetAmountVoucher());
					accountTransactionDetails
							.setVoucherDescription1(VoucherTypeDetails.SOCIETY_INVOICE_PAYMENT.toString());
					accountTransactionDetails.setReceiptNumber(paymentNumberPrefix);
					accountTransactionDetails.setPaymentId(payment.getId());

					AccountTransactionConfig accountTransactionConfig = accountTransactionConfigRepository
							.getByTransactionCodeAndProperty(VoucherTypeDetails.SOCIETY_INVOICE_PAYMENT.toString(),
									"TOTAL_PAID_AMOUNT");

					if (accountTransactionConfig != null) {
						if (accountTransactionConfig.getGlAccount() != null
								&& accountTransactionConfig.getGlAccount().getId() != null) {
							accountTransactionDetails.setGlAccount(
									glAccountRepository.findOne(accountTransactionConfig.getGlAccount().getId()));
							accountTransactionDetails
									.setAccountAspect(accountTransactionConfig.getAccountAspect().toUpperCase());
						}
					}

					accountTransactionDetails
							.setCreatedBy(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
					accountTransactionDetails.setCreatedDate(new Date());
					accountTransactionDetailsRepository.save(accountTransactionDetails);

					sequenceConfig.setCurrentValue(sequenceConfig.getCurrentValue() + 1);
					sequenceConfigRepository.save(sequenceConfig);
				}

			}
			log.info("approveSocietyPayment log inserted------");

			log.info("Approval status----------" + societyInvoicePaymentDto.getStatus());

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (LedgerPostingException l) {
			log.error("approveSocietyPayment method Exception----", l);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("approveSocietyPayment method exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}

		return baseDTO;
	}

	private void updatePurchaseInvoiceStatus(Long puchase_invoice_id, SocietyInvoicePaymentStatus stage) {
		jdbcTemplate.update(
				"UPDATE purchase_invoice SET invoice_status ='" + stage + "' WHERE id IN(" + puchase_invoice_id + ");");

	}

	private void updateParchaseInvoiceStatus(Long puchase_invoice_id) {
		jdbcTemplate.update("UPDATE purchase_invoice SET status='" + SocietyInvoicePaymentStatus.PAID + "' WHERE id IN("
				+ puchase_invoice_id + ");");

	}

	public BaseDTO searchPurchaseInvoiceDetailsBasedOnVoucherId(Long societyPaymentId) {
		BaseDTO baseDTO = new BaseDTO();
		double amountAlreadyAdjusted = 0.0;
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		String mainQuery = "select sipd.society_invoice_payment_id as societypayid ,sm.id as supplierid , sm.code socitycode , "
				+ "sm.name as societyname , sum(sipd.amount_without_tax) as amountwithouttax,  "
				+ "sum(sipd.tax_amount) as taxamount,sum(sipd.payable_amount) as paybleamount,sum(sipd.adjusted_amount) as adjustmentamount, "
				+ "sum(sipd.paid_amount) as paidamount,sum(sipd.balance_amount) as balanceamount,sum(sipd.payment_amount) as amonttobepaid, "
				+ "sipd.payment_percetnage as percentage, array_agg(sipd.purchase_invoice_id) as purchaseinvoiceId "
				+ "from society_invoice_payment sip "
				+ "join society_invoice_payment_details sipd on sip.id = sipd.society_invoice_payment_id "
				+ " join supplier_master sm on sipd.supplier_id = sm.id where sip.id =" + societyPaymentId
				+ " group by sm.code,sm.name,sm.id, " + " sipd.society_invoice_payment_id,sipd.payment_percetnage  ";

		listofData = jdbcTemplate.queryForList(mainQuery);
		List<SocietyPaymentVoucherSearchResponseDTO> societyPaymentVoucherListDTOList = new ArrayList<>();
		for (Map<String, Object> data : listofData) {
			SocietyPaymentVoucherSearchResponseDTO societyPaymentVoucherListDTO = new SocietyPaymentVoucherSearchResponseDTO();

			if (data.get("societypayid") != null) {
				societyPaymentVoucherListDTO.setSociety_payment_id(Long.valueOf(data.get("societypayid").toString()));
			}
			if (data.get("supplierid") != null) {
				societyPaymentVoucherListDTO.setSocietyId(Long.valueOf(data.get("supplierid").toString()));
			}
			if (data.get("socitycode") != null) {
				societyPaymentVoucherListDTO.setSocietyCode(data.get("socitycode").toString());
			}
			if (data.get("societyname") != null) {
				societyPaymentVoucherListDTO.setSocietyName(data.get("societyname").toString());

			}
			if (data.get("amountwithouttax") != null) {
				societyPaymentVoucherListDTO
						.setAmountWithoutTax(Double.valueOf(data.get("amountwithouttax").toString()));
			}
			if (data.get("taxamount") != null) {
				societyPaymentVoucherListDTO.setTax(Double.valueOf(data.get("taxamount").toString()));
			}
			if (data.get("paybleamount") != null) {
				societyPaymentVoucherListDTO.setTotalPayableAmount(Double.valueOf(data.get("paybleamount").toString()));
			}
			if (data.get("adjustmentamount") != null) {
				societyPaymentVoucherListDTO
						.setTotalAdjustmentAmount(Double.valueOf(data.get("adjustmentamount").toString()));
			}
			if (data.get("paidamount") != null) {
				societyPaymentVoucherListDTO.setTotalAmountPaid(Double.valueOf(data.get("paidamount").toString()));
			}
			if (data.get("balanceamount") != null) {
				societyPaymentVoucherListDTO.setBalanceAmount(Double.valueOf(data.get("balanceamount").toString()));
			}
			if (data.get("amonttobepaid") != null) {
				societyPaymentVoucherListDTO
						.setTotalAmountToBePaid(Double.valueOf(data.get("amonttobepaid").toString()));
			}
			if (data.get("percentage") != null) {
				societyPaymentVoucherListDTO
						.setIndividualInvoicePercentage(Double.valueOf(data.get("percentage").toString()));
			}
			/*
			 * if (data.get("purchaseinvoiceId") != null) { societyPaymentVoucherListDTO
			 * .setPurchase_invoice_id(Long.valueOf(data.get("purchaseinvoiceId").toString()
			 * )); }
			 */

			String val = data.get("purchaseinvoiceId").toString();
			String[] value = val.split(",");
			societyPaymentVoucherListDTO.setPurchaseInvoiceDetailsDTOList(new ArrayList<>());
			if (value != null && value.length > 0) {
				for (String idob : value) {
					String idValues = idob.replace("{", "");
					String idValues1 = idValues.replace("}", "");
					idValues1.trim();
					Integer purchaseId = Integer.parseInt(idValues1);

					List<SocietyInvoicePaymentDetails> societyInvoicePaymentDetailsList = societyInvoicePaymentDetailsRepository
							.getPurchaseInvoiceList(purchaseId, societyPaymentVoucherListDTO.getSociety_payment_id());
					if (societyInvoicePaymentDetailsList != null && societyInvoicePaymentDetailsList.size() > 0) {
						for (SocietyInvoicePaymentDetails purchaseInvoice : societyInvoicePaymentDetailsList) {
							PurchaseInvoiceDetailsDTO purchaseInvoiceDetailsDTO = new PurchaseInvoiceDetailsDTO();
							PurchaseInvoice purchasei = purchaseInvoiceRepository
									.getOne(purchaseInvoice.getPurchaseInvoice().getId());
							if (purchasei != null) {
								purchaseInvoiceDetailsDTO.setInvoiceNumber(purchasei.getInvoiceNumber());
								purchaseInvoiceDetailsDTO.setInvoiceNumberPrefix(purchasei.getInvoiceNumberPrefix());
								purchaseInvoiceDetailsDTO.setInvoiceDate(purchasei.getInvoiceDate());
							}
							purchaseInvoiceDetailsDTO.setAmountWithountTax(purchaseInvoice.getAmountwithouttax());
							purchaseInvoiceDetailsDTO.setTax(purchaseInvoice.getTaxamount());
							purchaseInvoiceDetailsDTO.setBalanceAmount(purchaseInvoice.getBalanceamount());
							purchaseInvoiceDetailsDTO.setTotalPaybleAmount(purchaseInvoice.getPayableamount());
							purchaseInvoiceDetailsDTO.setTotalInvoiceAmountToBePaid(purchaseInvoice.getPaymentamount());
							purchaseInvoiceDetailsDTO.setTotalAmountPaid(purchaseInvoice.getPaidamount());

							List<PurchaseInvoiceAdjustment> purchaseInvoiceAdjustmentList = purchaseInvoiceAdjustmentRepository
									.getByInvoiceId(purchaseInvoice.getPurchaseInvoice().getId());
							if (purchaseInvoiceAdjustmentList != null) {
								for (PurchaseInvoiceAdjustment purchaseInvoiceAdjustment : purchaseInvoiceAdjustmentList) {
									if (purchaseInvoiceAdjustment != null) {
										amountAlreadyAdjusted += purchaseInvoiceAdjustment.getTotalAdjustedAmount();
									}
								}
								purchaseInvoiceDetailsDTO.setTotalAdjustedAmount(amountAlreadyAdjusted);
							}
							purchaseInvoiceDetailsDTO.setPercentage(purchaseInvoice.getPaymentpercetnage());
							societyPaymentVoucherListDTO.getPurchaseInvoiceDetailsDTOList()
									.add(purchaseInvoiceDetailsDTO);

						}
					}

				}

			}

			societyPaymentVoucherListDTOList.add(societyPaymentVoucherListDTO);
			Collections.sort(societyPaymentVoucherListDTO.getPurchaseInvoiceDetailsDTOList(), new ComparatorId());
		}
		baseDTO.setResponseContents(societyPaymentVoucherListDTOList);

		if (societyPaymentVoucherListDTOList != null && societyPaymentId != null) {
			SocietyInvoicePaymentDto societyInvoicePaymentDto = new SocietyInvoicePaymentDto();
			societyInvoicePaymentDto.setSocietyInvoicePaymentId(societyPaymentId);
			societyInvoicePaymentDto.setSocietyInvoicePaymentNoteObj(
					societyInvoicePaymentNoteRepository.findBysocietyPaymentId(societyPaymentId));
			societyInvoicePaymentDto.getSocietyInvoicePaymentNoteObj().setSocietyInvoicePayment(null);
			societyInvoicePaymentDto.setSocietyInvoicePaymentLogObj(
					societyInvoicePaymentLogRepository.findSocietyInvoicePaymentById(societyPaymentId));
			societyInvoicePaymentDto.getSocietyInvoicePaymentLogObj().setSocietyInvoicePayment(null);

			if (societyInvoicePaymentDto.getSocietyInvoicePaymentNoteObj() != null) {
				long emp = employeeMasterRepository.getEmployeeIdByUserId(
						societyInvoicePaymentDto.getSocietyInvoicePaymentNoteObj().getForwardto().getId());
				EmployeeMaster employee = employeeMasterRepository.findEmployeeByUserId(emp);
				if (employee != null) {
					societyInvoicePaymentDto.setForwardToUser(employee.getEmpDisplayName());
				}
			}

			baseDTO.setResponseContent(societyInvoicePaymentDto);
		}

		List<Map<String, Object>> employeeData = new ArrayList<Map<String, Object>>();
		if (societyPaymentId != null) {
			log.info("<<<:::::::voucher::::not Null::::>>>>" + societyPaymentId != null ? societyPaymentId : "Null");
			ApplicationQuery applicationQueryForlog = applicationQueryRepository
					.findByQueryName("SOCIETY_INVOICE_PAYMENT_LOG_EMPLOYEE_DETAILS");
			if (applicationQueryForlog == null || applicationQueryForlog.getId() == null) {
				log.info("Application Query For Log Details not found for query name : " + applicationQueryForlog);
				baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
				return baseDTO;
			}
			String logquery = applicationQueryForlog.getQueryContent().trim();
			log.info("<=========VOUCHER_LOG_EMPLOYEE_DETAILS Query Content ======>" + logquery);
			logquery = logquery.replace(":societyPaymentId", "'" + societyPaymentId.toString() + "'");
			log.info("Query Content For VOUCHER_LOG_EMPLOYEE_DETAILS After replaced plan id View query : " + logquery);
			employeeData = jdbcTemplate.queryForList(logquery);
			log.info("<=========VOUCHER_LOG_EMPLOYEE_DETAILS Employee Data======>" + employeeData);
			baseDTO.setTotalListOfData(employeeData);
		}

		baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		return baseDTO;

	}

	static class ComparatorId implements Comparator<PurchaseInvoiceDetailsDTO> {
		@Override
		public int compare(PurchaseInvoiceDetailsDTO obj1, PurchaseInvoiceDetailsDTO obj2) {
			return obj1.getInvoiceNumber().compareTo(obj2.getInvoiceNumber());
		}
	}

	public BaseDTO searchPurchaseInvoiceDetailsBasedOnVoucherIdOld(Long voucherId) {
		log.info("<--Starts SocietyStopInvoiceService .searchPendingPurchaseInvoiceDetailsGroupedBySociety-->"
				+ voucherId);
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("voucherId=>" + voucherId);
			Voucher vocherDetail = voucherRepository.findOne(voucherId);

			List<PurchaseInvoice> purchaseInvoices = new ArrayList<>();

			if (vocherDetail != null) {

				// List<VoucherDetails> voucherDetailsList =
				// voucherDetailsRepository.findAllByVoucherId(voucherId);

				List<VoucherDetails> voucherDetailsList = vocherDetail.getVoucherDetailsList();
				log.info("voucherDetailsList size==>" + voucherDetailsList.size());

				int s = 0;
				for (VoucherDetails voucherdetails : voucherDetailsList) {
					log.info("sssss==>" + s + "<=voucher details id=>" + voucherDetailsList.get(s).getId());
					// voucherdetails.getPurchaseInvoice().setVoucherDetailId(voucherDetailsList.get(s).getId());
					purchaseInvoices.add(voucherdetails.getPurchaseInvoice());
					s++;
				}
			}
			baseDTO.setResponseContents(process(purchaseInvoices, voucherId));
			if (vocherDetail != null) {
				VoucherDto voucherDto = new VoucherDto();
				voucherDto.setVoucherId(vocherDetail.getId());
				voucherDto.setVoucherFromDate(vocherDetail.getFromDate());
				voucherDto.setVoucherToDate(vocherDetail.getToDate());
				voucherDto.setPaymentMode(vocherDetail.getPaymentMode());
				// voucherDto.setVoucherDetailsList(vocherDetail.getVoucherDetailsList());
				// voucherDto.setVoucherNote(vocherDetail.getVoucherNote());
				// voucherDto.setVoucherLogList(vocherDetail.getVoucherLogList());
				voucherDto.setVoucherNoteObj(voucherNoteRepository.findByVoucherId(vocherDetail.getId()));
				voucherDto.setVoucherLogObj(voucherLogRepository.findByVoucherId(vocherDetail.getId()));
				baseDTO.setResponseContent(voucherDto);
			}

			List<Map<String, Object>> employeeData = new ArrayList<Map<String, Object>>();
			if (vocherDetail != null) {
				log.info(
						"<<<:::::::voucher::::not Null::::>>>>" + vocherDetail != null ? vocherDetail.getId() : "Null");
				ApplicationQuery applicationQueryForlog = applicationQueryRepository
						.findByQueryName("VOUCHER_LOG_EMPLOYEE_DETAILS");
				if (applicationQueryForlog == null || applicationQueryForlog.getId() == null) {
					log.info("Application Query For Log Details not found for query name : " + applicationQueryForlog);
					baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
					return baseDTO;
				}
				String logquery = applicationQueryForlog.getQueryContent().trim();
				log.info("<=========VOUCHER_LOG_EMPLOYEE_DETAILS Query Content ======>" + logquery);
				logquery = logquery.replace(":voucherId", "'" + vocherDetail.getId().toString() + "'");
				log.info("Query Content For VOUCHER_LOG_EMPLOYEE_DETAILS After replaced plan id View query : "
						+ logquery);
				employeeData = jdbcTemplate.queryForList(logquery);
				log.info("<=========VOUCHER_LOG_EMPLOYEE_DETAILS Employee Data======>" + employeeData);
				baseDTO.setTotalListOfData(employeeData);
			}

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.error("searchPurchaseInvoiceDetailsBasedOnVoucherId exception==>", e);
		}
		return baseDTO;

	}

	private List<SocietyPaymentVoucherSearchResponseDTO> process(List<PurchaseInvoice> purchaseInvoices,
			Long voucherId) {

		List<SocietyPaymentVoucherSearchResponseDTO> res = new ArrayList<>();
		List<PurchaseInvoiceDetailsDTO> purchaseInvoiceList = null;
		try {
			purchaseInvoiceList = new ArrayList<PurchaseInvoiceDetailsDTO>();
			Map<Long, SocietyPaymentVoucherSearchResponseDTO> responseDTOMap = new HashMap<>();

			for (PurchaseInvoice invoice : purchaseInvoices) {

				Long societyId = invoice.getSupplierMaster().getId();
				Double amountWithoutTax = purchaseInvoiceRepository
						.sumOfItemAmountBypurchaseInvoiceId(invoice.getId() != null ? invoice.getId() : 0);
				log.info("getVoucherDetailId==>" + invoice.getVoucherDetailId());
				SocietyPaymentVoucherSearchResponseDTO dto = null;
				Double invoiceDetailsTotalAmount = 0.0;
				if (responseDTOMap.containsKey(societyId)) {

					dto = responseDTOMap.get(societyId);
					if (invoice.getId() != null) {
						// log.info("material_value==> " + invoice.getMaterialValue().toString());
						// Double
						// amountWithoutTax=purchaseInvoiceRepository.sumOfItemAmountBypurchaseInvoiceId(invoice.getId());

						log.info("amountWithoutTax==> " + amountWithoutTax != null ? amountWithoutTax : 0);
						/*
						 * dto.setAmountWithoutTax(dto.getAmountWithoutTax() + new
						 * BigDecimal(invoice.getMaterialValue().toString()).doubleValue());
						 */
						dto.setAmountWithoutTax(
								dto.getAmountWithoutTax() + (amountWithoutTax != null ? amountWithoutTax : 0));
						log.info("Amount without tax==> " + dto.getAmountWithoutTax());
					}
					if (invoice.getId() != null) {
						Double taxvalue = purchaseInvoiceRepository.purchaseItemTaxValueByinvoiceId(invoice.getId());
						log.info("tax_value==> " + taxvalue != null ? taxvalue : 0.0);
						dto.setTax((taxvalue != null ? taxvalue : 0.0) + dto.getTax());
					}

					List<BigDecimal> adjustmentAmount = purchaseInvoiceAdjustmentRepository
							.getAdjustmentAmountByInvoiceId(invoice.getId());
					log.info("adjustmentAmount --------------" + adjustmentAmount.size());

					if (adjustmentAmount.size() > 0) {
						for (BigDecimal d : adjustmentAmount) {
							dto.setTotalAdjustmentAmount(
									d.add(new BigDecimal(dto.getTotalAdjustmentAmount())).doubleValue());
							log.info(d + "<= TotalAdjustmentAmount =>" + dto.getTotalAdjustmentAmount());
						}
					}

					log.info("purchaseInvoice id-----------> " + invoice.getId());
					List<VoucherDetails> voucherDetailsList = voucherDetailsRepository
							.findVoucherDetailsByInvoiceIdandVoucherId(invoice.getId(), voucherId);

					log.info("voucherDetailsList list------" + voucherDetailsList.size());

					if (voucherDetailsList.size() > 0) {
						for (VoucherDetails voucherDetails : voucherDetailsList) {
							log.info("Voucher amount==> " + voucherDetails.getAmount());
							log.info("Already Total amount paid==> " + dto.getTotalAmountPaid());
							log.info("Already Total amount paid invoice details==>" + invoiceDetailsTotalAmount);
							dto.setTotalAmountPaid(voucherDetails.getAmount() + dto.getTotalAmountPaid());
							invoiceDetailsTotalAmount = voucherDetails.getAmount() + invoiceDetailsTotalAmount;
						}
						log.info("Total Paid amount ------" + dto.getTotalAmountPaid());
						log.info("Total Paid amount --invoiceDetailsTotalAmount----" + invoiceDetailsTotalAmount);
					}

					/*
					 * invoiceDetailsDTO.setTotalInvoiceAmountToBePaid(invoiceAmount!=null?
					 * invoiceAmount:0.0);
					 * 
					 * invoiceDetailsDTO.setBalanceAmount(((invoiceDetailsDTO.getTotalPaybleAmount()
					 * != null ? invoiceDetailsDTO.getTotalPaybleAmount(): 0.0) -
					 * (invoiceDetailsDTO.getTotalAmountPaid() != null ?
					 * invoiceDetailsDTO.getTotalAmountPaid(): 0.0)) -
					 * (invoiceAmount!=null?invoiceAmount:0.0));
					 */

					Double invoiceAmount = voucherDetailsRepository
							.getVoucherDetailsAmounyByInvoiceIdandVoucherId(invoice.getId(), voucherId);

					dto.setBalanceAmount(((((dto.getAmountWithoutTax() + dto.getTax()) - dto.getTotalAdjustmentAmount())
							- dto.getTotalAmountPaid()) - invoiceAmount) + dto.getBalanceAmount());
					dto.setTotalAmountToBePaid(invoiceAmount + dto.getTotalAmountToBePaid());

				} else {
					dto = new SocietyPaymentVoucherSearchResponseDTO();
					if (societyId != null) {
						log.info("supplier id==> " + societyId);
						dto.setSocietyId(societyId);

						log.info("Society name==> " + invoice.getSupplierMaster().getName());
						dto.setSocietyName(invoice.getSupplierMaster().getName());

						log.info("Society code==> " + invoice.getSupplierMaster().getCode());
						dto.setSocietyCode(invoice.getSupplierMaster().getCode());

						dto.setSocietyDisplayName(dto.getSocietyCode() + "/" + dto.getSocietyName());
						log.info("Society displayname==> " + dto.getSocietyDisplayName());
					}
					if (invoice.getId() != null) {
						// Double
						// amountWithoutTax=purchaseInvoiceRepository.sumOfItemAmountBypurchaseInvoiceId(invoice.getId());
						/*
						 * log.info("Material value==> " + invoice.getMaterialValue());
						 * dto.setAmountWithoutTax(invoice.getMaterialValue());
						 */
						log.info("amountWithoutTax==> " + amountWithoutTax != null ? amountWithoutTax : 0);
						dto.setAmountWithoutTax(amountWithoutTax != null ? amountWithoutTax : 0);
						log.info("AmountWithoutTax==> " + dto.getAmountWithoutTax());
					}
					if (invoice.getId() != null) {
						Double taxvalue = purchaseInvoiceRepository.purchaseItemTaxValueByinvoiceId(invoice.getId());
						log.info("tax_value==> " + taxvalue != null ? taxvalue : 0.0);
						dto.setTax((taxvalue != null ? taxvalue : 0.0) + dto.getTax());
					}

					log.info("purchaseInvoice id-----------> " + invoice.getId());
					List<BigDecimal> adjustmentAmount = purchaseInvoiceAdjustmentRepository
							.getAdjustmentAmountByInvoiceId(invoice.getId());
					log.info("adjustmentAmount --------------" + adjustmentAmount.size());

					if (adjustmentAmount.size() > 0) {
						for (BigDecimal d : adjustmentAmount) {
							dto.setTotalAdjustmentAmount(
									d.add(new BigDecimal(dto.getTotalAdjustmentAmount())).doubleValue());
							log.info(d + "<= TotalAdjustmentAmount =>" + dto.getTotalAdjustmentAmount());
						}
					}

					log.info("purchaseInvoice id-----------> " + invoice.getId());
					List<VoucherDetails> voucherDetailsList = voucherDetailsRepository
							.findVoucherDetailsByInvoiceIdandVoucherId(invoice.getId(), voucherId);

					log.info("voucherDetailsList list------" + voucherDetailsList.size());

					/*
					 * if (voucherDetailsList.size() > 0) { for (VoucherDetails voucherDetails :
					 * voucherDetailsList) { dto.setTotalAmountPaid(voucherDetails.getAmount() +
					 * dto.getTotalAmountPaid()); } log.info("Total Paid amount ------" +
					 * dto.getTotalAmountPaid()); }
					 */

					if (voucherDetailsList.size() > 0) {
						for (VoucherDetails voucherDetails : voucherDetailsList) {
							log.info("Voucher amount==> " + voucherDetails.getAmount());
							log.info("Already Total amount paid==> " + dto.getTotalAmountPaid());
							log.info("Already Total amount paid invoice details==>" + invoiceDetailsTotalAmount);
							dto.setTotalAmountPaid(voucherDetails.getAmount() + dto.getTotalAmountPaid());
							invoiceDetailsTotalAmount = voucherDetails.getAmount() + invoiceDetailsTotalAmount;
						}
						log.info("Total Paid amount ------" + dto.getTotalAmountPaid());
						log.info("Total Paid amount --invoiceDetailsTotalAmount----" + invoiceDetailsTotalAmount);
					}
					Double invoiceAmount = voucherDetailsRepository
							.getVoucherDetailsAmounyByInvoiceIdandVoucherId(invoice.getId(), voucherId);

					dto.setBalanceAmount(((((dto.getAmountWithoutTax() + dto.getTax()) - dto.getTotalAdjustmentAmount())
							- dto.getTotalAmountPaid()) - invoiceAmount) + dto.getBalanceAmount());
					dto.setTotalAmountToBePaid(invoiceAmount + dto.getTotalAmountToBePaid());
					responseDTOMap.put(societyId, dto);
				}

				PurchaseInvoiceDetailsDTO invoiceDetailsDTO = new PurchaseInvoiceDetailsDTO();
				if (societyId != null) {
					invoiceDetailsDTO.setSocietyId(societyId);
				}
				invoiceDetailsDTO.setInvoiceId(invoice.getId());
				if (invoice.getInvoiceNumberPrefix() != null) {
					invoiceDetailsDTO.setInvoiceNumberPrefix(invoice.getInvoiceNumberPrefix());
				}
				if (invoice.getInvoiceNumber() != null) {
					invoiceDetailsDTO.setInvoiceNumber(invoice.getInvoiceNumber());
				}
				if (invoice.getInvoiceDate() != null) {
					invoiceDetailsDTO.setInvoiceDate(invoice.getInvoiceDate());
				}
				if (invoice.getId() != null) {
					// Double
					// amountWithoutTax=purchaseInvoiceRepository.sumOfItemAmountBypurchaseInvoiceId(invoice.getId());
					// invoiceDetailsDTO.setAmountWithountTax(invoice.getMaterialValue());
					invoiceDetailsDTO.setAmountWithountTax(amountWithoutTax != null ? amountWithoutTax : 0);
				}
				if (invoice.getId() != null) {
					Double taxvalue = purchaseInvoiceRepository.purchaseItemTaxValueByinvoiceId(invoice.getId());
					log.info("tax_value==> " + taxvalue != null ? taxvalue : 0.0);
					invoiceDetailsDTO.setTax((taxvalue != null ? taxvalue : 0.0));
					// invoiceDetailsDTO.setTax(new
					// BigDecimal(map.get("tax_value").toString()).doubleValue());
				}
				invoiceDetailsDTO.setTotalAdjustedAmount(dto.getTotalAdjustmentAmount());
				invoiceDetailsDTO.setTotalAmountPaid(invoiceDetailsTotalAmount);
				invoiceDetailsDTO.setTotalPaybleAmount(
						(invoiceDetailsDTO.getAmountWithountTax() != null ? invoiceDetailsDTO.getAmountWithountTax()
								: 0.0) + (invoiceDetailsDTO.getTax() != null ? invoiceDetailsDTO.getTax() : 0.0));

				Double invoiceAmount = voucherDetailsRepository
						.getVoucherDetailsAmounyByInvoiceIdandVoucherId(invoice.getId(), voucherId);
				invoiceDetailsDTO.setTotalInvoiceAmountToBePaid(invoiceAmount != null ? invoiceAmount : 0.0);

				invoiceDetailsDTO.setBalanceAmount(((invoiceDetailsDTO.getTotalPaybleAmount() != null
						? invoiceDetailsDTO.getTotalPaybleAmount()
						: 0.0)
						- (invoiceDetailsDTO.getTotalAmountPaid() != null ? invoiceDetailsDTO.getTotalAmountPaid()
								: 0.0))
						- (invoiceAmount != null ? invoiceAmount : 0.0));
				/*
				 * invoiceDetailsDTO.setTotalInvoiceAmountToBePaid(
				 * invoiceDetailsDTO.getTotalAmountPaid() -
				 * invoiceDetailsDTO.getBalanceAmount());
				 */
				purchaseInvoiceList.add(invoiceDetailsDTO);

				/*
				 * if (responseDTOMap.containsKey(societyId)) {
				 * 
				 * SocietyPaymentVoucherSearchResponseDTO dto = responseDTOMap.get(societyId);
				 * // log.info("if //
				 * getAmountWithoutTax"+dto.getAmountWithoutTax()+"<==getMaterialValue==>"+
				 * invoice.getMaterialValue());
				 * dto.setAmountWithoutTax(dto.getAmountWithoutTax() +
				 * invoice.getMaterialValue());
				 * dto.setTax((invoice.getCgstValue()!=null?invoice.getCgstValue():0)+(invoice.
				 * getSgstValue()!=null?invoice.getSgstValue():0)+dto.getTax());
				 * //dto.setTax(invoice.getCgstValue() + invoice.getSgstValue());
				 * 
				 * List<PurchaseInvoiceAdjustment> purchaseInvoiceAdjustmentsList =
				 * purchaseInvoiceAdjustmentRepository .findAllByInvoiceId(invoice);
				 * 
				 * List<PurchaseInvoiceAdjustment> purchaseInvoiceAdjustmentsList =
				 * purchaseInvoiceAdjustmentRepository .getInvoicesByInvoiceId(invoice.getId());
				 * 
				 * for (PurchaseInvoiceAdjustment p : purchaseInvoiceAdjustmentsList) {
				 * log.info(p.getTotalAdjustedAmount()+"<=if TotalAdjustmentAmount=>"+
				 * dto.getTotalAdjustmentAmount());
				 * dto.setTotalAdjustmentAmount(p.getTotalAdjustedAmount() +
				 * dto.getTotalAdjustmentAmount());
				 * 
				 * totalAdjustmentAmount += p.getTotalAdjustedAmount();
				 * dto.setTotalAdjustmentAmount(totalAdjustmentAmount);
				 * log.info("if Invoice id==>"+invoice.getId()+"<=totalAdjustmentAmount=>"+
				 * totalAdjustmentAmount); } List<VoucherDetails> voucherDetailsList =
				 * voucherDetailsRepository.findAllByPurchaseInvoice(invoice);
				 * 
				 * voucherDetailsList.forEach(voucherDetails -> {
				 * dto.setTotalAmountPaid(voucherDetails.getAmount() +
				 * dto.getTotalAmountPaid()); });
				 * 
				 * } else {
				 * 
				 * SocietyPaymentVoucherSearchResponseDTO dto = new
				 * SocietyPaymentVoucherSearchResponseDTO();
				 * dto.setSocietyId(invoice.getSupplierMaster().getId());
				 * dto.setSocietyName(invoice.getSupplierMaster().getName());
				 * dto.setSocietyCode(invoice.getSupplierMaster().getCode());
				 * dto.setSocietyDisplayName(dto.getSocietyCode() + "/" + dto.getSocietyName());
				 * // log.info("else getMaterialValue==>"+invoice.getMaterialValue());
				 * dto.setAmountWithoutTax(invoice.getMaterialValue());
				 * dto.setTax((invoice.getCgstValue()!=null?invoice.getCgstValue():0) +
				 * (invoice.getSgstValue()!=null?invoice.getSgstValue():0));
				 * 
				 * List<PurchaseInvoiceAdjustment> purchaseInvoiceAdjustmentsList =
				 * purchaseInvoiceAdjustmentRepository .findAllByInvoiceId(invoice);
				 * 
				 * List<PurchaseInvoiceAdjustment> purchaseInvoiceAdjustmentsList =
				 * purchaseInvoiceAdjustmentRepository .getInvoicesByInvoiceId(invoice.getId());
				 * 
				 * for (PurchaseInvoiceAdjustment p : purchaseInvoiceAdjustmentsList) {
				 * log.info(p.getTotalAdjustedAmount()+"<=else TotalAdjustmentAmount=>"+
				 * dto.getTotalAdjustmentAmount());
				 * dto.setTotalAdjustmentAmount(p.getTotalAdjustedAmount() +
				 * dto.getTotalAdjustmentAmount()); } List<VoucherDetails> voucherDetailsList =
				 * voucherDetailsRepository.findAllByPurchaseInvoice(invoice);
				 * 
				 * voucherDetailsList.forEach(voucherDetails -> {
				 * dto.setTotalAmountPaid(voucherDetails.getAmount() +
				 * dto.getTotalAmountPaid()); });
				 * 
				 * responseDTOMap.put(societyId, dto); }
				 */
			}

			log.info("responseDTOMap size----------" + responseDTOMap.size());
			log.info("purchaseInvoiceList size-----" + purchaseInvoiceList.size());
			if (responseDTOMap.size() > 0) {
				for (Long key : responseDTOMap.keySet()) {

					/*
					 * Double invoiceAmount=voucherDetailsRepository.
					 * getVoucherDetailsAmounyByInvoiceIdandVoucherId(invoice.getId(),voucherId);
					 * invoiceDetailsDTO.setTotalInvoiceAmountToBePaid(invoiceAmount!=null?
					 * invoiceAmount:0.0);
					 * 
					 * invoiceDetailsDTO.setBalanceAmount(((invoiceDetailsDTO.getTotalPaybleAmount()
					 * != null ? invoiceDetailsDTO.getTotalPaybleAmount(): 0.0) -
					 * (invoiceDetailsDTO.getTotalAmountPaid() != null ?
					 * invoiceDetailsDTO.getTotalAmountPaid(): 0.0)) -
					 * (invoiceAmount!=null?invoiceAmount:0.0));
					 */

					SocietyPaymentVoucherSearchResponseDTO dto = responseDTOMap.get(key);
					dto.setTotalPayableAmount(
							(dto.getAmountWithoutTax() + dto.getTax()) - dto.getTotalAdjustmentAmount());
					// dto.setBalanceAmount(dto.getTotalPayableAmount() - dto.getTotalAmountPaid());
					// dto.setTotalAmountToBePaid(dto.getTotalAmountPaid() -
					// dto.getBalanceAmount());

					List<PurchaseInvoiceDetailsDTO> purchaseInvoiceDetailsDTOList = purchaseInvoiceList;

					List<PurchaseInvoiceDetailsDTO> purchaseInvoiceDetailsDTOList1 = new ArrayList<>();

					for (PurchaseInvoiceDetailsDTO purchaseInvoiceDetailsDTO : purchaseInvoiceDetailsDTOList) {
						if (purchaseInvoiceDetailsDTO.getSocietyId().equals(dto.getSocietyId())) {
							purchaseInvoiceDetailsDTOList1.add(purchaseInvoiceDetailsDTO);
						}
					}
					log.info("purchaseInvoiceDetailsDTOList before stream size--------"
							+ purchaseInvoiceDetailsDTOList1.size());

					List<PurchaseInvoiceDetailsDTO> detailsDTOList = purchaseInvoiceDetailsDTOList1.stream()
							.collect(Collectors.groupingBy(po -> po.getInvoiceId())).entrySet().stream()
							.map(e -> e.getValue().stream()
									.reduce((f1, f2) -> new PurchaseInvoiceDetailsDTO(f1.getInvoiceId(),
											f1.getAmountWithountTax() + f2.getAmountWithountTax(),
											(f1.getTax() != null ? f1.getTax() : 0)
													+ (f2.getTax() != null ? f2.getTax() : 0),
											f1.getTotalPaybleAmount() + f2.getTotalPaybleAmount(),
											f1.getTotalAdjustedAmount() + f2.getTotalAdjustedAmount(),
											f1.getTotalAmountPaid() + f2.getTotalAmountPaid(),
											f1.getBalanceAmount() + f2.getBalanceAmount(),
											f1.getTotalInvoiceAmountToBePaid() + f2.getTotalInvoiceAmountToBePaid(),
											f1.getInvoiceDate(), f1.getInvoiceNumber(), f1.getInvoiceNumberPrefix())))
							.map(f -> f.get()).collect(Collectors.toList());

					log.info("purchaseInvoiceDetailsDTOList after add lo0p size--------" + detailsDTOList.size());
					dto.setPurchaseInvoiceDetailsDTOList(detailsDTOList);
					res.add(dto);

				}
			}

			log.info("societyPaymentVoucherSearchResponseDTOList size=========>" + res.size());

			/*
			 * for (Long key : responseDTOMap.keySet()) {
			 * 
			 * SocietyPaymentVoucherSearchResponseDTO temp = responseDTOMap.get(key); //
			 * log.info("getAmountWithoutTax==>"+temp.getAmountWithoutTax()); //
			 * log.info("getTax==>"+temp.getTax());
			 * temp.setTotalPayableAmount(temp.getAmountWithoutTax() + temp.getTax()); //
			 * log.info("totalPayableAmount==>"+temp.getTotalPayableAmount());
			 * temp.setBalanceAmount(temp.getTotalPayableAmount() -
			 * temp.getTotalAdjustmentAmount());
			 * temp.setTotalAmountToBePaid(temp.getBalanceAmount());
			 * List<PurchaseInvoiceDetailsDTO> purchaseInvoiceDetailsDTOList =
			 * getAllPurchaseInvoiceDetails( purchaseInvoices);
			 * List<PurchaseInvoiceDetailsDTO> purchaseInvoiceDetailsDTOList1 = new
			 * ArrayList<>(); for (PurchaseInvoiceDetailsDTO purchaseInvoiceDetailsDTO :
			 * purchaseInvoiceDetailsDTOList) { if
			 * (purchaseInvoiceDetailsDTO.getSocietyId().equals(temp.getSocietyId())) {
			 * //purchaseInvoiceDetailsDTO.setInvoiceId(purchaseInvoiceDetailsDTO.
			 * getVoucherDetailId());
			 * purchaseInvoiceDetailsDTOList1.add(purchaseInvoiceDetailsDTO); } }
			 * temp.setPurchaseInvoiceDetailsDTO(purchaseInvoiceDetailsDTOList1);
			 * res.add(temp);
			 * 
			 * }
			 */
		} catch (Exception e) {
			log.error("process ::  Exception ==>", e);
		}
		return res;
	}

	public List<PurchaseInvoiceDetailsDTO> getAllPurchaseInvoiceDetails(List<PurchaseInvoice> listOfPurchaseInvoices) {
		log.info("Starts SocietyStopInvoiceService.getAllPurchaseInvoiceDetails method");

		List<PurchaseInvoiceDetailsDTO> listOfAdjustmentDTO = new ArrayList<PurchaseInvoiceDetailsDTO>();
		try {
			Double totalAmountPaid = 0.0;
			Double totalAdjustmentAmount = 0.0;
			Double tax = 0.0;
			Double amountWithountTax = 0.0;
			Double totalPaybleAmount = 0.0;
			Double balanceAmount = 0.0;
			Double getTotalAmountPaidAgainstVoucherForInvoiceTemp = 0.0;
			log.info("listOfPurchaseInvoices size==>" + listOfPurchaseInvoices.size());
			if (listOfPurchaseInvoices != null) {
				for (PurchaseInvoice purchaseInvoice : listOfPurchaseInvoices) {

					PurchaseInvoiceDetailsDTO purchaseInvoiceDetailsDTO = new PurchaseInvoiceDetailsDTO();
					purchaseInvoiceDetailsDTO.setVoucherDetailsId(purchaseInvoice.getVoucherDetailId());

					/*
					 * log.info("pur vot id==>" + purchaseInvoice.getVoucherDetailId() + "<=in id=>"
					 * + purchaseInvoice.getId());
					 */
					log.info("VoucherDetail Id==> " + purchaseInvoice.getVoucherDetailId());
					log.info("purchaseInvoice Id==> " + purchaseInvoice.getId());

					List<PurchaseInvoiceAdjustment> purchaseInvoiceAdjustments = purchaseInvoiceAdjustmentRepository
							.getInvoicesByInvoiceId(purchaseInvoice.getId());
					// log.info("Invoice
					// id==>"+purchaseInvoice.getId()+"<=size=>"+purchaseInvoiceAdjustments.size());
					if (purchaseInvoiceAdjustments != null && !purchaseInvoiceAdjustments.isEmpty()
							&& purchaseInvoiceAdjustments.size() > 0) {

						log.info("purchaseInvoiceAdjustments size==> " + purchaseInvoiceAdjustments.size());
						for (PurchaseInvoiceAdjustment PurchaseInvoiceAdjustment : purchaseInvoiceAdjustments) {
							totalAdjustmentAmount += PurchaseInvoiceAdjustment.getTotalAdjustedAmount();
							log.info("Invoice id==>" + purchaseInvoice.getId() + "<=totalAdjustmentAmount=>"
									+ totalAdjustmentAmount);
						}
					}
					purchaseInvoiceDetailsDTO.setTotalAdjustedAmount(totalAdjustmentAmount);
					amountWithountTax = (purchaseInvoice.getMaterialValue() != null)
							? purchaseInvoice.getMaterialValue()
							: 0.0;
					log.info("getAllPurchaseInvoiceDetails :: amountWithountTax==>" + amountWithountTax);
					purchaseInvoiceDetailsDTO.setAmountWithountTax(amountWithountTax);
					// log.info("getCgstValue==>"+purchaseInvoice.getCgstValue()+"<=getSgstValue=>"+purchaseInvoice.getSgstValue());
					// tax = ((purchaseInvoice.getCgstValue() != null) ?
					// purchaseInvoice.getCgstValue() : 0.0) + ((purchaseInvoice.getIgstValue() !=
					// null) ? purchaseInvoice.getIgstValue() : 0.0);
					tax = ((purchaseInvoice.getCgstValue() != null) ? purchaseInvoice.getCgstValue() : 0.0)
							+ ((purchaseInvoice.getSgstValue() != null) ? purchaseInvoice.getSgstValue() : 0.0);
					log.info("getAllPurchaseInvoiceDetails :: tax==>" + tax);
					purchaseInvoiceDetailsDTO.setTax(tax);

					totalPaybleAmount = amountWithountTax + tax;
					log.info("getAllPurchaseInvoiceDetails :: totalPaybleAmount==>" + totalPaybleAmount);
					// log.info("amountWithountTax==>"+amountWithountTax+"<=tax=>"+tax+"<=totalPaybleAmount=>"+totalPaybleAmount);
					purchaseInvoiceDetailsDTO.setTotalPaybleAmount(totalPaybleAmount);
					log.info("getAllPurchaseInvoiceDetails :: purchase invoice id==>" + purchaseInvoice.getId());
					totalAmountPaid = voucherDetailsRepository.getTotalVchAmtPurchaseInvId(purchaseInvoice.getId());
					log.info("getAllPurchaseInvoiceDetails :: totalAmountPaid==>" + totalAmountPaid);
					if (totalAmountPaid == null || totalAmountPaid.isNaN()) {
						purchaseInvoiceDetailsDTO.setTotalAmountPaid(0.0);
					} else {
						purchaseInvoiceDetailsDTO.setTotalAmountPaid(totalAmountPaid);
					}

					balanceAmount = totalPaybleAmount - totalAdjustmentAmount
							- ((totalAmountPaid != null) ? totalAmountPaid : 0.0);
					log.info("getAllPurchaseInvoiceDetails :: balanceAmount==> " + balanceAmount);
					purchaseInvoiceDetailsDTO.setBalanceAmount(balanceAmount);
					log.info("getAllPurchaseInvoiceDetails :: invoice number==> " + purchaseInvoice.getInvoiceNumber());
					purchaseInvoiceDetailsDTO.setInvoiceNumber(purchaseInvoice.getInvoiceNumber());
					log.info("getAllPurchaseInvoiceDetails :: invoice date==> " + purchaseInvoice.getInvoiceDate());
					purchaseInvoiceDetailsDTO.setInvoiceDate(purchaseInvoice.getInvoiceDate());
					log.info("getAllPurchaseInvoiceDetails :: invoice id==>" + purchaseInvoice.getId());
					purchaseInvoiceDetailsDTO.setInvoiceId(purchaseInvoice.getId());
					log.info("getAllPurchaseInvoiceDetails :: getInvoiceNumberPrefix==> "
							+ purchaseInvoice.getInvoiceNumberPrefix());
					purchaseInvoiceDetailsDTO.setInvoiceNumberPrefix(purchaseInvoice.getInvoiceNumberPrefix());
					// to get the voucher amount paid for a particular invoice.
					getTotalAmountPaidAgainstVoucherForInvoiceTemp = voucherDetailsRepository
							.getTotalAmountPaidAgainstVoucherForInvoice(purchaseInvoice.getId());
					purchaseInvoiceDetailsDTO
							.setTotalInvoiceAmountToBePaid((getTotalAmountPaidAgainstVoucherForInvoiceTemp != null)
									? getTotalAmountPaidAgainstVoucherForInvoiceTemp
									: 0.0);
					log.info("getAllPurchaseInvoiceDetails :: supplier master id==> "
							+ purchaseInvoice.getSupplierMaster().getId());
					purchaseInvoiceDetailsDTO.setSocietyId(purchaseInvoice.getSupplierMaster().getId());
					listOfAdjustmentDTO.add(purchaseInvoiceDetailsDTO);

				}
			}
			log.info("Ended SocietyStopInvoiceService.getAllPurchaseInvoiceDetails method");
		} catch (Exception e) {
			log.error("getAllPurchaseInvoiceDetails", e);
		}
		return listOfAdjustmentDTO;

	}

	public BaseDTO getCircleMasterbyDistrictIds(List<Long> districIds) {
		log.info("<--Starts SocietyPaymentVoucherService .getCircleMasterbyDistrictIds-->");
		log.info("<--districIds ID : " + districIds.size());
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<Long> circleIdList = new ArrayList<Long>();
			List<CircleMaster> circleMasterList = new ArrayList<CircleMaster>();
			if (districIds != null) {
				circleIdList = supplierMasterRepository.getCircleMasterIdsBydistrictIds(districIds);
			}
			if (circleIdList != null && !circleIdList.isEmpty()) {
				circleMasterList = circleMasterRepository.findAllCircleMasterByIdList(circleIdList);
			}
			baseDTO.setResponseContents(circleMasterList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception ex) {
			log.error("exception Occured ", ex);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<--End SocietyPaymentVoucherService .getCircleMasterbyDistrictIds-->");
		return baseDTO;
	}

	// Wrote by Vinayak 2019-10-29
	public BaseDTO getCircleDetailsbyDate(SearchSocietyInvoiceAdjustmentDTO SSIVA) {
		log.info("==============================SocietyPaymentVoucherService========================= : ");
		log.info("<--districIds ID : " + SSIVA.getSocietyIds().toString());
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<Long> circleIdList = new ArrayList<Long>();
			List<CircleMaster> circleMasterList = new ArrayList<CircleMaster>();
			log.info("==============================District Code ========================= : "
					+ SSIVA.getSocietyIds().toString());
			if (SSIVA.getSocietyIds().isEmpty()) {
				// circleIdList=supplierMasterRepository.getCircleMasterIdsBydistrictIds(districIds);
				circleMasterList = circleMasterRepository.findAllActiveCircleByInvoice(SSIVA.getInvoiceFromDate(),
						SSIVA.getInvoiceToDate());
				log.info("==============================Size 0========================= : ");
			} else {
				log.info("==============================Not Empty1========================= : ");
				circleMasterList = circleMasterRepository.findAllActiveCircleMastersbyDistrictCode(
						SSIVA.getInvoiceFromDate(), SSIVA.getInvoiceToDate(), SSIVA.getSocietyIds());
				log.info("==============================Not Empty========================= : ");
			}
			baseDTO.setResponseContents(circleMasterList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception ex) {
			log.error("exception Occured ", ex);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<--End SocietyPaymentVoucherService .getCircleMasterbyDistrictIds-->");
		return baseDTO;
	}

	public BaseDTO getLoomTypeByCircleIds(List<Long> circleIds) {
		log.info("<--SocietyPaymentVoucher .getAllLoomType() Started-->");
		BaseDTO baseDTO = new BaseDTO();
		List<Object> object = new ArrayList<>();
		SupplierMaster loomType = new SupplierMaster();
		List<SupplierMaster> loomTypeLists = new ArrayList<>();
		try {
			object = supplierMasterRepository.findLoomByCircleIds(circleIds);
			if (object != null && object.size() > 0) {
				log.info("<--SocietyPaymentVoucher object Size == ->" + object.size());
				for (int i = 0; i < object.size(); i++) {
					if (object.get(i) != null) {
						loomType = new SupplierMaster();
						log.info("<-  ===  -->" + object.get(i).toString());
						loomType.setLoomType(object.get(i).toString());
						loomTypeLists.add(loomType);
					}
				}
				log.info("getAllLoomType size() " + loomTypeLists.size());
				baseDTO.setResponseContents(loomTypeLists);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				log.info("getAllLoomType is empty ");
				baseDTO.setStatusCode(ErrorDescription.ERROR_EMPTY_LIST.getCode());
			}

		} catch (Exception e) {
			log.error("Exception in getAllLoomType() ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return baseDTO;
	}

	// Wrote by vinayak

	public BaseDTO getLoomTypeMasterDetails(SearchSocietyInvoiceAdjustmentDTO ss) {
		log.info("<--------SocietyPaymentVoucher .getAllLoomType() Started------>");
		BaseDTO baseDTO = new BaseDTO();
		List<Object> object = new ArrayList<>();
		SupplierMaster loomType = new SupplierMaster();
		List<SupplierMaster> loomTypeLists = new ArrayList<>();
		try {
			log.info("<--------SocietyPaymentVoucher .getAllLoomType() Started------>");
			if (ss.getSocietyIds().isEmpty() && ss.getProductVarietyIds().isEmpty()) {
				log.info("<------STEP4------>");
				object = supplierMasterRepository.findAllLoomTypebyFromdateandTodate(ss.getInvoiceFromDate(),
						ss.getInvoiceToDate());
			} else {
				log.info("<------STEP1------>");

				if (!ss.getProductVarietyIds().isEmpty() && ss.getSocietyIds().isEmpty()) {
					log.info("<------STEP2------>");
					object = supplierMasterRepository.findAllLoomTypebyDistrictCode(ss.getInvoiceFromDate(),
							ss.getInvoiceToDate(), ss.getProductVarietyIds());
				} else if (ss.getProductVarietyIds().isEmpty() && !ss.getSocietyIds().isEmpty()) {
					log.info("<------STEP3------>");
					object = supplierMasterRepository.findAllLoomTypebyCircleCode(ss.getInvoiceFromDate(),
							ss.getInvoiceToDate(), ss.getSocietyIds());
				} else {
					log.info("<------STEP5------>");
					object = supplierMasterRepository.findAllLoomTypebyDistrictCodeandCircleCode(
							ss.getInvoiceFromDate(), ss.getInvoiceToDate(), ss.getSocietyIds(),
							ss.getProductVarietyIds());
				}

			}

			// object = supplierMasterRepository.findLoomByCircleIds(circleIds);
			if (object != null && object.size() > 0) {
				log.info("<--SocietyPaymentVoucher object Size == ->" + object.size());
				for (int i = 0; i < object.size(); i++) {
					if (object.get(i) != null) {
						loomType = new SupplierMaster();
						log.info("<-  ===  -->" + object.get(i).toString());
						loomType.setLoomType(object.get(i).toString());
						loomTypeLists.add(loomType);
					}
				}
				log.info("getAllLoomType size() " + loomTypeLists.size());
				baseDTO.setResponseContents(loomTypeLists);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				log.info("getAllLoomType is empty ");
				baseDTO.setStatusCode(ErrorDescription.ERROR_EMPTY_LIST.getCode());
			}

		} catch (Exception e) {
			log.error("Exception in getAllLoomType() ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return baseDTO;
	}

	public BaseDTO getSocietyByLoomTypes(List<String> loomtypes) {
		log.info("<--Starts SocietyInvoiceAdjustmentService .getSocietyDropdown-->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<Object[]> supplierMasterList = supplierMasterRepository.findAllByLoomTypes(loomtypes);

			List<SocietyCodeDropDownDTO> societyCodeDropDownDTOSList = new ArrayList<>();
			if (supplierMasterList != null && supplierMasterList.size() > 0) {
				supplierMasterList.forEach(supplierMaster -> {
					SocietyCodeDropDownDTO societyCodeDropDownDTO = new SocietyCodeDropDownDTO();
					societyCodeDropDownDTO.setId(new Long(supplierMaster[0].toString()));
					societyCodeDropDownDTO.setCode((String) supplierMaster[1]);
					societyCodeDropDownDTO.setName((String) supplierMaster[2]);
					societyCodeDropDownDTO
							.setDisplayName(societyCodeDropDownDTO.getCode() + "/" + societyCodeDropDownDTO.getName());

					societyCodeDropDownDTOSList.add(societyCodeDropDownDTO);
				});
				baseDTO.setResponseContents(societyCodeDropDownDTOSList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}

		} catch (Exception exception) {
			log.error("exception Occured ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	//
	public BaseDTO getSocietyByLoomTypesv1(SearchSocietyInvoiceAdjustmentDTO loomtypes) {
		log.info("<--=============Starts SocietyInvoiceAdjustmentService .getSocietyDropdown==============-->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("<--=============Starts SocietyInvoiceAdjustmentService Step14==============--> "
					+ loomtypes.getProductVarietyIds());

			Date fromDate = loomtypes.getInvoiceFromDate();
			Date toDate = loomtypes.getInvoiceToDate();
			log.info("SocietyPaymentVoucherService. getAllDropdownListbyInvoicedate() - fromDate: " + fromDate);
			log.info("SocietyPaymentVoucherService. getAllDropdownListbyInvoicedate() - toDate: " + toDate);
			
			if (fromDate != null && toDate != null) {

				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				String fromDateStr = dateFormat.format(fromDate);
				String toDateStr = dateFormat.format(toDate);
				String queryContent = JdbcUtil.getApplicationQueryContent(jdbcTemplate,
						"SOCIETY_PAYMENT_FILTER_DROPDOWN_DATA_SQL");

				if (StringUtils.isEmpty(queryContent)) {
					baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getErrorCode());
					return baseDTO;
				}

				if (StringUtils.isNotEmpty(queryContent)) {

					queryContent = StringUtils.replace(queryContent, ":fromDate",
							AppUtil.getValueWithSingleQuote(fromDateStr));
					queryContent = StringUtils.replace(queryContent, ":toDate",
							AppUtil.getValueWithSingleQuote(toDateStr));

					if (loomtypes.getProductVarietyIds() != null && !loomtypes.getProductVarietyIds().isEmpty()) {
						log.info("----------------------DIST--------------------");
						queryContent = queryContent + " and dm.id in ("
								+ AppUtil.commaSeparatedIds(loomtypes.getProductVarietyIds()) + ")";
					}
					if (loomtypes.getSocietyIds() != null && !loomtypes.getSocietyIds().isEmpty()) {
						log.info("----------------------CIRCLE--------------------");
						queryContent = queryContent + " and sm.circle_id in ("
								+ AppUtil.commaSeparatedIds(loomtypes.getSocietyIds()) + ")";
					}
					if (loomtypes.getLoomTypeIds() != null && !loomtypes.getLoomTypeIds().isEmpty()) {
						log.info("----------------------LOOM--------------------");
						String lpt = loomtypes.getLoomTypeIds().toString();
						lpt = lpt.substring(1, lpt.length() - 1);
						lpt = "'" + lpt.replace(" ", "'") + "'";
						lpt = lpt.replace(",", "',");
						log.info("----------------------LType--------------------");
						queryContent = queryContent + " and sm.loom_type in (" + lpt + ")";
					}
					log.info("----------------------Query1--------------------" + queryContent);
				//	queryContent = queryContent + " group by sm.id,dm.id,sm.circle_id";
					List<Map<String, Object>> list = jdbcTemplate.queryForList(queryContent);
					log.info("----------------------DIST-------------------- : " + list.size());
					List<SocietyCodeDropDownDTO> societyCodeDropDownDTOSList = new ArrayList<>();
					for (Map<String, Object> data : list) {
						SocietyCodeDropDownDTO societyCodeDropDownDTO = new SocietyCodeDropDownDTO();
						Long id = Long.valueOf(data.get("supplier_id").toString());
						String code = data.get("supplier_code").toString();
						String name = data.get("supplier_name").toString();

						societyCodeDropDownDTO.setId(id);
						societyCodeDropDownDTO.setCode(code);
						societyCodeDropDownDTO.setName(name);
						societyCodeDropDownDTO.setDisplayName(
								societyCodeDropDownDTO.getCode() + "/" + societyCodeDropDownDTO.getName());

						societyCodeDropDownDTOSList.add(societyCodeDropDownDTO);
					}
					baseDTO.setResponseContents(societyCodeDropDownDTOSList);
					baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				}
			}
			log.info("<--=============Starts SocietyInvoiceAdjustmentService Step15==============--> "
					+ loomtypes.getSocietyIds());
			log.info("<--=============Starts SocietyInvoiceAdjustmentService Step16==============--> "
					+ loomtypes.getLoomTypeIds());
			log.info("<--=============Starts SocietyInvoiceAdjustmentService Step17==============--> "
					+ loomtypes.getProductVarietyIds());
		} catch (Exception exception) {
			log.error("exception Occured ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	// This Method used for get district code

	public BaseDTO getDistrictCodebyInvoicedate(SearchSocietyInvoiceAdjustmentDTO ssiaDTO) {
		log.info("<--++++++++++++++++Starts getDistrictCodebyInvoicedate .getSocietyDropdown+++++++++++++-->"
				+ ssiaDTO.getInvoiceFromDate());
		BaseDTO baseDTO = new BaseDTO();
		List<DistrictMaster> ObjectList = new ArrayList<DistrictMaster>();

		try {
			ObjectList = districtMasterRepository.findDistrictCode(ssiaDTO.getInvoiceFromDate(),
					ssiaDTO.getInvoiceToDate());
			log.info("=====================DistrictCodeSize================== : " + ObjectList.size());
			if (ObjectList != null && ObjectList.size() > 0) {
				baseDTO.setResponseContents(ObjectList);
			} else {
				log.error("========DistrictCodeList is Empty============ ");
				baseDTO.setStatusCode(ErrorDescription.ERROR_EMPTY_LIST.getCode());
			}

		} catch (Exception exception) {
			log.error("exception Occured ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO generatePDFBasedOnSocietyPaymentId(Long societyPaymentId) {

		BaseDTO baseDTO = new BaseDTO();
		FileOutputStream fos = null;
		ZipOutputStream zipOS = null;
		String pdfZipFilePath = null;
		int branRefno = 1;
		List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> listofData1 = new ArrayList<Map<String, Object>>();
		String mainQuery = "select sipd.branch_code as brandcode,array_agg(sm.code) as code,array_agg(sm.name) as name, "
				+ "sum(sipd.payable_amount) as paybleamount,sipd.bank_name as bankname,\n"
				+ "concat(v.reference_number_prefix,'-',v.reference_number) as voucherNumber "
				+ "FROM society_invoice_payment_details sipd " + "join supplier_master sm on sipd.supplier_id = sm.id "
				+ "join society_invoice_payment sip on sip.id=sipd.society_invoice_payment_id \n"
				+ "join voucher v on v.id=sip.voucher_id " + "where sipd.society_invoice_payment_id ="
				+ societyPaymentId
				+ " group by sipd.branch_code,sipd.bank_name,concat(v.reference_number_prefix,'-',v.reference_number) order by branch_code ";

		listofData = jdbcTemplate.queryForList(mainQuery);

		int refNumber = 1;

		try {

			// <Current_Year>-<Increment>

			Object adviceNumMaxObj = JdbcUtil.getValue(jdbcTemplate,
					"select coalesce(max(substring(advice_number,6,length(advice_number))::int),0) AS max_advice_number "
							+ "FROM society_invoice_payment_details where substring(advice_number,1,4)::int="
							+ getCurrentYr(),
					null, "max_advice_number");

			if (adviceNumMaxObj == null) {
				String adviceMaxNumberStr = JdbcUtil.getAppConfigValue(jdbcTemplate,
						AppConfigKey.SOCIETY_INVOICE_PAYMENT_ADVICE_NUMBER_MAX);

				Integer intValue = AppUtil.stringToInteger(adviceMaxNumberStr, 0);
				refNumber = intValue != null ? intValue.intValue() + 1 : refNumber;

			} else {
				Integer intValue = AppUtil.stringToInteger(String.valueOf(adviceNumMaxObj), 0);
				refNumber = intValue != null ? intValue.intValue() + 1 : refNumber;
			}

			List<Map<String, Object>> cv = null;
			String templateContent = AppUtil
					.getFileContent("/in/gov/cooptex/operation/template/spsPaymentTemplate.html", true);

			// StringBuilder idsSb = new StringBuilder();
			// String branchCode = null;

			List<String> filePathList = new ArrayList<String>();
			for (Map<String, Object> obj1 : listofData) {
				String branchcode = (String) obj1.get("brandcode");
				String voucherNumber = (String) obj1.get("vouchernumber");
				if (obj1.get("brandcode") == null) {
					baseDTO.setErrorDescription("Selected Society Payment having Empty Branch");
					return baseDTO;
				}

				StringBuilder sb = new StringBuilder();
				listofData1 = new ArrayList<Map<String, Object>>();
				String outputFilePath = null;
				int sn = 1;
				String subQuery = "select array_agg(sipd.id) as arrayid,sipd.branch_code as brandcode,sm.code as code,"
						+ "sm.name as name, " + "sum(sipd.payment_amount) as paybleamount,sipd.bank_name as bankname,"
						+ "sipd.branch_name as branchname  " + "FROM society_invoice_payment_details sipd "
						+ "join supplier_master sm on sipd.supplier_id = sm.id " + " where sipd.branch_code = '"
						+ branchcode + "'" + " and sipd.society_invoice_payment_id= " + societyPaymentId
						+ " group by sipd.branch_code,sm.code,sm.name,sipd.bank_name,sipd.branch_name order by branch_code ";
				listofData1 = jdbcTemplate.queryForList(subQuery);
				double total = 0;
				DecimalFormat twoPlaces = new DecimalFormat("0.00");
				String adviceNum = "";
				String adviceDateStr = "";
				for (Map<String, Object> obj2 : listofData1) {

					SocietyInvoicePaymentDto societyInvoicePaymentDto = new SocietyInvoicePaymentDto();

					System.out.println("-- ----------------------------------");

					if (obj2.get("brandcode") != null) {
						societyInvoicePaymentDto.setBranchCode((obj2.get("brandcode").toString()));
					}
					if (obj2.get("paidmonth") != null) {
						societyInvoicePaymentDto.setPaidmonth(Long.valueOf(obj2.get("paidmonth").toString()));
					}

					if (obj2.get("code") != null) {
						societyInvoicePaymentDto.setSocietyCode(obj2.get("code").toString());
					}
					if (obj2.get("name") != null) {
						societyInvoicePaymentDto.setSocietyName(obj2.get("name").toString());
					}
					if (obj2.get("paybleamount") != null) {
						societyInvoicePaymentDto
								.setTotalPayableAmount(Double.valueOf(obj2.get("paybleamount").toString()));
					}
					if (obj2.get("bankname") != null) {
						societyInvoicePaymentDto.setBankName(obj2.get("bankname").toString());
					}
					if (obj2.get("branchname") != null) {
						societyInvoicePaymentDto.setBranchBank(obj2.get("branchname").toString());
					}

					System.out.println("amount: " + societyInvoicePaymentDto.getTotalPayableAmount());

					if (societyInvoicePaymentDto.getTotalPayableAmount() > 0) {

						sb.append("<tr>");
						sb.append("<td width=\"5%\">").append(sn).append(".").append("</td>");
						String societyName = null;
						if(null != societyInvoicePaymentDto.getSocietyName()){
							if(societyInvoicePaymentDto.getSocietyName().contains("&")){
								societyName = societyInvoicePaymentDto.getSocietyName().replace("&", "&amp;");
							}else{
								societyName = societyInvoicePaymentDto.getSocietyName();
							}
						}
						sb.append("<td width=\"45%\">").append(societyName).append("")
								.append(societyInvoicePaymentDto.getSocietyCode()).append("</td>");
						sb.append("<td width=\"35%\">")
								.append(societyInvoicePaymentDto.getBankName() != null
										? societyInvoicePaymentDto.getBankName()
										: "")
								.append("<br/>")
								.append(societyInvoicePaymentDto.getBranchBank() != null
										? societyInvoicePaymentDto.getBranchBank()
										: "")
								.append("</td>");
						sb.append("<td width=\"15%\" align=\"right\">")
								.append(twoPlaces.format(societyInvoicePaymentDto.getTotalPayableAmount()))
								.append("</td>");
						sb.append("</tr>");

						sn++;
					}

					System.out.println("-- ----------------------------------");

					/*
					 * Integer sipdId = (Integer) idObj.get("sipd_id");
					 * idsSb.append(sipdId).append(",");
					 */

					String val = obj2.get("arrayid").toString();
					String[] value = val.split(",");
					Date currendate = new Date();
					if (value != null && value.length > 0) {
						for (String idob : value) {
							String idValues = idob.replace("{", "");
							String idValues1 = idValues.replace("}", "");
							idValues1.trim();
							String currentYear = getCurrentYr();

							String adviceNumber = currentYear + "-" + refNumber;
							Map<String, Object> advNumObj = checkAdviceNumberAlreadyExist(idValues1);
							Date adviceDate = null;

							if (null != advNumObj && advNumObj.get("adviceNumber") != null) {
								adviceNumber = advNumObj.get("adviceNumber").toString();
								adviceDate = advNumObj.get("advice_date") != null ? (Date) advNumObj.get("advice_date")
										: currendate;
							} else {
								updateAdviceNumberAndDate(adviceNumber, idValues1);
								adviceDate = currendate;
							}
							SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
							adviceDateStr = formatter.format(adviceDate);
							adviceNum = adviceNumber;
						}

					}
					total = total + twoPlaces.parse(twoPlaces.format(societyInvoicePaymentDto.getTotalPayableAmount()))
							.doubleValue();
				}

				sb.append("<tr>");
				sb.append("<td>&nbsp;</td>");
				sb.append("<td>&nbsp;</td>");
				sb.append("<td align=\"right\">Total</td>");
				sb.append("<td align=\"right\">").append(twoPlaces.format(total)).append("</td>");
				sb.append("</tr>");

				String amountToWord = AmountToWordConverter.converter(String.valueOf(twoPlaces.format(total)));

				sb.append("<tr>");
				sb.append("<td colspan=\"4\" align=\"left\">").append(amountToWord).append("</td>");
				sb.append("</tr>");
				String content = sb.toString();

				String contentForPDF = templateContent.replace("#{NUMBER}", adviceNum);

				contentForPDF = contentForPDF.replace("#{DATE}", adviceDateStr);

				if (StringUtils.isNotEmpty(voucherNumber)) {
					String[] voucherNumberArray = voucherNumber.split("-");
					if (voucherNumberArray.length >= 3) {
						voucherNumber = voucherNumberArray[0] + "-" + voucherNumberArray[1] + "-"
								+ voucherNumberArray[2];
					}
				}

				contentForPDF = contentForPDF.replace("#{VOUCHERNUMBER}", voucherNumber);

				contentForPDF = contentForPDF.replace("#{CONTENT}", content);

				String downloadPath = JdbcUtil.getAppConfigValue(jdbcTemplate, AppConfigKey.REPORT_PDF_DOWNLOAD_PATH);
				File pdfDir = new File(downloadPath);

				if (!pdfDir.isDirectory()) {
					pdfDir.mkdirs();
				}

				File htmlDir = new File(pdfDir.getAbsolutePath() + "/html");

				if (!htmlDir.isDirectory()) {
					htmlDir.mkdirs();
				}
				branRefno++;
				String branchcode1 = obj1.get("brandcode") != null ? (String) obj1.get("brandcode") : "";

				String inputFilePath = htmlDir.getAbsolutePath() + ".html";

				outputFilePath = pdfDir.getAbsolutePath() + "/" + branchcode1 + "" + branRefno + ".pdf";

				AppUtil.writeToFile(contentForPDF, inputFilePath);
				HeaderFooterPageEvent event = new HeaderFooterPageEvent();

				event.setLeftFooterTextLineOne("BANK ADVICE");
				event.setLeftFooterTextLineTwo("");

				createPDF(new File(inputFilePath), new File(outputFilePath), event);

				refNumber++;
				filePathList.add(outputFilePath);

				int bankAdvaiceSize = filePathList != null ? filePathList.size() : 0;

				if (bankAdvaiceSize > 0) {
					pdfZipFilePath = getZipFilePath();
					fos = new FileOutputStream(pdfZipFilePath);
					zipOS = new ZipOutputStream(fos);

					for (String FilePath : filePathList) {
						writeToZipFile(FilePath, zipOS);
					}

					File zipFile = new File(pdfZipFilePath);

					if (!zipFile.isFile() || zipFile.length() <= 0) {
						pdfZipFilePath = null;
						throw new Exception("Zip file not written");
					}
				}
				baseDTO.setResponseContent(pdfZipFilePath);
			}

		} catch (Exception e) {
			log.error("exception Occured ", e);

		} finally {
			try {
				if (zipOS != null) {
					zipOS.close();
				}
				if (fos != null) {
					fos.close();
				}
			} catch (Exception exx) {

			}
		}

		return baseDTO;

	}

	private void updateAdviceNumberAndDate(String adviceNumber, String idValues) {
		jdbcTemplate.update("UPDATE society_invoice_payment_details SET advice_date=now(), advice_number='"
				+ adviceNumber + "' WHERE id IN(" + idValues + ");");

	}

	private Map<String, Object> checkAdviceNumberAlreadyExist(String idValues) {
		Map<String, Object> dataMap = new HashMap<>();
		String subQuery = "SELECT advice_number  AS adviceNumber,advice_date AS advice_date FROM society_invoice_payment_details WHERE id IN("
				+ idValues + ");";
		List<Map<String, Object>> mapList = jdbcTemplate.queryForList(subQuery);
		if (!CollectionUtils.isEmpty(mapList)) {
			dataMap = mapList.get(0);
		}
		return dataMap;
	}

	private void writeToZipFile(String FilePath, ZipOutputStream zipOS) throws IOException {

		log.info("Writing file : '" + FilePath + "' to zip file");

		File aFile = new File(FilePath);
		FileInputStream fis = new FileInputStream(aFile);

		ZipEntry zipEntry = new ZipEntry(aFile.getName());
		zipOS.putNextEntry(zipEntry);

		byte[] bytes = new byte[1024];
		int length;
		while ((length = fis.read(bytes)) >= 0) {
			zipOS.write(bytes, 0, length);
		}

		zipOS.closeEntry();
		fis.close();

	}

	private String getZipFilePath() {
		String filePathforbanch = JdbcUtil.getAppConfigValue(jdbcTemplate, AppConfigKey.REPORT_PDF_DOWNLOAD_PATH);

		File file = new File(filePathforbanch);
		if (file.isFile()) {
			log.info("Invalid directory name. Directory path pointing to a file.");
			throw new RestException(
					"Invalid directory name. Directory path pointing to a file. File: " + file.getAbsolutePath());
		}

		if (filePathforbanch.charAt(filePathforbanch.length() - 1) != '/') {
			filePathforbanch = filePathforbanch + "/";
		}
		Date date = new Date();
		// This method returns the time in millis
		long timeMilli = date.getTime();
		Calendar calendar = Calendar.getInstance();
		// Returns current time in millis
		long timeMilli2 = calendar.getTimeInMillis();
		return (filePathforbanch + "SocietyPayment" + "-" + timeMilli2 + ".zip");
	}

	public static void createPDF(File htmlInputFile, File pdfOutputFile, HeaderFooterPageEvent headerFooterPageEvent)
			throws Exception {
		// step 1
		Document document = new Document();
		// step 2
		PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfOutputFile));
		writer.setInitialLeading(12);
		writer.setPageEvent(headerFooterPageEvent);
		// step 3
		document.open();
		// step 4
		XMLWorkerHelper.getInstance().parseXHtml(writer, document, new FileInputStream(htmlInputFile));

		// step 5
		document.close();

	}

	public BaseDTO getAllDropdownListbyInvoicedateOld(SearchSocietyInvoiceAdjustmentDTO ssiaDTO) {
		log.info("getAllDropdownListbyInvoicedate method starts" + ssiaDTO.getInvoiceFromDate());
		BaseDTO baseDTO = new BaseDTO();
		SocietyInvoicePaymentDropDownDTO societyInvoicePaymentDropDownDTO = new SocietyInvoicePaymentDropDownDTO();

		try {
			String fdate = new SimpleDateFormat("yyyy-MM-dd").format(ssiaDTO.getInvoiceFromDate());
			String tdate = new SimpleDateFormat("yyyy-MM-dd").format(ssiaDTO.getInvoiceToDate());
			ApplicationQuery appuery = applicationQueryRepository
					.findByQueryName("SOCIETY_CODE_DROPDOWN_FOR_SOCIETY_PAYMENT");

			societyInvoicePaymentDropDownDTO.setDistrictMasterList(districtMasterRepository
					.getDistrictCodeByInvoiceCreatedDate(ssiaDTO.getInvoiceFromDate(), ssiaDTO.getInvoiceToDate()));
			societyInvoicePaymentDropDownDTO.setCircleMasterList(circleMasterRepository
					.findAllActiveCircleMastersbyDate(ssiaDTO.getInvoiceFromDate(), ssiaDTO.getInvoiceToDate()));

			if (appuery != null) {
				String queryContent = appuery.getQueryContent();
				log.info("----------------------Query--------------------" + queryContent);
				if (queryContent != null && !queryContent.isEmpty()) {
					queryContent = queryContent.replace(":fdate", "'" + fdate + "'");
					queryContent = queryContent.replace(":tdate", "'" + tdate + "'");
				}
				queryContent = queryContent + " group by sm.id";
				List<Map<String, Object>> list = jdbcTemplate.queryForList(queryContent);
				log.info("--list-Size : " + list.size());
				List<SocietyCodeDropDownDTO> societyCodeDropDownDTOSList = new ArrayList<>();
				for (Map<String, Object> data : list) {
					SocietyCodeDropDownDTO societyCodeDropDownDTO = new SocietyCodeDropDownDTO();
					Long id = Long.valueOf(data.get("id").toString());
					String code = data.get("code").toString();
					String name = data.get("name").toString();
					societyCodeDropDownDTO.setId(id);
					societyCodeDropDownDTO.setCode(code);
					societyCodeDropDownDTO.setName(name);
					societyCodeDropDownDTO
							.setDisplayName(societyCodeDropDownDTO.getCode() + "/" + societyCodeDropDownDTO.getName());

					societyCodeDropDownDTOSList.add(societyCodeDropDownDTO);
				}

				societyInvoicePaymentDropDownDTO.setSocietyCodeDropDownDTOList(societyCodeDropDownDTOSList);

			}

			/*
			 * List<Object> object = new ArrayList<>(); SupplierMaster loomType = new
			 * SupplierMaster(); List<SupplierMaster> loomTypeLists = new ArrayList<>();
			 * object = supplierMasterRepository.findAllLoomTypebyFromdateandTodate(ssiaDTO.
			 * getInvoiceFromDate(),ssiaDTO.getInvoiceToDate());
			 * 
			 * if (object != null && object.size() > 0) {
			 * log.info("<--SocietyPaymentVoucher object Size == ->" + object.size()); for
			 * (int i = 0; i < object.size(); i++) { if (object.get(i) != null) { loomType =
			 * new SupplierMaster(); log.info("<-  ===  -->" + object.get(i).toString());
			 * loomType.setLoomType(object.get(i).toString()); loomTypeLists.add(loomType);
			 * } } log.info("getAllLoomType size() " + loomTypeLists.size());
			 * baseDTO.setResponseContents(loomTypeLists);
			 * baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode()); }
			 */

			societyInvoicePaymentDropDownDTO.setOrderTypeList(purchaseInvoiceRepository
					.getOrderTypeByToDateFromDate(ssiaDTO.getInvoiceFromDate(), ssiaDTO.getInvoiceToDate()));
			societyInvoicePaymentDropDownDTO.setLoomTypeList(supplierMasterRepository
					.getLoomTypebyToDateFromDate(ssiaDTO.getInvoiceFromDate(), ssiaDTO.getInvoiceToDate()));

			if (!societyInvoicePaymentDropDownDTO.getDistrictMasterList().isEmpty()
					|| !societyInvoicePaymentDropDownDTO.getCircleMasterList().isEmpty()
					|| !societyInvoicePaymentDropDownDTO.getSocietyCodeDropDownDTOList().isEmpty()
					|| !societyInvoicePaymentDropDownDTO.getLoomTypeList().isEmpty()
					|| !societyInvoicePaymentDropDownDTO.getOrderTypeList().isEmpty()) {
				baseDTO.setResponseContent(societyInvoicePaymentDropDownDTO);
			} else {
				log.error("========getAllDropdownListbyInvoicedate is NULL============ ");
				baseDTO.setStatusCode(ErrorDescription.ERROR_EMPTY_LIST.getCode());
			}

		} catch (Exception exception) {
			log.error("exception Occured ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	/**
	 * 
	 * @param ssiaDTO
	 * @return
	 */
	public BaseDTO getAllDropdownListbyInvoicedate(SearchSocietyInvoiceAdjustmentDTO adjustmentDTO) {
		log.info("SocietyPaymentVoucherService. getAllDropdownListbyInvoicedate() - START");
		BaseDTO baseDTO = new BaseDTO();
		SocietyInvoicePaymentDropDownDTO societyInvoicePaymentDropDownDTO = new SocietyInvoicePaymentDropDownDTO();
		try {

			Date fromDate = adjustmentDTO.getInvoiceFromDate();
			Date toDate = adjustmentDTO.getInvoiceToDate();
			log.info("SocietyPaymentVoucherService. getAllDropdownListbyInvoicedate() - fromDate: " + fromDate);
			log.info("SocietyPaymentVoucherService. getAllDropdownListbyInvoicedate() - toDate: " + toDate);

			if (fromDate != null && toDate != null) {

				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				String fromDateStr = dateFormat.format(fromDate);
				String toDateStr = dateFormat.format(toDate);
				String queryContent = JdbcUtil.getApplicationQueryContent(jdbcTemplate,
						"SOCIETY_PAYMENT_FILTER_DROPDOWN_DATA_SQL");

				if (StringUtils.isEmpty(queryContent)) {
					baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getErrorCode());
					return baseDTO;
				}

				List<String> orderTypeList = null;
				List<String> loomTypeList = null;
				List<CircleMaster> circleMasterList = null;
				List<DistrictMaster> districtMasterList = null;
				List<SocietyCodeDropDownDTO> supplierList = null;
				int mapListSize = 0;
				if (StringUtils.isNotEmpty(queryContent)) {

					queryContent = StringUtils.replace(queryContent, ":fromDate",
							AppUtil.getValueWithSingleQuote(fromDateStr));
					queryContent = StringUtils.replace(queryContent, ":toDate",
							AppUtil.getValueWithSingleQuote(toDateStr));

					List<Map<String, Object>> mapList = jdbcTemplate.queryForList(queryContent);
					mapListSize = mapList != null ? mapList.size() : 0;

					log.info("SocietyPaymentVoucherService. getAllDropdownListbyInvoicedate() - mapListSize: "
							+ mapListSize);
					supplierList = new ArrayList<>();
					orderTypeList = new ArrayList<>();
					loomTypeList = new ArrayList<>();
					circleMasterList = new ArrayList<>();
					districtMasterList = new ArrayList<>();

					if (mapListSize > 0) {
						for (Map<String, Object> data : mapList) {
							SocietyCodeDropDownDTO societyCodeDropDownDTO = new SocietyCodeDropDownDTO();
							Long supplierId = data.get("supplier_id") != null ? Long.valueOf(data.get("supplier_id").toString()) : null;
							String supplierCode = data.get("supplier_code") != null ? data.get("supplier_code").toString() : null;
							String supplierName = data.get("supplier_name") != null ? data.get("supplier_name").toString() : null;
							societyCodeDropDownDTO.setId(supplierId);
							societyCodeDropDownDTO.setCode(supplierCode);
							societyCodeDropDownDTO.setName(supplierName);
							societyCodeDropDownDTO.setDisplayName(
									societyCodeDropDownDTO.getCode() + "/" + societyCodeDropDownDTO.getName());
							supplierList.add(societyCodeDropDownDTO);

							String orderType = data.get("order_type") != null ? data.get("order_type").toString()
									: null;
							if (StringUtils.isNotEmpty(orderType)) {
								orderTypeList.add(orderType);
							}

							String loomType = data.get("loom_type") != null ? data.get("loom_type").toString() : null;
							if (StringUtils.isNotEmpty(loomType)) {
								loomTypeList.add(loomType);
							}

							Long circleId = data.get("circle_id") != null ? Long.valueOf(data.get("circle_id").toString()) : null;
							String circleCode = data.get("circle_code") != null ? data.get("circle_code").toString() : null;
							String circleName = data.get("circle_name") != null ? data.get("circle_name").toString() : null;
							if (circleId != null) {
								CircleMaster circleMaster = new CircleMaster();
								circleMaster.setId(circleId);
								circleMaster.setCode(circleCode);
								circleMaster.setName(circleName);
								circleMasterList.add(circleMaster);
							}

							Long districtId = data.get("district_id") != null ? Long.valueOf(data.get("district_id").toString()) : null;
							String districtCode = data.get("district_code") != null ? data.get("district_code").toString() : null;
							String districtName = data.get("district_name") != null ? data.get("district_name").toString() : null;
							if (districtId != null) {
								DistrictMaster districtMaster = new DistrictMaster();
								districtMaster.setId(districtId);
								districtMaster.setCode(districtCode);
								districtMaster.setName(districtName);
								districtMasterList.add(districtMaster);
							}
						}
					}
				}

				if (!CollectionUtils.isEmpty(orderTypeList)) {
					orderTypeList = orderTypeList.stream().distinct().collect(Collectors.toList());
				}

				if (!CollectionUtils.isEmpty(loomTypeList)) {
					loomTypeList = loomTypeList.stream().distinct().collect(Collectors.toList());
				}

				if (!CollectionUtils.isEmpty(supplierList)) {
					supplierList = supplierList.stream().filter(o -> o.getId() != null).distinct().collect(Collectors.toList());
				}
				
				if (!CollectionUtils.isEmpty(circleMasterList)) {
					circleMasterList = circleMasterList.stream().filter(o -> o.getId() != null).distinct().collect(Collectors.toList());
				}
				
				if (!CollectionUtils.isEmpty(districtMasterList)) {
					districtMasterList = districtMasterList.stream().filter(o -> o.getId() != null).distinct().collect(Collectors.toList());
				}
				
				societyInvoicePaymentDropDownDTO.setSocietyCodeDropDownDTOList(supplierList);
				societyInvoicePaymentDropDownDTO.setOrderTypeList(orderTypeList);
				societyInvoicePaymentDropDownDTO.setLoomTypeList(loomTypeList);
				societyInvoicePaymentDropDownDTO.setCircleMasterList(circleMasterList);
				societyInvoicePaymentDropDownDTO.setDistrictMasterList(districtMasterList);

				if (mapListSize > 0) {
					baseDTO.setResponseContent(societyInvoicePaymentDropDownDTO);
				} else {
					log.error("========getAllDropdownListbyInvoicedate is NULL============ ");
					baseDTO.setStatusCode(ErrorDescription.ERROR_EMPTY_LIST.getCode());
				}
			}

		} catch (Exception exception) {
			log.error("exception Occured ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	private String getCurrentYr() {
		Calendar c = Calendar.getInstance();
		return new SimpleDateFormat("yyyy").format(c.getTime());
	}

}
