package in.gov.cooptex.finance.service;

import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.dto.SocietyCodeDropDownDTO;
import in.gov.cooptex.core.accounts.model.*;
import in.gov.cooptex.core.accounts.repository.StopReleasePaymentRepository;
import in.gov.cooptex.core.accounts.repository.VoucherDetailsRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.PurchaseInvoiceDetailsDTO;
import in.gov.cooptex.core.dto.SocietyStopInvoiceDTO;
import in.gov.cooptex.core.dto.StopSocietyDTO;
import in.gov.cooptex.core.dto.StopSocietyInvoiceDTO;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.CircleMaster;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.InvoiceDetailsRequestDTO;
import in.gov.cooptex.finance.dto.SearchSocietyInvoiceAdjustmentDTO;
import in.gov.cooptex.finance.dto.SocietyPaymentVoucherSearchResponseDTO;
import in.gov.cooptex.finance.repository.SocietyStopInvoiceDetailsRepository;
import in.gov.cooptex.finance.repository.SocietyStopInvoiceRepository;
import in.gov.cooptex.finance.repository.StopReleasePaymentDetailsRepository;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.CircleMasterRepository;
import in.gov.cooptex.core.repository.PurchaseInvoiceAdjustmentRepository;
import in.gov.cooptex.core.repository.PurchaseInvoiceRepository;
import in.gov.cooptex.dto.pos.ReleaseSocietyInvoicesDTO;
import in.gov.cooptex.dto.pos.ReleaseSocietyInvoicesListDTO;
import in.gov.cooptex.dto.pos.SocietyReleaseInvoiceDTO;
import in.gov.cooptex.exceptions.ErrorDescription;
//import in.gov.cooptex.repositories.StopReleasePaymentDetailsRepository;
import in.gov.cooptex.finance.repository.StopReleasePaymentLogRepository;
import in.gov.cooptex.finance.repository.StopReleasePaymentNoteRepository;
import in.gov.cooptex.operation.enums.PurchaseInvoiceStatus;
import in.gov.cooptex.operation.model.SupplierMaster;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityManager;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ReleaseSocietyInvoiceService {

	@Autowired
	ResponseWrapper responseWrapper;

	@Autowired
	EntityManager em;

	@Autowired
	StopReleasePaymentDetailsRepository stopReleasePaymentDetailsRepository;

	@Autowired
	SocietyStopInvoiceService societyStopInvoiceService;

	@Autowired
	CircleMasterRepository circleMasterRepository;

	@Autowired
	PurchaseInvoiceAdjustmentRepository purchaseInvoiceAdjustmentRepository;

	@Autowired
	VoucherDetailsRepository voucherDetailsRepository;

	@Autowired
	SocietyStopInvoiceDetailsRepository societyStopInvoiceDetailsRepository;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	PurchaseInvoiceRepository purchaseInvoiceRepository;

	@Autowired
	SocietyStopInvoiceRepository societyStopInvoiceRepository;

	@Autowired
	StopReleasePaymentLogRepository stopReleasePaymentLogRepository;

	@Autowired
	StopReleasePaymentRepository stopReleasePaymentRepository;

	@Autowired
	StopReleasePaymentNoteRepository stopReleasePaymentNoteRepository;

	@Autowired
	UserMasterRepository userMasterRepository;

	public BaseDTO getReleaseSocietyInvoiceService(PaginationDTO paginationDto) {
		log.info("<==== Starts ReleaseSocietyInvoiceService.getReleaseSocietyInvoiceService =====>");
		BaseDTO response = new BaseDTO();
		try {

			Integer total = 0;
			Integer start = paginationDto.getFirst(), pageSize = paginationDto.getPageSize();
			start = start * pageSize;
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();
			// ApplicationQuery applicationQuery =
			// applicationQueryRepository.findByQueryName("NEW_INVESTMENT_LAZY_LIST_QUERY");

			String mainQuery = "select srp.id as stopReleasePaymentId,srp.supplier_id as supplierMasterId, srpl.stage as stage,"
					+ "srpd.purchase_invoice_id as purchaseInvoiceId,srpd.date_released as dateReleased,"
					+ "concat(pi.invoice_number_prefix,pi.invoice_number) as invoiceNumber,pi.invoice_date as invoiceDate,sm.code as supplierCode,sm.name as supplierName,"
					+ "cm.code as circleCode,cm.name as circleName " + "from stop_release_payment srp "
					+ "join stop_release_payment_details srpd on srp.id=srpd.stop_release_payment_id "
					+ "join (select max(id) as id from stop_release_payment_details group by stop_release_payment_id) srpd1 on srpd1.id=srpd.id "
					+ "join purchase_invoice pi on pi.id=srpd.purchase_invoice_id "
					+ "join supplier_master sm on sm.id=srp.supplier_id "
					+ "join stop_release_payment_log srpl on srpl.stop_release_payment_id=srp.id "
					+ "join (select max(id) as id from stop_release_payment_log  where action_name in ('RELEASED')  group by stop_release_payment_id) srpl1 on srpl.id=srpl1.id "
					+ "join circle_master cm on cm.id=sm.circle_id where 1=1 ";

			if (paginationDto.getFilters() != null) {

				if (paginationDto.getFilters().get("societyCode") != null) {
					mainQuery += " and upper(sm.name) like upper('%" + paginationDto.getFilters().get("societyCode")
							+ "%') ";
				}

				if (paginationDto.getFilters().get("regionCode") != null) {
					mainQuery += " and upper(cm.name) like upper('%" + paginationDto.getFilters().get("regionCode")
							+ "%') ";
				}
				if (paginationDto.getFilters().get("invoiceDate") != null) {
					Date fromDate = new Date((Long) paginationDto.getFilters().get("invoiceDate"));
					mainQuery += " and pi.invoice_date='" + fromDate + "'";
				}
				if (paginationDto.getFilters().get("releasedDate") != null) {
					Date toDate = new Date((Long) paginationDto.getFilters().get("releasedDate"));
					mainQuery += " and srpd.date_released='" + toDate + "'";
				}

				if (paginationDto.getFilters().get("purchaseInvoiceNumber") != null) {
					Double d = Double.valueOf(paginationDto.getFilters().get("purchaseInvoiceNumber").toString());
					mainQuery += " and pi.invoice_number >=" + d;
				}

				if (paginationDto.getFilters().get("stage") != null) {
					mainQuery += " and srpl.stage='" + paginationDto.getFilters().get("stage") + "'";
				}

				mainQuery += "group by srp.id,srp.supplier_id,srpd.purchase_invoice_id,srpd.date_released,concat(pi.invoice_number_prefix,pi.invoice_number),pi.invoice_date,sm.code,cm.code,cm.name,sm.name,srpl.stage";
			}
			String countQuery = "select count(t) from (" + mainQuery + ")t;";
			log.info("count query... " + countQuery);
			countData = jdbcTemplate.queryForList(countQuery);
			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					total = Integer.parseInt(data.get("count").toString().replace(",", ""));
			}
			log.info("total query... " + total);
			if (paginationDto.getSortField() == null)
				mainQuery += " order by srp.id desc limit " + pageSize + " offset " + start + ";";

			if (paginationDto.getSortField() != null && paginationDto.getSortOrder() != null) {

				log.info("Sort Field:[" + paginationDto.getSortField() + "] Sort Order:[" + paginationDto.getSortOrder()
						+ "]");
				if (paginationDto.getSortField().equals("societyCode")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by sm.name asc ";
				if (paginationDto.getSortField().equals("societyCode")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by sm.name desc ";

				if (paginationDto.getSortField().equals("regionCode")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by cm.name asc ";
				if (paginationDto.getSortField().equals("regionCode")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by cm.name desc ";

				if (paginationDto.getSortField().equals("invoiceDate")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by pi.invoice_date asc  ";
				if (paginationDto.getSortField().equals("invoiceDate")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by pi.invoice_date desc  ";

				if (paginationDto.getSortField().equals("purchaseInvoiceNumber")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by pi.invoice_number asc  ";
				if (paginationDto.getSortField().equals("purchaseInvoiceNumber")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by pi.invoice_number desc  ";

				if (paginationDto.getSortField().equals("releasedDate")
						&& paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by srpd.date_released asc ";
				if (paginationDto.getSortField().equals("releasedDate")
						&& paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by srpd.date_released desc ";

				if (paginationDto.getSortField().equals("stage") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by srpl.stage asc ";
				if (paginationDto.getSortField().equals("stage") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by srpl.stage desc ";
				mainQuery += " limit " + pageSize + " offset " + start + ";";
			}
			log.info("Main Qury....." + mainQuery);

			List<SocietyReleaseInvoiceDTO> stopInvoiceList = new ArrayList<>();
			listofData = jdbcTemplate.queryForList(mainQuery);
			for (Map<String, Object> data : listofData) {
				SocietyReleaseInvoiceDTO societyStopInvoiceDTO = new SocietyReleaseInvoiceDTO();
				if (data.get("invoicenumber") != null) {
					societyStopInvoiceDTO.setPurchaseInvoiceNumber(data.get("invoicenumber").toString());
				}
				if (data.get("invoicedate") != null) {
					societyStopInvoiceDTO.setInvoiceDate((Date) data.get("invoicedate"));
				}
				if (data.get("suppliermasterid") != null) {
					societyStopInvoiceDTO.setSocietyCode((data.get("suppliercode").toString()));
					societyStopInvoiceDTO.setSocietyName((data.get("suppliername").toString()));
					societyStopInvoiceDTO.setSupplierId(Long.parseLong(data.get("suppliermasterid").toString()));
				}
				if (data.get("purchaseinvoiceid") != null) {
					societyStopInvoiceDTO.setPurchaseInoiceId(Long.parseLong(data.get("purchaseinvoiceid").toString()));
				}

				if (data.get("stopreleasepaymentid") != null) {
					societyStopInvoiceDTO
							.setStopReleasePaymentId(Long.parseLong(data.get("stopreleasepaymentid").toString()));
				}

				if (data.get("dateReleased") != null) {
					societyStopInvoiceDTO.setReleasedDate((Date) data.get("dateReleased"));
				}

				if (data.get("circlecode") != null) {
					societyStopInvoiceDTO.setRegionCode((data.get("suppliercode").toString()));
					societyStopInvoiceDTO.setRegionName((data.get("circlename").toString()));

				}

				if (data.get("stage") != null) {
					societyStopInvoiceDTO.setStage(data.get("stage").toString());

				}

				stopInvoiceList.add(societyStopInvoiceDTO);
			}
			log.info("Returning loadLazySocietyStopInvoice list...." + stopInvoiceList.size());
			log.info("Total records present in loadLazySocietyStopInvoice..." + total);
			response.setResponseContents(stopInvoiceList);
			response.setTotalRecords(total);
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

			/*
			 * Session session = em.unwrap(Session.class);
			 * 
			 * Criteria criteria = session.createCriteria(StopReleasePayment.class,
			 * "stopReleasePayment");
			 * criteria.createAlias("stopReleasePayment.stopReleasePaymentDetailList",
			 * "stopReleasePaymentDetails");
			 * criteria.createAlias("stopReleasePayment.stopReleasePaymentLogList",
			 * "stopReleasePaymentDetailsLog");
			 * criteria.createAlias("stopReleasePaymentDetails.purchaseInvoice",
			 * "purchaseInvoice"); criteria.createAlias("purchaseInvoice.supplierMaster",
			 * "supplierMaster"); criteria.createAlias("supplierMaster.circleMaster",
			 * "circleMaster");
			 * criteria.add(Restrictions.isNotNull("stopReleasePaymentDetails.dateReleased")
			 * );
			 * 
			 * if (paginationDto.getFilters() != null) { log.info("salesInvoice filters :::"
			 * + paginationDto.getFilters());
			 * 
			 * if (paginationDto.getFilters().get("societyCode") != null &&
			 * !paginationDto.getFilters().get("societyCode").toString().trim().isEmpty()) {
			 * criteria.add(Restrictions .like("supplierMaster.name", "%" +
			 * paginationDto.getFilters().get("societyCode").toString().trim() + "%")
			 * .ignoreCase()); } if (paginationDto.getFilters().get("regionCode") != null &&
			 * !paginationDto.getFilters().get("regionCode").toString().trim().isEmpty()) {
			 * criteria.add( Restrictions .like("circleMaster.name", "%" +
			 * paginationDto.getFilters().get("regionCode").toString() + "%")
			 * .ignoreCase()); }
			 * 
			 * if (paginationDto.getFilters().get("invoiceNumber") != null) { //
			 * criteria.add( Restrictions.eq( "purchaseInvoice.invoiceNumber", //
			 * paginationDto.getFilters().get( "invoiceNumber" ) ) ); String invoicePayment
			 * = paginationDto.getFilters().get("invoiceNumber").toString();
			 * criteria.add(Restrictions.sqlRestriction(
			 * "CAST(purchasein1_.invoice_number AS TEXT) LIKE '%" + invoicePayment.trim() +
			 * "%' ")); }
			 * 
			 * if (paginationDto.getFilters().get("invoiceDate") != null) { Date date = new
			 * Date((long) paginationDto.getFilters().get("invoiceDate")); DateFormat
			 * dateFormat = new SimpleDateFormat("dd-MM-yyyy"); String strDate =
			 * dateFormat.format(date); Date minDate = dateFormat.parse(strDate); Date
			 * maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
			 * criteria.add(Restrictions.conjunction().add(Restrictions.ge(
			 * "purchaseInvoice.invoiceDate", minDate))
			 * .add(Restrictions.lt("purchaseInvoice.invoiceDate", maxDate))); } if
			 * (paginationDto.getFilters().get("releasedDate") != null) { Date date = new
			 * Date((long) paginationDto.getFilters().get("releasedDate")); DateFormat
			 * dateFormat = new SimpleDateFormat("dd-MM-yyyy"); String strDate =
			 * dateFormat.format(date); Date minDate = dateFormat.parse(strDate); Date
			 * maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
			 * criteria.add(Restrictions.conjunction()
			 * .add(Restrictions.ge("stopReleasePaymentDetails.dateReleased", minDate))
			 * .add(Restrictions.lt("stopReleasePaymentDetails.dateReleased", maxDate))); }
			 * 
			 * criteria.setProjection(Projections.rowCount()); Long totalResult = (long)
			 * ((Long) criteria.uniqueResult()).intValue(); criteria.setProjection(null);
			 * 
			 * ProjectionList projectionList = Projections.projectionList();
			 * projectionList.add(Projections.property("purchaseInvoice.invoiceNumber"));
			 * projectionList.add(Projections.property("purchaseInvoice.invoiceDate"));
			 * projectionList.add(Projections.property("supplierMaster.code"));
			 * projectionList.add(Projections.property("circleMaster.code"));
			 * projectionList.add(Projections.property("supplierMaster.name"));
			 * projectionList.add(Projections.property("circleMaster.name"));
			 * projectionList.add(Projections.property("supplierMaster.id"));
			 * projectionList.add(Projections.property("purchaseInvoice.id"));
			 * projectionList.add(Projections.property("stopReleasePaymentDetails.id"));
			 * projectionList.add(Projections.property(
			 * "stopReleasePaymentDetails.dateReleased"));
			 * projectionList.add(Projections.property("stopReleasePayment.id"));
			 * projectionList.add(Projections.property("stopReleasePaymentDetailsLog.stage")
			 * );
			 * 
			 * criteria.setProjection(projectionList);
			 * 
			 * if (paginationDto.getFirst() != null) { Integer pageNo =
			 * paginationDto.getFirst(); Integer pageSize = paginationDto.getPageSize(); if
			 * (pageNo != null && pageSize != null) { criteria.setFirstResult(pageNo *
			 * pageSize); criteria.setMaxResults(pageSize); log.info("PageNo : [" + pageNo +
			 * "] pageSize[" + pageSize + "]"); }
			 * 
			 * String sortField = paginationDto.getSortField(); String sortOrder =
			 * paginationDto.getSortOrder(); log.info("sortField outside : [" + sortField +
			 * "] sortOrder[" + sortOrder + "]"); if (paginationDto.getSortField() != null
			 * && paginationDto.getSortOrder() != null) { log.info("sortField : [" +
			 * paginationDto.getSortField() + "] sortOrder[" + paginationDto.getSortOrder()
			 * + "]");
			 * 
			 * if (paginationDto.getSortField().equals("societyCode")) { sortField =
			 * "supplierMaster.code"; } else if (sortField.equals("regionCode")) { sortField
			 * = "circleMaster.code"; } else if (sortField.equals("invoiceNumber")) {
			 * sortField = "purchaseInvoice.invoiceNumber"; } else if
			 * (sortField.equals("invoiceDate")) { sortField =
			 * "purchaseInvoice.invoiceDate"; } else if (sortField.equals("releasedDate")) {
			 * sortField = "stopReleasePaymentDetails.dateReleased"; } if
			 * (sortOrder.equals("DESCENDING")) { criteria.addOrder(Order.desc(sortField));
			 * } else { criteria.addOrder(Order.asc(sortField)); } } else {
			 * criteria.addOrder(Order.desc("stopReleasePaymentDetails.id")); } }
			 * 
			 * Long stopReleasePaymentId = null;
			 * 
			 * List<?> resultList = criteria.list(); if (resultList == null ||
			 * resultList.isEmpty() || resultList.size() == 0) {
			 * log.info("<==== Release purchase invoice is null =====>"); }
			 * List<SocietyReleaseInvoiceDTO> salesInvoiceList = new ArrayList<>();
			 * 
			 * Iterator<?> it = resultList.iterator();
			 * 
			 * while (it.hasNext()) { Object obj[] = (Object[]) it.next();
			 * StopReleasePayment salesInvoice = new StopReleasePayment();
			 * SocietyReleaseInvoiceDTO societyReleaseInvoiceDTO = new
			 * SocietyReleaseInvoiceDTO();
			 * societyReleaseInvoiceDTO.setInvoiceNumber((Integer) obj[0]);
			 * societyReleaseInvoiceDTO.setInvoiceDate((Date) obj[1]);
			 * societyReleaseInvoiceDTO.setSocietyCode((String) obj[2]);
			 * societyReleaseInvoiceDTO.setRegionCode((String) obj[3]);
			 * societyReleaseInvoiceDTO.setSocietyName((String) obj[4]);
			 * societyReleaseInvoiceDTO.setRegionName((String) obj[5]);
			 * societyReleaseInvoiceDTO.setSupplierId((Long) obj[6]);
			 * societyReleaseInvoiceDTO.setPurchaseInoiceId((Long) obj[7]); //
			 * societyReleaseInvoiceDTO.setStopReleasePaymentId( (Long) obj[8] );
			 * societyReleaseInvoiceDTO.setStopReleasePaymentDetailId((Long) obj[8]);
			 * societyReleaseInvoiceDTO.setReleasedDate((Date) obj[9]);
			 * societyReleaseInvoiceDTO.setStopReleasePaymentId((Long) obj[10]);
			 * societyReleaseInvoiceDTO.setStage((String) obj[11]);
			 * 
			 * 
			 * for (SocietyReleaseInvoiceDTO societyReleaseInvoiceDto : salesInvoiceList) {
			 * stopReleasePaymentId = societyReleaseInvoiceDto.getStopReleasePaymentId(); }
			 * 
			 * 
			 * 
			 * if (!societyReleaseInvoiceDTO.getStopReleasePaymentId().equals(
			 * stopReleasePaymentId)) {
			 * 
			 * }
			 * 
			 * salesInvoiceList.add(societyReleaseInvoiceDTO);
			 * 
			 * } response.setResponseContents(salesInvoiceList);
			 * response.setTotalRecords(totalResult.intValue());
			 * response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode()); }
			 */} catch (Exception e) {
			log.error("<==== Exception occured  =====>" + e);
			response.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<==== Ended ReleaseSocietyInvoiceService.getReleaseSocietyInvoiceService =====>");
		return response;
	}

	// @Transactional(rollbackFor = Exception.class)
	public BaseDTO createReleaseStopInvoice(SocietyStopInvoiceDTO societyStopInvoiceDTO) {
		log.info("<==== Starts ReleaseSocietyInvoiceService.createReleaseStopInvoice =====>");
		BaseDTO baseDTO = new BaseDTO();
		Set<Long> set = new HashSet<Long>();
		List<StopReleasePaymentDetails> stopReleasePaymentDetailsList = null;
		PurchaseInvoice purchaseInvoice = null;
		try {
			Validate.listNotNullOrEmpty(societyStopInvoiceDTO.getListOfPurchaseInvoices(),
					ErrorDescription.ERROR_EMPTY_LIST);

			Validate.notNull(societyStopInvoiceDTO.getNote(), ErrorDescription.NOTE_IS_EMPTY);

			for (PurchaseInvoiceDetailsDTO purchaseInvoiceDetailsDTO : societyStopInvoiceDTO
					.getListOfPurchaseInvoices()) {
				purchaseInvoice = new PurchaseInvoice();
				stopReleasePaymentDetailsList = stopReleasePaymentDetailsRepository
						.findByPurchaseInvoiceId(purchaseInvoiceDetailsDTO.getInvoiceId());
				if (stopReleasePaymentDetailsList == null) {
					log.info("stopReleasePaymentDetailsList is empty for purchase invoice id "
							+ purchaseInvoiceDetailsDTO.getInvoiceId());
					throw new RestException(ErrorDescription.PURCHASE_INVOICE_EMPTY);
				}

				purchaseInvoice = purchaseInvoiceRepository.findOne(purchaseInvoiceDetailsDTO.getInvoiceId());

				for (StopReleasePaymentDetails stopReleasePaymentDetails : stopReleasePaymentDetailsList) {
					stopReleasePaymentDetails.setDateReleased(new Date());
					stopReleasePaymentDetailsRepository.save(stopReleasePaymentDetails);
					set.add(stopReleasePaymentDetails.getStopReleasePayment().getId());
				}
				updatePurchaseInvoiceStoppedInvoiceStatus(purchaseInvoice.getId());
			}
			if (set.size() > 0) {
				for (Long stopReleasePaymentId : set) {
					StopReleasePayment stopReleasePayment = societyStopInvoiceRepository.findOne(stopReleasePaymentId);
					StopReleasePaymentLog stopReleasePaymentLog = new StopReleasePaymentLog();
					stopReleasePaymentLog.setActionName(PurchaseInvoiceStatus.RELEASED.name());
					stopReleasePaymentLog.setStage(PurchaseInvoiceStatus.SUBMITTED.name());
					stopReleasePaymentLog.setStopReleasePayment(stopReleasePayment);
					stopReleasePaymentLogRepository.save(stopReleasePaymentLog);

					// Note section
					StopReleasePaymentNote note = new StopReleasePaymentNote();
					note.setStopReleasePayment(stopReleasePayment);
					note.setForwardTo(userMasterRepository.findOne(societyStopInvoiceDTO.getForwardToUser()));
					note.setFinalApproval(societyStopInvoiceDTO.getIsFinalApprove());
					note.setNote(societyStopInvoiceDTO.getNote());
					note.setActionName(PurchaseInvoiceStatus.RELEASED.name());
					stopReleasePaymentNoteRepository.save(note);
				}

			}
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("<==== Ended ReleaseSocietyInvoiceService.createReleaseStopInvoice =====>");
		} catch (RestException restException) {
			log.error("Exception occured ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("Exception occured ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}

		return responseWrapper.send(baseDTO);
	}

	private void updatePurchaseInvoiceStoppedInvoiceStatus(Long puchase_invoice_id) {
		jdbcTemplate.update("UPDATE purchase_invoice SET status='" + PurchaseInvoiceStatus.RELEASED + "' WHERE id IN("
				+ puchase_invoice_id + ");");
	}

	public BaseDTO searchPurchaseInvoiceDetails(Long societyId) {
		log.info("<--Starts SocietyStopInvoiceService .searchPurchaseInvoiceDetails-->" + societyId);
		BaseDTO baseDTO = new BaseDTO();
		Set<Long> set = new HashSet<Long>();
		List<PurchaseInvoice> listOfPurchaseInvoice = null;
		List<PurchaseInvoice> listOfPurchaseInvoices = null;

		Validate.notNull(societyId, ErrorDescription.SOCIETY_IS_EMPTY);

		try {
			listOfPurchaseInvoice = purchaseInvoiceRepository.purchaseInvoiceListBySupplierId(societyId);
			if (listOfPurchaseInvoice == null) {
				log.info("Purchase invoice list id empty for purchaseInvoiceId:" + societyId);
				throw new RestException(ErrorDescription.ERROR_EMPTY_LIST);
			}
			for (PurchaseInvoice purchaseInvoice : listOfPurchaseInvoice) {
				set.add(purchaseInvoice.getId());
			}
			if (set.size() > 0) {
				for (Long purchaseInvoiceId : set) {
					PurchaseInvoice purchaseInvoice = purchaseInvoiceRepository.findOne(purchaseInvoiceId);
					listOfPurchaseInvoices.add(purchaseInvoice);
				}
			}
			baseDTO.setResponseContents(societyStopInvoiceService.getAllPurchaseInvoiceDetails(listOfPurchaseInvoices));
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("<--Ended SocietyStopInvoiceService .searchPurchaseInvoiceDetails-->");
		} catch (RestException restException) {
			log.error("exception Occured ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("exception Occured ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public List<PurchaseInvoiceDetailsDTO> getAllPurchaseInvoiceDetailsNew(List<Integer> listOfPurchaseInvoicesIds) {
		log.info("<--Starts SocietyStopInvoiceService .getAllPurchaseInvoiceDetailsNew-->");
		List<Map<String, Object>> purchaseInvoiceMapList = new ArrayList<Map<String, Object>>();
		String query = null;
		List<PurchaseInvoiceDetailsDTO> purchaseInvoiceDetailsDtoList = new ArrayList<>();
		ApplicationQuery applicationQuery = null;
		try {

			/*
			 * String purchaseInvoicesId = ""; for (Integer invoiceId :
			 * listOfPurchaseInvoicesIds) { purchaseInvoicesId = invoiceId.toString() + ",";
			 * }
			 */
			applicationQuery = applicationQueryRepository.findByQuery("SOCIETY_RELEASE_INVOICE_DETAILS");
			query = applicationQuery.getQueryContent();

			/*
			 * Long[] idsArray = new Long[listOfPurchaseInvoicesIds.size()]; idsArray =
			 * listOfPurchaseInvoicesIds.toArray(idsArray);
			 */
			String purchaseInvoicesIds = StringUtils.join(listOfPurchaseInvoicesIds, ",");

			if (purchaseInvoicesIds != null && !purchaseInvoicesIds.trim().isEmpty()) {
				log.info("getAllPurchaseInvoiceDetailsNew inside purchaseInvoiceId condition---------");
				query = query.concat(" and pi.id in (" + purchaseInvoicesIds + ")");
			}
			query = query + " order by sm.code,pi.invoice_number";
			log.info("      over all Query PurchaseInvoice query----------" + query);
			purchaseInvoiceMapList = jdbcTemplate.queryForList(query);
			int purchaseInvoiceMapListSize = purchaseInvoiceMapList != null ? purchaseInvoiceMapList.size() : 0;
			log.info("purchaseInvoiceMapList size------" + purchaseInvoiceMapListSize);

			if (!CollectionUtils.isEmpty(purchaseInvoiceMapList)) {
				Map<Object, List<Map<String, Object>>> purchaseInvoiceDetailsBySocietyMap = purchaseInvoiceMapList
						.stream().filter(o -> o.get("supplier_id") != null)
						.collect(Collectors.groupingBy(r -> r.get("supplier_id")));
				if (!CollectionUtils.isEmpty(purchaseInvoiceDetailsBySocietyMap)) {
					for (Map.Entry<Object, List<Map<String, Object>>> entry : purchaseInvoiceDetailsBySocietyMap
							.entrySet()) {

						List<Map<String, Object>> valuesList = entry.getValue();

						if (!CollectionUtils.isEmpty(valuesList)) {
							for (Map<String, Object> dataMap : valuesList) {
								PurchaseInvoiceDetailsDTO purchaseInvoiceDetailsDTO = new PurchaseInvoiceDetailsDTO();
								Long purchaseInvoiceId = dataMap.get("id") != null
										? Long.valueOf(dataMap.get("id").toString())
										: null;
								purchaseInvoiceDetailsDTO
										.setAmountWithountTax(getDouble(dataMap, "amount_without_tax"));
								purchaseInvoiceDetailsDTO.setTax(getDouble(dataMap, "tax_value"));
								purchaseInvoiceDetailsDTO.setTotalAdjustedAmount(purchaseInvoiceAdjustmentRepository
										.getTotalAdjustedAmountByInvoiceId(purchaseInvoiceId));
								purchaseInvoiceDetailsDTO.setTotalAmountPaid(
										voucherDetailsRepository.getTotalAmountByInvoiceId(purchaseInvoiceId));
								purchaseInvoiceDetailsDTO.setSocietyId(getLong(dataMap, "supplier_id"));
								purchaseInvoiceDetailsDTO.setInvoiceId(getLong(dataMap, "id"));
								purchaseInvoiceDetailsDTO
										.setInvoiceNumberPrefix(getString(dataMap, "invoice_number_prefix"));
								purchaseInvoiceDetailsDTO.setInvoiceNumber(dataMap.get("invoice_number") != null
										? Integer.valueOf(dataMap.get("invoice_number").toString())
										: null);
								purchaseInvoiceDetailsDTO.setInvoiceDate(
										dataMap.get("invoice_date") != null ? (Date) (dataMap.get("invoice_date"))
												: null);
								purchaseInvoiceDetailsDTO
										.setTotalPaybleAmount(purchaseInvoiceDetailsDTO.getAmountWithountTax()
												+ purchaseInvoiceDetailsDTO.getTax());
								purchaseInvoiceDetailsDTO
										.setBalanceAmount(purchaseInvoiceDetailsDTO.getTotalPaybleAmount()
												- (purchaseInvoiceDetailsDTO.getTotalAdjustedAmount()
														+ purchaseInvoiceDetailsDTO.getTotalAmountPaid()));

								purchaseInvoiceDetailsDTO
										.setTotalInvoiceAmountToBePaid(purchaseInvoiceDetailsDTO.getBalanceAmount());
								if (purchaseInvoiceDetailsDTO.getTotalInvoiceAmountToBePaid() > 0) {
									purchaseInvoiceDetailsDtoList.add(purchaseInvoiceDetailsDTO);
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			log.info("SocietyStopInvoiceService getAllPurchaseInvoiceDetailsNew exception", e);
		}
		log.info("<--Ends SocietyStopInvoiceService .getAllPurchaseInvoiceDetailsNew-->");
		return purchaseInvoiceDetailsDtoList;
	}

	public BaseDTO viewReleasePurchaseInvoiceDetails(SocietyReleaseInvoiceDTO societyReleaseInvoiceDTO) {
		log.info("<--Starts SocietyStopInvoiceService .searchPurchaseInvoiceDetails-->" + societyReleaseInvoiceDTO);
		BaseDTO baseDTO = new BaseDTO();
		Set<Long> set = new HashSet<Long>();
		List<PurchaseInvoiceDetailsDTO> listOfPurchaseInvoiceDTO = null;
		StopSocietyDTO stopSocietyDTO = new StopSocietyDTO();
		try {
			StopReleasePayment stopReleasePayment = new StopReleasePayment();
			Validate.notNull(societyReleaseInvoiceDTO.getPurchaseInoiceId(), ErrorDescription.PURCHASE_INVOICE_EMPTY);

			List<Integer> purchaseInvoiceIdList = societyStopInvoiceDetailsRepository
					.getpurchaseInvoiceIdlistbystopreleasepaymentID(societyReleaseInvoiceDTO.getStopReleasePaymentId());
			if (purchaseInvoiceIdList == null || purchaseInvoiceIdList.isEmpty()) {
				log.info("Purchase Invoice Not found for Purchase invoice id:"
						+ societyReleaseInvoiceDTO.getPurchaseInoiceId());
				throw new RestException(ErrorDescription.PURCHASE_INVOICE_EMPTY);
			}
//			listOfPurchaseInvoiceDTO = getAllPurchaseInvoiceDetails(listOfPurchaseInvoice);
			listOfPurchaseInvoiceDTO = getAllPurchaseInvoiceDetailsNew(purchaseInvoiceIdList);
			if (listOfPurchaseInvoiceDTO.size() > 0) {
				log.info("getStopReleasePaymentDetailId()==>"
						+ societyReleaseInvoiceDTO.getStopReleasePaymentDetailId());
				log.info("getStopReleasePaymentId()==>" + societyReleaseInvoiceDTO.getStopReleasePaymentId());
				/*
				 * StopReleasePaymentDetails stopReleasePaymentDetails1 =
				 * stopReleasePaymentDetailsRepository
				 * .findOne(societyReleaseInvoiceDTO.getStopReleasePaymentDetailId());
				 */
				// if (stopReleasePaymentDetails != null) {
				stopReleasePayment = societyStopInvoiceRepository
						.findOne(societyReleaseInvoiceDTO.getStopReleasePaymentId());
				listOfPurchaseInvoiceDTO.get(0).setStopSocityId(societyReleaseInvoiceDTO.getStopReleasePaymentId());
				listOfPurchaseInvoiceDTO.get(0)
						.setStopSocityDetailId(societyReleaseInvoiceDTO.getStopReleasePaymentDetailId());
				listOfPurchaseInvoiceDTO.get(0).setInvoiceFromDate(stopReleasePayment.getFromDate());
				listOfPurchaseInvoiceDTO.get(0).setInvoiceToDate(stopReleasePayment.getToDate());
				// }

			}
			StopReleasePaymentNote stopReleasePaymentNote = stopReleasePaymentNoteRepository
					.findReleasePaymentNoteByReleaseId(societyReleaseInvoiceDTO.getStopReleasePaymentId());
			List<StopReleasePaymentLog> stopReleasePaymentLogList = stopReleasePaymentLogRepository
					.findReleasePaymentLogByReleaseId(societyReleaseInvoiceDTO.getStopReleasePaymentId());
			StopReleasePaymentLog stopReleasePaymentLog = new StopReleasePaymentLog();
			if (stopReleasePaymentLogList != null && !stopReleasePaymentLogList.isEmpty()) {
				log.info("stopReleasePaymentLogList size==> " + stopReleasePaymentLogList.size());
				stopReleasePaymentLog = stopReleasePaymentLogList.get(stopReleasePaymentLogList.size() - 1);
			} else {
				log.error("StopReleasePaymentLog not found");
			}

			stopSocietyDTO.setStopReleasePaymentLog(stopReleasePaymentLog);
			stopReleasePaymentNote.setStopReleasePayment(null);
			stopSocietyDTO.setStopReleasePaymentNote(stopReleasePaymentNote);

			baseDTO.setResponseContents(listOfPurchaseInvoiceDTO);
			baseDTO.setResponseContent(stopSocietyDTO);

			List<Map<String, Object>> employeeData = new ArrayList<Map<String, Object>>();
			if (societyReleaseInvoiceDTO.getStopReleasePaymentId() != null) {
				log.info("<<<:::::::voucher::::not Null::::>>>>" + societyReleaseInvoiceDTO != null
						? societyReleaseInvoiceDTO.getStopReleasePaymentId().toString()
						: "Null");
				ApplicationQuery applicationQueryForlog = applicationQueryRepository
						.findByQueryName("SOCIETY_STOP_INVOICE_LOG_EMPLOYEE_DETAILS");
				if (applicationQueryForlog == null || applicationQueryForlog.getId() == null) {
					log.info("Application Query For Log Details not found for query name : " + applicationQueryForlog);
					baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
					return baseDTO;
				}
				String logquery = applicationQueryForlog.getQueryContent().trim();
				log.info("<=========SOCIETY_STOP_INVOICE_LOG_EMPLOYEE_DETAILS Query Content ======>" + logquery);
				logquery = logquery.replace(":stopreleasepaymentId",
						"'" + societyReleaseInvoiceDTO.getStopReleasePaymentId().toString() + "'");
				log.info(
						"Query Content For SOCIETY_STOP_INVOICE_LOG_EMPLOYEE_DETAILS After replaced plan id View query : "
								+ logquery);
				employeeData = jdbcTemplate.queryForList(logquery);
				log.info("<=========SOCIETY_STOP_INVOICE_LOG_EMPLOYEE_DETAILS Employee Data======>" + employeeData);
				baseDTO.setTotalListOfData(employeeData);
			}

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("<--Ended SocietyStopInvoiceService .searchPurchaseInvoiceDetails-->");
		} catch (RestException restException) {
			log.error("exception Occured ", restException);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		} catch (Exception exception) {
			log.error("exception Occured ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO getCircleDetailsbyDate(SearchSocietyInvoiceAdjustmentDTO SSIVA) {
		log.info("==============================SocietyPaymentVoucherService========================= : ");
		log.info("<--districIds ID : " + SSIVA.getSocietyIds().toString());
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<Long> circleIdList = new ArrayList<Long>();
			List<CircleMaster> circleMasterList = new ArrayList<CircleMaster>();
			log.info("==============================District Code ========================= : "
					+ SSIVA.getSocietyIds().toString());
			if (SSIVA.getSocietyIds().isEmpty()) {
				circleMasterList = circleMasterRepository.findStopReleaseCircleMastersbyDate(SSIVA.getInvoiceFromDate(),
						SSIVA.getInvoiceToDate());
				log.info("==============================Size 0========================= : ");
			} else {
				log.info("==============================Not Empty1========================= : ");
				circleMasterList = circleMasterRepository.findAllActiveCircleMastersbyDistrictCode(
						SSIVA.getInvoiceFromDate(), SSIVA.getInvoiceToDate(), SSIVA.getSocietyIds());
				log.info("==============================Not Empty========================= : ");
			}
			baseDTO.setResponseContents(circleMasterList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception ex) {
			log.error("exception Occured ", ex);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<--End SocietyPaymentVoucherService .getCircleMasterbyDistrictIds-->");
		return baseDTO;
	}

	private static final String DECIMAL_FORMAT = "#0.00";

	private Double getDouble(Map<String, Object> map, String key) {
		try {
			Object obj = map.get(key);
			if (obj != null) {
				String valueStr = String.valueOf(obj);
				if (!org.apache.commons.lang3.StringUtils.isEmpty(valueStr)) {
					DecimalFormat decimalFormat = new DecimalFormat(DECIMAL_FORMAT);
					Double value = decimalFormat.parse(decimalFormat.format(Double.valueOf(valueStr))).doubleValue();
					return value;
				}
			}
		} catch (Exception ex) {
			log.error("Exception at getDouble()", ex);
		}
		return null;
	}

	private Long getLong(Map<String, Object> map, String key) {
		try {
			Object obj = map.get(key);
			if (obj != null) {
				return Long.valueOf(String.valueOf(obj));
			}
		} catch (Exception ex) {
			log.error("Exception at getLong()", ex);
		}
		return null;
	}

	private String getString(Map<String, Object> map, String key) {
		try {
			Object obj = map.get(key);
			if (obj != null) {
				return String.valueOf(obj);
			}
		} catch (Exception ex) {
			log.error("Exception at getString()", ex);
		}
		return null;
	}

	public BaseDTO searchPurchaseInvoiceDetails(InvoiceDetailsRequestDTO invoiceDetailsRequestDTO) {
		log.info("<--Starts SocietyStopInvoiceService .getAllPendingPurchaseInvoices-->");
		BaseDTO baseDTO = new BaseDTO();
		List<PurchaseInvoiceDetailsDTO> purchaseInvoiceList = null;
		// List<SocietyPaymentVoucherSearchResponseDTO>
		// societyPaymentVoucherSearchResponseDTOList = null;
		List<PurchaseInvoiceDetailsDTO> societyPaymentVoucherSearchResponseDTOList = null;
		List<Map<String, Object>> purchaseInvoiceMapList = new ArrayList<Map<String, Object>>();
		String query = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Map<Long, SocietyPaymentVoucherSearchResponseDTO> responseDTOMap = new HashMap<>();
		ApplicationQuery applicationQuery = null;
		try {
			societyPaymentVoucherSearchResponseDTOList = new ArrayList<PurchaseInvoiceDetailsDTO>();
			purchaseInvoiceList = new ArrayList<PurchaseInvoiceDetailsDTO>();

			log.info("getAllPendingPurchaseInvoices inside productPlan null condtion---------");

			applicationQuery = applicationQueryRepository.findByQuery("RELEASE_INVOICE");
			query = applicationQuery.getQueryContent();

			if (invoiceDetailsRequestDTO.getStartDate() != null && invoiceDetailsRequestDTO.getEndDate() != null) {
				query = query.replaceAll(":fromDate", dateFormat.format(invoiceDetailsRequestDTO.getStartDate()));
				query = query.replaceAll(":toDate", dateFormat.format(invoiceDetailsRequestDTO.getEndDate()));
			} else {
				query = query.replaceAll("pi.acknowledged_date between  ':fromDate'  and ':toDate'", "1=1");
			}

			// query = query.replaceAll(":APPROVED",
			// PurchaseInvoiceStatus.APPROVED.toString());
			// query = query.replaceAll(":PARTIALLY_PAID",
			// PurchaseInvoiceStatus.PARTIALLY_PAID.toString());

			if (invoiceDetailsRequestDTO.getSupplierId() != null && invoiceDetailsRequestDTO.getSupplierId() > 0) {
				log.info("getAllPendingPurchaseInvoices inside supplier condtion---------");
				query = query.concat(" and pi.supplier_id in (" + invoiceDetailsRequestDTO.getSupplierId() + ")");
			}
			//query = query + " group by sm.id,sm.code,sm.name,pi.id order by sm.code,pi.invoice_number";
			query = query + " order by sm.code,pi.invoice_number";
			log.info("      over all Query PurchaseInvoice query----------" + query);
			purchaseInvoiceMapList = jdbcTemplate.queryForList(query);
			int purchaseInvoiceMapListSize = purchaseInvoiceMapList != null ? purchaseInvoiceMapList.size() : 0;
			log.info("purchaseInvoiceMapList size------" + purchaseInvoiceMapListSize);
			List<PurchaseInvoiceDetailsDTO> purchaseInvoiceDetailsDtoList = new ArrayList<>();
			if (!CollectionUtils.isEmpty(purchaseInvoiceMapList)) {
				Map<Object, List<Map<String, Object>>> purchaseInvoiceDetailsBySocietyMap = purchaseInvoiceMapList
						.stream().filter(o -> o.get("supplier_id") != null)
						.collect(Collectors.groupingBy(r -> r.get("supplier_id")));
				if (!CollectionUtils.isEmpty(purchaseInvoiceDetailsBySocietyMap)) {
					for (Map.Entry<Object, List<Map<String, Object>>> entry : purchaseInvoiceDetailsBySocietyMap
							.entrySet()) {

						List<Map<String, Object>> valuesList = entry.getValue();

						if (!CollectionUtils.isEmpty(valuesList)) {
							// PurchaseInvoiceDetailsDTO societyPaymentVoucherSearchResponseDTO = new
							// PurchaseInvoiceDetailsDTO();
							Map<String, Object> societyValueMap = valuesList.get(0);

							for (Map<String, Object> dataMap : valuesList) {
								PurchaseInvoiceDetailsDTO purchaseInvoiceDetailsDTO = new PurchaseInvoiceDetailsDTO();
								Long purchaseInvoiceId = dataMap.get("id") != null
										? Long.valueOf(dataMap.get("id").toString())
										: null;
								purchaseInvoiceDetailsDTO
										.setAmountWithountTax(getDouble(dataMap, "amount_without_tax"));
								purchaseInvoiceDetailsDTO.setTax(getDouble(dataMap, "tax_value"));
								purchaseInvoiceDetailsDTO.setTotalAdjustedAmount(purchaseInvoiceAdjustmentRepository
										.getTotalAdjustedAmountByInvoiceId(purchaseInvoiceId));
								purchaseInvoiceDetailsDTO.setTotalAmountPaid(
										voucherDetailsRepository.getTotalAmountByInvoiceId(purchaseInvoiceId));
								purchaseInvoiceDetailsDTO.setSocietyId(getLong(dataMap, "supplier_id"));
								purchaseInvoiceDetailsDTO.setInvoiceId(getLong(dataMap, "id"));
								purchaseInvoiceDetailsDTO
										.setInvoiceNumberPrefix(getString(dataMap, "invoice_number_prefix"));
								purchaseInvoiceDetailsDTO.setInvoiceNumber(dataMap.get("invoice_number") != null
										? Integer.valueOf(dataMap.get("invoice_number").toString())
										: null);
								purchaseInvoiceDetailsDTO.setInvoiceDate(
										dataMap.get("invoice_date") != null ? (Date) (dataMap.get("invoice_date"))
												: null);
								purchaseInvoiceDetailsDTO
										.setTotalPaybleAmount(purchaseInvoiceDetailsDTO.getAmountWithountTax()
												+ purchaseInvoiceDetailsDTO.getTax());
								purchaseInvoiceDetailsDTO
										.setBalanceAmount(purchaseInvoiceDetailsDTO.getTotalPaybleAmount()
												- (purchaseInvoiceDetailsDTO.getTotalAdjustedAmount()
														+ purchaseInvoiceDetailsDTO.getTotalAmountPaid()));

								purchaseInvoiceDetailsDTO
										.setTotalInvoiceAmountToBePaid(purchaseInvoiceDetailsDTO.getBalanceAmount());
								if (purchaseInvoiceDetailsDTO.getTotalInvoiceAmountToBePaid() > 0) {
									purchaseInvoiceDetailsDtoList.add(purchaseInvoiceDetailsDTO);
								}
							}

						}
					}
				}
			}
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			baseDTO.setResponseContents(purchaseInvoiceDetailsDtoList);
		} catch (Exception e) {
			log.info("SocietyStopInvoiceService getAllPendingPurchaseInvoices exception", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("<--Ends SocietyStopInvoiceService .getAllPendingPurchaseInvoices-->");
		return baseDTO;
	}

	public BaseDTO getSocietyByStopInvoice(SearchSocietyInvoiceAdjustmentDTO searchSocietyInvoiceAdjustmentDTO) {
		log.info("<--=============Starts SocietyInvoiceAdjustmentService .getSocietyDropdown==============-->");
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("<--=============Starts SocietyInvoiceAdjustmentService Step14==============--> "
					+ searchSocietyInvoiceAdjustmentDTO.getProductVarietyIds());

			String fdate = new SimpleDateFormat("yyyy-MM-dd")
					.format(searchSocietyInvoiceAdjustmentDTO.getInvoiceFromDate());
			String tdate = new SimpleDateFormat("yyyy-MM-dd")
					.format(searchSocietyInvoiceAdjustmentDTO.getInvoiceToDate());
			List<Object[]> supplierMasterList = new ArrayList<Object[]>();
			log.info("<--=============Fdate : ==============--> " + fdate
					+ "=========================== Tdate ===============" + tdate);
			ApplicationQuery appuery;
			// String qName="";
			appuery = applicationQueryRepository.findByQueryName("SOCIETY_CODE_DROPDOWN_FOR_RELEASE_INVOICE");
			if (appuery != null) {
				String queryContent = appuery.getQueryContent();
				log.info("----------------------Query--------------------" + queryContent);

				if (queryContent != null && !queryContent.isEmpty()) {

					queryContent = queryContent.replace(":fdate", "'" + fdate + "'");
					queryContent = queryContent.replace(":tdate", "'" + tdate + "'");

					if (searchSocietyInvoiceAdjustmentDTO.getProductVarietyIds() != null
							&& !searchSocietyInvoiceAdjustmentDTO.getProductVarietyIds().isEmpty()) {
						log.info("----------------------DIST--------------------");
						queryContent = queryContent + " and dm.id in ("
								+ AppUtil.commaSeparatedIds(searchSocietyInvoiceAdjustmentDTO.getProductVarietyIds())
								+ ")";
					}
					if (searchSocietyInvoiceAdjustmentDTO.getSocietyIds() != null
							&& !searchSocietyInvoiceAdjustmentDTO.getSocietyIds().isEmpty()) {
						log.info("----------------------CIRCLE--------------------");
						queryContent = queryContent + " and sm.circle_id in ("
								+ AppUtil.commaSeparatedIds(searchSocietyInvoiceAdjustmentDTO.getSocietyIds()) + ")";
					}
					if (searchSocietyInvoiceAdjustmentDTO.getLoomTypeIds() != null
							&& !searchSocietyInvoiceAdjustmentDTO.getLoomTypeIds().isEmpty()) {
						log.info("----------------------LOOM--------------------");
						String lpt = searchSocietyInvoiceAdjustmentDTO.getLoomTypeIds().toString();
						lpt = lpt.substring(1, lpt.length() - 1);
						lpt = "'" + lpt.replace(" ", "'") + "'";
						lpt = lpt.replace(",", "',");
						log.info("----------------------LType--------------------");
						queryContent = queryContent + " and sm.loom_type in (" + lpt + ")";
					}
					log.info("----------------------Query1--------------------" + queryContent);
					queryContent = queryContent + " group by sm.id";
					List<Map<String, Object>> list = jdbcTemplate.queryForList(queryContent);
					log.info("----------------------DIST-------------------- : " + list.size());
					List<SocietyCodeDropDownDTO> societyCodeDropDownDTOSList = new ArrayList<>();
					for (Map<String, Object> data : list) {
						SocietyCodeDropDownDTO societyCodeDropDownDTO = new SocietyCodeDropDownDTO();
						Long id = Long.valueOf(data.get("id").toString());
						String code = data.get("code").toString();
						String name = data.get("name").toString();

						log.info("<--=============Column1==============--> " + id);
						log.info("<--=============Column2==============--> " + code);
						log.info("<--=============Column3==============--> " + name);
						societyCodeDropDownDTO.setId(id);
						societyCodeDropDownDTO.setCode(code);
						societyCodeDropDownDTO.setName(name);
						societyCodeDropDownDTO.setDisplayName(
								societyCodeDropDownDTO.getCode() + "/" + societyCodeDropDownDTO.getName());

						societyCodeDropDownDTOSList.add(societyCodeDropDownDTO);
					}
					baseDTO.setResponseContents(societyCodeDropDownDTOSList);
					baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				}
			}

		} catch (Exception exception) {
			log.error("exception Occured ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO updateSocietyStopLog(StopSocietyInvoiceDTO stopSocietyInvoiceDTO) {
		log.info("SocietyStopInvoiceService :: updateSocietyStopLog ");
		BaseDTO baseDTO = new BaseDTO();
		StopReleasePaymentLog stopLog = null;
		try {
			if (stopSocietyInvoiceDTO != null) {
				// --create new row in emp_increment_log table
				log.info("Stop Release Payment id==>" + stopSocietyInvoiceDTO.getStopReleasePaymentId());
				StopReleasePaymentLog stopReleasePaymentLog = new StopReleasePaymentLog();
				if (stopSocietyInvoiceDTO.getStopReleasePaymentId() != null) {
					stopReleasePaymentLog.setStopReleasePayment(
							stopReleasePaymentRepository.findOne(stopSocietyInvoiceDTO.getStopReleasePaymentId()));
				}
				log.info("status==>" + stopSocietyInvoiceDTO.getStatus());
				stopReleasePaymentLog.setStage(stopSocietyInvoiceDTO.getStatus());
				log.info("forwardRemarks==> " + stopSocietyInvoiceDTO.getForwardRemarks());
				stopReleasePaymentLog.setRemarks(stopSocietyInvoiceDTO.getForwardRemarks());
				stopReleasePaymentLog.setActionName(stopSocietyInvoiceDTO.getActionName());
				stopLog = stopReleasePaymentLogRepository.save(stopReleasePaymentLog);

				// -- update stop_release_payment_note table(Approval or Final-Apprval)
				log.info("forward for==> " + stopSocietyInvoiceDTO.getForwardFor());
				if (stopSocietyInvoiceDTO.getForwardFor().equals(true)) {
					if (stopSocietyInvoiceDTO.getNoteId() != null) {
						log.info("Stop Society Note Id==> " + stopSocietyInvoiceDTO.getNoteId());
						log.info("forward user Id==> " + stopSocietyInvoiceDTO.getForwardUserId());
						StopReleasePaymentNote stopReleasePaymentNote = stopReleasePaymentNoteRepository
								.findOne(stopSocietyInvoiceDTO.getNoteId());
						stopReleasePaymentNote
								.setForwardTo(userMasterRepository.findOne(stopSocietyInvoiceDTO.getForwardUserId()));
						stopReleasePaymentNote.setFinalApproval(stopSocietyInvoiceDTO.getForwardFor());
						stopReleasePaymentNoteRepository.save(stopReleasePaymentNote);
					} else {
						log.error("Note id not found");
					}
				}
			} else {
				log.error("Stop invoice log detail not found");
			}
			if (stopLog != null) {
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			}

		} catch (Exception e) {
			log.error("<--- Error in updateIncrementlog ---> ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return baseDTO;
	}
}
