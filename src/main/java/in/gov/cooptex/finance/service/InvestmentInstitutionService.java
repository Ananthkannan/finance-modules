package in.gov.cooptex.finance.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.model.InvestmentCategoryMaster;
import in.gov.cooptex.core.accounts.model.InvestmentInstitutionMaster;
import in.gov.cooptex.core.accounts.repository.InvestmentCategoryRepository;
import in.gov.cooptex.core.accounts.repository.InvestmentInstitutionRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class InvestmentInstitutionService {

	@Autowired
	InvestmentInstitutionRepository investmentInstitutionRepository;

	@Autowired
	ResponseWrapper responseWrapper;

	public BaseDTO getById(Long id) {
		log.info("InvestmentInstitutionService  getById method started [" + id + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			InvestmentInstitutionMaster investmentInstitutionMaster= investmentInstitutionRepository.getOne(id);
			baseDTO.setResponseContent(investmentInstitutionMaster);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("InvestmentInstitutionService getById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("InvestmentInstitutionService getById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("InvestmentInstitutionService  getById method completed");
		return responseWrapper.send(baseDTO);
	}
	
	public BaseDTO getInvestmentInstitutionList() {
		log.info("InvestmentInstitutionService  getInvestmentInstitutionList method started ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<InvestmentInstitutionMaster> investmentInstitutionList= investmentInstitutionRepository.getInvestmentInstitution();
			baseDTO.setResponseContent(investmentInstitutionList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("InvestmentInstitutionService getInvestmentInstitutionList RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("InvestmentInstitutionService getInvestmentInstitutionList Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("InvestmentInstitutionService  getInvestmentInstitutionList method completed");
		return responseWrapper.send(baseDTO);
	}

}