package in.gov.cooptex.finance.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.core.accounts.enums.VoucherStatus;
import in.gov.cooptex.core.accounts.enums.VoucherTypeDetails;
import in.gov.cooptex.core.accounts.model.Payment;
import in.gov.cooptex.core.accounts.model.PaymentDetails;
import in.gov.cooptex.core.accounts.model.RebateClaim;
import in.gov.cooptex.core.accounts.model.RebateClaimCollection;
import in.gov.cooptex.core.accounts.model.RebateClaimCollectionLog;
import in.gov.cooptex.core.accounts.model.RebateClaimCollectionNote;
import in.gov.cooptex.core.accounts.model.RebateClaimNote;
import in.gov.cooptex.core.accounts.model.RebateClaimCollectionLog;
import in.gov.cooptex.core.accounts.model.SalesInvoiceAdjustment;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.VoucherDetails;
import in.gov.cooptex.core.accounts.model.VoucherLog;
import in.gov.cooptex.core.accounts.model.VoucherType;
import in.gov.cooptex.core.accounts.repository.PaymentDetailsRepository;
import in.gov.cooptex.core.accounts.repository.PaymentMethodRepository;
import in.gov.cooptex.core.accounts.repository.PaymentRepository;
import in.gov.cooptex.core.accounts.repository.PaymentTypeMasterRepository;
import in.gov.cooptex.core.accounts.repository.RebateClaimCollectionRepository;
import in.gov.cooptex.core.accounts.repository.RebateClaimRepository;
import in.gov.cooptex.core.accounts.repository.VoucherDetailsRepository;
import in.gov.cooptex.core.accounts.repository.VoucherRepository;
import in.gov.cooptex.core.accounts.repository.VoucherTypeRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.PaymentCategory;
import in.gov.cooptex.core.enums.SequenceName;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.PaymentMethod;
import in.gov.cooptex.core.model.SequenceConfig;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.SequenceConfigRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AmountMovementType;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.dto.ClaimStatementDtoList;
import in.gov.cooptex.finance.dto.CollectionAmountRebateDetailsDto;
import in.gov.cooptex.finance.dto.RebateCollectionAmountDto;
import in.gov.cooptex.finance.repository.RebateClaimCollectionLogRepository;
import in.gov.cooptex.finance.repository.RebateClaimCollectionNoteRepository;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class RebateClaimCollectionService {
	
	@Autowired
	RebateClaimCollectionRepository rebateClaimCollectionRepository;
	
	@Autowired
	RebateClaimCollectionLogRepository rebateClaimCollectionLogRepository;
	
	@Autowired
	RebateClaimCollectionNoteRepository rebateClaimCollectionNoteRepository;
	
	@Autowired
	RebateClaimRepository rebateClaimRepository;
	
	@Autowired
	EntityMasterRepository entityMasterRepository;
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	SequenceConfigRepository sequenceConfigRepository;
	
	@Autowired
	VoucherTypeRepository voucherTypeRepository;
	
	@Autowired
	UserMasterRepository userMasterRepository;
	
	@Autowired
	PaymentMethodRepository paymentMethodRepository;
	
	@Autowired
	VoucherRepository voucherRepository;
	
	@Autowired
	PaymentRepository paymentRepository;
	
	@Autowired
	PaymentDetailsRepository paymentDetailsRepository;
	
	@Autowired
	PaymentTypeMasterRepository paymentTypeMasterRepository;  
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	VoucherDetailsRepository voucherDetailsRepository;
	
	
	
	
	
	
	
	@SuppressWarnings("unused")
	public BaseDTO saveRebateClaimCollectionData(RebateCollectionAmountDto rebateCollectionAmountDto) {
		log.error("Start RebateClaimCollectionService saveRebateClaimCollectionData", rebateCollectionAmountDto);
		BaseDTO baseDTO = new BaseDTO();
		try {

			Validate.notNull(rebateCollectionAmountDto, ErrorDescription.REBATECOLLECTIONDTO_EMPTY);
			/*Validate.notNull(rebateCollectionAmountDto.getNetAmount(),
					ErrorDescription.REBATECOLLECTION_CLAIMAMOUNT_EMPTY);*/
			Validate.nonZeroOrNull(rebateCollectionAmountDto.getNetAmount(),
					ErrorDescription.REBATECOLLECTION_CLAIMAMOUNT_EMPTY);
			Validate.notNull(rebateCollectionAmountDto.getRebateClaim(),
					ErrorDescription.REBATECOLLECTION_REBATECLAIM_EMPTY);
			
			EntityMaster loginUserEntity = entityMasterRepository.findByUserRegion(loginService.getCurrentUser().getId());
			Voucher voucher=new Voucher();
			SequenceConfig sequenceConfig = sequenceConfigRepository.findBySequenceName(SequenceName.valueOf(SequenceName.REBATE_COLLECTION_AMOUNT.name()));
			if (sequenceConfig == null) {
				throw new RestException(ErrorDescription.SEQUENCE_CONFIG_NOT_EXIST);
			}
			String paymentNumberPrefix = loginUserEntity.getCode() +
					sequenceConfig.getPrefix()+ AppUtil.getCurrentYearString() + AppUtil.getCurrentMonthString();
			voucher.setReferenceNumberPrefix(paymentNumberPrefix);
			voucher.setReferenceNumber(sequenceConfig.getCurrentValue());
			voucher.setName(VoucherTypeDetails.Payment.toString());
			VoucherType voucherType = voucherTypeRepository.findByName(VoucherTypeDetails.Payment.toString());
			voucher.setVoucherType(voucherType);
			voucher.setNarration(VoucherTypeDetails.Payment.toString());
			voucher.setFromDate(new Date());
			voucher.setToDate(new Date());
			voucher.setNetAmount(rebateCollectionAmountDto.getVoucherDetailsAmount());
			VoucherLog voucherLog = new VoucherLog();
			voucherLog.setVoucher(voucher);
			voucherLog.setStatus(VoucherStatus.RECEIVED);
			UserMaster userMaster = userMasterRepository.findOne(loginService.getCurrentUser().getId());
			voucherLog.setUserMaster(userMaster);
			voucher.getVoucherLogList().add(voucherLog); 
			Payment payment = new Payment();
			if (loginUserEntity == null) {
				log.info("Login User Info Not Available");
				throw new RestException(ErrorDescription.LOGIN_USER_INFO_NOT_AVAILABLE);
			}
			payment.setEntityMaster(loginUserEntity);
			log.info("loginUserEntity " , loginUserEntity);			
			payment.setPaymentNumberPrefix(paymentNumberPrefix);
			payment.setPaymentNumber(sequenceConfig.getCurrentValue());
			payment.setCreatedBy(rebateCollectionAmountDto.getCreatedUser());
			PaymentDetails paymentDetails = new PaymentDetails();
			paymentDetails.setPayment(payment);
			paymentDetails.setPaymentCategory(PaymentCategory.PAID_IN);
			if(!rebateCollectionAmountDto.getPaymentMethod().getName().equalsIgnoreCase("Cash")){
				paymentDetails.setBankReferenceNumber(rebateCollectionAmountDto.getCollectionRefNumber().toString());
				paymentDetails.setDocumentDate(rebateCollectionAmountDto.getCollectionDate());
			}
			paymentDetails.setVoucher(voucher);
			//voucher.setPaymentMode(paymentMethodRepository.findOne(rebateCollectionAmountDto.getPaymentMethod().getId()));
			paymentDetails.setPaymentTypeMaster(paymentTypeMasterRepository.getCustomerPaymentType());
			paymentDetails.setPaymentMethod(paymentMethodRepository.findOne(rebateCollectionAmountDto.getPaymentMethod().getId()));
			paymentDetails.setPaidBy(userMasterRepository.findOne(rebateCollectionAmountDto.getEmployeeMaster().getUserId()));
			log.info("Saving Payment Entity " , payment);
			
			
			VoucherDetails voucherDetails = new VoucherDetails();
			voucherDetails.setVoucher(voucher);
			voucherDetails.setAmount(rebateCollectionAmountDto.getVoucherDetailsAmount());
			
			sequenceConfig.setCurrentValue(sequenceConfig.getCurrentValue() + 1);
			sequenceConfigRepository.save(sequenceConfig);
			voucher=voucherRepository.save(voucher);
			paymentRepository.save(payment);
			paymentDetailsRepository.save(paymentDetails);
			voucherDetailsRepository.save(voucherDetails);
			
			RebateClaimCollection rebateClaimCollection = new RebateClaimCollection();
			Iterator<Long> rebateClaimIterator=rebateCollectionAmountDto.getRefNumber().iterator();
			while(rebateClaimIterator.hasNext())
			{
				Long rebateClaimId=rebateClaimIterator.next();
				rebateClaimCollection = new RebateClaimCollection();
				RebateClaim rebateClaim=rebateClaimRepository.findOne(rebateClaimId);
				rebateClaimCollection.setCollectionAmount(rebateCollectionAmountDto.getNetAmount());
				rebateClaimCollection.setVoucher(voucher);
				rebateClaimCollection.setVersion((long) 0);
				rebateClaimCollection.setRebateClaim(rebateClaim);
				log.error("RebateClaimCollection....", rebateClaimCollection);
				rebateClaimCollection =rebateClaimCollectionRepository.save(rebateClaimCollection);
			}
			baseDTO.setStatusCode(0);
		} catch (RestException restexp) {
			log.error("RebateClaimCollectionService RestException", restexp);
			baseDTO.setStatusCode(restexp.getStatusCode());
		} catch (NullPointerException nullexp) {
			log.error("RebateClaimCollectionService RestException", nullexp);
			baseDTO.setStatusCode(ErrorDescription.REBATECOLLECTION_CLAIMAMOUNT_EMPTY.getCode());
		}  catch (Exception exception) {
			log.error("Error while Creating RebateData ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.error("End RebateClaimCollectionService saveRebateClaimCollectionData", rebateCollectionAmountDto);
		return baseDTO;
	}
	
	public void saveRebateClaimCollectionNoteAndLog(RebateClaimCollection rebateClaimCollection, UserMaster userMaster,
			RebateCollectionAmountDto rebateCollectionAmountDto) {
		
		log.info("Start saveRebateClaimCollectionNoteAndLog  called..."+rebateClaimCollection);
		BaseDTO baseDto=new BaseDTO();
		try
		{
			
			RebateClaimCollectionLog rebateClaimCollectionLog=new RebateClaimCollectionLog();
			rebateClaimCollectionLog.setRebateClaimCollection(rebateClaimCollection);
			rebateClaimCollectionLog.setStage(AmountMovementType.SUBMITTED);
			rebateClaimCollectionLog.setVersion((long) 0);
			rebateClaimCollectionLogRepository.save(rebateClaimCollectionLog);
			
			RebateClaimCollectionNote rebateClaimCollectionNote=new RebateClaimCollectionNote();
			UserMaster userMasters=userMasterRepository.findOne(userMaster.getId());
			rebateClaimCollectionNote.setForwardTo(userMasters);
			rebateClaimCollectionNote.setNote(rebateCollectionAmountDto.getNote());
			rebateClaimCollectionNote.setRebateClaimCollection(rebateClaimCollection);
			if(rebateCollectionAmountDto.getFianlApproval().equalsIgnoreCase("Approved"))
				rebateClaimCollectionNote.setFinalApproval(false);
			else
				rebateClaimCollectionNote.setFinalApproval(true);
			rebateClaimCollectionNoteRepository.save(rebateClaimCollectionNote);
			
			
			
			
		log.info("End saveRebateClaimCollectionNoteAndLog  called..."+rebateClaimCollection);
		}
		catch(Exception exp){
			log.error("Exception Cause  : " , exp);
			baseDto.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
	}

	public BaseDTO getRebateDetails(RebateCollectionAmountDto rebateCollectionAmountDto) {
		log.info("Start getRebateDetails  called..."+rebateCollectionAmountDto);
		BaseDTO baseDTO = new BaseDTO();
		try {
           
			if(rebateCollectionAmountDto.getRefNumber() != null)
			{
			 List<CollectionAmountRebateDetailsDto> CollectionAmountRebateDetailsDtoList=new ArrayList<CollectionAmountRebateDetailsDto>();	
			 List<RebateClaim> rebateClaimList=rebateClaimRepository.getRebateClaimDataById(rebateCollectionAmountDto.getRefNumber());
			 Iterator<RebateClaim> rebateClaimitr=rebateClaimList.iterator();
			 while(rebateClaimitr.hasNext())
			 {
				 RebateClaim rebateClaims=rebateClaimitr.next();
				 CollectionAmountRebateDetailsDto collectionAmountRebateDetailsDto=new CollectionAmountRebateDetailsDto();
				 collectionAmountRebateDetailsDto.setFromMonth(rebateClaims.getFromMonth());
				 collectionAmountRebateDetailsDto.setToMonth(rebateClaims.getToMonth());
				 collectionAmountRebateDetailsDto.setRebateRefNo(rebateClaims.getRefNumber());
				List<RebateClaimCollection> rebateCollectionDataList=rebateClaimCollectionRepository.getByClaimIdList(rebateClaims.getId());
				 if(rebateCollectionDataList==null || rebateCollectionDataList.size()<=0)
				 {
					 collectionAmountRebateDetailsDto.setTotalAmountCollectedAl(0.0); 
				 }
				 else
				 {
					 collectionAmountRebateDetailsDto.setTotalAmountCollectedAl(rebateCollectionDataList.stream().mapToDouble(rcd->rcd.getCollectionAmount()).sum());
				 }
				 
				 if(rebateClaims.getClaimAmount() != null && collectionAmountRebateDetailsDto.getTotalAmountCollectedAl()!= null)
				 {
					 collectionAmountRebateDetailsDto.setBalanceAmountCollected(rebateClaims.getClaimAmount()-collectionAmountRebateDetailsDto.getTotalAmountCollectedAl()); 
				 }else
				 {
					 collectionAmountRebateDetailsDto.setBalanceAmountCollected(0.0); 
				 }
				 collectionAmountRebateDetailsDto.setPendingAmountCollected(0.0);
				 collectionAmountRebateDetailsDto.setTotalAmountClaimed(rebateClaims.getClaimAmount());
				 collectionAmountRebateDetailsDto.setToYear(rebateClaims.getToYear());
				 collectionAmountRebateDetailsDto.setFromYear(rebateClaims.getFromYear());
				 collectionAmountRebateDetailsDto.setTotalAmountCollected(0.0);
				 CollectionAmountRebateDetailsDtoList.add(collectionAmountRebateDetailsDto);
				
			 }
			 baseDTO.setResponseContents(CollectionAmountRebateDetailsDtoList);
			 baseDTO.setStatusCode(0);
			}
			
		} catch (RestException restException) {

			log.error("RebateClaimCollectionService getActiveRebateClaimRestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {

			log.error("RebateClaimCollectionService getActiveRebateClaimException ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}

		log.info(" End getRebateDetails"+rebateCollectionAmountDto);
		return baseDTO;
	}

	public BaseDTO getAllRebateDetails() {
		log.info("Start getAllRebateDetails  called...");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<RebateClaim> rebateClaimDetailsList=rebateClaimRepository.getAllByStageFinalApproved();
			baseDTO.setResponseContents(rebateClaimDetailsList);
			baseDTO.setStatusCode(0);
		 } catch (RestException restException) {
			log.error("RebateClaimCollectionService getActiveRebateClaimRestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {

			log.error("RebateClaimCollectionService getActiveRebateClaimException ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info(" End getAllRebateDetails");
		return baseDTO;
	}
	
	public String generateAmountCollectionCode(SequenceConfig sequenceConfig){
		log.info("<==== Starts RebateClaimCollectionService.generateAmountCollectionCode ====>");
		String code=null;
		if(sequenceConfig  !=null) {
			Long currentValue= sequenceConfig.getCurrentValue();
			code = sequenceConfig.getPrefix();
			LocalDate localDate = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			Month month = localDate.getMonth();
			log.info("currentValue ::"+currentValue);
			code = code + String.format(month+"18-"+"%04d", currentValue+1);
		}
		log.info("<==== Ends RebateClaimCollectionService.generateAmountCollectionCode ====>"+code);
		return code;
	}

       public BaseDTO getCollectionData(PaginationDTO paginationDto) {
		log.info("Start getCollectionData  called..."+paginationDto);
		BaseDTO baseDTO = new BaseDTO();
		try{
		 	Integer total=0;
			Integer start = paginationDto.getFirst(),pageSize = paginationDto.getPageSize();
			start=start*pageSize;
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();
			String mainQuery ="select (rcc.id) as id,(pd.voucher_id) as voucherId,(rc.id) as rebateId ,(rc.ref_number) as refNumber,(rc.from_month) as fromMonth,(rc.from_year) as fromYear,(rc.to_month) as toMonth,(rc.to_year) as toYear,"+
					"(rcc.collection_amount) as collectionAmount,(pm.name) as paymentMethod,(rcc.created_date) createdDate,(rcl.stage) approvalStatus from rebate_claim rc"+
					" join rebate_claim_collection rcc on rc.id=rcc.rebate_claim_id"+
					" join payment_details pd on rcc.voucher_id=pd.voucher_id"+
					" join payment_method pm on pm.id=pd.payment_method_id"+
					" left join rebate_collection_log rcl on rcl.rebate_collection_id=rcc.id and rcl.id=(select max(id) from rebate_collection_log rcl1 where rcl.rebate_collection_id=rcl1.rebate_collection_id)";

			if (paginationDto.getFilters() != null) {
			if(paginationDto.getFilters().get("stage")!=null)
				mainQuery += " where rcl.stage='"+paginationDto.getFilters().get("stage")+"'";
			
			if(paginationDto.getFilters().get("rebateClaimCollection.rebateClaim.fromMonth")!=null)
			{
				String fromMonthYear = (String) paginationDto.getFilters().get("rebateClaimCollection.rebateClaim.fromMonth");
				if(AppUtil.isInteger(fromMonthYear))
					mainQuery += "where (cast(rc.from_Year as varchar)) like upper('%"+fromMonthYear+"%')";
				else
					mainQuery += " where upper(rc.from_Month) like upper('%"+paginationDto.getFilters().get("rebateClaimCollection.rebateClaim.fromMonth")+"%') ";
					
			}
			
			if(paginationDto.getFilters().get("rebateClaimCollection.rebateClaim.toMonth")!=null)
			{
				String toMonthYear = (String) paginationDto.getFilters().get("rebateClaimCollection.rebateClaim.toMonth");
				if(AppUtil.isInteger(toMonthYear))
					mainQuery += " where (cast(rc.to_Year as varchar)) like upper('%"+toMonthYear+"%') ";
				else
					mainQuery += " where upper(rc.to_Month) like upper('%"+paginationDto.getFilters().get("rebateClaimCollection.rebateClaim.toMonth")+"%') ";
					
			}
			
			if(paginationDto.getFilters().get("rebateClaimCollection.createdDate")!=null)
			{
				 Date date = new Date((long) paginationDto.getFilters().get("rebateClaimCollection.createdDate"));
				 SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
				 mainQuery += "where date(rcc.created_date) = "+ "'"+f.format(date)+"'";
					
			}
			if(paginationDto.getFilters().get("rebateClaimCollection.rebateClaim.refNumber")!=null)
				mainQuery += " where upper(rc.ref_Number) like upper('%"+paginationDto.getFilters().get("rebateClaimCollection.rebateClaim.refNumber")+"%')";
			
			if(paginationDto.getFilters().get("rebateClaimCollection.collectionAmount")!=null)
				mainQuery += " where (cast(rc.claim_Amount as varchar)) like upper('%"+paginationDto.getFilters().get("rebateClaimCollection.collectionAmount")+"%') ";
			
		   }
			String countQuery=mainQuery.replace("select (rcc.id) as id,(pd.voucher_id) as voucherId,(rc.id) as rebateId ,(rc.ref_number) as refNumber,(rc.from_month) as fromMonth,(rc.from_year) as fromYear,(rc.to_month) as toMonth,(rc.to_year) as toYear,(rcc.collection_amount) as collectionAmount,(pm.name) as paymentMethod,(rcc.created_date) createdDate,(rcl.stage) approvalStatus", "select count(distinct(rc.id)) as count");
			log.info("count query... "+countQuery);
			countData=jdbcTemplate.queryForList(countQuery);
			for(Map<String,Object> data:countData){
				if(data.get("count")!=null)
					total=Integer.parseInt(data.get("count").toString().replace(",", ""));
			}
			log.info("total query... "+total);
			if(paginationDto.getSortField()==null)
			mainQuery+=" order by id desc limit "+pageSize+" offset "+start+";";
			
			if(paginationDto.getSortField()!=null && paginationDto.getSortOrder()!=null)
			{
				log.info("Sort Field:["+paginationDto.getSortField()+"] Sort Order:["+paginationDto.getSortOrder()+"]");
				if(paginationDto.getSortField().equals("fromMonth") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by fromMonth asc  ";
				if(paginationDto.getSortField().equals("toMonth") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by toMonth desc  ";
				if(paginationDto.getSortField().equals("refNumber") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by refNumber asc ";
				if(paginationDto.getSortField().equals("stage") && paginationDto.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by stage asc ";
				if(paginationDto.getSortField().equals("stage") && paginationDto.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by stage desc ";
				mainQuery+=" limit "+pageSize+" offset "+start+";";
			}
			log.info("Main Qury....."+mainQuery);
			List<RebateClaimCollectionLog> rebateClaimLogList = new ArrayList<>();
			listofData = jdbcTemplate.queryForList(mainQuery);
			for(Map<String,Object> data:listofData){
				RebateClaimCollectionLog rebateClaimCollectionLog=new RebateClaimCollectionLog();
				if(data.get("id")!=null)
					rebateClaimCollectionLog.setId(Long.parseLong(data.get("id").toString()));
				if(data.get("approvalStatus")!=null)
					rebateClaimCollectionLog.setStage(data.get("approvalStatus").toString());
				RebateClaimCollection rebateClaimCollection=new RebateClaimCollection();
				Voucher voucher=new Voucher();
				voucher.setId(Long.parseLong(data.get("voucherId").toString()));
				rebateClaimCollection.setVoucher(voucher);
				RebateClaim rebateClaim=new RebateClaim();
				rebateClaim.setId(Long.parseLong(data.get("rebateId").toString()));
				rebateClaim.setRefNumber(data.get("refNumber").toString());
				Integer from = (Integer) data.get("fromYear"); 
				Integer to = (Integer) data.get("toYear"); 
				Long fromYear = new Long(from);
				Long toYear = new Long(to);
				BigDecimal bg=(BigDecimal) data.get("collectionAmount");
				Double claimAmount= bg.doubleValue();
				Date date = (Date) data.get("createdDate");
				rebateClaimCollection.setCollectionAmount(claimAmount);
				rebateClaim.setFromMonth(data.get("fromMonth").toString());
				rebateClaim.setToMonth(data.get("toMonth").toString());
				rebateClaim.setFromYear(fromYear);
				rebateClaim.setToYear(toYear);
				rebateClaimCollection.setRebateClaim(rebateClaim);
				rebateClaimCollection.setCreatedDate(date);
				rebateClaimCollectionLog.setRebateClaimCollection(rebateClaimCollection);
				rebateClaimLogList.add(rebateClaimCollectionLog);
				
			} 
			log.info("Returning getallRebateClaimlazy list...."+rebateClaimLogList.size());
			log.info("Total records present in getallRebateClaimlazy..."+total);
			baseDTO.setResponseContents(rebateClaimLogList);
			baseDTO.setTotalRecords(total);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		}catch(Exception exp){
			log.error("Exception Cause  : " , exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO ;
	}

	public BaseDTO showViewPage(RebateCollectionAmountDto rebateCollectionAmountDto) {
		
		log.info("Start showViewPage  called..."+rebateCollectionAmountDto);
		BaseDTO baseDto=new BaseDTO();
		RebateClaimCollection rebateCollectionData=new RebateClaimCollection();
		List<RebateClaimCollection> rebateCollectionDataList=new ArrayList<RebateClaimCollection>();
		Voucher voucher=new Voucher();
		PaymentDetails paymentDetails=new PaymentDetails();
		RebateClaim rebateClaim=new RebateClaim();
		UserMaster userMaster=new UserMaster();
		try
		{
			if(rebateCollectionAmountDto.getRebateClaimCollection().getVoucher().getId() != null)
			{
				voucher=voucherRepository.getVoucherByRefId(rebateCollectionAmountDto.getRebateClaimCollection().getVoucher().getId());
				paymentDetails=paymentDetailsRepository.getPaymentDetailsByVoucherId(voucher.getId());
				rebateClaim=rebateClaimRepository.getOne(rebateCollectionAmountDto.getViewRebateClaim().getId());
				userMaster=userMasterRepository.findOne(paymentDetails.getPaidBy().getId());
			}
			rebateCollectionAmountDto.setViewCreatedBy(userMaster.getUsername());
			rebateCollectionAmountDto.setViewDocumentAmount(voucher.getNetAmount());
			rebateCollectionAmountDto.setViewDoucmentDate(paymentDetails.getDocumentDate());
			rebateCollectionAmountDto.setViewRebateClaim(rebateClaim);
			rebateCollectionAmountDto.setViewRefNumber(paymentDetails.getBankReferenceNumber());
			rebateCollectionAmountDto.setPaymentMehthod(paymentDetails.getPaymentMethod().getName());
			rebateCollectionAmountDto.setTotalAmountCollected(voucher.getNetAmount());
			 CollectionAmountRebateDetailsDto collectionAmountRebateDetailsDto=new CollectionAmountRebateDetailsDto();
			 collectionAmountRebateDetailsDto.setFromMonth(rebateClaim.getFromMonth());
			 collectionAmountRebateDetailsDto.setToMonth(rebateClaim.getToMonth());
			 collectionAmountRebateDetailsDto.setRebateRefNo(rebateClaim.getRefNumber());
			 if(rebateClaim.getId() != null)
			 {
//				rebateCollectionData=rebateClaimCollectionRepository.getByClaimId(rebateClaim.getId()); 
				 rebateCollectionDataList=rebateClaimCollectionRepository.getListByClaimId(rebateClaim.getId());
				log.error("<<<===rebateCollectionDataList data size is===> : " , rebateCollectionDataList.size());
			 }
			 collectionAmountRebateDetailsDto.setTotalAmountCollectedAl(rebateCollectionDataList.stream().mapToDouble(rc->rc.getCollectionAmount()).sum());
			 collectionAmountRebateDetailsDto.setBalanceAmountCollected(rebateClaim.getClaimAmount()-collectionAmountRebateDetailsDto.getTotalAmountCollectedAl());
			 collectionAmountRebateDetailsDto.setPendingAmountCollected(collectionAmountRebateDetailsDto.getBalanceAmountCollected()-(rebateCollectionDataList.stream().mapToDouble(rc->rc.getCollectionAmount()).sum()));
			 collectionAmountRebateDetailsDto.setTotalAmountClaimed(rebateClaim.getClaimAmount());
			 collectionAmountRebateDetailsDto.setToYear(rebateClaim.getToYear());
			 collectionAmountRebateDetailsDto.setFromYear(rebateClaim.getFromYear());
			 collectionAmountRebateDetailsDto.setTotalAmountCollected(0.0);
			 rebateCollectionAmountDto.setCollectionAmountRebateDetailsDto(collectionAmountRebateDetailsDto);
			baseDto.setResponseContent(rebateCollectionAmountDto);
			baseDto.setStatusCode(0);
		}catch(Exception exp){
			log.error("Exception Cause  : " , exp);
			baseDto.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("End showViewPage  called..."+rebateCollectionAmountDto);
		return baseDto;
	}

}
