package in.gov.cooptex.finance.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.repository.BankMasterRepository;
import in.gov.cooptex.core.accounts.model.BankBranchMaster;
import in.gov.cooptex.core.accounts.repository.BankBranchMasterRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.BankMaster;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EmployeeLoanAndAdvanceDetails;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeePersonalInfoEmployment;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.LoanAdvanceRecoverySchedule;
import in.gov.cooptex.core.model.LoanMaster;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.EmpLoanDisbursementRepository;
import in.gov.cooptex.core.repository.EmpLoanPaymentRepository;
import in.gov.cooptex.core.repository.EmpLoanPreclosureRequestRepository;
import in.gov.cooptex.core.repository.EmployeeLoanAndAdvanceDetailsRepository;
import in.gov.cooptex.core.repository.EmployeePersonalInfoEmploymentRepository;
import in.gov.cooptex.core.repository.LoanAdvanceRecoveryScheduleRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.dto.pos.LoanInformationDTO;
import in.gov.cooptex.dto.pos.LoanInformationScheduleDTO;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.finance.model.EmpLoanPayment;
import in.gov.cooptex.personnel.hrms.loans.model.EmpLoanDisbursement;
import in.gov.cooptex.personnel.hrms.loans.model.EmpLoanPreclosureRequest;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class LoanPaymentService {

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	ApplicationQueryRepository appQueryRepository;

	@Autowired
	EmpLoanPreclosureRequestRepository empLoanPreclosureRequestRepository;

	@Autowired
	BankBranchMasterRepository bankBranchMasterRepository;

	@Autowired
	BankMasterRepository bankMasterRepository;

	@Autowired
	EmpLoanPaymentRepository empLoanPaymentRepository;

	@PersistenceContext
	EntityManager entityManager;

	@Autowired
	EmpLoanDisbursementRepository empLoanDisbursementRepository;
	
	@Autowired
	EmployeeLoanAndAdvanceDetailsRepository employeeLoanAndAdvanceDetailsRepository;
	
	@Autowired
	EmployeePersonalInfoEmploymentRepository employeePersonalInfoEmploymentRepository;
	
	@Autowired
	ApplicationQueryRepository applicationQueryRepository;
	
	@Autowired
	LoanAdvanceRecoveryScheduleRepository loanAdvanceRecoveryScheduleRepository;

	public BaseDTO loadList(PaginationDTO paginationDTO) {
		log.info("<=Call EmpLoanDisbursementService loadEmployeeLoanInformation=>");
		BaseDTO baseDTO = new BaseDTO();
		try {

			log.info(":: EmpLoanPreclosureRequest search started ::");
			Session session = entityManager.unwrap(Session.class);
			Criteria criteria = session.createCriteria(EmpLoanPreclosureRequest.class, "preclosureRequest");
			criteria.createAlias("preclosureRequest.empLoanAndAdvanceDetails", "loanAndAdvance");
			criteria.createAlias("loanAndAdvance.loanMaster", "loanMaster");

			log.info(":: Criteria search started ::");
			if (paginationDTO.getFilters() != null) {

				String loanNumberFilter = (String) paginationDTO.getFilters().get("loanNumber");
				if (loanNumberFilter != null) {
					criteria.add(Restrictions.like("loanAndAdvance.loanNumber", "%" + loanNumberFilter.trim() + "%")
							.ignoreCase());
				}

				String loanNameFilter = (String) paginationDTO.getFilters().get("loanName");
				if (loanNameFilter != null) {
					criteria.add(Restrictions.like("loanMaster.name", "%" + loanNameFilter.trim() + "%").ignoreCase());
				}

				String proclosureAmount = (String) paginationDTO.getFilters().get("preclosuereAmountFilter");
				if (proclosureAmount != null) {
					criteria.add(Restrictions.sqlRestriction("cast(this_.preclosure_amount as varchar) like '%"
							+ proclosureAmount.toString().trim() + "%'"));
				}

				String penaltyAmount = (String) paginationDTO.getFilters().get("penaltyAmount");
				if (penaltyAmount != null) {
					criteria.add(Restrictions.sqlRestriction(
							"cast(this_.penalty_amount as varchar) like '%" + penaltyAmount.toString().trim() + "%'"));
				}
				criteria.add(Restrictions.eq("preclosureRequest.payBy", "OTHERS"));

				criteria.setProjection(Projections.rowCount());
				Integer totalResult = ((Long) criteria.uniqueResult()).intValue();
				criteria.setProjection(null);

				ProjectionList projectionList = Projections.projectionList();
				projectionList.add(Projections.property("id"));
				projectionList.add(Projections.property("loanAndAdvance.id"));
				projectionList.add(Projections.property("loanAndAdvance.loanNumber"));
				projectionList.add(Projections.property("loanMaster.name"));
				projectionList.add(Projections.property("preclosureAmount"));
				projectionList.add(Projections.property("penaltyAmount"));

				criteria.setProjection(projectionList);

				if (paginationDTO.getFirst() != null) {
					Integer pageNo = paginationDTO.getFirst();
					Integer pageSize = paginationDTO.getPageSize();
					if (pageNo != null && pageSize != null) {
						criteria.setFirstResult(pageNo * pageSize);
						criteria.setMaxResults(pageSize);
						log.info("PageNo : [" + pageNo + "] pageSize[" + pageSize + "]");
					}

					String sortField = paginationDTO.getSortField();
					String sortOrder = paginationDTO.getSortOrder();
					log.info("sortField outside : [" + sortField + "] sortOrder[" + sortOrder + "]");
					if (paginationDTO.getSortField() != null && paginationDTO.getSortOrder() != null) {
						log.info("sortField : [" + paginationDTO.getSortField() + "] sortOrder["
								+ paginationDTO.getSortOrder() + "]");

						if (paginationDTO.getSortField().equals("loanNumber")) {
							sortField = "loanAndAdvance.loanNumber";
						} else if (sortField.equals("loanName")) {
							sortField = "loanMaster.name";
						} else if (sortField.equals("preclosuereAmountFilter")) {
							sortField = "preclosureRequest.preclosureAmount";
						} else if (sortField.equals("penaltyAmount")) {
							sortField = "preclosureRequest.penaltyAmount";
						}
						if (sortOrder.equals("DESCENDING")) {
							criteria.addOrder(Order.desc(sortField));
						} else {
							criteria.addOrder(Order.asc(sortField));
						}
					} else {
						criteria.addOrder(Order.desc("preclosureRequest.modifiedDate"));
					}

				}

				List<?> resultList = criteria.list();

				log.info("criteria list executed and the list size is : " + resultList.size());

				if (resultList == null || resultList.isEmpty() || resultList.size() == 0) {
					log.info("EmpLoanPreclosureRequest List is null or empty ");
				}

				List<EmpLoanPreclosureRequest> loanPreclosuereList = new ArrayList<>();

				Iterator<?> it = resultList.iterator();
				while (it.hasNext()) {
					Object ob[] = (Object[]) it.next();
					EmpLoanPreclosureRequest response = new EmpLoanPreclosureRequest();
					EmployeeLoanAndAdvanceDetails employeeLoanAndAdvanceDetails = new EmployeeLoanAndAdvanceDetails();
					LoanMaster loanMaster = new LoanMaster();
					response.setId((Long) ob[0]);
					Long loanAndAdvanceId = (Long) ob[1];
					String loanNumber = (String) ob[2];
					String loanName = (String) ob[3];
					employeeLoanAndAdvanceDetails.setId(loanAndAdvanceId);
					employeeLoanAndAdvanceDetails.setLoanNumber(loanNumber);
					loanMaster.setName(loanName);
					employeeLoanAndAdvanceDetails.setLoanMaster(loanMaster);
					log.info("Id::::::::" + response.getId());
					response.setPreclosureAmount((Double) ob[4]);
					response.setPenaltyAmount((Double) ob[5]);
					log.info(":: List Response ::" + response);
					response.setEmpLoanAndAdvanceDetails(employeeLoanAndAdvanceDetails);
					EmpLoanPayment empLoanPayment = empLoanPaymentRepository.findByLoanPreclosureId(response.getId());
					if (empLoanPayment == null) {
						response.setStatus(true);
					}
					loanPreclosuereList.add(response);
				}
				baseDTO.setResponseContents(loanPreclosuereList);
				baseDTO.setTotalRecords(totalResult);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (Exception exp) {
			log.error("Exception Cause  : ", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO loadEmployeeLoanInformationByLoanId(Long loanId) {
		log.info("<=Call EmpLoanDisbursementService loadEmployeeLoanInformationByLoanId=>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("loademployeeloaninformationbyloanid loanId==> " + loanId);
			String getQuery = "select lon.loan_and_advance_id as loanId,lon.loan_number,lm.name,lon.rate_of_intrest as rateOfInterest,lon.loan_amount,dis.loan_start_date,date(dis.loan_start_date + (dis.loan_tenure * interval '1 month')) as end_date,dis.loan_tenure,count(sch.paid_date) as completed_tenure,dis.loan_tenure-count(sch.paid_date) as remaining_tenure,lon.loan_amount+sum(sch.interest_amount)-sum(case when sch.paid_date is not null then sch.paid_amount else 0 end) as balance_amount,pm.code as payment_code,epr.id as emp_preclosureId  from loan_advance_recovery_schedule sch,emp_loan_disbursement dis,emp_loan_and_advance_details lon,loan_master lm,emp_loan_preclosure_request epr join payment_mode pm on epr.payment_mode_id=pm.id \n"
					+ " where sch.loan_disbursement_id = dis.id and dis.loan_id = lon.loan_and_advance_id and lon.loan_type_id = lm.id and lon.loan_and_advance_id= :loanId group by lon.loan_and_advance_id,lon.loan_number,lm.name,lon.rate_of_intrest,lon.loan_amount,dis.loan_start_date,dis.loan_tenure,pm.code,epr.id;";
			String query = getQuery.trim();
			query = query.replace(":loanId", loanId.toString());
			log.info("loademployeeloaninformationbyloanid query==>" + query);
			baseDTO.setResponseContent(jdbcTemplate.query(query, new RowMapper<LoanInformationDTO>() {
				@Override
				public LoanInformationDTO mapRow(ResultSet rs, int no) throws SQLException {

					LoanInformationDTO loanInformationDTO = new LoanInformationDTO();
					loanInformationDTO.setLoanId(rs.getLong("loanId"));
					loanInformationDTO.setLoanNumber(rs.getString("loan_number"));
					loanInformationDTO.setLoanName(rs.getString("name"));
					loanInformationDTO.setRateOfInterest(rs.getDouble("rateOfInterest"));
					loanInformationDTO.setLoanAmount(rs.getDouble("loan_amount"));
					loanInformationDTO.setStartDate(rs.getDate("loan_start_date"));
					loanInformationDTO.setEndDate(rs.getDate("end_date"));
					loanInformationDTO.setTotalTenure(rs.getInt("loan_tenure"));
					loanInformationDTO.setCompletedTenure(rs.getLong("completed_tenure"));
					loanInformationDTO.setRemainingTenure(rs.getLong("remaining_tenure"));
					loanInformationDTO.setBalanceAmount(rs.getDouble("balance_amount"));
					loanInformationDTO.setPaymentCode(rs.getString("payment_code"));
					loanInformationDTO.setLoanPreclosureId(rs.getLong("emp_preclosureId"));
					log.info("loadEmployeeLoanInformationByLoanId :: loanInformationDTO==> " + loanInformationDTO);
					return loanInformationDTO;
				}
			}));
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());

		} catch (RestException re) {
			log.error("RestException :: EmpLoanDisbursementService .loadEmployeeLoanInformationByLoanId ==> ", re);
			baseDTO.setStatusCode(re.getStatusCode());

		} catch (Exception e) {
			log.error("Exception :: EmpLoanDisbursementService loadEmployeeLoanInformationByLoanId ==> ", e);
			baseDTO.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());

		}
		return baseDTO;
	}

	public BaseDTO loadEmpLoanInfoDetailsByLoanid(Long loanId) {
		log.info("<=Call EmpLoanDisbursementService loadEmployeeLoanInformationByLoanId=>");
		BaseDTO baseDTO = new BaseDTO();
		final String EMPLOYEE_LOAN_SCHEDULE_DETAILS_QUERY = "EMPLOYEE_LOAN_SCHEDULE_DETAILS_QUERY";
		try {
			log.info("loademployeeloaninformationbyloanid loanId==> " + loanId);

			ApplicationQuery applicationQuery = appQueryRepository
					.findByQueryName(EMPLOYEE_LOAN_SCHEDULE_DETAILS_QUERY);
			if (applicationQuery == null) {
				throw new Exception("EmpLoanDisbursementService :: Query not found in DB. Query Key: "
						+ EMPLOYEE_LOAN_SCHEDULE_DETAILS_QUERY);
			}
			String query = applicationQuery.getQueryContent().trim();
			query = query.replace(":loanId", loanId.toString());
			log.info("loadEmpLoanInfoDetailsByLoanid query==>" + query);
			baseDTO.setResponseContent(jdbcTemplate.query(query, new RowMapper<LoanInformationScheduleDTO>() {
				@Override
				public LoanInformationScheduleDTO mapRow(ResultSet rs, int no) throws SQLException {

					LoanInformationScheduleDTO loanInformationScheduleDTO = new LoanInformationScheduleDTO();

					loanInformationScheduleDTO.setLoanScheduleId(rs.getLong("id"));
					loanInformationScheduleDTO.setYear(rs.getInt("year"));
					loanInformationScheduleDTO.setMonth(rs.getString("month"));
					loanInformationScheduleDTO.setPrincipalAmount(rs.getDouble("principal_amount"));
					loanInformationScheduleDTO.setInterestAmount(rs.getDouble("interest_amount"));
					loanInformationScheduleDTO.setPaidAmount(rs.getDouble("paid_amount"));
					loanInformationScheduleDTO.setBalanceAmount(rs.getDouble("balance_amount"));
					loanInformationScheduleDTO.setPaidDate(rs.getDate("paid_date"));
					loanInformationScheduleDTO.setEmiAmount(rs.getDouble("emiAmount"));
					loanInformationScheduleDTO.setLoanStatus(rs.getString("paid_status"));

					log.info("loadEmployeeLoanInformationByLoanId :: loanInformationScheduleDTO==> "
							+ loanInformationScheduleDTO);
					return loanInformationScheduleDTO;
				}
			}));
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());

		} catch (RestException re) {
			log.error("RestException :: EmpLoanDisbursementService .loadEmployeeLoanInformationByLoanId ==> ", re);
			baseDTO.setStatusCode(re.getStatusCode());

		} catch (Exception e) {
			log.error("Exception :: EmpLoanDisbursementService loadEmployeeLoanInformationByLoanId ==> ", e);
			baseDTO.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());

		}
		return baseDTO;
	}

	@Transactional
	public BaseDTO saveEmployeeLoanPayment(EmpLoanPayment empLoanPayment) {
		log.info("===== Start EmpLoanService.saveEmployeeLoanPayment ====");
		BaseDTO baseDTO = new BaseDTO();
		try {
			if (empLoanPayment.getEmpLoanPreclosureRequest() != null
					&& empLoanPayment.getEmpLoanPreclosureRequest().getId() != null) {
				EmpLoanPreclosureRequest empLoanPreclosureRequest = empLoanPreclosureRequestRepository
						.findOne(empLoanPayment.getEmpLoanPreclosureRequest().getId());
				empLoanPayment.setEmpLoanPreclosureRequest(empLoanPreclosureRequest);
			} else {
				log.info("=== EmpLoanPreclosureRequest Cannot be Empty====");
				throw new Exception("==== EmpLoanPreclosureRequest Cannot be Empty ===");
			}
			if (empLoanPayment.getBankBranchMaster() != null && empLoanPayment.getBankBranchMaster().getId() != null) {
				BankBranchMaster bankBranchMaster = bankBranchMasterRepository
						.findOne(empLoanPayment.getBankBranchMaster().getId());
				empLoanPayment.setBankBranchMaster(bankBranchMaster);
			}
			if (empLoanPayment.getBankMaster() != null && empLoanPayment.getBankMaster().getId() != null) {
				BankMaster bankMaster = bankMasterRepository.findOne(empLoanPayment.getBankMaster().getId());
				empLoanPayment.setBankMaster(bankMaster);
			}
			if (empLoanPayment.getEmpLoanPreclosureRequest() != null && empLoanPayment.getEmpLoanPreclosureRequest().getId() != null) {
				Long loanAndAdvanceId = empLoanPreclosureRequestRepository.getLoanAndAdvanceIdUsingPreclosureId(empLoanPayment.getEmpLoanPreclosureRequest().getId());
				if (loanAndAdvanceId != null) {
					Long loanDisbursementId = empLoanDisbursementRepository.getIdUsingLoanAndAdvanceId(loanAndAdvanceId);
					if (loanDisbursementId != null) {
						
						  int currentYear=AppUtil.getYear(new Date());
	                        Long year= new Long(currentYear);
	                        int month=AppUtil.getMonth(new Date());
	                        String salaryMonth=AppUtil.getMonthName(month);
	                        
	                        log.info("current year:"+year+"   current month:"+salaryMonth);
	                        
					    	LoanAdvanceRecoverySchedule loanAdvanceRecoverySchedule=loanAdvanceRecoveryScheduleRepository.getloanRecoveryScheduleByMonthAndYear(loanDisbursementId,salaryMonth,Long.valueOf(year));
					    	loanAdvanceRecoverySchedule.setPaidAmount(loanAdvanceRecoverySchedule.getTotalAmount());
					    	//loanAdvanceRecoverySchedule.setBalanceAmount(0.00);
					    	loanAdvanceRecoveryScheduleRepository.save(loanAdvanceRecoverySchedule);
					    	
					    	List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
					    	ApplicationQuery loanAdvancedRecoveryQuery = applicationQueryRepository.findByQueryName("GET_PRECLOSED_MONTH_LOAN_ADVANCED_RECOVERY_SCHEDULE_ID");
					    	if (loanAdvancedRecoveryQuery == null || loanAdvancedRecoveryQuery.getId() == null) {
								log.info("Application Query not found for query name : GET_LOAN_PRECLOSER_AMOUNT");
								baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
								return baseDTO;
							}
					    	String  loanAdvancedRecoveryMainQuery= loanAdvancedRecoveryQuery.getQueryContent().trim();
					    	loanAdvancedRecoveryMainQuery=loanAdvancedRecoveryMainQuery.replace(":loandisbursementId", loanDisbursementId.toString());
					    	listofData = jdbcTemplate.queryForList(loanAdvancedRecoveryMainQuery);
					    	for (Map<String, Object> data : listofData) {
					    		if (data.get("id") != null) {
					    			Long id=Long.valueOf(data.get("id").toString());
					    			log.info("loan and advanced recovery id:"+id);
					    			String deletesql = "delete from loan_advance_recovery_schedule where id = ?";
					    			jdbcTemplate.update(deletesql, id);
					    			//loanAdvanceRecoveryScheduleRepository.deleteLoanRecoverySchedule(id);
					    		}
					    	}
						
						
						//String updateQuery = "update loan_advance_recovery_schedule set paid_date=current_date where loan_disbursement_id="+loanDisbursementId;
						//jdbcTemplate.update(updateQuery);
					}
				}
			}
			empLoanPayment = empLoanPaymentRepository.save(empLoanPayment);
			log.info("======= empLoanPayment Id  =====   " + empLoanPayment != null ? empLoanPayment.getId() : null);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.info("===== Exception Occured in EmpLoanService.saveEmployeeLoanPayment ====");
			log.info("===== Exception is ====" + e);
		}
		log.info("===== End EmpLoanService.saveEmployeeLoanPayment ====");
		return baseDTO;
	}
	
	public BaseDTO getEmployeeDetailsByLoanId(Long LoanId) {
		log.info("LoanPaymentService::getEmployeeDetailsByLoanId Method Start");
		BaseDTO baseDTO=new BaseDTO();
		try {
			EmployeeLoanAndAdvanceDetails employeeLoanAndAdvanceDetails=employeeLoanAndAdvanceDetailsRepository.findOne(LoanId);
			EmployeePersonalInfoEmployment employeePersonalInfoEmployment=employeePersonalInfoEmploymentRepository.findByEmployementId(employeeLoanAndAdvanceDetails.getEmployeeMaster().getId());
			EmployeeMaster empMaster=new EmployeeMaster();
			if(employeePersonalInfoEmployment!=null) {
				EmployeePersonalInfoEmployment employeePersonalInfoEmploymentObj=new EmployeePersonalInfoEmployment();
				Department department=new Department();
				Designation designation=new Designation();
				EntityMaster entityMaster=new EntityMaster();
				SectionMaster sectionMaster=new SectionMaster();
				empMaster.setId(employeePersonalInfoEmployment.getEmployeeMaster().getId());
				empMaster.setFirstName(employeePersonalInfoEmployment.getEmployeeMaster().getFirstName());
				empMaster.setLastName(employeePersonalInfoEmployment.getEmployeeMaster().getLastName());
				department.setId(employeePersonalInfoEmployment.getCurrentDepartment().getId());
				department.setName(employeePersonalInfoEmployment.getCurrentDepartment().getName());
				designation.setId(employeePersonalInfoEmployment.getCurrentDesignation().getId());
				designation.setName(employeePersonalInfoEmployment.getCurrentDesignation().getName());
				entityMaster.setId(employeePersonalInfoEmployment.getWorkLocation().getId());
				entityMaster.setName(employeePersonalInfoEmployment.getWorkLocation().getName());
				employeePersonalInfoEmploymentObj.setCurrentDepartment(department);
				employeePersonalInfoEmploymentObj.setCurrentDesignation(designation);
				employeePersonalInfoEmploymentObj.setWorkLocation(entityMaster);
				sectionMaster.setId(employeePersonalInfoEmployment.getCurrentSection().getId());
				sectionMaster.setName(employeePersonalInfoEmployment.getCurrentSection().getName());
				employeePersonalInfoEmploymentObj.setCurrentSection(sectionMaster);
				employeePersonalInfoEmploymentObj.setPfNumber(employeePersonalInfoEmployment.getPfNumber());
				empMaster.setPersonalInfoEmployment(employeePersonalInfoEmploymentObj);
				
			}
			baseDTO.setResponseContent(empMaster);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		}
		catch(Exception e) {
			log.info("Exception",e);
		}
		log.info("LoanPaymentService::getEmployeeDetailsByLoanId Method End");
		return baseDTO;
	}
	
	public BaseDTO checkAlreadyPayrollRunCurrentMonth(Long loanId) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			EmployeeLoanAndAdvanceDetails employeeLoanAndAdvanceDetails=employeeLoanAndAdvanceDetailsRepository.findOne(loanId);
			EmpLoanDisbursement empLoanDisbursement=empLoanDisbursementRepository.getEmployeeLoanDisbursementDetails(employeeLoanAndAdvanceDetails.getEmployeeMaster().getId(), loanId);
			 
			//log.info("loan disburdement id:"+empLoanDisbursement!=null? empLoanDisbursement.getId():"null");
			
			int currentYear=AppUtil.getYear(new Date());
            Long year= new Long(currentYear);
            int month=AppUtil.getMonth(new Date());
            String salaryMonth=AppUtil.getMonthName(month);
            
            log.info("current year:"+year+"   current month:"+salaryMonth);
			LoanAdvanceRecoverySchedule loanAdvanceRecoverySchedule=loanAdvanceRecoveryScheduleRepository.getloanRecoveryScheduleByMonthAndYear(empLoanDisbursement.getId(),salaryMonth,Long.valueOf(year));
			
			if (loanAdvanceRecoverySchedule!=null && loanAdvanceRecoverySchedule.getPaidDate() == null) {
				baseDTO.setResponseContent(false);
			} else if(loanAdvanceRecoverySchedule!=null && loanAdvanceRecoverySchedule.getPaidDate() != null) {
				baseDTO.setResponseContent(true);
			}
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("Exception at EmpLoanDisbursementService. checkAlreadyPayrollRunCurrentMonth() ", e);
		}
		log.info("EmpLoanDisbursementService.checkAlreadyPayrollRunCurrentMonth() Ends");
		return baseDTO;
	}

}
