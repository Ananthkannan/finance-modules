package in.gov.cooptex.finance.service;

import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.core.accounts.model.*;
import in.gov.cooptex.core.accounts.repository.EntityBankBranchRepository;
import in.gov.cooptex.core.accounts.repository.PaymentDetailsRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.SequenceName;
import in.gov.cooptex.core.finance.service.OperationService;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EmployeePersonalInfoEmployment;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.SequenceConfig;
import in.gov.cooptex.core.repository.AmountTransferDetailsRepository;
import in.gov.cooptex.core.repository.AmountTransferRepository;
import in.gov.cooptex.core.repository.AppQueryRepository;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EmployeePersonalInfoEmploymentRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.SequenceConfigRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AmountMovementType;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.LedgerPostingException;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.finance.ChequeToBankResponseDTO;
import in.gov.cooptex.finance.repository.AmountTransferLogRepository;
import in.gov.cooptex.finance.repository.AmountTransferNoteRepository;
import lombok.extern.log4j.Log4j2;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Log4j2
@Service
public class ChequeToBankService {

	@Autowired
	EntityManager entityManager;

	@Autowired
	private AmountTransferRepository amountTransferRepository;

	@Autowired
	private AmountTransferDetailsRepository amountTransferDetailsRepository;

	@Autowired
	private AmountTransferLogRepository amountTransferLogRepository;

	@Autowired
	private AmountTransferNoteRepository amountTransferNoteRepository;

	@Autowired
	private EntityMasterRepository entityMasterRepository;

	@Autowired
	private PaymentDetailsRepository paymentDetailsRepository;

	@Autowired
	private UserMasterRepository userMasterRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	@Autowired
	EntityBankBranchRepository entityBankBranchRepository;

	@Autowired
	EmployeeMasterRepository employeeMasterRepository;

	@Autowired
	LoginService loginService;

	@Autowired
	AppQueryRepository appQueryRepository;

	@Autowired
	OperationService operationService;

	@Autowired
	SequenceConfigRepository sequenceConfigRepository;

	@Autowired
	EmployeePersonalInfoEmploymentRepository employeePersonalInfoEmploymentRepository;

	public BaseDTO getAll(PaginationDTO paginationDTO) {
		log.info("<<====   ChequeToBankService ---  getAll ====## STARTS");
		BaseDTO baseDTO = new BaseDTO();
		Integer totalRecords = 0;
		List<ChequeToBankResponseDTO> resultList = null;
		try {
			resultList = new ArrayList<ChequeToBankResponseDTO>();
			Integer start = paginationDTO.getFirst(), pageSize = paginationDTO.getPageSize();
			start = start * pageSize;
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();

			Long userID = loginService.getCurrentUser().getId();
			log.info("current userID :" + userID != null ? userID : 0);
			Object[] resultObj = entityMasterRepository
					.gettypeCodeIdRegionIdByLoggedInUser(userID != null ? userID : 0);
			Object ob[] = (Object[]) resultObj[0];
			String entityMasterTypeCode = (String) (ob[0] != null ? ob[0] : "");
			Long entityId = ob[1] != null ? Long.valueOf(ob[1].toString()) : null;
			Long regionId = ob[2] != null ? Long.valueOf(ob[2].toString()) : null;

			String condition = "'" + AmountMovementType.CHEQUE_TO_BANK + "'";

			if (entityMasterTypeCode.equalsIgnoreCase("REGIONAL_OFFICE") && entityId != null) {
				condition = condition + " and em.id in (select id from entity_master em1 where em1.region_id="
						+ entityId + " or em1.id=" + entityId + ")";
				if (regionId != null && regionId > 0) {
					condition = condition + " or em.id in (select id from entity_master em1 where em1.region_id="
							+ regionId + " or em1.id=" + regionId + ")";
				}
			} else if (entityMasterTypeCode.equalsIgnoreCase("D_AND_P_OFFICE") && regionId != null) {
				condition = condition + " and em.id in (select id from entity_master em1 where em1.region_id="
						+ regionId + " or em1.id=" + regionId + ")";
			} else if (entityMasterTypeCode.equalsIgnoreCase("SHOW_ROOM") && entityId != null) {
				condition = condition + " and em.id=" + entityId;
			} else if (entityMasterTypeCode.equalsIgnoreCase("INSTITUTIONAL_SALES_SHOWROOM") && entityId != null) {
				condition = condition + " and em.id=" + entityId;
			}
			if (entityMasterTypeCode.equalsIgnoreCase("HEAD_OFFICE")) {
				condition = condition + "";
			}

			ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("CHEQUE_TO_BANK_QUERY");
			String mainQuery = applicationQuery.getQueryContent();
			log.info("db query----------" + mainQuery);

			mainQuery = mainQuery.replaceAll(":CHEQUE_TO_BANK", condition);
			log.info("after replace query------" + mainQuery);

			if (paginationDTO.getFilters() != null) {
				if (paginationDTO.getFilters().get("status") != null) {
					mainQuery += " and stage='" + paginationDTO.getFilters().get("status") + "'";
				}
				if (paginationDTO.getFilters().get("totalChequeDeposited") != null) {
					mainQuery += " and att.total_amount_transfered>= '"
							+ paginationDTO.getFilters().get("totalChequeDeposited") + "'";
				}
				if (paginationDTO.getFilters().get("amountTransferBy") != null) {
					mainQuery += " and upper(concat(emp.first_name,' ',emp.last_name)) like upper('%"
							+ paginationDTO.getFilters().get("amountTransferBy") + "%')";
				}
				if (paginationDTO.getFilters().get("createdDate") != null) {
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					Date fromDate = new Date((long) paginationDTO.getFilters().get("createdDate"));
					log.info(" fromDate Filter : " + fromDate);
					mainQuery += " and att.created_date::date='" + format.format(fromDate) + "'";
				}

				if (paginationDTO.getFilters().get("depositedDate") != null) {
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					Date fromDate = new Date((long) paginationDTO.getFilters().get("depositedDate"));
					log.info(" fromDate Filter : " + fromDate);
					mainQuery += " and att.deposit_date::date='" + format.format(fromDate) + "'";
				}

				if (paginationDTO.getFilters().get("entityCodeName") != null) {
					mainQuery += " and upper(concat(em.name,' ',em.code)) like upper('%"
							+ paginationDTO.getFilters().get("entityCodeName") + "%')";
				}
			}

			String countQuery = mainQuery.replace(
					"select em.code as entity_code,em.name as entity_name,att.deposit_date,att.id as id,att.total_amount_transfered as totalamount,emp.first_name as employeefirstname,emp.last_name as employeelastname,att.created_date as createddate,atl.stage as stage",
					"select count(distinct(att.id)) as count ");
			log.info("count query... " + countQuery);
			countData = jdbcTemplate.queryForList(countQuery);

			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					totalRecords = Integer.parseInt(data.get("count").toString().replace(",", ""));
			}

			if (paginationDTO.getSortField() == null)
				mainQuery += " order by att.id desc limit " + pageSize + " offset " + start + ";";

			if (paginationDTO.getSortField() != null && paginationDTO.getSortOrder() != null) {
				log.info("Sort Field:[" + paginationDTO.getSortField() + "] Sort Order:[" + paginationDTO.getSortOrder()
						+ "]");
				if (paginationDTO.getSortField().equals("totalChequeDeposited")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by att.total_amount_transfered asc  ";
				if (paginationDTO.getSortField().equals("totalChequeDeposited")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by att.total_amount_transfered desc  ";
				if (paginationDTO.getSortField().equals("amountTransferBy")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by emp.first_name asc ";
				if (paginationDTO.getSortField().equals("amountTransferBy")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by emp.first_name desc ";
				if (paginationDTO.getSortField().equals("createdDate")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by att.created_date asc ";
				if (paginationDTO.getSortField().equals("createdDate")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by att.created_date desc ";
				if (paginationDTO.getSortField().equals("depositedDate")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by att.deposit_date asc ";
				if (paginationDTO.getSortField().equals("depositedDate")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by att.deposit_date desc ";

				if (paginationDTO.getSortField().equals("status") && paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by stage asc ";
				if (paginationDTO.getSortField().equals("status") && paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by stage desc ";

				if (paginationDTO.getSortField().equals("entityCodeName")
						&& paginationDTO.getSortOrder().equals("ASCENDING")) {
					mainQuery += " order by em.code asc ";
				}
				if (paginationDTO.getSortField().equals("entityCodeName")
						&& paginationDTO.getSortOrder().equals("DESCENDING")) {
					mainQuery += " order by em.code desc ";
				}

				mainQuery += " limit " + pageSize + " offset " + start + ";";
			}

			log.info("filter query----------" + mainQuery);

			listofData = jdbcTemplate.queryForList(mainQuery);
			log.info("amount transfer list size-------------->" + listofData.size());

			for (Map<String, Object> data : listofData) {
				ChequeToBankResponseDTO response = new ChequeToBankResponseDTO();
				response.setId(Long.valueOf(data.get("id").toString()));
				response.setTotalChequeDeposited(Double.valueOf(data.get("totalamount").toString()));
				response.setAmountTransferBy(
						data.get("employeefirstname").toString() + " " + data.get("employeelastname").toString());
				response.setStatus(data.get("stage").toString());
				response.setCreatedDate((Date) data.get("createddate"));

				response.setDepositeDate(data.get("deposit_date") == null ? null : (Date) data.get("deposit_date"));
				response.setEntityCodeName(new StringBuilder()
						.append(data.get("entity_code") == null ? "" : data.get("entity_code").toString())
						.append(data.get("entity_name") == null ? "" : "/" + data.get("entity_name").toString())
						.toString());

				resultList.add(response);
			}

			log.info("final list size---------------" + resultList.size());
			baseDTO.setTotalRecords(totalRecords);
			baseDTO.setResponseContents(resultList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("<<====   ChequeToBankService ---  getAll ====## ENDS");
		} catch (Exception ex) {
			log.error("inside lazy method exception------");
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO getChequeToBankDetailsById(Long id) {
		BaseDTO baseDTO = new BaseDTO();
		List<AmountTransferDetails> amountTransferDetails = null;
		List<PaymentDetails> paymentDetailsList = null;
		Double totalNetAmount = 0.0;
		try {
			log.info("start getChequeToBankDetailsById method----" + id);
			if (id != null) {
				ChequeToBankResponseDTO chequeToBankResponseDTO = new ChequeToBankResponseDTO();
				amountTransferDetails = new ArrayList<>();
				paymentDetailsList = new ArrayList<>();
				chequeToBankResponseDTO.setChellanDetailsList(new ArrayList<>());
				chequeToBankResponseDTO.setPaymentDetailsList(new ArrayList<>());

				String bankName = null;

				AmountTransfer amountTransfer = amountTransferRepository.findOne(id);

				amountTransferDetails = amountTransferDetailsRepository.findAllByAmountTransfer(amountTransfer);
				log.info("amountTransferDetails list size============>" + amountTransferDetails.size());

				for (AmountTransferDetails transferDetails : amountTransferDetails) {
					PaymentDetails paymentDetails = paymentDetailsRepository
							.findOne(transferDetails.getPaymentDetails().getId());
					paymentDetailsList.add(paymentDetails);
					ChequeToBankResponseDTO responseDTO = new ChequeToBankResponseDTO();
					responseDTO.setChellanNumber(transferDetails.getChallanNumber());
					responseDTO.setChellanDate(transferDetails.getChallanDate());
					responseDTO.setNetAmount(transferDetails.getChallanAmount());
					responseDTO.setBankReferenceNumber(paymentDetails.getBankReferenceNumber());
					responseDTO.setDocumentDate(paymentDetails.getDocumentDate());
					responseDTO.setModifiedDate(transferDetails.getModifiedDate());
					responseDTO.setAccountNumber(transferDetails.getToBranch().getBankBranchMaster().getBranchName()
							+ "/" + transferDetails.getToBranch().getAccountNumber());
					responseDTO.setId(transferDetails.getId());
					responseDTO.setEntityBankBranch(transferDetails.getToBranch());
					bankName = transferDetails.getToBranch().getBankBranchMaster().getBankMaster().getBankName();
					chequeToBankResponseDTO.getChellanDetailsList().add(responseDTO);
				}

				List<ChequeToBankResponseDTO> challanDetailsList = chequeToBankResponseDTO.getChellanDetailsList();

				challanDetailsList = challanDetailsList.stream()
						.sorted(Comparator.comparing(ChequeToBankResponseDTO::getModifiedDate).reversed())
						.collect(Collectors.toList());

				chequeToBankResponseDTO.setChellanDetailsList(challanDetailsList);
				log.info("getChequeToBankDetailsById  paymentlist-------------" + paymentDetailsList.size());

				for (PaymentDetails paymentDetails : paymentDetailsList) {
					ChequeToBankResponseDTO dto = new ChequeToBankResponseDTO();
					dto.setCreatedDate(paymentDetails.getCreatedDate());
					dto.setModifiedDate(paymentDetails.getModifiedDate());
					dto.setBankReferenceNumber(paymentDetails.getBankReferenceNumber());
					dto.setDocumentDate(paymentDetails.getDocumentDate());
					dto.setNetAmount(paymentDetails.getAmount() != null ? paymentDetails.getAmount() : 0);
					if (paymentDetails.getEffectiveStatus() != null) {
						dto.setEffectiveStatus(paymentDetails.getEffectiveStatus());
					} else {
						dto.setEffectiveStatus("");
					}
					if (bankName != null) {
						dto.setBankName(paymentDetailsRepository.getBankNameByPaymentDetailId(paymentDetails.getId()));
					} else {
						dto.setBankName("");
					}
					totalNetAmount = totalNetAmount
							+ (paymentDetails.getVoucher() != null ? paymentDetails.getVoucher().getNetAmount() : 0);
					dto.setCheckBox(true);
					dto.setId(paymentDetails.getId());
					chequeToBankResponseDTO.getPaymentDetailsList().add(dto);
				}

				List<ChequeToBankResponseDTO> payDetailsList = chequeToBankResponseDTO.getPaymentDetailsList();

				payDetailsList = payDetailsList.stream()
						.sorted(Comparator.comparing(ChequeToBankResponseDTO::getModifiedDate).reversed())
						.collect(Collectors.toList());

				chequeToBankResponseDTO.setPaymentDetailsList(payDetailsList);

				/*
				 * AmountTransferNote amountTransferNote = amountTransferNoteRepository
				 * .findByAmountTransferId(amountTransfer.getId());
				 */
				AmountTransferLog amountTransferLog = amountTransferLogRepository
						.findByAmountTransferId(amountTransfer.getId());

				chequeToBankResponseDTO.setEmployeeMaster(amountTransfer.getTransferedBy());
				chequeToBankResponseDTO.setTotalNetAmount(totalNetAmount);
				chequeToBankResponseDTO.setTotalChequeDeposited(amountTransfer.getTotalAmountTransfered());
				chequeToBankResponseDTO.setDepositeDate(
						amountTransfer.getDepositDate() != null ? amountTransfer.getDepositDate() : null);
				// chequeToBankResponseDTO.setFianlApproval(amountTransferNote.getFinalApproval());
				// chequeToBankResponseDTO.setNote(amountTransferNote.getNote());
				chequeToBankResponseDTO.setRemarks(amountTransferLog.getRemarks());
				// chequeToBankResponseDTO.setUserMaster(amountTransferNote.getForwardTo());
				chequeToBankResponseDTO.setId(amountTransfer.getId());
				chequeToBankResponseDTO.setStatus(amountTransferLog.getStage());

				baseDTO.setResponseContent(chequeToBankResponseDTO);

				/*
				 * user note Feature
				 */

				/*
				 * List<Map<String, Object>> employeeData = new ArrayList<Map<String,
				 * Object>>(); if(amountTransfer!=null) {
				 * log.info("<<<:::::::amountTransfer::::not Null::::>>>>"+amountTransfer);
				 * ApplicationQuery applicationQueryForlog =
				 * applicationQueryRepository.findByQueryName(
				 * "AMOUNT_TRANSFER_LOG_EMPLOYEE_DETAILS"); if (applicationQueryForlog == null
				 * || applicationQueryForlog.getId() == null) {
				 * log.info("Application Query For Log Details not found for query name : " +
				 * applicationQueryForlog);
				 * baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode())
				 * ; return baseDTO; } String logquery =
				 * applicationQueryForlog.getQueryContent().trim(); log.
				 * info("<=========AMOUNT_TRANSFER_LOG_EMPLOYEE_DETAILS Query Content ======>"
				 * +logquery); logquery = logquery.replace(":amounttransId",
				 * "'"+amountTransfer.getId().toString()+"'"); log.
				 * info("Query Content For AMOUNT_TRANSFER_LOG_EMPLOYEE_DETAILS After replaced plan id View query : "
				 * + logquery); employeeData = jdbcTemplate.queryForList(logquery); log.
				 * info("<=========AMOUNT_TRANSFER_LOG_EMPLOYEE_DETAILS Employee Data======>"
				 * +employeeData); baseDTO.setTotalListOfData(employeeData); }
				 */

				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (Exception ex) {
			log.error("inside getChequeToBankDetailsById method exception------", ex);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("getChequeToBankDetailsById method ends-------");
		return baseDTO;
	}

	public BaseDTO editChequeToBank(ChequeToBankResponseDTO chequeToBankResponseDTO) {
		log.info("editChequeToBank() - START");
		BaseDTO baseDTO = new BaseDTO();
		try {
			Voucher voucher = updateChequeToBank(chequeToBankResponseDTO);
			if (voucher != null && voucher.getId() != null) {
				jdbcTemplate.update("UPDATE payment_details SET created_date=? WHERE voucher_id=?",
						new Object[] { voucher.getCreatedDate(), voucher.getId() });
			}
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.info("Exception Occured At  editChequeToBank method -------", e);
		}
		return baseDTO;
	}

	@Transactional
	public Voucher updateChequeToBank(ChequeToBankResponseDTO chequeToBankResponseDTO) {
		log.info("updateChequeToBank() method starts-------");
		AmountTransfer amountTransfer = null;
		Voucher voucher = new Voucher();
		try {
			Payment payment = new Payment();
			PaymentDetails pmntDtls = new PaymentDetails();
			List<ChequeToBankResponseDTO> chequeToBankResponseDTOList = chequeToBankResponseDTO
					.getSplitChellanDetailsList();
			if (!CollectionUtils.isEmpty(chequeToBankResponseDTOList)) {

				for (ChequeToBankResponseDTO scdl : chequeToBankResponseDTOList) {
					if (scdl.getId() != null) {
						pmntDtls = paymentDetailsRepository.findOne(scdl.getId());
						voucher = pmntDtls.getVoucher();
						payment = pmntDtls.getPayment();
						break;
					}
				}

				Date depositedDate = null;
				for (ChequeToBankResponseDTO splitChellanDetails : chequeToBankResponseDTOList) {
					PaymentDetails paymentDetails = new PaymentDetails();
					AmountTransferDetails amountTransferDetails = new AmountTransferDetails();
					if (splitChellanDetails.getId() != null) {
						paymentDetails = paymentDetailsRepository.findOne(splitChellanDetails.getId());
						depositedDate = paymentDetails.getDepositedDate();
						amountTransferDetails = amountTransferDetailsRepository.getAmtTrnsDtlsbyAmtIdPmtDtlsId(
								chequeToBankResponseDTO.getId(), splitChellanDetails.getId());

						/* voucher=paymentDetails.getVoucher(); */
					} else {
						// paymentDetails.setCreatedDate(new Date());
						// amountTransferDetails.setCreatedDate(new Date());
						paymentDetails.setCreatedDate(voucher.getCreatedDate());
						amountTransferDetails.setCreatedDate(voucher.getCreatedDate());
					}
					paymentDetails.setVoucher(voucher);
					paymentDetails.setPayment(payment);
					paymentDetails.setPaymentCategory(pmntDtls.getPaymentCategory());
					paymentDetails.setPaymentMethod(pmntDtls.getPaymentMethod());
					paymentDetails.setPaymentTypeMaster(pmntDtls.getPaymentTypeMaster());
					paymentDetails.setAmount(splitChellanDetails.getNetAmount());
					paymentDetails.setBankMaster(splitChellanDetails.getChequeBankMaster());
					paymentDetails.setBankReferenceNumber(splitChellanDetails.getBankReferenceNumber());
					paymentDetails.setDepositedDate(depositedDate);
					paymentDetails.setDocumentDate(splitChellanDetails.getDocumentDate());
					paymentDetails.setCreatedBy(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
//					paymentDetails.setCreatedByName(loginService.getCurrentUser().getUsername());
					paymentDetailsRepository.save(paymentDetails);
					log.info("PaymentDetails updated----------->");
					EntityBankBranch toBranchId = null;
					if (splitChellanDetails.getEntityBankBranch() != null
							&& splitChellanDetails.getEntityBankBranch().getId() != null) {
						toBranchId = entityBankBranchRepository
								.findOne(splitChellanDetails.getEntityBankBranch().getId());
					} else {
						toBranchId = null;
					}
					amountTransfer = amountTransferRepository.findOne(chequeToBankResponseDTO.getId());
					amountTransferDetails.setAmountTransfer(amountTransfer);
					amountTransferDetails.setChallanAmount(splitChellanDetails.getNetAmount());
					amountTransferDetails.setChallanDate(splitChellanDetails.getChellanDate());
					amountTransferDetails.setChallanNumber(splitChellanDetails.getChellanNumber());
					amountTransferDetails.setToBranch(toBranchId);
					amountTransferDetails.setPaymentDetails(paymentDetails);
					amountTransferDetails
							.setCreatedBy(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
					amountTransferDetails.setCreatedByName(loginService.getCurrentUser().getUsername());
					amountTransferDetailsRepository.save(amountTransferDetails);

				}
			}

		} catch (Exception e) {
			log.info("Exception Occured At  editChequeToBank method -------", e);
		}
		return voucher;

	}

	@Transactional
	public BaseDTO saveChequeToBank(ChequeToBankResponseDTO chequeToBankResponseDTO) {
		BaseDTO baseDTO = new BaseDTO();
		log.info("start saveChequeToBank method start-------");
		AmountTransfer amountTransfer = null;
		try {
			if (chequeToBankResponseDTO.getId() == null) {
				log.info("Save cheque to bank condition------------");
				amountTransfer = new AmountTransfer();
				amountTransfer.setMovementType(AmountMovementType.CHEQUE_TO_BANK);
				amountTransfer.setEntityMaster(
						entityMasterRepository.findOne(chequeToBankResponseDTO.getEntityMaster().getId()));
				amountTransfer.setTransferedBy(
						employeeMasterRepository.findOne(chequeToBankResponseDTO.getEmployeeMaster().getId()));
				amountTransfer.setTotalAmountTransfered(chequeToBankResponseDTO.getTotalChequeDeposited());
				amountTransfer.setCreatedBy(loginService.getCurrentUser());
				amountTransfer.setCreatedByName(loginService.getCurrentUser().getUsername());
				amountTransfer.setCreatedDate(new Date());
				amountTransfer.setDepositDate(chequeToBankResponseDTO.getDepositeDate());
				amountTransferRepository.save(amountTransfer);
				log.info("Amounttransfer inserted-------------");

				for (ChequeToBankResponseDTO dto : chequeToBankResponseDTO.getChellanDetailsList()) {
					AmountTransferDetails amountTransferDetails = new AmountTransferDetails();
					amountTransferDetails.setAmountTransfer(amountTransfer);
					amountTransferDetails.setChallanAmount(dto.getNetAmount());
					amountTransferDetails.setChallanDate(dto.getChellanDate());
					amountTransferDetails.setChallanNumber(dto.getChellanNumber());
					amountTransferDetails
							.setToBranch(entityBankBranchRepository.findOne(dto.getEntityBankBranch().getId()));
					amountTransferDetails.setPaymentDetails(paymentDetailsRepository.findOne(dto.getId()));
					amountTransferDetails.setCreatedBy(loginService.getCurrentUser());
					amountTransferDetails.setCreatedByName(loginService.getCurrentUser().getUsername());
					amountTransferDetails.setCreatedDate(new Date());
					amountTransferDetailsRepository.save(amountTransferDetails);
					log.info("AmountTransferDetails inserted----------->");

					PaymentDetails paymentDetails = paymentDetailsRepository.findOne(dto.getId());
					paymentDetails.setDepositedDate(chequeToBankResponseDTO.getDepositeDate());
					paymentDetailsRepository.save(paymentDetails);
					log.info("PaymentDetails updated----------->");

				}

				/*
				 * if
				 * (chequeToBankResponseDTO.getStage().equals(AmountMovementType.FINAL_APPROVED)
				 * ) { baseDTO = operationService.amountTransferLedgerPosting(amountTransfer,
				 * chequeToBankResponseDTO.getStage()); log.info(":::baseDTO:::::>>>" +
				 * baseDTO!=null?baseDTO.getStatusCode():"null"); if (baseDTO != null &&
				 * baseDTO.getStatusCode().equals(ErrorDescription.SUCCESS_RESPONSE.getCode()))
				 * { for (ChequeToBankResponseDTO dto :
				 * chequeToBankResponseDTO.getChellanDetailsList()) { PaymentDetails
				 * paymentDetails = paymentDetailsRepository.findOne(dto.getId());
				 * paymentDetails.setDepositedDate(new Date());
				 * paymentDetailsRepository.save(paymentDetails);
				 * log.info("PaymentDetails updated----------->"); } SequenceConfig
				 * sequenceConfig = sequenceConfigRepository
				 * .findBySequenceName(SequenceName.CHEQUE_TO_BANK);
				 * sequenceConfig.setCurrentValue(Long.valueOf(sequenceConfig.getCurrentValue())
				 * + chequeToBankResponseDTO.getChellanDetailsList().size());
				 * sequenceConfigRepository.save(sequenceConfig); } else {
				 * log.info("::::operationService Ledger Posting Failure Response:::::::::");
				 * 
				 * baseDTO.setStatusCode(
				 * ErrorDescription.getError(FinanceErrorCode.ACCOUNT_POSTING_ERROR).
				 * getErrorCode());
				 * 
				 * throw new LedgerPostingException(); } }
				 */

			} else {
				/*
				 * Edit function
				 * 
				 */
				/*
				 * log.info("update cheque to bank condition------------" +
				 * chequeToBankResponseDTO.getId()); if (chequeToBankResponseDTO.getId() !=
				 * null) { amountTransfer =
				 * amountTransferRepository.findOne(chequeToBankResponseDTO.getId());
				 * amountTransfer.setEntityMaster(
				 * entityMasterRepository.findOne(chequeToBankResponseDTO.getEntityMaster().
				 * getId())); amountTransfer.setTransferedBy(
				 * employeeMasterRepository.findOne(chequeToBankResponseDTO.getEmployeeMaster().
				 * getId())); amountTransfer.setTotalAmountTransfered(chequeToBankResponseDTO.
				 * getTotalChequeDeposited()); amountTransferRepository.save(amountTransfer);
				 * log.info("AmountTranfer Updated--------"+amountTransfer.getId());
				 * 
				 * log.info("chellan list size--------"+chequeToBankResponseDTO.
				 * getChellanDetailsList().size());
				 * 
				 * for (ChequeToBankResponseDTO dto :
				 * chequeToBankResponseDTO.getChellanDetailsList()) { if(dto.getId()!=null) {
				 * AmountTransferDetails amountTransferDetails =
				 * amountTransferDetailsRepository.findOne(dto.getId());
				 * log.info("amoutDetails id--------"+amountTransferDetails);
				 * amountTransferDetails.setAmountTransfer(amountTransfer);
				 * amountTransferDetails.setChallanAmount(dto.getNetAmount());
				 * amountTransferDetails.setChallanDate(dto.getChellanDate());
				 * amountTransferDetails.setChallanNumber(dto.getChellanNumber());
				 * amountTransferDetails
				 * .setToBranch(entityBankBranchRepository.findOne(dto.getEntityBankBranch().
				 * getId())); amountTransferDetailsRepository.save(amountTransferDetails);
				 * log.info("AmountTransferDetails Updated----------->"); } }
				 * 
				 * log.info("Deleted list size----------"+chequeToBankResponseDTO.
				 * getDeletedChellanDetailsList());
				 * if(chequeToBankResponseDTO.getDeletedChellanDetailsList().size()>0) {
				 * for(ChequeToBankResponseDTO
				 * responseDTO:chequeToBankResponseDTO.getDeletedChellanDetailsList()) {
				 * amountTransferDetailsRepository.delete(responseDTO.getId());
				 * log.info("AmountTransactionDetails Deleted--------"); } } }
				 */
			}
			/*
			 * AmountTransferNote amountTransferNote = new AmountTransferNote();
			 * amountTransferNote.setAmountTransfer(amountTransfer);
			 * amountTransferNote.setFinalApproval(chequeToBankResponseDTO.getFianlApproval(
			 * )); amountTransferNote.setNote(chequeToBankResponseDTO.getNote());
			 * amountTransferNote
			 * .setForwardTo(userMasterRepository.findOne(chequeToBankResponseDTO.
			 * getUserMaster().getId()));
			 * amountTransferNote.setCreatedBy(loginService.getCurrentUser());
			 * amountTransferNote.setCreatedByName(loginService.getCurrentUser().getUsername
			 * ()); amountTransferNote.setCreatedDate(new Date());
			 * amountTransferNoteRepository.save(amountTransferNote);
			 */

			AmountTransferLog amountTransferLog = new AmountTransferLog();
			amountTransferLog.setAmountTransfer(amountTransfer);
			amountTransferLog.setStage(chequeToBankResponseDTO.getStage());
			// amountTransferLog.setCreatedBy(loginService.getCurrentUser());
			amountTransferLog.setCreatedByName(loginService.getCurrentUser().getUsername());
			// amountTransferLog.setCreatedDate(new Date());
			amountTransferLogRepository.save(amountTransferLog);
			log.info("saveChequeToBank note and log inserted------");

			/*
			 * if(chequeToBankResponseDTO.getStage().equals(AmountMovementType.
			 * FINAL_APPROVED)) {
			 * baseDTO=operationService.amountTransferLedgerPosting(amountTransfer,
			 * chequeToBankResponseDTO.getStage()); }
			 */
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());

		} /*
			 * catch (LedgerPostingException le) {
			 * log.info("<--LedgerPostingException :- -->" + le); if (amountTransfer != null
			 * && amountTransfer.getId() != 0) {
			 * amountTransferDetailsRepository.deletebyamountTransferId(amountTransfer.getId
			 * ()); amountTransferRepository.delete(amountTransfer.getId()); }
			 * baseDTO.setStatusCode(ErrorDescription.getError(FinanceErrorCode.
			 * ACCOUNT_POSTING_ERROR).getErrorCode()); return baseDTO;
			 * 
			 * }
			 */catch (Exception e) {
			log.error("exception in saveChequeToBank method------", e);
			baseDTO.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("saveChequeToBank method ends-------------");
		return baseDTO;
	}

	public BaseDTO getPaymentDetails(ChequeToBankResponseDTO chequeToBankResponseDTO) {
		BaseDTO baseDTO = new BaseDTO();
		List<ChequeToBankResponseDTO> paymentDetailsList = null;
		String query = "";
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Double netAmount = 0.0;
		try {
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();

			paymentDetailsList = new ArrayList<ChequeToBankResponseDTO>();
			ApplicationQuery applicationQuery = applicationQueryRepository
					.findByQueryName("CHEQUE_TO_BANK_PAYMENT_DETAILS");

			AppUtil.getEndDateTime(chequeToBankResponseDTO.getToDate());

			Long userID = loginService.getCurrentUser().getId();
			log.info("current userID :" + userID != null ? userID : 0);
			Object[] resultObj = entityMasterRepository
					.gettypeCodeIdRegionIdByLoggedInUser(userID != null ? userID : 0);
			Object ob[] = (Object[]) resultObj[0];
//			String entityMasterTypeCode = (String) (ob[0] != null ? ob[0] : "");
			Long entityId = ob[1] != null ? Long.valueOf(ob[1].toString()) : null;
//			Long regionId = ob[2] != null ? Long.valueOf(ob[2].toString()) : null;

			query = applicationQuery.getQueryContent().trim();
			query = query.replace(":fromDate", format.format(chequeToBankResponseDTO.getFromDate()));
			query = query.replace(":toDate",
					format.format(AppUtil.getEndDateTime(chequeToBankResponseDTO.getToDate())));
			query = query.replace(":entityID", entityId.toString());

			listofData = jdbcTemplate.queryForList(query);
			log.info("paymentDetailsList size-----------" + listofData);

			if (listofData.size() > 0) {
				for (Map<String, Object> data : listofData) {
					ChequeToBankResponseDTO bankResponseDTO = new ChequeToBankResponseDTO();
					if (data.get("id") != null) {
						bankResponseDTO.setId(Long.parseLong(data.get("id").toString()));
					}
					if (data.get("bank_reference_number") != null) {
						bankResponseDTO.setBankReferenceNumber(data.get("bank_reference_number").toString());
					}
					if (data.get("net_amount") != null) {
						bankResponseDTO.setNetAmount(Double.valueOf(data.get("net_amount").toString()));
					}
					if (data.get("receipt_date") != null) {
						bankResponseDTO.setDocumentDate((Date) data.get("receipt_date"));
					}
					if (data.get("bank_name") != null) {
						bankResponseDTO.setBankName(data.get("bank_name").toString());
					}
					bankResponseDTO.setCheckBox(false);
					paymentDetailsList.add(bankResponseDTO);
					netAmount = netAmount + bankResponseDTO.getNetAmount();
				}
			}

			log.info("paymentDetailsList size--------------" + paymentDetailsList.size());
			chequeToBankResponseDTO.setTotalNetAmount(netAmount);
			chequeToBankResponseDTO.setPaymentDetailsList(paymentDetailsList);
			chequeToBankResponseDTO.setChellanNumber(generateSequenceNumber());

			baseDTO.setResponseContent(chequeToBankResponseDTO);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			log.error("Exception occured in ChequeToBankService .save", e);
			baseDTO.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return baseDTO;
	}

	public String generateSequenceNumber() {
		String referenceNumber = "";
		Date currentDate = new Date();
		DateFormat df = new SimpleDateFormat("ddMMyy");
		try {
			EntityMaster entityMaster = entityMasterRepository
					.getEntityInfoByLoggedInUser(loginService.getCurrentUser().getId());
			SequenceConfig sequenceConfig = sequenceConfigRepository.findBySequenceName(SequenceName.CHEQUE_TO_BANK);
			if (sequenceConfig == null) {
				throw new RestException(ErrorDescription.SEQUENCE_CONFIG_NOT_EXIST);
			}
			log.info("::::::sequenceConfig current value:::::::::" + sequenceConfig.getCurrentValue());

			referenceNumber = entityMaster.getCode() + sequenceConfig.getSeparator() + df.format(currentDate)
					+ sequenceConfig.getSeparator() + sequenceConfig.getPrefix() + sequenceConfig.getSeparator()
					+ sequenceConfig.getCurrentValue();
			log.info("reference number-----------" + referenceNumber);

		} catch (Exception e) {
			log.error("Exception occured in ChequeToBankService.generateSequenceNumber", e);
		}
		return referenceNumber;
	}

	public BaseDTO getBranchByBankId(Long id) {
		BaseDTO baseDTO = new BaseDTO();
		List<EntityBankBranch> branchList = null;
		try {
			log.info("getBranchByBankId method starts-------->" + id);
			EmployeePersonalInfoEmployment employment = employeePersonalInfoEmploymentRepository
					.findWorkLocationByUserId(loginService.getCurrentUser().getId());
			if (id != null && employment != null) {
				baseDTO = new BaseDTO();
				branchList = new ArrayList<EntityBankBranch>();
				branchList = entityBankBranchRepository.getBranchByBankIdandentityID(id,
						employment.getWorkLocation().getId());
				baseDTO.setResponseContents(branchList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in getBranchByBankId ::", e);
			baseDTO.setStatusCode(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("getBranchByBankId method ends-------->");
		return baseDTO;
	}

	@Transactional
	public BaseDTO approveChequeToBank(ChequeToBankResponseDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("approveChequeToBank method start=============>" + requestDTO.getId());
			AmountTransfer amountTransfer = amountTransferRepository.findOne(requestDTO.getId());

			AmountTransferNote amountTransferNote = new AmountTransferNote();
			amountTransferNote.setAmountTransfer(amountTransfer);
			amountTransferNote.setFinalApproval(requestDTO.getFianlApproval());
			amountTransferNote.setNote(requestDTO.getNote());
			amountTransferNote.setForwardTo(userMasterRepository.findOne(requestDTO.getUserMaster().getId()));
			amountTransferNote.setCreatedBy(loginService.getCurrentUser());
			amountTransferNote.setCreatedByName(loginService.getCurrentUser().getUsername());
			amountTransferNote.setCreatedDate(new Date());
			amountTransferNoteRepository.save(amountTransferNote);
			log.info("approveChequeToBank note inserted------");

			AmountTransferLog amountTransferLog = new AmountTransferLog();
			amountTransferLog.setAmountTransfer(amountTransfer);
			amountTransferLog.setStage(requestDTO.getStage());
			amountTransferLog.setCreatedBy(loginService.getCurrentUser());
			amountTransferLog.setCreatedByName(loginService.getCurrentUser().getUsername());
			amountTransferLog.setCreatedDate(new Date());
			amountTransferLog.setRemarks(requestDTO.getRemarks());
			amountTransferLogRepository.save(amountTransferLog);
			log.info("approveChequeToBank log inserted------");

			log.info("Account posting Start--------" + amountTransfer);
			log.info("Approval status----------" + requestDTO.getStage());

			if (requestDTO.getStage().equals(AmountMovementType.FINAL_APPROVED)) {
				baseDTO = operationService.amountTransferLedgerPosting(amountTransfer, requestDTO.getStage());
			}
			log.info("Account posting End--------");
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (LedgerPostingException l) {
			log.error("approveChequeToBank method LedgerPostingException----", l);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		} catch (Exception e) {
			log.error("approveChequeToBank method exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO rejectChequeToBank(ChequeToBankResponseDTO requestDTO) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("rejectChequeToBank method start=============>" + requestDTO.getId());

			AmountTransfer amountTransfer = amountTransferRepository.findOne(requestDTO.getId());

			AmountTransferLog amountTransferLog = new AmountTransferLog();
			amountTransferLog.setAmountTransfer(amountTransfer);
			amountTransferLog.setStage(requestDTO.getStage());
			amountTransferLog.setCreatedBy(loginService.getCurrentUser());
			amountTransferLog.setCreatedByName(loginService.getCurrentUser().getUsername());
			amountTransferLog.setCreatedDate(new Date());
			amountTransferLog.setRemarks(requestDTO.getRemarks());
			amountTransferLogRepository.save(amountTransferLog);

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			log.info("rejectChequeToBank  log inserted------");
		} catch (Exception e) {
			log.error("rejectChequeToBank method exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO checkBalanceAmount(ChequeToBankResponseDTO requestDTO) {
		log.error("checkBalanceAmount method Starts------");
		BaseDTO baseDTO = new BaseDTO();
		try {

			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		} catch (Exception e) {
			log.error("checkBalanceAmount method exception----", e);
		}
		log.info("checkBalanceAmount  log Ends------");
		return baseDTO;
	}
}
