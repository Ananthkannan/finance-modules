package in.gov.cooptex.finance.service;

import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.model.StaffBonusAllocation;
import in.gov.cooptex.core.accounts.repository.StaffBonusAllocationRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class StaffBonusAllocationService {

	@Autowired
	StaffBonusAllocationRepository staffBonusAllocationRepository;

	@Autowired
	ResponseWrapper responseWrapper;

	public BaseDTO getById(Long id) {
		log.info("StaffBonusAllocationService getById method started [" + id + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			// Validate.notNull(id, ErrorDescription.STAFF_BONUS_ALLOCATION_ID_EMPTY);
			StaffBonusAllocation staffBonusAllocation = staffBonusAllocationRepository.getOne(id);
			baseDTO.setResponseContent(staffBonusAllocation);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("StaffBonusAllocationService getById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("StaffBonusAllocationService getById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("StaffBonusAllocationService getById method completed");
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO deleteById(Long id) {
		log.info("StaffBonusAllocationService deleteById method started [" + id + "]");
		BaseDTO baseDTO = new BaseDTO();
		try {
			// Validate.notNull(id, ErrorDescription.STAFF_BONUS_ALLOCATION_ID_EMPTY);
			staffBonusAllocationRepository.delete(id);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("StaffBonusAllocationService deleteById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (DataIntegrityViolationException exception) {
			log.error("StaffBonusAllocationService deleteById DataIntegrityViolationException ", exception);
			if (exception.getCause().getCause() instanceof PSQLException) {
				baseDTO.setStatusCode(ErrorDescription.CANNOT_DELETE_REFERENCED_RECORD.getErrorCode());
			} else {
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			}
		} catch (Exception exception) {
			log.error("StaffBonusAllocationService deleteById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("StaffBonusAllocationService deleteById method completed");
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO createStaffBonusAllocation(StaffBonusAllocation staffBonusAllocation) {
		BaseDTO baseDTO = new BaseDTO();
		staffBonusAllocationRepository.save(staffBonusAllocation);
		return baseDTO;
	}

}