package in.gov.cooptex.finance.service;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.core.accounts.dto.GratuityPaymentDTO;
import in.gov.cooptex.core.accounts.dto.SocietyCodeDropDownDTO;
import in.gov.cooptex.core.accounts.enums.VoucherStatus;
import in.gov.cooptex.core.accounts.enums.VoucherTypeDetails;
import in.gov.cooptex.core.accounts.model.StaffGratuityPayment;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.VoucherDetails;
import in.gov.cooptex.core.accounts.model.VoucherLog;
import in.gov.cooptex.core.accounts.model.VoucherNote;
import in.gov.cooptex.core.accounts.model.VoucherType;
import in.gov.cooptex.core.accounts.repository.StaffGratuityPaymentRepository;
import in.gov.cooptex.core.accounts.repository.VoucherLogRepository;
import in.gov.cooptex.core.accounts.repository.VoucherNoteRepository;
import in.gov.cooptex.core.accounts.repository.VoucherRepository;
import in.gov.cooptex.core.accounts.repository.VoucherTypeRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.SequenceName;
import in.gov.cooptex.core.model.AppFeature;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.FileMovementConfig;
import in.gov.cooptex.core.model.PaymentMode;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.model.SequenceConfig;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.PaymentModeRepository;
import in.gov.cooptex.core.repository.SequenceConfigRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.FinanceErrorCode;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class GratuityPaymentService {

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	LoginService loginService;

	@Autowired
	EmployeeMasterRepository employeeMasterRepository;

	@Autowired
	SequenceConfigRepository sequenceConfigRepository;

	@Autowired
	VoucherTypeRepository voucherTypeRepository;

	@Autowired
	UserMasterRepository userMasterRepository;

	@Autowired
	VoucherRepository voucherRepository;

	@Autowired
	StaffGratuityPaymentRepository staffGratuityPaymentRepository;

	@Autowired
	PaymentModeRepository paymentModeRepository;

	@Autowired
	VoucherNoteRepository voucherNoteRepository;

	@Autowired
	VoucherLogRepository voucherLogRepository;

	public BaseDTO getAllGratuityEmployeesByEntityId() {
		log.info("<============== GratuityPaymentService:getAllGratuityEmployeesByEntityId ================>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			EntityMaster entityMaster = entityMasterRepository.findByUserRegion(loginService.getCurrentUser().getId());
			log.info("<=============== entityMaster id =============>" + entityMaster.getId());
			if (entityMaster != null) {
				List<Object> employeeObjList = employeeMasterRepository.getAllGratuityEmployeesByEntityId(entityMaster.getId());
				List<EmployeeMaster> employeeMasterList=new ArrayList<>();
				log.info(employeeObjList);
//				em.id,em.first_name,em.last_name,em.emp_code,em.middle_name 
			/*	employeeList.forEach(employee -> {
					EmployeeMaster employeeMasterObj = new EmployeeMaster();
					employeeMasterObj.setId(new Long(employee[0]));
					employeeMasterObj.setFirstName((String) employee[1]);
					employeeMasterObj.setLastName((String) employee[2]);
					employeeMasterObj.setEmpCode((String) employee[2]);
					employeeMasterObj.setMiddleName((String) employee[2]);
					employeeMasterList.add(employeeMasterObj);
				});
				*/
				Iterator<?> employee = employeeObjList.iterator();
				while (employee.hasNext()) {
					Object ob[] = (Object[]) employee.next();
					EmployeeMaster employeeMasterObj = new EmployeeMaster();
					employeeMasterObj.setId(Long.valueOf((ob[0]).toString()));
					employeeMasterObj.setFirstName((String) ob[1]);
					employeeMasterObj.setLastName((String) ob[2]);
					employeeMasterObj.setEmpCode((String) ob[3]);
					employeeMasterObj.setMiddleName((String) ob[4]);
					employeeMasterList.add(employeeMasterObj);
				}
				baseDTO.setResponseContents(employeeMasterList);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (RestException restException) {
			log.error("GratuityPaymentService getAllGratuityEmployeesByEntityId RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("GratuityPaymentService getAllGratuityEmployeesByEntityId Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public BaseDTO getGratuityDetailsByEmployeeId(Long employeeId) {
		log.info("<============== GratuityPaymentService:getGratuityDetailsByEmployeeId ================>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			Validate.notNull(employeeId, ErrorDescription.STAFF_GRATUITY_PAYMENT_EMPLOYEE_EMPTY);
			if (employeeId != null) {
				EmployeeMaster employeeMaster = employeeMasterRepository.findOne(employeeId);
				ApplicationQuery applicationQuery = applicationQueryRepository
						.findByQueryName("STAFF_GRATUITY_PAYMENT_DETAILS_QUERY");
				if (applicationQuery == null || applicationQuery.getId() == null) {
					log.info("Application Query not found for query name : " + applicationQuery);
					baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
					return baseDTO;
				}
				String query = applicationQuery.getQueryContent().trim();
				query = query.replace(":employeeId", "'" + employeeId + "'");
				GratuityPaymentDTO gratuityPaymentDTO = (GratuityPaymentDTO) jdbcTemplate.queryForObject(query,
						new Object[] {}, new BeanPropertyRowMapper(GratuityPaymentDTO.class));
				log.info("<============== gratuityPaymentDTO =============>" + gratuityPaymentDTO);
				if (gratuityPaymentDTO != null) {
					gratuityPaymentDTO.setEmployeeMaster(employeeMaster);
					/* Gratuity Calculation Method */
					gratuityCalculation(gratuityPaymentDTO);
				}
				baseDTO.setResponseContent(gratuityPaymentDTO);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (RestException restException) {
			log.error("GratuityPaymentService getAllGratuityEmployeesByEntityId RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (EmptyResultDataAccessException resltException) {
			log.error("GratuityPaymentService getAllGratuityEmployeesByEntityId EmptyResultDataAccessException ",
					resltException);
			// Satus 10 For No data With Given Employee ID
			baseDTO.setStatusCode(ErrorDescription.getError(FinanceErrorCode.GRATUITY_NOT_ALLOCATED_FOR_EMPLOYEE).getCode());
		} catch (Exception exception) {
			log.error("GratuityPaymentService getAllGratuityEmployeesByEntityId Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public void gratuityCalculation(GratuityPaymentDTO gratuityPaymentDTO) {
		log.info("<============== GratuityPaymentService gratuityCalculation =============>");
		Double balanceAmountRemitted = 0d;
		Double gratuityAmount = 0d;
		Double employeeContribution = 0d;
		Double totalInvestedAmount = 0d;
		Double totalRoundedInvestedAmount = 0d;
		try {
			Validate.notNull(gratuityPaymentDTO.getJoiningDate(),
					ErrorDescription.ERROR_EMP_TRANSFER_JOINING_DATE_NOT_FOUND);
			Calendar joiningDateCal = Calendar.getInstance();
			joiningDateCal.setTime(gratuityPaymentDTO.getJoiningDate());
			LocalDate joiningDate = LocalDate.of(joiningDateCal.get(Calendar.YEAR),
					joiningDateCal.get(Calendar.MONTH) + 1, joiningDateCal.get(Calendar.DAY_OF_MONTH));
			log.info("joiningDate====>" + joiningDate);
			Calendar currentDateCal = Calendar.getInstance();
			currentDateCal.setTime(new Date());
			LocalDate currentDate = LocalDate.of(currentDateCal.get(Calendar.YEAR),
					currentDateCal.get(Calendar.MONTH) + 1, currentDateCal.get(Calendar.DAY_OF_MONTH));
			log.info("currentDate=====>" + currentDate);
			Period difference = Period.between(joiningDate, currentDate);
			gratuityPaymentDTO.setTotalServiceYears(difference.getYears());
			log.info("gratuityPaymentDTO TotalServiceYears=============>" + gratuityPaymentDTO.getTotalServiceYears());
			gratuityPaymentDTO.setTotalServiceMonths(difference.getMonths());
			log.info(
					"gratuityPaymentDTO TotalServiceMonths=============>" + gratuityPaymentDTO.getTotalServiceMonths());
			gratuityPaymentDTO.setTotalServiceDays(difference.getDays());
			log.info("gratuityPaymentDTO TotalServiceDays=============>" + gratuityPaymentDTO.getTotalServiceDays());
			if (gratuityPaymentDTO.getTotalServiceMonths() >= 5 && gratuityPaymentDTO.getTotalServiceDays() >= 15) {
				gratuityPaymentDTO.setTotalCompletedServiceYears(gratuityPaymentDTO.getTotalServiceYears() + 1);
			} else {
				gratuityPaymentDTO.setTotalCompletedServiceYears(gratuityPaymentDTO.getTotalServiceYears());
			}
			log.info("gratuityPaymentDTO TotalCompletedServiceYears=============>"
					+ gratuityPaymentDTO.getTotalCompletedServiceYears());
			/* Find Gratuity AMount */
			/*
			 * Using this formula -> Gratuity Amount=(Basic+DA)*number of years of service%2
			 */
			gratuityAmount = gratuityPaymentDTO.getTotalBasicDA() * gratuityPaymentDTO.getTotalCompletedServiceYears()
					/ 2;
			if (gratuityAmount != null) {
				gratuityPaymentDTO.setGratuityAmount(gratuityAmount);
				log.info("<========== gratuityAmount =============>" + gratuityAmount);
				/* Find Balance Remitted Amount */
				/*
				 * Using this formula -> Balance Remitted Amount= gratuity amount - already
				 * deposited amount
				 */
				balanceAmountRemitted = gratuityAmount - gratuityPaymentDTO.getAlreadyDepositedAmount();
				gratuityPaymentDTO.setBalanceAmountRemitted(balanceAmountRemitted);
				log.info("<========== Balance Amount Remitted =============>"
						+ gratuityPaymentDTO.getBalanceAmountRemitted());
			}
			/* Find Employee Contribution */
			/* Using this formula -> Employee Contribution=(Basic+DA)*2percentage*12 */
			employeeContribution = gratuityPaymentDTO.getTotalBasicDA() * 0.02 * 12;
			gratuityPaymentDTO.setEmployeeContribution(employeeContribution);
			log.info("<========== EmployeeContribution =============>" + gratuityPaymentDTO.getEmployeeContribution());
			/* Find Total Invested Amount */
			/*
			 * Using this formula -> Total Invested Amount= Balance Remitted Amount +
			 * Employee Contribution
			 */
			totalInvestedAmount = gratuityPaymentDTO.getBalanceAmountRemitted()
					+ gratuityPaymentDTO.getEmployeeContribution();
			gratuityPaymentDTO.setTotalInvestedAmount(totalInvestedAmount);
			log.info("<========== TotalInvestedAmount =============>" + gratuityPaymentDTO.getTotalInvestedAmount());
			if (totalInvestedAmount != null) {
				/* Find Total Invested Amount Rounded To Rs. 5/- */
				totalRoundedInvestedAmount = (double) (5 * (Math.round(totalInvestedAmount / 5)));
				gratuityPaymentDTO.setTotalRoundedInvestedAmount(totalRoundedInvestedAmount);
				gratuityPaymentDTO.setNetAmount(totalRoundedInvestedAmount);
			}
		} catch (Exception exception) {
			log.error("GratuityPaymentService getAllGratuityEmployeesByEntityId Exception ", exception);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public BaseDTO getById(Long id) {
		log.info("<============== GratuityPaymentService:getById() ================>");
		BaseDTO baseDTO = new BaseDTO();
		Double balanceAmountRemitted=0d,totalRoundedInvestedAmount=0d;
		try {
			
			Validate.notNull(id, ErrorDescription.STAFF_GRATUITY_PAYMENT_ID_EMPTY);
			if (id != null) {
				ApplicationQuery applicationQuery = applicationQueryRepository
						.findByQueryName("STAFF_GRATUITY_PAYMENT_VIEW_QUERY");
				if (applicationQuery == null || applicationQuery.getId() == null) {
					log.info("Application Query not found for query name : " + applicationQuery);
					baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
					return baseDTO;
				}
				String query = applicationQuery.getQueryContent().trim();
				query = query.replace(":sgpId", "'" + id + "'");
				GratuityPaymentDTO gratuityPaymentDTO = (GratuityPaymentDTO) jdbcTemplate.queryForObject(query,
						new Object[] {}, new BeanPropertyRowMapper(GratuityPaymentDTO.class));
				if (gratuityPaymentDTO.getPaymentModeId() != null) {
					PaymentMode paymentMode = paymentModeRepository.findOne(gratuityPaymentDTO.getPaymentModeId());
					gratuityPaymentDTO.setPaymentMode(paymentMode);
				}
				if (gratuityPaymentDTO.getEmployeeId() != null) {
					EmployeeMaster employeeMaster = employeeMasterRepository
							.findOne(gratuityPaymentDTO.getEmployeeId());
					gratuityPaymentDTO.setEmployeeMaster(employeeMaster);
				}
				if (gratuityPaymentDTO.getVoucherNoteId() != null) {
					VoucherNote voucherNote = voucherNoteRepository.findOne(gratuityPaymentDTO.getVoucherNoteId());
					gratuityPaymentDTO.setVoucherNote(voucherNote);
				}
					balanceAmountRemitted = gratuityPaymentDTO.getBalanceAmountRemitted();
					totalRoundedInvestedAmount = gratuityPaymentDTO.getTotalRoundedInvestedAmount();
					
				gratuityCalculation(gratuityPaymentDTO);
				gratuityPaymentDTO.setBalanceAmountRemitted(balanceAmountRemitted);
				gratuityPaymentDTO.setTotalRoundedInvestedAmount(totalRoundedInvestedAmount);
				
				log.info("<============== gratuityPaymentDTO =============>" + gratuityPaymentDTO);
				baseDTO.setResponseContent(gratuityPaymentDTO);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (RestException restException) {
			log.error("GratuityPaymentService getById RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("GratuityPaymentService getById Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	@Transactional
	public BaseDTO createGratuityPayment(GratuityPaymentDTO gratuityPaymentDTO) {
		log.info("GratuityPaymentService createGratuityPayment method started");
		BaseDTO baseDTO = new BaseDTO();
		UserMaster userMaster = null;
		try {
			List<VoucherDetails> voucherDetailsList = new ArrayList<>();
			List<VoucherNote> voucherNoteList = new ArrayList<>();
			Validate.notNull(gratuityPaymentDTO.getEmployeeMaster(),
					ErrorDescription.STAFF_GRATUITY_PAYMENT_EMPLOYEE_EMPTY);
			Validate.notNull(gratuityPaymentDTO.getNetAmount(),
					ErrorDescription.STAFF_GRATUITY_PAYMENT_NETAMOUNT_EMPTY);
			Validate.notNull(gratuityPaymentDTO.getPaymentMode(), ErrorDescription.VOUCHER_PAYMENT_MODE_IS_REQUIRED);
			Validate.notNull(gratuityPaymentDTO.getVoucherNote().getForwardTo(),
					ErrorDescription.INTEND_CONSOLIDATION_REQUIREMENT_FORWARDTO_REQUIRED);
			Validate.notNull(gratuityPaymentDTO.getVoucherNote().getFinalApproval(),
					ErrorDescription.INTEND_CONSOLIDATION_REQUIREMENT_FORWARDFOR_REQUIRED);
			Validate.notNull(gratuityPaymentDTO.getVoucherNote().getNote(),
					ErrorDescription.POLICY_NOTE_PROCESS_CREATENOTE);
			if (gratuityPaymentDTO.getId() == null) {
				if (loginService.getCurrentUser().getId() != null) {
					userMaster = userMasterRepository.findOne(loginService.getCurrentUser().getId());
				}
				EntityMaster entityMaster = entityMasterRepository.findByUserRegion(userMaster.getId());
				SequenceConfig sequenceConfig = sequenceConfigRepository
						.findBySequenceName(SequenceName.valueOf(SequenceName.STAFF_GRATUITY_PAYMENT_NUMBER.name()));
				if (sequenceConfig == null) {
					throw new RestException(ErrorDescription.SEQUENCE_CONFIG_NOT_EXIST);
				}
				String referenceNumberPrefix = entityMaster.getCode() + sequenceConfig.getSeparator()
						+ sequenceConfig.getPrefix() + AppUtil.getCurrentYearString() + AppUtil.getCurrentMonthString();
				Long referenceNumber = sequenceConfig.getCurrentValue();
				sequenceConfig.setCurrentValue(sequenceConfig.getCurrentValue() + 1);
				sequenceConfigRepository.save(sequenceConfig);
				log.info(
						"<======================= Gratuity Payment Sequence config Table Inserted =======================>");
				// Voucher
				Voucher voucher = new Voucher();
				voucher.setReferenceNumberPrefix(referenceNumberPrefix);
				voucher.setReferenceNumber(referenceNumber);
				voucher.setName(VoucherTypeDetails.STAFF_GRATUITY_PAYMENT.toString());
				VoucherType voucherType = voucherTypeRepository
						.findByName(VoucherTypeDetails.Payment.toString());
				voucher.setVoucherType(voucherType);
				voucher.setNetAmount(gratuityPaymentDTO.getNetAmount());
				voucher.setNarration(VoucherTypeDetails.STAFF_GRATUITY_PAYMENT.toString());
				voucher.setFromDate(new Date());
				voucher.setToDate(new Date());
				voucher.setPaymentMode(gratuityPaymentDTO.getPaymentMode());
				// Voucher Details
				VoucherDetails voucherDetails = new VoucherDetails();
				voucherDetails.setVoucher(voucher);
				voucherDetails.setAmount(gratuityPaymentDTO.getNetAmount());
				voucherDetailsList.add(voucherDetails);
				voucher.setVoucherDetailsList(voucherDetailsList);
				log.info(
						"<======================= Staff Gratuity Payment Voucher Detail Table Inserted =======================>");
				// Staff Gratuity Payment
				StaffGratuityPayment staffGratuityPayment = new StaffGratuityPayment();
				if (gratuityPaymentDTO.getEmployeeMaster() != null) {
					EmployeeMaster employeeMaster = employeeMasterRepository
							.findOne(gratuityPaymentDTO.getEmployeeMaster().getId());
					staffGratuityPayment.setEmpMaster(employeeMaster);
				}
				staffGratuityPayment.setEmployeeAmount(gratuityPaymentDTO.getEmployeeContribution());
				staffGratuityPayment.setEmployeerAmount(gratuityPaymentDTO.getBalanceAmountRemitted());
				staffGratuityPayment.setRoundOffValue(gratuityPaymentDTO.getTotalRoundedInvestedAmount());
				staffGratuityPayment.setTotalGratuityAmount(gratuityPaymentDTO.getNetAmount());
				staffGratuityPayment.setVoucher(voucher);
				// Voucher Log
				VoucherLog voucherLog = new VoucherLog();
				voucherLog.setVoucher(voucher);
				voucherLog.setStatus(VoucherStatus.SUBMITTED);
				voucherLog.setUserMaster(userMaster);
				voucher.getVoucherLogList().add(voucherLog);
				log.info(
						"<======================= Staff Gratuity Payment Voucher Log Table Inserted =======================>");
				// Voucher Note
				VoucherNote voucherNote = new VoucherNote();
				voucherNote.setNote(gratuityPaymentDTO.getVoucherNote().getNote());
				voucherNote.setFinalApproval(gratuityPaymentDTO.getVoucherNote().getFinalApproval());
				if (gratuityPaymentDTO.getVoucherNote().getForwardTo() != null) {
					UserMaster forwardTo = userMasterRepository
							.findOne((gratuityPaymentDTO.getVoucherNote().getForwardTo().getId()));
					voucherNote.setForwardTo(forwardTo);
				}
				voucherNote.setCreatedBy(userMaster);
				voucherNote.setCreatedByName(userMaster.getUsername());
				voucherNote.setCreatedDate(new Date());
				voucherNote.setVoucher(voucher);
				voucherNoteList.add(voucherNote);
				voucher.setVoucherNote(voucherNoteList);
				log.info(
						"<======================= Staff Gratuity Payment Voucher Note Inserted =======================>");
				staffGratuityPaymentRepository.save(staffGratuityPayment);
				log.info("<======================= Staff Gratuity Payment Table Inserted =======================>");
				voucherRepository.save(voucher);
				log.info(
						"<======================= Staff Gratuity Payment Voucher Table Inserted =======================>");
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			} else {
				Validate.notNull(gratuityPaymentDTO.getId(), ErrorDescription.STAFF_GRATUITY_PAYMENT_ID_EMPTY);
				if (gratuityPaymentDTO.getId() != null) {
					StaffGratuityPayment staffGratuityPayment = staffGratuityPaymentRepository
							.findOne(gratuityPaymentDTO.getId());
					if (staffGratuityPayment.getVoucher().getId() != null) {
						
						Voucher voucher = voucherRepository.findOne(staffGratuityPayment.getVoucher().getId());
						if (loginService.getCurrentUser().getId() != null) {
							userMaster = userMasterRepository.findOne(loginService.getCurrentUser().getId());
						}
						voucher.setPaymentMode(gratuityPaymentDTO.getPaymentMode());
						voucherRepository.save(voucher);
						log.info(
								"<======================= Staff Gratuity Payment Voucher Table updated =======================>");
						VoucherNote vouchernote = voucherNoteRepository
								.findByVoucherId(staffGratuityPayment.getVoucher().getId());
						if (gratuityPaymentDTO.getVoucherNote().getForwardTo() != null) {
							UserMaster forwardTo = userMasterRepository
									.findOne((gratuityPaymentDTO.getVoucherNote().getForwardTo().getId()));
							vouchernote.setForwardTo(forwardTo);
						}
						vouchernote.setFinalApproval(gratuityPaymentDTO.getVoucherNote().getFinalApproval());
						vouchernote.setNote(gratuityPaymentDTO.getVoucherNote().getNote());
						voucherNoteRepository.save(vouchernote);
						
						VoucherLog voucherLog = new VoucherLog();
						voucherLog.setVoucher(voucher);
						voucherLog.setStatus(VoucherStatus.SUBMITTED);
						log.info(":: userMaster ::"+userMaster);
						voucherLog.setUserMaster(userMaster);
						voucherLogRepository.save(voucherLog);
						log.info(
								"<======================= Staff Gratuity Payment Voucher Note Table updated =======================>");
						baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
					}
				}
			}
		} catch (RestException restException) {
			log.error("GratuityPaymentService createGratuityPayment RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (DataIntegrityViolationException exception) {
			log.error("GratuityPaymentService createGratuityPayment DataIntegrityViolationException ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		} catch (Exception exception) {
			log.error("GratuityPaymentService createGratuityPayment Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("GratuityPaymentService createGratuityPayment method completed");
		return baseDTO;
	}

	public BaseDTO lazyloadlist(PaginationDTO request) {
		log.info("<====================== GratuityPaymentService lazyloadlist method started ==================>");
		BaseDTO baseDTO = new BaseDTO();
		try {

			Integer total = 0;
			Integer start = request.getFirst(), pageSize = request.getPageSize();
			start = start * pageSize;
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();
			ApplicationQuery applicationQuery = applicationQueryRepository
					.findByQueryName("GRATUITY_PAYMENT_LIST_QUERY");

			String mainQuery = applicationQuery.getQueryContent().trim();
			if (request.getFilters() != null) {
				if (request.getFilters().get("employeeCodeName") != null) {
					mainQuery += " and em.emp_code like upper('%" + request.getFilters().get("employeeCodeName")
							+ "%')";
				}
				if (request.getFilters().get("pfNumber") != null) {
					mainQuery += " and epd.pf_number like upper('%" + request.getFilters().get("pfNumber") + "%')";
				}
				if (request.getFilters().get("voucherNumber") != null) {
					mainQuery += " and v.reference_number_prefix like upper('%"
							+ request.getFilters().get("voucherNumber") + "%')";
				}
				if (request.getFilters().get("gratuityAmount") != null) {
					mainQuery += " and (cast(sgp.total_gratuity_amount as varchar)) like '%"
							+ request.getFilters().get("gratuityAmount") + "%'";
				}
				if (request.getFilters().get("status") != null) {
					mainQuery += " and upper(vl.status) like upper('%" + request.getFilters().get("status") + "%')";
				}
			}
			mainQuery += " group by concat(em.emp_code,'/',em.first_name,em.last_name),epd.pf_number,em.id,v.id,sgp.id,epie.retirement_date,vl.status";
			String countQuery = "select count(*) as count from staff_gratuity_payment";
			log.info("count query... " + countQuery);
			countData = jdbcTemplate.queryForList(countQuery);
			for (Map<String, Object> data : countData) {
				if (data.get("count") != null)
					total = Integer.parseInt(data.get("count").toString().replace(",", ""));
			}

			if (request.getSortField() == null)
				mainQuery += " order by staffGratuityId desc limit " + pageSize + " offset " + start + ";";

			if (request.getSortField() != null && request.getSortOrder() != null) {
				log.info("Sort Field:[" + request.getSortField() + "] Sort Order:[" + request.getSortOrder() + "]");
				if (request.getSortField().equals("employeeCodeName") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by em.emp_code asc ";
				if (request.getSortField().equals("employeeCodeName") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by em.emp_code desc ";
				if (request.getSortField().equals("pfNumber") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by epd.pf_number asc  ";
				if (request.getSortField().equals("pfNumber") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by epd.pf_number desc  ";
				if (request.getSortField().equals("voucherNumber") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by v.reference_number_prefix asc ";
				if (request.getSortField().equals("voucherNumber") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by v.reference_number_prefix desc ";
				if (request.getSortField().equals("gratuityAmount") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by sgp.total_gratuity_amount asc ";
				if (request.getSortField().equals("gratuityAmount") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by sgp.total_gratuity_amount desc ";
				if (request.getSortField().equals("status") && request.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by vl.status asc  ";
				if (request.getSortField().equals("status") && request.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by vl.status desc  ";
				mainQuery += " limit " + pageSize + " offset " + start + ";";
			}
			log.info("Main Qury....." + mainQuery);
			List<GratuityPaymentDTO> gratuityPaymentDTOList = new ArrayList<>();
			listofData = jdbcTemplate.queryForList(mainQuery);
			for (Map<String, Object> data : listofData) {
				GratuityPaymentDTO gratuityPaymentDTO = new GratuityPaymentDTO();
				if (data.get("staffGratuityId") != null)
					gratuityPaymentDTO.setId(Long.parseLong(data.get("staffGratuityId").toString()));
				if (data.get("employeeCodeName") != null)
					gratuityPaymentDTO.setEmployeeCodeName(data.get("employeeCodeName").toString());
				if (data.get("voucherNumber") != null)
					gratuityPaymentDTO.setVoucherNumber(data.get("voucherNumber").toString());
				if (data.get("pfNumber") != null)
					gratuityPaymentDTO.setPfNumber(data.get("pfNumber").toString());
				if (data.get("gratuityAmount") != null)
					gratuityPaymentDTO.setGratuityAmount(Double.valueOf(data.get("gratuityAmount").toString()));
				if (data.get("status") != null)
					gratuityPaymentDTO.setStatus(data.get("status").toString());
				gratuityPaymentDTOList.add(gratuityPaymentDTO);
			}
			log.info("Returning GratuityPaymentDTO list...." + gratuityPaymentDTOList);
			log.info("Total records present in GratuityPaymentDTO..." + total);
			baseDTO.setResponseContent(gratuityPaymentDTOList);
			baseDTO.setTotalRecords(total);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception exp) {
			log.error("Exception Cause  : ", exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO approveGratuityPayment(GratuityPaymentDTO gratuityPaymentDTO) {
		BaseDTO baseDTO = new BaseDTO();
		log.info("<============ approveGratuityPayment method start =============>");
		try {
			Validate.notNull(gratuityPaymentDTO.getId(), ErrorDescription.STAFF_GRATUITY_PAYMENT_ID_EMPTY);
			if (gratuityPaymentDTO.getId() != null) {
				StaffGratuityPayment staffGratuityPayment = staffGratuityPaymentRepository
						.findOne(gratuityPaymentDTO.getId());
				if (staffGratuityPayment.getVoucher() != null) {
					Voucher voucher = voucherRepository.getOne(staffGratuityPayment.getVoucher().getId());
					log.info("<=========== voucher id ==========>" + voucher);

					VoucherNote voucherNote = new VoucherNote();
					voucherNote.setForwardTo(
							userMasterRepository.findOne(gratuityPaymentDTO.getVoucherNote().getForwardTo().getId()));
					log.info("voucherNote forwardTo user---------------->" + voucherNote.getForwardTo().getId());
					voucherNote.setFinalApproval(gratuityPaymentDTO.getVoucherNote().getFinalApproval());
					voucherNote.setNote(gratuityPaymentDTO.getVoucherNote().getNote());
					voucherNote.setVoucher(voucher);
					voucherNoteRepository.save(voucherNote);
					log.info("------------voucherNote table Inserted----------------");

					VoucherLog voucherLog = new VoucherLog();
					voucherLog.setRemarks(gratuityPaymentDTO.getVoucherLog().getRemarks());
					voucherLog.setVoucher(voucher);
					voucherLog.setStatus(gratuityPaymentDTO.getVoucherLog().getStatus());
					voucherLog.setUserMaster(
							userMasterRepository.findOne(gratuityPaymentDTO.getVoucherNote().getForwardTo().getId()));
					voucherLogRepository.save(voucherLog);
					log.info("------------voucherLog table Inserted----------------");

					baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				}
			}
		} catch (Exception e) {
			log.error("approveGratuityPayment exception ------", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("approveGratuityPayment method end-------");
		return baseDTO;
	}

	public BaseDTO rejectGratuityPayment(GratuityPaymentDTO gratuityPaymentDTO) {
		BaseDTO baseDTO = new BaseDTO();
		log.info("<=========== rejectGratuityPayment method start ===========>");
		try {
			Validate.notNull(gratuityPaymentDTO.getId(), ErrorDescription.STAFF_GRATUITY_PAYMENT_ID_EMPTY);
			if (gratuityPaymentDTO.getId() != null) {
				StaffGratuityPayment staffGratuityPayment = staffGratuityPaymentRepository
						.findOne(gratuityPaymentDTO.getId());
				if (staffGratuityPayment.getVoucher() != null) {
					Voucher voucher = voucherRepository.getOne(staffGratuityPayment.getVoucher().getId());
					log.info("<========== rejectPettyCashExpensePayment voucher id =========>"
							+ staffGratuityPayment.getVoucher().getId());
					VoucherLog voucherLog = new VoucherLog();
					voucherLog.setRemarks(gratuityPaymentDTO.getVoucherLog().getRemarks());
					voucherLog.setVoucher(voucher);
					voucherLog.setStatus(gratuityPaymentDTO.getVoucherLog().getStatus());
					voucherLog.setUserMaster(
							userMasterRepository.findOne(gratuityPaymentDTO.getVoucherNote().getForwardTo().getId()));
					voucherLogRepository.save(voucherLog);
					log.info("<======== voucherLog table Inserted ==========>");

					baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				}
			}
		} catch (Exception e) {
			log.error("<====== rejectGratuityPayment exception =======>", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<========== rejectGratuityPayment method end ===========>");
		return baseDTO;
	}
}
