package in.gov.cooptex.finance.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.gov.cooptex.core.accounts.model.PurchaseInvoice;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.repository.PurchaseInvoiceRepository;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class PurchaseInvoiceService {

	@Autowired
	private PurchaseInvoiceRepository purchaseInvoiceRepository;
	
	
	
	public BaseDTO loadPurchaseInvoiceBySupplierId(Long supplierId)
	{
		log.info("=========Supplier Id =====" + supplierId);
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<PurchaseInvoice> purchaseinvoiceList=purchaseInvoiceRepository.getPurchaseInvoiceBySupplierId(supplierId);
			baseDTO.setResponseContents(purchaseinvoiceList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("=========Exception =====" , e);
		}
		return baseDTO;
	}
}
