package in.gov.cooptex.finance.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.service.NotificationEmailService;
import in.gov.cooptex.core.accounts.model.BudgetConfig;
import in.gov.cooptex.core.accounts.model.BudgetRequest;
import in.gov.cooptex.core.accounts.model.BudgetTransfer;
import in.gov.cooptex.core.accounts.model.BudgetTransferDetails;
import in.gov.cooptex.core.accounts.model.BudgetTransferLog;
import in.gov.cooptex.core.accounts.model.BudgetTransferNote;
import in.gov.cooptex.core.accounts.repository.GlAccountRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.finance.repository.BudgetRequestRepository;
import in.gov.cooptex.core.finance.repository.BudgetTransferDetailsRepository;
import in.gov.cooptex.core.finance.repository.BudgetTransferLogRepository;
import in.gov.cooptex.core.finance.repository.BudgetTransferNoteRepository;
import in.gov.cooptex.core.finance.repository.BudgetTransferRepository;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.SystemNotification;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.AppQueryRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.SectionMasterRepository;
import in.gov.cooptex.core.repository.SystemNotificationRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.util.JdbcUtil;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.FinanceErrorCode;
import in.gov.cooptex.finance.dto.BudgetReAllocationDTO;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class BudgetReAllocationService {

	@Autowired
	BudgetRequestRepository budgetRequestRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	AppQueryRepository appQueryRepository;

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	SectionMasterRepository sectionMasterRepository;

	@Autowired
	BudgetTransferRepository budgetTransferRepository;

	@Autowired
	GlAccountRepository accountRepository;

	@Autowired
	UserMasterRepository userMasterRepository;

	@Autowired
	BudgetTransferLogRepository budgetTransferLogRepository;

	@Autowired
	BudgetTransferNoteRepository budgetTransferNoteRepository;

	@Autowired
	BudgetTransferDetailsRepository budgetTransferDetailsRepository;

	@Autowired
	SystemNotificationRepository systemNotificationRepository;

	@Autowired
	LoginService loginService;

	private static final String BUDGET_TRANSFER_VIEW_PAGE = "/pages/accounts/budget/viewBudgetTransfer.xhtml?faces-redirect=true";

	@Autowired
	NotificationEmailService notificationEmailService;

	// Budget Transfer Lazy load
	public BaseDTO getAllBudgetReAllocationLazyList(PaginationDTO paginationDTO) {
		log.info("BudgetReAllocationService getAllBudgetReAllocationLazyList() starts---");
		BaseDTO baseDTO = new BaseDTO();
		Integer totalRecords = 0;
		List<BudgetReAllocationDTO> budgetReAllocationDTOList = null;
		try {
			budgetReAllocationDTOList = new ArrayList<>();
			Integer start = paginationDTO.getFirst(), pageSize = paginationDTO.getPageSize();
			start = start * pageSize;
			List<Map<String, Object>> listofData = null;
			List<Map<String, Object>> countData = null;

			ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("BUDGET_TRANSFER_LIST");
			if (applicationQuery == null) {
				log.info("--------BUDGET_TRANSFER_LIST query not found-----");
				return null;
			}
			String mainQuery = applicationQuery.getQueryContent();
			log.info("budget transfer db query----------" + mainQuery);

			UserMaster userMaster = loginService.getCurrentUser();
			Long loginUserId = userMaster == null ? null : userMaster.getId();

			if (StringUtils.isNotEmpty(mainQuery)) {
				mainQuery = StringUtils.replace(mainQuery, ":forwardToUserId", String.valueOf(loginUserId));
			}

			if (paginationDTO.getFilters() != null) {
				if (paginationDTO.getFilters().get("budgetName") != null) {
					mainQuery += " and bc.code='" + paginationDTO.getFilters().get("budgetName") + "'";
				}
				if (paginationDTO.getFilters().get("fromEntity") != null) {
					mainQuery += " and upper(em.name) like upper('%" + paginationDTO.getFilters().get("fromEntity")
							+ "%') ";
				}
				if (paginationDTO.getFilters().get("toEntity") != null) {
					mainQuery += " and upper(em1.name) like upper('%" + paginationDTO.getFilters().get("toEntity")
							+ "%') ";
				}
				if (paginationDTO.getFilters().get("createdDate") != null) {
					Date createdDate = new Date((Long) paginationDTO.getFilters().get("createdDate"));
					mainQuery += " and date(bt.created_date)=date('" + createdDate + "')";
				}
				if (paginationDTO.getFilters().get("stage") != null) {
					mainQuery += " and btl.stage='" + paginationDTO.getFilters().get("stage") + "'";
				}
			}

			ApplicationQuery applicationCountQuery = appQueryRepository.findByQueryName("BUDGET_TRANSFER_LIST_COUNT");

			if (applicationCountQuery == null) {
				log.info("--------BUDGET_TRANSFER_LIST_COUNT query not found-----");
				return null;
			}

			String countQuery = applicationCountQuery.getQueryContent();
			log.info("count query... " + countQuery);
			if (StringUtils.isNotEmpty(countQuery)) {
				countQuery = StringUtils.replace(countQuery, ":forwardToUserId", String.valueOf(loginUserId));
			}
			countData = jdbcTemplate.queryForList(countQuery);

			if (countData != null) {
				for (Map<String, Object> data : countData) {
					if (data.get("count") != null)
						totalRecords = Integer.parseInt(data.get("count").toString().replace(",", ""));
					log.info("budget transfer list count-----" + totalRecords);
				}
			}

			if (paginationDTO.getSortField() == null) {
				mainQuery += " order by bt.id desc limit " + pageSize + " offset " + start + ";";
			}

			if (paginationDTO.getSortField() != null && paginationDTO.getSortOrder() != null) {
				log.info("Sort Field:[" + paginationDTO.getSortField() + "] Sort Order:[" + paginationDTO.getSortOrder()
						+ "]");
				if (paginationDTO.getSortField().equals("budgetName")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by bc.code asc ";
				if (paginationDTO.getSortField().equals("budgetName")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by bc.code desc ";

				if (paginationDTO.getSortField().equals("fromEntity")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by em.name asc  ";
				if (paginationDTO.getSortField().equals("fromEntity")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by em.name desc  ";

				if (paginationDTO.getSortField().equals("toEntity") && paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by em1.name asc  ";
				if (paginationDTO.getSortField().equals("toEntity")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by em1.name desc  ";

				if (paginationDTO.getSortField().equals("createdDate")
						&& paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by bt.created_date asc ";
				if (paginationDTO.getSortField().equals("createdDate")
						&& paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by bt.created_date desc ";

				if (paginationDTO.getSortField().equals("stage") && paginationDTO.getSortOrder().equals("ASCENDING"))
					mainQuery += " order by btl.stage asc ";
				if (paginationDTO.getSortField().equals("stage") && paginationDTO.getSortOrder().equals("DESCENDING"))
					mainQuery += " order by btl.stage desc ";

				mainQuery += " limit " + pageSize + " offset " + start + ";";
			}
			log.info("budget transfer main query--------" + mainQuery);

			listofData = jdbcTemplate.queryForList(mainQuery);

			if (listofData != null && !listofData.isEmpty()) {
				log.info("budgetTransfer list size-------------->" + listofData.size());
				for (Map<String, Object> data : listofData) {
					BudgetReAllocationDTO dto = new BudgetReAllocationDTO();
					dto.setId(Long.valueOf(data.get("id").toString()));
					dto.setCreatedDate(AppUtil.REPORT_DATE_FORMAT.parse(data.get("createdDate").toString()));
					dto.setBudgetName(data.get("budgetCode").toString() + "/" + data.get("budgetName").toString());
					dto.setFromEntity(data.get("fromName").toString());
					dto.setToEntity(data.get("toName").toString());
					dto.setStage(data.get("stage").toString());
					dto.setCreatedBy(
							data.get("createdBy") != null ? Long.valueOf(data.get("createdBy").toString()) : null);
					budgetReAllocationDTOList.add(dto);
				}
			}
			baseDTO.setTotalRecords(totalRecords);
			baseDTO.setResponseContents(budgetReAllocationDTOList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());

		} catch (Exception e) {
			log.error("getAllBudgetReAllocationLazyList exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("BudgetReAllocationService getAllBudgetReAllocationLazyList() starts---");
		return baseDTO;
	}

	// Get BudgetConfig values from BudgetRequest
	public BaseDTO getBudgetConfig() {
		BaseDTO baseDTO = new BaseDTO();
		List<BudgetRequest> budgetRequestList = null;
		List<Map<String, Object>> listofData = null;
		//
		try {
			budgetRequestList = new ArrayList<>();

			ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("BUDGET_CONFIG_LIST");
			if (applicationQuery == null) {
				log.info("--------BUDGET_CONFIG_LIST query not found-----");
				return null;
			}

			String mainQuery = applicationQuery.getQueryContent();
			log.info("database Query-----" + mainQuery);

			UserMaster currentuser = loginService.getCurrentUser();
			if (currentuser != null) {
				listofData = jdbcTemplate.queryForList(mainQuery,
						new Object[] { currentuser.getId(), currentuser.getId() });
			}

			int listofDataSize = listofData != null ? listofData.size() : 0;

			log.info("listofDataSize -----" + listofDataSize);

			// listofData = jdbcTemplate.queryForList(mainQuery);

			if (listofDataSize > 0) {
				log.info("listofData size------------" + listofData.size());
				for (Map<String, Object> budget : listofData) {
					BudgetRequest budgetRequest = new BudgetRequest();
					budgetRequest.setId(Long.valueOf(budget.get("id").toString()));
					budgetRequest.setBudgetConfig(new BudgetConfig());
					budgetRequest.getBudgetConfig().setId(Long.valueOf(budget.get("configId").toString()));
					budgetRequest.getBudgetConfig().setCode(budget.get("code").toString());
					budgetRequest.getBudgetConfig().setName(budget.get("name").toString());
					budgetRequest.getBudgetConfig().setFromMonth(budget.get("fromMonth").toString());
					budgetRequest.getBudgetConfig().setFromYear(Long.valueOf(budget.get("fromYear").toString()));
					budgetRequest.getBudgetConfig().setToMonth(budget.get("toMonth").toString());
					budgetRequest.getBudgetConfig().setToYear(Long.valueOf(budget.get("toYear").toString()));
					budgetRequestList.add(budgetRequest);
				}
			}
			baseDTO.setResponseContents(budgetRequestList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		} catch (Exception e) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			log.error("getBudgetConfig exception---", e);
		}
		return baseDTO;
	}

	private String replaceQueryParam(ApplicationQuery applicationQueryObj,
			BudgetReAllocationDTO budgetReAllocationDTO) {
		String queryContent = applicationQueryObj.getQueryContent();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMM");
		String finalQuery = queryContent.replace(":ENTITYID", budgetReAllocationDTO.getFromEntityId().toString());
		if (budgetReAllocationDTO.getFromSectionId() != null) {
			finalQuery = finalQuery.replace(":SECTIONID", budgetReAllocationDTO.getFromSectionId().toString());
		} else {
			finalQuery = finalQuery.replace(":SECTIONID", "null");
		}
		finalQuery = finalQuery.replace(":FROMDATE", dateFormat.format(budgetReAllocationDTO.getBudgetFromDate()));
		finalQuery = finalQuery.replace(":TODATE", dateFormat.format(budgetReAllocationDTO.getBudgetToDate()));
		finalQuery = finalQuery.replace(":CONFIGID", budgetReAllocationDTO.getBudgetConfigId().toString());
		return finalQuery;
	}

	// Get Records from BudgetAllocation and BudgetTransfer and
	// AccountTransactionDetails
	public BaseDTO generateBudgetReAllocation(BudgetReAllocationDTO budgetReAllocationDTO) {
		log.info("---------generateBudgetReAllocation service starts------");
		BaseDTO baseDTO = new BaseDTO();
		List<Map<String, Object>> listofData = null;
		String query = null;
		List<BudgetReAllocationDTO> fixedExpenseList = new ArrayList<>();
		List<BudgetReAllocationDTO> variableExpenseList = new ArrayList<>();
		try {
			if (budgetReAllocationDTO != null) {

				ApplicationQuery applicationQueryObj = appQueryRepository
						.findByQueryName("CHECK_EXISTING_BUDGET_TRANSFER_IS_IN_PROGRESS_SQL");
				if (applicationQueryObj == null) {
					log.info("---CHECK_EXISTING_BUDGET_TRANSFER_IS_IN_PROGRESS_SQL Application query not found----");
					baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
					return baseDTO;
				}

				String finalQuery = replaceQueryParam(applicationQueryObj, budgetReAllocationDTO);
				Long existInprogressCount = JdbcUtil.getCount(jdbcTemplate, finalQuery, null, "count");
				existInprogressCount = existInprogressCount != null ? existInprogressCount : 0L;

				if (existInprogressCount != null && existInprogressCount.longValue() > 0) {
					baseDTO.setStatusCode(FinanceErrorCode.BUDGET_TRANSFER_EXISTING_IS_INPROGRESS);
					return baseDTO;
				}

				ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("GENERATE_BUDGET_RE_ALLOCATION");
				if (applicationQuery == null) {
					log.info("---GENERATE_BUDGET_RE_ALLOCATION Application query not found----");
					baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
					return baseDTO;
				}
				query = replaceQueryParam(applicationQuery, budgetReAllocationDTO);
				log.info("After value changed query-------" + query);

				listofData = jdbcTemplate.queryForList(query);

				if (listofData != null && !listofData.isEmpty()) {
					log.info("listofData size------------" + listofData.size());
					for (Map<String, Object> budget : listofData) {
						BudgetReAllocationDTO dto = new BudgetReAllocationDTO();
						dto.setGlAccountId(Long.valueOf(budget.get("headId").toString()));
						dto.setHeadOfAccount(budget.get("headName").toString());
						dto.setBudgetEstimation(Double.valueOf(budget.get("budgetAmount").toString()));
						dto.setExpenseAmount(Double.valueOf(budget.get("expenseAmount").toString()));
						dto.setTransferedAmount(Double.valueOf(budget.get("transferedAmount").toString()));
						dto.setBalanceAmount(
								(dto.getBudgetEstimation() - dto.getExpenseAmount()) - dto.getTransferedAmount());
						if (budget.get("expenseCategory") != null) {
							dto.setExpenseCategory(budget.get("expenseCategory").toString());
							log.info("expense category--------------" + dto.getExpenseCategory());
						}
						if ("FIXED".equalsIgnoreCase(dto.getExpenseCategory())) {
							fixedExpenseList.add(dto);
						} else if ("VARIABLE".equalsIgnoreCase(dto.getExpenseCategory())) {
							variableExpenseList.add(dto);
						}
					}
				}
				budgetReAllocationDTO.setFixedBudgetDTOList(fixedExpenseList);
				budgetReAllocationDTO.setVariableBudgetDTOList(variableExpenseList);
				baseDTO.setResponseContent(budgetReAllocationDTO);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			}
		} catch (Exception e) {
			log.error("generateBudgetReAllocation exception------>", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("---------generateBudgetReAllocation service starts------");
		return baseDTO;
	}

	// Save or Update BudgetReAllocation
	@Transactional
	public BaseDTO saveOrUpdate(BudgetReAllocationDTO budgetReAllocationDTO) {
		log.info("----starts saveOrUpdate method------");
		List<BudgetReAllocationDTO> budgetReAllocationDTOList = new ArrayList<>();
		List<BudgetTransferDetails> budgetTransferDetailList = new ArrayList<>();
		BaseDTO baseDTO = new BaseDTO();
		try {
			BudgetTransferNote budgetTransferNote = null;
			BudgetTransfer budgetTransfer = null;
			BudgetTransferLog budgetTransferLog = null;
			if (budgetReAllocationDTO != null) {
				budgetReAllocationDTOList.addAll(budgetReAllocationDTO.getFixedBudgetDTOList());
				budgetReAllocationDTOList.addAll(budgetReAllocationDTO.getVariableBudgetDTOList());
				if (budgetReAllocationDTO.getId() == null) {
					log.info("-----------Inside Save Condition----------");
					budgetTransfer = new BudgetTransfer();
					budgetTransfer.setBudgetRequest(
							budgetRequestRepository.findOne(budgetReAllocationDTO.getBudgetRequestId()));
					budgetTransfer.setFromEntityMaster(
							entityMasterRepository.findById(budgetReAllocationDTO.getFromEntityId()));
					budgetTransfer
							.setToEntityMaster(entityMasterRepository.findById(budgetReAllocationDTO.getToEntityId()));
					if (budgetReAllocationDTO.getFromSectionId() != null) {
						budgetTransfer.setFromSectionMaster(
								sectionMasterRepository.findOne(budgetReAllocationDTO.getFromSectionId()));
					}
					if (budgetReAllocationDTO.getToSectionId() != null) {
						budgetTransfer.setToSectionMaster(
								sectionMasterRepository.findOne(budgetReAllocationDTO.getToSectionId()));
					}

					if (budgetReAllocationDTOList != null) {
						for (BudgetReAllocationDTO dto : budgetReAllocationDTOList) {
							if (dto.getTransferAmount() != null && dto.getTransferAmount().doubleValue() > 0D) {
								BudgetTransferDetails budgetTransferDetails = new BudgetTransferDetails();
								budgetTransferDetails.setBudgetTransfer(budgetTransfer);
								if (dto.getGlAccountId() != null) {
									budgetTransferDetails
											.setGlAccount(accountRepository.findById(dto.getGlAccountId()));
								}
								budgetTransferDetails.setTransferAmount(
										(dto.getTransferAmount() != null ? dto.getTransferAmount() : 0.0)
												+ (dto.getTransferedAmount() != null ? dto.getTransferedAmount()
														: 0.0));
								budgetTransferDetailList.add(budgetTransferDetails);
							}
						}
					}
					budgetTransfer.setBudgetTransferDetailList(budgetTransferDetailList);

					budgetTransferLog = new BudgetTransferLog();
					budgetTransferLog.setBudgetTransfer(budgetTransfer);
					budgetTransferLog.setStage("SUBMITTED");
					budgetTransfer.getBudgetTransferLogList().add(budgetTransferLog);

					budgetTransferNote = new BudgetTransferNote();
					budgetTransferNote.setBudgetTransfer(budgetTransfer);
					budgetTransferNote.setFinalApproval(budgetReAllocationDTO.getFinalApproval());
					budgetTransferNote
							.setForwardTo(userMasterRepository.findOne(budgetReAllocationDTO.getUserMaster().getId()));
					budgetTransferNote.setNote(budgetReAllocationDTO.getNote());
					budgetTransfer.getBudgetTransferNoteList().add(budgetTransferNote);

					budgetTransfer = budgetTransferRepository.save(budgetTransfer);
					log.info("-----budgetTransfer saved successfully-----");
				} else {
					log.info("-----------Inside Update Condition----------");
					budgetTransfer = budgetTransferRepository.getOne(budgetReAllocationDTO.getId());
					if (budgetTransfer != null) {
						if (budgetReAllocationDTOList != null) {
							for (BudgetReAllocationDTO dto : budgetReAllocationDTOList) {
								if (dto.getTransferAmount() != null && dto.getTransferAmount().doubleValue() > 0D) {
									BudgetTransferDetails budgetTransferDetails = budgetTransferDetailsRepository
											.findByTransferIdAndGlId(budgetReAllocationDTO.getId(),
													dto.getGlAccountId());
									budgetTransferDetails.setBudgetTransfer(budgetTransfer);
									if (dto.getGlAccountId() != null) {
										budgetTransferDetails
												.setGlAccount(accountRepository.findById(dto.getGlAccountId()));
									}
									budgetTransferDetails.setTransferAmount(
											(dto.getTransferAmount() != null ? dto.getTransferAmount() : 0.0)
													+ (dto.getTransferedAmount() != null ? dto.getTransferedAmount()
															: 0.0));
									budgetTransferDetailList.add(budgetTransferDetails);
								}
							}
							budgetTransferDetailsRepository.save(budgetTransferDetailList);
						}

						budgetTransferLog = new BudgetTransferLog();
						budgetTransferLog.setBudgetTransfer(budgetTransfer);
						budgetTransferLog.setStage("SUBMITTED");
						budgetTransferLogRepository.save(budgetTransferLog);

						budgetTransferNote = new BudgetTransferNote();
						budgetTransferNote.setBudgetTransfer(budgetTransfer);
						budgetTransferNote.setFinalApproval(budgetReAllocationDTO.getFinalApproval());
						budgetTransferNote.setForwardTo(
								userMasterRepository.findOne(budgetReAllocationDTO.getUserMaster().getId()));
						budgetTransferNote.setNote(budgetReAllocationDTO.getNote());
						budgetTransferNoteRepository.save(budgetTransferNote);

					}
				}
				/*
				 * Send Notification Mail
				 */

				if (budgetTransfer != null && budgetTransfer.getId() != null) {
					Map<String, Object> additionalData = new HashMap<>();
					additionalData.put("Url",
							BUDGET_TRANSFER_VIEW_PAGE + "&budgetTransferId=" + budgetTransfer.getId() + "&");
					notificationEmailService.sendMailAndNotificationForForward(budgetTransferNote, budgetTransferLog,
							additionalData);
				}
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			}
		} catch (Exception e) {
			log.error("saveOrUpdate exception--------", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("----ends saveOrUpdate method------");
		return baseDTO;
	}

	// Get Budget Transfer data by budget_transfer_id
	public BaseDTO getBudgetTransfer(BudgetReAllocationDTO budgetReAllocationDTO) {
		log.info("-------getBudgetTransfer starts---------");
		BaseDTO baseDTO = new BaseDTO();
		List<Map<String, Object>> listofData = null;
		String query = null;
		List<BudgetReAllocationDTO> fixedExpenseList = new ArrayList<>();
		List<BudgetReAllocationDTO> variableExpenseList = new ArrayList<>();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMM");
		SimpleDateFormat formatter = new SimpleDateFormat("MM/yyyy");
		BudgetReAllocationDTO allocationDTO = new BudgetReAllocationDTO();
		try {
			if (budgetReAllocationDTO != null) {
				Long transferId = budgetReAllocationDTO.getId();
				Long notificationId = budgetReAllocationDTO.getNotificationId();
				BudgetTransfer budgetTransfer = budgetTransferRepository.getOne(transferId);
				
				if (budgetTransfer != null) {
					//log.info("budget transfer obj---------" + budgetTransfer);

					allocationDTO.setBudgetName(budgetTransfer.getBudgetRequest().getBudgetConfig().getCode() + "/"
							+ budgetTransfer.getBudgetRequest().getBudgetConfig().getName());
					allocationDTO.setFromEntity(budgetTransfer.getFromEntityMaster().getName());
					allocationDTO.setToEntity(budgetTransfer.getToEntityMaster().getName());
					if (budgetTransfer.getFromSectionMaster() != null) {
						allocationDTO.setFromSection(budgetTransfer.getFromSectionMaster().getName());
					}
					if (budgetTransfer.getToSectionMaster() != null) {
						allocationDTO.setToSection(budgetTransfer.getToSectionMaster().getName());
					}

					String from = budgetTransfer.getBudgetRequest().getBudgetConfig().getFromMonth() + "/"
							+ budgetTransfer.getBudgetRequest().getBudgetConfig().getFromYear();
					String to = budgetTransfer.getBudgetRequest().getBudgetConfig().getToMonth() + "/"
							+ budgetTransfer.getBudgetRequest().getBudgetConfig().getToYear();

					Date fromDate = formatter.parse(from);
					Date toDate = formatter.parse(to);

					allocationDTO.setBudgetFromDate(fromDate);
					allocationDTO.setBudgetToDate(toDate);

					ApplicationQuery applicationQuery = appQueryRepository.findByQueryName("VIEW_BUDGET_RE_ALLOCATION");
					if (applicationQuery == null) {
						log.info("---VIEW_BUDGET_RE_ALLOCATION Application query not found----");
						return null;
					}
					query = applicationQuery.getQueryContent();

					query = query.replace(":ENTITYID", budgetTransfer.getFromEntityMaster().getId().toString());
					if (budgetTransfer.getFromSectionMaster() != null) {
						query = query.replace(":SECTIONID", budgetTransfer.getFromSectionMaster().getId().toString());
					} else {
						query = query.replace(":SECTIONID", "null");
					}

					query = query.replace(":FROMDATE", dateFormat.format(fromDate));
					query = query.replace(":TODATE", dateFormat.format(toDate));
					query = query.replace(":CONFIGID",
							budgetTransfer.getBudgetRequest().getBudgetConfig().getId().toString());
					query = query.replace(":TRANSFERID", transferId.toString());
					log.info("After value changed query-------" + query);

					listofData = jdbcTemplate.queryForList(query);

					if (listofData != null && !listofData.isEmpty()) {
						log.info("listofData size------------" + listofData.size());
						for (Map<String, Object> budget : listofData) {
							BudgetReAllocationDTO dto = new BudgetReAllocationDTO();
							dto.setGlAccountId(
									budget.get("headId") != null ? Long.valueOf(budget.get("headId").toString())
											: null);
							dto.setHeadOfAccount(
									budget.get("headName") != null ? budget.get("headName").toString() : null);
							dto.setBudgetEstimation(budget.get("budgetAmount") != null
									? Double.valueOf(budget.get("budgetAmount").toString())
									: 0D);
							dto.setExpenseAmount(budget.get("expenseAmount") != null
									? Double.valueOf(budget.get("expenseAmount").toString())
									: 0D);
							dto.setTransferedAmount(budget.get("transferedAmount") != null
									? Double.valueOf(budget.get("transferedAmount").toString())
									: 0D);
							dto.setBalanceAmount(
									(dto.getBudgetEstimation() - dto.getExpenseAmount()) - dto.getTransferedAmount());
							dto.setTransferAmount(budget.get("transferAmount") != null
									? Double.valueOf(budget.get("transferAmount").toString())
									: 0D);
							if (budget.get("expenseCategory") != null) {
								dto.setExpenseCategory(budget.get("expenseCategory").toString());
								log.info("expense category--------------" + dto.getExpenseCategory());
							}
							if ("FIXED".equalsIgnoreCase(dto.getExpenseCategory())) {
								fixedExpenseList.add(dto);
							} else if ("VARIABLE".equalsIgnoreCase(dto.getExpenseCategory())) {
								variableExpenseList.add(dto);
							}
						}
					}
					allocationDTO.setFixedBudgetDTOList(fixedExpenseList);
					allocationDTO.setVariableBudgetDTOList(variableExpenseList);

					// Get object from BudgetTransferLog where budget_transfer_id
					BudgetTransferLog budgetTransferLog = budgetTransferLogRepository.findByTransferID(transferId);
					if (budgetTransferLog != null) {
						log.info("budget transfer log stage---------------->" + budgetTransferLog.getStage());
						allocationDTO.setStage(budgetTransferLog.getStage());
					}

					// Get object from BudgetTransferNote where budget_transfer_id
					BudgetTransferNote budgetTransferNote = budgetTransferNoteRepository.findByTransferID(transferId);
					if (budgetTransferNote != null) {
						log.info("budget transfer note stage---------------->" + budgetTransferNote.getId());
						allocationDTO.setFinalApproval(budgetTransferNote.getFinalApproval());
						allocationDTO.setUserMaster(budgetTransferNote.getForwardTo());
						allocationDTO.setNote(budgetTransferNote.getNote());
						allocationDTO.setId(transferId);
					}

					// Get List from BudgetTransferLog where budget_transfer_id
					List<BudgetTransferLog> budgetTransferLogList = budgetTransferLogRepository
							.getAllByTransferID(transferId);
					if (budgetTransferLogList != null)
						allocationDTO.setBudgetTransferLogList(budgetTransferLogList);

					// Get object from BudgetTransferNote where budget_transfer_id
					List<BudgetTransferNote> budgetTransferNoteList = budgetTransferNoteRepository
							.gettAllByTransferID(transferId);
					if (budgetTransferNoteList != null)
						allocationDTO.setBudgetTransferNoteList(budgetTransferNoteList);
				}

				if (notificationId != null) {
					SystemNotification systemNotification = systemNotificationRepository.findOne(notificationId);
					systemNotification.setNotificationRead(true);
					systemNotificationRepository.save(systemNotification);
				}

				baseDTO.setResponseContent(allocationDTO);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else {
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			}
		} catch (Exception e) {
			log.error("getBudgetTransfer exception-----------", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("-------getBudgetTransfer ends---------");
		return baseDTO;
	}

	// Approve Budget Transfer
	public BaseDTO approveBudgetReAllocation(BudgetReAllocationDTO budgetReAllocationDTO) {
		log.info("approveBudgetReAllocation method starts------");
		BaseDTO baseDTO = new BaseDTO();
		try {
			if (budgetReAllocationDTO.getId() != null) {
				log.info("approveBudgetReAllocation dto id----------" + budgetReAllocationDTO.getId());
				BudgetTransfer budgetTransfer = budgetTransferRepository.getOne(budgetReAllocationDTO.getId());
				if (budgetTransfer != null) {
					log.info("budget transfer id--------" + budgetTransfer);
					BudgetTransferLog budgetTransferLog = new BudgetTransferLog();
					budgetTransferLog.setBudgetTransfer(budgetTransfer);
					budgetTransferLog.setStage(budgetReAllocationDTO.getStage());
					budgetTransferLog.setRemarks(budgetReAllocationDTO.getRemarks());
					budgetTransferLogRepository.save(budgetTransferLog);
					log.info("------budget transfer log saved--------");

					BudgetTransferNote budgetTransferNote = new BudgetTransferNote();
					budgetTransferNote.setBudgetTransfer(budgetTransfer);
					budgetTransferNote.setFinalApproval(budgetReAllocationDTO.getFinalApproval());
					budgetTransferNote
							.setForwardTo(userMasterRepository.findOne(budgetReAllocationDTO.getUserMaster().getId()));
					budgetTransferNote.setNote(budgetReAllocationDTO.getNote());
					budgetTransferNoteRepository.save(budgetTransferNote);
					log.info("------budget transfer note saved--------");

					String urlPath = BUDGET_TRANSFER_VIEW_PAGE + "&budgetTransferId=" + budgetReAllocationDTO.getId()
							+ "&";
					Long toUserId = null;
					String message = null;
					if (ApprovalStage.FINALAPPROVED.equals(budgetReAllocationDTO.getStage())) {
						toUserId = budgetTransfer.getCreatedBy().getId();
						message = "Budget Transfer has been Final Approved";
					} else {
						toUserId = budgetReAllocationDTO.getUserMaster() != null
								? budgetReAllocationDTO.getUserMaster().getId()
								: null;
						message = "Budget Transfer has been Approved";
					}

					notificationEmailService.saveIndividualNotification(loginService.getCurrentUser(), toUserId,
							urlPath, "Budget Transfer", message);

					baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
				}
			} else {
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			}
		} catch (Exception e) {
			log.error("approveBudgetReAllocation exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("approveBudgetReAllocation method ends------");
		return baseDTO;
	}

	// Reject Budget Transfer
	public BaseDTO rejectBudgetReAllocation(BudgetReAllocationDTO budgetReAllocationDTO) {
		log.info("rejectBudgetReAllocation method starts------");
		BaseDTO baseDTO = new BaseDTO();
		try {
			if (budgetReAllocationDTO.getId() != null) {
				log.info("rejectBudgetReAllocation dto id----------" + budgetReAllocationDTO.getId());
				BudgetTransfer budgetTransfer = budgetTransferRepository.getOne(budgetReAllocationDTO.getId());
				if (budgetTransfer != null) {
					log.info("budget transfer id--------" + budgetTransfer);
					BudgetTransferLog budgetTransferLog = new BudgetTransferLog();
					budgetTransferLog.setBudgetTransfer(budgetTransfer);
					budgetTransferLog.setStage(budgetReAllocationDTO.getStage());
					budgetTransferLog.setRemarks(budgetReAllocationDTO.getRemarks());
					budgetTransferLogRepository.save(budgetTransferLog);
					log.info("------budget transfer log saved--------");

					String urlPath = BUDGET_TRANSFER_VIEW_PAGE + "&budgetTransferId=" + budgetReAllocationDTO.getId()
							+ "&";
					Long toUserId = budgetTransferNoteRepository.getCreatedByUserId(budgetReAllocationDTO.getId());
					notificationEmailService.saveIndividualNotification(loginService.getCurrentUser(), toUserId,
							urlPath, "Budget Transfer", "Budget Transfer has been Rejected");

					baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
				}
			} else {
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
			}
		} catch (Exception e) {
			log.error("rejectBudgetReAllocation exception----", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("rejectBudgetReAllocation method ends------");
		return baseDTO;
	}

}
