package in.gov.cooptex.finance.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.repository.BankMasterRepository;
import in.gov.cooptex.common.repository.OrganizationMasterRepository;
import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.util.ResponseWrapper;
import in.gov.cooptex.core.accounts.dto.RegionCodeDropDownDTO;
import in.gov.cooptex.core.accounts.enums.VoucherStatus;
import in.gov.cooptex.core.accounts.enums.VoucherTypeDetails;
import in.gov.cooptex.core.accounts.model.CreditSalesDemand;
import in.gov.cooptex.core.accounts.model.CreditSalesRecovery;
import in.gov.cooptex.core.accounts.model.CreditSalesRequest;
import in.gov.cooptex.core.accounts.model.Payment;
import in.gov.cooptex.core.accounts.model.PaymentDetails;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.VoucherDetails;
import in.gov.cooptex.core.accounts.model.VoucherLog;
import in.gov.cooptex.core.accounts.model.VoucherType;
import in.gov.cooptex.core.accounts.repository.CreditSalesDemandDetailsRepository;
import in.gov.cooptex.core.accounts.repository.CreditSalesDemandLogRepository;
import in.gov.cooptex.core.accounts.repository.CreditSalesDemandNoteRepository;
import in.gov.cooptex.core.accounts.repository.EntityBankBranchRepository;
import in.gov.cooptex.core.accounts.repository.PaymentDetailsRepository;
import in.gov.cooptex.core.accounts.repository.PaymentMethodRepository;
import in.gov.cooptex.core.accounts.repository.PaymentRepository;
import in.gov.cooptex.core.accounts.repository.PaymentTypeMasterRepository;
import in.gov.cooptex.core.accounts.repository.VoucherRepository;
import in.gov.cooptex.core.accounts.repository.VoucherTypeRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.enums.PaymentCategory;
import in.gov.cooptex.core.enums.SequenceName;
import in.gov.cooptex.core.finance.repository.CreditSalesOrgRecoveryRepository;
import in.gov.cooptex.core.finance.service.PaymentFinanceService;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.CustomerMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.OrganizationMaster;
import in.gov.cooptex.core.model.PaymentMode;
import in.gov.cooptex.core.model.SequenceConfig;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.pos.repository.CreditSalesAccountInvoiceRepo;
import in.gov.cooptex.core.pos.repository.CreditSalesDemandRepository;
import in.gov.cooptex.core.pos.repository.CreditSalesRecoveryRepository;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.CreditSalesRequestRepository;
import in.gov.cooptex.core.repository.CustomerMasterRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.PaymentModeRepository;
import in.gov.cooptex.core.repository.SequenceConfigRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.dto.CreditSalesRecoveryDTO;
import in.gov.cooptex.finance.dto.CreditSalesRecoveryRequestDTO;
import in.gov.cooptex.finance.dto.CreditSalesRecoverySearchResponseDto;
import in.gov.cooptex.finance.dto.RecoveryCollectionSearchRequestDto;
import in.gov.cooptex.finance.dto.ViewCreditSalesRecoveryDetailsDTO;
import in.gov.cooptex.finance.model.CreditSalesOrganizationRecovery;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class CreditSalesRecoveryCollectionService {

	@Autowired
	CreditSalesDemandRepository creditSalesDemandRepository;

	@Autowired
	OrganizationMasterRepository organizationMasterRepository;

	@Autowired
	EntityMasterRepository entityMasterRepository;

	@Autowired
	CreditSalesRequestRepository creditSalesRequestRepository;

	@Autowired
	EntityManager entityManager;

	@Autowired
	ResponseWrapper responseWrapper;

	@Autowired
	CreditSalesAccountInvoiceRepo CreditSalesAccountInvoiceRepo;

	@Autowired
	ApplicationQueryRepository applicationQueryRepository;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	CustomerMasterRepository customerMasterRepository;

	@Autowired
	LoginService loginService;

	@Autowired
	EmployeeMasterRepository employeeMasterRepository;

	@Autowired
	CreditSalesDemandLogRepository creditSalesDemandLogRepository;

	@Autowired
	CreditSalesDemandNoteRepository creditSalesDemandNoteRepository;

	@Autowired
	UserMasterRepository userMasterRepository;

	@Autowired
	CreditSalesDemandDetailsRepository creditSalesDemandDetailsRepository;

	@Autowired
	PaymentModeRepository paymentModeRepository;

	@Autowired
	CreditSalesRecoveryRepository creditSalesRecoveryRepository;

	@Autowired
	VoucherTypeRepository voucherTypeRepository;

	@Autowired
	PaymentMethodRepository paymentMethodRepository;

	@Autowired
	SequenceConfigRepository sequenceConfigRepository;

	@Autowired
	VoucherRepository voucherRepository;
/*
	@Autowired
	PaymentModeRepository paymentModeRepo;*/

	@Autowired
	PaymentTypeMasterRepository paymentTypeMasterRepository;

	@Autowired
	PaymentRepository paymentRepository;

	@Autowired
	PaymentDetailsRepository paymentDetailsRepository;

	/*@Autowired
	BillPaymentRepository billPaymentRepository;*/

	@Autowired
	CreditSalesOrgRecoveryRepository creditSalesOrgRecoveryRepository;
	
	@Autowired
	BankMasterRepository bankMasterRepository;
	
	@Autowired
	EntityBankBranchRepository entityBankBranchRepository;
	
	@Autowired
	PaymentFinanceService paymentFinanceService;
	
	public BaseDTO getAllOraganizationMasters(Long creditSalesRequestId) {
		log.info("CreditSalesRecoveryCollectionService getAllOraganizationMasters method started "+creditSalesRequestId);
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<OrganizationMaster> organizationMasters = creditSalesDemandRepository.findAllOrganizationMasters(creditSalesRequestId);
			baseDTO.setResponseContent(organizationMasters);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("CreditSalesRecoveryCollectionService getAllOraganizationMasters RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("CreditSalesRecoveryCollectionService getAllOraganizationMasters Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("CreditSalesRecoveryCollectionService getAllOraganizationMasters method completed");
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO getAllCustomerMasters(Long creditSalesRequestId) {
		log.info("CreditSalesRecoveryCollectionService getAllCustomerMasters method started "+creditSalesRequestId);
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<CustomerMaster> customerMasterList = creditSalesDemandDetailsRepository.findAllCustomerMaster(creditSalesRequestId);
			baseDTO.setResponseContent(customerMasterList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("CreditSalesRecoveryCollectionService getAllCustomers RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("CreditSalesRecoveryCollectionService getAllCustomerMasters Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("CreditSalesRecoveryCollectionService getAllCustomerMasters method completed");
		return responseWrapper.send(baseDTO);
	}


	public BaseDTO getAllCreditSalesRequests() {
		log.info("CreditSalesRecoveryCollectionService getAllCreditSalesRequests method started ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<CreditSalesRequest> creditSalesRequestList = creditSalesDemandRepository.findAllCreditSalesRequest();
			baseDTO.setResponseContent(creditSalesRequestList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("CreditSalesRecoveryCollectionService getAllCreditSalesRequests RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("CreditSalesRecoveryCollectionService getAllCreditSalesRequests Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("CreditSalesRecoveryCollectionService getAllCreditSalesRequests method completed");
		return responseWrapper.send(baseDTO);
	}

	public BaseDTO getRecoveryCollectionSearchMasters() {
		log.info("CreditSalesRecoveryCollectionService getRecoveryCollectionSearchMasters method started ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<CreditSalesRequest> creditSalesRequestList = creditSalesDemandRepository.findAllCreditSalesRequest();
			List<CustomerMaster> customerMasterList = creditSalesDemandDetailsRepository.findAllCustomerMasters();
			List<OrganizationMaster> organizationMasters = creditSalesDemandRepository.findAllOrganizationMasters();
			baseDTO.setResponseContent(customerMasterList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("CreditSalesRecoveryCollectionService getRecoveryCollectionSearchMasters RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("CreditSalesRecoveryCollectionService getRecoveryCollectionSearchMasters Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("CreditSalesRecoveryCollectionService getRecoveryCollectionSearchMasters method completed");
		return responseWrapper.send(baseDTO);
	}

	
	public BaseDTO searchRecoveryCollectionDetails(RecoveryCollectionSearchRequestDto request){
		log.info("CreditSalesRecoveryCollectionService searchRecoveryCollectionDetails method started "+request);
		BaseDTO baseDTO = new BaseDTO();
		try {
			Validate.objectNotNull(request.getCreditSalesType(),ErrorDescription.CREDIT_SALES_TYPE_REQUIRED);
			Validate.objectNotNull(request.getCreditSalesRequest(),ErrorDescription.CREDIT_SALES_PERIOD_REQUIRED);
			Validate.objectNotNull(request.getCreditSalesRequest().getId(),ErrorDescription.CREDIT_SALES_PERIOD_REQUIRED);
			if("CUSTOMER".equalsIgnoreCase(request.getCreditSalesType())) {
				Validate.objectNotNull(request.getCustomerId(),ErrorDescription.RECOVERY_CUSTOMER_REQUIRED);	
			}
			if("ORGANIZATION".equalsIgnoreCase(request.getCreditSalesType())) {
				Validate.objectNotNull(request.getOrganizationId(),ErrorDescription.ORGANIZATION_REQUIRED);	
			}

			List<CreditSalesRecoverySearchResponseDto>  searchResponseList = executeRecoveryCollectionQuery(request);
			
			baseDTO.setResponseContent(searchResponseList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("CreditSalesRecoveryCollectionService searchRecoveryCollectionDetails RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("CreditSalesRecoveryCollectionService searchRecoveryCollectionDetails Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("CreditSalesRecoveryCollectionService searchRecoveryCollectionDetails method completed");
		return responseWrapper.send(baseDTO);
	}

	public List<CreditSalesRecoverySearchResponseDto> executeRecoveryCollectionQuery(
			RecoveryCollectionSearchRequestDto request) {
		log.info("Starts CreditSalesRecoveryCollectionService.executeRecoveryCollectionQuery " + request);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		List<CreditSalesRecoverySearchResponseDto> reoveryDetailsList = new ArrayList<>();
		Double organizationCollectionAmount = 0.0;
		String organizationCollectionPaymentStatus = "";
		try {
			ApplicationQuery applicationQuery = applicationQueryRepository
					.findByQueryName("RECOVERY_DEMAND_DETAILS_QUERY");

			if (applicationQuery == null || applicationQuery.getId() == null) {
				log.info("Application Query not found for query name : RECOVERY_DEMAND_DETAILS_QUERY");
				return null;
			}

			String query = applicationQuery.getQueryContent().trim();
			log.info("Query " + query);

			query = applicationQuery.getQueryContent().trim();
			String organizationId = "null", customerId = "null";
			if (request.getOrganizationId() == null) {
				organizationId = "null";
			} else {
				organizationId = request.getOrganizationId().toString();
			}

			if (request.getCustomerId() == null) {
				customerId = "null";
			} else {
				customerId = request.getCustomerId().toString();
			}

			query = query.replace(":organizationId", organizationId);
			query = query.replace(":customerId", customerId);
			query = query.replace(":creditSalesId", request.getCreditSalesRequest().getId().toString());
			query = query.replace(":month", request.getDemandMonth().toString());
			query = query.replace(":year", request.getDemandYear().toString());

			log.info("after replace Query==>" + query);
			log.info("<<<<:::::Value:::demand Month:>>>>>" + request.getDemandMonth() + ":Demand Year:::::"
					+ request.getDemandYear() + "::::CreditSalesDemand:::>>" + request.getCreditSalesDemandId());

			
			if(request.getCreditSalesDemandId()==null) {
				log.info("inside If condition-->"+creditSalesDemandDetailsRepository.getCreditSalesDemandId(
						request.getCustomerId(), request.getDemandYear(), request.getDemandMonth().toString(),
						request.getCreditSalesRequest().getId()));
				request.setCreditSalesDemandId(creditSalesDemandDetailsRepository.getCreditSalesDemandId(
						request.getCustomerId(), request.getDemandYear(), request.getDemandMonth().toString(),
						request.getCreditSalesRequest().getId()));
			}
			
			List<Object[]> organizationCollectionObj = creditSalesOrgRecoveryRepository
					.loadCreditSalesOrgRecoveryByMonthYear(request.getDemandMonth(), request.getDemandYear(),
							request.getCreditSalesDemandId());

			log.info("==========orgCollection Amount :::::::===>" + organizationCollectionObj.size());
			if (organizationCollectionObj.size() > 0) {
				for(Object obj[]:organizationCollectionObj) {
					log.info("organizationCollectionAmount:::>"+new Double(obj[0].toString()));
					organizationCollectionAmount=new Double(obj[0].toString());
					log.info("organizationCollectionPaymentStatus:::>"+(String) obj[1]);
					organizationCollectionPaymentStatus=(String) obj[1];
				}
			} else {
				log.info("::::::organizationCollectionObj is null:::::::::");
			}
			log.info("<<<<:::::::::organizationCollectionAmount ::::::::>>>>" + organizationCollectionAmount
					+ "===organizationCollectionPaymentStatus=======>" + organizationCollectionPaymentStatus);

			reoveryDetailsList = jdbcTemplate.query(query, new RowMapper<CreditSalesRecoverySearchResponseDto>() {

				@Override
				public CreditSalesRecoverySearchResponseDto mapRow(ResultSet rs, int no) throws SQLException {
					CreditSalesRecoverySearchResponseDto dto = new CreditSalesRecoverySearchResponseDto();
					dto.setCustomerCode(rs.getString("customerCode"));
					dto.setCustomerName(rs.getString("customerName"));
					dto.setCustomerId(rs.getLong("customerId"));
					dto.setCreditSalesDemandId(rs.getLong("creditSalesDemandId"));
					log.info(":::value::::salesDemandId:>>>" + dto.getCreditSalesDemandId() + "::customer ID::>"
							+ dto.getCustomerId() + "::month:>>"
							+ Long.valueOf(new DecimalFormat("00").format(request.getDemandMonth())) + "::year>>>"
							+ request.getDemandYear());
					Double amountToBeCollected = creditSalesRecoveryRepository
							.getcollectionAmountByMonthYeardemandIdCustId(dto.getCreditSalesDemandId(),
									dto.getCustomerId(), request.getDemandYear(), request.getDemandMonth());
					dto.setAmountTobeCollected(amountToBeCollected != null ? amountToBeCollected : 0.0);
					log.info("::AmountToBeCollected:::>>>" + dto.getAmountCollected());
					dto.setDesignationName(rs.getString("customerDesignation"));
					dto.setEmpCode(rs.getString("customerEmpCode"));
					dto.setSanctionOrderNumber(rs.getDouble("sanctionOrderNumber"));

					dto.setTotalPurchaseAmount(rs.getDouble("totalPurchaseAmount"));
					dto.setMonthlyInstallmentAmount(rs.getDouble("monthlyInstallmentAmount"));
					dto.setNoOfInstallment(rs.getLong("noOfInstallment"));

					dto.setCurrentRecoveryAmount(rs.getDouble("current_recovery"));
					String yearMonth = request.getDemandYear() + ""
							+ new DecimalFormat("00").format(request.getDemandMonth());
					log.info("::yearMonth:::>>>" + yearMonth);
					dto.setDueAmount(creditSalesRecoveryRepository.getDueAmountbyMonthYear(dto.getCreditSalesDemandId(),
							dto.getCustomerId(), yearMonth));
					log.info("::due Amount:::>>>" + dto.getDueAmount());

					dto.setBalanceAmount(rs.getDouble("balanceAmount"));
					dto.setAmountCollected(rs.getDouble("collectedAmt"));
					dto.setDemandAmount(rs.getDouble("demandAmt"));
					List<CreditSalesRecoveryDTO> recoveryHistoryList = executeRecoveryCollectionHistoryQuery(
							rs.getLong("creditSalesDemandId"), null);
					dto.setCollectionRecoveryHistory(recoveryHistoryList);
					return dto;
				}
			});
			log.info(
					"Ends CreditSalesRecoveryCollectionService.executeDemandDetailsQuery " + reoveryDetailsList.size());
			if (organizationCollectionAmount !=null && organizationCollectionAmount !=0.0) {
				reoveryDetailsList.get(0).setOrganizationCollectionAmount(organizationCollectionAmount);
				reoveryDetailsList.get(0).setPaymentStatus(organizationCollectionPaymentStatus);
			}
			
		} catch (Exception e) {
			log.error("::::CreditSalesRecoveryCollectionService.executeDemandDetailsQuery :::>>>>>", e);
		}
		return reoveryDetailsList;
	}


	public List<CreditSalesRecoveryDTO> executeRecoveryCollectionHistoryQuery(Long creditSalesDemandId,Long customerId){
		log.info("Starts CreditSalesRecoveryCollectionService.executeRecoveryCollectionHistoryQuery "+creditSalesDemandId);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		ApplicationQuery applicationQuery = applicationQueryRepository.findByQueryName("CREDIT_SALES_RECOVERY_HISTORY_QUERY");

		if (applicationQuery == null || applicationQuery.getId() == null) {
			log.info("Application Query not found for query name : " + applicationQuery.getQueryName());
			return null;
		}

		String query = applicationQuery.getQueryContent().trim();

		log.info("Query " + query);

		query = applicationQuery.getQueryContent().trim();

		if(creditSalesDemandId != null) 
			query = query.replace(":DEMANDID" ," and credit_sales_demand_id="+creditSalesDemandId);
		else
			query = query.replace(":DEMANDID" ,"");

		if(customerId != null)
			query = query.replace(":CUSTOMERID" ," and customer_id="+customerId);
		else
			query = query.replace(":CUSTOMERID" ,"");

		log.info("after replace Query==>" + query);

		List<CreditSalesRecoveryDTO>  reoveryDetailsList = jdbcTemplate.query(query, new RowMapper<CreditSalesRecoveryDTO>() {

			@Override
			public CreditSalesRecoveryDTO mapRow(ResultSet rs, int no) throws SQLException {
				CreditSalesRecoveryDTO dto = new CreditSalesRecoveryDTO();
				dto.setDemandAmount(rs.getDouble("demandAmount"));
				dto.setCollectedAmount(rs.getDouble("collectionAmount"));
				
				dto.setBalanceAmount(rs.getDouble("balanceAmount"));
				dto.setCreditDemandId(rs.getLong("demandId"));

				dto.setMonth(rs.getString("demandMonth"));
				dto.setDemandYear(rs.getLong("demandYear"));

				return dto;
			}
		});

		log.info("Ends CreditSalesRecoveryCollectionService.executeRecoveryCollectionHistoryQuery ");

		return reoveryDetailsList;
	}

	@Transactional
	public BaseDTO  saveCreditRecovery(CreditSalesRecoveryRequestDTO request)
	{
		BaseDTO  response  = new BaseDTO ();
		log.info("Start CreditSalesRecoveryCollectionService.Save credit recovery collection ====>>>>");

		List<CreditSalesRecoverySearchResponseDto> collectionRecoveryList = request.getCreditSalesRecoveryResponseList(); 
		try {
			Validate.objectNotNull(request.getTotalRecoveryAmount(),ErrorDescription.TOTAL_RECOVERY_AMOUNT);
			Validate.objectNotNull(request.getPaymentMode(),ErrorDescription.MODE_OF_RECEIPT_REQUIRED);
//			Validate.objectNotNull(request.getPaymentMode().getId(),ErrorDescription.MODE_OF_RECEIPT_REQUIRED);
			if(!"CASH".equalsIgnoreCase(request.getPaymentMode().getCode()) && !"ADJUSTMENT".equalsIgnoreCase(request.getPaymentMode().getCode())) {
				Validate.objectNotNull(request.getBankMaster(),ErrorDescription.BANK_NAME_REQUIRED);
				Validate.objectNotNull(request.getBankMaster().getId(),ErrorDescription.BANK_NAME_REQUIRED);
				Validate.objectNotNull(request.getReferenceNumber(),ErrorDescription.REFERENCE_NUMBER_REQUIRED);
				Validate.objectNotNull(request.getReferenceDate(),ErrorDescription.REFERENCE_DATE_REQUIRED);
				Validate.objectNotNull(request.getReferenceAmount(),ErrorDescription.REFERENCE_AMOUNT_REQUIRED);
			}
			if(collectionRecoveryList == null || collectionRecoveryList.size() == 0)
				throw new RestException(ErrorDescription.RECOVERY_LIST_REQUIRED);
			EntityMaster loginUserEntity = entityMasterRepository.findByUserRegion(loginService.getCurrentUser().getId());
			
			if (request.getTotalRecoveryAmount() > 0.0) {
				log.info("<<<::::::TotalRecoveryAmount:::::>>>"+request.getTotalRecoveryAmount());
				SequenceConfig sequenceConfig = sequenceConfigRepository.findBySequenceName(
						SequenceName.valueOf(SequenceName.CREDIT_SALES_RECOVERY_COLLECTION_NUMBER.name()));
				if (sequenceConfig == null) {
					throw new RestException(ErrorDescription.SEQUENCE_CONFIG_NOT_EXIST);
				}else {
					log.info("::::sequenceConfig current Value::::::"+sequenceConfig.getCurrentValue());
				}
				String paymentNumberPrefix = loginUserEntity.getCode() + sequenceConfig.getSeparator()
						+ sequenceConfig.getPrefix() + AppUtil.getCurrentMonthString() + AppUtil.getCurrentYearString();

				Voucher voucher = new Voucher();
				voucher.setName(VoucherTypeDetails.CREDIT_COLLECTION.toString());
				voucher.setNarration(VoucherTypeDetails.CREDIT_COLLECTION.toString());
				voucher.setReferenceNumberPrefix(paymentNumberPrefix);
				voucher.setReferenceNumber(sequenceConfig.getCurrentValue());
				VoucherType voucherType = voucherTypeRepository.findByName(VoucherTypeDetails.Receipt.toString());
				voucher.setVoucherType(voucherType);

				voucher.setNetAmount(request.getTotalRecoveryAmount());
				voucher.setPaymentFor(VoucherTypeDetails.CREDIT_COLLECTION.toString());
				voucher.setFromDate(new Date());
				voucher.setToDate(new Date());
				VoucherDetails voucherDet = new VoucherDetails();
				voucherDet.setAmount(request.getTotalRecoveryAmount());
				voucherDet.setVoucher(voucher);
				List<VoucherDetails> voucherDetList = new ArrayList<>();
				voucherDetList.add(voucherDet);
				voucher.setVoucherDetailsList(voucherDetList);

				VoucherLog voucherLog = new VoucherLog();
				voucherLog.setVoucher(voucher);
				voucherLog.setStatus(VoucherStatus.FINALAPPROVED);
				UserMaster userMaster = userMasterRepository.findOne(loginService.getCurrentUser().getId());
				voucherLog.setUserMaster(userMaster);
				voucher.getVoucherLogList().add(voucherLog);
				voucher.setPaymentMode(paymentModeRepository.findByCode(request.getPaymentMode().getCode()));
				// Voucher voucherObj = voucherRepository.save(voucher);

				Payment payment = new Payment();
				if (loginUserEntity == null) {
					log.info("Login User Info Not Available");
					throw new RestException(ErrorDescription.LOGIN_USER_INFO_NOT_AVAILABLE);
				}
				payment.setEntityMaster(loginUserEntity);
				log.info("loginUserEntity ", loginUserEntity);
				payment.setPaymentNumberPrefix(paymentNumberPrefix);
				payment.setPaymentNumber(sequenceConfig.getCurrentValue());

				PaymentDetails paymentDetails = new PaymentDetails();
				paymentDetails.setPayment(payment);
				paymentDetails.setPaymentCategory(PaymentCategory.PAID_IN);
				if (!request.getPaymentMode().getCode().equalsIgnoreCase("Cash") && !"ADJUSTMENT".equalsIgnoreCase(request.getPaymentMode().getCode())) {
					paymentDetails.setBankReferenceNumber(request.getReferenceNumber().toString());
					paymentDetails.setBankMaster(bankMasterRepository.findOne(request.getBankMaster().getId()));
					paymentDetails.setEntityBankBranch(entityBankBranchRepository.getOne(request.getEntityBankBranch().getId()));					
					paymentDetails.setDocumentDate(request.getReferenceDate());
				}
				paymentDetails.setVoucher(voucher);
				paymentDetails.setPaymentTypeMaster(paymentTypeMasterRepository.getCustomerPaymentType());
				paymentDetails.setPaymentMethod(
						paymentMethodRepository.findByCode(request.getPaymentMode().getCode()));
				// paymentDetails.setPaidBy(userMasterRepository.findOne(request.getEmployeeMaster().getUserId()));

				sequenceConfig.setCurrentValue(sequenceConfig.getCurrentValue() + 1);
				sequenceConfigRepository.save(sequenceConfig);
				voucher = voucherRepository.save(voucher);
				paymentRepository.save(payment);
				paymentDetailsRepository.save(paymentDetails);

				List<CreditSalesRecovery> creditSalesRecoveryList = new ArrayList<>();
				for (CreditSalesRecoverySearchResponseDto dto : collectionRecoveryList) {
					/*
					 * if(dto.getAmountTobeCollected() == null || dto.getAmountTobeCollected()==0) {
					 * throw new RestException(ErrorDescription.RECOVERY_AMOUNT_REQUIRED); }
					 */
					CreditSalesRecovery recovery = new CreditSalesRecovery();
					CustomerMaster customer = customerMasterRepository.findOne(dto.getCustomerId());
					CreditSalesDemand creditSalesDemand = creditSalesDemandRepository
							.findOne(dto.getCreditSalesDemandId());
					recovery.setCreditSalesDemand(creditSalesDemand);
					recovery.setVoucher(voucher);
					PaymentMode paymentMode = paymentModeRepository
							.findByPaymentMode(request.getPaymentMode().getCode());
					recovery.setPaymentMode(paymentMode);

					List<CreditSalesRecovery> recoveryList = calculatePrevoiusDemandDetails(customer, creditSalesDemand,
							paymentMode, dto.getAmountTobeCollected(), recovery);

					creditSalesRecoveryList.addAll(recoveryList);

				}
				creditSalesRecoveryRepository.save(creditSalesRecoveryList);
				if (voucherLog.getStatus().equals(VoucherStatus.FINALAPPROVED)) {
					log.info("::::payment Id:::::"+payment.getId());
					paymentFinanceService.creditCollectionSplitAmountPosting(voucher, null ,payment.getId());
				}
			}
			log.info("::::Demand Month::::"+request.getDemandMonth()+"::::Demand Year::::"+request.getDemandYear()+":::CreditSalesDemandId:::"+
							collectionRecoveryList.get(0).getCreditSalesDemandId());
			
			Double creditSalesOrganizationRecoveryAmount = creditSalesOrgRecoveryRepository
					.getCreditSalesOrgRecoveryByMonthYear(request.getDemandMonth(), request.getDemandYear(),
							collectionRecoveryList.get(0).getCreditSalesDemandId());
			log.info("<<<<<:::::::creditSalesOrganizationRecovery:::::::>>>>"+creditSalesOrganizationRecoveryAmount);
			if (creditSalesOrganizationRecoveryAmount == null) {
				log.info("<<<:::::Inside creditSalesOrgRecovery For this month:::::::>>>");
				SequenceConfig sequenceConfig = sequenceConfigRepository.findBySequenceName(
						SequenceName.valueOf(SequenceName.CREDIT_SALES_RECOVERY_COLLECTION_NUMBER.name()));
				if (sequenceConfig == null) {
					throw new RestException(ErrorDescription.SEQUENCE_CONFIG_NOT_EXIST);
				}else {
					log.info("::::sequenceConfig current Value::::::"+sequenceConfig.getCurrentValue());
				}
				String paymentNumberPrefix = loginUserEntity.getCode() + sequenceConfig.getSeparator()
						+ sequenceConfig.getPrefix() + AppUtil.getCurrentMonthString() + AppUtil.getCurrentYearString();

				Voucher voucherForOrganizationRecovery = new Voucher();
				voucherForOrganizationRecovery.setName(VoucherTypeDetails.CREDIT_COLLECTION.toString());
				voucherForOrganizationRecovery.setNarration(VoucherTypeDetails.CREDIT_COLLECTION.toString());
				voucherForOrganizationRecovery.setReferenceNumberPrefix(paymentNumberPrefix);
				voucherForOrganizationRecovery.setReferenceNumber(sequenceConfig.getCurrentValue());
				VoucherType voucherType = voucherTypeRepository.findByName(VoucherTypeDetails.Receipt.toString());
				voucherForOrganizationRecovery.setVoucherType(voucherType);

				voucherForOrganizationRecovery.setNetAmount(request.getOrganizationCollectionAmount());
				voucherForOrganizationRecovery.setPaymentFor(VoucherTypeDetails.CREDIT_COLLECTION.toString());
				voucherForOrganizationRecovery.setFromDate(new Date());
				voucherForOrganizationRecovery.setToDate(new Date());
				VoucherDetails voucherDet = new VoucherDetails();
				voucherDet.setAmount(request.getOrganizationCollectionAmount());
				voucherDet.setVoucher(voucherForOrganizationRecovery);
				List<VoucherDetails> voucherDetList=new ArrayList<>();
				voucherDetList.add(voucherDet);
				voucherForOrganizationRecovery.setVoucherDetailsList(voucherDetList);

				VoucherLog voucherLog = new VoucherLog();
				voucherLog.setVoucher(voucherForOrganizationRecovery);
				voucherLog.setStatus(VoucherStatus.FINALAPPROVED);
				UserMaster userMaster = userMasterRepository.findOne(loginService.getCurrentUser().getId());
				voucherLog.setUserMaster(userMaster);
				voucherForOrganizationRecovery.getVoucherLogList().add(voucherLog); 
				voucherForOrganizationRecovery.setPaymentMode(paymentModeRepository.findByCode(request.getPaymentMode().getCode()));
				
				
				Payment payment = new Payment();
				if (loginUserEntity == null) {
					log.info("Login User Info Not Available");
					throw new RestException(ErrorDescription.LOGIN_USER_INFO_NOT_AVAILABLE);
				}
				payment.setEntityMaster(loginUserEntity);
				log.info("loginUserEntity ", loginUserEntity);
				payment.setPaymentNumberPrefix(paymentNumberPrefix);
				payment.setPaymentNumber(sequenceConfig.getCurrentValue());

				PaymentDetails paymentDetails = new PaymentDetails();
				paymentDetails.setPayment(payment);
				paymentDetails.setPaymentCategory(PaymentCategory.PAID_IN);
				if (!request.getPaymentMode().getCode().equalsIgnoreCase("CASH") && !request.getPaymentMode().getCode().equalsIgnoreCase("ADJUSTMENT")) {
					paymentDetails.setBankReferenceNumber(request.getReferenceNumber().toString());
					paymentDetails.setDocumentDate(request.getReferenceDate());
				}
				paymentDetails.setVoucher(voucherForOrganizationRecovery);
				paymentDetails.setPaymentTypeMaster(paymentTypeMasterRepository.getCustomerPaymentType());
				paymentDetails.setPaymentMethod(
						paymentMethodRepository.findByCode(request.getPaymentMode().getCode()));
				// paymentDetails.setPaidBy(userMasterRepository.findOne(request.getEmployeeMaster().getUserId()));
				

				sequenceConfig.setCurrentValue(sequenceConfig.getCurrentValue() + 1);
				sequenceConfigRepository.save(sequenceConfig);
				voucherForOrganizationRecovery = voucherRepository.save(voucherForOrganizationRecovery);
				paymentRepository.save(payment);
				paymentDetailsRepository.save(paymentDetails);
				
				CustomerMaster customer = customerMasterRepository.findOne(collectionRecoveryList.get(0).getCustomerId());
				CreditSalesDemand creditSalesDemand=creditSalesDemandRepository.findOne(collectionRecoveryList.get(0).getCreditSalesDemandId());
				CreditSalesOrganizationRecovery creditSalesOrgRecoveryObj=new CreditSalesOrganizationRecovery();
				creditSalesOrgRecoveryObj.setDemandMonth(request.getDemandMonth().toString());
				creditSalesOrgRecoveryObj.setDemandYear(Double.parseDouble(request.getDemandYear().toString()));
				creditSalesOrgRecoveryObj.setCreditSalesDemand(creditSalesDemand);
				creditSalesOrgRecoveryObj.setCustomerMaster(customer);
				creditSalesOrgRecoveryObj.setCollectionAmount(request.getOrganizationCollectionAmount());	
				creditSalesOrgRecoveryObj.setVoucher(voucherForOrganizationRecovery);
				creditSalesOrgRecoveryRepository.save(creditSalesOrgRecoveryObj);
			}
			else {
				log.info("<<<:::::creditSalesOrgRecovery Already Entered For this month:::::::>>>");
			}
			response.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
		}catch (RestException restException) {
			log.error("CreditSalesRecoveryCollectionService saveCreditRecovery RestException ", restException);
			response.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("CreditSalesRecoveryCollectionService saveCreditRecovery Exception ", exception);
			response.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("End CreditSalesRecoveryCollectionService.saveCreditRecovery ====>>>>");

		return response;
	}


	public List<CreditSalesRecovery> calculatePrevoiusDemandDetails(CustomerMaster customer, CreditSalesDemand demand,
			PaymentMode payMode, Double collcetionAmt, CreditSalesRecovery recovery) {
		log.info("<<<<:::Starts:::calculatePrevoiusDemandDetails::::::>>>>");
		List<CreditSalesRecovery> creditSalesRecovery = new ArrayList<>();
		try {
			log.info("<<<::::::::Values:::::::customerId:::>>>" + customer.getId() + "<<:::CreditSalesDemand::::>>"
					+ demand.getId() + "<<<::::::>>>");
			/*
			 * String queryFetchAllDemandDetails =
			 * "select distinct (cdd.demand_month),cdd.customer_id,cdd.demand_amount,cdd.demand_year,cdd.credit_sales_demand_id  from credit_sales_demand_details cdd left join credit_sales_recovery csr on cdd.credit_sales_demand_id=csr.credit_sales_demand_id and cdd.customer_id="
			 * +customer.getId()+" and  csr.credit_sales_demand_id="+demand.getId()
			 * +" order by demand_month desc;";
			 */
			String queryFetchAllDemandDetails = "SELECT cdd.demand_month,cdd.customer_id,"
					+ " cdd.demand_amount,cdd.demand_year,cdd.credit_sales_demand_id"
					+ " FROM credit_sales_demand_details cdd  left JOIN credit_sales_recovery csr"
					+ " ON cdd.credit_sales_demand_id = csr.credit_sales_demand_id" + " where  cdd.customer_id = "
					+ customer.getId() + " AND cdd.credit_sales_demand_id = " + demand.getId()
					+ " group by cdd.demand_month, cdd.customer_id, cdd.demand_amount,"
					+ " cdd.demand_year, cdd.credit_sales_demand_id"
					+ " ORDER BY cdd.demand_year, Cast(cdd.demand_month as integer)";

			log.info("Query string....." + queryFetchAllDemandDetails);
			List<Map<String, Object>> dataList1 = jdbcTemplate.queryForList(queryFetchAllDemandDetails);
			List<CreditSalesRecoveryDTO> creditSalesRecoveryDTOList = new ArrayList<>();
			for (Map<String, Object> data : dataList1) {
				CreditSalesRecoveryDTO reco = new CreditSalesRecoveryDTO();
				if (data.get("customer_id") != null)
					reco.setCustomerId(Long.parseLong(data.get("customer_id").toString()));
				if (data.get("demand_month") != null)
					reco.setDemandMonth(Long.parseLong(data.get("demand_month").toString()));
				if (data.get("demand_amount") != null)
					reco.setDemandAmount(Double.parseDouble(data.get("demand_amount").toString()));
				if (data.get("demand_year") != null)
					reco.setDemandYear(Long.parseLong(data.get("demand_year").toString()));
				if (data.get("credit_sales_demand_id") != null)
					reco.setCreditDemandId(Long.parseLong(data.get("credit_sales_demand_id").toString()));
				creditSalesRecoveryDTOList.add(reco);
			}
			Calendar cal = Calendar.getInstance();
			int month = cal.get(Calendar.MONTH) + 1;
			int year = cal.get(Calendar.YEAR);
		 
//			String query = "select sum(collection_amount) as collectionamt,sum(demand_amount) as demandamt,demand_month,demand_year,customer_id ,credit_sales_demand_id from credit_sales_recovery where customer_id="
//					+ customer.getId() + " and credit_sales_demand_id=" + demand.getId()
//					+ " group by demand_month,demand_year,customer_id,credit_sales_demand_id order by demand_year,demand_month desc;";
			String query = "select row_number() over(order by demand_year,cast(demand_month as integer)),sum(collection_amount) as collectionamt, demand_amount AS demandamt,demand_month,demand_year,customer_id ,credit_sales_demand_id from credit_sales_recovery where customer_id="
					+ customer.getId() + " and credit_sales_demand_id=" + demand.getId()
					+ " group by demand_month,demand_year,customer_id,credit_sales_demand_id, demand_amount";
			
			log.info("Already paid query....." + query);
			List<Map<String, Object>> dataList = jdbcTemplate.queryForList(query);

			List<CreditSalesRecoveryDTO> alreadyPaidList = new ArrayList<>();
			for (Map<String, Object> data : dataList) {

				CreditSalesRecoveryDTO paidDemandMonth = new CreditSalesRecoveryDTO();
				if (data.get("collectionamt") != null) {
					paidDemandMonth.setCollectionAmt(Double.parseDouble(data.get("collectionamt").toString()));
				}
				if (data.get("demandamt") != null) {
					paidDemandMonth.setDemandAmount(Double.parseDouble(data.get("demandamt").toString()));
				}
				if (data.get("demand_month") != null) {
					paidDemandMonth.setDemandMonth(Long.parseLong(data.get("demand_month").toString()));
				}
				if (data.get("demand_year") != null) {
					paidDemandMonth.setDemandYear(Long.parseLong(data.get("demand_year").toString()));
				}
				if (data.get("row_number") != null) {
					paidDemandMonth.setFirst(Integer.parseInt(data.get("row_number").toString()));
				}
				alreadyPaidList.add(paidDemandMonth);

			}
			Long mon = creditSalesRecoveryDTOList.get(0).getDemandMonth();
			Long yr = creditSalesRecoveryDTOList.get(0).getDemandYear();

			Long lastmonth = creditSalesRecoveryDTOList.get(creditSalesRecoveryDTOList.size() - 1).getDemandMonth();
			Long lastYear = creditSalesRecoveryDTOList.get(creditSalesRecoveryDTOList.size() - 1).getDemandYear();

			AtomicInteger atmonth = new AtomicInteger(mon.intValue());
			AtomicInteger atyear = new AtomicInteger(yr.intValue());
			Double remaing = 0.0;
			if (alreadyPaidList == null || (alreadyPaidList != null && alreadyPaidList.size() == 0)) {
				log.info("Inside alreadyPaidList Size 0 or null:::getDemandAmount:::>>>>" + creditSalesRecoveryDTOList.get(0).getDemandAmount()
						+ ":::collection:::>>>" + collcetionAmt);
				Long noOfDues = (long) Math.ceil(collcetionAmt / creditSalesRecoveryDTOList.get(0).getDemandAmount()); // to
																														// find
																														// no
																														// of
																														// entry
																														// need
																														// to
																														// put
																														// in
																														// recovery
																														// table
																														// for
																														// month
						
				log.info("no of dues::::::::::::" + noOfDues);
				log.info("collectionAmount::::::::::::" + collcetionAmt);
				if (noOfDues==0) {
					noOfDues=1l;
				}
				for (Long i = 1l; i <= noOfDues; i++) {
					log.info(i + " PredictSalesRecovery entry  collectionAmount ::" + collcetionAmt + " month ::"
							+ atmonth + "  year ::" + atyear);
					log.info("last Month ::" + lastmonth + " lastYear ::" + lastYear);
					CreditSalesRecovery preCreditSalesRecovery = new CreditSalesRecovery();
					preCreditSalesRecovery.setCustomerMaster(customer);
					preCreditSalesRecovery.setCreditSalesDemand(demand);
					preCreditSalesRecovery.setPaymentMode(payMode);
					preCreditSalesRecovery.setVoucher(recovery.getVoucher());
					preCreditSalesRecovery.setDemandMonth(atmonth + "");
					preCreditSalesRecovery.setDemandYear(Double.parseDouble(atyear + ""));
					preCreditSalesRecovery.setDemandAmount(creditSalesRecoveryDTOList.get(0).getDemandAmount());

					if (collcetionAmt > creditSalesRecoveryDTOList.get(0).getDemandAmount()) {
						collcetionAmt = collcetionAmt - creditSalesRecoveryDTOList.get(0).getDemandAmount();
						preCreditSalesRecovery.setCollectionAmount(creditSalesRecoveryDTOList.get(0).getDemandAmount());
					} else {
						preCreditSalesRecovery.setCollectionAmount(collcetionAmt);
					}
					log.info("Before incremented     last Month ::" + lastmonth + " lastYear ::" + lastYear
							+ "  atmonth ::" + atmonth + "  atyear" + atyear);
					creditSalesRecovery.add(preCreditSalesRecovery);
					atmonth.getAndIncrement();
					if (atmonth.get() > 12) {
						atmonth.set(1);
						atyear.getAndIncrement();
					}

					log.info("After incremented       last Month ::" + lastmonth + " lastYear ::" + lastYear
							+ "atmonth ::" + atmonth + "  atyear" + atyear);
					if (atmonth.get() > lastmonth && atyear.get() > lastYear)
						break;
				}
				creditSalesRecoveryDTOList.stream()
						.filter(p -> p.getDemandYear().equals(month) && p.getDemandMonth().equals(year))
						.collect(Collectors.toList());
			} else {
				Comparator<CreditSalesRecoveryDTO> comparator = Comparator.comparing(CreditSalesRecoveryDTO::getFirst);
				log.info(alreadyPaidList.stream().collect(Collectors.maxBy(comparator))+"atmonth ::" + atmonth + "\t atyear ::" + atyear);
				CreditSalesRecoveryDTO lastPaidSalesRecovery = alreadyPaidList.stream().max(comparator).get();
//				CreditSalesRecoveryDTO lastPaidSalesRecovery = alreadyPaidList.get(alreadyPaidList.stream().collect(Collectors.maxBy(comparator))));
//				 list.stream().max(Comparator.naturalOrder()); .collect(Collectors.toList());
//				CreditSalesRecoveryDTO lastPaidSalesRecovery = alreadyPaidList.stream().map(a-> a.getFirst().MAX_VALUE);

				log.info("Already Paid List last record demand amount ::" + lastPaidSalesRecovery.getDemandAmount()
						+ "\t collectionAmount" + lastPaidSalesRecovery.getCollectionAmt());

				Double pendingAmount = lastPaidSalesRecovery.getDemandAmount()
						- lastPaidSalesRecovery.getCollectionAmt();

				atmonth = new AtomicInteger(lastPaidSalesRecovery.getDemandMonth().intValue());
				atyear = new AtomicInteger(lastPaidSalesRecovery.getDemandYear().intValue());

				log.info("atmonth ::" + atmonth + "\t atyear ::" + atyear);

				log.info("pendindAmount ::" + pendingAmount);

				if (pendingAmount > 0) {
					CreditSalesRecovery preCreditSalesRecovery = new CreditSalesRecovery();
					preCreditSalesRecovery.setCustomerMaster(customer);
					preCreditSalesRecovery.setCreditSalesDemand(demand);
					preCreditSalesRecovery.setPaymentMode(payMode);
					preCreditSalesRecovery.setVoucher(recovery.getVoucher());
					preCreditSalesRecovery.setDemandMonth(lastPaidSalesRecovery.getDemandMonth() + "");
					preCreditSalesRecovery
							.setDemandYear(Double.parseDouble(lastPaidSalesRecovery.getDemandYear() + ""));
					preCreditSalesRecovery.setDemandAmount(lastPaidSalesRecovery.getDemandAmount());
					preCreditSalesRecovery.setCollectionAmount(pendingAmount);
					creditSalesRecovery.add(preCreditSalesRecovery);
				}
				atmonth.getAndIncrement();
				if (atmonth.get() > 12) {
					atmonth.set(1);
					atyear.getAndIncrement();
				}
				log.info("atmonth ::" + atmonth + "\t atyear ::" + atyear);

				collcetionAmt = collcetionAmt - pendingAmount;

				log.info("collcetionAmt ::" + collcetionAmt);
				if (collcetionAmt > 0) {
					log.info("collectionAmount ::::::" + collcetionAmt);
					log.info("demandAmount ::::::" + creditSalesRecoveryDTOList.get(0).getDemandAmount());
					Long noOfDues = (long) Math
							.ceil(collcetionAmt / creditSalesRecoveryDTOList.get(0).getDemandAmount()); // to find no of
																										// entry need to
																										// put in
																										// recovery
																										// table for
																										// month
					log.info("no of dues::::::::::::" + noOfDues);
					for (Long i = 1l; i <= noOfDues; i++) {
						log.info(i + " PredictSalesRecovery entry  collectionAmount ::" + collcetionAmt + " month ::"
								+ atmonth + "  year ::" + atyear);
						log.info("last Month ::" + lastmonth + " lastYear ::" + lastYear);
						CreditSalesRecovery preCreditSalesRecovery = new CreditSalesRecovery();
						preCreditSalesRecovery.setCustomerMaster(customer);
						preCreditSalesRecovery.setCreditSalesDemand(demand);
						preCreditSalesRecovery.setPaymentMode(payMode);
						preCreditSalesRecovery.setVoucher(recovery.getVoucher());
						preCreditSalesRecovery.setDemandMonth(atmonth + "");
						preCreditSalesRecovery.setDemandYear(Double.parseDouble(atyear + ""));
						preCreditSalesRecovery.setDemandAmount(creditSalesRecoveryDTOList.get(0).getDemandAmount());

						if (collcetionAmt > creditSalesRecoveryDTOList.get(0).getDemandAmount()) {
							collcetionAmt = collcetionAmt - creditSalesRecoveryDTOList.get(0).getDemandAmount();
							preCreditSalesRecovery
									.setCollectionAmount(creditSalesRecoveryDTOList.get(0).getDemandAmount());
						} else {
							preCreditSalesRecovery.setCollectionAmount(collcetionAmt);
						}
						log.info("Before incremented     last Month ::" + lastmonth + " lastYear ::" + lastYear
								+ "  atmonth ::" + atmonth + "  atyear" + atyear);
						creditSalesRecovery.add(preCreditSalesRecovery);
						atmonth.getAndIncrement();
						if (atmonth.get() > 12) {
							atmonth.set(1);
							atyear.getAndIncrement();
						}

						log.info("After incremented       last Month ::" + lastmonth + " lastYear ::" + lastYear
								+ "atmonth ::" + atmonth + "  atyear" + atyear);
						if (atmonth.get() > lastmonth && atyear.get() > lastYear)
							break;
					}
				}
			}
			
		} catch (RestException restException) {
			log.error("CreditSalesRecoveryCollectionService.calculatePrevoiusDemandDetails RestException ",
					restException);
		} catch (Exception exception) {
			log.error("CreditSalesRecoveryCollectionService.calculatePrevoiusDemandDetails Exception ", exception);
		}
		log.info("End CreditSalesRecoveryCollectionService.calculatePrevoiusDemandDetails====>>>>");

		return creditSalesRecovery;
	}


	public BaseDTO searchCollectionRecovery(CreditSalesRecoveryDTO request) {
		log.info("searchCollectionRecovery called..."+request);
		BaseDTO baseDTO = new BaseDTO();
		try{
			Integer total=0;
			Integer start = request.getFirst(),pageSize = request.getPagesize();
			start=start*pageSize;
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();
			
			/*String mainQuery ="  select * from (select " + 
					"  csr.voucher_id as voucherId," + 
					"  date(csr.created_date) as createdDate,"+
					"   SUM(csr.collection_amount) as collectionAmount," + 
					"   sum(csr.demand_amount) AS demandAmount," + 
					"  cr.name as salesPeriod," + 
					"  om.org_name as organizationName," + 
					"  om.org_code as organizationCode," + 
//					"  csd.id as demandId," + 
					"  array_agg(csr.id)" + 
					"  from " + 
					"  credit_sales_recovery csr" + 
					"  left join credit_sales_demand csd on csd.id = csr.credit_sales_demand_id" + 
					"  Left join credit_sales_request cr on cr.id = csd.credit_sales_request_id" + 
					"  left join organization_master om on om.id = csd.organization_id" + 
					"  group by " + 
					"   csr.voucher_id," + 
					"  date(csr.created_date),"+
//					"  csr.demand_amount," + 
					"  cr.name," + 
//					"  csd.id," + 
					"  om.org_name," + 
					"  om.org_code" + 
					" )t  where 1=1 ";*/


			ApplicationQuery applicationQuery = applicationQueryRepository.findByQueryName("CREDIT_SALES_RECOVERY_LIST_LAZY_QUERY");

			if (applicationQuery == null || applicationQuery.getId() == null) {
				log.info("Application Query not found for query name : " + applicationQuery.getQueryName());
				return null;
			}

			String mainQuery = applicationQuery.getQueryContent().trim();

			log.info("Query " + mainQuery);

			mainQuery = applicationQuery.getQueryContent().trim();
			
			
			log.info("filters ::::::::"+request.getFilters());
			if(request.getFilters().get("salesPeriodName") != null
					&& !request.getFilters().get("salesPeriodName").toString().trim().isEmpty()) {
				mainQuery += " and upper(t.salesPeriod) like upper('%"+request.getFilters().get("salesPeriodName").toString().trim()+"%') ";
			}

			if(request.getFilters().get("demandAmount")!=null) {
				Double demandAmt = Double.parseDouble(request.getFilters().get("demandAmount").toString());
				mainQuery +=" and t.demandAmount="+demandAmt+" ";
			}

			if(request.getFilters().get("collectionAmt")!=null) {
				Double collectionAmt = Double.parseDouble(request.getFilters().get("collectionAmt").toString());
				mainQuery +=" and t.collectionAmount="+collectionAmt+" ";
			}

			if(request.getFilters().get("organizationName") != null
					&& !request.getFilters().get("organizationName").toString().trim().isEmpty()) {

				mainQuery += " and ("
						+ " upper(t.organizationName) like upper('%"+request.getFilters().get("organizationName").toString().trim()+"%') or"
						+ " upper(t.organizationCode) like upper('%"+request.getFilters().get("organizationName").toString().trim()+"%'))";
			}

//			if(request.getFilters().get("createdDate")!=null) {
//				Double collectionAmt = Double.parseDouble(request.getFilters().get("collectionAmt").toString());
//				mainQuery +=" and t.collectionAmount="+collectionAmt+" ";
//			}
			
			if(request.getFilters().get("createdDate")!=null) {
				Date createdDate=new Date((Long)request.getFilters().get("createdDate"));
				mainQuery += " and t.createdDate='"+createdDate+"'";	
			}
			
			String countQuery=mainQuery.replace("SELECT *", "select count(*) as count ");
			log.info("count query... "+countQuery);
			countData=jdbcTemplate.queryForList(countQuery);
			for(Map<String,Object> data:countData){
				if(data.get("count")!=null)
					total=Integer.parseInt(data.get("count").toString().replace(",", ""));
			}


			if(request.getSortField()==null)
				mainQuery+=" order by t.voucherId desc limit "+pageSize+" offset "+start+";";

			if(request.getSortField()!=null && request.getSortOrder()!=null)
			{
				log.info("Sort Field:["+request.getSortField()+"] Sort Order:["+request.getSortOrder()+"]");
				if(request.getSortField().equals("demandAmount") && request.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by t.demandAmount asc ";
				if(request.getSortField().equals("demandAmount") && request.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by t.demandAmount desc ";
				if(request.getSortField().equals("collectionAmt") && request.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by t.collectionAmount asc  ";
				if(request.getSortField().equals("collectionAmt") && request.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by t.collectionAmount desc  ";
				if(request.getSortField().equals("salesPeriod") && request.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by t.salesPeriod asc ";
				if(request.getSortField().equals("salesPeriod") && request.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by t.salesPeriod desc ";
				if(request.getSortField().equals("organizationName") && request.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by t.organizationName asc ";
				if(request.getSortField().equals("organizationName") && request.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by t.organizationName desc ";
				if(request.getSortField().equals("organizationCode") && request.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by t.organizationCode desc ";
				if(request.getSortField().equals("organizationCode") && request.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by t.organizationCode asc ";
				if(request.getSortField().equals("createdDate") && request.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by t.createdDate asc ";
				if(request.getSortField().equals("createdDate") && request.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by t.createdDate desc ";

				mainQuery+=" limit "+pageSize+" offset "+start+";";
			}
			log.info("Main Qury....."+mainQuery);
			List<CreditSalesRecoveryDTO> creditSalesRecoveryList = new ArrayList<>();
			listofData = jdbcTemplate.queryForList(mainQuery); 
			for(Map<String,Object> data:listofData){
				CreditSalesRecoveryDTO dto = new CreditSalesRecoveryDTO();
				if(data.get("voucherId")!=null)
					dto.setVoucherId(Long.parseLong(data.get("voucherId").toString()));

				if(data.get("demandAmount")!=null)
					dto.setDemandAmount(Double.parseDouble(data.get("demandAmount").toString()));

				if(data.get("collectionAmount")!=null)
					dto.setCollectionAmt(Double.parseDouble(data.get("collectionAmount").toString()));

				if(data.get("organizationCode")!=null)
					dto.setOrganizationCode((data.get("organizationCode").toString()));

				if(data.get("organizationName")!=null)
					dto.setOrganizationName((data.get("organizationName").toString()));

				if(data.get("salesPeriod")!=null)
					dto.setSalesPeriodName((data.get("salesPeriod").toString()));

				if(data.get("createdDate")!=null) {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					Date dt = sdf.parse(data.get("createdDate").toString());
					dto.setCreatedDate(dt);
				}
				creditSalesRecoveryList.add(dto);
			} 
			log.info("Returning creditSalesRecoveryList...."+creditSalesRecoveryList.size());
			log.info("Total records present in creditSalesRecoveryList..."+total);
			baseDTO.setResponseContent(creditSalesRecoveryList);
			baseDTO.setTotalRecords(total);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

		}catch(Exception exp){
			log.error("Exception Cause  : " , exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO ;
	}


	public BaseDTO getRecoveryCollectionByVoucherId(Long voucherId) {
		log.info("CreditSalesRecoveryCollectionService getRecoveryCollectionByVoucherId method started ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			ViewCreditSalesRecoveryDetailsDTO viewDto = new ViewCreditSalesRecoveryDetailsDTO();

			List<CreditSalesRecoverySearchResponseDto>  searchResponseList = new ArrayList<>();
			List<CreditSalesRecovery> creditSalesRecoveryList = creditSalesRecoveryRepository.findByVoucherId(voucherId);
			if(creditSalesRecoveryList != null && creditSalesRecoveryList.size() != 0) {
				
				viewDto.setCreditSalesRequest(creditSalesRecoveryList.get(0).getCreditSalesDemand().getCreditSalesRequest());
				viewDto.setCreatedByName(creditSalesRecoveryList.get(0).getCreatedBy().getUsername());
				viewDto.setCreatedDate(creditSalesRecoveryList.get(0).getCreatedDate());
				
				viewDto.setModifiedByName(creditSalesRecoveryList.get(0).getModifiedBy().getUsername());
				viewDto.setModifiedDate(creditSalesRecoveryList.get(0).getModifiedDate());
				
				if(creditSalesRecoveryList.get(0).getCreditSalesDemand().getOrganizationMaster() != null) {
					viewDto.setOrganizationName(creditSalesRecoveryList.get(0).getCreditSalesDemand().getOrganizationMaster().getOrgName());
					viewDto.setOrganizationCode(creditSalesRecoveryList.get(0).getCreditSalesDemand().getOrganizationMaster().getOrgCode());
				}
				
				Voucher  voucher = creditSalesRecoveryList.get(0).getVoucher();
				if(voucher != null) {
					viewDto.setTotalReferenceAmount(voucher.getNetAmount());
					viewDto.setTotalRecoveryAmount(voucher.getNetAmount());
				}
				if(creditSalesRecoveryList.get(0).getPaymentMode() != null)
					viewDto.setModeOfReceipt(creditSalesRecoveryList.get(0).getPaymentMode().getPaymentMode());
				
				List<PaymentDetails> paymentDetailList = paymentDetailsRepository.getPaymentListByVoucher(voucher.getId());
				if(paymentDetailList != null && paymentDetailList.size() != 0) {
					viewDto.setReferenceNumber(paymentDetailList.get(0).getBankReferenceNumber());
					viewDto.setReferenceDate(paymentDetailList.get(0).getDocumentDate());
				}
					
				

				Map<Long, List<CreditSalesRecovery>> recoveryListGroupByCustomer = creditSalesRecoveryList.stream()
						.collect(Collectors.groupingBy(s -> s.getCustomerMaster().getId()));

				for (Map.Entry<Long, List<CreditSalesRecovery>> entry : recoveryListGroupByCustomer.entrySet()) {

					Double collectedAmount = entry.getValue().stream().mapToDouble(s->s.getCollectionAmount()).sum();
					
					CreditSalesRecoverySearchResponseDto searchResponseDto = new CreditSalesRecoverySearchResponseDto();

					searchResponseDto.setCustomerCode(entry.getValue().get(0).getCustomerMaster().getCode());
					searchResponseDto.setCustomerName(entry.getValue().get(0).getCustomerMaster().getName());
					searchResponseDto.setDesignationName(entry.getValue().get(0).getCustomerMaster().getDesignation());
					searchResponseDto.setEmpCode(entry.getValue().get(0).getCustomerMaster().getEmpCode());

					searchResponseDto.setSanctionOrderNumber(entry.getValue().get(0).getCreditSalesDemand().getCredistSalesDemandDetList().get(0).getSanctionOrderNumber());
					searchResponseDto.setTotalPurchaseAmount(entry.getValue().get(0).getCreditSalesDemand().getCredistSalesDemandDetList().get(0).getTotalPurchaseAmount());
					searchResponseDto.setNoOfInstallment(entry.getValue().get(0).getCreditSalesDemand().getCredistSalesDemandDetList().get(0).getNumberOfInstallment().longValue());
					searchResponseDto.setMonthlyInstallmentAmount(entry.getValue().get(0).getCreditSalesDemand().getCredistSalesDemandDetList().get(0).getMonthlyInstallmentAmount());

					searchResponseDto.setAmountCollected(collectedAmount);
					searchResponseDto.setCurrentRecoveryAmount(collectedAmount);
					
					List<CreditSalesRecovery> existingRecoveryList =creditSalesRecoveryRepository.findByCustomerIdAndCreditSalesDemandIdAndVoucherId(entry.getValue().get(0).getCreditSalesDemand().getId(),entry.getValue().get(0).getCustomerMaster().getId(),entry.getValue().get(0).getVoucher().getId());
					
					if(existingRecoveryList != null && existingRecoveryList.size() != 0) {
						Double  alreadCollectedAmt = existingRecoveryList.stream().mapToDouble(r->r.getCollectionAmount()).sum();
						searchResponseDto.setAlreadyCollectedAmount(alreadCollectedAmt);
					}
					log.info("searchResponseDto.getTotalPurchaseAmount() ::"+searchResponseDto.getTotalPurchaseAmount()+"\t alreadCollectedAmt ::"+searchResponseDto.getAlreadyCollectedAmount()+"\t collectedAmount::"+collectedAmount);
					
					Double toBeCollectedAmt = 0.0 ;
					if(searchResponseList != null)
						toBeCollectedAmt = searchResponseDto.getTotalPurchaseAmount() - searchResponseDto.getAlreadyCollectedAmount() - collectedAmount;
					if(toBeCollectedAmt < 0)	
						toBeCollectedAmt  = 0.0;
					
					log.info("Customer ::"+searchResponseDto.getCustomerName()+"\t collectionAmount ::"+collectedAmount+"\t to be collected ::"+toBeCollectedAmt);
					
					searchResponseDto.setAmountTobeCollected(toBeCollectedAmt);
					
					
					List<CreditSalesRecoveryDTO> list = executeRecoveryCollectionHistoryQuery(entry.getValue().get(0).getCreditSalesDemand().getId(), entry.getValue().get(0).getCustomerMaster().getId());

					List<CreditSalesRecoveryDTO> collectionRecoveryHistoryList = new ArrayList<>();

					if(list == null) {
						searchResponseDto.setCollectionRecoveryHistory(collectionRecoveryHistoryList);
					}else {
						searchResponseDto.setCollectionRecoveryHistory(list);
					}
					searchResponseList.add(searchResponseDto);
				}
				
				viewDto.setCustomerList(searchResponseList);
			}
			baseDTO.setResponseContent(viewDto);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("CreditSalesRecoveryCollectionService getRecoveryCollectionByVoucherId RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("CreditSalesRecoveryCollectionService getRecoveryCollectionByVoucherId Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("CreditSalesRecoveryCollectionService getRecoveryCollectionByVoucherId method completed");
		return responseWrapper.send(baseDTO);
	}
	
	
	public BaseDTO loadMonthYearInstallment(RecoveryCollectionSearchRequestDto request){
		log.info("CreditSalesRecoveryCollectionService searchRecoveryCollectionDetails method started "+request);
		BaseDTO baseDTO = new BaseDTO();
		try {
			Validate.objectNotNull(request.getCreditSalesType(),ErrorDescription.CREDIT_SALES_TYPE_REQUIRED);
			Validate.objectNotNull(request.getCreditSalesRequest(),ErrorDescription.CREDIT_SALES_PERIOD_REQUIRED);
			Validate.objectNotNull(request.getCreditSalesRequest().getId(),ErrorDescription.CREDIT_SALES_PERIOD_REQUIRED);
			if("CUSTOMER".equalsIgnoreCase(request.getCreditSalesType())) {
				Validate.objectNotNull(request.getCustomerId(),ErrorDescription.RECOVERY_CUSTOMER_REQUIRED);	
			}
			if("ORGANIZATION".equalsIgnoreCase(request.getCreditSalesType())) {
				Validate.objectNotNull(request.getOrganizationId(),ErrorDescription.ORGANIZATION_REQUIRED);	
			}

			List<RecoveryCollectionSearchRequestDto> monthyearinstallmentList = executeloadMonthYearInstallmentList(
					 request.getOrganizationId(),request.getCreditSalesRequest().getId());
			baseDTO.setResponseContent(monthyearinstallmentList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (RestException restException) {
			log.error("CreditSalesRecoveryCollectionService searchRecoveryCollectionDetails RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("CreditSalesRecoveryCollectionService searchRecoveryCollectionDetails Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("CreditSalesRecoveryCollectionService searchRecoveryCollectionDetails method completed");
		return responseWrapper.send(baseDTO);
	}
	
	
	public List<RecoveryCollectionSearchRequestDto> executeloadMonthYearInstallmentList(Long organizationId,Long creditSalesRequestId){
		log.info("Starts CreditSalesRecoveryCollectionService.executeloadMonthYearInstallmentList:::organizationId:::>>"
				+ organizationId + "::::creditSalesRequestId::>>" + creditSalesRequestId);
		List<RecoveryCollectionSearchRequestDto> recoveryCollectionSearchRequestDtoList=new ArrayList<>();
		try {
			
			List<Object[]> loadMonthYearInstallmentObjList = creditSalesDemandRepository
					.loadMonthYearInstallmentList(creditSalesRequestId, organizationId);
			log.info("<<<::loadMonthYearInstallmentObjList.size:::>>>>"+loadMonthYearInstallmentObjList.size());

			loadMonthYearInstallmentObjList.forEach(monthyearinstallment -> {
				RecoveryCollectionSearchRequestDto recoveryCollectionSearchRequestDto=new RecoveryCollectionSearchRequestDto();
				recoveryCollectionSearchRequestDto.setInstalment(new Integer(monthyearinstallment[0].toString()));
				recoveryCollectionSearchRequestDto.setDemandMonth(new Long(monthyearinstallment[1].toString()));
				recoveryCollectionSearchRequestDto.setDemandYear(new Long(monthyearinstallment[2].toString()));
				recoveryCollectionSearchRequestDto.setCreditSalesDemandId(new Long(monthyearinstallment[4].toString()));
				recoveryCollectionSearchRequestDtoList.add(recoveryCollectionSearchRequestDto);
			});
			log.info("<<<::size:::>>>>"+recoveryCollectionSearchRequestDtoList.size());
		} catch (RestException restException) {
			log.error("CreditSalesRecoveryCollectionService searchRecoveryCollectionDetails RestException ", restException);
		} catch (Exception exception) {
			log.error("CreditSalesRecoveryCollectionService searchRecoveryCollectionDetails Exception ", exception);
		}
		log.info("Ends CreditSalesRecoveryCollectionService.executeDemandDetailsQuery ");
		return recoveryCollectionSearchRequestDtoList;
	}
}

