package in.gov.cooptex.finance.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;


import in.gov.cooptex.common.service.LoginService;
import in.gov.cooptex.common.service.NotificationEmailService;
import in.gov.cooptex.core.accounts.enums.VoucherStatus;
import in.gov.cooptex.core.accounts.enums.VoucherTypeDetails;
import in.gov.cooptex.core.accounts.model.ExpenseDetails;
import in.gov.cooptex.core.accounts.model.ExpenseReason;
import in.gov.cooptex.core.accounts.model.GlAccount;
import in.gov.cooptex.core.accounts.model.PurchaseSalesAdvance;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.VoucherDetails;
import in.gov.cooptex.core.accounts.model.VoucherLog;
import in.gov.cooptex.core.accounts.model.VoucherNote;
import in.gov.cooptex.core.accounts.model.VoucherType;
import in.gov.cooptex.core.accounts.repository.GlAccountRepository;
import in.gov.cooptex.core.accounts.repository.StaffInvoiceAdjustmentRepository;
import in.gov.cooptex.core.accounts.repository.VoucherDetailsRepository;
import in.gov.cooptex.core.accounts.repository.VoucherLogRepository;
import in.gov.cooptex.core.accounts.repository.VoucherNoteRepository;
import in.gov.cooptex.core.accounts.repository.VoucherRepository;
import in.gov.cooptex.core.accounts.repository.VoucherTypeRepository;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.PettyCashPayFor;
import in.gov.cooptex.core.enums.SequenceName;
import in.gov.cooptex.core.model.ApplicationQuery;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeePersonalInfoEmployment;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ExpenseType;
import in.gov.cooptex.core.model.PaymentMode;
import in.gov.cooptex.core.model.SequenceConfig;
import in.gov.cooptex.core.model.SystemNotification;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.repository.ApplicationQueryRepository;
import in.gov.cooptex.core.repository.EmployeeMasterRepository;
import in.gov.cooptex.core.repository.EmployeePersonalInfoEmploymentRepository;
import in.gov.cooptex.core.repository.EntityMasterRepository;
import in.gov.cooptex.core.repository.PaymentModeRepository;
import in.gov.cooptex.core.repository.SequenceConfigRepository;
import in.gov.cooptex.core.repository.SupplierMasterRepository;
import in.gov.cooptex.core.repository.SystemNotificationRepository;
import in.gov.cooptex.core.repository.UserMasterRepository;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.finance.advance.repository.PurchaseSalesAdvanceRepository;
import in.gov.cooptex.finance.dto.PettyCashAdjustmentDetailsDTO;
import in.gov.cooptex.finance.dto.PettyCashExpenseDTO;
import in.gov.cooptex.finance.model.StaffInvoiceAdjustment;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.repository.ExpenseDetailsRepository;
import in.gov.cooptex.operation.repository.ExpenseReasonRepository;
import in.gov.cooptex.operation.repository.ExpenseTypeRepository;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class PettyCashExpenseService {

	@Autowired
	ExpenseTypeRepository expenseTypeRepository;
	
	@Autowired
	GlAccountRepository glAccountRepository;
	
	@Autowired 
	EmployeeMasterRepository employeeMasterRepository;
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	EntityMasterRepository entityMasterRepository;
	
	@Autowired
	UserMasterRepository userMasterRepository;
	
	@Autowired
	PaymentModeRepository paymentModeRepository;
	
	@Autowired
	ExpenseDetailsRepository expenseDetailsRepository;
	
	@Autowired
	SequenceConfigRepository sequenceConfigRepository;
	
	@Autowired
	VoucherTypeRepository voucherTypeRepository;
	
	@Autowired
	VoucherRepository voucherRepository;
	
	@Autowired
	SupplierMasterRepository supplierMasterRepository;
	
	@Autowired
	EntityManager entityManager;
	
	@Autowired
	ApplicationQueryRepository applicationQueryRepository;
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	VoucherNoteRepository voucherNoteRepository;
	
	@Autowired
	VoucherLogRepository voucherLogRepository;
	
	@Autowired
	VoucherDetailsRepository voucherDetailsRepository;
	
	@Autowired
	StaffInvoiceAdjustmentRepository staffInvoiceAdjustmentRepository;
	
	@Autowired
	ExpenseReasonRepository expenseReasonRepository;
	
	@Autowired
	PurchaseSalesAdvanceRepository purchaseSalesAdvanceRepository;
	
	@Autowired
	NotificationEmailService notificationEmailService;
	
	private static final String VIEW_PETTY_CASH_EXPENSE = "/pages/finance/expenseTransaction/viewExpenseTransaction.xhtml?faces-redirect=true";
	
	@Autowired
	SystemNotificationRepository systemNotificationRepository;
	
	public BaseDTO getAllExpenseTypes() {
		log.info("============== PettyCashExpenseService:getAllExpenseTypes ================");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<ExpenseType> expenseTypeList = expenseTypeRepository.findAll();
			baseDTO.setResponseContent(expenseTypeList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		}catch (RestException restException) {
			log.error("PettyCashExpenseService getAllExpenseTypes RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("PettyCashExpenseService getAllExpenseTypes Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}
	
	@Transactional
	public BaseDTO createPettyCashExpense(PettyCashExpenseDTO pettyCashExpenseDTO){
		log.info("<============== PettyCashExpenseService:createPettyCashExpense ================>");
		BaseDTO baseDTO=new BaseDTO();
		List<ExpenseDetails> expenseDetailsList = new ArrayList<ExpenseDetails>();
		List<VoucherDetails> voucherDetailsList=new ArrayList<>();
		List<VoucherNote> voucherNoteList=new ArrayList<>();
		try {
			if(pettyCashExpenseDTO.getId()==null) {
				EntityMaster entityMaster = entityMasterRepository.findByUserRegion(loginService.getCurrentUser().getId());
				SequenceConfig sequenceConfig = sequenceConfigRepository.findBySequenceName(SequenceName.valueOf(SequenceName.PETTY_CASH_REF_NUMBER.name()));
				if (sequenceConfig == null) {
					throw new RestException(ErrorDescription.SEQUENCE_CONFIG_NOT_EXIST);
				}
				String referenceNumber=entityMaster.getCode() + sequenceConfig.getSeparator()+
						sequenceConfig.getPrefix()+ AppUtil.getCurrentYearString() + AppUtil.getCurrentMonthString();
				String expenseDetailsRefNumber = referenceNumber+sequenceConfig.getCurrentValue();
				Voucher voucher=new Voucher();
				voucher.setReferenceNumberPrefix(referenceNumber);
				voucher.setReferenceNumber(sequenceConfig.getCurrentValue());
				voucher.setName(VoucherTypeDetails.PETTY_CASH_EXPENSE.toString());
				VoucherType voucherType = voucherTypeRepository.findByName(VoucherTypeDetails.Payment.toString());
				voucher.setVoucherType(voucherType);
//				voucher.setNetAmount(pettyCashExpenseDTO.getTotalAmount());
				voucher.setNetAmount(pettyCashExpenseDTO.getBalancePaymentAmount());
				voucher.setNarration(pettyCashExpenseDTO.getVoucherNarration());
				voucher.setPaymentFor(VoucherTypeDetails.PETTY_CASH_EXPENSE.toString());
				voucher.setFromDate(new Date());
				voucher.setToDate(new Date());
				voucher.setEntityMaster(entityMaster);
				voucher.setPaymentMode(pettyCashExpenseDTO.getPaymentMode());
				for(ExpenseDetails expenseDetails:pettyCashExpenseDTO.getExpenseDetailsList()){
					ExpenseDetails expenseDetailsObj=new ExpenseDetails();
					expenseDetailsObj.setEntityMaster(entityMaster);
					if(pettyCashExpenseDTO.getEmployeeMaster()!=null) {
						expenseDetailsObj.setEmpMaster(employeeMasterRepository.findById(pettyCashExpenseDTO.getEmployeeMaster().getId()));
						expenseDetailsObj.setPayTo(PettyCashPayFor.EMPLOYEE);
					}
					expenseDetailsObj.setRefNumber(expenseDetailsRefNumber);
					expenseDetailsObj.setExpenseDate(new Date());
					if(expenseDetails.getExpenseAmount()!=null) {
						expenseDetailsObj.setExpenseAmount(expenseDetails.getExpenseAmount());
					}
					if(pettyCashExpenseDTO.getExpenseType()!=null) {
						expenseDetailsObj.setExpenseType(expenseTypeRepository.findOne(pettyCashExpenseDTO.getExpenseType().getId()));
					}
					expenseDetailsObj.setNarration(expenseDetails.getNarration());
					if(expenseDetails.getAccount()!=null) {
						expenseDetailsObj.setAccount(glAccountRepository.findById(expenseDetails.getAccount().getId()));
					}
					if(pettyCashExpenseDTO.getExpenseReason()!=null) {
						expenseDetailsObj.setExpenseReason(expenseReasonRepository.findOne(pettyCashExpenseDTO.getExpenseReason().getId()));
					}
					expenseDetailsObj.setExpenseTransactionName(pettyCashExpenseDTO.getExpenseTransactionName());
					if(pettyCashExpenseDTO.getSupplierMaster()!=null) {
						expenseDetailsObj.setPayTo(PettyCashPayFor.SUPPLIER);
						expenseDetailsObj.setSupplierMaster(supplierMasterRepository.findOne(pettyCashExpenseDTO.getSupplierMaster().getId()));
					}
					if(pettyCashExpenseDTO.getPayFor().equals(PettyCashPayFor.OTHER)) {
						expenseDetailsObj.setPayTo(PettyCashPayFor.OTHER);
						expenseDetailsObj.setPayFor(pettyCashExpenseDTO.getPayName());
					}
					if(pettyCashExpenseDTO.getPaymentMode()!=null) {
						expenseDetailsObj.setPaymentMode(pettyCashExpenseDTO.getPaymentMode());
					}
					expenseDetailsObj.setVoucher(voucher);
					expenseDetailsList.add(expenseDetailsObj);
					//Voucher Details
					VoucherDetails voucherDetails = new VoucherDetails();
					voucherDetails.setVoucher(voucher);
					voucherDetails.setAmount(expenseDetails.getExpenseAmount());
					if(expenseDetailsObj.getAccount()!=null) {
						voucherDetails.setGlAccount(expenseDetailsObj.getAccount());
					}
					voucherDetailsList.add(voucherDetails);
				}
				voucher.setVoucherDetailsList(voucherDetailsList);
				//Voucher Log
				VoucherLog voucherLog = new VoucherLog();
				voucherLog.setVoucher(voucher);
				voucherLog.setStatus(VoucherStatus.SUBMITTED);
				UserMaster userMaster = userMasterRepository.findOne(loginService.getCurrentUser().getId());
				voucherLog.setUserMaster(userMaster);
				voucher.getVoucherLogList().add(voucherLog); 
				//Voucher Note
				VoucherNote voucherNote = new VoucherNote();
				voucherNote.setNote(pettyCashExpenseDTO.getNote());
				voucherNote.setFinalApproval(pettyCashExpenseDTO.getFinalApproval());
				if(pettyCashExpenseDTO.getForwardTo()!=null) {
					UserMaster forwardTo=userMasterRepository.findOne((pettyCashExpenseDTO.getForwardTo().getId()));
					voucherNote.setForwardTo(forwardTo);
				}
				voucherNote.setCreatedBy(userMaster);
				voucherNote.setCreatedByName(userMaster.getUsername());
				voucherNote.setCreatedDate(new Date());
				voucherNote.setVoucher(voucher);
				voucherNoteList.add(voucherNote);
				voucher.setVoucherNote(voucherNoteList);
				voucherRepository.save(voucher);
				expenseDetailsRepository.save(expenseDetailsList);
				if (pettyCashExpenseDTO.getTotalAdjustmentAmount()!=0.0 && pettyCashExpenseDTO.getTotalAdjustmentAmount() != null) {
					StaffInvoiceAdjustment staffInvoiceAdjustment=new StaffInvoiceAdjustment();
					staffInvoiceAdjustment.setVoucher(voucher);
					staffInvoiceAdjustment.setAdjustedAmount(pettyCashExpenseDTO.getTotalAdjustmentAmount());
					log.info("::::::::::::employee ID::::::::::::"+pettyCashExpenseDTO.getLoggedInEmployeeMaster().getId());
					staffInvoiceAdjustment.setEmpMaster(employeeMasterRepository.findById(pettyCashExpenseDTO.getLoggedInEmployeeMaster().getId()));
					staffInvoiceAdjustment.setAdvanceType(VoucherTypeDetails.PETTY_CASH_EXPENSE.toString());
					staffInvoiceAdjustmentRepository.save(staffInvoiceAdjustment);
				}
				else {
					log.info("::::::::::AdjustedAmount Not Given::::::::::");
				}
				sequenceConfig.setCurrentValue(sequenceConfig.getCurrentValue() + 1);
				sequenceConfigRepository.save(sequenceConfig);
				log.info("Start PettycashService.savePettyCash");
				
				/*
				 * Send Notification Mail
				 */
				Map<String, Object> additionalData = new HashMap<>();

				additionalData.put("Url", VIEW_PETTY_CASH_EXPENSE+"&id="+ voucher.getId() + "&");
				notificationEmailService.sendMailAndNotificationForForward(voucherNote, voucherLog,
						additionalData);
			}else {
				updatePettyCashExpense(pettyCashExpenseDTO);
			}
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		}catch (RestException restException) {
			log.error("PettyCashExpenseService createPettyCashExpense RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("PettyCashExpenseService createPettyCashExpense Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}
	
	public BaseDTO search(PaginationDTO request) {
		BaseDTO baseDTO = new BaseDTO();
		
		try{
			Integer total=0;
			Integer start = request.getFirst(),pageSize = request.getPageSize();
			start=start*pageSize;
			List<Map<String, Object>> listofData = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> countData = new ArrayList<Map<String, Object>>();
			ApplicationQuery applicationQuery = applicationQueryRepository.findByQueryName("PETTY_CASH_EXPENSE_LIST");

			String mainQuery = applicationQuery.getQueryContent().trim();
			EntityMaster entityMaster = entityMasterRepository.findByUserRegion(loginService.getCurrentUser().getId());
			String condition="";
			if(entityMaster!=null && entityMaster.getId()!=null) {
				condition += " and (v.entity_id="+entityMaster.getId()+" or vn.forward_to="+loginService.getCurrentUser().getId()+")" ; 
			}
			
			if(request.getFilters()!=null) {
				if (request.getFilters().get("expenseTypeName")!=null) {
					condition += " and t.expense_type like '%"+request.getFilters().get("expenseTypeName")+"%'";
				}
				if (request.getFilters().get("expenseTransactionName")!=null) {
					condition += " and  t.reason_code_name like '%"+request.getFilters().get("expenseTransactionName")+"%'";
				}
				if (request.getFilters().get("payTo")!=null) {
					condition += " and t.payTo like '%"+request.getFilters().get("payTo")+"%'";
				}
				if (request.getFilters().get("createdDate")!=null) {
					Date createdDate=new Date((Long)request.getFilters().get("createdDate"));
					condition += " and t.createdDate::date = date '"+createdDate+"'";	
				}
				if (request.getFilters().get("status")!=null) {
					condition += " and t.status='"+request.getFilters().get("status")+"'";
				}
			}
			
			mainQuery=mainQuery.replace(":condition", condition);
			String countQuery=mainQuery.replace("select * ","select count(*) ");
			
			log.info("count query... "+countQuery);
			countData=jdbcTemplate.queryForList(countQuery);
			for(Map<String,Object> data:countData){
				if(data.get("count")!=null)
					total=Integer.parseInt(data.get("count").toString().replace(",", ""));
			}
			
			log.info("total======>"+total);
			
			if(request.getSortField()==null)
			mainQuery+=" order by voucherId desc limit "+pageSize+" offset "+start+";";
			
			if(request.getSortField()!=null && request.getSortOrder()!=null)
			{
				log.info("Sort Field:["+request.getSortField()+"] Sort Order:["+request.getSortOrder()+"]");
				if(request.getSortField().equals("expenseTypeName") && request.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by t.expense_type asc ";
				if(request.getSortField().equals("expenseTypeName") && request.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by t.expense_type desc ";
				if(request.getSortField().equals("expenseTransactionName") && request.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by t.reason_code_name asc  ";
				if(request.getSortField().equals("expenseTransactionName") && request.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by t.reason_code_name desc  ";
				if(request.getSortField().equals("payTo") && request.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by t.payTo asc ";
				if(request.getSortField().equals("payTo") && request.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by t.payTo desc ";
				if(request.getSortField().equals("createdDate") && request.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by t.createdDate asc  ";
				if(request.getSortField().equals("createdDate") && request.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by t.createdDate desc  ";
				if(request.getSortField().equals("status") && request.getSortOrder().equals("ASCENDING"))
					mainQuery+=" order by t.status asc  ";
				if(request.getSortField().equals("status") && request.getSortOrder().equals("DESCENDING"))
					mainQuery+=" order by t.status desc  ";
				mainQuery+=" limit "+pageSize+" offset "+start+";";
			}
			log.info("Main Qury....."+mainQuery);
			List<PettyCashExpenseDTO> pettyCashExpenseDTOList=new ArrayList<>();
			listofData = jdbcTemplate.queryForList(mainQuery); 
			for(Map<String,Object> data:listofData){
				PettyCashExpenseDTO pettyCashExpenseDTO=new PettyCashExpenseDTO();
				if(data.get("voucherId")!=null)
					pettyCashExpenseDTO.setId(Long.parseLong(data.get("voucherId").toString()));
				if(data.get("expense_type")!=null)
					pettyCashExpenseDTO.setExpenseTypeName(data.get("expense_type").toString());
				if(data.get("reason_code_name")!=null)
					pettyCashExpenseDTO.setExpenseTransactionName(data.get("reason_code_name").toString());
				if(data.get("payTo")!=null)
					pettyCashExpenseDTO.setPayTo(data.get("payTo").toString());
				if(data.get("status")!=null)
					pettyCashExpenseDTO.setStatus(data.get("status").toString());	
				if(data.get("createdDate")!=null)
					pettyCashExpenseDTO.setCreatedDate((Date)data.get("createdDate"));
				pettyCashExpenseDTOList.add(pettyCashExpenseDTO);
			} 
			log.info("Returning PettyCashExpense list...."+pettyCashExpenseDTOList);
			log.info("Total records present in PettyCashExpense..."+total);
			baseDTO.setResponseContent(pettyCashExpenseDTOList);
			baseDTO.setTotalRecords(total);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		}catch(Exception exp){
			log.error("Exception Cause  : " , exp);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO ;
	}
	
	public BaseDTO getPettyCashExpenseDetailsById(Long id,Long notificationId) {
		log.info(" PettyCashExpenseService getPettyCashExpenseDetailsById : " +id);
		BaseDTO baseDTO = new BaseDTO();PettyCashExpenseDTO pettyCashExpenseDTO=new PettyCashExpenseDTO();
		try {
			List<Map<String, Object>> queryData = new ArrayList<Map<String, Object>>();
			ApplicationQuery applicationQuery = applicationQueryRepository.findByQueryName("PETTY_CASH_EXPENSE_VIEW");
			if (applicationQuery == null || applicationQuery.getId() == null) {
				log.info("Application Query not found for query name : " + applicationQuery);
				baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
				return baseDTO;
			}
			String query = applicationQuery.getQueryContent().trim();
			query = query.replace(":voucherId", id.toString());
			log.info("Query Content For Petty Cash Expense View query : " + query);
			queryData = jdbcTemplate.queryForList(query);
			List<ExpenseDetails> expenseDetailsList=new ArrayList<>();
			if(!queryData.isEmpty()) {
				for(Map<String,Object> queryDataMap : queryData){
					if(queryDataMap.get("expenseTypeId")!=null) {
						Long expenseTypeId=Long.valueOf(queryDataMap.get("expenseTypeId").toString());
						ExpenseType expenseType=expenseTypeRepository.findOne(expenseTypeId);
						pettyCashExpenseDTO.setExpenseType(expenseType);
					}
					if(queryDataMap.get("reason_code_name")!=null) {
						pettyCashExpenseDTO.setReasonCodeName(queryDataMap.get("reason_code_name").toString());
					}
					if(queryDataMap.get("payTo")!=null) {
						pettyCashExpenseDTO.setPayTo(queryDataMap.get("payTo").toString());
					}else {
						pettyCashExpenseDTO.setPayTo(queryDataMap.get("payFor").toString());
					}
					if(queryDataMap.get("supplierId")!=null) {
						Long supplierId=Long.valueOf(queryDataMap.get("supplierId").toString());
					/*	SupplierMaster supplierMaster=supplierMasterRepository.findOne(supplierId);*/
						SupplierMaster supplierMaster=supplierMasterRepository.getSupplierMasterBySupplierID(supplierId);
						pettyCashExpenseDTO.setSupplierMaster(supplierMaster);
					}
					if(queryDataMap.get("employeeId")!=null) {
						Long employeeId=Long.valueOf(queryDataMap.get("employeeId").toString());
						EmployeeMaster empMaster=employeeMasterRepository.findOne(employeeId);
						pettyCashExpenseDTO.setEmployeeMaster(empMaster);
					}
					if(queryDataMap.get("payFor")!=null) {
						pettyCashExpenseDTO.setPayName(queryDataMap.get("payFor").toString());
					}
					if(queryDataMap.get("createdBy")!=null) {
						pettyCashExpenseDTO.setCreatedBy(queryDataMap.get("createdBy").toString());
					}
					if(queryDataMap.get("createdDate")!=null) {
						pettyCashExpenseDTO.setCreatedDate((Date)queryDataMap.get("createdDate"));
					}
					if(queryDataMap.get("modifiedBy")!=null) {
						pettyCashExpenseDTO.setModifiedBy(queryDataMap.get("modifiedBy").toString());
					}
					if(queryDataMap.get("modifiedDate")!=null) {
						pettyCashExpenseDTO.setModifiedDate((Date)queryDataMap.get("modifiedDate"));
					}
					if(queryDataMap.get("voucherNarration").toString()!=null) {
						pettyCashExpenseDTO.setVoucherNarration(queryDataMap.get("voucherNarration").toString());
					}
					if(queryDataMap.get("paymentMode").toString()!=null) {
						Long paymentmodeId=Long.valueOf(queryDataMap.get("paymentMode").toString());
						PaymentMode paymentMode=paymentModeRepository.findOne(paymentmodeId);
						pettyCashExpenseDTO.setPaymentMode(paymentMode);
					}
					if(queryDataMap.get("voucherNote").toString()!=null) {
						Long vouchernoteId=Long.valueOf(queryDataMap.get("voucherNote").toString());
						VoucherNote voucherNote=voucherNoteRepository.findOne(vouchernoteId);
						pettyCashExpenseDTO.setFinalApproval(voucherNote.getFinalApproval());
						pettyCashExpenseDTO.setNote(voucherNote.getNote());
						UserMaster userMaster=userMasterRepository.findOne(voucherNote.getForwardTo().getId());
						pettyCashExpenseDTO.setForwardTo(userMaster);
					}
					if(queryDataMap.get("status").toString()!=null) {
						pettyCashExpenseDTO.setStatus(queryDataMap.get("status").toString());
					}
					ExpenseDetails expenseDetails=new ExpenseDetails();
					if(queryDataMap.get("amount").toString()!=null) {
						expenseDetails.setExpenseAmount(Double.valueOf(queryDataMap.get("amount").toString()));
					}
					if(queryDataMap.get("description").toString()!=null) {
						expenseDetails.setNarration(queryDataMap.get("description").toString());
					}
					if(queryDataMap.get("glAccount").toString()!=null) {
						Long glaccount=Long.valueOf(queryDataMap.get("glAccount").toString());
						GlAccount glAccounts=glAccountRepository.findOne(glaccount);
						expenseDetails.setAccount(glAccounts);
					}
					expenseDetailsList.add(expenseDetails);
				}
				pettyCashExpenseDTO.setTotalAmount(expenseDetailsList.stream().mapToDouble(ExpenseDetails::getExpenseAmount).sum());
//				pettyCashExpenseDTO.setTotalAmount(voucherRepository.getNetAmountByVoucherId(id));
				pettyCashExpenseDTO.setTotalAdjustmentAmount(staffInvoiceAdjustmentRepository.getStaffInvoiceAdjAmountByvoucherID(id));
					

				log.info(":::::totalAmount::::::"+pettyCashExpenseDTO.getTotalAmount());
				pettyCashExpenseDTO.setExpenseDetailsList(expenseDetailsList);
				baseDTO.setResponseContent(pettyCashExpenseDTO);
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
			
			Voucher voucherObj= voucherRepository.findOne(id);
			List<Map<String, Object>> employeeData = new ArrayList<Map<String, Object>>();
			log.info("<<<:::::::VoucherObj::::::::>>>>"+voucherObj);
			if(voucherObj!=null) {
				 ApplicationQuery applicationQueryForlog = applicationQueryRepository.findByQueryName("PETTY_CASH_EXPENSE_LOG_EMPLOYEE_DETAILS");
				 if (applicationQueryForlog == null || applicationQueryForlog.getId() == null) {
					 log.info("Application Query For Log Details not found for query name : " + applicationQueryForlog);
					 baseDTO.setStatusCode(ErrorDescription.APPLICATION_QUERY_NOT_FOUND.getCode());
					 return baseDTO;
				 }
				 String logquery = applicationQueryForlog.getQueryContent().trim();
				 log.info("<=========PETTY_CASH_EXPENSE_LOG_EMPLOYEE_DETAILS Query Content ======>"+logquery);
				 logquery = logquery.replace(":voucherId", "'"+id.toString()+"'");
				 log.info("Query Content For PETTY_CASH_EXPENSE_LOG_EMPLOYEE_DETAILS After replaced plan id View query : " + logquery);
				 employeeData = jdbcTemplate.queryForList(logquery);
				 log.info("<=========PETTY_CASH_EXPENSE_LOG_EMPLOYEE_DETAILS Employee Data======>"+employeeData);
				 baseDTO.setTotalListOfData(employeeData);
			}
			if (notificationId != null && notificationId>0) {
				SystemNotification systemNotification = systemNotificationRepository
						.findOne(notificationId);
				systemNotification.setNotificationRead(true);
				systemNotificationRepository.save(systemNotification);
			}
			
			
		} catch (RestException restException) {
			baseDTO.setStatusCode(restException.getStatusCode());
			log.error("RestException in SalesPaymentCollectionService ", restException);
		} catch (Exception exception) {
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.error("Exception in SalesPaymentCollectionService ", exception);
		}
		return baseDTO;
	}
	
	public BaseDTO approvePettyCashExpensePayment(PettyCashExpenseDTO pettyCashExpenseDTO) {
		BaseDTO baseDTO = new BaseDTO();
		log.info("-----approvePettyCashExpensePayment method start-------");
		try {
			if (pettyCashExpenseDTO.getId() != null) {
				Voucher voucher = voucherRepository
						.getOne(pettyCashExpenseDTO.getId());
				log.info("voucher id------------" + voucher);

				VoucherNote voucherNote = new VoucherNote();
				voucherNote.setForwardTo(userMasterRepository.findOne(pettyCashExpenseDTO.getForwardTo().getId()));
				log.info("voucherNote forwardTo user---------------->" + voucherNote.getForwardTo().getId());
				voucherNote.setFinalApproval(pettyCashExpenseDTO.getFinalApproval());
				voucherNote.setNote(pettyCashExpenseDTO.getNote());
				voucherNote.setVoucher(voucher);
				voucherNoteRepository.save(voucherNote);
				log.info("------------voucherNote table Inserted----------------");

				VoucherLog voucherLog = new VoucherLog();
				voucherLog.setRemarks(pettyCashExpenseDTO.getVoucherLog().getRemarks());
				voucherLog.setVoucher(voucher);
				voucherLog.setStatus(pettyCashExpenseDTO.getVoucherLog().getStatus());
				voucherLog.setUserMaster(userMasterRepository.findOne(pettyCashExpenseDTO.getForwardTo().getId()));
				voucherLogRepository.save(voucherLog);
				log.info("------------voucherLog table Inserted----------------");

				Map<String, Object> additionalData = new HashMap<>();

				additionalData.put("Url", VIEW_PETTY_CASH_EXPENSE+"&id="+ voucher.getId() + "&");
				notificationEmailService.sendMailAndNotificationForForward(voucherNote, voucherLog,
						additionalData);
				
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (Exception e) {
			log.error("approvePettyCashExpensePayment exception ------", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("-----approvePettyCashExpensePayment method end-------");
		return baseDTO;
	}
	
	public BaseDTO rejectPettyCashExpensePayment(PettyCashExpenseDTO pettyCashExpenseDTO) {
		BaseDTO baseDTO = new BaseDTO();
		log.info("-----rejectPettyCashExpensePayment method start-------");
		try {
			if (pettyCashExpenseDTO.getId() != null) {
				Voucher voucher = voucherRepository.getOne(pettyCashExpenseDTO.getId());
				log.info("rejectPettyCashExpensePayment voucher id------------" + pettyCashExpenseDTO.getId());

				VoucherLog voucherLog = new VoucherLog();
				voucherLog.setRemarks(pettyCashExpenseDTO.getVoucherLog().getRemarks());
				voucherLog.setVoucher(voucher);
				voucherLog.setStatus(pettyCashExpenseDTO.getVoucherLog().getStatus());
				voucherLog.setUserMaster(userMasterRepository.findOne(loginService.getCurrentUser().getId()));
				voucherLogRepository.save(voucherLog);
				log.info("------------voucherLog table Inserted----------------");

				Map<String, Object> additionalData = new HashMap<>();

				additionalData.put("Url", VIEW_PETTY_CASH_EXPENSE+"&id="+ voucher.getId() + "&");
				notificationEmailService.sendMailAndNotificationForForward(null, voucherLog,
						additionalData);
				
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			}
		} catch (Exception e) {
			log.error("rejectPettyCashExpensePayment exception ------", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("-----rejectPettyCashExpensePayment method end-------");
		return baseDTO;
	}
	
	public BaseDTO updatePettyCashExpense(PettyCashExpenseDTO pettyCashExpenseDTO){
		log.info("<============== PettyCashExpenseService:updatePettyCashExpense ================>");
		BaseDTO baseDTO=new BaseDTO();
		List<ExpenseDetails> expenseDetailsList = new ArrayList<ExpenseDetails>();
		List<VoucherDetails> voucherDetailsList=new ArrayList<>();
		try {
			if(pettyCashExpenseDTO.getId()!=null) {
				EntityMaster entityMaster = entityMasterRepository.findByUserRegion(loginService.getCurrentUser().getId());
				SequenceConfig sequenceConfig = sequenceConfigRepository.findBySequenceName(SequenceName.valueOf(SequenceName.PETTY_CASH_REF_NUMBER.name()));
				if (sequenceConfig == null) {
					throw new RestException(ErrorDescription.SEQUENCE_CONFIG_NOT_EXIST);
				}
				String referenceNumber=entityMaster.getCode() + sequenceConfig.getSeparator()+
						sequenceConfig.getPrefix()+ AppUtil.getCurrentYearString() + AppUtil.getCurrentMonthString();
				String expenseDetailsRefNumber = referenceNumber+sequenceConfig.getCurrentValue();
				Voucher voucher=voucherRepository.findOne(pettyCashExpenseDTO.getId());
//				voucher.setNetAmount(pettyCashExpenseDTO.getTotalAmount());
				voucher.setNetAmount(pettyCashExpenseDTO.getBalancePaymentAmount());
				voucher.setNarration(pettyCashExpenseDTO.getVoucherNarration());
				voucher.setFromDate(new Date());
				voucher.setToDate(new Date());
				voucher.setPaymentMode(pettyCashExpenseDTO.getPaymentMode());
				List<ExpenseDetails> expenseDetailsLists=expenseDetailsRepository.findByVoucherId(pettyCashExpenseDTO.getId());
				if(expenseDetailsLists.size()>0) {
					expenseDetailsRepository.deleteInBatch(expenseDetailsLists);
				}
				List<VoucherDetails> voucherDetailsLists=voucherDetailsRepository
						.findVoucherDetailsByVoucherId(pettyCashExpenseDTO.getId());
				if(voucherDetailsLists.size()>0) {
					voucherDetailsRepository.deleteInBatch(voucherDetailsLists);
				}
				for(ExpenseDetails expenseDetails:pettyCashExpenseDTO.getExpenseDetailsList()){
					ExpenseDetails expenseDetailsObj=new ExpenseDetails();
					expenseDetailsObj.setEntityMaster(entityMaster);
					if(pettyCashExpenseDTO.getEmployeeMaster()!=null) {
						expenseDetailsObj.setEmpMaster(employeeMasterRepository.findById(pettyCashExpenseDTO.getEmployeeMaster().getId()));
						expenseDetailsObj.setPayTo(PettyCashPayFor.EMPLOYEE);
					}
					expenseDetailsObj.setRefNumber(expenseDetailsRefNumber);
					expenseDetailsObj.setExpenseDate(new Date());
					if(expenseDetails.getExpenseAmount()!=null) {
						expenseDetailsObj.setExpenseAmount(expenseDetails.getExpenseAmount());
					}
					if(pettyCashExpenseDTO.getExpenseType()!=null) {
						expenseDetailsObj.setExpenseType(expenseTypeRepository.findOne(pettyCashExpenseDTO.getExpenseType().getId()));
					}
					expenseDetailsObj.setNarration(expenseDetails.getNarration());
					if(expenseDetails.getAccount()!=null) {
						expenseDetailsObj.setAccount(glAccountRepository.findById(expenseDetails.getAccount().getId()));
					}
					expenseDetailsObj.setExpenseTransactionName(pettyCashExpenseDTO.getExpenseTransactionName());
					if(pettyCashExpenseDTO.getSupplierMaster()!=null) {
						expenseDetailsObj.setPayTo(PettyCashPayFor.SUPPLIER);
						expenseDetailsObj.setSupplierMaster(supplierMasterRepository.findOne(pettyCashExpenseDTO.getSupplierMaster().getId()));
					}
					if(pettyCashExpenseDTO.getExpenseReason()!=null) {
						expenseDetailsObj.setExpenseReason(expenseReasonRepository.findOne(pettyCashExpenseDTO.getExpenseReason().getId()));
					}
					if(pettyCashExpenseDTO.getPayFor().equals(PettyCashPayFor.OTHER)) {
						expenseDetailsObj.setPayTo(PettyCashPayFor.OTHER);
						expenseDetailsObj.setPayFor(pettyCashExpenseDTO.getPayName());
					}
					if(pettyCashExpenseDTO.getPaymentMode()!=null) {
						expenseDetailsObj.setPaymentMode(pettyCashExpenseDTO.getPaymentMode());
					}
					expenseDetailsObj.setVoucher(voucher);
					expenseDetailsList.add(expenseDetailsObj);
					//Voucher Details
					VoucherDetails voucherDetails = new VoucherDetails();
					voucherDetails.setVoucher(voucher);
					voucherDetails.setAmount(expenseDetails.getExpenseAmount());
					if(expenseDetails.getAccount()!=null) {
						voucherDetails.setGlAccount(glAccountRepository.findById(expenseDetails.getAccount().getId()));
					}
					voucherDetailsList.add(voucherDetails);
				}
				voucherDetailsRepository.save(voucherDetailsList);
				//Voucher Log
				VoucherLog voucherLog = new VoucherLog();
				voucherLog.setVoucher(voucher);
				voucherLog.setStatus(VoucherStatus.SUBMITTED);
				UserMaster userMaster = userMasterRepository.findOne(loginService.getCurrentUser().getId());
				voucherLog.setUserMaster(userMaster);
				
				//Voucher Note
				VoucherNote voucherNote = new VoucherNote();
				voucherNote.setNote(pettyCashExpenseDTO.getNote());
				voucherNote.setFinalApproval(pettyCashExpenseDTO.getFinalApproval());
				if(pettyCashExpenseDTO.getForwardTo()!=null) {
					UserMaster forwardTo=userMasterRepository.findOne((pettyCashExpenseDTO.getForwardTo().getId()));
					voucherNote.setForwardTo(forwardTo);
				}
				voucherNote.setCreatedBy(userMaster);
				voucherNote.setCreatedByName(userMaster.getUsername());
				voucherNote.setCreatedDate(new Date());
				voucherNote.setVoucher(voucher);

				//Staff Invoice Adjustment Edit
				StaffInvoiceAdjustment staffInvoiceAdjustment = staffInvoiceAdjustmentRepository
						.findStaffInvoiceAdjByvoucherID(pettyCashExpenseDTO.getId());
				if (staffInvoiceAdjustment != null) {
					if (pettyCashExpenseDTO.getTotalAdjustmentAmount() != 0.0
							&& pettyCashExpenseDTO.getTotalAdjustmentAmount() != null) {
						staffInvoiceAdjustment.setAdjustedAmount(pettyCashExpenseDTO.getTotalAdjustmentAmount());
						staffInvoiceAdjustment.setEmpMaster(employeeMasterRepository
								.findById(pettyCashExpenseDTO.getLoggedInEmployeeMaster().getId()));
						staffInvoiceAdjustment.setAdvanceType(VoucherTypeDetails.PETTY_CASH_EXPENSE.toString());
						staffInvoiceAdjustmentRepository.save(staffInvoiceAdjustment);
					} else {
						staffInvoiceAdjustmentRepository.delete(staffInvoiceAdjustment);
					}
				} else {
					if (pettyCashExpenseDTO.getTotalAdjustmentAmount() != null && pettyCashExpenseDTO.getTotalAdjustmentAmount() != 0.0) {
						staffInvoiceAdjustment = new StaffInvoiceAdjustment();
						staffInvoiceAdjustment.setAdjustedAmount(pettyCashExpenseDTO.getTotalAdjustmentAmount());
						staffInvoiceAdjustment.setEmpMaster(employeeMasterRepository
								.findById(pettyCashExpenseDTO.getLoggedInEmployeeMaster().getId()));
						staffInvoiceAdjustment.setAdvanceType(VoucherTypeDetails.PETTY_CASH_EXPENSE.toString());
						staffInvoiceAdjustment.setVoucher(voucher);
						staffInvoiceAdjustmentRepository.save(staffInvoiceAdjustment);
					} else {
						log.info("::::::::::AdjustedAmount Not Given::::::::::");
					}
				}

				voucherLogRepository.save(voucherLog);
				voucherNoteRepository.save(voucherNote);
				voucherRepository.save(voucher);
				expenseDetailsRepository.save(expenseDetailsList);
				sequenceConfig.setCurrentValue(sequenceConfig.getCurrentValue() + 1);
				sequenceConfigRepository.save(sequenceConfig);
				
				Map<String, Object> additionalData = new HashMap<>();

				additionalData.put("Url", VIEW_PETTY_CASH_EXPENSE+"&id="+ voucher.getId() + "&");
				notificationEmailService.sendMailAndNotificationForForward(voucherNote, voucherLog,
						additionalData);
				log.info("Start PettycashService.updatePettyCashExpense");
			}
		}catch (RestException restException) {
			log.error("PettyCashExpenseService updatePettyCashExpense RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("PettyCashExpenseService updatePettyCashExpense Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	public BaseDTO getAllExpenseReason() {
		log.info("==============PettyCashExpenseService:getAllExpenseReason================");
		BaseDTO baseDTO = new BaseDTO();
		try {
			List<ExpenseReason> expenseReasonList = expenseReasonRepository.findAll();
			baseDTO.setResponseContent(expenseReasonList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		}catch (RestException restException) {
			log.error("PettyCashExpenseService getAllExpenseReason RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("PettyCashExpenseService getAllExpenseReason Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}

	
	@Autowired
	EmployeePersonalInfoEmploymentRepository employeePersonalInfoEmploymentRepository;
	
	public BaseDTO getSectionDetails(String empCode) {

		log.info("==============PettyCashExpenseService:getSectionDetails================");
		BaseDTO baseDTO = new BaseDTO();

		try {
			Long empId=employeeMasterRepository.getEmpIdByCode(empCode);
			EmployeePersonalInfoEmployment emppersonalInfoempmaster = employeePersonalInfoEmploymentRepository.findyByEmployeeId(empId);
			if (emppersonalInfoempmaster != null) {
				if (emppersonalInfoempmaster.getCurrentSection() != null) {
					baseDTO.setGeneralContent(
							emppersonalInfoempmaster.getCurrentSection().getCode() + " / " + emppersonalInfoempmaster.getCurrentSection().getName());
					baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				} else {
					baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
				}
			}

		} catch (Exception e) {
			log.info("============== Exception Occured In PettyCashExpenseService:getSectionDetails================",
					e);
		}
		return baseDTO;
	}

	public BaseDTO getAllPurchaseSalesAdvancebyEmpId(Long empid) {
		log.info("==============PettyCashExpenseService:getAllExpenseReason================"+empid);
		BaseDTO baseDTO = new BaseDTO();
		Double staffInvoiceAdjAmount=0.0;
		List<PettyCashAdjustmentDetailsDTO> pettyCashAdjustmentDetailsDTOList = new ArrayList<>();
		try {
			List<PurchaseSalesAdvance> purchaseSalesAdvanceList = purchaseSalesAdvanceRepository.getAllPurchaseSalesAdvancebyEmpId(empid);
			staffInvoiceAdjAmount=staffInvoiceAdjustmentRepository.getStaffInvoiceAdjAmountByEmpID(empid);
			
			for (PurchaseSalesAdvance purchaseSalesAdvanceObj : purchaseSalesAdvanceList) {
				PettyCashAdjustmentDetailsDTO pettyCashAdjustmentDetailsDTO= new PettyCashAdjustmentDetailsDTO();
				pettyCashAdjustmentDetailsDTO.setAdjustedAmount(staffInvoiceAdjAmount);
				pettyCashAdjustmentDetailsDTO.setPurchaseSalesAdvanceId(purchaseSalesAdvanceObj.getId());
				pettyCashAdjustmentDetailsDTO.setAmount(purchaseSalesAdvanceObj.getAmount());
				pettyCashAdjustmentDetailsDTO.setAdvanceDate(purchaseSalesAdvanceObj.getCreatedDate());
				pettyCashAdjustmentDetailsDTOList.add(pettyCashAdjustmentDetailsDTO);
			}
			
			baseDTO.setResponseContent(pettyCashAdjustmentDetailsDTOList);
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		}catch (RestException restException) {
			log.error("PettyCashExpenseService getAllExpenseReason RestException ", restException);
			baseDTO.setStatusCode(restException.getStatusCode());
		} catch (Exception exception) {
			log.error("PettyCashExpenseService getAllExpenseReason Exception ", exception);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return baseDTO;
	}
	
	
}