package in.gov.cooptex.finance.repository;

import in.gov.cooptex.core.accounts.model.StopReleasePayment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SocietyStopInvoiceRepository extends JpaRepository<StopReleasePayment, Long> {
}
