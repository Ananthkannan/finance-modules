package in.gov.cooptex.finance.repository;

import in.gov.cooptex.core.accounts.model.StopReleasePaymentDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;


public interface StopReleasePaymentDetailsRepository extends JpaRepository<StopReleasePaymentDetails,Long> {


    @Modifying
    @Transactional
    @Query(value ="update stop_release_payment_details  set date_released = :dateReleased where purchase_invoice_id = :invoiceId ",nativeQuery = true)
    void updateStopReleasePaymentDetails(@Param("invoiceId") Long invoiceId, @Param("dateReleased") Date dateReleased);

    @Query(value = "select s from StopReleasePaymentDetails s where s.dateReleased is null")
    List<StopReleasePaymentDetails> getAllStoppedInvoiceDetails();

    @Query("select s from StopReleasePaymentDetails s where s.purchaseInvoice.id = :purchaseInvoiceId")
    List<StopReleasePaymentDetails> findByPurchaseInvoiceId(@Param( "purchaseInvoiceId") Long purchaseInvoiceId);



}
