package in.gov.cooptex.finance.repository;

import in.gov.cooptex.core.accounts.model.PurchaseInvoiceItems;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PurchaseInvoiceItemsRepository extends JpaRepository<PurchaseInvoiceItems, Long> {

    @Query("select pii from PurchaseInvoiceItems pii where pii.purchaseInvoice.id = :purchaseInvoiceId")
    List<PurchaseInvoiceItems> findByPurchaseInvoiceId(@Param( "purchaseInvoiceId" ) Long purchaseInvoiceId);
    
    @Query("select pii.purchaseInvoice.id from PurchaseInvoiceItems pii where pii.productVarietyMaster.id = :productVarietyId")
    List<Long> findByProductVarietyId(@Param( "productVarietyId" ) Long productVarietyId);
    
} 
