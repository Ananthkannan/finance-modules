package in.gov.cooptex.finance.repository;

import in.gov.cooptex.core.accounts.model.StopReleasePaymentDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SocietyStopInvoiceDetailsRepository extends JpaRepository<StopReleasePaymentDetails, Long> {

    @Query("select st from StopReleasePaymentDetails st where st.purchaseInvoice.id = :purchaseInvoiceId")
    List<StopReleasePaymentDetails> findByPurchaseInvoiceId(@Param( "purchaseInvoiceId" ) Long purchaseInvoiceId);
    
    @Query(value = "select srd.purchase_invoice_id from stop_release_payment_details srd where srd.stop_release_payment_id=:stopreleasepaymentID group by purchase_invoice_id", nativeQuery = true)
	List<Integer> getpurchaseInvoiceIdlistbystopreleasepaymentID(@Param("stopreleasepaymentID") Long stopreleasepaymentID);
    
}
