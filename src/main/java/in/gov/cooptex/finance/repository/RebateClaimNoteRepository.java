package in.gov.cooptex.finance.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import in.gov.cooptex.core.accounts.model.RebateClaimNote;

public interface RebateClaimNoteRepository extends JpaRepository<RebateClaimNote, Long> {

	RebateClaimNote getById(Long rebateClaimId);

	@Query("select rc,rc.rebateClaim,rc.forwardTo from RebateClaimNote rc   where rc.rebateClaim.id=?1")
	RebateClaimNote getRebateClaimNote(Long long1);

	@Query("select rc from RebateClaimNote rc where rc.rebateClaim.id=?1")
	RebateClaimNote getRebateClaimData(Long id);

}
