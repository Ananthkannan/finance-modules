package in.gov.cooptex.finance.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import in.gov.cooptex.core.accounts.model.RebateClaimCollectionLog;


public interface RebateClaimCollectionLogRepository extends JpaRepository<RebateClaimCollectionLog, Long> {

}
