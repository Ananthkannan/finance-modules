package in.gov.cooptex.finance.repository;

import in.gov.cooptex.core.accounts.model.AmountTransferLog;
import in.gov.cooptex.core.accounts.model.AmountTransferNote;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import java.util.List;


@Repository
public interface AmountTransferLogRepository extends JpaRepository<AmountTransferLog, Long> {

    @Query(nativeQuery = true,value="select * from amount_transfer_log where amount_transfer_id = ?1 ORDER BY id DESC LIMIT 1")
    AmountTransferLog findByAmountTransferId(Long amountTransferId);

//
////    @Query("select * from amount_transfer_log where amount_transfer_id=14 order by created_date desc limit 1 ;")
////    List<AmountTransferLog> getSingle();
//    @Query("select atd.id,bankmas.branch_name as frombbranch,amounttr.total_amount_transfered,empmas.first_name,amounttr.created_date,atl.stage,bankmaster.branch_name as tobranch  \n"+
//                  "            from amount_transfer_details atd inner join entity_bank_branch ebb on atd.from_branch_id=ebb.id inner join entity_bank_branch entbb on ebb.branch_id=entbb.id \n"+
//                  "            inner join bank_branch_master bankbrm on entbb.branch_id=bankbrm.id inner join bank_master bankmas on bankbrm.bank_id=bankmas.id\n"+
//                  "            inner join amount_transfer amounttr on amounttr.id=atd.amount_transfer_id inner join emp_master empmas on  amounttr.transfered_by =empmas.id \n"+
//                  "            inner join entity_bank_branch ebbt on atd.to_branch_id=ebbt.id and  ebbt.branch_id=entbb.id and entbb.branch_id=bankbrm.id\n"+
//                  "            inner join bank_master bankmaster on bankbrm.bank_id=bankmaster.id\n"+
//                  "            left join amount_transfer_log atl on amounttr.id=atl.amount_transfer_id where upper(amounttr.movement_type)=upper('banktobank')")
//    List<AmountTransferLog> getBankToBankAll();
    @Query(value="SELECT atl FROM AmountTransferLog atl WHERE atl.amountTransfer.id = :amtTransferId")
    AmountTransferLog getAmountTransferLogDetails(@Param("amtTransferId") Long id);

    @Modifying
	@Transactional
	@Query(nativeQuery=true, value="DELETE FROM amount_transfer_log WHERE amount_transfer_id=:amountTransferId AND stage=:stage")
	void deleteByAmountTransferId(@Param("amountTransferId") Long amountTransferId, @Param("stage") String stage);
}
