package in.gov.cooptex.finance.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import in.gov.cooptex.core.accounts.model.RebateClaimCollectionNote;

public interface RebateClaimCollectionNoteRepository extends JpaRepository<RebateClaimCollectionNote, Long> {

}
