package in.gov.cooptex.finance.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import in.gov.cooptex.core.accounts.model.StopReleasePaymentNote;
import org.springframework.data.repository.query.Param;

public interface StopReleasePaymentNoteRepository extends JpaRepository<StopReleasePaymentNote, Long> {

	
	@Query(nativeQuery = true, value = "select * from stop_release_payment_note where stop_release_payment_id=:stopReleasePaymentId and action_name='STOPPED'")
	StopReleasePaymentNote findReleasePaymentNoteByResId(@Param("stopReleasePaymentId") Long stopReleasePaymentId);
	
	@Query(nativeQuery = true, value = "select * from stop_release_payment_note where stop_release_payment_id=:stopReleasePaymentId and action_name='RELEASED'")
	StopReleasePaymentNote findReleasePaymentNoteByReleaseId(@Param("stopReleasePaymentId") Long stopReleasePaymentId);
}
