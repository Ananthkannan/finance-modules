package in.gov.cooptex.finance.repository;

import in.gov.cooptex.core.accounts.model.StopReleasePaymentLog;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface StopReleasePaymentLogRepository extends JpaRepository<StopReleasePaymentLog, Long> {

	// StopReleasePaymentLog findReleasePaymentLogByResId(Long
	// stopReleasePaymentId);

	@Query(nativeQuery = true, value = "select * from stop_release_payment_log where stop_release_payment_id=:stopReleasePaymentId and action_name='STOPPED'")
	List<StopReleasePaymentLog> findReleasePaymentLogByResId(@Param("stopReleasePaymentId") Long stopReleasePaymentId);

	@Query(nativeQuery = true, value = "select * from stop_release_payment_log where stop_release_payment_id=:stopReleasePaymentId and action_name='RELEASED'")
	List<StopReleasePaymentLog> findReleasePaymentLogByReleaseId(
			@Param("stopReleasePaymentId") Long stopReleasePaymentId);
}
