package in.gov.cooptex.finance.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import in.gov.cooptex.core.accounts.model.RebateClaimLog;

@Repository
public interface RebateClaimLogRepository extends JpaRepository<RebateClaimLog, Long> {

	@Query("select t1.stage from RebateClaimLog t1 where t1.rebateClaim.id=?1")
	List<String> getMaxApprovedStatus(Long id);

	@Query("select t1 from RebateClaimLog t1 where t1.rebateClaim.id=?1")
	RebateClaimLog getRebateClaimData(Long id);

}
