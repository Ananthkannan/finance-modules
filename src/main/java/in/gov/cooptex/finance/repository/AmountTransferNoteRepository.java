package in.gov.cooptex.finance.repository;

import in.gov.cooptex.core.accounts.model.AmountTransferNote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AmountTransferNoteRepository extends JpaRepository<AmountTransferNote,Long> {
    @Query(nativeQuery = true,value="select * from amount_transfer_note where amount_transfer_id = ?1 ORDER BY id DESC LIMIT 1")
    AmountTransferNote findByAmountTransferId(Long amountTransferId);
    
    @Query(value="SELECT atn FROM AmountTransferNote atn WHERE atn.amountTransfer.id = :amtTransferId")
    AmountTransferNote getAmountTransferNoteDetails(@Param("amtTransferId") Long id);
}
