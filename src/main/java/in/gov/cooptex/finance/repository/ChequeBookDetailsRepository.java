package in.gov.cooptex.finance.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import in.gov.cooptex.core.accounts.model.ChequeBook;
import in.gov.cooptex.core.model.ProfitMarginMaster;
import in.gov.cooptex.operation.model.SupplierMaster;

public interface ChequeBookDetailsRepository extends JpaRepository<ChequeBook, Long> {
	
	
	@Query("SELECT cb FROM ChequeBook cb WHERE cb.chequeBookNumber =:chequeBookNumber")
	List<ChequeBook> findchequeBookNumber(@Param("chequeBookNumber")String chequeBookNumber);
	
	@Query(nativeQuery=true,value="select min(cb.cheque_number) from cheque_book cb where cb.cheque_book_number=?1 ")
      Long findchequeNumberFrom(String chequeBookNumber);
	

	@Query(nativeQuery=true,value="select  max(cb.cheque_number) from cheque_book cb where cb.cheque_book_number=?1 ")
      Long findchequeNumberTo(String chequeBookNumber);
	
	@Modifying
	@Transactional
	@Query("delete FROM ChequeBook cb WHERE cb.chequeBookNumber =:chequeBookNumber")
	void deleteBychequeBookNumber(@Param("chequeBookNumber")String chequeBookNumber);

	
	@Query(nativeQuery=true,value="select * from cheque_book where entity_bank_branch_id=:entityBrand_id and cheque_book_number=:checkBookNo and cheque_number between :from and :to limit 1")
	List<ChequeBook> findListOfCheckBookRange(@Param("entityBrand_id") Long entityBrand_id,@Param("checkBookNo") String checkBookNo, @Param("from")Long from,@Param("to") Long to);

	
	
	@Query("SELECT cb FROM ChequeBook cb WHERE cb.chequeBookNumber = :chequeBookNumber")
	List<ChequeBook> getallCheckBookNo(@Param("chequeBookNumber") String chequeBookNumber);

}
