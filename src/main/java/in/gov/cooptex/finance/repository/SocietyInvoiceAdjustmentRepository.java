package in.gov.cooptex.finance.repository;

import in.gov.cooptex.core.accounts.model.SocietyInvoiceAdjustment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface SocietyInvoiceAdjustmentRepository extends JpaRepository<SocietyInvoiceAdjustment, Long> {

	SocietyInvoiceAdjustment findById(Long id); 

	List<SocietyInvoiceAdjustment> findBySocietyId(Long societyId);

    @Query(nativeQuery = true, value = "select * from society_invoice_adjustment sia where sia.society_id = :societyId limit 1")
    SocietyInvoiceAdjustment findBySocietyIdlimit(@Param("societyId") Long societyId);
	
	List<SocietyInvoiceAdjustment> findAllBySocietyId(Long societyId);
	
	@Query("SELECT sia FROM SocietyInvoiceAdjustment sia WHERE DATE(sia.createdDate)<=DATE(:invoiceToDate)")
	List<SocietyInvoiceAdjustment> findSocietyAdjustmentListbyTodate(@Param("invoiceToDate") Date invoiceToDate);
	
	@Query("SELECT sia FROM SocietyInvoiceAdjustment sia WHERE DATE(sia.createdDate)<=DATE(:invoiceToDate) AND sia.societyId in (:societyIds)")
	List<SocietyInvoiceAdjustment> findSocietyAdjustmentListbyTodateAndSocietyIds(@Param("invoiceToDate") Date invoiceToDate, @Param("societyIds") List<Long> societyIds);

	// @Query("SELECT sia FROM SocietyInvoiceAdjustment sia WHERE sia.id in
	// (:societyCode) AND sia.purchaseInvoice.invoiceIssuedToProductwareHouse.id in
	// (:regionCode)" +
	// " AND sia.purchaseInvoice.createdDate >= :invoiceFromDate and
	// sia.purchaseInvoice.createdDate <= :invoiceToDate" ) 
	// List<SocietyInvoiceAdjustment> searchByParameters(@Param("regionCode")
	// List<String> regionCode, @Param("societyCode") List<String> societyCode,
	// @Param("invoiceFromDate") Date invoiceFromDate, @Param("invoiceToDate") Date
	// invoiceToDate);
}
