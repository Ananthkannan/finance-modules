package in.gov.cooptex.finance.excel.report;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;


import in.gov.cooptex.mis.dto.BaseReportDto;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class EmployeeRetirmentExcelReport {

	public String generateExcelReport(List<Map<String, Object>> reportList, BaseReportDto baseReport,
			String uploadFilePath, String uploadFileUrl) {

		SXSSFWorkbook workbook = null;
		Sheet dataSheet;
		List<String> showColumn = baseReport.getColumnsMap();
		int numOfColumns = showColumn.size();
		log.info("Number of Columns to show are " + numOfColumns);
		workbook = new SXSSFWorkbook(-1);
		dataSheet = workbook.createSheet();
		Integer listSize = reportList.size();
		if (reportList != null && listSize > 0) {
			int cell = 1;
			int i = 0;
			try {
				Map<String, Object> dataSheetRowMap = CommonExcelExportUtil.generateExcelReport(baseReport,
						numOfColumns, workbook, dataSheet);
				Row dataRow1 = (Row) dataSheetRowMap.get("Row");
				dataSheet = (Sheet) dataSheetRowMap.get("Sheet");
				CellStyle headercellStyle = CommonExcelExportUtil.createHeaderStyle(workbook);
				dataRow1 = CommonExcelExportUtil.getRow(dataSheet, i + 7);

				CommonExcelExportUtil.getCell(dataRow1, 0).setCellValue("#");
				CommonExcelExportUtil.getCell(dataRow1, 0).setCellStyle(headercellStyle);
				for (int hCell = 1; hCell <= numOfColumns; hCell++) {
					// dataSheet.autoSizeColumn(hCell - 1);
					if ("Employee_Name".equals(showColumn.get(hCell - 1))) {
						CommonExcelExportUtil.getCell(dataRow1, hCell)
								.setCellValue("Employee Code / Name");
					} else {
						CommonExcelExportUtil.getCell(dataRow1, hCell)
								.setCellValue(showColumn.get(hCell - 1).replace("_", " "));
					}
					CommonExcelExportUtil.getCell(dataRow1, hCell).setCellStyle(headercellStyle);
				}

				/*
				 * Based on Reflection methods will be invoked here.
				 */

				int serialNumber = 1;

				Map<Integer, Object> value = new HashMap<Integer, Object>();

				for (Map<String, Object> data : reportList) {

					int row = i + 8;

					dataRow1 = CommonExcelExportUtil.getRow(dataSheet, row);
					CommonExcelExportUtil.getCell(dataRow1, 0).setCellValue(serialNumber);

					for (String colName : showColumn) {

						if (data != null) {
							if (data.get(colName) == null) {
								Cell c = CommonExcelExportUtil.getCell(dataRow1, cell++);
								c.setCellType(Cell.CELL_TYPE_BLANK);
							} else {
								Cell c = CommonExcelExportUtil.getCell(dataRow1, cell);
								if ("date_of_joining".equals(colName) || "retirement_date".equals(colName)
										|| "due_by".equals(colName) || "due_to".equals(colName)) {
									c.setCellType(Cell.CELL_TYPE_NUMERIC);
									if (data.get(colName).toString().isEmpty()) {
										c.setCellType(Cell.CELL_TYPE_BLANK);
									} else {
										DateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
										CreationHelper createHelper = workbook.getCreationHelper();
										short dateFormat = createHelper.createDataFormat().getFormat("dd-MM-yyyy");
										CellStyle cellStyle = workbook.createCellStyle();
										cellStyle.setDataFormat(dateFormat);
										cellStyle.setAlignment(HorizontalAlignment.LEFT);
										c.setCellStyle(cellStyle);
										Date d = sdf.parse(data.get(colName).toString());
										c.setCellValue(d);

									}
								}

								else {
									c.setCellValue(data.get(colName).toString());
								}
								cell++;
							}
						}
					}

					if (row % 100 == 0) {
						((SXSSFSheet) dataSheet).flushRows(100);

					}
					cell = 1;
					i++;
					serialNumber++;
				}
				((SXSSFSheet) dataSheet).trackAllColumnsForAutoSizing();
				for (int hCell = 0; hCell < numOfColumns + 1; hCell++) {
					log.info("column index >>>>>>>>>>>>>>>>"+hCell);
					dataSheet.autoSizeColumn(hCell);
				}
			} /*catch (ParseException e) {
				log.info("Generate Excel Date Parse Exception : ", e);
			}*/ catch (IllegalArgumentException iae) {
				// throw new

				// ReportsException("No Data Available to Export"); }

			} catch (Exception ex) {
				log.info("Generate Excel Exception : ", ex);
			}
		}
		File temp = null;
		Random generator = new Random();
		String fileName = "";
		fileName = baseReport.getReportName().replaceAll(" ", "_");
		Long userId = baseReport.getCreatedById();
		DecimalFormat formatter = new DecimalFormat("000");
		try {
			int num = generator.nextInt(1000); // Will this generate a file w/// 1000 num?
			Date date = new Date();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
			fileName = "/" + fileName + "_" + dateFormat.format(date) + "_" + userId + "_" + formatter.format(num)
					+ ".xlsx";
			temp = new File(uploadFilePath + fileName);
			temp.createNewFile();
		} catch (Exception e) {
			e.printStackTrace();
		}
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(temp);
			workbook.write(fos);
			fos.flush();
			fos.close();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		return uploadFileUrl + fileName;
	}

}
