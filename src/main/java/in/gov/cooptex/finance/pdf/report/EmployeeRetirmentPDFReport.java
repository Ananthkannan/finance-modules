package in.gov.cooptex.finance.pdf.report;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import in.gov.cooptex.mis.dto.BaseReportDto;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class EmployeeRetirmentPDFReport {

	private Font dynamicCellFont = new Font(FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.BLACK);
	private Font font = new Font(FontFamily.TIMES_ROMAN, 12, Font.BOLD, BaseColor.WHITE);
	private PdfPCell cell = null;

	/**
	 * for this functionality to get reports of pdf as byte array
	 * 
	 * @param List
	 *            of Report Dto , BaseReport
	 * @return byte[] of pdf
	 * @throws IOException
	 */

	public String generatePdfReport(List<Map<String, Object>> reportList, BaseReportDto baseReport,
			String uploadFilePath, String uploadFileUrl) throws DocumentException, IOException {

		Random generator = new Random();
		DecimalFormat formatter = new DecimalFormat("000");
		String fileName = "";
		Long userId = baseReport.getCreatedById();
		fileName = baseReport.getReportName().replaceAll(" ", "_");
		int num = generator.nextInt(1000); // Will this generate a file w/ 1000 num?
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		fileName = "/" + fileName + "_" + dateFormat.format(date) + "_" + userId + "_" + formatter.format(num) + ".pdf";

		List<String> showColumn = baseReport.getColumnsMap();

		// step 1(create document based on page)
		Document document = null;
		if (baseReport.getPdfType().equals("landscape")) {
			document = new Document();
			Rectangle size = new Rectangle(2000, 1000);
			document.setPageSize(size);
			document.setMargins(20, 20, 20, 20);

		} else {
			document = new Document(PageSize.A4, 30, 30, 40, 80);
		}

		// step 2

		PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(uploadFilePath + fileName));
		writer.setBoxSize("art", new Rectangle(36, 54, 559, 788));
		ITextHeaderAndFooter event = new ITextHeaderAndFooter();
		writer.setPageEvent(event);

		// step 3
		document.open();
		if (baseReport != null) {
			document.add(createReportHeader(baseReport));
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);

			// If search fields exist show search based on search field label and values
			/*
			 * Map<String, String> searchFields = baseReport.getSearchCriteria(); if
			 * (searchFields != null && searchFields.size() > 0) {
			 * document.add(createReportSearchFieldsToHeader(baseReport));
			 * document.add(Chunk.NEWLINE); document.add(Chunk.NEWLINE); }
			 */
			if (reportList != null && reportList.size() > 0) {
				// step 4
				document.add(createTable(reportList, baseReport));
				document.newPage();
			}
		}
		// step 5
		document.close();
		return uploadFileUrl + fileName;
	}

	public static byte[] convertDocToByteArray(String sourcePath) {
		byte[] bytes = null;
		InputStream inputStream = null;
		try {
			File file = new File(sourcePath);
			inputStream = new FileInputStream(file);
			bytes = new byte[(int) file.length()];
			inputStream.read(bytes);
		} catch (Exception e) {
			log.info("Exceptio in conver doc to byte array ", e);
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					log.info("Exceptio in conver doc to byte array ", e);
				}
			}
		}
		return bytes;
	}

	private Element createReportHeader(BaseReportDto baseReport) {
		PdfPTable table = new PdfPTable(6);
		PdfPTable headerRightContent = new PdfPTable(1);

		try {

			table.setWidthPercentage(90);

			table.setWidths(new int[] { 2, 3, 3, 3, 3, 3 });

			// Report logo
			// Document document=new Document();
			// PdfWriter PdfWriter.getInstance(document,new
			// FileOutputStream(baseReport.getLogoPath()));
			Image image = Image.getInstance(baseReport.getLogoPath());
			image.setWidthPercentage(20);

			cell = new PdfPCell(image);
			cell.setPaddingTop(4);
			cell.setPaddingBottom(5);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.BOTTOM);

			cell.setHorizontalAlignment(Element.ALIGN_LEFT);

			table.addCell(cell);

			cell = new PdfPCell(new Phrase("", dynamicCellFont));
			cell.setBorder(Rectangle.BOTTOM);
			cell.setPaddingTop(20);
			cell.setPaddingBottom(5);
			cell.setColspan(3);
			table.addCell(cell);
			// Report Right content

			PdfPCell c2;
			cell = new PdfPCell(new Phrase("Created By : " + baseReport.getCreatedBy()));
			cell.setBorder(Rectangle.NO_BORDER);
			headerRightContent.addCell(cell);
			cell = new PdfPCell(new Phrase("Created Date : " + baseReport.getCreatedDate()));
			cell.setBorder(Rectangle.NO_BORDER);
			headerRightContent.addCell(cell);
			c2 = new PdfPCell(headerRightContent);// this line made the
													// difference
			c2.setPaddingTop(15);
			c2.setPaddingBottom(5);
			c2.setColspan(2);
			c2.setBorder(Rectangle.BOTTOM);
			c2.setBackgroundColor(BaseColor.WHITE);
			c2.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(c2);
		} catch (Exception e) {
			log.error("Exception on Batch report Creation ", e);
		}

		return table;
	}

	private Element createReportSearchFieldsToHeader(BaseReportDto baseReport) {
		Map<String, String> searchFields = baseReport.getSearchCriteria();

		PdfPTable table = new PdfPTable(1);

		try {

			table.setWidthPercentage(90);
			StringBuilder content = new StringBuilder();
			content.append("Reports Based On - ");
			for (Map.Entry<String, String> searchField : searchFields.entrySet()) {
				content.append("  " + searchField.getKey() + " : " + searchField.getValue() + "  ");
			}
			cell = new PdfPCell(new Phrase(content.toString(), dynamicCellFont));
			cell.setPaddingTop(4);
			cell.setPaddingBottom(5);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell.setBorder(Rectangle.NO_BORDER);

			cell.setHorizontalAlignment(Element.ALIGN_LEFT);

			table.addCell(cell);

		} catch (Exception e) {
			log.error("Exception on Batch report Creation ", e);
		}

		return table;
	}

	private Element createTable(List<Map<String, Object>> reportList, BaseReportDto baseReport)
			throws DocumentException, IOException {

		List<String> showColumn = baseReport.getColumnsMap();

		int numOfColumns = showColumn.size();

		// a table with number of columns
		PdfPTable table = new PdfPTable(numOfColumns + 1);
		try {
			table.setWidthPercentage(90);

			int columnWidths[] = new int[numOfColumns + 1];
			columnWidths[0] = 2;
			for (int i = 1; i < numOfColumns + 1; i++) {
				columnWidths[i] = 3;
			}
			table.setWidths(columnWidths);
			table.setSpacingBefore(15f);
			table.setSpacingAfter(10f);
			table.setHeaderRows(2);
			// Report Heading
			cell = new PdfPCell(new Phrase(baseReport.getReportHeader(), font));
			cell.setBackgroundColor(BaseColor.BLUE);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);

			cell.setPaddingTop(4);
			cell.setPaddingBottom(4);
			cell.setColspan(numOfColumns + 1);
			table.addCell(cell);

			Integer listSize = reportList.size();
			if (reportList != null && listSize > 0) {
				// Table Header
				addHeaderDataCellToTable(table, "#");
				for (int hCell = 0; hCell < numOfColumns; hCell++) {
					if (showColumn.get(hCell).equalsIgnoreCase("Employee_Name")) {
						addHeaderDataCellToTable(table, "Employee Code / Name");
					} else {
						addHeaderDataCellToTable(table, showColumn.get(hCell).replace("_", " "));
					}
				}

				Integer counter = 1;

				for (Map<String, Object> report : reportList) {

					// Table Body
					addDataCellToTable(table, counter.toString());
					for (String colName : showColumn) {
						if (report.get(colName) != null) {
							addDataCellToTable(table, report.get(colName).toString());
						} else {
							addDataCellToTable(table, "");
						}

					}
					counter++;
				}

			}
		} catch (IllegalArgumentException iae) {
			// throw new ReportsException("No Data Available to Export", iae);
		} catch (Exception e) {
			log.error("Exception on Batch report Creation ", e);
		}

		return table;
	}

	/**
	 * for this functionality to add cell to the table with normal style
	 * 
	 * @param table
	 *            instance, cell value
	 * @return
	 * @throws IOException
	 * @throws DocumentException
	 */

	public void addDataCellToTable(PdfPTable table, String cellValue) throws DocumentException, IOException {
		cell = new PdfPCell(new Phrase(cellValue, dynamicCellFont));
		cell.setPaddingLeft(3);
		cell.setPaddingTop(4);
		cell.setPaddingBottom(4);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
	}

	public void addTotalToTable(PdfPTable table, Long cellValue) throws DocumentException, IOException {
		cell = new PdfPCell(new Phrase(cellValue));
		cell.setPaddingLeft(3);
		cell.setPaddingTop(4);
		cell.setPaddingBottom(4);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
	}

	// For Total Row
	public void addTotalDataCellToTable(PdfPTable table, String cellValue, int alignment) {
		Font totalFont = new Font(FontFamily.TIMES_ROMAN, 12, Font.BOLD, BaseColor.BLACK);
		cell = new PdfPCell(new Phrase(cellValue, totalFont));
		cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
		cell.setPaddingLeft(3);
		cell.setPaddingTop(4);
		cell.setPaddingBottom(4);
		if ("Total".equalsIgnoreCase(cellValue)) {
			cell.setColspan(3);
		}
		cell.setHorizontalAlignment(alignment);
		table.addCell(cell);
	}

	// Right Align
	public void alignDataToRight(PdfPTable table, String cellValue) throws DocumentException, IOException {
		cell = new PdfPCell(new Phrase(cellValue, dynamicCellFont));
		cell.setPaddingRight(6);
		cell.setPaddingTop(4);
		cell.setPaddingBottom(4);
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		table.addCell(cell);
	}

	public void addDataCellToTable(PdfPTable table, String cellValue, BaseFont bf)
			throws DocumentException, IOException {
		cell = new PdfPCell(new Phrase(cellValue, new Font(bf, 12)));
		cell.setPaddingLeft(3);
		cell.setPaddingTop(4);
		cell.setPaddingBottom(4);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
	}

	/**
	 * for this functionality to add cell for header to the table with header styles
	 * * @param table instance, cell value
	 * 
	 * @return
	 */

	public void addHeaderDataCellToTable(PdfPTable table, String cellValue) {
		cell = new PdfPCell(new Phrase(cellValue, dynamicCellFont));
		cell.setBackgroundColor(BaseColor.GRAY);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setPaddingLeft(3);
		cell.setPaddingTop(4);
		cell.setPaddingBottom(4);
		table.addCell(cell);
	}

	public void addSummaryRowToTable(PdfPTable table, String content, String cellValue, Integer rows) {
		cell = new PdfPCell(new Phrase(content, dynamicCellFont));
		cell.setColspan(rows - 1);
		cell.setBackgroundColor(BaseColor.GRAY);
		cell.setPaddingLeft(3);
		cell.setPaddingTop(4);
		cell.setPaddingBottom(4);
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase(cellValue, dynamicCellFont));
		cell.setBackgroundColor(BaseColor.GRAY);
		cell.setPaddingLeft(3);
		cell.setPaddingTop(4);
		cell.setPaddingBottom(4);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
	}

	// Right Align
	public void addDataCellToRight(PdfPTable table, String cellValue) throws DocumentException, IOException {
		cell = new PdfPCell(new Phrase(cellValue, dynamicCellFont));
		cell.setPaddingLeft(3);
		cell.setPaddingTop(4);
		cell.setPaddingBottom(4);
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		table.addCell(cell);
	}
}
