package in.gov.cooptex.common.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.finance.model.EmpLoanPayment;
import in.gov.cooptex.finance.service.LoanPaymentService;
import lombok.extern.log4j.Log4j2;

@RestController
@Log4j2
@RequestMapping("/loanPayment")
public class LoanPaymentController {
	
	@Autowired
	LoanPaymentService loanPaymentService;
	
	
	
	@RequestMapping(value = "/loadloanpreclosureDataList",method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<BaseDTO> loadLoanPreclosureRequestData(@RequestBody PaginationDTO paginationDTO)
	{
		log.info(" === Start LoanPaymentBeanController.loadLoanPreclosureRequestData ===");
		BaseDTO baseDTO=loanPaymentService.loadList(paginationDTO);
		log.info(" === End LoanPaymentBeanController.loadLoanPreclosureRequestData ===");
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/loademployeeloaninformationbyloanid/{loanId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<BaseDTO> loadEmployeeLoanInformationByLoanId(@PathVariable Long loanId) {
		log.info(":: Enter into LoanPaymentController Controller - loadEmployeeLoanInformationByLoanId ::");
		BaseDTO baseDTO = loanPaymentService.loadEmployeeLoanInformationByLoanId(loanId);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	/**
	 * @author krishnakumar
	 * @param loanId
	 * @return
	 */
	@RequestMapping(value = "/loademploaninfodetailsbyloanid/{loanId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<BaseDTO> loadEmpLoanInfoDetailsByLoanid(@PathVariable Long loanId) {
		log.info(":: Enter into LoanPaymentController Controller - loadEmpLoanInfoDetailsByLoanid ::");
		BaseDTO baseDTO = loanPaymentService.loadEmpLoanInfoDetailsByLoanid(loanId);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	

	@RequestMapping(value="/saveemploanpayment",method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<BaseDTO> saveEmpLoanPayment(@RequestBody EmpLoanPayment empLoanPayment)
	{
		log.info(":: Enter into LoanPaymentController Controller - saveEmpLoanPayment ::");
		BaseDTO baseDto=loanPaymentService.saveEmployeeLoanPayment(empLoanPayment);
		return new ResponseEntity<BaseDTO>(baseDto,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getemployeedetails/{loanId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<BaseDTO> loadEmpDetailsByLoanid(@PathVariable Long loanId) {
		log.info(":: Enter into LoanPaymentController Controller - loadEmpLoanInfoDetailsByLoanid ::");
		BaseDTO baseDTO = loanPaymentService.getEmployeeDetailsByLoanId(loanId);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/checkalreadypayrollrun/{loanId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<BaseDTO> checkAlreadyPayrollRunCurrentMonth(@PathVariable Long loanId) {
		log.info("EmpLoanDisbursementController checkAlreadyPayrollRunCurrentMonth() ");
		BaseDTO baseDTO = loanPaymentService.checkAlreadyPayrollRunCurrentMonth(loanId);
		return new ResponseEntity<BaseDTO>(baseDTO, HttpStatus.OK);
	}
}
