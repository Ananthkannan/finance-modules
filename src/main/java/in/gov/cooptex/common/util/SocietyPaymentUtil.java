package in.gov.cooptex.common.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.ui.EntityType;
import in.gov.cooptex.core.util.DBUtil;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.dto.pos.ProductVarietyDTO;
import in.gov.cooptex.finance.dto.PurchaseInvoiceDTO;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class SocietyPaymentUtil {
	
	private static final String GET_SOCIETY_PAYMENT_DATA_QUERY = "SELECT * FROM temp_society_pending_invoices";
	
	private static final String GET_HEAD_OFFICE_ENTITY_ID_QUERY = "SELECT id FROM entity_master WHERE "
			+ "entity_type_id=(SELECT id FROM entity_type_master WHERE code='${entityCode}')";
	
	private static final String GET_SUPPLIER_ID_QUERY = "SELECT id FROM supplier_master WHERE "
			+ "code='${supplierCode}'";
	
	private static final String GET_PRODUCT_VARIETY_TAX_ID_QUERY = "SELECT id FROM product_variety_tax WHERE "
			+ "product_variety_id=${varietyId} AND tax_id=(SELECT id FROM tax_master WHERE tax_code='${taxCode}') AND tax_percent = ${taxPercent}";
	
	private static final String PURCHASE_INVOICE_INSERT_QUERY = "INSERT INTO public.purchase_invoice " + 
			"(entity_id, invoice_number_prefix, invoice_number, supplier_id, invoice_date, material_value,status,created_by, created_date, modified_by, modified_date, version) " + 
			"VALUES(:entityId, :invoiceNumberPrefix, :invoiceNumber, :supplierId, :invoiceDate, :materialValue,:status,:createdBy,:createdDate,:modifiedBy,:modifiedDate,:version)";
	
	private static final String PURCHASE_INVOICE_ITEMS_INSERT_QUERY = "INSERT INTO public.purchase_invoice_items " + 
			"(purchase_invoice_id, product_id, item_qty, uom_id, unit_rate, item_amount, created_by, created_date, modified_by, modified_date, version) " + 
			"VALUES(:invoiceId, :productId, :itemQty, :uomId, :unitRate, :itemAmount, :createdBy,:createdDate,:modifiedBy,:modifiedDate,:version)";
	
	private static final String PURCHASE_INVOICE_ITEMS_TAX_INSERT_QUERY = "INSERT INTO public.purchase_invoice_item_tax " + 
			"(purchase_invoice_item_id, product_variety_tax_id, tax_value, created_by, created_date, modified_by, modified_date, version) " + 
			"VALUES(:itemId,:varietyTaxId,:taxValue,:createdBy,:createdDate,:modifiedBy,:modifiedDate,:version)";
	
	private static final String SET_MAXID_IN_SEQUENCE_QUERY = "SELECT setval('${sequenceId}', coalesce(max(id),0)+1, true) FROM ${tableName}";
	
	private static final String GET_USER_DETAILS_QUERY = "SELECT id AS userId FROM user_master WHERE username='${userName}'";
	
	private static final String GET_PRODUCT_VARIETY_DETAILS_QUERY = "SELECT pvm.id AS varietyId,pvm.code AS productCode,pvm.name AS productName,pvm.uom_id AS uomId FROM product_variety_master pvm WHERE LOWER(pvm.code) = LOWER('${varietyCode}')";
	
	private static final SimpleDateFormat currentFormat = new SimpleDateFormat("ddMMyyyy");
	
	private List<String> taxCodeList = Arrays.asList("CGST","SGST");

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	private Long headOfficeEntityId;
	
	private Long userId;
	
	private Date currentDate = new Date();
	
	public static void main(String... strings) {
		
		SocietyPaymentUtil societyPaymentUtil = new SocietyPaymentUtil();
		societyPaymentUtil.initDB();
		String userName = "hosuper";
		societyPaymentUtil.getSocietyInvoiceValues(userName);
	}

	private Long getHeadOfficeEntityId() {

		String getEntityIdSQL = GET_HEAD_OFFICE_ENTITY_ID_QUERY;
		
		getEntityIdSQL = StringUtils.replace(getEntityIdSQL, "${entityCode}", EntityType.HEAD_OFFICE);
		
		return jdbcTemplate.query(getEntityIdSQL, new ResultSetExtractor<Long>() {
			@Override
			public Long extractData(ResultSet rs) throws SQLException, DataAccessException {
				return rs.next() ? rs.getLong("id") : null;
			}
		});
	}
	
	private Long getSupplierIdByCode(String supplierCode) {

		String getSupplierIdSQL = GET_SUPPLIER_ID_QUERY;
		
		getSupplierIdSQL = StringUtils.replace(getSupplierIdSQL, "${supplierCode}", supplierCode);
		
		return jdbcTemplate.query(getSupplierIdSQL, new ResultSetExtractor<Long>() {
			@Override
			public Long extractData(ResultSet rs) throws SQLException, DataAccessException {
				return rs.next() ? rs.getLong("id") : null;
			}
		});
	}
	
	private void setMaxIdInSequenceTables(String sequenceId, String tableName) {
		log.info("SocietyPaymentUtil. setMaxIdInSequenceTables() Starts ");
		String sequenceMaxIdSQL = SET_MAXID_IN_SEQUENCE_QUERY;
		sequenceMaxIdSQL = StringUtils.replace(sequenceMaxIdSQL, "${sequenceId}", sequenceId);
		sequenceMaxIdSQL = StringUtils.replace(sequenceMaxIdSQL, "${tableName}", tableName);
		log.info("Sequence Max Id Set Query :" + sequenceMaxIdSQL);
		jdbcTemplate.execute(sequenceMaxIdSQL);
	}
	
	private Long getProductVarietyTaxId(Long varietyId,String taxCode,Double taxPercent) {

		String getVarietyTaxIdSQL = GET_PRODUCT_VARIETY_TAX_ID_QUERY;
		
		getVarietyTaxIdSQL = StringUtils.replace(getVarietyTaxIdSQL, "${varietyId}", String.valueOf(varietyId));
		
		getVarietyTaxIdSQL = StringUtils.replace(getVarietyTaxIdSQL, "${taxCode}", taxCode);
		
		getVarietyTaxIdSQL = StringUtils.replace(getVarietyTaxIdSQL, "${taxPercent}", String.valueOf(taxPercent));
		
		return jdbcTemplate.query(getVarietyTaxIdSQL, new ResultSetExtractor<Long>() {
			@Override
			public Long extractData(ResultSet rs) throws SQLException, DataAccessException {
				return rs.next() ? rs.getLong("id") : null;
			}
		});
	}
	
	private ProductVarietyDTO getProductVarietyByCode(String varietyCode) {
		log.info("SocietyPaymentUtil. getProductVarietyByCode() Starts ");
		ProductVarietyDTO productVarietyDTO = null;
		try {
			String getProductVarietySQL = GET_PRODUCT_VARIETY_DETAILS_QUERY;
			getProductVarietySQL = StringUtils.replace(getProductVarietySQL, "${varietyCode}", varietyCode);
			log.info("Product Variety Details Query :" + getProductVarietySQL);
			List<Map<String, Object>> dataMapList = jdbcTemplate.queryForList(getProductVarietySQL);
			int dataMapListSize = dataMapList == null ? 0 : dataMapList.size();
			if (dataMapListSize > 0) {
				productVarietyDTO = new ProductVarietyDTO();
				Map<String, Object> dataMap = dataMapList.get(0);
				productVarietyDTO.setId(
						dataMap.get("varietyId") == null ? null : Long.valueOf(dataMap.get("varietyId").toString()));
				productVarietyDTO
						.setProdCode(dataMap.get("productCode") == null ? null : dataMap.get("productCode").toString());
				productVarietyDTO
						.setProdName(dataMap.get("productName") == null ? null : dataMap.get("productName").toString());

				productVarietyDTO
						.setUomId(dataMap.get("uomId") == null ? null : Long.valueOf(dataMap.get("uomId").toString()));
			} else {
				productVarietyDTO = null;
			}
		} catch (Exception e) {
			log.error("Exception at StockOBUploadUtil. getProductVarietyByCode() ", e);
		}
		return productVarietyDTO;
	}
	
	private List<Map<String,Object>> getSocietyInvoiceValues(String userName){
		log.info("SocietyPaymentUtil. getSocietyInvoiceValues() Starts");
		List<PurchaseInvoiceDTO> dataMapList = null;
		Map<String,Object> supplierMap = null;
		Long supplierId = null;
		Long purchaseInvocieId = null;
		Map<String,ProductVarietyDTO> productMap = null;
		Map<String,Long> varietyTaxIdMap = null;
		try {
			
			userId = getCurrentUserId(userName);
			dataMapList = new ArrayList<>();
			headOfficeEntityId = getHeadOfficeEntityId();
			supplierMap = new HashMap<>();
			productMap = new HashMap<>();
			varietyTaxIdMap = new HashMap<>();
			
			dataMapList =jdbcTemplate.query(GET_SOCIETY_PAYMENT_DATA_QUERY,
					new BeanPropertyRowMapper<PurchaseInvoiceDTO>(PurchaseInvoiceDTO.class));
			
			int dataMapListSize = dataMapList == null ? 0 : dataMapList.size();
			log.info("Society Invoice Data List Size :"+dataMapListSize);
			if(dataMapListSize > 0) {
				Map<String, List<PurchaseInvoiceDTO>> dataMap = dataMapList.stream()
		                .collect(Collectors.groupingBy(p -> getGroupingByKey(p)));
				int dataMapSize = dataMap == null ? 0 : dataMap.size();	
				log.info("Purchase Invoice Data List Size After Grouped By Invoice Number and Society Code :"+dataMapSize);
				
				for (Map.Entry<String, List<PurchaseInvoiceDTO>> entry : dataMap.entrySet()) {
					
					String key = entry.getKey();
					List<PurchaseInvoiceDTO> dtoValuesList = entry.getValue();
					int dtoValuesListSize = dtoValuesList == null ? 0 : dtoValuesList.size();
					if(dtoValuesListSize > 0) {
						String supplierCode = key.split("-")[1];
						if(supplierMap != null && supplierMap.containsKey(supplierCode)) {
							log.info("Supplier Data is already exist in Map");
						}else {
							supplierId = getSupplierIdByCode(supplierCode);
							supplierMap.put(supplierCode, supplierId);
						}
						PurchaseInvoiceDTO purchaseInvoiceDTO = dtoValuesList.get(0);
						dtoValuesList.forEach(invoiceDetail ->  {
							invoiceDetail.setInvoiceAmount(stringToDouble(invoiceDetail.getPendingInvoiceAmount()));
							invoiceDetail.setItemQty(stringToDouble(invoiceDetail.getInvoiceQty()));
							invoiceDetail.setItemInvoiceValue(stringToDouble(invoiceDetail.getInvoiceValue()));
							invoiceDetail.setItemGstPercent(stringToDouble(invoiceDetail.getGstPercent()));
							invoiceDetail.setItemGstAmount(stringToDouble(invoiceDetail.getGstAmount()));
						});
							
						double materialValue = dtoValuesList.stream().filter(o -> o.getInvoiceAmount() != null)
								.collect(Collectors.summingDouble(PurchaseInvoiceDTO::getInvoiceAmount));
						purchaseInvocieId = savePurchaseInvoiceTables(key,purchaseInvoiceDTO,supplierMap,materialValue);
					}
					
					Map<String, List<PurchaseInvoiceDTO>> itemsDataObjMap = dtoValuesList.stream()
							.collect(Collectors.groupingBy(a -> a.getProductCode()));
					
					log.info("itemsDataObjMap "+itemsDataObjMap);
					
					for (Map.Entry<String, List<PurchaseInvoiceDTO>> itemsEntry : itemsDataObjMap.entrySet()) {
						
						try {
								List<PurchaseInvoiceDTO> itemsValues = itemsEntry.getValue();
								
								double itemQty = itemsValues.stream().filter(o -> o.getItemQty() != null)
										.collect(Collectors.summingDouble(PurchaseInvoiceDTO::getItemQty));
								
								String varietyCode = itemsValues.get(0).getProductCode();
								
								double invoiceValue = itemsValues.get(0).getItemInvoiceValue();
								
						        double taxPercent = itemsValues.get(0).getItemGstPercent();
						        
						        double taxValue = itemsValues.get(0).getItemGstAmount();
								
								double unitRate = invoiceValue / itemQty;
								
								double roundedUnitRate = Math.round(unitRate * 100.0) / 100.0;
														
								if(productMap != null && StringUtils.isNotEmpty(varietyCode) && 
										productMap.containsKey(varietyCode)) {
									log.info("Product Data is already exist in Map");
								}else {
									ProductVarietyDTO varietyDTO = getProductVarietyByCode(varietyCode);
									productMap.put(varietyCode, varietyDTO);
								}
								
								ProductVarietyDTO varietyDTOObj = new ProductVarietyDTO();
								
								varietyDTOObj = productMap.get(varietyCode);
								
								if(varietyDTOObj != null) {
									KeyHolder holder = new GeneratedKeyHolder();
									
										SqlParameterSource parameters = new MapSqlParameterSource()
										.addValue("invoiceId", purchaseInvocieId)
										.addValue("productId", varietyDTOObj == null ? null : varietyDTOObj.getId())
										.addValue("itemQty",itemQty)
										.addValue("uomId",varietyDTOObj == null ? null : varietyDTOObj.getUomId())
										.addValue("unitRate",roundedUnitRate)
										.addValue("itemAmount",invoiceValue)
										.addValue("createdBy", userId)
										.addValue("createdDate", currentDate)
										.addValue("modifiedBy", userId)
										.addValue("modifiedDate", currentDate)
										.addValue("version", 0);
			
										namedParameterJdbcTemplate.update(PURCHASE_INVOICE_ITEMS_INSERT_QUERY, parameters, holder, new String[] { "id" });
									
									Long purchaseInvoiceItemsId = holder.getKey().longValue();
									
									int taxCodeListSize = taxCodeList == null ? 0 : taxCodeList.size();
									if(taxCodeListSize > 0) {
										for (String taxCode : taxCodeList) {
											
											try {
												String varietyTaxKey = varietyDTOObj.getId()+"-"+taxCode;
												if(varietyTaxIdMap != null && varietyTaxIdMap.containsKey(varietyTaxKey)) {
													log.info("Product Data is already exist in Map");
												}else {
													Long varietyTaxId = getProductVarietyTaxId(varietyDTOObj.getId(), taxCode , taxPercent / taxCodeListSize);
													varietyTaxIdMap.put(varietyTaxKey, varietyTaxId);
												}
												
												Long productVarietyTaxId = varietyTaxIdMap.get(varietyTaxKey);
												if(productVarietyTaxId != null) {
													SqlParameterSource itemtaxparameters = new MapSqlParameterSource()
															.addValue("itemId", purchaseInvoiceItemsId)
															.addValue("varietyTaxId", varietyTaxIdMap.get(varietyTaxKey))
															.addValue("taxValue",taxValue / taxCodeListSize)
															.addValue("createdBy", userId)
															.addValue("createdDate", currentDate)
															.addValue("modifiedBy", userId)
															.addValue("modifiedDate", currentDate)
															.addValue("version", 0);
				
													namedParameterJdbcTemplate.update(PURCHASE_INVOICE_ITEMS_TAX_INSERT_QUERY, itemtaxparameters);
												}
											}catch(Exception e) {
												log.error("Exception at Purchase Invoice Items Tax Saved",e);
											}
										}
									}
								}
								
						}catch(Exception e) {
							log.error("Exception at Purchase Invoice Items Saved",e);	
						}
					}
				}
				setMaxIdInSequenceTables("purchase_invoice_id_seq", "purchase_invoice");
				setMaxIdInSequenceTables("purchase_invoice_items_id_seq", "purchase_invoice_items");
				setMaxIdInSequenceTables("purchase_invoice_item_tax_id_seq", "purchase_invoice_item_tax");
			}
		}catch(Exception e) {
			log.error("Exception at SocietyPaymentUtil. convertSocietyInvoiceValues() ",e);
		}
		log.info("SocietyPaymentUtil. getSocietyInvoiceValues() Ends");
		return null;
	}
	
	private Long getCurrentUserId(String userName) {
		log.info("SocietyPaymentUtil. getCurrentUserId() Starts ");
		String getCurrentUserIdSQL = GET_USER_DETAILS_QUERY;
		getCurrentUserIdSQL = StringUtils.replace(getCurrentUserIdSQL, "${userName}", String.valueOf(userName));

		return jdbcTemplate.query(getCurrentUserIdSQL, new ResultSetExtractor<Long>() {
			@Override
			public Long extractData(ResultSet rs) throws SQLException, DataAccessException {
				return rs.next() ? rs.getLong("userId") : null;
			}
		});
	}
	
	private Long savePurchaseInvoiceTables(String key, PurchaseInvoiceDTO purchaseInvoiceDTO,
			Map<String,Object> supplierMap,Double materialValue) {
		log.info("SocietyPaymentUtil. savePurchaseInvoiceTables() ");
		Long purchaseInvoiceId = null;
		KeyHolder holder = new GeneratedKeyHolder();
		try {
			headOfficeEntityId = getHeadOfficeEntityId();
			String invoiceNumber = key.split("-")[0];
			String supplierCode = key.split("-")[1];
			String invoiceDateMonYearString = purchaseInvoiceDTO.getInvoiceDate();
			String invoiceNumberPrefix = null;
			if(StringUtils.isNotEmpty(invoiceDateMonYearString)) {
				String invoiceDateMonString = getMonthName(Integer.parseInt(getData(invoiceDateMonYearString,2,4)));
				String invoiceDateYearString = getData(invoiceDateMonYearString,4,8);
				invoiceNumberPrefix = new StringBuilder().append("PI").append(invoiceDateMonString).
						append(invoiceDateYearString).toString();
			}
			String formattedDate = AppUtil.REPORT_DATE_FORMAT.format(currentFormat.parse(invoiceDateMonYearString));
			Date invoiceDate = AppUtil.REPORT_DATE_FORMAT.parse(formattedDate);
						
			SqlParameterSource parameters = new MapSqlParameterSource()
			.addValue("entityId", headOfficeEntityId)
			.addValue("invoiceNumberPrefix", invoiceNumberPrefix)
			.addValue("invoiceNumber",StringUtils.isNotEmpty(invoiceNumber) ? Integer.parseInt(invoiceNumber) : null)
			.addValue("supplierId",supplierMap.get(supplierCode))
			.addValue("invoiceDate",invoiceDate)
			.addValue("materialValue",materialValue)
			.addValue("status",ApprovalStage.APPROVED)
			.addValue("createdBy", userId)
			.addValue("createdDate", currentDate)
			.addValue("modifiedBy", userId)
			.addValue("modifiedDate", currentDate)
			.addValue("version", 0);

			namedParameterJdbcTemplate.update(PURCHASE_INVOICE_INSERT_QUERY, parameters, holder, new String[] { "id" });
			purchaseInvoiceId = holder.getKey().longValue();
			
		}catch(Exception e) {
			log.error("Exception at SocietyPaymentUtil. savePurchaseInvoiceTables() ",e);
		}
		return purchaseInvoiceId;
	}

	private String getGroupingByKey(PurchaseInvoiceDTO p){
		return p.getInvoiceNumber()+"-"+p.getSocietyCode();
	}
	
	private static Double stringToDouble(String value) {
		if (StringUtils.isEmpty(value)) {
			log.info("Value is Empty");
			return 0.00;
		}
		//String amount = AppUtil.DECIMAL_FORMAT.format(BigDecimal.valueOf(Long.valueOf(value), 2));
		return Double.valueOf(value);
	}
	
	private String getMonthName(Integer monthNum) {
		String monthString;
	    switch (monthNum) {
	        case 1:  monthString = "Jan";     break;
	        case 2:  monthString = "Feb";     break;
	        case 3:  monthString = "Mar";     break;
	        case 4:  monthString = "Apr";     break;
	        case 5:  monthString = "May";     break;
	        case 6:  monthString = "Jun";     break;
	        case 7:  monthString = "Jul";     break;
	        case 8:  monthString = "Aug";     break;
	        case 9:  monthString = "Sep";     break;
	        case 10: monthString = "Oct";     break;
	        case 11: monthString = "Nov";     break;
	        case 12: monthString = "Dec";     break;
	        default: monthString = "Invalid month"; break;
	    }
	    return monthString;
	}
	
	private String getData(String lineData, int beginIndex, int endIndex) {
		try {
			return lineData.substring(beginIndex, endIndex);
		} catch (IndexOutOfBoundsException ex) {
			log.error("Data not available between " + beginIndex + " , " + endIndex + " - Line Number: ");
		} catch (Exception ex) {
			log.error("Exception at getData() @ Line Number >>>> " + ex.toString());
		}

		return null;
	}
	
	private void initDB() {
		DBUtil dbUtil = new DBUtil();
		jdbcTemplate = new JdbcTemplate();
		jdbcTemplate.setDataSource(dbUtil.getPGSimpleDataSource());
		namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dbUtil.getPGSimpleDataSource());
	}
}
